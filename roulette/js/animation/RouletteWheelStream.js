this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var RouletteWheelStream = function (scene,options,address){
        this.ABSVideoStream_constructor(scene,options,address);
        this.initialize_RouletteWheelStream();
    };

    var p = createjs.extend(RouletteWheelStream, alteastream.ABSVideoStream);

// static properties:
// events:
// VideoStreamSmall was here
// public vars:
// private vars:
    p.streamMonitor = null;
    p.spinnerWheel = null;
    p.spinnerWheelBackground = null;
// constructor:
    p.initialize_RouletteWheelStream = function (){
    };

    p.setPlayer = function(address){
        /* // local ver
        var streamPreviewParams = JSON.parse(localStorage.getItem("streamPreviewParams"));
        //temp HC "testroulette" instead of streamPreviewParams.machineName
        //this.webrtcPlayer.setPreview(streamPreviewParams.previewCamera, streamPreviewParams.shopMachine, "testroulette");
        this.webrtcPlayer.setPreview2(streamPreviewParams.previewCamera, streamPreviewParams.shopMachine, "testroulette");//setPreview2
        */ // local ver
        this.webrtcPlayer.setPreview(address.webrtcurl,alteastream.AbstractScene.GAME_TOKEN,alteastream.AbstractScene.GAME_ID);// local ver

        var videoStream = this.videoStream = new createjs.DOMElement(this.videoElement.htmlVideo);
        alteastream.Roulette.getInstance().addChildAt(videoStream,0);
        this.videoStream.scaleX = 0.32;
        this.videoStream.scaleY = 0.32;
        this.videoStream.mouseEnabled = false;

        this.showVideoStream(false);
        this.setDynamicPosition(-0.053,0.605);

        var _this = this;
        window.addEventListener("resize",function(){
            _this._resize();
        });

        this._local_activate();// local ver
    };

    p.handleError = function(error){
        var that = this;
        this.scene.throwAlert(alteastream.Alert.ERROR,error,function(){
            that.webrtcPlayer.stop();
        });
    }

    p.concreteActivate = function(){
        if(this.spinnerWheel){
            this.spinnerWheel.runPreload(false);
            this.scene.removeChild(this.spinnerWheel);
            this.scene.removeChild(this.spinnerWheelBackground);
            this.spinnerWheel = null;
        }
    };

    p.addSpinner = function () {};//temp toggle visibility

    p.appendSpinner = function () {//temp toggle visibility
        if(!this.streamMonitor._streamEstablished){
            var spinnerWheelBackground = this.spinnerWheelBackground = new createjs.Shape();
            spinnerWheelBackground.graphics.beginFill("#000000").drawRect(0, 0, 344, 344);
            spinnerWheelBackground.x = 29;
            spinnerWheelBackground.y = 656;

            var spinner = this.spinnerWheel = new alteastream.MockLoader();
            this.scene.addChild(spinnerWheelBackground, spinner);
            spinner.x = spinnerWheelBackground.x + 172;
            spinner.y = spinnerWheelBackground.y + 96;
            spinner.runPreload(true);
        }
    };

    p.setMedia = function(){

    }

    p.setPreloadPoster = function(bool){

    }

    p.addMonitor = function(){
        this.streamMonitor = new alteastream.StreamMonitor(this);
    };

    p.streamConnected = function(){
        this.streamMonitor.streamConnected();
    };

    p.startStreaming = function(){
        this.webrtcPlayer.start();//preview
    };

    p.onStreamFailed = function(){
        this.scene.throwAlert(alteastream.Alert.ERROR, "Streaming failed");
    }

    p.showVideoStream = function (show) {
        this.videoStream.visible = show;
    };

    p.setDynamicPosition = function(xPerc,yPerc){
        this._dynamicPositionXY = {x:xPerc,y:yPerc};
        this._resize();
    };

    p._getDynamicPosition = function(){
        return this._dynamicPositionXY;
    };

    p._resize = function(){
        var w = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        var h = (window.innerHeight > 0) ? window.innerHeight : screen.width;

        //this.SPACING_LEFT
        //this.SPACING_TOP
        var spacingX = 2*(w/1000);
        var spacingY = 2*(w/1000);

        var dynX = this._getDynamicPosition().x;
        var dynY = this._getDynamicPosition().y;

        this.videoStream.x = w*dynX-spacingX;
        this.videoStream.y = h*dynY+spacingY;
    };

    p._local_activate = function(){//click on screen while loading to start video
        this.videoElement.setSource("../lobby/videotest.mp4");
        this.videoElement.play();
        this.videoElement.setLoop(true);
        this.videoElement.setMuted(false);
        this.showVideoStream(true);

        // x:69 y:430
        /*var that = this;
        stage.addEventListener("stagemousedown", function(){
            var w = (window.innerWidth > 0) ? window.innerWidth : screen.width;
            var xPos = w/4;
            TweenLite.to(that.videoStream,0.3,{x:xPos,y:0,scaleX:0.5,scaleY:0.5,onComplete:function(that){
                    that.setDynamicPosition(0.252,-0.004);
                    setTimeout(function(){
                        that.videoStream.scaleX = 0.32;
                        that.videoStream.scaleY = 0.32;
                        that.setDynamicPosition(-0.053,0.605);},1000);
                },onCompleteParams:[that]});
        })*/

    };

    alteastream.RouletteWheelStream = createjs.promote(RouletteWheelStream,"ABSVideoStream");
})();