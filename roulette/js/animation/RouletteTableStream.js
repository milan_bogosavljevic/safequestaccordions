this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var RouletteTableStream = function (scene,options,address){
        this.ABSVideoStream_constructor(scene,options,address);
        this.initialize_RouletteTableStream();
    };

    var p = createjs.extend(RouletteTableStream, alteastream.ABSVideoStream);
// static properties:
// events:

// public vars:
// private vars:
    p.streamMonitor = null;
    p.spinner = null;
    p.spinnerBackground = null;
// constructor:
    p.initialize_RouletteTableStream = function (){
    };

    p.setPlayer = function(address){
        /* // local ver
        var streamPreviewParams = JSON.parse(localStorage.getItem("streamPreviewParams"));
        this.webrtcPlayer.setPreview(streamPreviewParams.previewCamera, streamPreviewParams.shopMachine, streamPreviewParams.machineName);
        */ // local ver
        this.webrtcPlayer.set(address.webrtcurl,alteastream.AbstractScene.GAME_TOKEN,alteastream.AbstractScene.GAME_ID);// local ver

        this._local_activate();// local ver
    };

    p.handleError = function(error){
        var that = this;
        this.scene.throwAlert(alteastream.Alert.ERROR,error,function(){
            that.webrtcPlayer.stop();
            that.scene.socketCommunicator.disposeCommunication();//quitPlay() if started
            that.scene.exitMachineGameplay();
        });
    };

    p.concreteActivate = function(){
        if(is.safari()){
            //var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
            //if(is.ipad() || is.ios() || isiPadOS){
            this.scene.setIosOverlay();
            //}
        }
        this.spinner.runPreload(false);
        this.scene.removeChild(this.spinner);
        this.scene.removeChild(this.spinnerBackground);
        this.spinner = null;

        if(this.scene.streamVideoSmall){
            this.scene.streamVideoSmall.showVideoStream(true);
        }
    };

    p.addSpinner = function(){
        var spinnerBackground = this.spinnerBackground = alteastream.Assets.getImage(alteastream.Assets.images.liveStreamConnecting);
        spinnerBackground.regX = spinnerBackground.image.width*0.5;
        spinnerBackground.regY = spinnerBackground.image.height*0.5;
        spinnerBackground.x = alteastream.AbstractScene.GAME_WIDTH *0.5;
        spinnerBackground.y = alteastream.AbstractScene.GAME_HEIGHT *0.5;

        var spinner = this.spinner = new alteastream.MockLoader();
        spinner.customScaleGfx(0.4);
        this.scene.addChild(spinnerBackground, spinner);
        //spinner.setLabel("Connecting to stream..");
        spinner.x = spinnerBackground.x + 196;
        spinner.y = spinnerBackground.y - 80;
        spinner.runPreload(true);
    };

    p.setMedia = function(){
        window.parent.switchSource("background_music");

        var soundMachineIsToggled = window.localStorage.getItem("soundMachineIsToggled");
        if(soundMachineIsToggled !== null) {
            soundMachineIsToggled = Boolean(soundMachineIsToggled);
        }else{
            soundMachineIsToggled = false;
        }

        if(soundMachineIsToggled === false){
            this.videoElement.setMuted(false);
            this.videoElement.setVolume(0.1);
            console.log("AUDIO ON:::::");
        }

        this.scene.controlBoard.restoreMachineSoundState();
        this.scene.controlBoard.restoreMusicSoundState();
    };

    p.setPreloadPoster = function(bool){
        /*var poster = alteastream.Assets.getImage(alteastream.Assets.images.poster);
                this.scene.addChild(poster);
                poster.mouseEnabled = false;*/
    };

    p.addMonitor = function(){
        this.streamMonitor = new alteastream.StreamMonitor(this);
    };

    p.startStreaming = function(){
        this.webrtcPlayer.start();//preview
    };

    p.streamConnected = function(){
        this.streamMonitor.streamConnected();
    };

    p.onStreamFailed = function(){
        var _this = this;
        this.scene.throwAlert(alteastream.Alert.ERROR, "Streaming failed. Exiting game",function(){_this.scene.onQuit();});
    };

    p._local_activate = function(){//click on screen while loading to start video
        this.videoElement.setSource("../lobby/videotest.mp4");
        this.videoElement.play();
        this.videoElement.setLoop(true);
        this.videoElement.setMuted(false);

        this.scene.controlBoard.restoreMachineSoundState();
        this.scene.controlBoard.restoreMusicSoundState();
    };

    alteastream.RouletteTableStream = createjs.promote(RouletteTableStream,"ABSVideoStream");
})();