

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var RouletteAssets = function() {};
    var Assets = alteastream.Assets;

    RouletteAssets.videoManifest = [
        'assets/sprite_sheets/drum.mp4'
    ];

    RouletteAssets.count = 0;

    RouletteAssets.loadManifest = function(lang){
        Assets.setImageLang(lang);
        var language = Assets.getImageLang();
        var localePath = "assets/images/locale/" + language + "/";
        return [
            // Images
            //{id:Assets.images.preloadingPoster, src:"assets/images/webrtc.png"},
            {id:Assets.images.mainBackgroundWithHole, src:"assets/images/cbMainBgWithHole.png"},
            //{id:Assets.images.backgroundCB, src:"assets/images/control_board_bg.jpg"},
            //{id:Assets.images.btnMockSS, src:"assets/images/buttons/btnMockSS.png"},
            {id:Assets.images.fieldSpec, src:"assets/images/buttons/fieldSpec.png"},
            {id:Assets.images.fieldSpecH, src:"assets/images/buttons/fieldSpecH.png"},
            {id:Assets.images.fieldMini, src:"assets/images/buttons/fieldMini.png"},
            {id:Assets.images.fieldMiniH, src:"assets/images/buttons/fieldMiniH.png"},
            {id:Assets.images.fieldSmall, src:"assets/images/buttons/fieldSmall.png"},
            {id:Assets.images.fieldSmall_red, src:"assets/images/buttons/fieldSmall_red.png"},
            {id:Assets.images.fieldSmall_black, src:"assets/images/buttons/fieldSmall_black.png"},
            {id:Assets.images.fieldSmall_green, src:"assets/images/buttons/fieldSmall_green.png"},
            {id:Assets.images.fieldMedium, src:"assets/images/buttons/fieldMedium.png"},
            {id:Assets.images.fieldLarge, src:"assets/images/buttons/fieldLarge.png"},
            {id:Assets.images.fieldZero, src:"assets/images/buttons/fieldZero.png"},
            {id:Assets.images.fieldSplit, src:"assets/images/buttons/fieldSplit.png"},
            {id:Assets.images.fieldSplitTest, src:"assets/images/buttons/fieldSplitTest.png"},//temp dev visibility test only
            {id:Assets.images.fieldSmallH, src:"assets/images/buttons/fieldSmallH.png"},
            {id:Assets.images.fieldMediumH, src:"assets/images/buttons/fieldMediumH.png"},
            {id:Assets.images.fieldLargeH, src:"assets/images/buttons/fieldLargeH.png"},
            {id:Assets.images.fieldZeroH, src:"assets/images/buttons/fieldZeroH.png"},
            {id:Assets.images.label_red, src:"assets/images/buttons/label_red.png"},
            {id:Assets.images.label_black, src:"assets/images/buttons/label_black.png"},
            {id:Assets.images.fieldTrack, src:"assets/images/buttons/fieldTrack.png"},
            {id:Assets.images.fieldTrack_black, src:"assets/images/buttons/fieldTrack_black.png"},
            {id:Assets.images.fieldTrack_red, src:"assets/images/buttons/fieldTrack_red.png"},
            {id:Assets.images.fieldTrack_green, src:"assets/images/buttons/fieldTrack_green.png"},
            {id:Assets.images.fieldTrackH, src:"assets/images/buttons/fieldTrackH.png"},
            {id:Assets.images.winningBg_red, src:"assets/images/buttons/winningBg_red.png"},
            {id:Assets.images.winningBg_black, src:"assets/images/buttons/winningBg_black.png"},
            {id:Assets.images.winningBg_green, src:"assets/images/buttons/winningBg_green.png"},
            {id:Assets.images.btnFullScreen_On,src:"assets/images/buttons/btnFullScreen_On.png"},
            {id:Assets.images.btnFullScreen_Off,src:"assets/images/buttons/btnFullScreen_Off.png"},

            {id:Assets.images.spinner, src:"assets/images/spinner.png"},
            {id:Assets.images.liveStreamConnecting, src:"assets/images/liveStreamConnecting.png"},
            //{id:Assets.images.btnIncSS, src:"assets/images/buttons/count_btn_plus.png"},
            //{id:Assets.images.btnDecSS, src:"assets/images/buttons/count_btn_minus.png"},
           // {id:Assets.images.btnMediumSS, src:"assets/images/buttons/middle_btn.png"},
            //{id:Assets.images.btnArrowLeftSS, src:"assets/images/buttons/left_btn.png"},
            //{id:Assets.images.btnArrowRightSS, src:"assets/images/buttons/right_btn.png"},
            //{id:Assets.images.btnMedium2SS, src:"assets/images/buttons/yello_btn.png"},

            //btns
            {id:Assets.images.btnQuitSS,src:"assets/images/buttons/quitSS.png"},
            {id:Assets.images.btnInfoSS,src:"assets/images/buttons/infoSS.png"},
            {id:Assets.images.btnStepBackSS,src:"assets/images/buttons/backBtn.png"},
            {id:Assets.images.btnClearSS,src:"assets/images/buttons/clearBtn.png"},
            {id:Assets.images.soundOn,src:"assets/images/buttons/sound_icon_default.png"},
            {id:Assets.images.soundOff,src:"assets/images/buttons/sound_icon_disabled.png"},
            {id:Assets.images.btnStart,src:"assets/images/buttons/start_btn.png"},
            {id:Assets.images.btnExitForQuitCounter,src:"assets/images/buttons/exit_btn_for_counter.png"},

            {id:Assets.images.chipBtn0,src:"assets/images/buttons/chipBtn0.png"},
            {id:Assets.images.chipBtn1,src:"assets/images/buttons/chipBtn1.png"},
            {id:Assets.images.chipBtn2,src:"assets/images/buttons/chipBtn2.png"},
            {id:Assets.images.chipBtn3,src:"assets/images/buttons/chipBtn3.png"},
            {id:Assets.images.chipBtn4,src:"assets/images/buttons/chipBtn4.png"},
            {id:Assets.images.chipBtn5,src:"assets/images/buttons/chipBtn5.png"},
            {id:Assets.images.chipBtn6,src:"assets/images/buttons/chipBtn6.png"},
            {id:Assets.images.chipBtn7,src:"assets/images/buttons/chipBtn7.png"},
           // {id:Assets.images.chipBlank,src:"assets/images/buttons/chipBlank.png"},

            {id:Assets.images.chip0,src:"assets/images/buttons/chip0.png"},
            {id:Assets.images.chip1,src:"assets/images/buttons/chip1.png"},
            {id:Assets.images.chip2,src:"assets/images/buttons/chip2.png"},
            {id:Assets.images.chip3,src:"assets/images/buttons/chip3.png"},
            {id:Assets.images.chip4,src:"assets/images/buttons/chip4.png"},
            {id:Assets.images.chip5,src:"assets/images/buttons/chip5.png"},
            {id:Assets.images.chip6,src:"assets/images/buttons/chip6.png"},
            {id:Assets.images.chip7,src:"assets/images/buttons/chip7.png"},


            //{id:Assets.images.singleShotTextImg,src:"assets/images/buttons/single.png"},
            //{id:Assets.images.burstShotTextImg,src:"assets/images/buttons/burst.png"},


           // {id:Assets.sounds.shooter,src:"assets/sounds/shooter.ogg"},
            //{id:Assets.sounds.shooter2,src:"assets/sounds/shooter2.ogg"},
            //{id:Assets.sounds.shooterStop,src:"assets/sounds/shootStop.ogg"},
           // {id:Assets.sounds.btnUp,src:"assets/sounds/btnUp.ogg"},
           // {id:Assets.sounds.btnDown,src:"assets/sounds/btnUp.ogg"},
           // {id:Assets.sounds.btnDownDouble,src:"assets/sounds/btnDownDouble.ogg"},
            //{id:Assets.sounds.bgMusicMachine,src:"assets/sounds/bgMusicMachine.ogg"},
            {id:Assets.sounds.error1,src:"assets/sounds/error1.ogg"},
            //{id:Assets.sounds.bgMusic, src:"assets/sounds/background_music.mp3"},
            {id:Assets.sounds.buttonClick, src:"assets/sounds/buttonClick.mp3"},
            {id:Assets.sounds.winSound, src:"assets/sounds/winSound.mp3"},
            {id:Assets.sounds.winCheer, src:"assets/sounds/winCheer.mp3"},
            {id:Assets.sounds.buttonCollect, src:"assets/sounds/buttonCollect.mp3"},
            {id:Assets.sounds.countLoop, src:"assets/sounds/countLoop.mp3"},
            {id:Assets.sounds.chipDown, src:"assets/sounds/chipDown.mp3"},

            {id:Assets.sounds.spoken_0, src:"assets/sounds/0.mp3"},
            {id:Assets.sounds.spoken_1, src:"assets/sounds/1.mp3"},
            {id:Assets.sounds.spoken_2, src:"assets/sounds/2.mp3"},
            {id:Assets.sounds.spoken_3, src:"assets/sounds/3.mp3"},
            {id:Assets.sounds.spoken_4, src:"assets/sounds/4.mp3"},
            {id:Assets.sounds.spoken_5, src:"assets/sounds/5.mp3"},
            {id:Assets.sounds.spoken_6, src:"assets/sounds/6.mp3"},
            {id:Assets.sounds.spoken_7, src:"assets/sounds/7.mp3"},
            {id:Assets.sounds.spoken_8, src:"assets/sounds/8.mp3"},
            {id:Assets.sounds.spoken_9, src:"assets/sounds/9.mp3"},
            {id:Assets.sounds.spoken_10, src:"assets/sounds/10.mp3"},
            {id:Assets.sounds.spoken_11, src:"assets/sounds/11.mp3"},
            {id:Assets.sounds.spoken_12, src:"assets/sounds/12.mp3"},
            {id:Assets.sounds.spoken_13, src:"assets/sounds/13.mp3"},
            {id:Assets.sounds.spoken_14, src:"assets/sounds/14.mp3"},
            {id:Assets.sounds.spoken_15, src:"assets/sounds/15.mp3"},
            {id:Assets.sounds.spoken_16, src:"assets/sounds/16.mp3"},
            {id:Assets.sounds.spoken_17, src:"assets/sounds/17.mp3"},
            {id:Assets.sounds.spoken_18, src:"assets/sounds/18.mp3"},
            {id:Assets.sounds.spoken_19, src:"assets/sounds/19.mp3"},
            {id:Assets.sounds.spoken_20, src:"assets/sounds/20.mp3"},
            {id:Assets.sounds.spoken_21, src:"assets/sounds/21.mp3"},
            {id:Assets.sounds.spoken_22, src:"assets/sounds/22.mp3"},
            {id:Assets.sounds.spoken_23, src:"assets/sounds/23.mp3"},
            {id:Assets.sounds.spoken_24, src:"assets/sounds/24.mp3"},
            {id:Assets.sounds.spoken_25, src:"assets/sounds/25.mp3"},
            {id:Assets.sounds.spoken_26, src:"assets/sounds/26.mp3"},
            {id:Assets.sounds.spoken_27, src:"assets/sounds/27.mp3"},
            {id:Assets.sounds.spoken_28, src:"assets/sounds/28.mp3"},
            {id:Assets.sounds.spoken_29, src:"assets/sounds/29.mp3"},
            {id:Assets.sounds.spoken_30, src:"assets/sounds/30.mp3"},
            {id:Assets.sounds.spoken_31, src:"assets/sounds/31.mp3"},
            {id:Assets.sounds.spoken_32, src:"assets/sounds/32.mp3"},
            {id:Assets.sounds.spoken_33, src:"assets/sounds/33.mp3"},
            {id:Assets.sounds.spoken_34, src:"assets/sounds/34.mp3"},
            {id:Assets.sounds.spoken_35, src:"assets/sounds/35.mp3"},
            {id:Assets.sounds.spoken_36, src:"assets/sounds/36.mp3"},

            {id:Assets.sounds.spoken_welcome, src:"assets/sounds/welcome.mp3"},
            {id:Assets.sounds.spoken_youWin_1, src:"assets/sounds/youWin_1.mp3"},
            {id:Assets.sounds.spoken_youWin_2, src:"assets/sounds/youWin_2.mp3"},
            {id:Assets.sounds.spoken_youWin_3, src:"assets/sounds/youWin_3.mp3"},
            {id:Assets.sounds.spoken_youWin_4, src:"assets/sounds/youWin_4.mp3"},
            {id:Assets.sounds.spoken_noWin_1, src:"assets/sounds/noWin_1.mp3"},
            {id:Assets.sounds.spoken_noWin_2, src:"assets/sounds/noWin_2.mp3"},
            {id:Assets.sounds.spoken_placeBet, src:"assets/sounds/placeBet.mp3"},
            {id:Assets.sounds.spoken_placeBet_2, src:"assets/sounds/placeBet_2.mp3"},
            {id:Assets.sounds.spoken_doSpin_1, src:"assets/sounds/doSpin_1.mp3"},
            {id:Assets.sounds.spoken_doSpin_2, src:"assets/sounds/doSpin_2.mp3"},
            {id:Assets.sounds.spoken_doSpin_3, src:"assets/sounds/doSpin_3.mp3"},
            {id:Assets.sounds.spoken_maxReached, src:"assets/sounds/maxReached.mp3"},
            {id:Assets.sounds.spoken_clearedBets, src:"assets/sounds/clearBets.mp3"},
            //{id:Assets.sounds.spoken_last5, src:"assets/sounds/last5Drawn.mp3"},
            {id:Assets.sounds.spoken_winningNumber_1, src:"assets/sounds/winningNumber_1.mp3"},
            {id:Assets.sounds.spoken_winningNumber_2, src:"assets/sounds/winningNumber_2.mp3"}

            //locale
            /*{id:Assets.imgTexts.drawTxtImg,src:localePath + "/drawTxt.png"},
             */

            //images big
            //{id:Assets.images.mainBackground,src:"assets/images/mainBackground.jpg"},//1

            //sounds
        ];
    };

    RouletteAssets.loadFontManifest = function(){
        return [
            //"css/BebasNeue-Regular.ttf",
            "css/RobotoCondensed-Bold.ttf",
            "css/HurmeGeometricSans3.otf"
        ];
    };


    RouletteAssets.atlasImage = {};
    //var aI = RouletteAssets.atlasImage = {};

    //aI[Assets.atlasImage.ss] = "ss";

    RouletteAssets.texts = {};
    var t = RouletteAssets.texts["en"] = {};
    t[Assets.texts.stake] = "BET";
    t[Assets.texts.win] = "WIN";
    t[Assets.texts.crd] = "Cash:";
    t[Assets.texts.bet] = "Bet:";

    t[Assets.texts.sessionDoesNotExist] = "Session does not exist";
    t[Assets.texts.casinoServiceUnreachable] = "CasinoServiceUnreachable";


    t = RouletteAssets.texts["sr"] = {};

    RouletteAssets.loadVideo = function(show,progress){
        show();

    };

    alteastream.RouletteAssets = RouletteAssets;
})();