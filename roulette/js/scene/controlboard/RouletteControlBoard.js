// namespace:
this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var RouletteControlBoard = function(params) {
        this.AbstractControlBoard_constructor();
        this.initialize_RouletteControlBoard(params);
    };
    var p = createjs.extend(RouletteControlBoard, alteastream.AbstractControlBoard);
    var _this = null;

// static properties:
// events:
// public properties:
// private properties:
    p._color_highlighted_text = "";
    p._color_disabled_text = "";
    p._color_enabled_text = "";

// constructor:

    p.initialize_RouletteControlBoard = function(params) {
        _this = this;

        this._color_highlighted_text = "green";
        this._color_disabled_text = "#0c1828";
        this._color_enabled_text = "white";

        this._setTopButtons();
        this._setValuesPanel();
        this._setChipsButtonsPanel(params.chipValues);
        this._setPlayButtonsPanel();
    };

// static methods:
    RouletteControlBoard.getInstance = function(){
        return _this;
    };
// public methods:

    p.setBetAmount = function(amount) {
        //this._textStakeAmount.text = amount;
        this._textStakeAmount.setText(amount);
    };

// private methods:
    p._setChipsButtonsPanel = function(chipValues){
        console.log("COMM:: chipsButtonsPanel");
        var assets = alteastream.Assets;
        this.chipsPanel = new createjs.Container();
        this.addChild(this.chipsPanel);

        var numOfButtons = chipValues.length;
        var buttonWidth = assets.getImage(assets.images.chipBtn0).image.width/3;
        var spacingX = Math.ceil(buttonWidth+(buttonWidth/10));
        var middlePoint = alteastream.AbstractScene.GAME_WIDTH/2-spacingX/2;

        var startX = Math.ceil(middlePoint+spacingX/2+(((numOfButtons/2)-1)*spacingX));// from right to left from highest value
        var startY = 600;

        //var positions = [];
        for(var i = 0;i<chipValues.length;i++){
            var valueTxt = new createjs.Text(String(chipValues[i]),"20px RobotoCondensed","#ffffff").set({textAlign:"center",textBaseline:"middle",mouseEnabled:false});
            var chip = new alteastream.ImageButton(assets.getImageURI(assets.images["chipBtn"+i]),3,valueTxt);
            chip.name = chipValues[i];
            this.chipsPanel.addChild(chip);

            var posX = Math.ceil(startX-(i*spacingX));
            chip.x = Math.ceil(posX+buttonWidth/2);
            chip.y = startY+5;
            chip.setClickHandler(this.onChipButton);
            //positions.push(posX);
        }

        this.mainBg = alteastream.Assets.getImage(alteastream.Assets.images.mainBackgroundWithHole);
        this.addChildAt(this.mainBg,0);
        //this.mainBg.y = 535;
        //this.mainBg.alpha = 0.4;
        this.mainBg.mouseEnabled = false;

        //this.selectedChip = "1";//when all
        //this.selectedChip = String(response.values[response.values.length-1]);//when all
       // console.log("this.selectedChip "+this.selectedChip);
    };

    //chipsPanel === this now
    p.onChipButton = function(e){
        var chipName = e.currentTarget.name;
        if(chipName === undefined || _this.selectedChip === chipName)
            return;
        _this.chipsPanel.getChildByName(_this.selectedChip).setDisabled(false);
        _this.highlightChip(_this.selectedChip,false);
        _this.selectedChip = chipName;
        //chipName.setHighlighted(true);
        _this.highlightChip(chipName,true);

        alteastream.BetTable.getInstance().setChipValue(e.currentTarget);
    };

    p._setTopButtons = function(){
        var spacingTop = 5;
        var btnQuit = this.btnQuit = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnQuitSS),3);
        btnQuit.x = 1865;
        btnQuit.y = spacingTop;
        this.addChild(btnQuit);

        var width = this.btnQuit.width;
        var spacing = (width/2)-10;

        //var btnSound = this.btnSound = alteastream.Assets.getImage(alteastream.Assets.images.soundOn);
        var btnSound = this.btnSound = alteastream.Assets.getImage(alteastream.Assets.images.soundOff);
        this.soundToggled = true;
        btnSound.x = this.btnQuit.x - width - spacing;
        btnSound.y = spacingTop;
        this.addChild(btnSound);

        var btnInfo = this.btnInfo = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnInfoSS),3);
        btnInfo.x = this.btnSound.x - width - spacing;
        btnInfo.y = spacingTop;
        btnInfo.highlightedTextColor  = "#fff";
        this.addChild(btnInfo);

        var scene = alteastream.Roulette.getInstance();
        var btnFs = scene.btnFs = alteastream.Assets.getImage(alteastream.Assets.images.btnFullScreen_On);
        btnFs.x = this.btnInfo.x - width - spacing;
        btnFs.y = this.btnInfo.y;
        var method;
        if(is.safari() === true || is.edge() === true){
            method = "otherBrowsersFullScreenToggle";
        }else{
            method = "normalBrowsersFullScreenToggle";
        }
        btnFs.addEventListener("click", scene[method]);
        this.addChild(btnFs);
    };

    p._setPlayButtonsPanel = function(){
        console.log("_setPlayButtonsPanel:::::::::::::::::::::");

        var assets = alteastream.Assets;
        var buttonsPanel = this.buttonsPanel = new createjs.Container();

        var fontSize = 16;
        var fontSmall = fontSize+"px RobotoCondensed";
        var white = "#ffffff";

        //var text = new alteastream.BitmapText("back",fontSmall, white,{x:0,y:0,textAlign:"center",textBaseline:"middle"});
        var btnStepBack = this.btnStepBack = new alteastream.ImageButton(assets.getImageURI(assets.images.btnStepBackSS),3/*,text*/);
        buttonsPanel.addChild(btnStepBack);
        btnStepBack.setClickHandler(this.onStepBackBtn);

        //text = new alteastream.BitmapText("clear",fontSmall, white,{x:0,y:0,textAlign:"center",textBaseline:"middle"});
        var btnClearAll = this.btnClearAll = new alteastream.ImageButton(assets.getImageURI(assets.images.btnClearSS),3/*,text*/);
        buttonsPanel.addChild(btnClearAll);
        btnClearAll.x= 723;
        btnClearAll.setClickHandler(this.onClearAllBtn);

        var textTemp = new alteastream.BitmapText("TEMP",fontSmall, white,{x:0,y:0,textAlign:"center",textBaseline:"middle"});
        var btnSpin = this.btnSpin = new alteastream.ImageButton(assets.getImageURI(assets.images["chipBtn2"]),3,textTemp);
        //buttonsPanel.addChild(btnSpin);
        btnSpin.x= 905;
        btnSpin.y= 25;
        btnSpin.setClickHandler(this.onSpin);

        this.addChild(buttonsPanel);
        buttonsPanel.x = 570;
        buttonsPanel.y = 580;

        this.enableSpin(false);
    };

    /////////////////////////////////////ABS ROULETTE//////////////////////////////////

    p._setValuesPanel = function(){
        console.log("_setValuesPanel:::::::::::::::::::::");
        var valuesPanel = this.valuesPanel = new createjs.Container();
        this.addChild(valuesPanel);
        this.valuesPanel.y = 1005;
        var roboto14 ="14px RobotoCondensed";
        var robotoBold24 ="bold 24px RobotoCondensed";
        var colorGreen ="#4bdc0a";
        var colorGray ="#b8b8b8";
        var colorWhite ="#ffffff";
        var bottomLine1 = 25;
        var bottomLine2 = 50;

        this.statusText = new createjs.Text("GETTING STATUS..",roboto14,colorWhite).set({x:960,y:554,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this.statusText);

        var _textFinalsLabel = new createjs.Text("FINALS",roboto14,colorWhite).set({x:1715,y:554,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(_textFinalsLabel);

        this._textCrd = new createjs.Text("BALANCE",roboto14,colorGray).set({x:540,y:bottomLine1,textAlign:"right",textBaseline:"middle",mouseEnabled:false});//975
        this._textCurrencyValue = new createjs.Text("",roboto14,colorGray).set({x:this._textCrd.x+5,y:bottomLine1,textAlign:"left",textBaseline:"middle",mouseEnabled:false});
        this._textCrdAmount = new createjs.Text("0.00",robotoBold24,colorGreen).set({x:this._textCrd.x-10,y:bottomLine2,textAlign:"center",textBaseline:"middle",mouseEnabled:false});

        this._textMinStakeLabel = new createjs.Text("MIN. STAKE",roboto14,colorGray).set({x:850,y:bottomLine1,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1170
        this.minBetAmountText = new createjs.Text("0.00",robotoBold24,colorWhite).set({x:850,y:bottomLine2,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1170

        this._textCurrentStakeLabel = new createjs.Text("CURRENT STAKE",roboto14,colorGray).set({x:960,y:bottomLine1,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1280
        this._textStakeAmount = new createjs.Text("0.00",robotoBold24,colorWhite).set({x:960,y:bottomLine2,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1280

        this._textMaxStakeLabel = new createjs.Text("MAX. STAKE",roboto14,colorGray).set({x:1070,y:bottomLine1,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1400
        this.maxBetAmountText = new createjs.Text("0.00",robotoBold24,colorWhite).set({x:1070,y:bottomLine2,textAlign:"center",textBaseline:"middle",mouseEnabled:false});//1400

        valuesPanel.addChild(
            this._textCurrencyValue,
            this._textCrd,
            this._textCrdAmount,
            this._textCurrentStakeLabel,
            this._textStakeAmount,
            this._textWinAmount,
            this._textMinStakeLabel,
            this.minBetAmountText,
            this._textMaxStakeLabel,
            this.maxBetAmountText
        );

        this._streetBetsLabel = new createjs.Text("STREET BETS",roboto14,colorGray).set({x:1300,y:1030,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this._streetBetsLabel);
        this._streetBetsAvail = new createjs.Text("AVAILABLE",roboto14,colorWhite).set({x:1300,y:1055,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this._streetBetsAvail);

        this._sixLineBetsLabel = new createjs.Text("SIX LINE BETS",roboto14,colorGray).set({x:1400,y:1030,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this._sixLineBetsLabel);
        this._sixLineBetsAvail = new createjs.Text("AVAILABLE",roboto14,colorWhite).set({x:1400,y:1055,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.addChild(this._sixLineBetsAvail);
    };

    p.setLastPlayedNumbers = function(nums){
        console.log("COMM:: playedNumbersPanel");
        var playedNumbersPanel = this.playedNumbersPanel = new createjs.Container();
        this.addChild(playedNumbersPanel);
        playedNumbersPanel.x = 1572;
        playedNumbersPanel.y = 1055;

        var _textPlayedNumbers = new createjs.Text("LATEST NUMBERS","14px RobotoCondensed","#b8b8b8").set({x:150,y:-25,textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        this.numsArray = [];
        var startNumsArray = nums;
        playedNumbersPanel.addChild(_textPlayedNumbers);

        for (var i = 0,j=startNumsArray.length;i<j; i++) {
            var color = alteastream.BetTable.getInstance().getBetField("num_"+startNumsArray[i]).color;
            if(color === "black")//temp
                color = "#ffffff";
            var number = new createjs.Text(String(startNumsArray[i]),"20px RobotoCondensed",color).set({textAlign:"center",textBaseline:"middle"});
            number.x = 32*i;
            number.mouseEnabled = false;
            playedNumbersPanel.addChild(number);
            this.numsArray.push(number);
        }
        this.numsArray[0].font = "28px RobotoCondensed";
        playedNumbersPanel.mouseEnabled = false;
        playedNumbersPanel.mouseChildren = false;
    };

    p.updateLastPlayedNumbers = function(num){
        console.log("COMM:: updateLastPlayed");
        var numsArray = this.numsArray;
        for (var i = numsArray.length-1,j = 0; i > j; i--) {
            numsArray[i].text = numsArray[i-1].text;
            numsArray[i].color = numsArray[i-1].color;
            numsArray[i].x = 34*i;
        }
        var color = alteastream.BetTable.getInstance().getBetField("num_"+num).color;
        if(color === "black")//temp
            color = "#ffffff";
        numsArray[0].font = "bold 28px Roboto Condensed";
        numsArray[0].color = color;
        numsArray[0].text = String(num);
    };

    p.updateStatus = function(status){
        console.log("status:: "+status);
        this.statusText.text = status;
    };

    p.updateStreetBetLabels = function(bool){
        this._streetBetsAvail.color = bool===true?"#4bdc0a":"#ff0000";
    };

    p.updateSixLineBetLabels = function(bool){
        this._sixLineBetsAvail.color = bool===true?"#4bdc0a":"#ff0000";
    };

    p.setCurrency = function(c){
        this._textCurrencyValue.text = "("+c+")";
    };

    p.setStakesInfo = function(min,max){
        this.minBetAmountText.text = min.toFixed(2);
        this.maxBetAmountText.text = max.toFixed(2);
    };

    p.onClearAllBtn = function(){
        alteastream.BetTable.getInstance().removeAllBets();
        var assets = alteastream.Assets;
        assets.playSound(assets.sounds.buttonClick);
        assets.playSound(assets.sounds.spoken_clearedBets);
        assets.playSoundChain(assets.sounds.spoken_clearedBets,1,function(){
            assets.playSound(assets.sounds.spoken_placeBet_2);
        });
    };

    p.onStepBackBtn = function(){
        alteastream.BetTable.getInstance().onStepBack();
        alteastream.Assets.playSound(alteastream.Assets.sounds.buttonClick);
    };

    p.onSpin = function(){
        _this.resetUI(false);
        _this.showUI(false);
    };

    p.onSpinWin = function(){
        this.resetUI(true);
    };

    p.onSpinLost = function(gameResult){
        this.resetUI(true);
        this.showUI(true);
        console.log("winningNumber.text here:::: "+gameResult.winNumber);
        /*this.winningNumber.text = String(gameResult.winNumber);
        this.winningNumberLabel.color = "#ffffff";
        this.winningNumber.color = "#ffffff";
        this.currentWinAmount.color = "#ffffff";*/
        this.win = false;
    };

    p.resetUI = function(bool){
        console.log("resetUI:::: "+bool);
        //this.enableSpin(bool);
        //this.enableQuit(bool);

        /*this.enableStepBack(bool);
        this.resetBallFrame();
        */
    };

    p.showUI = function(bool){
        console.log("showUI:::: "+bool);
        //this.background.visible = bool;
        //this.chipsPanel.visible = bool;
        //this.infoButtonsPanel.visible = bool;
        //this.buttonsPanel.visible = bool;
    };

    p.setDefaultChip = function(defaultCoinName){
        this.selectedChip = defaultCoinName;
        this.highlightChip(defaultCoinName,true);
        console.log("setDefaultChip "+defaultCoinName);
    };

    p.highlightChip = function(chipName,bool){
        var chip = this.chipsPanel.getChildByName(chipName);
        chip.setHighlighted(bool);
        //chip.scaleX = chip.scaleY =bool ===true?1.25:1;
    };

    p.disableChip = function(chip,bool){
        this.chipsPanel.getChildByName(chip).setDisabled(bool);
    };

    p.disableAllChips = function(bool){
        var children = this.chipsPanel.children;
        for(var i = 0,j=children.length;i<j;i++){
            this.chipsPanel.getChildByName(children[i].name).setDisabled(bool);
        }
        if(bool ===true){
            this.enableStepBack(false);
        }else{
            this.enableStepBack(alteastream.BetTable.getInstance().chipsOnTable>0);
        }
    };

    p.onBetAction = function(currentBetAmount,bool){
        this.setBetAmountText(currentBetAmount);
        this.enableStepBack(bool);
        alteastream.Assets.playSound(alteastream.Assets.sounds.chipDown);
    };

    p.setBetAmountText = function(betAmount){
        var color = betAmount<alteastream.BetTable.getInstance().minStake?"#bd0000":"#2dff00";
        var bet = parseFloat(String(betAmount));
        this._textStakeAmount.text = bet.toFixed(2);
        this._textStakeAmount.color = color;
    };

    p.enableStepBack = function(bool){
        this.btnStepBack.setDisabled(!bool);
        this.btnClearAll.setDisabled(!bool);
    };

    p.enableSpin = function(bool){
        this.btnSpin.setDisabled(!bool);
    };

    p.enableQuit = function(bool){
        this.btnQuit.setDisabled(!bool);
    };

    p.onSpin = function(bool){
        //this.enableSpin(!bool);
        this.enableQuit(!bool);
        this.enableStepBack(!bool);
        //this.resetBallFrame();
        this.showUI(!bool);
        /*if(bool)
            this.storeButtonStates();
        else{
            this.restoreButtonStates();
        }*/
    };

    p.soundBtnHandler = function(){
        var assets = alteastream.Assets;
        this.soundToggled = !this.soundToggled;
        this.btnSound.image = this.soundToggled === true? assets.getImage(assets.images.soundOff).image: assets.getImage(assets.images.soundOn).image;
        //assets.setMute(this.soundToggled);

        /* // local ver
        var action = this.soundToggled===true?"pause":"play";
        window.parent.manageBgMusic(action);
        */ // local ver
    };

    p.restoreMachineSoundState = function (){
        console.log("restoreMachineSoundState Here::::: ");
        /*var soundMachineIsToggled = window.localStorage.getItem("soundMachineIsToggled");
        if(soundMachineIsToggled !== null) {
            this.soundMachineToggled = soundMachineIsToggled === "false";
            this.soundMachineBtnHandler();
        }*/
    };

    p.restoreMusicSoundState = function (){
        console.log("restoreMusicSoundState Here::::: ");
        /*var gameplaySoundIsToggled = window.localStorage.getItem("gameplaySoundIsToggled");
        if(gameplaySoundIsToggled !== null) {
            this.soundToggled = gameplaySoundIsToggled === "false";
            this.soundBtnHandler();
        }*/
    };


    alteastream.RouletteControlBoard = createjs.promote(RouletteControlBoard,"AbstractControlBoard");
})();