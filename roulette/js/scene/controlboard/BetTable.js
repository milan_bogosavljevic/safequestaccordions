
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var BetTable = function(game,betmatrix,chipValues) {
        this.ABSBetTable_constructor(game,betmatrix,chipValues);
        this.initialize_BetTable(chipValues);
    };

    var p = createjs.extend(BetTable, alteastream.ABSBetTable);
    var _this = null;

// static properties:
// events:
// public properties:
    p.BETCHIPS = [];
    p.selectedChipValue = 0;
    p.chipsOnTable = 0;
    p._betButons = [];
    p._chipButons = [];
    p.runAnimations = false;
    p._animationsCounter = 0;
    p._noCliksAllowed = false;

// private properties:
// constructor:
// static methods:
    BetTable.getInstance = function(){
        return _this;
    };

// public methods:
    p.initialize_BetTable = function(chipValues) {
        _this = this;
        console.log("initialize_BetTable ");
        var startingY = 670;
        this.BETCHIPS = chipValues;
        for(var i = 0;i<this.BETCHIPS.length;i++){//all chips to be cloned
            var chip =  new alteastream.ABSChip(this.BETCHIPS[i],i);
            chip.name = this.BETCHIPS[i];
            this.addChild(chip);
            chip.visible = false;
        }
        //var hoverAction = alteastream.AbstractScene.HAS_TOUCH === true? null: this.highlightBet;

        var assets = alteastream.Assets;

        var fieldHeight = assets.getImage(assets.images.fieldSmall).image.height;
        var splitFieldDim = assets.getImage(assets.images.fieldSplit).image.height;
        var smallFieldWidth = assets.getImage(assets.images.fieldSmall).image.width;
        var mediumFieldWidth = assets.getImage(assets.images.fieldMedium).image.width;
        var largeFieldWidth = assets.getImage(assets.images.fieldLarge).image.width;
        var zeroFieldWidth = assets.getImage(assets.images.fieldZero).image.width;
        var fieldMiniHeight = assets.getImage(assets.images.fieldMini).image.height;
        var fieldMiniWidth = assets.getImage(assets.images.fieldMini).image.width;
        var startingX = (alteastream.AbstractScene.GAME_WIDTH/2)-(6*smallFieldWidth);

        // betField border
        var betFieldBorder = new createjs.Shape();
        this.addChild(betFieldBorder);

        var _borderThickness = 2;
        var betFieldBorderGfx = betFieldBorder.graphics;
        betFieldBorderGfx.setStrokeStyle(_borderThickness).beginStroke("#ffffff");
        betFieldBorderGfx.moveTo(startingX+smallFieldWidth*6, startingY)
        .lineTo((startingX+smallFieldWidth*6)+7*smallFieldWidth, startingY)
        .lineTo((startingX+smallFieldWidth*6)+7*smallFieldWidth, startingY+3*fieldHeight)
        .lineTo((startingX+smallFieldWidth*6)+6*smallFieldWidth, startingY+3*fieldHeight)
        .lineTo((startingX+smallFieldWidth*6)+6*smallFieldWidth, startingY+5*fieldHeight)
        .lineTo(startingX+smallFieldWidth*6, startingY+5*fieldHeight)
        .endStroke();
        betFieldBorder.cache(startingX+smallFieldWidth*6,startingY-_borderThickness,(startingX+smallFieldWidth*6)+7*smallFieldWidth,startingY+5*fieldHeight);
        betFieldBorder.mouseEnabled = false;

        var betFieldBorderLeft = new createjs.Bitmap(betFieldBorder.cacheCanvas);
        this.addChild(betFieldBorderLeft);
        betFieldBorderLeft.x=startingX+smallFieldWidth*6;
        betFieldBorderLeft.y=startingY-_borderThickness;
        betFieldBorderLeft.scaleX*=-1;
        betFieldBorderLeft.mouseEnabled = false;
        // betField border

        // raceTrack borders
        var raceTrackBorderOuter = new createjs.Shape();
        this.addChild(raceTrackBorderOuter);
        raceTrackBorderOuter.x=1715;
        raceTrackBorderOuter.y=830;
        raceTrackBorderOuter.graphics.setStrokeStyle(3).beginStroke("#ffffff").drawCircle(0,0,165);
        raceTrackBorderOuter.cache(-168,-168,335,335);

        var raceTrackBorderInner = new createjs.Shape();
        this.addChild(raceTrackBorderInner);
        raceTrackBorderInner.x=1715;
        raceTrackBorderInner.y=830;
        raceTrackBorderInner.graphics.setStrokeStyle(3).beginStroke("#ffffff").drawCircle(0,0,130);
        raceTrackBorderInner.cache(-132,-132,264,264);
        // raceTrack borders


        //inner and splits
        this._setInnerBets([3,6,9,12,15,18,21,24,27,30,33,36],startingX,startingY,"fieldSmall");
        this._setInnerBets([2,5,8,11,14,17,20,23,26,29,32,35],startingX,startingY+fieldHeight/*-1*/,"fieldSmall");
        this._setInnerBets([1,4,7,10,13,16,19,22,25,28,31,34],startingX,startingY+fieldHeight*2/*-2*/,"fieldSmall");
        this._setInnerBets([0],startingX-zeroFieldWidth,startingY,"fieldZero");

        var splitsHY = startingY+fieldHeight/2-splitFieldDim/2;
        var splitsHX = startingX+smallFieldWidth-splitFieldDim/2;
        this._setSplitBets([3,6,9,12,15,18,21,24,27,30,33,36],splitsHX,splitsHY,"split2H_","fieldSplit");
        this._setSplitBets([2,5,8,11,14,17,20,23,26,29,32,35],splitsHX,splitsHY+fieldHeight,"split2H_","fieldSplit");
        this._setSplitBets([1,4,7,10,13,16,19,22,25,28,31,34],splitsHX,splitsHY+fieldHeight*2,"split2H_","fieldSplit");

        var splitsVX = startingX+smallFieldWidth/2-splitFieldDim/2;
        var splitsVY = startingY+fieldHeight-splitFieldDim/2;
        this._setSplitBets([2,5,8,11,14,17,20,23,26,29,32,35],splitsVX,splitsVY,"split2V_","fieldSplit");
        this._setSplitBets([1,4,7,10,13,16,19,22,25,28,31,34],splitsVX,splitsVY+fieldHeight,"split2V_","fieldSplit");

        var splits4X = startingX+smallFieldWidth-splitFieldDim/2;
        var splits4Y = startingY+fieldHeight-splitFieldDim/2;
        this._setSplitBets([2,5,8,11,14,17,20,23,26,29,32],splits4X,splits4Y,"split4_","fieldSplit");
        this._setSplitBets([1,4,7,10,13,16,19,22,25,28,31],splits4X,splits4Y+fieldHeight,"split4_","fieldSplit");

        //zero splits
        var splitsZeroX = startingX-splitFieldDim/2;
        var splitsZeroY = startingY+3*fieldHeight+fieldHeight/2-splitFieldDim/2;
        var splitsZeroY2 = startingY+6*fieldHeight-splitFieldDim/2;
        for(var j =1;j<6;j++){
            var yPos = j<4?splitsZeroY:splitsZeroY2;
            this._setSplitBets([j],splitsZeroX,yPos-(j*fieldHeight),"split0_","fieldSplit");
        }

        //streets and 6-lines
        var streetX = startingX+smallFieldWidth/2-splitFieldDim/2;
        var streetY = startingY-splitFieldDim/2;
        var streetAndLinePositions = [3,6,9,12,15,18,21,24,27,30,33,36];
        for(var s=0;s<streetAndLinePositions.length;s++){
            var betBtn = new alteastream.ABSBetField("street_"+s,null,"fieldSplit");
            this.addChild(betBtn);
            betBtn.x=streetX+(s*(betBtn.getWidth()*4));// fieldSmall is 4 times smaller than regular field
            betBtn.y=streetY;
            this._setAsButton(betBtn,this.placeBet,this.highlightBet);
            this._betButons.push(betBtn);
        }

        var lineX = startingX+smallFieldWidth-splitFieldDim/2;
        var lineY = startingY-splitFieldDim/2;
        for(var l=0;l<streetAndLinePositions.length-1;l++){
            var betBtnLine = new alteastream.ABSBetField("line_"+l,null,"fieldSplit");
            this.addChild(betBtnLine);
            betBtnLine.x=lineX+(l*(betBtnLine.getWidth()*4));// fieldSmall is 4 times smaller than regular field
            betBtnLine.y=lineY;
            this._setAsButton(betBtnLine,this.placeBet,this.highlightBet);
            this._betButons.push(betBtnLine);
        }

        //outer and neighbours
        var cPosX = startingX+(12*smallFieldWidth);
        this._setOuterBets("c1",cPosX,startingY,"fieldSmall");// decal here?
        this._setOuterBets("c2",cPosX,startingY+fieldHeight,"fieldSmall");
        this._setOuterBets("c3",cPosX,startingY+fieldHeight*2,"fieldSmall");

        this._setOuterBets("1st 12",startingX,startingY+fieldHeight*3,"fieldLarge");
        this._setOuterBets("2nd 12",startingX+largeFieldWidth,startingY+fieldHeight*3,"fieldLarge");
        this._setOuterBets("3rd 12",startingX+largeFieldWidth*2,startingY+fieldHeight*3,"fieldLarge");

        this._setOuterBets("1 - 18",startingX,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("EVEN",startingX+mediumFieldWidth,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("red",startingX+mediumFieldWidth*2,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("black",startingX+mediumFieldWidth*3,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("ODD",startingX+mediumFieldWidth*4,startingY+fieldHeight*4,"fieldMedium");
        this._setOuterBets("19 - 36",startingX+mediumFieldWidth*5,startingY+fieldHeight*4,"fieldMedium");

        var xPosSpec = 1636;
        var yPosSpec = 767;
        this._setSpecialBets("jeuZ",xPosSpec,yPosSpec,"fieldSpec");
        this._setSpecialBets("voisins",xPosSpec,yPosSpec+fieldMiniHeight,"fieldSpec");
        this._setSpecialBets("orphelins",xPosSpec,yPosSpec+fieldMiniHeight*2,"fieldSpec");
        this._setSpecialBets("tiers",xPosSpec,yPosSpec+fieldMiniHeight*3,"fieldSpec");

        //temp::
        var wheelOrder = [0,32,15,19,4,21,2,25,17,34,6,27,13,36,11,30,8,23,10,5,24,16,33,1,20,14,31,9,22,18,29,7,28,12,35,3,26];
        for(var n=0;n<wheelOrder.length;n++){
            var num = wheelOrder[n];
            this._setNeighbourBets("neighbours_"+num,1715,830,"fieldTrack",num,n);
        }

        for(var f=0;f<10;f++){
            var brk = f<5?f:f-5;
            var xPF = 1555+(brk*fieldMiniWidth);
            var yPF = f<5?570:570+fieldMiniHeight;
            this._setFinalsBets("finals_"+f,xPF,yPF,"fieldMini",f);
        }

        this.toDefaultCoin();
        this.setSpecialBetProps();
        //this._setLastPlayedPanel();
    };

    p._setInnerBets = function(row,posX,posY,fieldSmall){
        for(var i =0,j =this.BETMATRIX.length;i<j;i++) {
            var matrix =this.BETMATRIX[i];
            for (var k = 0, l = row.length; k < l; k++) {
                if (matrix.name === "num_" +row[k]) {
                    var betBtn = new alteastream.ABSBetField(matrix.name,row[k],fieldSmall);
                    this.addChild(betBtn);
                    betBtn.x=posX+(k*betBtn.getWidth())/*-(k+1)*/;
                    betBtn.y=posY;
                    this._setAsButton(betBtn,this.placeBet,this.highlightBet);
                    this._betButons.push(betBtn);
                    if(matrix.name !== "num_0")
                        betBtn.setColor(matrix.color,fieldSmall);
                }
            }
        }
    };

    p._setOuterBets = function(name,posX,posY,fieldString){
        var field = new alteastream.ABSBetField(name,name,fieldString);
        this.addChild(field);
        field.x=posX;
        field.y=posY;
        this._setAsButton(field,this.placeBet,this.highlightBet);
        this._betButons.push(field);
    };

    p._setSplitBets = function(row,posX,posY,splitType,fieldSmall){
        for(var i =0,j =this.BETMATRIX.length;i<j;i++) {
            var matrix =this.BETMATRIX[i];
            for (var k = 0, l = row.length; k < l; k++) {
                if (matrix.name === splitType+row[k]) {
                    var betBtn = new alteastream.ABSBetField(matrix.name,null,fieldSmall);
                    this.addChild(betBtn);
                    betBtn.x=posX+(k*(betBtn.getWidth()*4));// fieldSmall is 4 times smaller than regular field
                    betBtn.y=posY;
                    this._setAsButton(betBtn,this.placeBet,this.highlightBet);
                    this._betButons.push(betBtn);
                }
            }
        }
    };

    p._setSpecialBets = function(name,posX,posY,fieldString){
        var field = new alteastream.ABSBetField(name,name,fieldString);
        this.addChild(field);
        field.x=posX;
        field.y=posY;
        field.setFont(20,"RobotoCondensed");//bitmap font
        //this._setAsButton(field,this.placeBet,this.highlightBet);
        this._setAsButton(field,this.placeBet,this.highlightSpecials);
        this._betButons.push(field);
    };

    p._setNeighbourBets = function(name,posX,posY,fieldString,num,n){
        var field = new alteastream.ABSBetField(name,num,fieldString);
        this.addChild(field);
        field.x=posX;
        field.y=posY;
        field.regX = field.getWidth() * 0.5;
        field.regY = 163;//radius
        field.rotation = n*(360/37);
        field.setFont(18,"RobotoCondensed");//bitmap font
        this._setAsButton(field,this.placeBet,this.highlightNeighbours);
        this._betButons.push(field);
        var color = num>0?this.BETMATRIX[num].color:"green";
        field.setColor(color,fieldString);
    };

    p._setFinalsBets = function(name,posX,posY,fieldString,ind){
        var field = new alteastream.ABSBetField(name,ind,fieldString);
        this.addChild(field);
        field.x=posX;
        field.y=posY;
        field.setFont(20,"RobotoCondensed");//bitmap font
        this._setAsButton(field,this.placeBet,this.highlightFinals);
        this._betButons.push(field);
    };

    p.setSpecialBetProps = function(){
        var _setSpecial = function(type,length){
            for(var i=0;i<length;i++){
                _this[type+i+"Chips"] = _this.getBetField(type+i).special.length;
            }
        }

        _setSpecial("street_",this.STREET_POSITIONS);
        _setSpecial("line_",this.LINES_POSITIONS);
        _setSpecial("neighbours_",this.TOTAL_NUMBERS);
        _setSpecial("finals_",this.FINALS_NUMBERS);

        this.jeuzChips = this.getBetField("jeuZ").special.length;
        this.tiersChips = this.getBetField("tiers").special.length;
        this.orphelinsChips = this.getBetField("orphelins").special.length;
        this.voisinsChips = this.getBetField("voisins").special.length;
    };

    p.setChipValue = function(chip){
        var value = parseInt(chip.name);
        _this.selectedChipValue = value;
        _this.selectedBetValue = value / 100;
        console.log("selected chip "+ _this.selectedChipValue+", bet value "+ _this.selectedBetValue);
        _this.setAvailableBets();
        //_this.setHoverImage(_this.selectedChipValue);

        //alteastream.Assets.playSound("buttonClick");
    };

    p.highlightNeighbours = function(betFieldMesh,bool){
        _this.highlightRaceTrack(betFieldMesh,bool);
         _this.highlightBet(betFieldMesh,bool);
    };

    p.highlightSpecials = function(betFieldMesh,bool){
        _this.highlightRaceTrack(betFieldMesh,bool);
        _this.highlightBet(betFieldMesh,bool);
    };

    p.highlightFinals = function(betFieldMesh,bool){
        _this.highlightRaceTrack(betFieldMesh,bool);
        var numbers = _this.getBetField(betFieldMesh.name).numbers;
        var finalN = numbers[0];
        var name = "finals_"+String(finalN);
        var mesh = _this.getChildByName(name);
        mesh.setHighlighted(bool);
        _this.highlightBet(betFieldMesh,bool);
    };

    p.highlightBet = function(betFieldMesh,bool){
        //just highlight
        /*if(_this.getBetField(betFieldMesh.name).bet>0.00){
            console.log("bet: "+  _this.getBetField(betFieldMesh.name).bet);
        }*/
        var numbers = _this.getBetField(betFieldMesh.name).numbers;
        for(var i = 0,j = numbers.length;i<j;i++){
            var name = "num_"+String(numbers[i]);
            var mesh = _this.getChildByName(name);
            mesh.setHighlighted(bool);
        }
        var prefix = String(betFieldMesh.name.substr(0,4));
        if(prefix !=="spli" && prefix!=="num_" && prefix!=="stre" && prefix!=="line")
            _this.getChildByName(String(betFieldMesh.name)).setHighlighted(bool);

        if(_this.getBetField(betFieldMesh.name).bet>0.00){
        //if(_this.getChildByName(betFieldMesh.name).chipsArr.length>1){
            _this._revealChipStack(betFieldMesh,bool);
            _this._showToolTip(betFieldMesh,bool);
        }
    };

    p.highlightRaceTrack = function(betFieldMesh,bool){
        var numbers = _this.getBetField(betFieldMesh.name).numbers;
        for(var i = 0,j = numbers.length;i<j;i++){
            var name = "neighbours_"+String(numbers[i]);
            var mesh = _this.getChildByName(name);
            mesh.setHighlighted(bool);
        }
    };

    p._revealChipStack = function(betFieldMesh,bool){
        var betField = _this.getChildByName(betFieldMesh.name);
        var betFieldHeight = betFieldMesh.normalState.image.height;
        var chipsArrLength = betField.chipsArr.length;
        var spacing = chipsArrLength>1?28:0;
        var orIndex = this.getChildIndex(betField.chipsArr[betField.chipsArr.length-1]);

        for(var i = 0,j = chipsArrLength;i<j;i++){
            var chip = betField.chipsArr[i];
            var index = bool === true? this.numChildren-1 : orIndex;
            this.setChildIndex(chip,index);
            var revealedY = (chipsArrLength*i)+spacing-(i*spacing)-spacing;
            var defaultY = betFieldMesh.y + (betFieldHeight/2) - (4*i);
            chip.y = bool === true? chip.y+revealedY : defaultY;
            chip.scaleX = chip.scaleY = bool === true? 1+0.15*i : 1;
        }
        //console.log("POS "+betField.chipsArr[chipsArrLength-1].y);
    };

    p._showToolTip = function(betFieldMesh,bool){
        console.log("bet: "+  _this.getBetField(betFieldMesh.name).bet);
        /*if(bool){
            var height = 30;//betFieldMesh.normalState.image.height/2;//40*_this.getChildByName(betFieldMesh.name).chipsArr.length;
            var width = 40;//betFieldMesh.normalState.image.width/2;
            var radius = 20;
            var rect = this.rect = new createjs.Shape();
            //rect.graphics.setStrokeStyle(2).beginStroke("white").beginFill("black").drawRect(0, 0, width, height);
            rect.graphics.setStrokeStyle(2).beginStroke("white").beginFill("black").drawCircle(0, 0, radius);
            this.addChild(rect);
            rect.alpha = 0.6;
            //rect.regX = width;
           // rect.regY = height;
            rect.x = betFieldMesh.x + (betFieldMesh.normalState.image.width/2)+20;
            rect.y = betFieldMesh.y + (betFieldMesh.normalState.image.height/2)+10;
        }else{
            if(this.rect){
                this.removeChild(this.rect);
            }
        }*/

    };

    p.onSpin = function(){
        this.disableBetting();
    };

    p.disableBetting = function(){
        console.log("disableBetting: ");
        this.enableButtons(false);
        this.resetAnimations();
        this.hideSpecials(true);
    };

    p.enableBetting = function(){
        this.enableButtons(true);
        //this.resetAnimations();
        this.setAvailableBets();
        this.setAvailableCoins();
        this.hideSpecials(false);
    };

    p.highlightWin = function (winNumber) {
        console.log("highlightWin:::::::::::::::::::::::::::::");// temp all:::
        //console.log("WIN BETS (exclude splits from highlighting!!!):: "+wonBetsArray);
        //this.winningNumber = this.scene.getChildByName("num_"+String(winNumber));
        //this.winningNumber.position.y = fieldHighY;
        var wonBetsArray = this.wonBetsArray;
        var meshName = String(wonBetsArray[0]);
        var field = this.getChildByName(meshName);
        this.runAnimations = true;
        this._animateWin(field);

        console.log("on collect or some end game end event:: test!!!!");// temp all local:::
        /*setTimeout(function(){
            _this.onCollectedWin();
            alteastream.RouletteControlBoard.getInstance().showUI(true);
            alteastream.RouletteControlBoard.getInstance().setWin(0);
            alteastream.Roulette.getInstance().winningNumberApplet.show(false);
        },5000);*/
    };

    p._animateWin = function(field){
        if(this.runAnimations){
            field.setHighlighted(true);
            var that = this;
            setTimeout(function(){
                field.setHighlighted(false);
                that._animationsCounter = (that._animationsCounter < that.wonBetsArray.length-1)? that._animationsCounter+1: 0;

                var meshName = String(that.wonBetsArray[that._animationsCounter]);
                var next = that.getChildByName(meshName);
                setTimeout(function(){that._animateWin(next)},250);
            },250);
        }
    };

    p.onCollectedWin = function(){
        console.log("onCollectedWin");
        this.resetAnimations();
        //this.enableButtons(true);
        //this.setAvailableCoins();
    };

    p.resetAnimations = function(){
        if(this.runAnimations){
            this.runAnimations = false;
            //this.winningNumber.position.y = fieldStartY;
            for(var i = 0,j = this.wonBetsArray.length;i<j;i++){
                var meshName = String(this.wonBetsArray[i]);
                this.getChildByName(meshName).setHighlighted(false);
            }
        }
    };

    p.toDefaultCoin = function() {
        var _defaultCoinName = String(this.BETCHIPS[this.BETCHIPS.length-1]);
        this.setChipValue(this.getChildByName(_defaultCoinName));
        alteastream.RouletteControlBoard.getInstance().setDefaultChip(_defaultCoinName);
    };

    p.placeBet = function(betFieldMesh){
        console.log("PLACE BET "+betFieldMesh.name);
        if(_this.getBetField(betFieldMesh.name).special){
            var specialBet = _this.getBetField(betFieldMesh.name).special;
            _this._selectedBetValuesArr.push(_this.selectedBetValue);
            _this.pushBet(betFieldMesh.name,_this.selectedBetValue*specialBet.length);
            for(var i=0,j=specialBet.length;i<j;i++){
                var specialBetName = specialBet[i];
                var specialBetMesh = _this.getChildByName(specialBetName);
                var targetBetType = _this.BETMATRIX[_this._lookUpTable[specialBetName]];
                targetBetType.bet = Math.round((targetBetType.bet + _this.selectedBetValue) * 100) / 100;
                _this.recalculateChips(_this.recalculate(targetBetType.bet),specialBetMesh);
                _this.chipsOnTable+=i;
            }
        }
        else{
            _this.pushBet(betFieldMesh.name,_this.selectedBetValue);
            _this._selectedBetValuesArr.push(_this.selectedBetValue);
            var currentBetOnField = _this.getBetField(betFieldMesh.name).bet;
            _this.recalculateChips(_this.recalculate(currentBetOnField),betFieldMesh);
            _this.chipsOnTable++;
        }
        _this.setAvailableBets();
        _this.setAvailableCoins();

        alteastream.RouletteControlBoard.getInstance().onBetAction(_this._currentBetAmount,true);
    };

    p.removeBet = function(){
        var betFieldMesh = this._currentBet[this._currentBet.length-1][0];
        if(_this.getBetField(betFieldMesh).special){
            var specialBet = _this.getBetField(betFieldMesh).special;
            _this.popBet();
            var ind = _this._selectedBetValuesArr.pop();
            for(var i=0,j=specialBet.length;i<j;i++){
                var specialBetName = specialBet[i];
                var specialBetMesh = _this.getChildByName(specialBetName);
                //specialBetMesh.chipsArr.pop();
                //specialBetMesh.chipsArr.splice(i,1);
                var targetBetType = _this.BETMATRIX[_this._lookUpTable[specialBetName]];
                targetBetType.bet = Math.round((targetBetType.bet - ind) * 100) / 100;
                //console.log("targetBetType.bet "+targetBetType.bet);
                //console.log("_this.selectedBetValue "+_this.selectedBetValue);
                _this.recalculateChips(_this.recalculate(targetBetType.bet),specialBetMesh);
                _this.chipsOnTable-=i;
            }
        }
        else{
            var betFieldMesh1 = this.getChildByName(this._currentBet[this._currentBet.length-1][0]);
            this.popBet();
            _this._selectedBetValuesArr.pop();
            var currentBetOnField = _this.getBetField(betFieldMesh1.name).bet;
            _this.recalculateChips(_this.recalculate(currentBetOnField),betFieldMesh1);
            _this.chipsOnTable--;
        }

        _this.setAvailableBets();
        _this.setAvailableCoins();

        alteastream.RouletteControlBoard.getInstance().onBetAction(_this._currentBetAmount,_this.chipsOnTable>0);
        //_this.testArrayLength("removeBet");
    };

    p.removeAllBets = function(){
        for(var i = 0, j = _this._currentBet.length;i<j;i++ ){
            var betFieldMesh = _this.getChildByName(_this._currentBet[i][0]).name;
            if(_this.getBetField(betFieldMesh).special){
                var specialBet = _this.getBetField(betFieldMesh).special;
                for(var k=0,l=specialBet.length;k<l;k++){
                    var specialBetName = specialBet[k];
                    var specialBetMesh = _this.getChildByName(specialBetName);
                    for(var m=0,n=specialBetMesh.chipsArr.length;m<n;m++){
                        this.removeChild(specialBetMesh.chipsArr[m]);
                        //specialBetMesh.chipsArr.splice(m,1);
                    }
                }
            }
            else {
                for(var o = 0, p = _this._currentBet.length;o<p;o++ ){
                    var betFieldMesh1 = _this.getChildByName(_this._currentBet[o][0]);
                    _this.clearChipsPerField(betFieldMesh1);
                }
            }
        }
        _this.clearBets();
        _this.chipsOnTable = 0;

        //_this.toDefaultCoin();
        _this.setAvailableBets();
        _this.setAvailableCoins();
        _this.resetAnimations();

        alteastream.RouletteControlBoard.getInstance().onBetAction(_this._currentBetAmount,false);
        //_this.testArrayLength("removeAllBets");
    };

    p.onStepBack = function(){
        this.removeBet();
        this.resetAnimations();
    };

    p.enableButtons = function(bool){
        if(bool === true){
            if(!this.maxStakeReached){
                this._pickableButtons(true);
                this._noCliksAllowed = false;
            }else{
                this.onlyHoverAllowed();
            }

            this.setAvailableBets();
        }else
            this._pickableButtons(false);
    };

    p.onlyHoverAllowed = function(){//LATEST
        console.log("NOOOOOOOOOO CLICK, only hover maybe");
        //this.scene.hoverCursor = "auto";
        this._pickableButtons(true);
        this._noCliksAllowed = true;
    };

    p._pickableButtons = function(bool){//LATEST
        for(var i =0,j = this._betButons.length;i<j;i++)
            this._betButons[i].mouseEnabled = bool;
    };

    p._setAsButton = function(mesh,onClick,onHover){
        mesh.on("click",function(e){
            if(_this._noCliksAllowed){
                return;
            }
            onClick(e.currentTarget);
        });
        if(onHover !== null){
            mesh.on("mouseover",function(e){
                onHover(e.currentTarget,true);
            });

            mesh.on("mouseout",function(e){
                onHover(e.currentTarget,false);
            });
        }
    };

    p.recalculate = function(currentBet){
        currentBet = currentBet*100;
        var BETCHIPS = this.BETCHIPS;
        var amounts = [];

        for(var i = 0,j=BETCHIPS.length-1;i<j;i++){
            var chip =  Math.floor(currentBet/BETCHIPS[i]);
            currentBet -= chip*BETCHIPS[i];
            amounts[i]=chip;
        }
        amounts[BETCHIPS.length-1]=Math.floor(currentBet);
        return amounts;
    };

    p.clearChipsPerField = function(betFieldMesh){
        if(betFieldMesh.chipsArr.length>0){
            for(var i = 0,j = betFieldMesh.chipsArr.length;i<j;i++ ){
                this.removeChild(betFieldMesh.chipsArr[i]);
               // betFieldMesh.chipsArr[i].dispose();
            }
        }
        betFieldMesh.chipsArr = [];
    };

    p.recalculateChips = function(amounts,betFieldMesh) {
        this.clearChipsPerField(betFieldMesh);
        //for (var i= 0,j = amounts.length; i < j ; i++) {
        for (var i= amounts.length-1,j = 0; i >= j ; i--) {// reversed, now its from lowest to highest chip value placement
            if(amounts[i]>0){
                //for(var k = 0,l = amounts[i];k<l;k++){
                for(var k = amounts[i]-1,l = 0;k>=l;k--){// reversed
                    this.deployChips(this.BETCHIPS[i],betFieldMesh);
                }
            }
        }
    };

    p.deployChips = function(chip,betFieldMesh){
        var selectedChipName = String(chip);
        var selectedChip = this.getChildByName(selectedChipName).clone(true);
        console.log("selectedChipName "+selectedChipName);
        this.addChild(selectedChip);
        selectedChip.visible = true;
        selectedChip.mouseChildren = false;

        var meshPositionX = (betFieldMesh.x +betFieldMesh.normalState.image.width/2);
        var yPos = (betFieldMesh.y + (betFieldMesh.normalState.image.height/2)) - (4*betFieldMesh.chipsArr.length);

        selectedChip.x = meshPositionX/*+gamecore.Utils.NUMBER.randomRange(-2,2)*/;
        selectedChip.y = yPos;
        selectedChip.rotation = gamecore.Utils.NUMBER.randomRange(-30,30);

        betFieldMesh.chipsArr.push(selectedChip);
    };

    //checkAvail
    p.setAvailableBets = function() {
        console.log("specialBets here:::::")
        var chipValue = this.selectedBetValue;
        var currentBet = this._currentBetAmount;
        var maxStake = this.maxStake;
        var attemptedCredit = alteastream.Roulette.getInstance().credit - currentBet;

        //console.log("CHIP SELECTED "+chipValue);
        //if (attemptedCredit < alteastream.Roulette.getInstance().credits)//CHECK!!!

        var _setAvailable = function(betType,betName){
            if (attemptedCredit < (betType * chipValue) || (betType * chipValue + currentBet) > maxStake) {
                _this.getChildByName(betName).setDisabled(true);
            }else {
                _this.getChildByName(betName).setDisabled(false);
            }
        }

        _setAvailable(this.jeuzChips,"jeuZ");
        _setAvailable(this.tiersChips,"tiers");
        _setAvailable(this.orphelinsChips,"orphelins");
        _setAvailable(this.voisinsChips,"voisins");

        for(var i = 0;i<this.TOTAL_NUMBERS;i++){
            _setAvailable(this.neighbours_0Chips,"neighbours_"+i);
        }
        for(var j = 0;j<this.FINALS_NUMBERS;j++){
            _setAvailable(this["finals_"+j+"Chips"],"finals_"+j);
        }

        for(var k = 0;k<this.STREET_POSITIONS;k++){
            var betNameStreet = "street_"+k;
            _this.getChildByName(betNameStreet).mouseEnabled = !(attemptedCredit < (this.street_0Chips * chipValue) || (this.street_0Chips * chipValue + currentBet) > maxStake);
        }

        for(var l = 0;l<this.LINES_POSITIONS;l++){
            var betNameLine = "line_"+l;
            _this.getChildByName(betNameLine).mouseEnabled = !(attemptedCredit < (this.line_0Chips * chipValue) || (this.line_0Chips * chipValue + currentBet) > maxStake);
        }

        var controlBoard = alteastream.RouletteControlBoard.getInstance();
        controlBoard.updateStreetBetLabels( _this.getChildByName("street_0").mouseEnabled);
        controlBoard.updateSixLineBetLabels(_this.getChildByName("line_0").mouseEnabled);
        //staviti za split image (fieldSplit )mozda da bude mala tacka, menja avail sa "x" i "o"
        //_this.getChildByName(betNameLine).image = bool === true? o:x:
    };

    p.hideSpecials = function(bool){
        console.log("hide specialBets here::::: "+bool)
       /* var scene = this.scene;
        scene.getChildByName("orphelins_gray").visible = !bool;
        scene.getChildByName("tiers_gray").visible = !bool;
        scene.getChildByName("voisins_gray").visible = !bool;
        scene.getChildByName("jeuZ_gray").visible = !bool;*/
    };

    p.setAvailableCoins = function() {
        var betChips = this.BETCHIPS;
        var maxStake = this.maxStake;
        var currentBetAmount = this._currentBetAmount;
        var currentCredit = alteastream.Roulette.getInstance().credit;
        var controlBoard = alteastream.RouletteControlBoard.getInstance();
        var availCoins = [];

        for (var i = 0,j = betChips.length; i<j; i++ ) {
            var betChip = betChips[i];
            var betChipValue = betChips[i]/100;

            if ((currentBetAmount + betChipValue) > currentCredit || betChipValue > currentCredit || betChipValue > maxStake || currentBetAmount > 0 && (currentBetAmount + betChipValue) > maxStake) {
                controlBoard.disableChip(String(betChip),true);
            }
            else {
                controlBoard.disableChip(String(betChip),false);
                availCoins.push(betChip);
            }
        }

        if(availCoins.length>0){
            if(availCoins.indexOf(_this.selectedChipValue)>-1){
                var chip = String(availCoins[availCoins.indexOf(_this.selectedChipValue)]);
                //var chip = String(availCoins[0]);
                controlBoard.highlightChip(chip,true);
            }else{_this.toDefaultCoin();}
        }
    };

    //local test
    /*p.testArrayLength = function(onBet){
        var betmatrix = _this.BETMATRIX;
        for(var i =0,j = betmatrix.length;i<j;i++){
            var betField = _this.scene.getChildByName(betmatrix[i].name);
            console.log(onBet+" testArrayLength chipsArr.length " +betField.chipsArr.length);
        }
    };*/

    alteastream.BetTable = createjs.promote(BetTable, "ABSBetTable");
})();