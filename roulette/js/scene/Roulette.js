

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Roulette = function() {
        this.AbstractScene_constructor();
        this.initialize_Roulette();
    };
    var p = createjs.extend(Roulette, alteastream.AbstractScene);

// static properties:
    //statuses::
    Roulette.START = 1;
    Roulette.PLACE_BET = 2;
    Roulette.BALL_IN_RIM = 3;
    Roulette.NO_MORE_BETS = 4;
    Roulette.WINNING_NUMBER = 5;
    Roulette.TABLE_CLOSED = 6;
    Roulette.DEALER_LOCK = 7;
// events:
// public properties:
// private properties:

    var _this = null;
    p.active = false;
    p.okToShowBalance = true;
    p._gameResult = {};

// constructor:
    p.initialize_Roulette = function() {
        _this = this;
        this._local_activate();// local ver
        this.setSessionToken();
        this._callInitMethods();

        console.log("initialize_Roulette..");
    };

    Roulette.getInstance = function(){
        return _this;
    };

    p._callInitMethods = function() {
        var machineParams = window.localStorage.getItem('machineParameters');
        this.machineParameters = JSON.parse(machineParams);

        // todo until backend sets this 4 parameters
        /*if(!this.machineParameters.chipValues){this.machineParameters.chipValues = [1000,500,100,50,25,10,5,1];}
        if(!this.machineParameters.playedNumbers){this.machineParameters.playedNumbers = [19,33,16,5,0,6,24,31,9,7];}
        if(!this.machineParameters.minStake){this.machineParameters.minStake = 0.20;}
        if(!this.machineParameters.maxStake){this.machineParameters.maxStake = 5.00;}*/

        this.setHttpCommunication();

        this.setSocketCommunication("Roulette");
        this.socketCommunicator.activate(this.machineParameters);

        //this._addInfo();
        this.addGiveUpButton(true);//giveUpButton
        //this.monitorNetworkStatus();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5,8);
    };

    //new socket
    p.onSocketConnect = function(frame){ // invalid token fix new
        console.log("onSocketConnect::::::::::::::::::: ");
        _this.socketCommunicator.onGameConnect(frame);

        var machineParams = _this.machineParameters;

        _this._setControlBoard(machineParams);
        _this._setBetTable(machineParams.chipValues);
        _this._setWinningNumberApplet();

        _this._setStreamVideo(machineParams);// new stream load comment
        _this._setStreamVideoSmall(machineParams);
        localStorage.removeItem("streamPreviewParams");// stream preview roulette

        _this.setGameParameters(machineParams);
        //_this.setPayout(machineParams.payout);//NE RULET
        //new stakes
        //_this.setCurrency();
        //_this.setMonetaryValueView(_this.controlBoard._toValueView);//NE RULET
        //_this.checkStreamLoad();// new stream load uncomment

        // new stream load comment block start
        _this.streamVideoSmall.activate(function(){});
        _this.streamVideo.activate(function(){
            _this.socketCommunicator.confirmGameInit();
            _this.streamVideoSmall.appendSpinner();
        });
        // new stream load comment block end

        //_this._setInfo();
        _this.showGame(false);
        _this.setRenderer();

        window.localStorage.removeItem('machineParameters');
    };

    p._setStreamVideo = function(address){
        var options = {id:"videoOutput",width:1920,height:1080,style:{width:"50%",height:"50%",marginLeft:"25%"}};
        this.streamVideo = new alteastream.RouletteTableStream(this,options,address);
        //this.streamVideo = new alteastream.VideoStream(this,"videoOutput",address);//old
    };

    p._setStreamVideoSmall = function(address){
        var options = {id:"videoOutputSmall",width:1920,height:1080,style:{width:"100%",height:"100%"}};
        this.streamVideoSmall = new alteastream.RouletteWheelStream(this,options,address);
        //this.streamVideoSmall = new alteastream.VideoStreamSmall(this,"videoOutputSmall",address);//old
    };

    p._setControlBoard = function(params){
        console.log("_setControlBoard::::::::::::::::::: ");
        this.controlBoard = new alteastream.RouletteControlBoard(params);
        this.controlBoard.plugin(this);
        this.controlBoard.initialSetup();
        this.addChild(this.controlBoard);
        this.controlBoard.visible = false;
    };

    p._setBetTable = function(chipValues){
        console.log("_setBetTable::::::::::::::::::: ");
        this.betTable = new alteastream.BetTable(this,alteastream.TableMatrix.FRENCH,chipValues);
        this.addChild(this.betTable);
        this.betTable.visible = false;
        //this.betTable.y=730;
        //this.betTable.x=85;
    };

    p._setWinningNumberApplet = function(){
        this.winningNumberApplet = new alteastream.WinningNumberApplet();
        this.addChild(this.winningNumberApplet);
        this.winningNumberApplet.x = alteastream.AbstractScene.GAME_WIDTH*0.5 - this.winningNumberApplet.getWidth()*0.5;
        this.winningNumberApplet.y = alteastream.AbstractScene.GAME_HEIGHT*0.5- this.winningNumberApplet.getHeight()*0.5-280;
        this.winningNumberApplet.mouseEnabled = false;
        this.winningNumberApplet.mouseChildren = false;
    };

    p.setGameParameters = function(response){
        console.log("setGameParameters:: ");
        console.log("balance:: "+response.balance);

       ///////////////////////////////
        this.setCrd(response.balance);

        console.log("COMM:: gameParameters");
        this.minStake = response.minStake;
        this.maxStake = response.maxStake || 20;

        this.controlIdleTime = response.controlIdleTime;
        this._kickOutPing = this.controlIdleTime/3;

        this.currency = response.currency || "N/A";
        //decimal
        //var code = 8364;//resp.user.currency
        //this.currency = String.fromCharCode( code);

        //hex, css
        //var code = '20AC';//20A4 resp.user.currency
        //this.currency = String.fromCharCode( parseInt(code, 16) );

        this.betTable.setStakeMinMax(this.minStake,this.maxStake);
        this.controlBoard.setCurrency(this.currency);
        this.controlBoard.setStakesInfo(this.minStake,this.maxStake);
        this.controlBoard.setLastPlayedNumbers(response.playedNumbers);

        this.betTable.setAvailableBets();
        this.betTable.setAvailableCoins();
        this.betTable.removeAllBets();

        this.disableAllBets(true);
    };

    p.showGame = function(bool){
        if(bool === true){
            this.addGiveUpButton(false);
            //this.controlBoard.soundBtnHandler();
            this.streamVideo.setMedia();

           /* if(is.safari()){
                var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
                if(is.not.ipad() && is.not.ios() && !isiPadOS){
                    alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
                }
            }else{
                alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
            }*/
        }

        this.controlBoard.visible = bool;
        this.betTable.visible = bool;
    };

    p.disableAllBets = function(bool){
        if(bool === true){
            this.controlBoard.disableAllChips(true);
            this.betTable.disableBetting();
        }else{
            this.controlBoard.disableAllChips(false);
            this.betTable.enableBetting();
            //this.betTable.setAvailableBets();
            //this.betTable.setAvailableCoins();
            //this.betTable.removeAllBets();
        }
    }

    //treba neki StatusHandler class
    // inicirati tek posle CONFIRM_GAME_INIT
    p.statusChange = function(status){
        console.log("statusChange:: "+status);
        var assets = alteastream.Assets;
        switch (status) {
            case Roulette.START:
                //clear winnings
                //this.disableAllBets(true);
                this.disableAllBets(false);// temp insted of PLACE_BET not comming at this moment
                //this.betTable.onCollectedWin();// oncollect?
                this.betTable.resetAnimations();
                this.winningNumberApplet.show(false);
                this.controlBoard.showUI(true);
                this.controlBoard.updateStatus("PLACE YOUR BETS");
                assets.playSound(assets.sounds.spoken_placeBet_2);
                break;
            case Roulette.PLACE_BET:
                //this.disableAllBets(false);// temp PLACE_BET not comming at this moment
                this.controlBoard.updateStatus("PLACE YOUR BETS");
                assets.playSound(assets.sounds.spoken_placeBet);
                break;
            case Roulette.BALL_IN_RIM:
                this.onSpin();
                this.controlBoard.updateStatus("FINISH YOUR BETS");
                assets.playSound(assets.sounds.spoken_placeBet);
                break;
            case Roulette.NO_MORE_BETS:
                this.disableAllBets(true);
                this.sendFinalBet();
                this.controlBoard.updateStatus("NO MORE BETS");
                assets.playSound(assets.sounds.spoken_doSpin_1);
                break;
            case Roulette.WINNING_NUMBER:
                this.controlBoard.updateStatus("WINNING NUMBER");
                break;
            case Roulette.TABLE_CLOSED:
                this.disableAllBets(true);
                this.controlBoard.updateStatus("TABLE CLOSED");
                break;
            case Roulette.DEALER_LOCK:
                this.disableAllBets(true);
                this.controlBoard.updateStatus("DEALER LOCK");
                break;
        }
    };

    p.onSpin = function(){
        console.log("onSpin ");
        //this.controlBoard.onSpin();
        //this.betTable.onSpin();
        //this.sendFinalBet();
    };

    p.onPrizeDetection = function(number){
        this.controlBoard.updateStatus("WINNING NUMBER"); //temp maybe no status sent
        this.setGameResult(number);
    };

    p.sendFinalBet = function(){
        console.log("COMM:: sendBet");
        this.okToShowBalance = false;

        var take = this.credit - this.betTable._currentBetAmount;
        this.controlBoard.setCrd(take);
        var finalUserBet = this.betTable.getFinalUserBet();

        //temp loop
        var betToSend = [];
        for(var i=0,j=finalUserBet.length;i<j;i++){
            betToSend.push(finalUserBet[i]);
            betToSend[i][1]*=100;
        }
        console.log("betToSend:::::::::::::::::: "+betToSend);
        /*// local ver
        alteastream.SocketCommunicatorRoulette.getInstance().sendBet(betToSend);
        */// local ver
    };

    p.setGameResult = function(response){
        console.log("COMM:: gameResult");
        var gameResult = this._gameResult;
        gameResult.winAmount = response.win;
        gameResult.newBalance = response.balance;
        gameResult.winNumber = response.drawnNo;
        gameResult.winColor = this.betTable.getBetField("num_"+gameResult.winNumber).color;

        this.betTable.hideSpecials(false);

        if(gameResult.winAmount>0.00){
            this.betTable.setWinningBets(gameResult.winNumber);
            this.winResult(gameResult);
        }else{
            this.noWinResult(gameResult);
        }

        this.winningNumberApplet.show(true);
        this.controlBoard.updateLastPlayedNumbers(gameResult.winNumber);
        this.okToShowBalance = true;
        this.setCrd(gameResult.newBalance);
        this._resultSpoken(gameResult.winAmount>0.00);
    };

    p.winResult = function(gameResult){
        console.log("winResult :: ");
        console.log("AMOUNT:: "+gameResult.winAmount);
        console.log("NUMBER:: "+gameResult.winNumber);
        console.log("COLOR:: "+gameResult.winColor);
        this.controlBoard.onSpinWin();
        this.winningNumberApplet.setResult(gameResult,true);

        this.betTable.highlightWin(gameResult.winNumber);
    };

    p.noWinResult = function(gameResult){
        console.log("noWinResult:: ");
        this.controlBoard.onSpinLost(gameResult);
        //after setSpinToBet
        //this.betTable.enableButtons(true);
        this.winningNumberApplet.setResult(gameResult,false);
        //this.betTable.setAvailableCoins();//LATEST
    };

    p._resultSpoken = function(win){
        var assets = alteastream.Assets;
        var spokenNumber = "spoken_"+this._gameResult.winNumber;
        assets.playSoundChain(spokenNumber,1,function(){
            var rnd;
            var phrase;
            if(win===true){
                rnd = gamecore.Utils.NUMBER.randomRange(1,4);
                phrase = "spoken_youWin_"+rnd;
                assets.playSound(phrase,1);

                assets.playSoundChain(phrase,1,function(){
                    assets.playSound("winSound",0.8);
                    assets.playSound("winCheer",0.7);
                });
            }else{
                rnd = gamecore.Utils.NUMBER.randomRange(1,2);
                phrase = "spoken_noWin_"+rnd;
                assets.playSound(phrase,1);
            }
        });
    };

    p.setCrd = function(amount){
        var crdAmountCents = Math.min(amount/1000);//1718900
        crdAmountCents = crdAmountCents.toFixed(2);
        this.credit = crdAmountCents;
        if(this.okToShowBalance)
            this.controlBoard.setCrd(amount);
    };

    p.getGameResult = function(){
        return this._gameResult;
    };

    p.infoBtnHandler = function() {
        this.onInfo();
    };

    p.onInfo = function(){
       console.log("INFO");
    };

    p.quitBtnHandler = function(){
        _this._setQuitPanel();
    };

    p.quitConfirmed = function(){
        //if(_this.socketCommunicator)
        //_this.socketCommunicator.disposeCommunication();
        _this.disableAllBets(true);
        _this.onQuit();
    };

    p.onQuit = function(){
        //alteastream.VideoStreamPreload.stop();// new stream load uncomment
        //alteastream.VideoStreamPreload.removeVideoElement();// new stream load uncomment
        this.streamVideo.stop();// new stream load comment
        this.socketCommunicator.quitPlay();
    };

    //exitMachineGame::
    p.exitMachineGameplay = function(){
        this.killGame();
        this.closeGame();
    };

    //new socket
    p.closeGame = function(){
        window.top.manageBgMusic("pause");
        //window.localStorage.setItem('backgroundMusicIsMuted', String(_this.controlBoard.soundToggled));
        window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN+"&backToHouse="+alteastream.AbstractScene.BACK_TO_HOUSE);
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
    };

    p.addGiveUpButton = function(bool){
        if(bool === true){
            var giveUpButton = this.giveUpButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnQuitSS),3);
            var width = giveUpButton.width;
            var spacing = width/2;
            giveUpButton.x = alteastream.AbstractScene.GAME_WIDTH - width - spacing;
            giveUpButton.y = 10;
            this.addChild(giveUpButton);
            giveUpButton.addEventListener("click", function () {
                _this.quitBtnHandler();
                //_this.streamVideo.stop();
                //_this.exitMachineGameplay();// nije otvorio soket, ne moze da dobije 85, token itd..mozda http poziv u ovom slucaju
            });
        }else{
            this.removeChild(this.giveUpButton);
            this.giveUpButton = null;
        }
    };

    p._local_activate = function() {
       /* this._isMobile = function() { // verzija ako hocemo da se ne instancira keyboard listener za mobilni
              //return "yes";
              return "not";
        };*/
        this._callInitMethods = function () {};
        //var machineParams = "ws://87.130.112.6:8083/ws";

        this.setHttpCommunication();

        this.setSocketCommunication("Roulette");

        this.addGiveUpButton(true);
        this.giveUpButton.y+=50;

        this.machineParameters = {"state":0,"key":"79a2fb56-7c2c-46cf-97d5-35db5661425e","controlIdleTime":60000,"streamendpoint":"ws://87.130.112.6:8889","webrtcurl":"ws://87.130.112.6:8083/ws","chipValues":[1000,500,100,50,25,10,5,1],"playedNumbers":[19,33,16,5,0,6,24,31,9,7],"balance":999800,"minStake":0.20,"maxStake":5.00,"tokenValue":100.0,"tokens":0,"maxTokens":60,"currency":"RSD","payout":{"1":5,"2":10,"3":15,"4":45,"101":0.25},"bonus":{},"bonus_limit":0,"gameCode":"ticketcircus"};

        this._setControlBoard(this.machineParameters);
        this._setBetTable(this.machineParameters.chipValues);

        this._setStreamVideo(this.machineParameters.webrtcurl);
        this._setStreamVideoSmall(this.machineParameters.webrtcurl);

        this.setGameParameters(this.machineParameters);
        //this.setCurrency();
        //this._addInfo();

        //this._setInfo();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5,8);
        //this.setMonetaryValueView(this.controlBoard._toValueView);//NE RULET

        this._setWinningNumberApplet();
        this.showGame(true);
        setTimeout(function(){
            _this.addGiveUpButton(false);
            if(_this._hiddenMenuButton){
                _this._hiddenMenuButton.visible = true;
                //_this._showHiddenMenu(true);
            }
        },2000);

        setTimeout(function(){_this.statusChange(Roulette.START);},3000);
        //setTimeout(function(){_this.statusChange(Roulette.PLACE_BET);},1000)
        setTimeout(function(){_this.statusChange(Roulette.BALL_IN_RIM);},8000);
        setTimeout(function(){_this.statusChange(Roulette.NO_MORE_BETS);},13000);
        setTimeout(function(){
            _this.statusChange(Roulette.WINNING_NUMBER);
            _this.onPrizeDetection({"user_bets":5.0,"win":250,"drawnNo":28,"amount":0,"money":0,"balance":862206});
        },18000);
        setTimeout(function(){_this.statusChange(Roulette.START);},23000);
        //setTimeout(function(){_this.statusChange(Roulette.PLACE_BET);},18000)
    };

    p.onMinStake = function(bool){
        console.log("min stake true:::::::::::::");
        this.controlBoard.enableSpin(bool);
    };

    p.onMaxStake = function(bool){
        this.betTable.enableButtons(!bool);
        if(bool ===true){
            console.log("max stake true:::::::::::::");
            this.betTable.onlyHoverAllowed();
            //gamecore.Assets.playSound("spoken_maxReached",1);
        }
    };
    

// static methods:
// public methods:

    alteastream.Roulette = createjs.promote(Roulette,"AbstractScene");
})();