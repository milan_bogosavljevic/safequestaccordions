// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var WinningNumberApplet = function() {
        this.Container_constructor();
        this.initialize_WinningNumberApplet();
    };

    var p = createjs.extend(WinningNumberApplet, createjs.Container);
    var _instance = null;

// static properties:
// events:
// public properties:
// private properties:
    p._normalFontColor = "#ffffff";
    p._highlitedFontColor = "#000000";

// constructor:
// static methods:
// public methods:
    p.initialize_WinningNumberApplet = function() {

        _instance = this;

        this._resultBg = alteastream.Assets.getImage(alteastream.Assets.images.winningBg_black);
        this.addChild(this._resultBg);

        this._fieldWidth =  this._resultBg.image.width;
        this._fieldHeight = this._resultBg.image.height;
        var fontSmall = "24px RobotoCondensed";
        var fontBig = "60px RobotoCondensed";
        var colorWhite = "#ffffff";

        var _resultLabel = this._resultLabel = new createjs.Text("Winning Number",fontSmall,colorWhite).set({textAlign:"center",textBaseline:"middle"});
        this.addChild(_resultLabel);
        _resultLabel.x = this._fieldWidth*0.5;
        _resultLabel.y = -30;

        var _resultNumber = this._resultNumber = new createjs.Text("30",fontBig,colorWhite).set({textAlign:"center",textBaseline:"middle"});
        this.addChild(_resultNumber);
        _resultNumber.x = this._fieldWidth*0.5;
        _resultNumber.y = this._fieldHeight*0.5+5;

        var _winLabel = this._winLabel = new createjs.Text("YOU WIN:",fontSmall,colorWhite).set({textAlign:"center",textBaseline:"middle"});
        this.addChild(_winLabel);
        _winLabel.x = this._fieldWidth*0.5;
        _winLabel.y = this._fieldHeight+30;

        var _winAmount = this._winAmount = new createjs.Text("0.20",fontBig,colorWhite).set({textAlign:"center",textBaseline:"middle"});
        this.addChild(_winAmount);
        _winAmount.x = this._fieldWidth*0.5;
        _winAmount.y = this._fieldHeight+80;

        this.show(false);
    };

    p.getWidth = function(){
        return  this._fieldWidth;
    };

    p.getHeight = function(){
        return  this._fieldHeight;
    };

    p.setResult = function(result,win){
        this._resultBg.image = alteastream.Assets.getImage(alteastream.Assets.images["winningBg_"+result.winColor]).image;
        this._resultNumber.text = result.winNumber;
        this._winAmount.text = win === true? Math.min(result.winAmount/100).toFixed(2):"";
        this._winLabel.text = win === true?"You Win":"No Win";
    }

    p.show = function(bool){
       this.visible = bool;
    };

    alteastream.WinningNumberApplet = createjs.promote(WinningNumberApplet,"Container");
})();