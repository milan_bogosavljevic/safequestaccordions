
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSBetTable = function(game,betmatrix) {
        this.Container_constructor();
        this.initialize_ABSBetTable(game,betmatrix);
    };

    var p = createjs.extend(ABSBetTable, createjs.Container);
    var _instance = null;

// static properties:

// events:
// public properties:
    p.BETMATRIX = [];
    p.TOTAL_NUMBERS = 37;
    p.FINALS_NUMBERS = 10;
    p.STREET_POSITIONS = 12;
    p.LINES_POSITIONS = 11;
    p._currentBet = [];
    p._selectedBetValuesArr = [];
    p.finalUserBet = [];
    p._currentBetAmount = 0.00;
    p.minStake = 0;
    p.maxStake = 0;
    p.minStakeReached = false;
    p.maxStakeReached = false;
    p.minStakeOn = false;
    p.maxStakeOn = false;
    p.minStakeOff = false;
    p.maxStakeOff = false;
// private properties:
    
// constructor:
// static methods:
// public methods:
    p.initialize_ABSBetTable = function(game,betmatrix) {
        _instance = this;
        this.game = game;//gamecore[gamecore.AbstractScene.NAME].getInstance();
        this.scene = this.game/*.scene*/;
        this.BETMATRIX = betmatrix;
        this._createLookUpTable(this.BETMATRIX);
    };

    p.setStakeMinMax = function(minStake,maxStake){
        this.minStake = minStake;
        this.maxStake = maxStake;
    };

    p.getMinStake = function(){
        return this.minStake;
    };

    p.getMaxStake = function(){
        return this.maxStake;
    };

    p._checkStake = function(){
        this.minStakeReached = this._currentBetAmount>=this.minStake;
        this.maxStakeReached = this._currentBetAmount>=this.maxStake;
        if(this.minStakeReached){
            if(!this.minStakeOn){
                this.minStakeOn = true;
                this.minStakeOff = false;
                alteastream.Roulette.getInstance().onMinStake(true);
            }
        }else{
            this.minStakeOn = false;
            if(!this.minStakeOff){
                this.minStakeOff = true;
                alteastream.Roulette.getInstance().onMinStake(false);
            }
        }
        if(this.maxStakeReached){
            if(!this.maxStakeOn){
                this.maxStakeOn = true;
                this.maxStakeOff = false;
                alteastream.Roulette.getInstance().onMaxStake(true);
            }
        }else{
            this.maxStakeOn = false;
            if(!this.maxStakeOff){
                this.maxStakeOff = true;
                alteastream.Roulette.getInstance().onMaxStake(false);
            }
        }
    };

    p.pushBet = function(chosenBet,amount){
        this._currentBet.push([chosenBet,amount]);
        var targetBetType = this.BETMATRIX[this._lookUpTable[chosenBet]];
        targetBetType.bet = Math.round((targetBetType.bet + amount) * 100) / 100;
        //console.log("chosenBet:: "+chosenBet);
        //console.log("pushBet:: "+targetBetType.bet);
        this._currentBetAmount = Math.round((this._currentBetAmount + amount) * 100) / 100;
        //console.log("currentBetAmount:: "+this._currentBetAmount);
        this._checkStake();
    };

    p.popBet = function(){
        var lastBetType = this._currentBet.pop();
        var targetBetType = this.BETMATRIX[this._lookUpTable[lastBetType[0]]];
        targetBetType.bet = Math.round((targetBetType.bet - lastBetType[1]) * 100) / 100;
        this._currentBetAmount = Math.round((this._currentBetAmount - lastBetType[1]) * 100) / 100;
        //console.log("currentBetAmount--:: "+this._currentBetAmount);
        this._checkStake();
    };

    p.clearBets = function(){
        this._currentBet = [];
        this._selectedBetValuesArr = [];
        this._currentBetAmount = 0.00;
        this.finalUserBet = [];
        this.highlightUserBet = [];//correctionHighlight new

        this.minStakeReached = false;
        this.maxStakeReached = false;
        this.minStakeOn = false;
        this.maxStakeOn = false;
        this.minStakeOff = false;
        this.maxStakeOff = false;
        alteastream.Roulette.getInstance().onMinStake(false);
        alteastream.Roulette.getInstance().onMaxStake(false);

        for(var i = 0,j = this.BETMATRIX.length; i<j; i++){
            this.BETMATRIX[i].bet = 0.00;
        }
    };

    p.getFinalUserBet = function(){
        //var cb = JSON.parse(JSON.stringify( this._currentBet ));
        //this.highlightUserBet = this.mergeBets(cb);// only jeuZ voisins...

        var finalBet = [];
        for(var i = 0,j = this.BETMATRIX.length; i<j; i++){
            var betName = this.BETMATRIX[i].name;
            var specials = this.BETMATRIX[i].special || null;
            var betAmount = this.BETMATRIX[i].bet;
            if(betAmount > 0.00){
                if(!specials){
                    finalBet.push([betName,betAmount]);
                }
            }
        }
        this.highlightUserBet = this.mergeBets(finalBet);
        this.finalUserBet = this.mergeBets(finalBet);
        return this.finalUserBet;
    };

    p.mergeBets = function(bets){
        var merged = [];
        var helper = [];
        var index;

        for(var i = 0,j = bets.length; i < j; i++) {
            index = helper.indexOf(bets[i][0]);
            if(index !== -1) {
                var sum = merged[index][1]+bets[i][1];
                merged[index][1] = Math.floor(sum*100)/100;
            }
            else {
                helper.push(bets[i][0]);
                merged.push(bets[i]);
            }
        }
        //console.log("MERGED "+merged);
        return merged;
    };

    p.getBetField = function(chosenBet){
        return this.BETMATRIX[this._lookUpTable[chosenBet]];
    };

    p.setWinningBets = function(winningNumber){
        this.wonBetsArray = [];
        var betMatrix = this.BETMATRIX;
        var finalUserBet = this.highlightUserBet;//correctionHighlight new
        var isSplit;
        var isNumber;
        var hasSplitOrDirect = false;
        for(var i = 0,j = finalUserBet.length; i<j; i++){
            var playedBets = finalUserBet[i][0];
            var betType = betMatrix[this._lookUpTable[playedBets]];
            isSplit = betType.name.substr(0,5)==="split" && betType.numbers.indexOf(winningNumber)>-1;
            isNumber = betType.name ==="num_"+String(winningNumber);
            if(isSplit === true || isNumber ===true){
                hasSplitOrDirect = true;
            }
            else{
                if(betType.numbers.indexOf(winningNumber)>-1){
                     this.wonBetsArray.push([betType.name]);
                }
            }
        }
        if(hasSplitOrDirect){
            var number = "num_"+String(winningNumber);
            this.wonBetsArray.unshift([number]);
        }
    };

// private methods:
    p._createLookUpTable = function(betMatrix){
        this._lookUpTable = {};
        for(var i = 0,j = betMatrix.length; i<j; i++)
            this._lookUpTable[betMatrix[i].name] = i;
    };

    alteastream.ABSBetTable = createjs.promote(ABSBetTable,"Container");
})();