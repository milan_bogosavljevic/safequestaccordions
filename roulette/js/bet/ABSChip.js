// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSChip = function(value,assetID) {
        this.Container_constructor();
        this.initialize_ABSChip(value,assetID);
    };

    var p = createjs.extend(ABSChip, createjs.Container);
    var _instance = null;

// static properties:

// events:
// public properties:
// private properties:
    
// constructor:
// static methods:
// public methods:
    p.initialize_ABSChip = function(value,assetID) {
        _instance = this;

        var fontSize = 16;
        var fontSmall = fontSize+"px RobotoCondensed";
        var white = "#ffffff";

        this.skin = alteastream.Assets.getImage(alteastream.Assets.images["chip"+assetID]);
        this.addChild(this.skin);

        var w = this.skin.image.width,h = this.skin.image.height;

        //if(value!==null)
        this.label = new alteastream.BitmapText(String(value),fontSmall, white,{x:w/2,y:h/2-fontSize/2,textAlign:"center"});
        this.addChild(this.label);

        this.regX = this.regY = this.skin.image.width/2;
        this.mouseEnabled = false;
    };

    alteastream.ABSChip = createjs.promote(ABSChip,"Container");
})();