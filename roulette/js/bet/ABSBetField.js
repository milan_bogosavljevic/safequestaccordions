// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSBetField = function(name,label,imageName) {
        this.Container_constructor();
        this.initialize_ABSBetField(name,label,imageName);
    };

    var p = createjs.extend(ABSBetField, createjs.Container);
    var _instance = null;

// static properties:
// events:
// public properties:
// private properties:
    p._normalFontColor = "#ffffff";
    p._highlitedFontColor = "#000000";
    p._disabledFontColor = "#b4b4b4";

// constructor:
// static methods:
// public methods:
    p.initialize_ABSBetField = function(name,label,imageName) {

        _instance = this;
        this.name = name;

        this.highlightState = alteastream.Assets.getImage(alteastream.Assets.images[imageName+"H"]);
        this.normalState = alteastream.Assets.getImage(alteastream.Assets.images[imageName]);
        this.addChild(this.highlightState,this.normalState);
        this.highlightState.visible = false;

        var w = this._fieldWidth =  this.normalState.image.width,h = this._fieldHeight = this.normalState.image.height;

        var fontSize = 30;
        //var bitmapCachedY = this.bitmapCachedY=6;
        //var fontSmall = "bold "+fontSize+"px HurmeGeometricSans3";
        var fontSmall = fontSize+"px RobotoCondensed";

        if(label!==null){
            this.label = new alteastream.BitmapText(String(label),fontSmall, this._normalFontColor,{x:w/2,y:(h/2-fontSize/2),textAlign:"center"/*,yCacheAdd:bitmapCachedY*/});
            this.label.mouseEnabled = false;
            this.addChild(this.label);
            if(label ==="red" || label ==="black"){
                this.label.visible = false;
                var decal = this.decal = alteastream.Assets.getImage(alteastream.Assets.images["label_"+label]);
                this.addChild(decal);
                decal.x = w/2-decal.image.width/2;
                decal.y = h/2-decal.image.height/2;
            }
        }

        this.chipsArr = [];
    };

    p.getWidth = function(){
        return  this._fieldWidth;
    };

    p.getHeight = function(){
        return  this._fieldHeight;
    };

    p.setColor = function(color,imageName){
        this.normalState.image = alteastream.Assets.getImage(alteastream.Assets.images[imageName+"_"+color]).image;
    }

    p.setFont = function(size,fontName){// bitmapText, temp srediti
        var fontSize = size;
        var fontSmall = fontSize+"px "+ fontName;

        var labelTxt = this.label.text;
        var childIndx = this.getChildIndex(this.label);
        this.removeChild(this.label);

        this.label = new alteastream.BitmapText(labelTxt,fontSmall, this._normalFontColor,{x:this._fieldWidth/2,y:this._fieldHeight/2-fontSize/2,textAlign:"center",yCacheAdd:this.bitmapCachedY});
        this.addChildAt(this.label,childIndx);
    };

    p.setFontColors = function(normal,highlight){
        this._normalFontColor = normal;
        this._highlitedFontColor = highlight;
    }

    p.setHighlighted = function(bool){
        this.highlightState.visible = bool;
        this.normalState.visible = !bool;
        //this.label.color = bool === true? this._highlitedFontColor : this._normalFontColor;// normal text
        var color = bool ===true?this._highlitedFontColor : this._normalFontColor;// bitmap text
        this.label.updateText("color",color);// bitmap text
    };

    p.setDisabled = function(bool){
        //this.highlightState.visible = !bool;
        this.normalState.visible = true;
        //this.label.color = bool === true? this._highlitedFontColor : this._normalFontColor;// normal text
        var color = bool ===true?this._disabledFontColor : this._normalFontColor;// bitmap text
        this.label.updateText("color",color);// bitmap text
        this.alpha = bool ===true? 0.7:1;
        this.mouseEnabled = !bool;
    }

    alteastream.ABSBetField = createjs.promote(ABSBetField,"Container");
})();