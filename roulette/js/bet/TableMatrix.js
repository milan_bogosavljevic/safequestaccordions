this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var TableMatrix = function() {};

    TableMatrix.FRENCH = [
            {name:"num_0", bet:0.00,numbers:[0],color:"green"},  {name:"num_1", bet:0.00,numbers:[1],color:"red"},  {name:"num_2", bet:0.00,numbers:[2],color:"black"},  {name:"num_3", bet:0.00,numbers:[3],color:"red"},  {name:"num_4", bet:0.00,numbers:[4],color:"black"},  {name:"num_5", bet:0.00,numbers:[5],color:"red"},  {name:"num_6", bet:0.00,numbers:[6],color:"black"},  {name:"num_7", bet:0.00,numbers:[7],color:"red"},  {name:"num_8", bet:0.00,numbers:[8],color:"black"},
            {name:"num_9", bet:0.00,numbers:[9],color:"red"},  {name:"num_10",bet:0.00,numbers:[10],color:"black"}, {name:"num_11",bet:0.00,numbers:[11],color:"black"}, {name:"num_12",bet:0.00,numbers:[12],color:"red"}, {name:"num_13",bet:0.00,numbers:[13],color:"black"}, {name:"num_14",bet:0.00,numbers:[14],color:"red"}, {name:"num_15",bet:0.00,numbers:[15],color:"black"}, {name:"num_16",bet:0.00,numbers:[16],color:"red"}, {name:"num_17",bet:0.00,numbers:[17],color:"black"},
            {name:"num_18",bet:0.00,numbers:[18],color:"red"}, {name:"num_19",bet:0.00,numbers:[19],color:"red"}, {name:"num_20",bet:0.00,numbers:[20],color:"black"}, {name:"num_21",bet:0.00,numbers:[21],color:"red"}, {name:"num_22",bet:0.00,numbers:[22],color:"black"}, {name:"num_23",bet:0.00,numbers:[23],color:"red"}, {name:"num_24",bet:0.00,numbers:[24],color:"black"}, {name:"num_25",bet:0.00,numbers:[25],color:"red"}, {name:"num_26",bet:0.00,numbers:[26],color:"black"},
            {name:"num_27",bet:0.00,numbers:[27],color:"red"}, {name:"num_28",bet:0.00,numbers:[28],color:"black"}, {name:"num_29",bet:0.00,numbers:[29],color:"black"}, {name:"num_30",bet:0.00,numbers:[30],color:"red"}, {name:"num_31",bet:0.00,numbers:[31],color:"black"}, {name:"num_32",bet:0.00,numbers:[32],color:"red"}, {name:"num_33",bet:0.00,numbers:[33],color:"black"}, {name:"num_34",bet:0.00,numbers:[34],color:"red"}, {name:"num_35",bet:0.00,numbers:[35],color:"black"},
            {name:"num_36",bet:0.00,numbers:[36],color:"red"},

            {name:"1st 12", bet:0.00,numbers:[1,2,3,4,5,6,7,8,9,10,11,12]},
            {name:"2nd 12", bet:0.00,numbers:[13,14,15,16,17,18,19,20,21,22,23,24]},
            {name:"3rd 12", bet:0.00,numbers:[25,26,27,28,29,30,31,32,33,34,35,36]},
            {name:"EVEN",   bet:0.00,numbers:[2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36]},
            {name:"red",    bet:0.00,numbers:[32,19,21,25,34,27,36,30,23,5,16,1,14,9,18,7,12,3]},
            {name:"black",  bet:0.00,numbers:[26,15,4,2,17,6,13,11,8,10,24,33,20,31,22,29,28,35]},
            {name:"ODD",    bet:0.00,numbers:[1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35]},
            {name:"19 - 36",bet:0.00,numbers:[19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36]},
            {name:"1 - 18", bet:0.00,numbers:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]},

            {name:"c1", bet:0.00,numbers:[3,6,9,12,15,18,21,24,27,30,33,36]},
            {name:"c2", bet:0.00,numbers:[2,5,8,11,14,17,20,23,26,29,32,35]},
            {name:"c3", bet:0.00,numbers:[1,4,7,10,13,16,19,22,25,28,31,34]},

            {name:"split0_1", bet:0.00,numbers:[0,1]},
            {name:"split0_2", bet:0.00,numbers:[0,2]},
            {name:"split0_3", bet:0.00,numbers:[0,3]},
            {name:"split0_4", bet:0.00,numbers:[0,1,2]},
            {name:"split0_5", bet:0.00,numbers:[0,2,3]},

            {name:"split2H_1",   bet:0.00,numbers:[1,4]},
            {name:"split2H_2",   bet:0.00,numbers:[2,5]},
            {name:"split2H_3",   bet:0.00,numbers:[3,6]},
            {name:"split2H_4",   bet:0.00,numbers:[4,7]},
            {name:"split2H_5",   bet:0.00,numbers:[5,8]},
            {name:"split2H_6",   bet:0.00,numbers:[6,9]},
            {name:"split2H_7",   bet:0.00,numbers:[7,10]},
            {name:"split2H_8",   bet:0.00,numbers:[8,11]},
            {name:"split2H_9",   bet:0.00,numbers:[9,12]},
            {name:"split2H_10",  bet:0.00,numbers:[10,13]},
            {name:"split2H_11",  bet:0.00,numbers:[11,14]},
            {name:"split2H_12",  bet:0.00,numbers:[12,15]},
            {name:"split2H_13",  bet:0.00,numbers:[13,16]},
            {name:"split2H_14",  bet:0.00,numbers:[14,17]},
            {name:"split2H_15",  bet:0.00,numbers:[15,18]},
            {name:"split2H_16",  bet:0.00,numbers:[16,19]},
            {name:"split2H_17",  bet:0.00,numbers:[17,20]},
            {name:"split2H_18",  bet:0.00,numbers:[18,21]},
            {name:"split2H_19",  bet:0.00,numbers:[19,22]},
            {name:"split2H_20",  bet:0.00,numbers:[20,23]},
            {name:"split2H_21",  bet:0.00,numbers:[21,24]},
            {name:"split2H_22",  bet:0.00,numbers:[22,25]},
            {name:"split2H_23",  bet:0.00,numbers:[23,26]},
            {name:"split2H_24",  bet:0.00,numbers:[24,27]},
            {name:"split2H_25",  bet:0.00,numbers:[25,28]},
            {name:"split2H_26",  bet:0.00,numbers:[26,29]},
            {name:"split2H_27",  bet:0.00,numbers:[27,30]},
            {name:"split2H_28",  bet:0.00,numbers:[28,31]},
            {name:"split2H_29",  bet:0.00,numbers:[29,32]},
            {name:"split2H_30",  bet:0.00,numbers:[30,33]},
            {name:"split2H_31",  bet:0.00,numbers:[31,34]},
            {name:"split2H_32",  bet:0.00,numbers:[32,35]},
            {name:"split2H_33",  bet:0.00,numbers:[33,36]},

            {name:"split2V_1",  bet:0.00,numbers:[1,2]},
            {name:"split2V_2",  bet:0.00,numbers:[2,3]},
            {name:"split2V_4",  bet:0.00,numbers:[4,5]},
            {name:"split2V_5",  bet:0.00,numbers:[5,6]},
            {name:"split2V_8",  bet:0.00,numbers:[8,9]},
            {name:"split2V_7",  bet:0.00,numbers:[7,8]},
            {name:"split2V_11", bet:0.00,numbers:[11,12]},
            {name:"split2V_10", bet:0.00,numbers:[10,11]},
            {name:"split2V_13", bet:0.00,numbers:[13,14]},
            {name:"split2V_14", bet:0.00,numbers:[14,15]},
            {name:"split2V_17", bet:0.00,numbers:[17,18]},
            {name:"split2V_16", bet:0.00,numbers:[16,17]},
            {name:"split2V_19", bet:0.00,numbers:[19,20]},
            {name:"split2V_20", bet:0.00,numbers:[20,21]},
            {name:"split2V_22", bet:0.00,numbers:[22,23]},
            {name:"split2V_23", bet:0.00,numbers:[23,24]},
            {name:"split2V_25", bet:0.00,numbers:[25,26]},
            {name:"split2V_26", bet:0.00,numbers:[26,27]},
            {name:"split2V_28", bet:0.00,numbers:[28,29]},
            {name:"split2V_29", bet:0.00,numbers:[29,30]},
            {name:"split2V_31", bet:0.00,numbers:[31,32]},
            {name:"split2V_32", bet:0.00,numbers:[32,33]},
            {name:"split2V_34", bet:0.00,numbers:[34,35]},
            {name:"split2V_35", bet:0.00,numbers:[35,36]},

            {name:"split4_1",  bet:0.00,numbers:[1,2,4,5]},
            {name:"split4_2",  bet:0.00,numbers:[2,3,5,6]},
            {name:"split4_4",  bet:0.00,numbers:[4,5,7,8]},
            {name:"split4_5",  bet:0.00,numbers:[5,6,8,9]},
            {name:"split4_7",  bet:0.00,numbers:[7,8,10,11]},
            {name:"split4_8",  bet:0.00,numbers:[8,9,11,12]},
            {name:"split4_10", bet:0.00,numbers:[10,11,13,14]},
            {name:"split4_11", bet:0.00,numbers:[11,12,14,15]},
            {name:"split4_13", bet:0.00,numbers:[13,14,16,17]},
            {name:"split4_14", bet:0.00,numbers:[14,15,17,18]},
            {name:"split4_16", bet:0.00,numbers:[16,17,19,20]},
            {name:"split4_17", bet:0.00,numbers:[17,18,20,21]},
            {name:"split4_19", bet:0.00,numbers:[19,20,22,23]},
            {name:"split4_20", bet:0.00,numbers:[20,21,23,24]},
            {name:"split4_22", bet:0.00,numbers:[22,23,25,26]},
            {name:"split4_23", bet:0.00,numbers:[23,24,26,27]},
            {name:"split4_25", bet:0.00,numbers:[25,26,28,29]},
            {name:"split4_26", bet:0.00,numbers:[26,27,29,30]},
            {name:"split4_28", bet:0.00,numbers:[28,29,31,32]},
            {name:"split4_29", bet:0.00,numbers:[29,30,32,33]},
            {name:"split4_31", bet:0.00,numbers:[31,32,34,35]},
            {name:"split4_32", bet:0.00,numbers:[32,33,35,36]},

            //0-spiel:
            {name:"jeuZ",      bet:0.00,numbers:[0,3,12,15,26,32,35],special:["split0_3", "split2H_12", "split2H_32", "num_26"]},
            //serie 0/2/3:
            {name:"voisins",   bet:0.00,numbers:[0,3,2,4,7,12,15,18,21,19,22,25,26,28,29,32,35],special:["split0_5", "split0_5", "split2H_4", "split2H_12", "split2H_18", "split2H_19", "split2H_32", "split4_25", "split4_25"]},
            //orphelins:
            {name:"orphelins", bet:0.00,numbers:[1,6,9,14,17,20,31,34],special:["num_1", "split2H_6", "split2H_14", "split2H_17", "split2H_31"]},
            //serie 5/8:
            {name:"tiers",     bet:0.00,numbers:[5,8,11,10,13,16,23,24,27,30,33,36],special:["split2H_5", "split2V_10", "split2H_13", "split2V_23", "split2H_27", "split2H_33"]},

            {name:"street_0",  bet:0.00,numbers:[1,2,3],special:["num_1", "num_2", "num_3"]},
            {name:"street_1",  bet:0.00,numbers:[4,5,6],special:["num_4", "num_5", "num_6"]},
            {name:"street_2",  bet:0.00,numbers:[7,8,9],special:["num_7", "num_8", "num_9"]},
            {name:"street_3",  bet:0.00,numbers:[10,11,12],special:["num_10", "num_11", "num_12"]},
            {name:"street_4",  bet:0.00,numbers:[13,14,15],special:["num_13", "num_14", "num_15"]},
            {name:"street_5",  bet:0.00,numbers:[16,17,18],special:["num_16", "num_17", "num_18"]},
            {name:"street_6",  bet:0.00,numbers:[19,20,21],special:["num_19", "num_20", "num_21"]},
            {name:"street_7",  bet:0.00,numbers:[22,23,24],special:["num_22", "num_23", "num_24"]},
            {name:"street_8",  bet:0.00,numbers:[25,26,27],special:["num_25", "num_26", "num_27"]},
            {name:"street_9",  bet:0.00,numbers:[28,29,30],special:["num_28", "num_29", "num_30"]},
            {name:"street_10", bet:0.00,numbers:[31,32,33],special:["num_31", "num_32", "num_33"]},
            {name:"street_11", bet:0.00,numbers:[34,35,36],special:["num_34", "num_35", "num_36"]},

            {name:"line_0",  bet:0.00,numbers:[1,2,3,4,5,6],special:["num_1", "num_2", "num_3","num_4", "num_5", "num_6"]},
            {name:"line_1",  bet:0.00,numbers:[4,5,6,7,8,9],special:["num_4", "num_5", "num_6","num_7", "num_8", "num_9"]},
            {name:"line_2",  bet:0.00,numbers:[7,8,9,10,11,12],special:["num_7", "num_8", "num_9","num_10", "num_11", "num_12"]},
            {name:"line_3",  bet:0.00,numbers:[10,11,12,13,14,15],special:["num_10", "num_11", "num_12","num_13", "num_14", "num_15"]},
            {name:"line_4",  bet:0.00,numbers:[13,14,15,16,17,18],special:["num_13", "num_14", "num_15","num_16", "num_17", "num_18"]},
            {name:"line_5",  bet:0.00,numbers:[16,17,18,19,20,21],special:["num_16", "num_17", "num_18","num_19", "num_20", "num_21"]},
            {name:"line_6",  bet:0.00,numbers:[19,20,21,22,23,24],special:["num_19", "num_20", "num_21","num_22", "num_23", "num_24"]},
            {name:"line_7",  bet:0.00,numbers:[22,23,24,25,26,27],special:["num_22", "num_23", "num_24","num_25", "num_26", "num_27"]},
            {name:"line_8",  bet:0.00,numbers:[25,26,27,28,29,30],special:["num_25", "num_26", "num_27","num_28", "num_29", "num_30"]},
            {name:"line_9", bet:0.00,numbers:[28,29,30,31,32,33],special:["num_28", "num_29", "num_30","num_31", "num_32", "num_33"]},
            {name:"line_10", bet:0.00,numbers:[31,32,33,34,35,36],special:["num_31", "num_32", "num_33","num_34", "num_35", "num_36"]},

            // finals
            {name:"finals_0",  bet:0.00,numbers:[0,10,20,30],special:["num_0", "num_10", "num_20","num_30"]},
            {name:"finals_1",  bet:0.00,numbers:[1,11,21,31],special:["num_1", "num_11", "num_21","num_31"]},
            {name:"finals_2",  bet:0.00,numbers:[2,12,22,32],special:["num_2", "num_12", "num_22","num_32"]},
            {name:"finals_3",  bet:0.00,numbers:[3,13,23,33],special:["num_3", "num_13", "num_23","num_33"]},
            {name:"finals_4",  bet:0.00,numbers:[4,14,24,34],special:["num_4", "num_14", "num_24","num_34"]},
            {name:"finals_5",  bet:0.00,numbers:[5,15,25,35],special:["num_5", "num_15", "num_25","num_35"]},
            {name:"finals_6",  bet:0.00,numbers:[6,16,26,36],special:["num_6", "num_16", "num_26","num_36"]},
            {name:"finals_7",  bet:0.00,numbers:[7,17,27],special:["num_7", "num_17", "num_27"]},
            {name:"finals_8",  bet:0.00,numbers:[8,18,28],special:["num_8", "num_18", "num_28"]},
            {name:"finals_9",  bet:0.00,numbers:[9,19,29],special:["num_9", "num_19", "num_29"]},

            // 4 neighbours:
            {name:"neighbours_0",  bet:0.00,numbers:[3,26,0,32,15],special:["num_3", "num_26", "num_0", "num_32", "num_15"]},
            {name:"neighbours_1",  bet:0.00,numbers:[16,33,1,20,14],special:["num_16", "num_33", "num_1", "num_20", "num_14"]},
            {name:"neighbours_2",  bet:0.00,numbers:[4,21,2,25,17],special:["num_4", "num_21", "num_2", "num_25", "num_17"]},
            {name:"neighbours_3",  bet:0.00,numbers:[12,35,3,26,0],special:["num_12", "num_35", "num_3", "num_26", "num_0"]},
            {name:"neighbours_4",  bet:0.00,numbers:[15,19,4,21,2],special:["num_15", "num_19", "num_4", "num_21", "num_2"]},
            {name:"neighbours_5",  bet:0.00,numbers:[23,10,5,24,16],special:["num_23", "num_10", "num_5", "num_24", "num_16"]},
            {name:"neighbours_6",  bet:0.00,numbers:[17,34,6,27,13],special:["num_17", "num_34", "num_6", "num_27", "num_13"]},
            {name:"neighbours_7",  bet:0.00,numbers:[18,29,7,28,12],special:["num_18", "num_29", "num_7", "num_28", "num_12"]},
            {name:"neighbours_8",  bet:0.00,numbers:[11,30,8,23,10],special:["num_11", "num_30", "num_8", "num_23", "num_10"]},
            {name:"neighbours_9",  bet:0.00,numbers:[14,31,9,22,18],special:["num_14", "num_31", "num_9", "num_22", "num_18"]},
            {name:"neighbours_10", bet:0.00,numbers:[8,23,10,5,24],special:["num_8", "num_23", "num_10", "num_5", "num_24"]},
            {name:"neighbours_11", bet:0.00,numbers:[13,36,11,30,8],special:["num_13", "num_36", "num_11", "num_30", "num_8"]},
            {name:"neighbours_12", bet:0.00,numbers:[7,28,12,35,3],special:["num_7", "num_28", "num_12", "num_35", "num_3"]},
            {name:"neighbours_13", bet:0.00,numbers:[6,27,13,36,11],special:["num_6", "num_27", "num_13", "num_36", "num_11"]},
            {name:"neighbours_14", bet:0.00,numbers:[1,20,14,31,9],special:["num_1", "num_20", "num_14", "num_31", "num_9"]},
            {name:"neighbours_15", bet:0.00,numbers:[0,32,15,19,4],special:["num_0", "num_32", "num_15", "num_19", "num_4"]},
            {name:"neighbours_16", bet:0.00,numbers:[5,24,16,33,1],special:["num_5", "num_24", "num_16", "num_33", "num_1"]},
            {name:"neighbours_17", bet:0.00,numbers:[2,25,17,34,6],special:["num_2", "num_25", "num_17", "num_34", "num_6"]},
            {name:"neighbours_18", bet:0.00,numbers:[9,22,18,29,7],special:["num_9", "num_22", "num_18", "num_29", "num_7"]},
            {name:"neighbours_19", bet:0.00,numbers:[32,15,19,4,21],special:["num_32", "num_15", "num_19", "num_4", "num_21"]},
            {name:"neighbours_20", bet:0.00,numbers:[33,1,20,14,31],special:["num_33", "num_1", "num_20", "num_14", "num_31"]},
            {name:"neighbours_21", bet:0.00,numbers:[19,4,21,2,25],special:["num_19", "num_4", "num_21", "num_2", "num_25"]},
            {name:"neighbours_22", bet:0.00,numbers:[31,9,22,18,29],special:["num_31", "num_9", "num_22", "num_18", "num_29"]},
            {name:"neighbours_23", bet:0.00,numbers:[30,8,23,10,5],special:["num_30", "num_8", "num_23", "num_10", "num_5"]},
            {name:"neighbours_24", bet:0.00,numbers:[10,5,24,16,33],special:["num_10", "num_5", "num_24", "num_16", "num_33"]},
            {name:"neighbours_25", bet:0.00,numbers:[21,2,25,17,34],special:["num_21", "num_2", "num_25", "num_17", "num_34"]},
            {name:"neighbours_26", bet:0.00,numbers:[35,3,26,0,32],special:["num_35", "num_3", "num_26", "num_0", "num_32"]},
            {name:"neighbours_27", bet:0.00,numbers:[34,6,27,13,36],special:["num_34", "num_6", "num_27", "num_13", "num_36"]},
            {name:"neighbours_28", bet:0.00,numbers:[29,7,28,12,35],special:["num_29", "num_7", "num_28", "num_12", "num_35"]},
            {name:"neighbours_29", bet:0.00,numbers:[22,18,29,7,28],special:["num_22", "num_18", "num_29", "num_7", "num_28"]},
            {name:"neighbours_30", bet:0.00,numbers:[36,11,30,8,23],special:["num_36", "num_11", "num_30", "num_8", "num_23"]},
            {name:"neighbours_31", bet:0.00,numbers:[20,14,31,9,22],special:["num_20", "num_14", "num_31", "num_9", "num_22"]},
            {name:"neighbours_32", bet:0.00,numbers:[26,0,32,15,19],special:["num_26", "num_0", "num_32", "num_15", "num_19"]},
            {name:"neighbours_33", bet:0.00,numbers:[24,16,33,1,20],special:["num_24", "num_16", "num_33", "num_1", "num_20"]},
            {name:"neighbours_34", bet:0.00,numbers:[25,17,34,6,27],special:["num_25", "num_17", "num_34", "num_6", "num_27"]},
            {name:"neighbours_35", bet:0.00,numbers:[28,12,35,3,26],special:["num_28", "num_12", "num_35", "num_3", "num_26"]},
            {name:"neighbours_36", bet:0.00,numbers:[27,13,36,11,30],special:["num_27", "num_13", "num_36", "num_11", "num_30"]}
        ];

    alteastream.TableMatrix = TableMatrix;
})();