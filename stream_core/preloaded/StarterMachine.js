
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var StarterMachine = function(locationSearch,gameId) {
        this.ABSStarter_constructor(locationSearch,gameId);
        this.initialize_StarterMachine();
    };
    var p = createjs.extend(StarterMachine, alteastream.ABSStarter);

// static properties:
// public properties:
// private properties:
// static methods:

// constructor:
    p.initialize_StarterMachine = function() {
    };

// public methods:
    p.pingServer_super = p.pingServer;
    p.pingServer = function(){
        window.doLoad();// novo za invalid session
        this.pingServer_super();
        alteastream.AbstractScene.SERVER_URL = this._serverUrl;
        alteastream.AbstractScene.GAME_TYPE = this._gameCode;
        console.log("game code:: "+this._gameCode);
    };

    alteastream.StarterMachine = createjs.promote(StarterMachine,"ABSStarter");
})();