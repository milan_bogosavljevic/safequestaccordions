

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var MouseSubject = function() {
        this.initialize_MouseSubject();
    };
    var p = MouseSubject.prototype;

// static properties:
// public properties:
// private properties:

// constructor:
    p.initialize_MouseSubject = function() {
        this.observers = [];
    };

// static methods:
// public methods:
    p.subscribe = function(observer){
        this.observers.push(observer);
    };

    p.subscribeToAll = function(observers){

    };

    p.unsubscribe = function(observer){
        var index = this.observers.indexOf(observer);
        if(index > -1) {
            this.observers.splice(index, 1);
        }
    };

    p.unsubscribeAll = function(){
        this.observers = [];
    };

    p.notifyObservers = function(type,subject){
        if(this.observers.length>0)
        for(var i = 0,j = this.observers.length;i<j;i++){
            var observer = this.observers[i].props;
            if(observer.eventType === type)
                observer.notify(subject);
        }
    };

    alteastream.MouseSubject = MouseSubject;
})();