// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var MouseWheel = function() {
        this.initialize_MouseWheel();
    };
    var p = MouseWheel.prototype;

// static properties:
// events:
// public properties:
// private properties:
    p._enabled = false;
    p._target = null;
    var that = null;

// constructor:

    p.initialize_MouseWheel = function() {
        that = this;
    };

// static methods:
    MouseWheel.getInstance = function(){
        return that;
    };

// public methods:
    p.enable = function(bool){
        var canvas = document.getElementById("screen");
        var listener = bool === true? "addEventListener": "removeEventListener";
        canvas[listener]("mousewheel", that._onMouseWheel, {passive: true});
        canvas[listener]("DOMMouseScroll", that._onMouseWheel, {passive: true});
        this._enabled = bool;
        console.log("MouseWheel enabled:: "+bool);
    };

    p.setTarget = function(target) {
        this._target = target;
    };

/*    p.setUpDownHandlers = function(up,down){
        p._rollUp = up || this._rollUp;
        p._rollDown = down || this._rollDown;
        if(!this._enabled)
            this.enable(true);
    };*/

// private methods:
/*    p._onMouseWheel = function(e){
        if(Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))>0){
            that._rollUp();
        }
        else{
            that._rollDown();
        }
    };*/

    p._onMouseWheel = function(e){
        if(Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)))>0){
            that._target.onMouseWheel(1);
        }
        else{
            that._target.onMouseWheel(-1);
        }
    };

/*    p._rollUp = function(){
        throw "MouseWheel: No handler defined";
    };

    p._rollDown = function(){
        throw "MouseWheel: No handler defined";
    };*/

    alteastream.MouseWheel = MouseWheel;
})();