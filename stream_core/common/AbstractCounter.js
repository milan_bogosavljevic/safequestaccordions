
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var AbstractCounter = function(){
        this.Container_constructor();
        this.initAbstractCounter();
    };

    var p = createjs.extend(AbstractCounter,createjs.Container);

    p.counterInterval = null;
    p.counterCircle = null;
    p.counterCircleBackground = null;
    p.strokeTickness = null;
    p.counterRadius = null;
    p.colors = null;

    var _this;

    p.initAbstractCounter = function () {
        console.log("AbstractCounter initialized:::");
        _this = this;
        this.setLayout();
    };

    AbstractCounter.getInstance = function() {
        return _this;
    };

    p.show = function(bool){
        this.visible = bool;
    };

    p.update = function(waitTime){
        this.clearCounterInterval();
        var machineReadyTime = waitTime || 10;
        //var fullCircle = circleDegree || 360;
        var fullCircle = this.degree;
        var slice = (fullCircle/machineReadyTime)/fullCircle;

        var fillPercent = 1;
        this.counterInterval = setInterval(function () {
            machineReadyTime--;
            fillPercent -= slice;

            this._updateCounter(machineReadyTime);

            var fill = Math.round(fillPercent * fullCircle);
            var color = machineReadyTime>2?this.colors[0]:this.colors[1];
            this.counterCircle.graphics.clear().setStrokeStyle(this.strokeTickness).beginStroke(color).arc(0, 0, this.counterRadius, 0, (Math.PI/180)*fill).endStroke();
        }.bind(this),1000);
    };

    //new socket
    p._updateCounter = function(count){
        if(count>-1)
            this.confirmEnterCounter.text = count;
        else
            this.clearCounterInterval();
    };

    p.clearCounterInterval = function(){
        clearInterval(this.counterInterval);
        this.confirmEnterCounter.text = 0;
        this.counterCircle.graphics.clear();
    };

    p._createText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.addChild(textInstance);
    };

    alteastream.AbstractCounter = createjs.promote(AbstractCounter,"Container");
})();