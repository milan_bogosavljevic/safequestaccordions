
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Chip = function(amount,tag,multiplier) {
        this.Container_constructor();
        this.initialize_Chip(amount,tag,multiplier);
    };
    var p = createjs.extend(Chip, createjs.Container);
    // private properties:
    p._frontImage = null;
    p._backImage = null;
    p._frontFont = null;
    p._backFont = null;
    p._prizeImage = null;
    p._prizeAmount = null;
    p._moneyAmount = null;
    p._coinsAmount = null;
    p._isEur = null;
    //p._contrastFontsTags = [4,7,8,10,12];// with new chips
    // public properties:
    p.backScaleXForAnimation = 1;// chipovi za info u mobile verziji se scaliraju  tako da mora da postoji odvojen scaling za flip animaciju
                                 // tj. za mobile info cipove flip animacija scalira X od 0 do 0.68 , a za win na mobile , win na desktop, info na desktop scalira X od 0 do 1

    // static properties:
    // events:
    // constructor:
    p.initialize_Chip = function(amount,tag,multiplier) {
        this._init(amount,tag,multiplier);
    };

    // static methods:
    // private methods:
    p._init = function(amount,tag,multiplier){
        this.tagId = Number(tag);// with new chips

        var frontFont;

        if(amount > 99){
            frontFont = this._isMobile() === "not" ? "bold 24px HurmeGeometricSans3" : "bold 18px HurmeGeometricSans3";
            if(amount > 999){
                frontFont = this._isMobile() === "not" ? "bold 22px HurmeGeometricSans3" : "bold 16px HurmeGeometricSans3";
            }
        }else{
            frontFont = this._isMobile() === "not" ? "bold 28px HurmeGeometricSans3" : "bold 20px HurmeGeometricSans3";
        }

        //var fontBig = this._frontFont = this._isMobile() === "not" ? "bold 28px HurmeGeometricSans3" : "bold 20px HurmeGeometricSans3";
        this._frontFont = frontFont;
        this._backFont = this._isMobile() === "not" ? "bold 28px HurmeGeometricSans3" : "bold 20px HurmeGeometricSans3";

        var alignCenter = "center";
        var alignBaseline = "middle";

        var chipFront = "chip_"+tag+"_back";//new
        var chipBack = "chip_"+tag;//new
        var prizeImage = this._prizeImage = alteastream.Assets.getImage(alteastream.Assets.images[chipFront]);

        //var fontColor = this._frontFontColor = "#FFFFFF";//new
        //this._backFontColor = "#000000";//new
        var fontColor = this._frontFontColor = "#000000";//new
        this._frontImage = alteastream.Assets.getImage(alteastream.Assets.images[chipFront]);
        this._backImage = alteastream.Assets.getImage(alteastream.Assets.images[chipBack]);

        this.regX = prizeImage.image.width*0.5;
        this.regY = prizeImage.image.height*0.5;

        this._coinsAmount = amount;
        this._moneyAmount = (amount * multiplier).toFixed(2);

        var prizeAmount = this._prizeAmount = new createjs.Text(String(amount), frontFont, fontColor).set({textBaseline:alignBaseline,textAlign:alignCenter,alpha:0.8});
        prizeAmount.x = this.regX;
        prizeAmount.y = this.regY;
        this.addChild(prizeImage,prizeAmount);
    };

    p._isMobile = function() {
        return localStorage.getItem("isMobile");
    };

    p._updateProps = function(isEur) {
        if(isEur === true){
            this._prizeImage.image = this._backImage.image;
            this._prizeAmount.font = this._frontFont;
            this._prizeAmount.text = this._moneyAmount;
            //this._prizeAmount.color = this._backFontColor;//new
        }else{
            this._prizeImage.image = this._frontImage.image;
            this._prizeAmount.font = this._backFont;
            this._prizeAmount.text = this._coinsAmount;
            //this._prizeAmount.color = this._contrastFontsTags.indexOf(this.tagId)>-1? this._backFontColor: this._frontFontColor//new
        }
        this._prizeAmount.y = isEur === true? this.regY-10: this.regY;//new
    };

    // public methods:
    p.dispose = function(){
        this.removeAllChildren();
    };
    p.getSize = function(){
        return this._prizeImage.image.width;
    };

    p.updateChip = function(isEur, doAnimation) {
        if(this._isEur !== isEur){
            this._isEur = isEur;
            var method = doAnimation === true ? "flipCoin" : "_updateProps";
            this[method](isEur);
        }
    };

    p.flipCoin = function(isEur) {
        var _this = this;
        createjs.Tween.get(this).to({scaleX:0},100, createjs.Ease.quintInOut).call(function () {
            _this._updateProps(isEur);
            createjs.Tween.get(_this).to({scaleX:_this.backScaleXForAnimation},100,createjs.Ease.quintInOut);
        });
    };

    alteastream.Chip = createjs.promote(Chip,"Container");
})();