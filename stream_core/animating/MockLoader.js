
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MockLoader = function(passedAnim){
        this.Container_constructor();
        this.initMockLoader(passedAnim);
    };

    var p = createjs.extend(MockLoader,createjs.Container);


    p.preloadInfoText = null;
    p.currentAnimation = null;
    p.active = false;
    p._defaultAnimation = true;

    p.initMockLoader = function (passedAnim) {
        if(passedAnim)
            this._defaultAnimation = false;

        this._createDynamicText("preloadInfoText","","bold 22px HurmeGeometricSans3","#FFFFFF",{x:0,y:0,textAlign:"center", textBaseline:"middle"});

        var currentAnimation = this.currentAnimation = this._defaultAnimation===true? alteastream.Assets.getImage(alteastream.Assets.images.spinner): passedAnim;
        this.addChild(currentAnimation);
        this._centerPivot();
        currentAnimation.x = 0;
        //currentAnimation.y = 90;
        var topMarging = 30;
        var spacing = 30;
        currentAnimation.y = this.currentAnimation.regY+topMarging;
        this.preloadInfoText.y = currentAnimation.y-(this._getDimensions().height*0.5)-spacing;
        currentAnimation.mouseEnabled = false;

        this.runPreload(false);
    };

    p.customScaleGfx = function(scale) {
        this.currentAnimation.scale = scale;
    };

    p.customGfxPosition = function(x,y) {
        this.currentAnimation.x = x;
        this.currentAnimation.y = y;
    };

    p.customFont = function(font, align) {
        this.preloadInfoText.font =  font;
        this.preloadInfoText.textAlign = align;
    };

    p.setLabel = function(label,newColor){
        this.preloadInfoText.text = label;
        this.preloadInfoText.color = newColor || "#ffffff";
    };

    p.runPreload = function(bool){
        this.preloadInfoText.visible = bool;
        this.currentAnimation.visible = bool;
        this.active = bool;
        if(bool){
            if(this._defaultAnimation){
                this.currentAnimation.on("tick",function(){this.rotation+=5;});
            }else{
                this.currentAnimation.animate(true);
            }
        }

        else{
            if(this._defaultAnimation){
                this.currentAnimation.removeAllEventListeners("tick");
            }else{
                this.currentAnimation.animate(false);
            }
        }
    };

    p._centerPivot = function(){
        if(this._defaultAnimation === true){
            this.currentAnimation.regX = this.currentAnimation.image.width*0.5;
            this.currentAnimation.regY = this.currentAnimation.image.height*0.5;
        }
    };

    p._getDimensions = function(){
        var dimensions = {};
        if(this._defaultAnimation === true){
            dimensions.width = this.currentAnimation.image.width;
            dimensions.height = this.currentAnimation.image.height;
        }else{
            dimensions.width = this.currentAnimation.getWidth();
            dimensions.height = this.currentAnimation.getHeight();
        }
        return dimensions;
    };

    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.addChild(textInstance);
    };

    alteastream.MockLoader = createjs.promote(MockLoader,"Container");
})();


