/**
 * Created by Dacha on 9/25/14.
 */

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var BetTotal = function(scene) {
        this.initialize(scene);
    };
    var p = BetTotal.prototype = new alteastream.AbstractBet();

// static properties:
// events:
// public properties:
// private properties:
// constructor:
    p.BetTotal_init = p.initialize;

    p.initialize = function(scene) {
        this.BetTotal_init(scene);
    };

// static methods:
// public methods:
    p.onStakeChange = function(){
        //this.scene.controlBoard.setStakeAmount(this.scene._stakeIncrements[this.scene._selectedStake]);
        var stake = this.scene._stakeIncrements[this.scene._selectedStake];
        var multiplier = this.getMultiplier();
        this.scene.controlBoard.setStakeAmount(stake,multiplier);
    };

    p.manageStakeButtons = function(){
        var controlBoard = this.scene.controlBoard;
        var stake = this.scene._selectedStake;

        if(stake === this.scene._stakeIncrements.length - 1){
            controlBoard.btnIncStake.setDisabled(true);
            controlBoard.btnDecStake.setDisabled(false);
        }
        else if(stake === 1){
            controlBoard.btnDecStake.setDisabled(false);
            controlBoard.btnIncStake.setDisabled(false);
        }
        else if(stake === 0){
            controlBoard.btnDecStake.setDisabled(true);
            controlBoard.btnIncStake.setDisabled(false);
        }
        else if(stake === this.scene._stakeIncrements.length - 2){
            controlBoard.btnIncStake.setDisabled(false);
            controlBoard.btnDecStake.setDisabled(false);
        }else{
            controlBoard.btnDecStake.setDisabled(false);
            controlBoard.btnIncStake.setDisabled(false);
        }
        controlBoard.setSingleToBurst(stake>0);
    };

    p.betValue = function(){
        //return this.scene.controlBoard.getStakeAmount();
        return this.scene.getSelectedStake();//Math.min(this.scene.getSelectedStake()*multiplier).toFixed(2) //  coini ili lova da se salje?
    };

    p.canBet = function(have,trying){
        have = have/1000;
        return have >= Math.min(trying*this.getMultiplier()).toFixed(2);
    };

    p.betValueToSend =  function(){
        return this.betValue();
    };

// private methods:

    alteastream.BetTotal = BetTotal;
})();