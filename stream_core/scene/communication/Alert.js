/**
 * Created by Dacha on 11.04.14.
 */

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Alert = function(type,exception) {
        this.Container_constructor();
        this.initialize_Alert(type,exception);
    };
    var p = createjs.extend(Alert, createjs.Container);

// static properties:
    Alert.INFO = 0;
    Alert.WARNING = 1;
    Alert.ERROR = 2;
    Alert.EXCEPTION = 3;

// events:
// public properties:
// private properties:
    p.exceptionObject = null;

// constructor:

    p.initialize_Alert = function(type,exception) {
        this._init(type,exception);
    };
// static methods:
// public methods:
    p.dispose = function(){
        this.removeAllChildren();
    };

// private methods:
    p._init = function(type,exception){
        var setType = 0;
        var strokeColor = "#fff000";
        var typeColor = "#ff0000";
        //var sound = "error1";
        var sound = "error";
        switch(type){
            case Alert.INFO:
                setType = "INFO";
                strokeColor = "#97e5bf";
                typeColor = "#9cd4e5";
                sound = "info";
                break;
            case Alert.EXCEPTION:
                setType = "EXCEPTION";
                strokeColor = "#97e5bf";
                typeColor = "#ffb753";
                sound = "exception";
                break;
            case Alert.WARNING:
                setType = "WARNING";
                typeColor = "#ffb753";
                sound = "warning";
                break;
            case Alert.ERROR:
                setType = "ERROR";
                break;
           }

        this.exceptionObject = {};
        this.exceptionObject.method = setType;//TEMP
        this.exceptionObject.message = exception;

        var fontBig = "bold 20px Verdana";
        var font = "18px Verdana";
        var black = "#000";

        var alignCenter = "center";
        var alignBaseline = "middle";
        var exceptionMethod = this.exceptionObject.method || "message undefined";
        var exceptionMessage = this.exceptionObject.message || "message undefined";

        var _width = alteastream.AbstractScene.GAME_WIDTH || document.getElementById("screen").width;
        var _height = alteastream.AbstractScene.GAME_HEIGHT || document.getElementById("screen").height;

        var bg = new createjs.Shape(new createjs.Graphics().beginFill(black).drawRect(0, 0, _width, _height));
        bg.alpha = 0.6;
        bg.cache(0, 0, _width, _height);

        var infoFieldWidth = 570;
        var infoFieldHeight = 54;
        var infoFieldX = _width/2 - infoFieldWidth/2;
        var infoFieldY = _height/2;

        var infoField = new createjs.Shape(new createjs.Graphics().setStrokeStyle(3).beginStroke(strokeColor).beginFill(black).drawRect(infoFieldX, infoFieldY, infoFieldWidth, infoFieldHeight));
        infoField.cache(infoFieldX, infoFieldY, infoFieldWidth, infoFieldHeight);

        var methodTf = new createjs.Text(String(exceptionMethod), fontBig, typeColor).set({textBaseline:alignBaseline,textAlign:alignCenter,mouseEnabled:false});
        var messageTf = new createjs.Text(String(exceptionMessage), font, strokeColor).set({textBaseline:alignBaseline,textAlign:alignCenter,mouseEnabled:false});
        this.addChild(bg,infoField,methodTf,messageTf);

        methodTf.x = (infoFieldX+infoFieldWidth/2);
        methodTf.y = (infoFieldY+infoFieldHeight/2) - 9;

        messageTf.x =  (infoFieldX+infoFieldWidth/2);
        messageTf.y = (infoFieldY+infoFieldHeight/2) + 11;

        alteastream.Assets.playSound(sound);

        this.on("click",function (e){});
    };

    alteastream.Alert = createjs.promote(Alert,"Container");
})();