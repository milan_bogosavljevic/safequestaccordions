
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var AbstractInfo = function(scene) {
    this.Container_constructor();
    this.initialize_AbstractInfo(scene);
};
var p = createjs.extend(AbstractInfo, createjs.Container);

// static properties:

// events:
    /**
     * @event infoScreenChanged
     */

// public properties:
    p.scene = null;

// private properties:

// constructor:

    p.initialize_AbstractInfo = function(scene) {
        this.scene = scene;
    };

    p.showInfo = function(){};

// static methods:
// public methods:



    alteastream.AbstractInfo = createjs.promote(AbstractInfo,"Container");
})();