
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var QuitCounter = function(){
        this.AbstractCounter_constructor();
        this.initQuitCounter();
    };

    var p = createjs.extend(QuitCounter,alteastream.AbstractCounter);
    p.thinCircle = null;

    var _this;

    p.initQuitCounter = function () {
        console.log("QuitCounter initialized:::");
        _this = this;
        this.strokeTickness = 19;
        this.counterRadius = 250;
        this.degree = 288;
        this.colors = ["#f9c221", "#ff0000"];
    };

    QuitCounter.getInstance = function() {
        return _this;
    };

    p.setLayout = function(){
        var circleShape = this._circleShape = new createjs.Shape();
        var rad = 320;
        circleShape.graphics.clear().beginFill("#000000").drawCircle(0, 0, rad);
        this.addChild(circleShape);
        circleShape.mouseEnabled = false;
        circleShape.cache(-rad,-rad,rad*2,rad*2);
        circleShape.alpha = 0.7;

        var counterCircleBackground = this.counterCircleBackground = new createjs.Shape();
        counterCircleBackground.graphics.clear().setStrokeStyle(19).beginStroke("#555153").arc(0, 0, 250, 0, (Math.PI/180) * 288).endStroke();
        counterCircleBackground.rotation = -234;
        counterCircleBackground.x = 0;
        counterCircleBackground.y = 0;
        counterCircleBackground.mouseEnabled = false;
        this.addChild(counterCircleBackground);

        var counterCircle = this.counterCircle = new createjs.Shape();
        counterCircle.rotation = -234;
        counterCircle.x = counterCircleBackground.x;
        counterCircle.y = counterCircleBackground.y;
        counterCircle.mouseEnabled = false;
        this.addChild(counterCircle);

        var thinCircle = this.thinCircle = new createjs.Shape();
        thinCircle.graphics.clear().setStrokeStyle(3).beginStroke("#744006").arc(0, 0, 320, 0, (Math.PI/180) * 328).endStroke();
        thinCircle.rotation = -254;
        thinCircle.x = 0;
        thinCircle.y = 0;
        thinCircle.mouseEnabled = false;
        this.addChild(thinCircle);

        this._createText("confirmEnterCounter","0","bold 130px HurmeGeometricSans3", "#ffffff",{x:0,y:0,textAlign:"center",textBaseline:"middle"});//1332
    };

    p.adjustMobile = function(){
        this.strokeTickness = 10;
        this.counterRadius = 145;
        this.degree = 268;

        var rad = 200;
        this._circleShape.uncache();
        this._circleShape.graphics.clear().beginFill("#000000").drawCircle(0, 0, rad);
        this._circleShape.cache(-rad,-rad,rad*2,rad*2);

        this.confirmEnterCounter.font = "bold 65px HurmeGeometricSans3";
        this.confirmEnterCounter.x = 0;
        this.confirmEnterCounter.y = 0;

        var counterCircleBackground = this.counterCircleBackground;
        counterCircleBackground.graphics.clear().setStrokeStyle(10).beginStroke("#555153").arc(0, 0, 145, 0, (Math.PI/180) * this.degree).endStroke();
        counterCircleBackground.rotation = -224;
        counterCircleBackground.x = 0;
        counterCircleBackground.y = 0;

        var counterCircle = this.counterCircle;
        counterCircle.graphics.clear().setStrokeStyle(10).beginStroke("#555153").arc(0, 0, 145, 0, (Math.PI/180) * this.degree).endStroke();
        counterCircle.rotation = -224;
        counterCircle.x = counterCircleBackground.x;
        counterCircle.y = counterCircleBackground.y;

        var thinCircle = this.thinCircle;
        thinCircle.graphics.clear().setStrokeStyle(1).beginStroke("#744006").arc(0, 0, 200, 0, (Math.PI/180) * 328).endStroke();
        thinCircle.rotation = -254;
        thinCircle.x = counterCircleBackground.x;
        thinCircle.y = counterCircleBackground.y;
    };

    p._updateCounter = function(count){
        if(count>-1)
            this.confirmEnterCounter.text = count;
        else{
            this.clearCounterInterval();
            this.doneCallback();
        }
    };

    alteastream.QuitCounter = createjs.promote(QuitCounter,"AbstractCounter");
})();