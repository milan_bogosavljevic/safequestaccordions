
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var Clock = function(scene,x,y) {
    this.Container_constructor();
    this.initialize_Clock(scene,x,y);
};
var p = createjs.extend(Clock, createjs.Container);

// static properties:

// public properties:
    p.parent = null;
    p.timer = null;
    p._clockText = null;

// private properties:

// constructor:
    p.initialize_Clock = function(scene,x,y) {
        this.parent = scene;
        this._clockText = new createjs.Text("00000000","bold 15px Arial","#bbb8ba").set({y:8,textAlign:"center",textBaseline:"middle"});

        var clockBg = new createjs.Shape();
        clockBg.graphics.beginFill("#000000").drawRoundRect(-89, -5, 178, 23, 5);
        clockBg.alpha = 0.7;
        clockBg.cache(-89, -5, 178, 23);
        this.addChild(clockBg);

        this.addChild(this._clockText);
        this.parent.addChild(this);
        this.x = x;
        this.y = y;

        this.visible = false;
        this.mouseEnabled = false;
        this.mouseChildren = false;
    };

    p.show = function(bool){
        if(bool){
            var that = this;
            var startTime = function() {
                that._clockText.text = new Date().toLocaleString();
                that.timer = setTimeout(startTime, 500);
            };
            startTime();
        }else{
            clearTimeout(this.timer);
        }
        this.visible = bool;
    };

// static methods:
// public methods:
    alteastream.Clock = createjs.promote(Clock,"Container");
})();