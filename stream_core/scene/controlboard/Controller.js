/**
 * Created by Dacha on 06-Jul-15.
 */
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var Controller = function (game) {
        this.ABSController_constructor(game);
        this.initialize_Controller();
    };

    var p = createjs.extend(Controller,alteastream.ABSController);


// static properties:


// events:
// public vars:
// private vars:

// constructor:
    p.initialize_Controller = function () {
    };

// static methods:
// public functions:

    p.onShoot = function(game/*,e*/){
        game.sendBtnHandler(/*e*/);
    };

    p.onShooterAction = function(game,e){
        game.moveShooterBtnHandler(e);
    };

    p.onStakeChange = function(game,type){
        game.controlBoard.onKeyboardStakeChange(type);
    };

    p.onToValueViewChange = function(game) {
        game.controlBoard.onBtnBetViewHandler();
    };

    p.onInfo = function(game) {
        game.infoBtnHandler();
    };

    p.onUp = function(game,e){
        if(e !== null)
            game.stopShooterBtnHandler(e);
    };

    // private functions:

    alteastream.Controller = createjs.promote(Controller,"ABSController");
})();