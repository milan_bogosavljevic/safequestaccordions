/**
 * Created by Dacha on 06-Jul-15.
 */
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
    var ABSController = function (game) {
        this.initialize_ABSController(game);
    };

    var p = ABSController.prototype;
    var keyboard = null;
    var GameMessageType = {
        7:"MOVE_SHOOTER_LEFT",
        8:"MOVE_SHOOTER_RIGHT",
        9:"STOP_SHOOTER",
        10:"DISPENSE_TOKENS",
        MOVE_SHOOTER_LEFT:7,
        MOVE_SHOOTER_RIGHT:8,
        STOP_SHOOTER:9,
        DISPENSE_TOKENS:10
    };



// static properties:
    p.KEYCODE_SHOOT = 32;
    p.KEYCODE_LEFT = 37;
    p.KEYCODE_RIGHT = 39;
    p.KEYCODE_BET_UP = 38;
    p.KEYCODE_BET_DOWN = 40;
    p.KEYCODE_TO_VALUE_VIEW = 17;
    p.KEYCODE_INFO = 73;

// events:

// public vars:
// private vars:
    p.shootActive = false;
    p.leftActive = false;
    p.betUpActive = false;
    p.betDownActive = false;
    p.rightActive = false;
    p.toViewValueActive = false;
    p.infoActive = false;
    p.active = false;
    p.twinComponent = null;


// constructor:
    p.initialize_ABSController = function (game) {
        this.game = game;
    };

// static methods:

// public functions:
    p.activate = function(){
        keyboard = this;
        window.parent.addEventListener("keydown",this.keyDown,false);
        window.parent.addEventListener("keyup",this.keyUp,false);
        var canvas = document.getElementById("screen");
        canvas.focus();
        window.addEventListener("click",function(){
            if(document.activeElement !== canvas)
                canvas.focus();
        });
    };

    p.keyDown = function(e) {
        e.preventDefault();
       /* if (!e) {
            var e = window.event;
        }*/
        if(keyboard.game.controlBoard.active)
            return;
        keyboard.active = true;
        var game = keyboard.game;
        var currentType = -1;
        switch(e.keyCode){
            case keyboard.KEYCODE_SHOOT:
                if(!keyboard.shootActive) {
                    keyboard.shootActive = true;
                    currentType = 0;
                    keyboard.onShoot(game/*,GameMessageType.DISPENSE_TOKENS*/);
                }
                break;
            case keyboard.KEYCODE_LEFT:
                if(keyboard.rightActive)
                    return;
                if(!keyboard.leftActive) {
                    keyboard.leftActive = true;
                    currentType = 1;
                    keyboard.onShooterAction(game,GameMessageType.MOVE_SHOOTER_LEFT);
                }
                break;
            case keyboard.KEYCODE_RIGHT:
                if(keyboard.leftActive)
                    return;
                if(!keyboard.rightActive) {
                    keyboard.rightActive = true;
                    currentType = 2;
                    keyboard.onShooterAction(game,GameMessageType.MOVE_SHOOTER_RIGHT);
                }
                break;
            case keyboard.KEYCODE_BET_UP:
                if(keyboard.betUpActive)
                    return;
                if(!keyboard.betDownActive) {
                    keyboard.betUpActive = true;
                    currentType = 3;
                    keyboard.onStakeChange(game,"inc");
                }
                break;
            case keyboard.KEYCODE_BET_DOWN:
                if(keyboard.betDownActive)
                    return;
                if(!keyboard.betUpActive) {
                    keyboard.betDownActive = true;
                    currentType = 4;
                    keyboard.onStakeChange(game,"dec");
                }
                break;
            case keyboard.KEYCODE_TO_VALUE_VIEW:
                if(!keyboard.toViewValueActive) {
                    keyboard.toViewValueActive = true;
                    currentType = 5;
                    keyboard.onToValueViewChange(game);
                }
                break;
            case keyboard.KEYCODE_INFO:
                if(!keyboard.infoActive) {
                    keyboard.infoActive = true;
                    currentType = 6;
                    keyboard.onInfo(game);
                }
                break;
        }
        if(currentType>-1){
            game.controlBoard.blockMouse(true);
            game.controlBoard.highlightCurrent(currentType,true);
        }
    };

    p.keyUp = function(e) {
        e.preventDefault();
        /* if (!e) {
         var e = window.event;
         }*/
        if(keyboard.game.controlBoard.active)
            return;
        keyboard.active = false;
        var game = keyboard.game;
        var currentType = -1;
        var type = null;
        switch(e.keyCode){
            case keyboard.KEYCODE_SHOOT:
                keyboard.shootActive = false;
                currentType = 0;
                break;
            case keyboard.KEYCODE_LEFT:
                if(keyboard.rightActive)
                    return;
                keyboard.leftActive = false;
                currentType = 1;
                type = GameMessageType.STOP_SHOOTER;
                break;
            case keyboard.KEYCODE_RIGHT:
                if(keyboard.leftActive)
                    return;
                keyboard.rightActive = false;
                currentType = 2;
                type = GameMessageType.STOP_SHOOTER;
                break;
            case keyboard.KEYCODE_BET_UP:
                if(keyboard.betDownActive)
                    return;
                keyboard.betUpActive = false;
                currentType = 3;
                break;
            case keyboard.KEYCODE_BET_DOWN:
                if(keyboard.betUpActive)
                    return;
                keyboard.betDownActive = false;
                currentType = 4;
                break;
            case keyboard.KEYCODE_TO_VALUE_VIEW:
                keyboard.toViewValueActive = false;
                currentType = 5;
                break;
            case keyboard.KEYCODE_INFO:
                keyboard.infoActive = false;
                currentType = 6;
                break;
        }
        if(currentType>-1){
            game.controlBoard.blockMouse(false);
            game.controlBoard.highlightCurrent(currentType,false);
        }
        keyboard.onUp(keyboard.game,type);
    };

    p.disableLeftRight = function(bool){
        keyboard.leftActive = bool;
        keyboard.rightActive = bool;
    };

    p.disableUpDown = function(bool){
        keyboard.betUpActive = bool;
        keyboard.betDownActive = bool;
    };

    p.disableShoot = function(bool){
        keyboard.shootActive = bool;
    };

// private functions:

    alteastream.ABSController = ABSController;
})();