
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var LocationMachinesBKP = function(lobby,maxNumberOfMachines) {
    this.Container_constructor();
    this.initialize_LocationMachinesBKP(lobby,maxNumberOfMachines);
};
var p = createjs.extend(LocationMachinesBKP, createjs.Container);

// static properties:
// events:

// public properties:
    p.lobby = null;
    p.isFocused = false;
    p._machinesComponent = null;
    //p._machinesComponentRoolOverOutListener = null;
    p._canScroll = false;
    p._scrollComponent = null;

    p._scrollLevelsByPage = null;
    p._currentScrollLevelByPage = 0;

    p._pageUpButton = null;
    p._pageDownButton = null;

    //p._scrollLevelsByOneRow = null;
    //p._currentScrollLevelByOneRow = 0;

    //p._numOfMachines = 0;
    p._machinesPerPage = 0;

    var that = null;

// private properties:

// constructor:
    p.initialize_LocationMachinesBKP = function(lobby,maxNumberOfMachines) {
        console.log("LocationMachinesBKP initialized:::");
        this.setComponents(lobby,maxNumberOfMachines);
    };

    p.setComponents = function(lobby,maxNumberOfMachines) {
        this.lobby = lobby;
        that = this;

        //this._machinesComponent = new alteastream.MachinesComponent(maxNumberOfMachines);
        this._machinesComponent = new alteastream.MachinesComponent(maxNumberOfMachines, 3, 1);

        this._machinesPerPage = 3; // ako se menja ovo mora da se promeni i _rowsPerPage u machinesComponent

        var maskWidth = this._machinesComponent.getWidth();
        //var contHeight = this._machinesComponent.getHeight();
        var maskHeight = this._machinesComponent.getMaskHeight();

        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(this._machinesComponent._startPosition, this._machinesComponent._startPosition, maskWidth, maskHeight);
        this._machinesComponent.mask = mask;

/*        var machinesComponentBackground = this._machinesComponentBackground = new createjs.Shape();
        machinesComponentBackground.graphics.beginFill("#000000").drawRect(-55, -31, 1579, 945);
        machinesComponentBackground.alpha = 0.6;
        machinesComponentBackground.cache(-55, -31, 1579, 945);
        machinesComponentBackground.visible = false;*/

        /*        var machinesComponentBackground = new createjs.Shape();
                machinesComponentBackground.graphics.beginFill("#e2d631").drawRoundRect(0, 0, contWidth, contHeight, 8);
                machinesComponentBackground.alpha = 1;
                machinesComponentBackground.cache(0,0,contWidth,contHeight);*/

        /*        var machinesComponentRoolOverOutListener = this._machinesComponentRoolOverOutListener = new createjs.Shape();
                machinesComponentRoolOverOutListener.graphics.beginFill("#000000").drawRect(0, 0, contWidth, contHeight);
                machinesComponentRoolOverOutListener.alpha = 0.01;
                machinesComponentRoolOverOutListener.cache(0,0,contWidth,contHeight);*/

        var scrollComponentBackground = this._scrollComponentBackground = new createjs.Shape();
        scrollComponentBackground.graphics.beginFill("#000000").drawRoundRect(-35, 0, 30, maskHeight, 8);
        scrollComponentBackground.alpha = 0.6;
        scrollComponentBackground.cache(-35, 0, 30, maskHeight, 8);
        scrollComponentBackground.visible = false;

        var numOfPages = this._machinesComponent.getNumberOfPages();
        var scrollComponent = this._scrollComponent = new alteastream.ScrollComponent(numOfPages);
        scrollComponent.rotation = 90;
        scrollComponent.x = -12;
        scrollComponent.y = 5;

        var pageUpButton = this._pageUpButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMockUpDown),3);
        pageUpButton.visible = false;
        pageUpButton.setClickHandler(function () {
            if(that._currentScrollLevelByPage > 0){
                that._currentScrollLevelByPage--;
                that._manageButtonsState();
                that.scrollByPage();
            }
        });

        var pageDownbutton = this._pageDownButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMockUpDown),3);
        pageDownbutton.rotation = 180;
        pageDownbutton.visible = false;
        pageDownbutton.setClickHandler(function () {
            if(that._currentScrollLevelByPage < (that._scrollLevelsByPage.length - 1)) {
                that._currentScrollLevelByPage++;
                that._manageButtonsState();
                that.scrollByPage();
            }
        });

        pageUpButton.x = 40;
        pageDownbutton.x = pageUpButton.x + 170;
        pageUpButton.y = 820;
        pageDownbutton.y = pageUpButton.y + 71;

        this._machinesComponent.visible = this._scrollComponent.visible = false;

        var buttonBackTxt = new createjs.Text("Back to Lobby","16px HurmeGeometricSans3","#ffffff").set({textAlign:"left"});
        var backButton = this._backButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBack),3,buttonBackTxt);
        backButton.centerText(64, 4);
        backButton.visible = false;
        backButton.x = -55;
        backButton.y = -95;
        backButton.setClickHandler(function () {
            alteastream.StreamPreview.getInstance().exitPreview();// TODO DODATO , PROVERITI
            that.lobby.backToLobbyFromLocation();
            backButton.visible = false;
        });

        this.addChild(/*machinesComponentBackground, */this._machinesComponent , scrollComponentBackground , this._scrollComponent, pageUpButton, pageDownbutton, backButton/*, machinesComponentRoolOverOutListener*/);

        var bigTent = this._bigTent = alteastream.Assets.getImage(alteastream.Assets.images.house);
        bigTent.regX = bigTent.image.width/2;
        bigTent.regY = bigTent.image.height/2;
        bigTent.startingAnimatioXPos = 680;
        bigTent.startingAnimatioYPos = 330;
        this.addChild(bigTent);

        //this._originalX = this._bigTent.x;
        //this._originalY = this._bigTent.y;

        var _locationLabel = this._locationLabel = new createjs.Text("Mock text","bold 26px HurmeGeometricSans3","#ffffff").set({textAlign:"center", mouseEnabled:false, x:900, y:-26, visible:false});
        var _numOfMachinesLabel = this._numOfMachinesLabel = new createjs.Text("0","bold 20px HurmeGeometricSans3","#ffffff").set({textAlign:"center", mouseEnabled:false, x:120, y:-24, visible:false});
        this.addChild(_locationLabel, _numOfMachinesLabel);

        this.visible = false;
    };

    p._manageButtonsState = function() {
        if(this._currentScrollLevelByPage === 0) {
            this._pageUpButton.setDisabled(true);
            this._pageDownButton.setDisabled(false);
        }else if(this._currentScrollLevelByPage === this._scrollLevelsByPage.length - 1) {
            this._pageDownButton.setDisabled(true);
            this._pageUpButton.setDisabled(false);
        }else{
            if(this._pageDownButton.isDisabled() === true){
                this._pageDownButton.setDisabled(false);
            }
            if(this._pageUpButton.isDisabled() === true){
                this._pageUpButton.setDisabled(false);
            }
        }
    };

/*    p.turnOnEventListeners = function(bool) {
        var addOrRemove = bool === true ? "addEventListener" : "removeEventListener";

        this._machinesComponentRoolOverOutListener[addOrRemove]("rollover",this.onRoll);
        this._machinesComponentRoolOverOutListener[addOrRemove]("rollout",this.onRoll);

        document.getElementById('screen')[addOrRemove]("mousewheel", this.onScroll);
        document.getElementById('screen')[addOrRemove]("DOMMouseScroll", this.onScroll);
    };*/

/*    p.onScroll = function(e) {
        if (that._canScroll) {
            that.doScroll(e.deltaY);
        }
    };*/

/*    p.onRoll = function(e) {
        var canScroll = e.type === "rollover";
        var cursorStyle = canScroll === true ? "ns-resize" : "default";

        that._canScroll = canScroll;
        document.getElementsByTagName("body")[0].style.cursor = cursorStyle;
    };*/

// static methods:
// public methods:
    p.focusIn = function(backToHouse){
        var point = this.lobby.selectedLocation.localToGlobal(0, 0);
        var startReturnPoint = this.startReturnPoint = this.globalToLocal(point.x, point.y);
        this._bigTent.x = startReturnPoint.x;
        this._bigTent.y = startReturnPoint.y;

        this._bigTent.aplha = 1;
        this.visible = true;

        var method = backToHouse === true ? "focusInRegular" : "focusInAnimated";
        this[method]();
        this.lobby.setSwipeTarget(this,"y");
        //this._backButton.visible = true;
    };

    p.focusInRegular = function() {
        this._bigTent.x = this._bigTent.startingAnimatioXPos;//680
        this._bigTent.y = this._bigTent.startingAnimatioYPos;//330
        this._bigTent.scaleX = this._bigTent.scaleY = 5;
        this._bigTent.alpha = 0;
        this.lobby.setFocused(this);
        this.lobby.setBackgroundImagesForStreamPreview(true);
        this.isFocused = true;
        this.setMachinesAndScroll(this.lobby.selectedLocation);
        this.showMachinesHideShape(true);
        this.lobby.intervalUpdateLobby(true);
        this._backButton.visible = true;
       // this.lobby.setSwipeTarget(this,"y");
    };

    p.focusInAnimated = function() {
        TweenLite.to(this._bigTent,0.5,{x:this._bigTent.startingAnimatioXPos,y:this._bigTent.startingAnimatioYPos,scaleX:5,scaleY:5,alpha:0,onComplete:function(that){//680 330
                that.lobby.setFocused(that);
                that.lobby.setBackgroundImagesForStreamPreview(true);
                that.isFocused = true;
                that.setMachinesAndScroll(that.lobby.selectedLocation);
                that.showMachinesHideShape(true);
                that.lobby.intervalUpdateLobby(true);
                that._backButton.visible = true;
                //that.lobby.setSwipeTarget(this,"y");
                //that.lobby.intervalMachinesStatus(true);
            },onCompleteParams:[this]});
    };

    p.onSwipe = function (direction) {
        var _that = this;
        if(direction === 1){
            if(_that._currentScrollLevelByPage > 0){
                _that._currentScrollLevelByPage--;
                _that._manageButtonsState();
                _that.scrollByPage();
            }
        }
       else{
            if(_that._currentScrollLevelByPage < (_that._scrollLevelsByPage.length - 1)) {
                _that._currentScrollLevelByPage++;
                _that._manageButtonsState();
                _that.scrollByPage();
            }
        }

       // that._manageButtonsState();
        //that.scrollByPage();
    };

    p.showMachinesHideShape = function(bool) {
        //this._machinesComponentBackground.visible = bool;
        this._machinesComponent.visible = bool;
        this._scrollComponentBackground.visible = bool;
        this._scrollComponent.visible = bool;
        this._locationLabel.visible = bool;
        this._numOfMachinesLabel.visible = bool;
        this._bigTent.visible = !bool;
        if(this._machinesComponent.getNumberOfPages() > 1){
            this._showButtons(bool);
        }
    };

    p.focusOut = function(){
        this.lobby.intervalUpdateLobby(false);
        this.lobby.streamPreview.exitPreview();
        //this.turnOnEventListeners(false);
        this.showMachinesHideShape(false);
        this._bigTent.alpha = 0;

        TweenLite.to(this._bigTent,0.5,{x:this.startReturnPoint.x,y:this.startReturnPoint.y,scaleX:1,scaleY:1,alpha:1,onComplete:function(that){
                that.isFocused = false;
                that._clearAndReset();
                that.lobby.currentMap.carousel.showChosenHouse(true);
            },onCompleteParams:[this]});
    };

    p.setMachinesAndScroll = function(selectedLocation) {
        this._machinesComponent.resetComponent(selectedLocation);
        this._updateLabel(selectedLocation.casinoName, this._machinesComponent.getNumberOfActiveMachines());
        //this._numOfMachines = selectedLocation.machines.length;
        var numOfPages = this._machinesComponent.getNumberOfPages();
        var contHeight = this._machinesComponent.getHeight();
        //var machineHeight = this._machinesComponent.getMachineHeight();

        this._currentScrollLevelByPage = 0;
        this._scrollLevelsByPage = [];
        for (var i = 0; i < numOfPages; i++) {
            this._scrollLevelsByPage.push(i * contHeight * -1);
        }

/*        this._currentScrollLevelByOneRow = 0;
        this._scrollLevelsByOneRow = [];
        for(var j = 0; j < this._numOfMachines; j++){
            this._scrollLevelsByOneRow.push(j * machineHeight * -1);
        }*/

        this._scrollComponent.resetComponent(numOfPages);
        this._scrollComponent.moveCurrentDot(this._currentScrollLevelByPage);
        this._machinesComponent.y = 0;

        this._setButtonsStateOnStart(numOfPages);

        this._machinesComponent.getChildAt(0)._onMouseClick();
    };

    p._updateLabel = function(locationName, numOfMachines) {
        this._locationLabel.text = locationName.toUpperCase();
        var moreThanOne = numOfMachines > 1 ? " machines" : " machine";
        this._numOfMachinesLabel.text = numOfMachines + moreThanOne;
    };

    p._setButtonsStateOnStart = function(numOfPages) {
        if(numOfPages > 1){
            this._pageUpButton.setDisabled(true);
            this._pageDownButton.setDisabled(false);
        }
    };

    p._showButtons = function(show) {
        this._pageUpButton.visible = this._pageDownButton.visible = show;
    };

/*    p.doScroll = function (deltaY) {
        if(deltaY > 0){
            var maxScrollLevel = Math.floor(this._numOfMachines/this._machinesComponent.getColumnsPerPage());
            if(this._currentScrollLevelByOneRow < maxScrollLevel){
                this._currentScrollLevelByOneRow++;
            }else{
                return;
            }
        }else{
            if(this._currentScrollLevelByOneRow > 0){
                this._currentScrollLevelByOneRow--;
            }else{
                return;
            }
        }
        this._scrollByRow();
    };*/

/*    p._scrollByRow = function() {
        var y = this._scrollLevelsByOneRow[this._currentScrollLevelByOneRow];
        this.animateScroll(y,300);
        var currentScrollLevel = Math.floor(this._currentScrollLevelByOneRow/this._machinesComponent.getRowsPerPage());

        if(this._currentScrollLevelByPage !== currentScrollLevel){
            this._currentScrollLevelByPage = currentScrollLevel;
            this._scrollComponent.moveCurrentDot(this._currentScrollLevelByPage);
        }
    };*/

    p.scrollByPage = function() {//
        var y = this._scrollLevelsByPage[this._currentScrollLevelByPage];
        if(this._currentScrollLevelByPage === (this._scrollLevelsByPage.length - 1)){
            var numOfMachinesThatDoesntFit = this._machinesComponent.getNumberOfActiveMachines()%this._machinesPerPage;
            if(numOfMachinesThatDoesntFit > 0){
                var yPosCorrector = this._machinesPerPage - numOfMachinesThatDoesntFit;
                var distanceToCorrect = yPosCorrector * this._machinesComponent.getMachineHeight();
                //console.log(distanceToCorrect);
                y += distanceToCorrect;
            }
        }

        //Mozda::
        //this._machinesComponent.cacheMachines(true);

        //Milan
        this.animateScroll(y,500);//mozda kesirati pre animacije, lose ide tween sad kad nema bitmapText
        this._scrollComponent.moveCurrentDot(this._currentScrollLevelByPage);// onComplete animacije ovo
    };

    p.animateScroll = function(y,duration) {
        if(createjs.Tween.hasActiveTweens(this._machinesComponent)){
            createjs.Tween.removeTweens(this._machinesComponent);
        }
        //Milan
        createjs.Tween.get(this._machinesComponent).to({y:y}, duration, createjs.Ease.cubicOut);

        //Mozda::
        /*createjs.Tween.get(this._machinesComponent).to({y:y} , duration , createjs.Ease.cubicOut).call(function(){
            that._scrollComponent.moveCurrentDot(that._currentScrollLevelByPage);
            that._machinesComponent.cacheMachines(false);
        });*/
    };

/*    p.onScrollDotClick = function(index) {
        this._currentScrollLevelByPage = index;
        this._currentScrollLevelByOneRow = index * this._machinesComponent.getRowsPerPage();
        this.scrollByPage();
    };*/

/*    p.update = function(status) {
        if(this.isFocused){
            var selectedLocationId = this.lobby.selectedLocation.id;
            for(var i = 0 , n = status.length; i < n; i++){
                if(status[i].locationId === selectedLocationId){
                    this._machinesComponent.updateMachines(status[i].machines);
                    break;
                }
            }
        }
    };*/

// private methods:
    p._clearAndReset = function(){
        this.visible = false;
        this._bigTent.scaleX = this._bigTent.scaleY = 1;
        //this._bigTent.x = this._originalX;
        //this._bigTent.y = this._originalY;
    };

    alteastream.LocationMachinesBKP = createjs.promote(LocationMachinesBKP,"Container");
})();