
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreviewBKPWOWZA = function(){
        this.Container_constructor();
        this.initStreamPreviewBKPWOWZA();
    };

    var p = createjs.extend(StreamPreviewBKPWOWZA,createjs.Container);

    p.startX = 0;
    p.sWidth = 0;
    p.sHeight = 0;
    p.SPACING_LEFT = 8;
    p.SPACING_TOP = 10;
    p.SPACING_OVERLAY = 7;
    p.THUMB_WIDTH = 1264;
    p.THUMB_HEIGHT = 712;
    p.player = null;
    p._thumbNotLoaded = true;
    p.currentStream = "";
    p._active = false;
    p._wowzaPlayerOptions = {
        "license":"PLAY1-nByuW-4enAj-fD37H-9r9kK-cux7u",
        "title":"",
        "description":"",
        "sourceURL":"http%3A%2F%2F84.199.144.157%3A1935%2Flive%2Fmachine2322.stream%2Fplaylist.m3u8",
        "autoPlay":false,
        "mute":true,
        "loop":false,
        "audioOnly":false,
        ///"debugLevel":"OFF",
        "uiShowQuickRewind":false
    };

    var _instance = null;

    p.initStreamPreviewBKPWOWZA = function () {
        _instance = this;
        var videoHolder = this.videoHolder = document.createElement('div');
        videoHolder.id = 'playerElement';
        videoHolder.style.top = 0;
        videoHolder.style.left = 0;
        videoHolder.style.width = 65.5 + "%";
        videoHolder.style.height = 65.5 + "%";
        videoHolder.style.position = "absolute";
        videoHolder.style.backgroundColor = "none";
        videoHolder.style.visibility = "hidden";
        videoHolder.style.border = "none";
        document.getElementById("wrapper").appendChild(videoHolder);

        var overlay = this.overlay = document.createElement('div');
        overlay.id = 'overlay';
        overlay.style.backgroundColor = "rgba(0, 0, 0, 0.01)";
        overlay.style.top = 0;
        overlay.style.left = 0;
        overlay.style.width = 67 + "%";
        overlay.style.height = 67 + "%";
        overlay.style.position = "fixed";
        overlay.style.visibility = "hidden";
        document.getElementById("wrapper").appendChild(overlay);

        var bgShape = this.bgShape = new createjs.Shape();
        bgShape.graphics.beginFill("#000").drawRect(0,0,1382,920);
        bgShape.x = 438;
        bgShape.y = 82;
        bgShape.alpha = 0.85;
        this.addChild(bgShape);
        bgShape.cache(0,0,1380,920);
        bgShape.visible = false;
        bgShape.mouseEnabled = false;

        /*var border = this.border = new createjs.Shape();
        border.graphics.beginStroke("#94fffd").setStrokeStyle(5).drawRect(0,0,1268,800);
        border.x = 498;
        border.y = 150;
        this.addChild(border);
        border.cache(0,0,1268,800);
        border.visible = false;
        border.mouseEnabled = false;*/

        var frame = this.frame = alteastream.Assets.getImage(alteastream.Assets.images.window_big);
        frame.x = 478;
        frame.y = 130;
        this.addChild(frame);
        //frame.visible = false;
        frame.mouseEnabled = false;

        var labelsContainer = this.labelsContainer = new createjs.Container();
        this.addChild(labelsContainer);
        var xPos = 505;
        var xPos2 = 660;
        var font = "15px HurmeGeometricSans3";
        var col_1 = "#c0dbff";
        var col_2 = "#FFF";
        this._createDynamicText("biggestGameWinLabel","BIGGEST GAME WIN: ",font,col_1,{x:xPos,y:865,textAlign:"left"},labelsContainer);
        this._createDynamicText("biggestGameWinValue","",font,col_2,{x:xPos2,y:865,textAlign:"left"},labelsContainer);

        this._createDynamicText("tokensFiredLabel","TOKENS FIRED: ",font,col_1,{x:xPos,y:885,textAlign:"left"},labelsContainer);
        this._createDynamicText("tokensFiredValue","",font,col_2,{x:xPos2,y:885,textAlign:"left"},labelsContainer);

        this._createDynamicText("lastWinLabel","LAST WIN: ",font,col_1,{x:xPos,y:905,textAlign:"left"},labelsContainer);
        this._createDynamicText("lastWinValue","",font,col_2,{x:xPos2,y:905,textAlign:"left"},labelsContainer);

        this._createDynamicText("totalWinLabel","TOTAL WIN: ",font,col_1,{x:xPos,y:925,textAlign:"left"},labelsContainer);
        this._createDynamicText("totalWinValue","",font,col_2,{x:xPos2,y:925,textAlign:"left"},labelsContainer);

        this._createDynamicText("isFreeToPlayLabel","STATUS: ","16px HurmeGeometricSans3",col_1,{x:xPos+550,y:925,textAlign:"left"},labelsContainer);
        this._createDynamicText("isFreeToPlayValue","","16px HurmeGeometricSans3",col_2,{x:xPos2+460,y:925,textAlign:"left"},labelsContainer);
        labelsContainer.visible = false;

        var spinner = this.spinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = 1130;
        spinner.y = 490;
        spinner.mouseEnabled = false;

        var thumbnailBitmap = this.thumbnailBitmap = alteastream.Assets.getImage(alteastream.Assets.images.thumb);
        this.addChild(thumbnailBitmap);
        thumbnailBitmap.x = 500;
        thumbnailBitmap.y = 152;
        thumbnailBitmap.visible = false;
        thumbnailBitmap.mouseEnabled = false;

        var circle = this.circle = new createjs.Shape();
        circle.graphics.beginFill("#000").drawCircle(0,0,100);
        circle.x = 1080+50;
        circle.y = 470+50;
        circle.alpha = 0.5;
        this.addChild(circle);
        circle.cache(-100,-100,200,200);
        circle.visible = false;
        circle.mouseEnabled = false;

        var liveIcon = this.liveIcon = alteastream.Assets.getImage(alteastream.Assets.images.liveIcon);
        this.addChild(liveIcon);
        liveIcon.x = 1080;
        liveIcon.y = 475;
        liveIcon.visible = false;
        liveIcon.mouseEnabled = false;
        this._createDynamicText("enterLiveLabel","WATCH LIVE","bold 22px HurmeGeometricSans3","#FFF",{x:1070,y:560,textAlign:"left",visible:false},this);

        var videoStream = this.videoStream = new createjs.DOMElement(videoHolder);
        this.addChild(videoStream);
        this.videoStream.visible = false;

        this._blockUIOverlay(false);
        this.setDynamicPosition(0.270,0.125);

        this.createPlayer(videoHolder.id, this._wowzaPlayerOptions);

        var buttonExitTxt = new alteastream.BitmapText("BACK", "16px HurmeGeometricSans3", "#fff",{textAlign:"center"});
        var buttonExit = this.buttonExit = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMockSS),3,buttonExitTxt);
        buttonExit.regX = 70;
        buttonExit.regY = 18;
        buttonExit.x = 1690;
        buttonExit.y = 107;
        this.addChild(buttonExit);
        buttonExit.visible = false;
        buttonExit.setClickHandler(function (e) {
            this.exitPreview();
        }.bind(this));

        window.addEventListener("resize",function(){
            _instance._resize();
        });
    };

    StreamPreviewBKPWOWZA.getInstance = function(){
        return _instance;
    };

    p.activate = function(){
        this.player = WowzaPlayer.get(this.videoHolder.id);
    };

    p.start = function(){
        alteastream.Lobby.getInstance().intervalUpdateLobby(false);
        this._active = true;
        this._thumbNotLoaded = true;
        this.focusThumbnail(true);
        this.updateLoungeComponents();
        this.intervalUpdateLounge(true);
    };

    p.focusThumbnail = function(bool){
        this.thumbnailBitmap.visible = false;
        this.enterLiveLabel.visible = false;
        this.liveIcon.visible = false;
        this.circle.visible = false;

        this.spinner.runPreload(false);
        this.spinner.setLabel("Connecting preview");
        this.spinner.runPreload(true);

        this.intervalThumbnail(bool);
    };

    p.intervalThumbnail = function(bool){
        var that = this;
        if(bool === true)
            this.thumbnailPreviewTimer = setInterval(function(){
                that.updatePreviewThumbnail();
            },2000);
        else{
            clearInterval(this.thumbnailPreviewTimer);
            this.thumbnailBitmap.visible = false;
            this.enterLiveLabel.visible = false;
            this.liveIcon.visible = false;
            this.circle.visible = false;
        }
    };

    p.intervalUpdateLounge = function(bool){
        var that = this;
        if(bool === true)
            this.thumbnailPropsTimer = setInterval(function(){
                    that.updateLoungeComponents();
            },2000);
        else
            clearInterval(this.thumbnailPropsTimer);
    };

    p.updateLoungeComponents = function(){
        if(this._active){
            var that = this;
            alteastream.Requester.getInstance().getCurrentMachine(this.currentStream,function(response){that.updateProps(response);})
        }
    };

    p.updatePreviewThumbnail = function(){
        alteastream.Assets.getThumbnail(this.machineInfo.machineName,this.thumbnailBitmap,this.THUMB_WIDTH,this.THUMB_HEIGHT,function(){
            if(this._thumbNotLoaded === true) {
                this._thumbNotLoaded = false;
                this.spinner.runPreload(false);
                this.labelsContainer.visible = true;
                this.buttonExit.visible = true;
                this.thumbnailBitmap.visible = true;
                this.enterLiveLabel.visible = true;
                this.liveIcon.visible = true;
                this.circle.visible = true;
                this._blockUIOverlay(false);
                if (this._active)
                    alteastream.QueuePanel.getInstance().onStreamPreviewBKPWOWZAEnter();
            }
        }.bind(this));
    };

    p.exitPreview = function(){
        if(!this._active)
            return;
        this._active = false;

        alteastream.QueuePanel.getInstance().onStreamPreviewBKPWOWZAExit();
        alteastream.Lobby.getInstance().onStreamPreviewBKPWOWZAExit();

        this._hideWowzaUI(false);
        this.buttonExit.visible = false;
        this.currentStream = "";
        this.show(false);
        this.hidePlayer(true);
        this.destroyPlayer();
        this._blockUIOverlay(false);
        this.focusThumbnail(false);
        this.intervalUpdateLounge(false);
        alteastream.Lobby.getInstance().intervalUpdateLobby(true);
        this.spinner.runPreload(false);
        this.labelsContainer.visible = false;
    };

    p.updateInfo = function(machineInfo){
        this.machineInfo = machineInfo;
        this.currentStream = machineInfo.id;
    };

    p.updateProps = function(machineInfo){
        //console.log("PROPS "+machineInfo.biggestGameWin);
        var i = 0;
        for(var key in machineInfo){
            var prop = Object.keys(machineInfo)[i];
            //this[prop+"Value"].text = machineInfo[key];
            this[prop+"Value"].setText(machineInfo[key]);
            i++;
        }
        //this.isFreeToPlayValue.text = machineInfo.isFreeToPlay === true?"AVAILABLE":"BUSY";
        var avail = machineInfo.isFreeToPlay === true?"AVAILABLE":"BUSY";
        var color = machineInfo.isFreeToPlay === true?"#ffffff":"#ff0000";
        this.isFreeToPlayValue.setText(avail);
        this.isFreeToPlayValue.setColor(color);
    };

    p.changeStream = function(src){
        this._blockUIOverlay(true);
        this.show(true);
        this.bgShape.visible = true;
        //this.border.visible = true;

        this.hidePlayer(false);
        this.destroyPlayer();

        var id = this.videoHolder.id;
        this._wowzaPlayerOptions.sourceURL = src;
        this.createPlayer(id, this._wowzaPlayerOptions);

        this.player = WowzaPlayer.get(id);
        this.playStartedListener();
        this.playStateListener();

        this._hideWowzaContainer(true);
        this._hideWowzaUI(true);
    };

    p.playStateListener = function(){
        var stateIsChanged = function ( stateChangedEvent ) {
            if (stateChangedEvent.currentState === WowzaPlayer.State.PLAYING) {
                _instance.player.removeOnStateChanged (stateIsChanged);
                setTimeout(function(){
                    _instance.spinner.runPreload(false);
                    _instance._hideWowzaContainer(false);
                    _instance.buttonExit.setDisabled(false);
                    alteastream.QueuePanel.getInstance().buttonEnter.setDisabled(false);
                },3000);
            }
        };
        this.player.onStateChanged( stateIsChanged );
    };

    p.playStartedListener = function ( ) {
        var playStarted = function ( ) {
                _instance.player.removeOnPlay(playStarted);
                _instance._blockUIOverlay(true);
                _instance.buttonExit.setDisabled(true);
                alteastream.QueuePanel.getInstance().buttonEnter.setDisabled(true);
                _instance.intervalThumbnail(false);
                _instance.spinner.setLabel("Connecting to machine "+_instance.currentStream);
                _instance.spinner.runPreload(true);
        };
        this.player.onPlay( playStarted );
    };

    p.onQueueEnterResponse = function(){
        this.buttonExit.setDisabled(true);
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p.createPlayer = function(id, options){
        WowzaPlayer.create(id, options);
    };

    p.hidePlayer = function(bool){
        this.videoStream.visible = !bool;
    };

    p.destroyPlayer = function(){
        if(this.player!==null){
            this.player.destroy();
            this.player = null;
        }
    };

    p.dispose = function(){
        this.intervalUpdateLounge(false);
        this.player.finish();
        this.destroyPlayer();
    };

    p.reset = function(){
        this.buttonExit.visible = true;
        this.buttonExit.setDisabled(false);
    };

    p._resize = function(){
        var w = window.innerWidth;
        var h = window.innerHeight;
        var spacingX = this.SPACING_LEFT*(w/1000);
        var spacingY = this.SPACING_TOP*(w/1000);
        var spacingOverlay = this.SPACING_OVERLAY*(w/1000);

        var dynX = this._getDynamicPosition().x;
        var dynY = this._getDynamicPosition().y;

        this.videoStream.x = w*dynX-spacingX;
        this.videoStream.y = h*dynY+spacingY;

        this.overlay.style.left = w*dynX-(spacingX+spacingOverlay)+"px";
        this.overlay.style.top = h*dynY+(spacingY-spacingOverlay)+"px";
    };

    p.setDynamicPosition = function(xPerc,yPerc){
        this._dynamicPositionXY = {x:xPerc,y:yPerc};
        this._resize();
    };

    p._getDynamicPosition = function(){
        return this._dynamicPositionXY;
    };

    p._createDynamicText = function(instance,text,font,color,props,parent){
        //var textInstance = this[instance] = new alteastream.DynamicText(text,font,color).set(props);
        var textInstance = this[instance] = new alteastream.BitmapText(text,font,color,props);
        parent.addChild(textInstance);
    };

    p._blockUIOverlay = function(bool){
        this.overlay.style.visibility = bool === true?"visible":"hidden";
    };
    
    p._hideWowzaUI = function(bool){
        document.getElementById("playerElement-UI").style.opacity = bool === true?0.01:1;
        document.getElementById("playerElement-OverlayPlay").style.visibility = bool === true?"hidden":"visible";
    };
    
    p._hideWowzaContainer = function(bool){
        document.getElementById("playerElement-Container").style.backgroundColor = bool === true?"rgba(0, 0, 0, 0.01)":"#000000";
        document.getElementById("playerElement-Container").style.cursor = bool === true?"auto":"pointer";
    };

    alteastream.StreamPreviewBKPWOWZA = createjs.promote(StreamPreviewBKPWOWZA,"Container");
})();