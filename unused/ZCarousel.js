

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ZCarousel = function (houses) {
        this.Container_constructor();
        this.initialize_ZCarousel(houses);
    };

    var p = createjs.extend(ZCarousel,createjs.Container);
    var _this = null;

// static properties:
// events:
// public vars:
// private vars:
    var radiusX = 880;
    var radiusY = 70;
    var speed = 0;
    var minSpeed = 0;
    var depthZ = 0.25;
    var startScale = 0.75;
    var startAngle = 0;
    var controlAngle = Math.PI/2;
    var matrix = new createjs.Matrix2D();
    var unitsArray = [];
    var animationOver = true;


// constructor:
    p.initialize_ZCarousel = function (houses) {
        _this = this;
        this.name="CAROUSEL";
        this.visible = false;
        this.houses = houses;

        var diff = this.diff = this.degToRad(360/houses.length);
        var faceAngle = houses.length % 2 === 0? 360: 180;
        startAngle = -Math.PI + diff + this.degToRad(faceAngle/houses.length);
        minSpeed = 3;

        this._setup();
        //this.update(diff - controlAngle,true);

        //ABSMAP _onMouseAction mouseover"
        //Lobby addObserver mouseover"
    };

    p.registerUI = function(left,right){
        this.buttonLeft = left;
        this.buttonRight = right;
        this.update(this.diff - controlAngle,true);
    };

    p.hideUI = function(bool){
        this.buttonLeft.visible = !bool;
        this.buttonRight.visible = !bool;
    };

    p.move = function(clockwise){
        this.buttonLeft.mouseEnabled = false;
        this.buttonRight.mouseEnabled = false;
        this.removeHighlight();
        var nextAngle = clockwise===true? controlAngle + this.diff - 0.05: controlAngle - this.diff + 0.05;
        this.renderAnimation(true,nextAngle,clockwise);
    };

    p.onSwipe = function(direction){
        var bool = direction === -1;
        this.move(bool);
    };

    p.degToRad = function(degree) {
        return degree * Math.PI / 180;
    };
// static methods:
// public methods:
    //on renderComponents:
    p.update = function(nextAngle,isClockwise) {
        var speedSet = isClockwise===true? controlAngle < nextAngle: controlAngle > nextAngle;
        if(speedSet){
            speed = minSpeed;
        }
        else{
            speed = 0;
            this.renderAnimation(false);
            this.clicksEnabled(false);
            this.buttonLeft.mouseEnabled = true;
            this.buttonRight.mouseEnabled = true;
/*            var house = this.getChildAt(unitsArray.length-1).getChildAt(0);

            if(house.enabled){
                house.allow = true;
                //house.highlight(true);
            }*/
        }
        var velocity = speed*createjs.Matrix2D.DEG_TO_RAD;
        controlAngle = isClockwise === true? controlAngle+velocity: controlAngle-velocity;

        var posX;
        var posY;
        var posArr = [];
        var depthArray = [];
        var currentAngle = startAngle;
        var slices = Math.PI*2/this.houses.length;

        var nAngle = isClockwise===true? slices: -slices;

        for (var i = 0,j = this.houses.length; i < j; i++){

            currentAngle = isClockwise===true? currentAngle + nAngle: currentAngle - nAngle;

            posX = Math.cos(currentAngle+controlAngle)*radiusX;
            posY = Math.sin(currentAngle+controlAngle)*radiusY;

            posArr[i]=1+(posY*depthZ)/radiusY;

            depthArray.push({'id':unitsArray[i].id, 'position':posY});

            matrix.identity();
            matrix.scale(posArr[i]*startScale,posArr[i]*startScale);
            matrix.translate(posX,posY);
            matrix.decompose(unitsArray[i]);
        }

        depthArray.sort(function(a,b) {
            var x = a.position;
            var y = b.position;
            return x > y ? 1 : -1;
        });

        for (var k = 0,l = depthArray.length; k < l; k++){
            var depthArrayId = depthArray[k].id;
            this.setChildIndex(unitsArray[depthArrayId],k);
        }
        var house = this.getChildAt(unitsArray.length-1).getChildAt(0);

        if(house.enabled){
            //house.allow = true;
            house.allow = animationOver;
            //house.highlight(true);
        }
    };

    p._setup =  function() {
        var housesArr = this.houses;
        //var arr = ["red","blue","yellow","green","cyan"];

        for (var i=0; i < housesArr.length; i++){ // postavlja poslednji sator na centralno mesto, zato nije moglo da se klikne
        //for (var i=housesArr.length-1; i > -1; i--){
            var cont = new createjs.Container();                            // cont mora da ima id zbog setChildIndex(line 93)
            cont.id = i;                                                    // probao sam da umesto cont instanciram Location i njega jednostavno dodam u carouselContainer
            var location = new alteastream.Location(housesArr[i]);

            var labelBg = new createjs.Shape();
            var w = 190,h=92;
            labelBg.graphics.beginFill("#ffffff").drawRoundRect(0, 0, w, h, 4);
            labelBg.regX = w*0.5;
            labelBg.regY = h*0.5;
            labelBg.y = -18;
            labelBg.mouseEnabled = false;
            labelBg.cache(-w*0.5,-h*0.5,w*2,h*2);

            var fontMid = "bold 30px HurmeGeometricSans3";
            var white = "#734841";
            var locationText = new alteastream.BitmapText(location.casinoName,fontMid,white,{x:0,y:-30,textAlign:"center"});
            locationText.mouseEnabled = false;
            locationText.uncache();
            gamecore.Utils.DISPLAY.cacheText(locationText);
            // ali location instanca takodje mora da ima id
            cont.addChild(location,labelBg,locationText);

            unitsArray.push(cont); // postavlja poslednji sator na centralno mesto, zato nije moglo da se klikne
            //unitsArray.unshift(cont);

            this.addChild(cont);
        }

        this.visible = true;
        //this.startAnimation(true);
    };

    p.removeHighlight = function() {
        var locationsNum = this.numChildren;
        for(var i = 0; i < locationsNum; i++){
            var location = this.getChildAt(i).getChildAt(0);
            if(location.enabled === true){
                location.highlight(false);
            }
        }
    };

    p.clicksEnabled = function(bool) {
        var locationsNum = this.numChildren;
        for(var i = 0; i < locationsNum; i++){
            this.getChildAt(i).getChildAt(0).allow = bool;
        }
    };

    p.startAnimation = function(bool) {//staro, skloniti

    };

    p.getLocation = function(id){
        var locationsNum = this.numChildren;
        for(var i = 0; i < locationsNum; i++){
            var location = this.getChildAt(i).getChildAt(0);
            if(location.id === id){
                return location;
            }
        }
    };

    p.renderAnimation = function(render,angle,isClockwise) {
        if(render === true){
            _this.addEventListener("tick" , function () {
                animationOver = false;
                _this.update(angle,isClockwise);
            });
        }else{
            animationOver = true;
            _this.removeAllEventListeners("tick");
        }
    };

    alteastream.ZCarousel = createjs.promote(ZCarousel,"Container");
})();