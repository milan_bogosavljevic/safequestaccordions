
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var ToolTip = function(lobby) {
    this.Container_constructor();
    this.initialize_ToolTip(lobby);
};
var p = createjs.extend(ToolTip, createjs.Container);

// static properties:
// events:

// public properties:
    p.isFocused = false;
// private properties:

// constructor:
    p.initialize_ToolTip = function(lobby) {
        this.lobby = lobby;
        //var font = "26px HurmeLight";
        var fontMid = "24px HurmeGeometricSans3";
        //var fontSmall = "20px HurmeLight";
        var white = "#fff";
        //var green = "#4eccb7";//4fa780
        //var red = "#ff4f4f";
        //var gray = "#dedede";

        var popupBg = this.popupBg = alteastream.Assets.getImage(alteastream.Assets.images.popupBg);
        this.addChild(popupBg);

        popupBg.regX = popupBg.image.width*0.5;
        popupBg.regY = popupBg.image.height*0.5;
        popupBg.scaleX = 0.1;
        popupBg.x = popupBg.regX;
        popupBg.y = popupBg.regY-90;

        var _infoContainer = this._infoContainer = new createjs.Container();
        this.addChild(_infoContainer);
        _infoContainer.visible = false;

        var flag = this.flag = alteastream.Assets.getImage(alteastream.Assets.images.tooltipImage);
        this._infoContainer.addChild(flag);
        flag.x = 124;
        flag.y = -78;

        this._createDynamicText("locationText","",fontMid,white,{x:147,y:-40,textAlign:"center"});

        this.x = 810;
        this.y = 160;

        //NO INFO ON HOVER
        //this._createDynamicText("numOfMachinesLabel","available machines:",font,white,{x:15,y:50,textAlign:"left",textBaseline:"alphabetic"});//45
        //this._createDynamicText("numOfMachinesText","",font,white,{x:255,y:50,textAlign:"left",textBaseline:"alphabetic"});//35 "left"

        /*this._createDynamicText("emptyPlayFieldsLabel","available seats:",fontSmall,green,{x:240,y:220,textAlign:"left",textBaseline:"alphabetic"});
        this._createDynamicText("emptyPlayFieldsText","",fontSmall,green,{x:390,y:220,textAlign:"left",textBaseline:"alphabetic"});//245

        this._createDynamicText("busyPlayFieldsLabel","busy seats:",fontSmall,red,{x:280,y:240,textAlign:"left",textBaseline:"alphabetic"});
        this._createDynamicText("busyPlayFieldsText","",fontSmall,red,{x:390,y:240,textAlign:"left",textBaseline:"alphabetic"});

        this._createDynamicText("spectatorsLabel","spectators:",fontSmall,gray,{x:280,y:260,textAlign:"left",textBaseline:"alphabetic"});//TEMP, if exists
        this._createDynamicText("spectatorsText","",fontSmall,gray,{x:390,y:260,textAlign:"left",textBaseline:"alphabetic"});//TEMP, if exists

        this._createDynamicText("visitLabel","VISIT","bold 20px Verdana",white,{x:0,y:0,textAlign:"center",textBaseline:"alphabetic",visible:false});*/

        this.mouseEnabled = false;
        this.mouseChildren = false;
        this.hide();
    };
// static methods:
// public methods:
    p.rolloverObserver = function(location){
        this.show(location);
    };

    p.rolloutObserver = function(){
        this.hide();
    };

    p.clickObserver = function(location){
        //if(location.enab === true)//NO INFO ON HOVER
        //this.hide();
    };

    p.show = function(location){
        //location.clickEnabled(false);//NO INFO ON HOVER
        this.flag.image = location.flag.image;
        //this.x = location.x - 83;//TEMP localtoglobal
        //this.y = location.y + 490;
        //this.locationText.text = location.name;
        //this.locationText.text = location.casinoName;
        this.locationText.setText(location.casinoName);

        //this.numOfMachinesText.text = location.numberOfMachines();//NO INFO ON HOVER
        //this.numOfMachinesText.visible = false;
         /*this.emptyPlayFieldsText.text = location.numberOfEmptyPlayFields();
        this.busyPlayFieldsText.text = location.numberOfBusyPlayFields();
        this.spectatorsText.text = location.numberOfSpectators();//TEMP, if exists*/

        this.currentLocation = location;
        this.popupBg.scaleX = 0.1;
        this.visible = true;

        //console.log("biggestGameWin ID: "+location.infoObject.biggestGameWin);
        //console.log("biggestGameWin ID 2: "+location.getMachineInfo(2322));
        //location.getMachineInfo(2322);

       /* var pt = location.localToGlobal(0,0);//NO INFO ON HOVER
        var pt2 = this.globalToLocal(pt.x,pt.y);
        this.visitLabel.x = pt2.x;
        this.visitLabel.y = pt2.y;
        this.visitLabel.visible = false;*/

        TweenLite.to(this.popupBg,0.25,{scaleX:1,onComplete:this._show,onCompleteParams:[this]});
    };

    p._show = function(that){
        that.isFocused = true;
        that._infoContainer.visible = true;

        //numOfMachinesText
        //that._typeHeader(that);//NO INFO ON HOVER
        //that._typeInfos(that.currentLocation);
    };
    
    p.update = function(){
        if(this.isFocused){
            this._updateInfos(this.currentLocation);
        }
    };

    p.hide = function(){
        //this.visitLabel.visible = false;//NO INFO ON HOVER
        this._infoContainer.visible = false;
        this.visible = false;
        this.isFocused = false;
    };

// private methods:
    p._createDynamicText = function(instance,text,font,color,props){
        //var textInstance = this[instance] = new alteastream.DynamicText(text,font,color).set(props);
        var textInstance = this[instance] = new alteastream.BitmapText(text,font,color,props);
        this._infoContainer.addChild(textInstance);
    };

    p._typeInfos = function(location){
        var delayStep = 500;
        this.emptyPlayFieldsText.text = location.numberOfEmptyPlayFields();
        this.busyPlayFieldsText.text = location.numberOfBusyPlayFields();
        this.spectatorsText.text = location.numberOfSpectators();//TEMP, if exists

        this.emptyPlayFieldsText.visible =
        this.emptyPlayFieldsLabel.visible =
        this.busyPlayFieldsText.visible =
        this.spectatorsText.visible =           //TEMP, if exists
        this.busyPlayFieldsLabel.visible = false;
        this.visitLabel.visible = false;

        var that = this;
        this.emptyPlayFieldsLabel.autoType(30,delayStep,function(){
            that.emptyPlayFieldsText.visible = true;
        });

        this.busyPlayFieldsLabel.autoType(30,delayStep*2,function(){
            that.busyPlayFieldsText.visible = true;
        });

        this.spectatorsLabel.autoType(30,delayStep*3,function(){//TEMP, if exists
            that.spectatorsText.visible = true;
            that.visitLabel.visible = true;
            location.clickEnabled(true);
        });
    };

    p._typeHeader = function(that){
        that.numOfMachinesText.visible = false;
        that.numOfMachinesLabel.autoType(30,0,function(){
            that.numOfMachinesText.visible = true;
        });
    };

    p._updateInfos = function(location){
        this.emptyPlayFieldsText.text = location.numberOfEmptyPlayFields();
        this.busyPlayFieldsText.text = location.numberOfBusyPlayFields();
        this.spectatorsText.text = location.numberOfSpectators();//TEMP, if exists

        var color = "#fff";
        this.emptyPlayFieldsText.blitzHighlightColor(color);
        this.busyPlayFieldsText.blitzHighlightColor(color);
        this.spectatorsText.blitzHighlightColor(color);//TEMP, if exists
    };

    alteastream.ToolTip = createjs.promote(ToolTip,"Container");
})();