
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var MachineInfo = function(machineProps) {
    this.initialize_MachineInfo(machineProps);
};
var p = MachineInfo.prototype;

// static properties:
// public properties:
    p.id = null;
    p.isFreeToPlay = false;
    p.machineName = "";
    p.machineCameras = "";
    p.gameCode = "";
    p.previewCamera = "";
    p.selected = false;

// private properties:
// constructor:

    p.initialize_MachineInfo = function(machineProps) {
        this.id = machineProps.machineIndex;
        this.isFreeToPlay = machineProps.isFreeToPlay;
        this.machineName = machineProps.machineName;
        this.machineCameras = machineProps.machineCameras;
        this.gameCode = machineProps.gameCode;
        this.previewCamera = machineProps.previewCamera;
    };

// static methods:
// public methods:
    alteastream.MachineInfo = MachineInfo;
})();