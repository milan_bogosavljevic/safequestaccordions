/**
 * Created by Dacha on 25-Jan-16.
 */
// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var AbstractVideo = function (scene){
        this.initialize(scene);
    };

    var p = AbstractVideo.prototype;

// static properties:
// events:

// public vars:
    p.scene = null;
    p.currentVideo = null;
    p.videoID = null;
    p.videoElement = null;


// private vars:
// constructor:
    p.initialize = function (scene){
        this.scene = scene;
    };

// static methods:
// public functions:
    p.activate = function(){
        this.currentVideo.visible = true;
        this.videoElement.play();
    };

    p.reset = function(){};
    
    p.dispose = function() {
        var video = document.getElementById(this.videoID);
        video.parentNode.removeChild(video);
        video = null;

        this.currentVideo.parent.removeChild(this.currentVideo);
        this.currentVideo = null;
        this.videoElement = null;
    };

// private functions:

    alteastream.AbstractVideo = AbstractVideo;
})();