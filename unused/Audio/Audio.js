
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var Audio = function() {};
    var p = Audio.prototype;
// static properties:
    Audio.playSound = function(soundId) {
        alteastream.Assets.playSound(soundId);
    };

    Audio.playSoundChannel = function(soundId, loop, complete) {
        alteastream.Assets.playSoundChannel(soundId, loop, complete);
    };

    Audio.stopSoundChannel = function(soundId) {
        alteastream.Assets.stopSoundChannel(soundId);
    };

    Audio.setMute = function(bool) {
        alteastream.Assets.setMute(bool);
    };

    Audio.stopSound = function() {
        alteastream.Assets.stopSound();
    };

    Audio.stopWinLineSounds = function(){
        alteastream.Assets.stopWinLineSounds();
    };

    Audio.getSoundLength = function(sound) {
        return alteastream.Assets.getSoundLength(sound);
    };

    Audio.isPlaying = function(sound){
        return alteastream.Assets.isPlaying(sound);
    };

    Audio.setReference = function(scene){
        this.scene = scene;
    };

    Audio.soundVolume = function(sound, vol) {
        alteastream.Assets.soundVolume(sound, vol);
    };

    Audio.stopAllSounds = function() {
        alteastream.Assets.stopAllSounds();
    };

    Audio.playSoundChannelWithOptions = function(instance, properties) {
        return alteastream.Assets.playSoundChannelWithOptions(instance, properties);
    };


// events:
// public properties:
    p.scene = null;
// private properties:
// constructor:
// static methods:
// public methods:
// private methods:

alteastream.Audio = Audio;

})();