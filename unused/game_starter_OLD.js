function resize(){

    var isMobile = window.localStorage.getItem('isMobile');
    //var isMobile = "yes";
    var maxWidth = isMobile === "yes"? 960:1920;
    var maxHeight = isMobile === "yes"? 540:1080;

    var deviceWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    var deviceHeight = (window.innerHeight > 0) ? window.innerHeight : screen.height;

    var newWidth = deviceWidth * 0.98;
    var newHeight = deviceHeight * 0.98;
    if(deviceWidth > maxWidth) {
        newWidth = maxWidth;
        newHeight = maxHeight;
    }
    var marginTop;
    var newWidthToHeight = newWidth/newHeight;
    if (newWidthToHeight > widthToHeight) {
        // window width is too wide relative to desired game width
        newWidth = newHeight * widthToHeight;
        marginTop = (window.innerHeight - newHeight) / 2;
        $(".game_container").css({
            "height": newHeight,
            "width": newWidth,
            "marginTop" : marginTop,
            "marginLeft": (window.innerWidth - newWidth) / 2
        });

    } else { // window height is too high relative to desired game height
        newHeight = newWidth / widthToHeight;
        marginTop = (window.innerHeight - newHeight) / 2;
        $(".game_container").css({
            "height": newHeight,
            "width": newWidth,
            "marginTop" : marginTop,
            "marginLeft": (window.innerWidth-newWidth) / 2
        });
    }
}