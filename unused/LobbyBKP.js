

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LobbyBKP = function() {
        this.AbstractScene_constructor();
        this.initialize_LobbyBKP();
    };
    var p = createjs.extend(LobbyBKP, alteastream.AbstractScene);

// static properties:
    LobbyBKP.GAME_ID = 0;
    LobbyBKP.GAME_NAME = "";
    LobbyBKP.GAME_URL = "";
    LobbyBKP.SERVER_URL = "";
    LobbyBKP.GAME_QUIT = "";
    LobbyBKP.GAME_WIDTH = 1920;
    LobbyBKP.GAME_HEIGHT = 1080;

    LobbyBKP.LOCATION_1 = "Serbia";
    LobbyBKP.LOCATION_2 = "Austria";
    LobbyBKP.LOCATION_3 = "Germany";
    LobbyBKP.LOCATION_4 = "Italy";
    LobbyBKP.LOCATION_5 = "Poland";

// events:
// public properties:
    p.selectedLocation = null;
    p.toolTip = null;
    p.currentMap = null;
    p.currentLocationMachines = null;
    p._currentFocused = null;
    p.queuePanel = null;
    p.streamPreview = null;
    p.justWatching = true;
// private properties:

    var _instance = null;


// constructor:
    p.initialize_LobbyBKP = function() {
        _instance = this;
        var port = ":8080";
        var protocol = "http://";
        alteastream.AbstractScene.GAME_WIDTH = LobbyBKP.GAME_WIDTH;
        alteastream.AbstractScene.GAME_HEIGHT = LobbyBKP.GAME_HEIGHT;
        alteastream.AbstractScene.GAME_QUIT = LobbyBKP.GAME_QUIT;
        alteastream.AbstractScene.GAME_URL = LobbyBKP.GAME_URL;
        alteastream.AbstractScene.SERVER_URL = protocol+LobbyBKP.SERVER_URL+port;
        alteastream.AbstractScene.GAME_TOKEN = LobbyBKP.GAME_TOKEN;
        alteastream.AbstractScene.GAME_NAME = LobbyBKP.GAME_NAME;
        alteastream.AbstractScene.GAME_ID = LobbyBKP.GAME_ID;

        this.setCommunicator();
    };
// static methods:
    LobbyBKP.getInstance = function(){
        return _instance;
    };
// public methods:
    p.setCommunicator = function(){
        this.requester = new alteastream.Requester(this);
        this.startCommunication(this.requester);
    };

    p.startCommunication = function(communicator){
        this.globalMouseEnabled(false);
        communicator.getHouses(function(response) {
            if(response.method !=="Exception") {
                _instance.globalMouseEnabled(true);
                console.log("STARTLobbyBKP "+response);
                //that.setGameParameters(response);// no for LobbyBKP
                _instance._setComponents(response);
                _instance.setSocketCommunication();
            }
            else
                var err = new alteastream.ErrorGameNotInitialized(response);
        });
    };

    p.onSocketConnect = function(frame){
        _instance.setRenderer();
        _instance.socketCommunicator.onLobbyBKPConnect(frame);
    };

    p.getName = function(suf){
        return LobbyBKP["LOCATION_"+ suf];
    };

    p.setFocused = function(object){
        this._currentFocused = object;
    };

    p.getFocused = function(){
        return this._currentFocused;
    };

    p.switchFocusIn = function(objectIn){
        objectIn.focusIn();
        this.getFocused().focusOut();
    };

    p.clickObserver = function(location){
        if(location.enab === true){
            this.selectedLocation = location;
            this.requester.getMachines(location.id,function(response){
                _instance.switchFocusIn(_instance.currentLocationMachines);
                _instance.toolTip.hide();
                location.setMachines(response);
        });
        }
    };

    /*p.update = function(status){// globalUpdate
        this.currentMap.update(status);
        this.toolTip.update();
        this.currentLocationMachines.update(status);
    };*/

    p._setComponents = function(response){
        //TEMP>
        if(is.mobile()){
            var errUI = {"message":"UI not supported yet. Run on desktop","method":"Handheld device"};
            this.throwErrorUI(errUI);
            return;
        }
        //TEMP<

        var mainBackground = this.mainBackground = alteastream.Assets.getImage(alteastream.Assets.images.mainBackground);
        var currentMap = this.currentMap = new alteastream.ABSMap(this);//TEMP ALL
        var europeBg = alteastream.Assets.getImage(alteastream.Assets.images.europe);
        europeBg.mouseEnabled = false;
        currentMap.addChild(europeBg);
        currentMap.x = 960-305;
        currentMap.y = 230;
        currentMap.name = "europe";
        this.setFocused(currentMap);

        var toolTip = this.toolTip = new alteastream.ToolTip(this);

        var testAvailable = this.testAvailable = new createjs.Text("test available: ", "26px Hurme", "#18ff06").set({textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        testAvailable.x = 730;
        testAvailable.y = 1050;

        var testAvailableTxt = this.testAvailableTxt = new createjs.Text("", "26px Hurme", "#18ff06").set({textAlign:"left",textBaseline:"middle",mouseEnabled:false});
        testAvailableTxt.x = 850;
        testAvailableTxt.y = 1050;

        //var status = response.param.status;
        //this.setMap(status);
        this.setMap(response);

        this.addChild(mainBackground, currentMap, toolTip , this.currentLocationMachines,testAvailable,testAvailableTxt);

        currentMap.addObserver(new alteastream.MouseObserver(this,"clickObserver","click"));
        currentMap.addObserver(new alteastream.MouseObserver(toolTip,"rolloverObserver","mouseover"));
        currentMap.addObserver(new alteastream.MouseObserver(toolTip,"rolloutObserver","mouseout"));
        currentMap.addObserver(new alteastream.MouseObserver(toolTip,"clickObserver","click"));

        var streamPreview = this.streamPreview = new alteastream.StreamPreview(230,230);
        this.addChild(streamPreview);
        streamPreview.show(false);

        var queuePanel = this.queuePanel = new alteastream.QueuePanel(this);
        this.addChild(queuePanel);
        queuePanel.show(false);
    };

    p.onQueueCall = function(button,machineInfo){
        this.streamPreview.setSourceAndId(machineInfo.machineCameras["1"].endpoint,machineInfo.id);//videti s Vladom koji camera endpoint index da se uzima
        this.queuePanel.enterMachineQueue(button,machineInfo.id);
    };

    p.onQueueEnter = function(){
        var that = this;
        this.justWatching = false;
        this.requester.enterMachine(function(response){
            that.queuePanel.onQueueEnterResponse(response);
            that.streamPreview.activate();
            that.streamPreview.updateInfo();
            that.currentLocationMachines._machinesComponent.disableActiveMachines(true);
            that.currentMap.clickEnabled(false);
        });
    };

    p.onQueueReset = function(){
        this.justWatching = true;
        this.streamPreview.reset();
        this.currentLocationMachines._machinesComponent.disableActiveMachines(false);
        this.currentMap.clickEnabled(true);
    };

    p.onQueueLeave = function(){
        this.justWatching = true;
        this.streamPreview.reset();
        this.currentMap.clickEnabled(true);
        //this.streamPreview.show(false);
        //this.queuePanel.show(false);
    };

    p.onMachineSelected = function(){
        this.justWatching = true;
        this.socketCommunicator.unsubscribeFromAll();
        this.killGame();
        this.goToMachine();
    };

    p.goToMachine = function(){
        var qStr = "?usr="+alteastream.AbstractScene.GAME_TOKEN+"&serverUrl="+alteastream.AbstractScene.SERVER_URL+"&machine_name="+alteastream.AbstractScene.GAME_ID+"&game="+"rmgame";//alteastream.AbstractScene.GAME_NAME
        window.top.changeIFrameSrc("http://139.59.147.230/machine/"+qStr);
    };

    /*p.setMap = function(status){
        var maxNumberOfMachines = 0;
        for(var i = 0, s = status.length;i<s;i++){
            var location = new alteastream.Location(this.getName(status[i].locationId),status[i]);
            this.currentMap.addChild(location);
            var locationMachinesNumber = location.machines.length;
            if(locationMachinesNumber > maxNumberOfMachines){maxNumberOfMachines = locationMachinesNumber;}
        }
        var currentLocationMachines = this.currentLocationMachines = new alteastream.LocationMachines(this,maxNumberOfMachines);
        currentLocationMachines.x = 440;
        currentLocationMachines.y = 85;
    };*/

  /*{"simmachine":{"machineName":"simmachine","machineCameras":{"1":{"type":1,"endpoint":"https://www.youtube.com/embed/ucZzg1vmOSk?autoplay=0"},"2":{"type":2,"endpoint":"https://www.youtube.com/embed/vdSRo1ta2y8?autoplay=0"},"4":{"type":4,"endpoint":"https://www.youtube.com/embed/vnOV7iKR1vE?autoplay=0"}},"isFreeToPlay":true,"machineIndex":1},"1234":{"machineName":"1234","machineCameras":{"1":{"type":1,"endpoint":"https://www.youtube.com/embed/VC_-2hpzQt0?autoplay=0"},"2":{"type":2,"endpoint":"https://www.youtube.com/embed/xqBL5vGhNFQ?autoplay=0"},"4":{"type":4,"endpoint":"https://www.youtube.com/embed/oUkVt0EjJVs?autoplay=0"}},"isFreeToPlay":true,"machineIndex":2}}*/

    p.setMap = function(locations){ // COUNTRIES FROM GAMEINFO.TXT
        var maxNumberOfMachines = 20; // todo mora dinamicki da se setuje
        var testAvailable = "";
        for(var p in locations){
            var locationId = p;
            var locationName = this.getName(locationId);
            var casinoName = locations[locationId].name;
            //var locationObject = locations[locationId];

            var location = new alteastream.Location(locationId,locationName,casinoName);
            this.currentMap.addChild(location);

            testAvailable += location.name + " ";
            //console.log("Test available: "+location.name);

        }
        this.testAvailableTxt.text = testAvailable;
        var currentLocationMachines = this.currentLocationMachines = new alteastream.LocationMachines(this,maxNumberOfMachines);
        currentLocationMachines.x = 440;
        currentLocationMachines.y = 85;
    };

    p.onQuit = function(){
        this.killGame();
        this.closeGame();
    };

    //JUST LOCAL TESTS FROM HERE::::::::>
   /* p.testUpdate = function(){
        var rndN = gamecore.Utils.NUMBER;
        var resp ={"param":
            {"credits":5000.0,
                "status":[
                    {"locationId":0,"machines":[{"id":1,"emptyPlayFields":rndN.randomRange(0,4)},{"id":2,"emptyPlayFields":rndN.randomRange(0,4)}]},
                    {"locationId":1,"machines":[{"id":28,"emptyPlayFields":rndN.randomRange(0,4)},{"id":29,"emptyPlayFields":rndN.randomRange(0,4)},{"id":30,"emptyPlayFields":rndN.randomRange(0,4)}]},
                    {"locationId":2,"machines":[{"id":68,"emptyPlayFields":rndN.randomRange(0,4)},{"id":70,"emptyPlayFields":rndN.randomRange(0,4)},{"id":69,"emptyPlayFields":rndN.randomRange(0,4)}]},
                    {"locationId":3,"machines":[{"id":85,"emptyPlayFields":rndN.randomRange(0,4)},{"id":86,"emptyPlayFields":rndN.randomRange(0,4)},{"id":87,"emptyPlayFields":rndN.randomRange(0,4)}]},
                    {"locationId":4,"machines":[{"id":50,"emptyPlayFields":rndN.randomRange(0,4)},{"id":51,"emptyPlayFields":rndN.randomRange(0,4)},{"id":52,"emptyPlayFields":rndN.randomRange(0,4)}]}
                ],
                "method":"GameUpdate"
            }
        };

        var status = resp.param.status;
        this.update(status);
    };*/

    alteastream.LobbyBKP = createjs.promote(LobbyBKP,"AbstractScene");
})();