//var HOST                                        = "localhost";
var HOST                                        = "139.59.147.230";
var stompClient                                 = null;
var socket                                      = null;
var machine_queue                               = "topic";
var machine_name                                = "0";
var messages_queue                              = "msg";
var socket_prefix                               = "ws";
var user_token                                  =  window.location.href.split("?usr=")[1]; //window.location.href.split("?usr=")[1] ? window.location.href.split("?usr=")[1] : "eyJhbGciOiJIUzUxMiJ9.eyJvYmoiOnsidG9rZW4iOiI5ZzZuNGIifSwiZXhwIjoxNTIzODk5MDY0fQ.47tom5bzXPUdYzpbrw_V0moV_6gfO6dbmXAGJtQzRU_RSHXBiUfcmo9XVQFlrkectaSIBoFRzSvuP7oBChb6Gw";
var machineReadyInterval                        = -1;
var machineReadyTime                            = 10;
var game                                        = "rmgame";
var currentGameUid                              = "";
var gameActive                                  = false;
var USER_SOCKET_INACTIVITY_TIMEOUT              = 10;
var apppath					                    = "http://"+HOST+":8080/";
var wplayer                                     = null;
var jplayer                                     = null;
var session_token                               = jwt_decode(user_token).token;


var subscriptions = [];

function toLocalStorage(obj){
    localStorage.pp = JSON.stringify(obj);
}
function fromLocalStorage(obj){
    return JSON.parse(localStorage.pp);
}

var SocketMessageType = {
    1:"MACHINE_INFO",
    2:"MACHINE_QUEUE_CHANGED",
    3:"MACHINE_QUEUE_PLAYER_ADDED",
    4:"MACHINE_QUEUE_PLAYER_REMOVED",
    5:"MACHINE_READY_FOR_PLAYER",
    MACHINE_INFO:1,
    MACHINE_QUEUE_CHANGED:2,
    MACHINE_QUEUE_PLAYER_ADDED:3,
    MACHINE_QUEUE_PLAYER_REMOVED:4,
    MACHINE_READY_FOR_PLAYER:5
}
var SocketMessageCategory = {
    1:"CATEGORY_TO_USER",
    2:"CATEGORY_TO_MACHINE",
    CATEGORY_TO_USER:1,
    CATEGORY_TO_MACHINE:2
};
var UserMessageType = {
    1:"REQUEST_MACHINE_INFO",
    2:"GAME_COMMAND",
    3:"REGISTER_SOCKET",
    4:"CHECK_USER",
    REQUEST_MACHINE_INFO:1,
    GAME_COMMAND:2,
    REGISTER_SOCKET:3,
    CHECK_USER:4
};
var GameMessageType = {
    1:"CONFIRM_GAME_INIT",
    2:"MACHINE_RESET_START",
    3:"MACHINE_RESET_END",
    7:"MOVE_SHOOTER_LEFT",
    8:"MOVE_SHOOTER_RIGHT",
    9:"STOP_SHOOTER",
    10:"DISPENSE_TOKENS",
    11:"PRIZE_DETECTION",
    30:"USER_INACTIVITY_WARNING",
    40:"NON_PLAYING_TIME_LIMIT_REACHED",
    50:"TOKENS_COUNT_CHANGED",
    60:"BALANCE_CHANGED",
    100:"HEARTBEAT",
    CONFIRM_GAME_INIT:1,
    MACHINE_RESET_START:2,
    MACHINE_RESET_END:3,
    MOVE_SHOOTER_LEFT:7,
    MOVE_SHOOTER_RIGHT:8,
    STOP_SHOOTER:9,
    DISPENSE_TOKENS:10,
    PRIZE_DETECTION:11,
    USER_INACTIVITY_WARNING:30,
    NON_PLAYING_TIME_LIMIT_REACHED:40,
    TOKENS_COUNT_CHANGED:50,
    BALANCE_CHANGED:60,
    HEARTBEAT:100
};
var UserControls = {
    LEFT:37,
    RIGHT:39,
    SPACE:32,
    37:"LEFT",
    39:"RIGHT",
    32:"SPACE"
}

$(document).ready(function(){
    $("#user_token").html(user_token);
    getHouses();
    $("#shooter_left_button").mousedown(function(){
        if(!gameActive){ return; }
        console.log("levi shooter ide");
        keys[UserControls.LEFT] = true;
        shooterAction(GameMessageType.MOVE_SHOOTER_LEFT);
    });
    $("#shooter_left_button").mouseup(function(){
        if(!gameActive){ return; }
        delete keys[UserControls.LEFT];
        shooterAction(GameMessageType.STOP_SHOOTER);

        console.log("levi shooter stao");
    });
    $("#shooter_right_button").mousedown(function(){
        if(!gameActive){ return; }
        console.log("levi shooter ide");
        keys[UserControls.RIGHT] = true;
        shooterAction(GameMessageType.MOVE_SHOOTER_RIGHT);
    });
    $("#shooter_right_button").mouseup(function(){
        if(!gameActive){ return; }
        delete keys[UserControls.RIGHT];
        shooterAction(GameMessageType.STOP_SHOOTER);
        console.log("levi shooter stao");
    });
    $("#dispense_tokens_button").click(function(){
        if(!gameActive){ return; }
        var roundSize = $("#shooter_round_size").val();
        shooter.burst = roundSize;
        burstCoins();
        shooterAction(GameMessageType.DISPENSE_TOKENS,roundSize);
        shooter.iscd = true;
        shooter.lcd = Date.now();
    });
    ctx = $("#game_canvas").get()[0].getContext("2d");
    setInterval(mainLoop,100);

});

var lastMachineFrame = 0;
var keys = {};
var ctx;
var shooter = {
    x:100,
    y:180,
    w:15,
    h:60,
    spd:0.8,
    cdt:300,
    iscd:false,
    lcd:0,
    burst:0
}
var coins = [];

var machines = {};
var webrtcPlayer = new WEBRTCPlayer();



function loadMachine(machineIndex){
    machine_name = machineIndex;
    var machine = machines[machineIndex];
    var cameraEndpoint = machine.machineCameras[1].endpoint;
    console.debug(cameraEndpoint);

    // wplayer = WowzaPlayer.create('playerElement',
    //     {
    //         "license":"PLAY1-nByuW-4enAj-fD37H-9r9kK-cux7u",
    //         "title":"",
    //         "description":"",
    //         "sourceURL":cameraEndpoint,//"http%3A%2F%2F84.199.144.157%3A1935%2Flive%2Fmachine2322.stream%2Fplaylist.m3u8",
    //         "autoPlay":true,
    //         "volume":"75",
    //         "mute":false,
    //         "loop":false,
    //         "audioOnly":false,
    //         "uiShowQuickRewind":true,
    //         "uiQuickRewindSeconds":"30"
    //     }
    // );

    wplayer = WowzaPlayer.create('playerElement',
        {
            "license":"PLAY1-nByuW-4enAj-fD37H-9r9kK-cux7u",
            "sourceURL":cameraEndpoint,//"http%3A%2F%2F84.199.144.157%3A1935%2Flive%2Fmachine2322.stream%2Fplaylist.m3u8",
            "autoPlay":true
        }
    );

    //wplayer.play();


    //$("#iframe_wrapper").html('<iframe width="320" height="240" src="'+machine.machineCameras[1].endpoint+'" frameborder="0" allowfullscreen></iframe>');
    $("#machine_wrapper").fadeIn();
    for(var i=0;i<subscriptions.length;i++){
        subscriptions[i].unsubscribe();
    }

    socket = null;
    stompClient = null;
    gameActive = false;
    connectSocket();
}



function getMachines(house){
    var machinesDiv = $("#machines");
    $.get(apppath+"machine/getmachines/"+house,function(res){
        var output = "";
        for(var r in res){
            console.log(res[r].machineName);
            machines[res[r].machineIndex] = res[r];
            output+="<button onclick='loadMachine("+res[r].machineIndex+")'>" + res[r].machineName + "</button>";
        }
        machinesDiv.html(output);
    });
}
function getHouses(){
    var housesDiv = $("#houses");
    $.get(apppath+"machine/gethouses",function(res){
        var output = "";
        for(var r in res){
            output+="<button onclick='getMachines("+r+")'>" + res[r].name + "</button>";
        }
        housesDiv.html(output);
    });
}

function burstCoins(){
    if(shooter.burst<1){
        return;
    }
    coins.push({x: shooter.x, y: shooter.y});
    shooter.burst--;
    setTimeout(burstCoins,100);
}


/* Game ack interval */
setInterval(function(){
    heartbeatgame();
},10000);

setInterval(function(){
    heartbeatuser();
},50000);

function heartbeatuser(){
    $.get(apppath+"user/heartbeat/"+user_token,function(res){
        user_token = res;
        console.log(res);
    });
}

heartbeatuser();

function heartbeatgame(){
    if(stompClient!=null && gameActive) {
        stompClient.send("/" + messages_queue + "/message", {}, packGameCommand({
            val: 0,
            tp: GameMessageType.HEARTBEAT,
            tkn: user_token
        }));
    }
}

function mainLoop(){
    if(!gameActive){
        return;
    }
    if(Date.now()-lastMachineFrame > 1100){
        // console.log(Date.now()-lastMachineFrame);
        // console.log("LAG DETECTED");
    }
    return;
    ctx.clearRect(0,0,320,240);
    if(shooter.iscd){
        if(Date.now()-shooter.lcd > shooter.cdt){
            shooter.iscd = false;
        }
    }
    if(keys[UserControls.LEFT]){
        shooter.x -= shooter.spd
    }
    if(keys[UserControls.RIGHT]){
        shooter.x += shooter.spd
    }
    if(keys[UserControls.SPACE]){
        if (!shooter.iscd && shooter.burst<1) {
            var roundSize = $("#shooter_round_size").val();
            shooter.burst = roundSize;
            burstCoins();
            shooter.iscd = true;
            shooter.lcd = Date.now();
        }
    }
    ctx.fillStyle = "blue";
    for(var i=coins.length-1;i>=0;i--){
        coins[i].y-=2;
        ctx.beginPath();
        ctx.arc(coins[i].x+shooter.w/2, coins[i].y, 8, 0, 2 * Math.PI, false);
        ctx.fill();
        if(coins[i].y<-10){
            coins.splice(i,1);
        }
    }
    ctx.fillStyle = "red";
    ctx.fillRect(shooter.x,shooter.y,shooter.w,shooter.h);
}


window.onkeydown = function(evt){
    if(!gameActive){ return; }
    console.debug(evt);
    if(!keys[evt.keyCode]) {
        if (evt.keyCode == UserControls.LEFT) {
            shooterAction(GameMessageType.MOVE_SHOOTER_LEFT);
        }
        if (evt.keyCode == UserControls.RIGHT) {
            shooterAction(GameMessageType.MOVE_SHOOTER_RIGHT);
        }
        keys[evt.keyCode] = true;
    }
}
window.onkeyup = function(evt){
    if(!gameActive){ return; }
    delete keys[evt.keyCode];
    if(evt.keyCode==UserControls.LEFT||evt.keyCode==UserControls.RIGHT){
        shooterAction(GameMessageType.STOP_SHOOTER);
    }
    if(evt.keyCode==UserControls.SPACE){

    }
}


function packGameCommand(payload){
    return JSON.stringify({
        tp:UserMessageType.GAME_COMMAND,
        tkn:currentGameUid,
        val:payload
    })
}

function shooterAction(actionType,value){
    stompClient.send("/"+messages_queue+"/message", {}, JSON.stringify({
        tp:UserMessageType.GAME_COMMAND,
        tkn:currentGameUid,
        val:{
            val:value,
            tp:actionType,
            tkn:user_token
        }
    }));
    console.debug(actionType);
}

function enterMachine(){
    $.get(apppath+"machine/enterqueue?m="+machine_name+"&t="+user_token,function(res){
        switch(res.vl){
            case 0:
                console.debug("Successfully added to machine queue");

                break;
            case 1:
                console.debug("User already in queue for machine");
                break;
        }
    });
}
function leaveMachine(){
    $.get(apppath+"machine/leavemachine?m="+machine_name+"&t="+user_token,function(res){
        switch(res.vl){
            case 0:
                console.debug("Successfully removed from machine queue");
                break;
            case 1:
                console.debug("User already in queue for machine");
                break;
        }
    });
}

function closeGame(){
    $("#queue_access_set").fadeIn();
    $("#game_controls_set").fadeOut();
    $("#videoOutput").fadeOut();
}

function frameDropTimer(){
    console.log("Frames dropped");
}

function byteCount(bts){
    lastMachineFrame = Date.now();
    console.log("Bytes per second: " + bts);
}

function startGame(){
    gameActive = false;
    $.get(apppath+"gamestart/"+user_token+"/"+machine_name+"?game="+game,function(res){
        var res = JSON.parse(res);
        if(res.state!=0){
            return;
        }
        $("#playerElement").fadeOut(function(){
            wplayer.stop();
        });
        var host = res.streamendpoint; //"139.59.147.230";
        //var client = new WebSocket(host);

        var canvas = document.querySelector('#videoOutput');
        webrtcPlayer.videoOutput    = canvas;
        webrtcPlayer.address        = res.webrtcurl + "?token=" + user_token + "&id=" + machine_name;
        webrtcPlayer.camera         = res.webrtccam;


        // jplayer = new jsmpeg(client, {
        //     canvas: canvas
        // });
        //
        // jplayer.onbytecount = byteCount;
        // setTimeout(function(){
        //     jplayer.client.send('{"token":1,"id":'+machine_name+'}');
        // },1000);

        $("#queue_access_set").fadeOut();
        $("#game_controls_set").fadeIn();
        $("#videoOutput").fadeIn();

        $("#currentBalance").html((res.balance/100).toFixed(2));
        $("#currentTokens").html(res.tokens);
        stompClient.unsubscribe('/'+machine_queue+'/'+currentGameUid);
        currentGameUid = res.key;
        var s1 = stompClient.subscribe('/'+machine_queue+'/'+currentGameUid, function (msg) {
            var msg = JSON.parse(msg.body);
            parseGameMessage(msg);
        });
        subscriptions.push(s1);
        stompClient.send("/"+messages_queue+"/message", {}, JSON.stringify({
            tp:UserMessageType.GAME_COMMAND,
            tkn:currentGameUid,
            val:{
                tp:GameMessageType.CONFIRM_GAME_INIT,
                tkn:user_token
            }
        }))
    });
}
function connectSocket(){
    socket = new SockJS(apppath+'/'+socket_prefix);
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        console.log('/'+machine_queue+'/'+session_token);
        var s2 = stompClient.subscribe('/'+machine_queue+'/'+session_token, function (greeting) {
            console.debug("Direct user message");
            console.debug(greeting.body);
            parseMessage(JSON.parse(greeting.body));
        });
        subscriptions.push(s2);
        var s3 = stompClient.subscribe('/'+machine_queue+'/'+machine_name, function (greeting) {
            console.debug("Public message from machine");
            parseMessage(JSON.parse(greeting.body));
        });
        subscriptions.push(s3);
        // stompClient.send("/"+messages_queue+"/message", {}, JSON.stringify({tp:UserMessageType.REGISTER_SOCKET,tkn:user_token}));
        // stompClient.send("/"+messages_queue+"/message", {}, JSON.stringify({tp:UserMessageType.REQUEST_MACHINE_INFO,tkn:user_token,val:machine_name}));
    });
}

var prizeMessageTimeout = -1;


function showPrizeMessage(msg){
    clearTimeout(prizeMessageTimeout);
    $("#prizePanel").html(msg);
    $("#prizePanel").fadeIn();
    prizeMessageTimeout = setTimeout(function(){
        $("#prizePanel").fadeOut();
    },10000);
}

function parseGameMessage(msg){
    console.debug(msg);
    switch(msg.cat){
        case GameMessageType.MACHINE_RESET_START:

            break;
        case GameMessageType.MACHINE_RESET_END:
            gameActive = true;
            break;
        case GameMessageType.NON_PLAYING_TIME_LIMIT_REACHED:
            console.debug("Non playing time limit reached, game is closed");
            closeGame();
            break;
        case GameMessageType.USER_INACTIVITY_WARNING:
            switch(msg.vl){
                case 1:
                    console.debug("First warning...return to game");
                    heartbeatgame();
                    break;
                case 2:
                    console.debug("Second warning...return to game");
                    heartbeatgame();
                    break;
                case 3:
                    console.debug("Third warning...game is closed");
                    closeGame();
                    break;
            }
            console.debug(msg);
            break;
        case GameMessageType.PRIZE_DETECTION:
            if(msg.vl){
                showPrizeMessage("You won " + (msg.vl.money/100).toFixed(2));
                $("#currentBalance").html((msg.vl.balance/100).toFixed(2));
                $("#currentWin").html(msg.vl.win);
            }
            console.log(msg);
            break;
        case GameMessageType.TOKENS_COUNT_CHANGED:
            $("#currentTokens").html(msg.vl);
        case GameMessageType.BALANCE_CHANGED:
            $("#currentBalance").html((msg.vl/100).toFixed(2));
            break;
    }
}

function parseMessage(msg){
    switch(msg.tp){
        case SocketMessageType.MACHINE_INFO:
            console.log("machine info message");
            console.log(msg);
            $("#machine_user_queue").html(msg.vl.queueCount);
            break;
        case SocketMessageType.MACHINE_QUEUE_CHANGED:
            console.log(msg);
            $("#machine_user_queue").html(msg.vl);
            break;
        case SocketMessageType.MACHINE_READY_FOR_PLAYER:
            console.log(msg);
            $("#machineWaitingTimer").css('color','green');
            $("#machineWaitingTimer").css('display','block');
            $("#machineWaitingTimer").html("Machine is ready for you: ");
            clearInterval(machineReadyInterval);
            machineReadyTime = 10;
            machineReadyInterval = setInterval(function(){
                machineReadyTime--;
                if(machineReadyTime<5){
                    $("#machineWaitingTimer").css('color','red');
                }
                $("#machineWaitingTimer").html("Click here to start (" + machineReadyTime + " sec left)");
                if(machineReadyTime<0){
                    $("#machineWaitingTimer").css('display','none');
                    clearInterval(machineReadyInterval);
                    machineReadyTime = 10;
                }

            },1000);
            break;
    }
}

//connectSocket();

