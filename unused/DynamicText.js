
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var DynamicText = function(text,font,color) {
    this.BitmapText_constructor(text,font,color);
    this.initialize_DynamicText(text,font,color);
};
var p = createjs.extend(DynamicText, alteastream.BitmapText);

// static properties:
// events:

// public properties:

// private properties:
    p._defaultColor  = "#000";
    p._highlightColor  = "#FFF";

// constructor:
    p.initialize_DynamicText = function(text,font,color) {
        this.text = text;
        this.font = font;
        this._defaultColor = this.color = color;
        this.animatedText = text;
        this.mouseEnabled = false;
        this._cursor = " _";
    };
// static methods:
// public methods:

    p.autoType = function(speed,delay,callback) {
        this.visible = false;
        var _cursor = this._cursor;
        clearTimeout(this._timeout);
        clearInterval(this._interval);

        var that = this;
        this._timeout = setTimeout(function(){
            that.visible = true;
            var txtArr = that.animatedText.split("");
            that.text = "";
            var _interval = that._interval = setInterval(function (){
                if (txtArr.length > 0) {
                    that.text = that.text.slice(0, that.text.length - _cursor.length);
                    that.text += (txtArr.shift() + _cursor);
                } else {
                    clearInterval(_interval);
                    that.text = that.animatedText;
                    if(callback)
                        callback();
                }
            },speed);
        },delay);
    };

    p.autoTypeCursor = function(type){
        this._cursor = type;
    };

    p.highlight = function(bool){
        this.color = bool === true? this._highlightColor: this._defaultColor;
    };

    p.blitzHighlightColor = function(color){
        this.color = color || this._highlightColor;
        var that = this;
        setTimeout(function(){
            that.color = that._defaultColor;
        },250);
    };

    alteastream.DynamicText = createjs.promote(DynamicText,"BitmapText");
})();