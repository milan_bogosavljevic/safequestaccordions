/**
 * Created by Dacha on 25-Jan-16.
 */
// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var IframeStream = function (scene,id,source){
        this.AbstractVideo_constructor(scene);
        this.initialize_IframeStream(id,source);
    };

    var p = createjs.extend(IframeStream, alteastream.AbstractVideo);

// static properties:
// events:

// public vars:
// private vars:
// constructor:
    p.initialize_IframeStream = function (id,source){
        console.log("initialize stream "+id);
        var attributes = {
            tag: '<iframe id = "'+id+'" src = "'+source+'" frameborder="0" width="'+alteastream.AbstractScene.GAME_WIDTH+'" height="'+alteastream.AbstractScene.GAME_HEIGHT+'" allowfullscreen allow="autoplay;"></iframe>',
        };

        var videoElement = this.videoElement = $(attributes.tag).appendTo(document.getElementById("wrapper"))[0];
        videoElement.style.width = "100%";
        videoElement.style.height = "100%";
        videoElement.style.zIndex = -1;

        this.currentVideo = new createjs.DOMElement(this.videoElement);
        this.videoElement.style.opacity = 0;
    };

    p.preActivate = function(){
        //if(is.not.ios())
           // this.videoElement.play();//ok
    };

    p.activate = function(){
        var that = this;
        setTimeout(function(){that.videoElement.style.opacity = 1;},2000);//animate
        //if(is.ios())
           // this.videoElement.play();//ok
    };

    p.reset = function(){
        this.currentVideo.visible = false;
        //this.videoElement.pause();//ok
    };

// static methods:
    
// public functions:

// private functions:

    alteastream.IframeStream = createjs.promote(IframeStream,"AbstractVideo");
})();