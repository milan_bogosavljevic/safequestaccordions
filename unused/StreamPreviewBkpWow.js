
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreviewWow = function(width,height){
        this.Container_constructor();
        this.initStreamPreviewWow(width,height);
    };

    var p = createjs.extend(StreamPreviewWow,createjs.Container);

    p.startX = 0;
    p.sWidth = 0;
    p.sHeight = 0;
    p.src = "";
    p.id = "";
    p.playing = false;

    var _instance = null;

    p.initStreamPreviewWow = function (width,height) {
        _instance = this;
        this.sWidth = width;
        this.sHeight = height;

        var video = this.video = document.createElement('div');
        video.id = 'wowza_player';
        video.style.top = 0;
        video.style.left = 0;
        video.style.width = this.sWidth + "px";
        video.style.height = this.sHeight + "px";
        video.style.position = "absolute";
        video.style.backgroundColor = 'none';
        video.style.visibility = 'hidden';
        video.style.border = "none";
        video.style.pointerEvents = "none";
        document.getElementById("wrapper").appendChild(video);

        var scriptTag = document.createElement('script');
        scriptTag.id = 'player_embed';
        scriptTag.src = '//player.cloud.wowza.com/hosted/6wxqjvq5/wowza.js';
        scriptTag.type = 'text/javascript';
        document.getElementById("wrapper").appendChild(scriptTag);

        var videoStream = this.videoStream = new createjs.DOMElement(video);
        this.addChild(videoStream);

        this.videoStream.mouseEnabled = false;
        this.videoStream.visible = false;

        this.setDynamicPosition(0.240,0.115);//inside machine 0.24,0.11
        setTimeout(function(){
            var playerStyle = document.getElementById("fcplayer_wrapper").style;
            playerStyle.width = "100%";
            playerStyle.height = "100%";
        },1000);

        window.addEventListener("resize",function(){
            _instance._resize();
        });
    };

    StreamPreviewWow.getInstance = function(){
        return _instance;
    };

    p.activate = function(){
        if(!this.playing){
            this.player = document.getElementById("fcplayer").childNodes[0];
            this.player.play();
            this.playing = true;
        }
        console.log("PLAY src>>>>>>>>>>>>> "+this.player.src);
    };

    p.updateInfo = function(){
        //this.machineInfoText.text = this.id;
    };

    p.setSource = function(src){
        this.src = src;
    };

    p.setID = function(id){
        this.id = id;
    };

    p.switch = function(){
        _instance.videoStream.visible = true;
        //_instance.video.src = _instance.src;// za video
    };

    p.reset = function(){
        _instance.videoStream.visible = false;
        //_instance.video.src = "";// za video
        _instance.id = "";
        //this.machineInfoText.text = "--";
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p._resize = function(){
        var w = window.innerWidth;
        var h = window.innerHeight;

        var gameWidth = alteastream.AbstractScene.GAME_WIDTH;
        var gameHeight = alteastream.AbstractScene.GAME_HEIGHT;

        var scaleX = w / (gameWidth+this.sWidth);
        var scaleY = h / (gameHeight+this.sHeight);

        this.scaleX = (gameWidth*scaleX)/1000;
        this.scaleY = (gameWidth*scaleY)/1000;
        this.x = w*this._getDynamicPosition().x;
        this.y = h*this._getDynamicPosition().y;
    };

    p.setDynamicPosition = function(xPerc,yPerc){
        this._dynamicPositionXY = {x:xPerc,y:yPerc};
        this._resize();
    };

    p._getDynamicPosition = function(){
        return this._dynamicPositionXY;
    };

    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new alteastream.DynamicText(text,font,color).set(props);
        this.addChild(textInstance);
    };

    alteastream.StreamPreviewWow = createjs.promote(StreamPreviewWow,"Container");
})();


