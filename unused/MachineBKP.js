
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MachineBKP = function(){
        this.Container_constructor();
        this.initMachineBKP();
    };

    var p = createjs.extend(MachineBKP,createjs.Container);
    //p._fullSeatImages = null;
    p._flagImage = null;
    p._idText = null;
    p._nameText = null;
    p._avaliabilityText = null;
    p.entranceDisabled = false;

    p.initMachineBKP = function () {
        console.log("MachineBKP Initialized...");

        var background = new createjs.Shape();
        background.graphics.beginStroke("#fff").setStrokeStyle(5).beginFill("#000000").drawRect(0,0,1360,58); // proveriti za cache da li treba da se pomera zbog bordera
        background.alpha = 0.1;

        this.setBounds(0,0,1360,58);

        var y = 29;

        var flag = this._flagImage = new createjs.Bitmap(alteastream.Assets.getImageURI(alteastream.Assets.images.serbiaFlag));
        flag.regY = flag.image.height/2;
        flag.x = 10;
        flag.y = y;

        var MachineBKPText = new createjs.Text("ID:" , "20px Hurme", "#798a94");
        MachineBKPText.textAlign = "left";
        MachineBKPText.textBaseline= "middle";
        MachineBKPText.x = 100;
        MachineBKPText.y = y;

        var idTxt = this._idText = new createjs.Text("", "20px Hurme", "#55aa83");
        idTxt.textAlign = "left";
        idTxt.textBaseline= "middle";
        idTxt.x = 130;
        idTxt.y = y;

        var MachineBKPNameText = new createjs.Text("NAME: " , "20px Hurme", "#798a94");
        MachineBKPNameText.textAlign = "left";
        MachineBKPNameText.textBaseline= "middle";
        MachineBKPNameText.x = 300;
        MachineBKPNameText.y = y;

        var nameTxt = this._nameText = new createjs.Text("", "20px Hurme", "#55aa83");
        nameTxt.textAlign = "left";
        nameTxt.textBaseline= "middle";
        nameTxt.x = 380;
        nameTxt.y = y;

        var avaliabilityTxt = this._avaliabilityText = new createjs.Text("", "20px Hurme", "#55aa83");
        avaliabilityTxt.textAlign = "center";
        avaliabilityTxt.textBaseline= "middle";
        avaliabilityTxt.x = 800;
        avaliabilityTxt.y = y;

       /* var freeSeatImg = new createjs.Bitmap(alteastream.Assets.getImageURI(alteastream.Assets.images.seatOff));
        freeSeatImg.regY = freeSeatImg.image.height/2;
        freeSeatImg.x = 500;
        freeSeatImg.y = y;

        var freeSeatImg2 = freeSeatImg.clone();
        freeSeatImg2.x = freeSeatImg.x + 20;

        var freeSeatImg3 = freeSeatImg.clone();
        freeSeatImg3.x = freeSeatImg2.x + 20;

        var freeSeatImg4 = freeSeatImg.clone();
        freeSeatImg4.x = freeSeatImg3.x + 20;

        var fullSeatImg = new createjs.Bitmap(alteastream.Assets.getImageURI(alteastream.Assets.images.seatOn));
        fullSeatImg.regY = fullSeatImg.image.height/2;
        fullSeatImg.x = freeSeatImg.x;
        fullSeatImg.y = freeSeatImg.y;

        var fullSeatImg2 = fullSeatImg.clone();
        fullSeatImg2.x = freeSeatImg2.x;
        fullSeatImg2.y = freeSeatImg2.y;

        var fullSeatImg3 = fullSeatImg.clone();
        fullSeatImg3.x = freeSeatImg3.x;
        fullSeatImg3.y = freeSeatImg3.y;

        var fullSeatImg4 = fullSeatImg.clone();
        fullSeatImg4.x = freeSeatImg4.x;
        fullSeatImg4.y = freeSeatImg4.y;

        this._fullSeatImages = [];
        this._fullSeatImages.push(fullSeatImg,fullSeatImg2,fullSeatImg3,fullSeatImg4);*/
        /*var that = this;
        var buttonLeaveTxt = new createjs.Text("LEAVE QUEUE", "16px Hurme", "#fff").set({textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var buttonLeave = this.buttonLeave = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMockSS),3,buttonLeaveTxt);
        buttonLeave.regX = 70;
        buttonLeave.regY = 18;
        buttonLeave.x = 1130;
        buttonLeave.y = y;
        buttonLeave.visible = false;

        buttonLeave.setClickHandler(function (e) {
            //buttonEnter.setDisabled(true);
            console.log("LEAVE QUEUE::::::::::::::::: ID: ");// MachineBKPIndex
            that.uncache();
            that.buttonLeave.visible = false;
            that.buttonEnter.setDisabled(false);
            alteastream.Lobby.getInstance().queuePanel.resetEntrance();
            that.cache(0,0,1360,58);
            alteastream.Lobby.getInstance().requester.leaveMachineBKP();
        });*/
        var that = this;
        var buttonEnterTxt = new createjs.Text("ENTER MachineBKP", "16px Hurme", "#fff").set({textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var buttonEnter = this.buttonEnter = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMockSS),3,buttonEnterTxt);
        buttonEnter.regX = 70;
        buttonEnter.regY = 18;
        buttonEnter.x = 1276;
        buttonEnter.y = y;

        buttonEnter.setClickHandler(function (e) {
            console.log("SIT on " + that.id);
            console.log("LOAD MachineBKP:GAME WINDOW START::::::::::::::::: ID: "+e.target.parent);// MachineBKPIndex
            alteastream.Lobby.getInstance().onQueueCall(e.currentTarget,that.MachineBKPInfo);
        });


        this.addChild(background,flag,MachineBKPText,idTxt,MachineBKPNameText , nameTxt , avaliabilityTxt,/*buttonLeave,*/buttonEnter);
        this.cache(0,0,1360,58);
        this.showMachineBKP(false);
    };

    p.setMachineBKPInfo = function(MachineBKP,flagImage) {
        this._flagImage.image = flagImage.image;
        this.uncache();
        this._idText.text = MachineBKP.id;
        this._nameText.text = MachineBKP.MachineBKPName;
        this._avaliabilityText.text = MachineBKP.isFreeToPlay ? "AVAILABLE" : "NOT AVAILABLE"; // make methods for easier interpretation of object (class/??)
        this._avaliabilityText.color = MachineBKP.isFreeToPlay ? "#7de2b5" : "#ea303f";
        this.id = MachineBKP.id;
        this.MachineBKPInfo = MachineBKP;
        this.showMachineBKP(true);
        this.disableEntrance(!this.MachineBKPInfo.isFreeToPlay);
        this.cache(0,0,1360,58);

    };

    p.showMachineBKP = function(bool) {
        if(this.visible !== bool){
            this.visible = bool;
        }
    };

    p.getHeight = function () {
        return this.getBounds().height;
    };

    p.getWidth = function () {
        return this.getBounds().width;
    };

    p.disableEntrance = function(bool){
        this.uncache();
       // if(!this.entranceDisabled){
            this.entranceDisabled = bool;
            this.buttonEnter.setDisabled(bool);
        this.cache(0,0,1360,58);
       // }
    };

   /* p.updateFullSeatsImages = function(numOfFreeSeats){
        var numOfFullSeats = 4 - numOfFreeSeats;
        this.uncache();
        for(var i = 0; i < this._fullSeatImages.length; i++){
            this._fullSeatImages[i].visible = i < numOfFullSeats;
        }
        this.cache(0,0,1360,58);
    };*/

    alteastream.MachineBKP = createjs.promote(MachineBKP,"Container");
})();


