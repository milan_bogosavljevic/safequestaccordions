this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var VideoStream = function (scene,id,address){
        this.initialize_VideoStream(scene,id,address);
    };

    var p = VideoStream.prototype;

    p.count = 0;
    p.streamArrived = false;
// static properties:
// events:

// public vars:
// private vars:
// constructor:
    p.initialize_VideoStream = function (scene,id,address){

        this.scene = scene;

        var videoID = this.videoID = id;

        //check here for mobile for width/height 960/540
        var videoTag ="<video id ='"+videoID+"' width = '1280' height='720' autoplay muted playsinline></video>";

        var videoElement = this.videoElement = $(videoTag).appendTo(document.getElementById("wrapper"))[0];
        document.getElementById("wrapper").insertBefore(document.getElementById(videoID), document.getElementById("wrapper").firstChild);

        var style = videoElement.style;
        style.width = "100%";
        style.height = "100%";
        style.pointerEvents = "none";

        /////////////////////////////////////
        style["video::-webkit-media-controls-panel"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-play-button"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-start-playback-button"] = "display: none!important;";
        style["video::-webkit-media-controls-start-playback-button"] = "-webkit-appearance: none;";
        //////////////////////////////

        /* style["video::-webkit-media-controls-panel"] = "display: none!important;";
         style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

         style["video::-webkit-media-controls-play-button"] = "display: none!important;";
         style["video::-webkit-media-controls-play-button"] = "-webkit-appearance: none;";

         style["video::-webkit-media-overlay-play-button"] = "display: none!important;";
         style["video::-webkit-media-overlay-play-button"] = "-webkit-appearance: none;";

         style["video::-webkit-media-controls-start-playback-button"] = "display: none!important;";
         style["video::-webkit-media-controls-start-playback-button"] = "-webkit-appearance: none;";

         style["video::-webkit-media-controls-panel-container"] = "-webkit-appearance: none;";
         style["video::-webkit-media-controls-panel-container"] = "display: none!important;";*/

        var webrtcPlayer = this.webrtcPlayer = new WEBRTCGamePlayer(this.videoElement);
        this.webrtcPlayer.set(address.webrtcurl,alteastream.AbstractScene.GAME_TOKEN,alteastream.AbstractScene.GAME_ID);

        webrtcPlayer.onError = function(error) {
            if(error) {
                this.scene.throwAlert(alteastream.Alert.ERROR,error);
                //webrtcPlayer.stop();
            }
        }.bind(this);
    };

    p.activate = function(callback){
        var that = this;
        //var backgroundMusicIsMuted = window.localStorage.getItem('backgroundMusicIsMuted');

        this.streamArrived = false;
        this.webrtcPlayer.onStreamEvent = function(evt){
            if(evt.type === 'state' && evt.new === 'CONNECTED'){
                console.log("JUST CONNECTED ::::::::::::::::::: "+evt);
            }
            if(evt.type === 'state' && evt.old === 'DISCONNECTED' && evt.new === 'CONNECTED'){
                console.log("CONNECTED OK::::::::::::::::::: "+evt);

                that.streamArrived = true;
                clearInterval(that.streamEventMonitor);
                that.count = 0;

                that.spinner.runPreload(false);
                that.scene.removeChild(that.spinner);
                that.spinner = null;

                callback();

                window.top.switchSource("bgMusic");
                //that.scene.controlBoard.soundToggled = backgroundMusicIsMuted === "false";
                //that.scene.controlBoard.soundBtnHandler();

                window.top.playHelperSound("machineNoise", {loop:-1, volume:1});
                // hajkova zelja, ako se skloni ovaj blok igra se ponasa tako sto pamti stanje backgroung muzike
                // i na osnovu toga pusta ili ne pusta zvuk kada se udje u masinu ili se vrati u lobi iz masine
                //if(that.scene.controlBoard.soundToggled === false){
                //that.scene.controlBoard.soundBtnHandler();
                //}

                stage.addEventListener("stagemousedown", function onceOnly(){
                    stage.removeEventListener("stagemousedown",onceOnly);
                    window.top.stopHelperSound("machineNoise");
                    that.videoElement.muted = false;
                });
            }
        };

        //this.webrtcPlayer.start();
        this.webrtcPlayer.startGameplayStream();//test dacha

        var spinner = this.spinner = new alteastream.MockLoader();
        this.scene.addChild(spinner);
        spinner.setLabel("Connecting to stream..");
        spinner.x = alteastream.AbstractScene.GAME_WIDTH *0.5;
        spinner.y = (alteastream.AbstractScene.GAME_HEIGHT*0.58);
        spinner.runPreload(true);

        this.streamEventMonitor = setInterval(function(){
            if(that.streamArrived === false){
                if(that.count<10){
                    that.count++;
                    console.log("counting now "+that.count);
                }else{
                    clearInterval(that.streamEventMonitor);
                    that.count = 0;
                    that.webrtcPlayer.start();
                }
            }
        },1000);
    };

    p.stop = function(){
        this.webrtcPlayer.stop();
    };

    p.reset = function(){

    };

    alteastream.VideoStream = VideoStream;
})();