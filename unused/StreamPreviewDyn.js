
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreviewDyn = function(){
        this.Container_constructor();
        this.initStreamPreviewDyn();
    };

    var p = createjs.extend(StreamPreviewDyn,createjs.Container);

    p.startX = 0;
    p.sWidth = 0;
    p.sHeight = 0;
    p.SPACING_LEFT = 8;
    p.SPACING_TOP = 10;
    p.SPACING_OVERLAY = 7;
    p.IN_FOCUS_SCALE = 65.5 + "%";
    p.IN_FOCUS_X = 0.270;
    p.IN_FOCUS_Y = 0.125;
    p.player = null;
    p.currentStream = "";
    p._active = false;
    p._wowzaPlayerOptions = {
        "license":"PLAY1-nByuW-4enAj-fD37H-9r9kK-cux7u",
        "title":"",
        "description":"",
        "sourceURL":"http%3A%2F%2F84.199.144.157%3A1935%2Flive%2Fmachine2322.stream%2Fplaylist.m3u8",
        "autoPlay":false,
        "mute":true,
        "loop":false,
        "audioOnly":false,
        ///"debugLevel":"OFF",
        "uiShowQuickRewind":false
    };

    var _instance = null;

    p.initStreamPreviewDyn = function () {
        _instance = this;
        var videoHolder = this.videoHolder = document.createElement('div');
        videoHolder.id = 'playerElement';
        videoHolder.style.top = 0;
        videoHolder.style.left = 0;
        videoHolder.style.width = 65.5 + "%";
        videoHolder.style.height = 65.5 + "%";
        //videoHolder.style.width = 17.3 + "%";//small start
        //videoHolder.style.height = 22.5 + "%";//small start
        videoHolder.style.position = "absolute";
        videoHolder.style.backgroundColor = "none";
        videoHolder.style.visibility = "hidden";
        videoHolder.style.border = "none";
        document.getElementById("wrapper").appendChild(videoHolder);

        var overlay = this.overlay = document.createElement('div');
        overlay.id = 'overlay';
        overlay.style.backgroundColor = "rgba(0, 0, 0, 0.01)";
        overlay.style.top = 0;
        overlay.style.left = 0;
        //overlay.style.width = 17.3 + "%";//small start
        //overlay.style.height = 22.5 + "%";//small start
        overlay.style.width = 67 + "%";
        overlay.style.height = 67 + "%";
        overlay.style.position = "fixed";
        overlay.style.visibility = "hidden";
        document.getElementById("wrapper").appendChild(overlay);

        var bgShape = this.bgShape = new createjs.Shape();
        bgShape.graphics.beginFill("#000").drawRect(0,0,1382,920);
        bgShape.x = 438;
        bgShape.y = 82;
        bgShape.alpha = 0.85;
        this.addChild(bgShape);
        bgShape.cache(0,0,1380,920);
        bgShape.visible = false;

        var border = this.border = new createjs.Shape();
        border.graphics.beginStroke("#94fffd").setStrokeStyle(5).drawRect(0,0,1268,800);
        border.x = 498;
        border.y = 150;
        this.addChild(border);
        border.cache(0,0,1268,800);
        border.visible = false;

        var labelsContainer = this.labelsContainer = new createjs.Container();
        this.addChild(labelsContainer);
        var xPos = 505;
        var xPos2 = 660;
        var font = "15px Hurme";
        var col_1 = "#c0dbff";
        var col_2 = "#FFF";
        this._createDynamicText("biggestGameWinLabel","BIGGEST GAME WIN: ",font,col_1,{x:xPos,y:875,textAlign:"left",textBaseline:"middle"},labelsContainer);
        this._createDynamicText("biggestGameWinValue","",font,col_2,{x:xPos2,y:875,textAlign:"left",textBaseline:"middle"},labelsContainer);

        this._createDynamicText("tokensFiredLabel","TOKENS FIRED: ",font,col_1,{x:xPos,y:895,textAlign:"left",textBaseline:"middle"},labelsContainer);
        this._createDynamicText("tokensFiredValue","",font,col_2,{x:xPos2,y:895,textAlign:"left",textBaseline:"middle"},labelsContainer);

        this._createDynamicText("lastWinLabel","LAST WIN: ",font,col_1,{x:xPos,y:915,textAlign:"left",textBaseline:"middle"},labelsContainer);
        this._createDynamicText("lastWinValue","",font,col_2,{x:xPos2,y:915,textAlign:"left",textBaseline:"middle"},labelsContainer);

        this._createDynamicText("totalWinLabel","TOTAL WIN: ",font,col_1,{x:xPos,y:935,textAlign:"left",textBaseline:"middle"},labelsContainer);
        this._createDynamicText("totalWinValue","",font,col_2,{x:xPos2,y:935,textAlign:"left",textBaseline:"middle"},labelsContainer);
        labelsContainer.visible = false;

        var spinner = this.spinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = 1130;
        spinner.y = 490;
        spinner.mouseEnabled = false;

        var videoStream = this.videoStream = new createjs.DOMElement(videoHolder);
        this.addChild(videoStream);
        this.videoStream.visible = false;

        this._blockUIOverlay(false);
        this.setDynamicPosition(0.270,0.125);//big start
        //this.setDynamicPosition(0.238,0.095);//small start

        this.createPlayer(videoHolder.id, this._wowzaPlayerOptions);

        var buttonExitTxt = new createjs.Text("< BACK", "16px Hurme", "#fff").set({textAlign:"center",textBaseline:"middle",mouseEnabled:false});
        var buttonExit = this.buttonExit = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMockSS),3,buttonExitTxt);
        buttonExit.regX = 70;
        buttonExit.regY = 18;
        buttonExit.x = 1690;
        buttonExit.y = 130;
        this.addChild(buttonExit);
        buttonExit.visible = false;
        buttonExit.setClickHandler(function (e) {
            this.exitPreview();
        }.bind(this));

        window.addEventListener("resize",function(){
            _instance._resize();
        });

        //setTimeout(function(){this.scaleToFocus(true);}.bind(this),5000);
    };

    StreamPreviewDyn.getInstance = function(){
        return _instance;
    };

    p.activate = function(){
        this.player = WowzaPlayer.get(this.videoHolder.id);

        //this._hideWowzaContainer(true);//small start
        //this._hideWowzaUI(true);//small start
        //this.playStartedListener();
        //this.playStateListener();
    };

    p.exitPreview = function(){
        if(!this._active)
            return;
        this._active = false;

        alteastream.QueuePanel.getInstance().onStreamPreviewDynExit();
        alteastream.Lobby.getInstance().onStreamPreviewDynExit();

        this._hideWowzaUI(false);
        this.buttonExit.visible = false;
        this.currentStream = "";
        this.show(false);
        this.hidePlayer(true);
        this.spinner.runPreload(false);
        this.destroyPlayer();
        this._blockUIOverlay(false);
    };

    p.updateInfo = function(machineInfo){
        this.spinner.setLabel("Click for live preview of machine "+machineInfo.id);//Connecting to machine
        this.currentStream = machineInfo.id;

        this.infoObj = machineInfo.infoObject;
        var i = 0;
        for(var key in this.infoObj){
            var prop = Object.keys(this.infoObj)[i];
            this[prop+"Value"].text = this.infoObj[key];
            i++;
        }
    };

    p.changeStream = function(src){
        this._active = true;
        this.show(true);
        this.bgShape.visible = true;
        this.border.visible = true;
        this.labelsContainer.visible = false;

        this.hidePlayer(false);
        this.destroyPlayer();

        var id = this.videoHolder.id;
        this._wowzaPlayerOptions.sourceURL = src;
        this.createPlayer(id, this._wowzaPlayerOptions);
        this._hideWowzaContainer(true);
        this._hideWowzaUI(true);

        this.player = WowzaPlayer.get(id);
        this.spinner.runPreload(true);
        this.playStartedListener();
        this.playStateListener();
    };

    p.playStateListener = function(){
        var stateIsChanged = function ( stateChangedEvent ) {
            if (stateChangedEvent.currentState === WowzaPlayer.State.PLAYING) {
                _instance.player.removeOnStateChanged (stateIsChanged);
                setTimeout(function(){
                    _instance._hideWowzaContainer(false);
                    _instance.spinner.runPreload(false);
                    _instance.buttonExit.visible = true;
                    _instance.labelsContainer.visible = true;
                    alteastream.QueuePanel.getInstance().onStreamPreviewDynEnter();
                },3000);
            }
        };
        this.player.onStateChanged( stateIsChanged );
    };

    p.playStartedListener = function ( ) {
        var playStarted = function ( ) {
                _instance.player.removeOnPlay(playStarted);
                _instance._blockUIOverlay(true);
        };
        this.player.onPlay( playStarted );
    };

    p.onQueueEnterResponse = function(){
        this.buttonExit.setDisabled(true);
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p.createPlayer = function(id, options){
        WowzaPlayer.create(id, options);
    };

    p.hidePlayer = function(bool){
        this.videoStream.visible = !bool;
    };

    p.destroyPlayer = function(){
        if(this.player!==null){
            this.player.destroy();
            this.player = null;
        }
    };

    p.dispose = function(){
        this.player.finish();
        this.destroyPlayer();
    };

    p.reset = function(){
        this.buttonExit.visible = true;
        this.buttonExit.setDisabled(false);
    };

    p._resize = function(){
        var w = window.innerWidth;
        var h = window.innerHeight;
        var spacingX = this.SPACING_LEFT*(w/1000);
        var spacingY = this.SPACING_TOP*(w/1000);
        var spacingOverlay = this.SPACING_OVERLAY*(w/1000);

        var dynX = this._getDynamicPosition().x;
        var dynY = this._getDynamicPosition().y;

        this.videoStream.x = w*dynX-spacingX;
        this.videoStream.y = h*dynY+spacingY;

        this.overlay.style.left = w*dynX-(spacingX+spacingOverlay)+"px";
        this.overlay.style.top = h*dynY+(spacingY-spacingOverlay)+"px";

        //if(document.getElementById("playerElement-OverlayPlay"))//small start
        //document.getElementById("playerElement-OverlayPlay").style.display = "block";//small start
    };

    p.setDynamicPosition = function(xPerc,yPerc){
        this._dynamicPositionXY = {x:xPerc,y:yPerc};
        this._resize();
    };

    p.scaleToFocus = function(bool){//small start
        if(bool === true){
            this.videoHolder.style.width = this.IN_FOCUS_SCALE;
            this.videoHolder.style.height = this.IN_FOCUS_SCALE;
            this.overlay.style.width = 67 + "%";
            this.overlay.style.height = 67 + "%";
            this.setDynamicPosition(this.IN_FOCUS_X,this.IN_FOCUS_Y);
        }
    };

    p._getDynamicPosition = function(){
        return this._dynamicPositionXY;
    };

    p._createDynamicText = function(instance,text,font,color,props,parent){
        var textInstance = this[instance] = new alteastream.DynamicText(text,font,color).set(props);
        parent.addChild(textInstance);
    };

    p._blockUIOverlay = function(bool){
        this.overlay.style.visibility = bool === true?"visible":"hidden";
    };
    
    p._hideWowzaUI = function(bool){
        document.getElementById("playerElement-UI").style.opacity = bool === true?0.01:1;
    };
    
    p._hideWowzaContainer = function(bool){
        document.getElementById("playerElement-Container").style.backgroundColor = bool === true?"rgba(0, 0, 0, 0.01)":"#000000";
    };

    alteastream.StreamPreviewDyn = createjs.promote(StreamPreviewDyn,"Container");
})();