/**
 * Created by Dacha on 11.04.14.
 */

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ErrorPopUp = function(exception,scene) {
        this.Container_constructor();
        this.initialize_ErrorPopUp(exception,scene);
    };
    var p = createjs.extend(ErrorPopUp, createjs.Container);

// static properties:

// events:
// public properties:
// private properties:
    p.exceptionObject = {};

// constructor:

    p.initialize_ErrorPopUp = function(exception,scene) {
        this._init(exception,scene);
    };
// static methods:
// public methods:
    p.dispose = function(){
        this.removeAllChildren();
    };

// private methods:
    p._init = function(exception,scene){
        this.exceptionObject.method = "WARNING:";//TEMP
        this.exceptionObject.message = exception;

        var fontBig = "bold 20px Verdana";
        var font = "18px Verdana";
        var black = "#000";
        var red = "#ff0000";
        var yellow = "#fff000";
        var alignCenter = "center";
        var exceptionMessage = exception.message || "message undefined";

        var _width = alteastream.AbstractScene.GAME_WIDTH || document.getElementById("screen").width;
        var _height = alteastream.AbstractScene.GAME_HEIGHT || document.getElementById("screen").height;

        var bg = new createjs.Shape(new createjs.Graphics().beginFill(black).drawRect(0, 0, _width, _height));
        bg.alpha = 0.6;
        bg.cache(0, 0, _width, _height);

        var infoFieldWidth = 570;
        var infoFieldHeight = 54;
        var infoFieldX = _width/2 - infoFieldWidth/2;
        var infoFieldY = _height/2;

        var infoField = new createjs.Shape(new createjs.Graphics().setStrokeStyle(3).beginStroke(yellow).beginFill(black).drawRect(infoFieldX, infoFieldY, infoFieldWidth, infoFieldHeight));
        infoField.cache(infoFieldX, infoFieldY, infoFieldWidth, infoFieldHeight);

        var methodTfX = infoFieldX+infoFieldWidth/2;
        var methodTfY = (infoFieldY+infoFieldHeight/2)-5;

        var messageTfX = infoFieldX+infoFieldWidth/2;
        var messageTfY = (infoFieldY+infoFieldHeight/2)+15;

        var methodTf = new createjs.Text(String(exception.method), fontBig, red).set({x:methodTfX,y:methodTfY,textAlign:alignCenter});
        var messageTf = new createjs.Text(String(exceptionMessage), font, yellow).set({x:messageTfX,y:messageTfY,textAlign:alignCenter});
        this.addChild(bg,infoField,methodTf,messageTf);
    };

    alteastream.ErrorPopUp = createjs.promote(ErrorPopUp,"Container");
})();