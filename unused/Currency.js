/**
 * Created by Dacha on 12/19/14.
 */

    // namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var Currency = function (currencySymbol) {
        this.Container_constructor();
        this.initialize_Currency(currencySymbol);
    };

    var p = createjs.extend(Currency, createjs.Container);

    //static properties:
    //events:
    //public vars:
    //private vars:
    p._currencySymbol = null;

    //constructor:
    p.initialize_Currency = function(currencySymbol){
        this._currencySymbol = currencySymbol;
    };

    //static methods:
    //public functions:
    p.appendTo = function(text, props){
        var currencyTextName = text.name+"currencyHead";
        //var currTxt = new alteastream.BitmapText("(" + this._currencySymbol + ")",text.font,text.color,{textAlign:"center",textBaseline:"center",name:currencyTextName});
        var currTxt = new createjs.Text("(" + this._currencySymbol + ")",text.font,text.color).set({textAlign:"center",textBaseline:"center",name:currencyTextName});
        currTxt.y = text.y;
        currTxt.x = text.x + text.getMeasuredWidth() + 10;
        if(props){ gamecore.Utils.DISPLAY.setProps(currTxt,props); }
        text.parent.addChild(currTxt);
    };

    /*p.align = function(text){
     var currTxt = text.parent.getChildByName(text.name+"currencyHead");
     currTxt.y = text.y;
     currTxt.x = this._setXPosition(text);
     }

    p.setVisibleOn = function(text,bool){
        text.parent.getChildByName(text.name+"currencyHead").visible = bool;
    }*/

    //private functions:
    alteastream.Currency = createjs.promote(Currency,"Container");
})();