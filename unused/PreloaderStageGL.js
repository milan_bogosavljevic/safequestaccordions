

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
var PreloaderStageGL = function(stage) {
    this.StageGL_constructor(stage);
    this.initialize(stage);
}
var p = PreloaderStageGL.prototype = createjs.extend(PreloaderStageGL,createjs.StageGL);

// static properties:
// events:
// public properties:


// private properties:
    p._percent = null;
    p._registrants = null;
    p.screenWidth = null;
    p.screenHeight = null;
// constructor:
    //p.Stage_initialize = p.init;
    p.initialize = function(stage) {
        //this.Stage_initialize(stage);
        this._percent = new alteastream.Percent();
        this._registrants = [];
    };

// static methods:
// public methods:
    p.getProgress = function() {
        return this._percent.getDecimal();
    };

    p.setProgress = function(percent) {
        this._percent.set(percent);
        this.propagate();
    };

    p.register = function(observer) {
        if(!typeof observer.isPreloaderComponent === 'function'
            || !observer.isPreloaderComponent())
            throw "Can not register non PreloaderStageGLComponet object instance";

        this._registrants.push(observer);
    };

    p.propagate = function() {
        for(var index in this._registrants)
            this._registrants[index].setProgress(this._percent.getDecimal());

        // temp solution
        //this.update();
    };

    p.setSize = function(document){
        var canvas = document.getElementById('screen');
        this.screenWidth = canvas.width;
        this.screenHeight = canvas.height;
        //console.log("screenWidth "+this.screenWidth);
        //console.log("screenHeight "+this.screenHeight);
    };

    p.onResize = function(window) {
        console.log("ONRESIZE");
        var gameArea = document.getElementById('wrapper');
        var w = window.innerWidth;
        var h = window.innerHeight;

        var gameWidth = this.screenWidth;
        var gameHeight = this.screenHeight;

        this.scaleX = w / gameWidth;
        this.scaleY = h / gameHeight;

        gameArea.style.width = 100 + '%';
        gameArea.style.height = 100 + '%';

        gameArea.style.marginTop = 0 + 'px';
        gameArea.style.marginLeft = 0 + 'px';

        this.canvas.width = w/*gameWidth * this.scaleX*/;
        this.canvas.height = h/*gameHeight * this.scaleY*/;
        //console.log("canvas width "+this.canvas.width);
        this.update();
    };

    p.render = function(){
        var that = this;
        createjs.Ticker.timingMode = createjs.Ticker.RAF;
        that.enableMouseOver(20);
        createjs.Ticker.addEventListener("tick", function(event) {
            that.update(event);
            //if(that.fpsText){ that.fpsText.text = String(createjs.Ticker.getMeasuredFPS().toFixed(0))+" fps";}///
        });
    };


// private methods:

alteastream.PreloaderStageGL = createjs.promote(PreloaderStageGL,"StageGL");
})();