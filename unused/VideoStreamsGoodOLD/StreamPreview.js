
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreview = function(){
        this.Container_constructor();
        this.initStreamPreview();
    };

    var p = createjs.extend(StreamPreview,createjs.Container);

    // private properties
    p._count = 0;
    p._streamEstablished = false;
    p._attempts = 0;
    p._active = false;
    p._changeStreamTimeout = null;
    // public properties
    p.startX = 0;
    p.sWidth = 0;
    p.sHeight = 0;
    p.player = null;
    p.currentStream = "";
    p.currentMachineName = "";
    p.webRtcPlayer = null;

    var _instance = null;

    p.initStreamPreview = function () {
        console.log("StreamPreview initialized:::");
        _instance = this;

        var options = {id:"videoOutput",width:1920,height:1080,style:{width:"100%",height:"100%"}};
        this.videoElement = new alteastream.ABSVideoElement(options);

        var frame = this.frame = alteastream.Assets.getImage(alteastream.Assets.images.window_big);
        frame.x = -100;// location machines x = 100
        frame.y = -121;// location machines y = 121
        frame.scaleX = 4;
        frame.scaleY = 4;
        this.addChild(frame);
        frame.mouseEnabled = false;

        var loadAnimation = new alteastream.SSAnimator("animatedLogo",250,244,19);
        var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        this.addChild(spinner);
        spinner.x = 864;
        spinner.y = 372;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;
    };

    StreamPreview.getInstance = function(){
        return _instance;
    };

    // private methods
    p._onLoading = function () {
        this.frame.visible = true;
        this.spinner.setLabel("Connecting to machine "+_instance.currentMachineName);
        this.spinner.runPreload(true);
    };

    p._onLoaded = function () {
        this.spinner.runPreload(false);
        this.frame.visible = false;
        console.log("StreamLoaded::::");
    };

    p._startMonitor = function(machineInfo){
        var that = this;
        this.streamEventMonitor = setInterval(function(){
            if(!that._streamEstablished){
                if(that._attempts<3){
                    if(that._count<6){
                        that._count++;
                        console.log("monitoring.. "+that._count);
                    }else{
                        that._attempts++;
                        that._resetMonitor();
                        that.switchStream(machineInfo);
                        //that.changeStream(machineInfo); // <= nedostajalo? poslednja promena sa stream preview roulette uslovima? switchStream() je sadrzao i changeStream()..?
                        that.spinner.setLabel("Fetching "+_instance.currentMachineName+" preview","#ffcc00");
                    }
                }else{
                    that._stopMonitor();
                    that._attempts = 0;
                    that.spinner.runPreload(false);
                    that.spinner.preloadInfoText.visible = true;
                    that.spinner.setLabel("Preview for "+_instance.currentMachineName+" failed","#ff0000");
                }
            }
        },1000);
    };

    p._resetMonitor = function(){
        clearInterval(this.streamEventMonitor);
        this._count = 0;
    };

    p._stopMonitor = function(){
        this._resetMonitor();
        this._streamEstablished = false;
    };

    p._streamConnected = function(){
        this._streamEstablished = true;
        this._attempts = 0;
        this._resetMonitor();
    };

    p._clearPreview = function(){
        this.currentStream = "";
        this.show(false);
        this.destroyPlayer();
        this.spinner.runPreload(false);
        this.frame.visible = true;
    };

    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.labelsContainer.addChild(textInstance);
        textInstance.mouseEnabled = false;
    };

    // public methods
    p.start = function(){
        this._active = true;
        alteastream.LocationMachines.getInstance().resetIntervalUpdateLounge();
    };

    p.exitPreview = function(){
        this._active = false;
        clearTimeout(this._changeStreamTimeout);
        this._stopMonitor();
        this._clearPreview();
    };

    p.updateInfo = function(machineInfo){
        this.machineInfo = machineInfo;
        alteastream.AbstractScene.GAME_CODE = machineInfo.gameCode;
        alteastream.AbstractScene.DEFAULT_GAME_ID = machineInfo.defaultGameId;
    };

    p.activate = function(){
        if(!this.webRtcPlayer){
            this.webRtcPlayer = new WEBRTCGamePlayer(this.videoElement.htmlVideo);
            this._local_activate();// local ver
        }
    };

    p.tryToSwitchStream = function(machine) {
        if(this.currentStream !== machine.machineInfo.machineIndex){// if !machine.selected?
            alteastream.MachinesComponent.getInstance().clearActiveMachines();
            alteastream.SocketCommunicatorLobby.getInstance().lobbySwitchSubscriptions(this.currentStream,machine.machineInfo.machineIndex);
            alteastream.QueuePanel.getInstance().setCurrentMachine(machine.machineInfo);

            machine.selectMachine(true);
            // stream preview roulette
            if(alteastream.Lobby.getInstance().selectedLocation.casinoName === "RouletteStaging"){
                this.switchStream(machine.machineInfo);
                var obj = {
                    previewCamera:machine.machineInfo.previewCamera,
                    shopMachine:alteastream.AbstractScene.SHOP_MACHINE,
                    machineName:machine.machineInfo.machineName,
                    casinoName:alteastream.Lobby.getInstance().selectedLocation.casinoName
                }

                var objToStore = JSON.stringify(obj);
                window.localStorage.setItem("streamPreviewParams", objToStore);

            }// stream preview roulette
            else{
                if(!machine.isFreeToPlay){
                    this.switchStream(machine.machineInfo);
                    this.changeStream(machine.machineInfo);
                    this.changeFrameImage(machine.thumb);
                }else{
                    this.updateInfo(machine.machineInfo);
                }
            }
        }
    };

    p.switchStream = function(machineInfo){
        this.clearVideoElement();
        this.spinner.runPreload(false);
        this.currentStream = machineInfo.machineIndex;
        this.currentMachineName = machineInfo.machineName;
        //this.changeStream(machineInfo);
        this.updateInfo(machineInfo);
        this.start();
    };

    p.changeStream = function(machineInfo){
        /*this.currentStream = machineInfo.machineIndex;
        this.currentMachineName = machineInfo.machineName;*/
        this.show(true);
        this._stopMonitor();

        clearTimeout(this._changeStreamTimeout);

        this._changeStreamTimeout = setTimeout(function () {
            _instance.webRtcPlayer.setPreview(machineInfo.previewCamera,alteastream.AbstractScene.SHOP_MACHINE,machineInfo.machineName);// soket, id kuce, naziv masine
            _instance.webRtcPlayer.start();
            _instance.webRtcPlayer.onStreamEvent = function(evt){
                if(evt.type === 'state' && evt.new === 'CONNECTED'){
                    console.log("JUST CONNECTED ::::::::::::::::::: "+evt);
                }
                if(evt.type === 'state' && evt.old === 'DISCONNECTED' && evt.new === 'CONNECTED'){
                    console.log("CONNECTED OK::::::::::::::::::: "+evt);

                    _instance._streamConnected();
                    _instance._onLoaded();
                }
            };
        },1000);

        this._onLoading();
        this._startMonitor(machineInfo);
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p.destroyPlayer = function(){
        this.webRtcPlayer.stop();
        this.clearVideoElement();
    };

    p.dispose = function(){
        clearTimeout(this._changeStreamTimeout);
        this._stopMonitor();
        this.destroyPlayer();
        console.log("DISPOSING STREAM PREVIEW");
    };

    p.clearVideoElement = function(){
        this.videoElement.clear();
    };

    p.changeFrameImage = function(thumb) {
        if(this.frame.visible){
            gamecore.Utils._removeAllFilters([this.frame]);
            this.frame.image = thumb.hasThumbImage === true ? thumb.image : alteastream.Assets.getImage(alteastream.Assets.images.thumb).image;
            gamecore.Utils.DISPLAY.FILTERS.setBlur([this.frame], 2);
        }
    };

    p.adjustMobile = function(){
        var footer = new createjs.Shape();
        footer.graphics.beginFill("#000000").drawRoundRect(0, 0, 616, 40, 3);
        footer.cache(0, 0, 616, 40);
        footer.alpha = 0.3;
        footer.x = 319;
        footer.y = 429;

        this.addChildAt(footer, 0);

        this.frame.x = -10;// location machines x = 10
        this.frame.y = -10;// location machines y = 10
        this.frame.scaleX = 2;
        this.frame.scaleY = 2;

        if(this.spinner)
            this.removeChild(this.spinner);

        var loadAnimation = new alteastream.SSAnimator("animatedLogo",125,122,19);
        var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        this.addChild(spinner);
        spinner.x = 470;
        spinner.y = 160;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;

        //this.removeChild(this.frame);//todo proveriti
    }

    p._local_activate = function(){
        this.videoElement.setSource("videotest.mp4");
        this.videoElement.play();
        this.videoElement.setLoop(true);
        this.videoElement.setMuted(false);

        this.currentStream = 333333333;
        this.start();

        this._onLoading();
        setTimeout(function(){
            _instance._onLoaded();
        },1000);
    };

    alteastream.StreamPreview = createjs.promote(StreamPreview,"Container");
})();