this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var VideoStreamSmall = function (scene,id,address){
        this.initialize_VideoStreamSmall(scene,id,address);
    };

    var p = VideoStreamSmall.prototype;

    p._count = 0;
    p._attempts = 0;
    p._streamEstablished = false;
    p.spinner = null;
    p.spinnerBackground = null;
// static properties:
// events:

// public vars:
// private vars:
// constructor:
    p.initialize_VideoStreamSmall = function (scene,id,address){

        this.scene = scene;

        var videoID = this.videoID = id;

        //check here for mobile for width/height 960/540
        //var videoTag ="<video id ='"+videoID+"' width = '1280' height='720' autoplay muted playsinline></video>";

        var isMobile = window.localStorage.getItem('isMobile');
        var videoWidth = isMobile === "not" ? 1280 : 960;
        var videoHeight = isMobile === "not" ? 720 : 540;
        var videoTag ="<video id ='"+videoID+"' width = '"+videoWidth+"' height= '"+videoHeight+"' autoplay muted playsinline></video>";

        var videoElement = this.videoElement = $(videoTag).appendTo(document.getElementById("wrapper"))[0];
        document.getElementById("wrapper").insertBefore(document.getElementById(videoID), document.getElementById("wrapper").firstChild);

        var style = videoElement.style;
        style.width = "100%";
        style.height = "100%";
        style.pointerEvents = "none";

        style["video::-webkit-media-controls-panel"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-play-button"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-start-playback-button"] = "display: none!important;";
        style["video::-webkit-media-controls-start-playback-button"] = "-webkit-appearance: none;";

        var webrtcPlayer = this.webrtcPlayer = new WEBRTCGamePlayer(this.videoElement);

        // stream preview roulette
        /* // local ver
        var streamPreviewParams = JSON.parse(localStorage.getItem("streamPreviewParams"));
        //temp HC "testroulette" instead of streamPreviewParams.machineName
        this.webrtcPlayer.setPreview(streamPreviewParams.previewCamera, streamPreviewParams.shopMachine, "testroulette");
        */ // local ver
        this.webrtcPlayer.setPreview(address.webrtcurl,alteastream.AbstractScene.GAME_TOKEN,alteastream.AbstractScene.GAME_ID);// local ver
        // stream preview roulette

        webrtcPlayer.onError = function(error) {
            if(error) {
                //{"id":"error","code":-7,"txt":"User session does not exist"}
                //{"id":"error","code":13,"txt":"User token is expired or does not exist"}
                var that = this;
                this.scene.throwAlert(alteastream.Alert.ERROR,error,function(){
                    webrtcPlayer.stop();
                   // that.scene.socketCommunicator.disposeCommunication();//quitPlay() if started
                    //that.scene.exitMachineGameplay();
                });
            }
        }.bind(this);

        var videoStream = this.videoStream = new createjs.DOMElement(videoElement);
        alteastream.Roulette.getInstance().addChildAt(videoStream,0);
        this.videoStream.scaleX = 0.32;
        this.videoStream.scaleY = 0.32;
        this.videoStream.mouseEnabled = false;

        this.showVideoStream(false);
        //this.setDynamicPosition(-0.0830,0.605);//-0.05,0.605
        this.setDynamicPosition(-0.053,0.605);

        var _this = this;
        window.addEventListener("resize",function(){
            _this._resize();
        });

        this._local_activate();// local ver
    };

    p.showVideoStream = function (show) {
        this.videoStream.visible = show;
    };

    p.setDynamicPosition = function(xPerc,yPerc){
        this._dynamicPositionXY = {x:xPerc,y:yPerc};
        this._resize();
    };

    p._getDynamicPosition = function(){
        return this._dynamicPositionXY;
    };

    p._resize = function(){
        var w = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        var h = (window.innerHeight > 0) ? window.innerHeight : screen.width;

        //this.SPACING_LEFT
        //this.SPACING_TOP
        var spacingX = 2*(w/1000);
        var spacingY = 2*(w/1000);

        var dynX = this._getDynamicPosition().x;
        var dynY = this._getDynamicPosition().y;

        this.videoStream.x = w*dynX-spacingX;
        this.videoStream.y = h*dynY+spacingY;
    };

    p.activate = function(callback){
        var that = this;
        //var backgroundMusicIsMuted = window.localStorage.getItem('backgroundMusicIsMuted');

        this._streamEstablished = false;
        this.webrtcPlayer.onStreamEvent = function(evt){
            if(evt.type === 'state' && evt.new === 'CONNECTED'){
                console.log("JUST CONNECTED ::::::::::::::::::: "+evt);
            }
            if(evt.type === 'state' && evt.old === 'DISCONNECTED' && evt.new === 'CONNECTED'){
                console.log("CONNECTED OK::::::::::::::::::: "+evt);

                that._streamConnected();

                //that.scene.removeChild(poster);

                /*var playMachineNoiseSound = true;

                if(is.safari()){
                    //var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
                    //if(is.ipad() || is.ios() || isiPadOS){
                        playMachineNoiseSound = false;
                        that.scene.setIosOverlay();
                    //}
                }*/

                if(that.spinner){
                    that.spinner.runPreload(false);
                    that.scene.removeChild(that.spinner);
                    that.scene.removeChild(that.spinnerBackground);
                    that.spinner = null;
                }

                callback();

                //window.top.switchSource("bgMusic");
                //that.scene.controlBoard.soundToggled = backgroundMusicIsMuted === "false";
                //that.scene.controlBoard.soundBtnHandler();

                //window.top.playHelperSound("machineNoise", {loop:-1, volume:1});
                // hajkova zelja, ako se skloni ovaj blok igra se ponasa tako sto pamti stanje backgroung muzike
                // i na osnovu toga pusta ili ne pusta zvuk kada se udje u masinu ili se vrati u lobi iz masine
                //if(that.scene.controlBoard.soundToggled === false){
                //that.scene.controlBoard.soundBtnHandler();
                //}

                /*var soundMachineIsToggled = window.localStorage.getItem("soundMachineIsToggled");
                if(soundMachineIsToggled !== null) {
                    soundMachineIsToggled = Boolean(soundMachineIsToggled);
                }else{
                    soundMachineIsToggled = false;
                }

                if(playMachineNoiseSound === true && soundMachineIsToggled === false){
                    window.top.playHelperSound("machineNoise", {loop: -1, volume: 1});
                    stage.addEventListener("stagemousedown", function onceOnly() {
                        stage.removeEventListener("stagemousedown", onceOnly);
                        window.top.stopHelperSound("machineNoise");
                        that.videoElement.muted = false;
                        that.videoElement.volume = 0.08;
                    });
                }

                that.scene.controlBoard.restoreMachineSoundState();
                that.scene.controlBoard.restoreMusicSoundState();*/
            }
        };

        this.webrtcPlayer.startGameplayStream();

        // todo add here some poster image, so that loading screen is not black
        /*var poster = alteastream.Assets.getImage(alteastream.Assets.images.poster);
        this.scene.addChild(poster);
        poster.mouseEnabled = false;*/

/*        var spinnerBackground = this.spinnerBackground = alteastream.Assets.getImage(alteastream.Assets.images.liveStreamConnecting);
        spinnerBackground.regX = spinnerBackground.image.width*0.5;
        spinnerBackground.regY = spinnerBackground.image.height*0.5;
        spinnerBackground.x = alteastream.AbstractScene.GAME_WIDTH *0.5;
        spinnerBackground.y = alteastream.AbstractScene.GAME_HEIGHT *0.5;*/

/*        var spinnerBackground = this.spinnerBackground = new createjs.Shape();
        spinnerBackground.graphics.beginFill("#000000").drawRect(0, 0, 344, 344);
        spinnerBackground.x = 29;
        spinnerBackground.y = 656;

        var spinner = this.spinner = new alteastream.MockLoader();
        //spinner.customScaleGfx(0.4);
        this.scene.addChild(spinnerBackground, spinner);
        //spinner.setLabel("Stream connecting...");
        spinner.x = spinnerBackground.x + 172;
        spinner.y = spinnerBackground.y + 172;
        spinner.runPreload(true);*/

        this._startMonitor();
    };

    p.addSpinner = function () {
        if(!this._streamEstablished){
            var spinnerBackground = this.spinnerBackground = new createjs.Shape();
            spinnerBackground.graphics.beginFill("#000000").drawRect(0, 0, 344, 344);
            spinnerBackground.x = 29;
            spinnerBackground.y = 656;

            var spinner = this.spinner = new alteastream.MockLoader();
            this.scene.addChild(spinnerBackground, spinner);
            spinner.x = spinnerBackground.x + 172;
            spinner.y = spinnerBackground.y + 96;
            spinner.runPreload(true);
        }
    };

    p.stop = function(){
        this.webrtcPlayer.stop();
        this.clearVideoElement();
    };

    p.reset = function(){

    };

    p.clearVideoElement = function(){
        /*if(is.edge()){ //poslednje dodato 30.06, ne radi stream na Edge-u
            this.recreateVideoElement();
        }*/
        this.videoElement.removeAttribute('src');
        this.videoElement.load();
    };

    p._startMonitor = function(){
        var that = this;
        this.streamEventMonitor = setInterval(function(){
            if(!that._streamEstablished){
                if(that._attempts<5){
                    if(that._count<10){
                        that._count++;
                        console.log("monitoring.. "+that._count);
                    }else{
                        that._attempts++;
                        that._resetMonitor();
                        that.clearVideoElement();
                        that.webrtcPlayer.start();
                        console.log("attempt.. "+that._attempts);
                    }
                }else{
                    that._stopMonitor();
                    that._attempts = 0;
                    that.scene.throwAlert(alteastream.Alert.ERROR, "Streaming failed. Exiting game",function(){that.scene.onQuit();});
                }
            }
        },1000);
    };

    p._resetMonitor = function(){
        this._count = 0;
    };

    p._stopMonitor = function(){
        this._resetMonitor();
        this._streamEstablished = false;
        clearInterval(this.streamEventMonitor);
    };

    p._streamConnected = function(){
        this._streamEstablished = true;
        this._attempts = 0;
        this._resetMonitor();
        clearInterval(this.streamEventMonitor);
    };

    /*p.recreateVideoElement = function(){//poslednje dodato 30.06, ne radi stream na Edge-u
        var canvas = document.getElementById("screen");
        canvas.parentNode.removeChild(this.videoElement);
        var videoElement = this.videoElement = document.createElement("video");
        videoElement.id = "videoOutput";
        videoElement.autoplay = true;
        canvas.parentNode.insertBefore(videoElement,canvas);
        var style = videoElement.style;
        style.top = "0px";
        style.left ="0px" ;
        style.width = 100 + "%";
        style.height =  "0 auto";
        style.position = "absolute";
        style.pointerEvents = "none";

        this.webrtcPlayer.video = videoElement;
    };*/

    p._local_activate = function(){//click on screen while loading to start video
        this.videoElement.src = "../lobby/videotest.mp4";
        this.videoElement.play();
        this.videoElement.loop = true;
        this.videoElement.muted = false;

        //this.scene.controlBoard.restoreMachineSoundState();
        //this.scene.controlBoard.restoreMusicSoundState();
    };

    alteastream.VideoStreamSmall = VideoStreamSmall;
})();