
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var PreloaderComponentBar = function(stage,isDesktop) {
        this.initialize(stage,isDesktop);
    };

    var p = PreloaderComponentBar.prototype = new alteastream.PreloaderComponent();

// static properties:
// events:
// public properties:
// private properties:
// constructor:
    p.PreloaderComponent_initialize = p.initialize;

    p.initialize = function(stage,isDesktop) {
        this.PreloaderComponent_initialize(stage);
        // sredi koordinate
        var props = isDesktop === true ?
            {frameWidth:596, frameHeight:13, frameRoundness:6, font:"14px Arial", txtY:22, barWidth:592, barHeight:11, barMargin:2} :
            {frameWidth:298, frameHeight:7, frameRoundness:3, font:"10px Arial", txtY:13, barWidth:296, barHeight:6, barMargin:1};

        var shape1 = new createjs.Shape();
        shape1.graphics.beginFill("#ffffff").drawRoundRect(0, 0, props.frameWidth, props.frameHeight, props.frameRoundness);
        shape1.cache(0, 0, props.frameWidth, props.frameHeight, props.frameRoundness);

        /*var shape2 = new createjs.Shape();
        shape2.graphics.beginFill("#1fa38a").drawRoundRect(2, 2, 592, 9, 6);
        shape2.cache(2, 2, 592, 9, 6);*/

        var mask = new createjs.Shape();
        //mask.graphics.beginFill("#03cafc").drawRect(0, 0, 0, 0);
        mask.graphics.beginFill("#ffcb05").drawRect(0, 0, 0, 0);
        mask.cache(0, 0, 0, 0);
        //shape2.mask = mask;

        var text = new createjs.Text("LOADING...", props.font, "white");
        text.x = props.frameWidth/2;
        text.y = props.txtY;
        text.textAlign = "center";
        var cacheBounds = text.getBounds();
        text.cache(cacheBounds.x, cacheBounds.y, cacheBounds.width, cacheBounds.height);

        var r = 255;//31
        var g = 203;//163
        var b = 4;//138
        var start = 120;

        var r2 = 204;
        var g2 = 234;
        var b2 = 228;
        var start2 = 255;//255

        this.addChild(shape1);
        //this.addChild(shape2);
        this.addChild(mask);
        this.addChild(text);
        var that = this;
        this.addOnProgressChangeAction(function() {
            mask.uncache();
            mask.graphics.clear();
            var progress = props.barWidth*that.getProgress();
            var tmpR = Math.round(start + (r-start) * that.getProgress());
            var tmpG = Math.round(start + (g-start) * that.getProgress());
            var tmpB = Math.round(start + (b-start) * that.getProgress());
            mask.graphics.beginFill("rgb(" + tmpR + "," + tmpG + "," + tmpB + ")").drawRect(props.barMargin, props.barMargin, progress, props.barHeight);
            mask.cache(0,0,progress,props.barHeight);
            //mask.updateCache();

          /*  var tmpR = Math.round(start + (r-start) * that.getProgress());
            var tmpG = Math.round(start + (g-start) * that.getProgress());
            var tmpB = Math.round(start + (b-start) * that.getProgress());
            shape2.graphics.clear();
            shape2.graphics.beginFill("rgb(" + tmpR + "," + tmpG + "," + tmpB + ")").drawRoundRect(2, 2, 592, 9, 6);
            //shape2.cache(2, 2, 592, 9, 6);
            shape2.updateCache();

            var tmpR2 = Math.round(start2 + (r2-start2) * that.getProgress());
            var tmpG2 = Math.round(start2 + (g2-start2) * that.getProgress());
            var tmpB2 = Math.round(start2 + (b2-start2) * that.getProgress());
            shape1.graphics.clear();
            shape1.graphics.beginFill("rgb(" + tmpR2 + "," + tmpG2 + "," + tmpB2 + ")").drawRoundRect(0, 0, 596, 13, 6);//596
            //shape1.cache(0, 0, 596, 13, 6);
            shape1.updateCache();*/

        });
    };


// static methods:
// public methods:
// private methods:

    alteastream.PreloaderComponentBar = PreloaderComponentBar;
})();