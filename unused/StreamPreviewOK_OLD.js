
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreviewOK_OLD = function(){
        this.Container_constructor();
        this.initStreamPreviewOK_OLD();
    };

    var p = createjs.extend(StreamPreviewOK_OLD,createjs.Container);

    p.startX = 0;
    p.sWidth = 0;
    p.sHeight = 0;
    p.SPACING_LEFT = 8;
    p.SPACING_TOP = 10;
    p.player = null;
    p.currentStream = "";
    p._active = false;
    p.webrtcPlayer = null;

    var _instance = null;

    p.initStreamPreviewOK_OLD = function () {
        _instance = this;

        var videoID = this.videoID = "videoOutput";//id;
        var videoTag;

        videoTag ="<video id ='"+videoID+"' width = '1280' height='720' autoplay muted playsinline>";
        videoTag += "</video>";

        var videoElement = this.videoElement = $(videoTag).appendTo(document.getElementById("wrapper"))[0];
        //videoElement.poster = './assets/images/webrtc.png';

        var style = videoElement.style;
        style.top = 0;
        style.left = 0;
        style.width = 65.5 + "%";
        style.height = 65.5 + "%";
        style.position = "absolute";
        style.pointerEvents = "none";
        //style.zIndex = -1;

        var bgShape = this.bgShape = new createjs.Shape();
        bgShape.graphics.beginFill("#000").drawRect(0,0,1382,920);
        bgShape.x = 438;
        bgShape.y = 82;
        bgShape.alpha = 0.85;
        this.addChild(bgShape);
        bgShape.cache(0,0,1380,920);
        bgShape.visible = false;
        bgShape.mouseEnabled = false;

        var frame = this.frame = alteastream.Assets.getImage(alteastream.Assets.images.window_big);
        frame.x = 478;
        frame.y = 130;
        this.addChild(frame);
        //frame.visible = false;
        frame.mouseEnabled = false;

        var labelsContainer = this.labelsContainer = new createjs.Container();
        this.addChild(labelsContainer);
        var xPos = 505;
        var xPos2 = 660;
        var font = "15px HurmeGeometricSans3";
        var col_1 = "#c0dbff";
        var col_2 = "#FFF";
        this._createDynamicText("biggestGameWinLabel","BIGGEST GAME WIN: ",font,col_1,{x:xPos,y:870,textAlign:"left"},labelsContainer);
        this._createDynamicText("biggestGameWinValue","",font,col_2,{x:xPos2,y:870,textAlign:"left"},labelsContainer);

        this._createDynamicText("tokensFiredLabel","TOKENS FIRED: ",font,col_1,{x:xPos,y:890,textAlign:"left"},labelsContainer);
        this._createDynamicText("tokensFiredValue","",font,col_2,{x:xPos2,y:890,textAlign:"left"},labelsContainer);

        this._createDynamicText("lastWinLabel","LAST WIN: ",font,col_1,{x:xPos,y:910,textAlign:"left"},labelsContainer);
        this._createDynamicText("lastWinValue","",font,col_2,{x:xPos2,y:910,textAlign:"left"},labelsContainer);

        this._createDynamicText("totalWinLabel","TOTAL WIN: ",font,col_1,{x:xPos,y:930,textAlign:"left"},labelsContainer);
        this._createDynamicText("totalWinValue","",font,col_2,{x:xPos2,y:930,textAlign:"left"},labelsContainer);

        this._createDynamicText("isFreeToPlayLabel","STATUS: ","16px HurmeGeometricSans3",col_1,{x:xPos+550,y:925,textAlign:"left"},labelsContainer);
        this._createDynamicText("isFreeToPlayValue","","16px HurmeGeometricSans3",col_2,{x:xPos2+460,y:925,textAlign:"left"},labelsContainer);
        labelsContainer.visible = false;

        var spinner = this.spinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = 1130;
        spinner.y = 490;
        spinner.mouseEnabled = false;

        var videoStream = this.videoStream = new createjs.DOMElement(videoElement);
        this.addChild(videoStream);
        this.videoStream.visible = false;

        this.setDynamicPosition(0.270,0.125);

        var buttonExitTxt = new alteastream.BitmapText("BACK", "16px HurmeGeometricSans3", "#fff",{textAlign:"center"});
        var buttonExit = this.buttonExit = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnMockSS),3,buttonExitTxt);
        buttonExit.regX = 70;
        buttonExit.regY = 18;
        buttonExit.x = 1690;
        buttonExit.y = 107;
        this.addChild(buttonExit);
        buttonExit.visible = false;
        buttonExit.setClickHandler(function (e) {
            this.exitPreview();
        }.bind(this));

        window.addEventListener("resize",function(){
            _instance._resize();
        });
    };

    StreamPreviewOK_OLD.getInstance = function(){
        return _instance;
    };

    p.activate = function(){
        var webrtcPlayer = this.webrtcPlayer = new WEBRTCPlayer();
        webrtcPlayer.videoOutput = this.videoElement;
        webrtcPlayer.onError = function(error) {
            if(error) {
                console.error(error);
                //this.scene.throwAlert(alteastream.Alert.ERROR,error);
                webrtcPlayer.stop();
            }
        }.bind(this);
    };

    p.start = function(){
        alteastream.Lobby.getInstance().intervalUpdateLobby(false);
        this._active = true;
        this.updateLoungeComponents();
        this.intervalUpdateLounge(true);
    };

    p.intervalUpdateLounge = function(bool){
        var that = this;
        if(bool === true)
            this.thumbnailPropsTimer = setInterval(function(){
                that.updateLoungeComponents();
            },2000);
        else
            clearInterval(this.thumbnailPropsTimer);
    };

    p.updateLoungeComponents = function(){
        if(this._active){
            var that = this;
            alteastream.Requester.getInstance().getCurrentMachine(this.currentStream,function(response){that.updateProps(response);})
        }
    };

    p.exitPreview = function(){
        if(!this._active)
            return;
        this._active = false;

        alteastream.QueuePanel.getInstance().onStreamPreviewOK_OLDExit();
        alteastream.Lobby.getInstance().onStreamPreviewOK_OLDExit();

        this.buttonExit.visible = false;
        this.currentStream = "";
        this.show(false);
        this.hidePlayer(true);
        this.destroyPlayer();
        this.intervalUpdateLounge(false);
        alteastream.Lobby.getInstance().intervalUpdateLobby(true);
        this.spinner.runPreload(false);
        this.labelsContainer.visible = false;
    };

    p.updateInfo = function(machineInfo){
        this.machineInfo = machineInfo;
        this.currentStream = machineInfo.id;
        alteastream.AbstractScene.GAME_CODE = machineInfo.gameCode;
    };

    p.updateProps = function(machineInfo){
        //console.log("PROPS "+machineInfo.biggestGameWin);
        var i = 0;
        for(var key in machineInfo){
            var prop = Object.keys(machineInfo)[i];
            this[prop+"Value"].setText(machineInfo[key]);
            i++;
        }
        var avail = machineInfo.isFreeToPlay === true?"AVAILABLE":"BUSY";
        var color = machineInfo.isFreeToPlay === true?"#ffffff":"#ff0000";
        this.isFreeToPlayValue.setText(avail);
        this.isFreeToPlayValue.setColor(color);
    };

    p.switchStream = function(machineInfo){
        this.changeStream(machineInfo);
        this.updateInfo(machineInfo);
        this.start();
    };

    p.changeStream = function(machineInfo){
        //machineInfo.machineCameras["1"].endpoint;
        this.show(true);
        this.bgShape.visible = true;
        //this.border.visible = true;

        this.hidePlayer(false);
        this.destroyPlayer();

        this.webrtcPlayer.address = machineInfo.machineCameras["1"].endpoint+"?token=PREVIEW&id=1";// id is non relevant for now
        //this.webrtcPlayer.camera = machineInfo.machineCameras["4"].endpoint;
        this.webrtcPlayer.camera = machineInfo.machineCameras["1"].streamname;
        this.webrtcPlayer.start();
       //{"2323":{"machineName":"2323","machineCameras":{"1":{"type":1,"endpoint":"ws://142.93.138.117:8889","streamname":"rtsp://player:player@house_1:4554"},"4":{"type":4,"endpoint":"ws://139.59.147.230:8889","streamname":"rtsp://player:player@192.168.1.251:554"}},"isFreeToPlay":true,"machineIndex":842215987,"biggestGameWin":1142,"tokensFired":8835,"lastWin":1,"totalWin":2378},"2322":{"machineName":"2322","machineCameras":{"1":{"type":1,"endpoint":"ws://142.93.138.117:8889","streamname":"rtsp://player:player@house_1:4555"},"4":{"type":4,"endpoint":"ws://139.59.147.230:8889","streamname":"rtsp://player:player@192.168.1.250:554"}},"isFreeToPlay":true,"machineIndex":842215986,"biggestGameWin":31,"tokensFired":678,"lastWin":1,"totalWin":33}}
        this.webrtcPlayer.onPlay = function(){
            setTimeout(function(){
                _instance._onLoaded();
            },1000);
        };
        this._onLoading();
    };

    p._onLoading = function () {
        this.buttonExit.setDisabled(true);
        alteastream.QueuePanel.getInstance().buttonEnter.setDisabled(true);
        this.spinner.setLabel("Connecting to machine "+_instance.currentStream);
        this.spinner.runPreload(true);
        this.labelsContainer.visible = false;
    };

    p._onLoaded = function () {
        this.spinner.runPreload(false);
        this.labelsContainer.visible = true;
        this.buttonExit.visible = true;

        this.buttonExit.setDisabled(false);
        alteastream.QueuePanel.getInstance().onStreamPreviewOK_OLDEnter();
    };

    p.onQueueEnterResponse = function(){
        this.buttonExit.setDisabled(true);
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p.hidePlayer = function(bool){
        this.videoStream.visible = !bool;
    };

    p.destroyPlayer = function(){
        this.webrtcPlayer.stop();
    };

    p.dispose = function(){
        this.intervalUpdateLounge(false);
        this.destroyPlayer();
    };

    p.reset = function(){
        this.buttonExit.visible = true;
        this.buttonExit.setDisabled(false);
    };

    p._resize = function(){
        var w = window.innerWidth;
        var h = window.innerHeight;
        var spacingX = this.SPACING_LEFT*(w/1000);
        var spacingY = this.SPACING_TOP*(w/1000);

        var dynX = this._getDynamicPosition().x;
        var dynY = this._getDynamicPosition().y;

        this.videoStream.x = w*dynX-spacingX;
        this.videoStream.y = h*dynY+spacingY;
    };

    p.setDynamicPosition = function(xPerc,yPerc){
        this._dynamicPositionXY = {x:xPerc,y:yPerc};
        this._resize();
    };

    p._getDynamicPosition = function(){
        return this._dynamicPositionXY;
    };

    p._createDynamicText = function(instance,text,font,color,props,parent){
        var textInstance = this[instance] = new alteastream.BitmapText(text,font,color,props);
        parent.addChild(textInstance);
    };

    alteastream.StreamPreviewOK_OLD = createjs.promote(StreamPreviewOK_OLD,"Container");
})();