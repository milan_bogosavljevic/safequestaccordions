
// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var SwiperOLD = function (){
        this.initialize_SwiperOLD();
    };

    var p = SwiperOLD.prototype;
    var _instance = null;
// static properties:
// public vars:
// private vars:
    p._target = null;
// constructor:
    p.initialize_SwiperOLD = function (){
        _instance = this;
        createjs.Touch.enable(stage);
    };

// static methods:
    
// public functions:
    p.activate = function(bool){
        var addRemoveListener = bool ===true?"addEventListener":"removeEventListener";
        stage[addRemoveListener]('mousedown', this._onStart);
        stage[addRemoveListener]('stagemouseup', this._onEnd);
    };

    p.setTargetAndDirection = function(obj,direction){
        this._target = obj;
        this._direction = direction;
        console.log("TARGET::::::::::: "+direction);
    };

// private functions:
    p._onStart = function(event){
        _instance.prevInteraction = new Date().getTime();
        _instance.prevStageX = event.stageX;
        _instance.prevStageY = event.stageY;
        _instance.isDown = true;
    };

    p._onEnd = function(event){
        if(_instance.isDown){
            var dx = _instance._direction === "x"?event.stageX - _instance.prevStageX:0;
            var dy = _instance._direction === "y"?event.stageY - _instance.prevStageY:0;
            var distance = Math.sqrt(dx*dx+dy*dy);
            if(distance!==0){
                var deltaTime = new Date().getTime() - _instance.prevInteraction;
                var speed = distance / deltaTime;
                if (speed>2.5){
                    var dist = _instance._direction === "x"?dx:dy;
                    _instance._swipe(dist>0?1:-1);
                }
            }
            _instance.isDown=false;
        }
    };

    p._swipe = function(direction){
        this._target.onSwipe(direction);//* direction
    };

    alteastream.SwiperOLD = SwiperOLD;
})();