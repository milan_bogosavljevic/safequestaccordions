/**
 * Created by Dacha on 25-Jan-16.
 */
// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var CanvasStream = function (scene,elementId,address){
        this.AbstractVideo_constructor(scene);
        this.initialize_CanvasStream(elementId,address);
    };

    var p = createjs.extend(CanvasStream, alteastream.AbstractVideo);
// static properties:
// public vars:
// private vars:
// constructor:
    p.initialize_CanvasStream = function (elementId,address){
        console.log("initialize stream "+elementId);
        var tag = '<canvas id = "'+elementId+'" width="'+alteastream.AbstractScene.GAME_WIDTH+'" height="'+alteastream.AbstractScene.GAME_HEIGHT+'" ></canvas>';

        this.videoElement = $(tag).appendTo(document.getElementById("wrapper"))[0];
        var style = this.videoElement.style;
        style.position = "fixed";
        style.border = 0;
        style.left = 0;
        style.top = 0;
        style.width = "100%";
        style.height = "100%";// na 88%
        style.outline = "none";
        style.pointerEvents = "none";
        style.zIndex = -5;// 1

        var canvas = this.videoElement;
        this.player = new jsmpeg(new WebSocket(address), {
            canvas: canvas
        });
        //this.pingWithToken();//Vlada new??

        this.currentVideo = new createjs.DOMElement(canvas);
        //this.scene.addChild(this.currentVideo );
        this.videoElement.style.opacity = 0;
    };

    p.activate = function(){
        var that = this;
        this.stateCheck = setInterval(function () {
            if (that.player.client.readyState !== that.player.client.OPEN) {
                console.log("CONNECTING STATE..");
            }else{
                clearInterval(that.stateCheck);
                console.log("OPEN STATE..");
                that.player.client.send(JSON.stringify({token:alteastream.AbstractScene.GAME_TOKEN,id:alteastream.AbstractScene.GAME_ID}));
                TweenLite.to(that.videoElement,2,{delay:1,autoAlpha:1});
            }
        }, 100);
    };

    /*{
  "token": "697q76gcpeg3k1fdaksmnbcjbl",
  "exp": 1531466979

   kljuc od sesije
sa tim malim tokenom se kacis na stomp strim

ovaj veliki token koristis kao i do sada za sve ostalo
sa jednom malom cakom
on istice za minut
i moras da ga regenerises na manje od toga
tako sto saljes zahtev na:
user/heartbeat/TOKEN
tada dobijas novi veliki token, i koristis ga umesto starog
mali token ne diras
jer je on sve vreme isti
}*/

    /*p.pingWithToken = function(){//?
        setTimeout(function(){
            that.player.client.send(JSON.stringify({token:1,id:alteastream.AbstractScene.GAME_ID}));
        },1000);
        // setTimeout(function(){
        //jplayer.client.send('{"token":1,"id":'+machine_name+'}');
    //},1000);
    };*/



// static methods:
    
// public functions:

// private functions:

    alteastream.CanvasStream = createjs.promote(CanvasStream,"AbstractVideo");
})();