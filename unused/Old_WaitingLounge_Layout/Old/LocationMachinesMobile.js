
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LocationMachinesMobile = function(lobby) {
        this.LocationMachines_constructor(lobby);
        this.initialize_LocationMachinesMobile();
    };
    var p = createjs.extend(LocationMachinesMobile, alteastream.LocationMachines);

// static properties:
// events:
// public properties:
// private properties:
// constructor:
    p.initialize_LocationMachinesMobile = function() {};

    p.setComponents = function(lobby) {
        var that = this;
        this.lobby = lobby;

        var rowsPerPage = 2;
        var columnsPerPage = 3;
        this._machinesComponent = new alteastream.MachinesComponent(rowsPerPage, columnsPerPage);

        this._machinesPerPage = rowsPerPage * columnsPerPage;

        this._machinesComponent.adjustMobile();

        var maskWidth = this._machinesComponent.getWidth();
        var maskHeight = this._machinesComponent.getMaskHeight();

        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(this._machinesComponent._startPositionX, this._machinesComponent._startPositionY, maskWidth, maskHeight);
        this._machinesComponent.mask = mask;

/*        var machinesComponentBackground = this._machinesComponentBackground = new createjs.Shape();
        machinesComponentBackground.graphics.beginFill("#000000").drawRect(-20, -10, 960, 540);
        machinesComponentBackground.alpha = 0.8;
        machinesComponentBackground.cache(-20, -10, 960, 540);
        machinesComponentBackground.visible = false;*/

        this._machinesComponent.visible = false;

        var topSectionBackground = this._topSectionBackground = new createjs.Shape();
        topSectionBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 933, 60, 2);
        topSectionBackground.alpha = 0.75;
        topSectionBackground.cache(0, 0, 933, 60);
        topSectionBackground.visible = false;
        topSectionBackground.x = 5;
        topSectionBackground.y = 5;
        //topSectionBackground.originalSize = {width:926, height:60, roundness:2};
        //topSectionBackground.listViewSize = {width:240, height:50, roundness:2};
        this.addChild(topSectionBackground);

        var buttonBackTxt = new createjs.Text("To Locations","16px HurmeGeometricSans3","#ffffff").set({textAlign:"center",textBaseline:"middle"});
        var backToLocationsButton = this._backToLocationsButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBack),3,buttonBackTxt);
        backToLocationsButton.centerText(15,2);
        backToLocationsButton.visible = false;
        backToLocationsButton.x = 12 + (backToLocationsButton._singleImgWidth*0.5);
        backToLocationsButton.y = 12 + (backToLocationsButton._height*0.5);
        backToLocationsButton.setClickHandler(function () {
            that.backToLocationsButtonHandler();
            that.lobby.checkIfHiddenMenuIsShown();
        });

        var buttonBackToLobbyTxt = new createjs.Text("To Lobby","16px HurmeGeometricSans3","#ffffff").set({textAlign:"left",textBaseline:"middle"});
        var backToLobbyButton = this._backToLobbyButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBack2),3,buttonBackToLobbyTxt);
        backToLobbyButton.centerText(15, 2);
        backToLobbyButton.visible = false;
        backToLobbyButton.x = 12 + (backToLobbyButton._singleImgWidth*0.5);
        backToLobbyButton.y = 12 + (backToLobbyButton._height*0.5);
        backToLobbyButton.setClickHandler(function () {
            that.backToLobbyButtonHandler();
            that.lobby.checkIfHiddenMenuIsShown();
        });

        this.addChild(/*machinesComponentBackground,*/ this._machinesComponent , backToLocationsButton, backToLobbyButton);

        var bigTent = this._bigTent = alteastream.Assets.getImage(alteastream.Assets.images.house);
        bigTent.regX = bigTent.image.width/2;
        bigTent.regY = bigTent.image.height/2;
        bigTent.x = 470;
        bigTent.y = 245;
        this.addChild(bigTent);

        var streamPreview = this.streamPreview = new alteastream.StreamPreview();
        this.addChild(streamPreview);
        streamPreview.mouseEnabled = false;
        streamPreview.mouseChildren = false;
        streamPreview.show(false);
        streamPreview.adjustMobile();

        var _locationLabel = this._locationLabel = new createjs.Text("Mock text","16px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", mouseEnabled:false, x:473, y:36, visible:false});
        //var _numOfMachinesLabel = this._numOfMachinesLabel = new createjs.Text("0","16px HurmeGeometricSans3","#ffffff").set({textAlign:"left", textBaseline:"middle", mouseEnabled:false, x:_locationLabel + 10, y:_locationLabel.y, visible:false});
        this.addChild(_locationLabel/*, _numOfMachinesLabel*/);

        var queuePanel = this.queuePanel = new alteastream.QueuePanel();
        this.addChild(queuePanel);
        queuePanel.adjustMobile();

        var divider = this.divider = new createjs.Text("|","14px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", x:760, y:36});

        var area = new createjs.Shape();
        area.graphics.beginFill("#fff").drawRect(-82, -28, 82, 56);

        var showAllMachinesButton = this.showAllMachinesButton = new createjs.Text("All Machines","14px HurmeGeometricSans3","#2aff00").set({textAlign:"right", textBaseline:"middle", x:divider.x - 10, y:divider.y});
        showAllMachinesButton.hitArea = area;
        showAllMachinesButton.on("click", function () {
            if(showAllMachinesButton.isActive === false){
                that._showAvailableOrAllButtonsHandler(false);
                that.lobby.checkIfHiddenMenuIsShown();
            }
        });

        var area2 = new createjs.Shape();
        area2.graphics.beginFill("#71c8e0").drawRect(0, -28, 80, 56);

        var showAvailableMachinesButton = this.showAvailableMachinesButton = new createjs.Text("Free To Play","14px HurmeGeometricSans3","#ffffff").set({textAlign:"left", textBaseline:"middle", x:divider.x + 10, y:divider.y});
        showAvailableMachinesButton.hitArea = area2;
        showAvailableMachinesButton.on("click", function () {
            if(showAvailableMachinesButton.isActive === false){
                that._showAvailableOrAllButtonsHandler(true);
                that.lobby.checkIfHiddenMenuIsShown();
            }
        });

        showAllMachinesButton.visible = showAvailableMachinesButton.visible = divider.visible = false;
        showAllMachinesButton.isActive = showAvailableMachinesButton.isActive = false;

        var scroller = this._scroller = new alteastream.Scroller(this._machinesComponent);
        scroller.setScrollType("scrollByPage");
        scroller.setPreAnimateScrollMethod(function () {
            that._canUpdateThumbs = false;
            that._machinesComponent.mouseChildren = false;
            that.lobby.checkIfHiddenMenuIsShown();
        });
        scroller.setPostAnimateScrollMethod(function () {
            scroller.updateCurrentDotPosition();
            that._canUpdateThumbs = true;
            that._machinesComponent.mouseChildren = true;
        });

        scroller.setScrollDirection("y");

        var numOfPages = this._machinesComponent.getMaxNumberOfPages();
        var paginationOptions = {
            numberOfPages:numOfPages,
            scroller:scroller,
            customDot:null,
            customCurrentDot:null,
            dotsSpacing:-2,
            dotsWidth:10,
            dotFont:null,
            currentDotFont:null
        };

        var pagination = this._pagination = new alteastream.Pagination(paginationOptions);
        pagination.rotation = 90;
        pagination.x = -2;
        pagination.y = 84;

        pagination.visible = false;
        pagination.enablePaginationClick(false);
        scroller.plugInPagination(pagination);

/*        var pageUpButton = this.pageUpButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnPrev),3);
        pageUpButton.regX = pageUpButton._singleImgWidth/2;
        pageUpButton.regY = pageUpButton._height/2;

        var pageDownbutton = this.pageDownbutton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnNext),3)
        pageDownbutton.regX = pageDownbutton._singleImgWidth/2;
        pageDownbutton.regY = pageDownbutton._height/2;

        pagination.setUpButton(pageUpButton, -30, 0, scroller);
        pagination.setDownButton(pageDownbutton, 0, 0, scroller);

        pagination.hideUI(true);*/

        this.addChild(pagination);

        // filtering
        this.addChild(divider, showAllMachinesButton,showAvailableMachinesButton);
        this.visible = false;
    };

    p.setListLayout = function(setList) {
        this.gridLayoutIsActive = !setList;
        this.lobby.setBackgroundImagesForStreamPreview(setList);
        this._machinesComponent.setGridView(!setList);
        this._machinesComponent.setShowOnlyAvailable(setList);
        this._backToLocationsButton.visible = !setList;
        this._backToLobbyButton.visible = setList;
        //this._locationLabel.visible = !setList;
        this._resetScrollComponent(setList);
        //this._updateMachinesLabel(setList);
        this._updateMachinesLabel();
        this._manageButtonsState();

        var btnImage = setList === true ? "btnHiddenMenuTransparentBlack" : "btnHiddenMenuTransparent";
        this.lobby._hiddenMenuButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));

        this.showAllMachinesButton.visible = !setList;
        this.showAvailableMachinesButton.visible = !setList;
        this.divider.visible = !setList;

        var scrollType = setList === true ? "scrollByRow" : "scrollByPage";
        this._scroller.setScrollType(scrollType);
    };

    p.onSwipe = function (direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    p._superShowMachinesHideShape = p.showMachinesHideShape;
    p.showMachinesHideShape = function(bool){
        this._superShowMachinesHideShape(bool);
        var btnImage = bool === false ? "btnHiddenMenu" : "btnHiddenMenuTransparent";
        this.lobby._hiddenMenuButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));
    };

    p._superUpdateMachinesLabel = p._updateMachinesLabel;
    p._updateMachinesLabel = function(setList){
        this._superUpdateMachinesLabel(setList);
        //this._locationLabel.x = setList ? 240 : 473;
        this._locationLabel.x = this.gridLayoutIsActive ? 473 : 231;
        this._locationLabel.font = this.gridLayoutIsActive ? "16px HurmeGeometricSans3" : "12px HurmeGeometricSans3";
        //this._numOfMachinesLabel.y = setList ? 37 : this._locationLabel.y;
    };

    p._manageButtonsState = function() {};
    p._setButtonsStateOnStart = function(numOfPages) {};
    p._showButtons = function(show) {};
    p._updateButtonsPosition = function(listView){};

// static methods:
// public methods:

    alteastream.LocationMachinesMobile = createjs.promote(LocationMachinesMobile,"LocationMachines");
})();