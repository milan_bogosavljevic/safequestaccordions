
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreview = function(){
        this.Container_constructor();
        this.initStreamPreview();
    };

    var p = createjs.extend(StreamPreview,createjs.Container);

    p._count = 0;
    p._streamEstablished = false;
    p._attempts = 0;
    p.startX = 0;
    p.sWidth = 0;
    p.sHeight = 0;
    p.player = null;
    p.currentStream = "";
    p.currentMachineName = "";
    p._active = false;
    p.webRtcPlayer = null;
    p._changeStreamTimeout = null;

    var _instance = null;

    p.initStreamPreview = function () {
        console.log("StreamPreview initialized:::");
        _instance = this;

        var videoID = this.videoID = "videoOutput";

        //check here for mobile for width/height 960/540
        //var videoTag ="<video id ='"+videoID+"' width = '1280' height='720' autoplay muted playsinline></video>";

        var isMobile = window.localStorage.getItem('isMobile');
        var videoWidth = isMobile === "not" ? 1280 : 960;
        var videoHeight = isMobile === "not" ? 720 : 540;
        var videoTag ="<video id ='"+videoID+"' width = '"+videoWidth+"' height= '"+videoHeight+"' autoplay muted playsinline></video>";

        var videoElement = this.videoElement = $(videoTag).appendTo(document.getElementById("wrapper"))[0];
        document.getElementById("wrapper").insertBefore(document.getElementById(videoID), document.getElementById("wrapper").firstChild);// wrapper.childNodes[0]  comment zIndex!!

        var style = videoElement.style;
        style.width = "100%";
        style.height = "100%";
        style.pointerEvents = "none";

        style["video::-webkit-media-controls-panel"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-play-button"] = "display: none!important;";
        style["video::-webkit-media-controls-panel"] = "-webkit-appearance: none;";

        style["video::-webkit-media-controls-start-playback-button"] = "display: none!important;";
        style["video::-webkit-media-controls-start-playback-button"] = "-webkit-appearance: none;";

        var frame = this.frame = alteastream.Assets.getImage(alteastream.Assets.images.window_big);
        frame.x = 265;
        frame.y = 5;
        frame.scaleX = 2.99;
        frame.scaleY = 2.94;
        this.addChild(frame);
        frame.mouseEnabled = false;

        var labelsContainer = this.labelsContainer = new createjs.Container();
        this.addChild(labelsContainer);
        labelsContainer.mouseEnabled = false;
        labelsContainer.mouseChildren = false;
        labelsContainer.x = 820;

        var yPos = 777;
        var font = "15px HurmeGeometricSans3";
        var col_1 = "#ffffff";
        var col_2 = "#ffcc00";//00ff18
        var col_3 = "#3ee019";//00ff18

        var tempX = 80;

        this._createDynamicText("tokensInfoLabel","PRIZES COLLECTED",font,col_1,{x:325+tempX,y:yPos,textAlign:"left", textBaseline:"middle"},labelsContainer);

        //this._createDynamicText("tokensFiredLabel","TOKENS FIRED: ",font,col_1,{x:205,y:yPos,textAlign:"left"},labelsContainer);
        this._createDynamicText("tokensFiredLabel","COINS FIRED: ",font,col_1,{x:-530,y:yPos,textAlign:"left", textBaseline:"middle"},labelsContainer);
        this._createDynamicText("tokensFiredValue","",font,col_3,{x:this.tokensFiredLabel.x+100,y:yPos,textAlign:"left", textBaseline:"middle"},labelsContainer);

        this._createDynamicText("lastWinLabel","Last: ",font,col_1,{x:510+tempX,y:yPos,textAlign:"left", textBaseline:"middle"},labelsContainer);
        this._createDynamicText("lastWinValue","",font,col_2,{x:this.lastWinLabel.x+35,y:yPos,textAlign:"left", textBaseline:"middle"},labelsContainer);

        this._createDynamicText("totalWinLabel","Total: ",font,col_1,{x:595+tempX,y:yPos,textAlign:"left", textBaseline:"middle"},labelsContainer);
        this._createDynamicText("totalWinValue","",font,col_2,{x:this.totalWinLabel.x+40,y:yPos,textAlign:"left", textBaseline:"middle"},labelsContainer);

        this._createDynamicText("biggestGameWinLabel","Biggest: ",font,col_1,{x:678+tempX,y:yPos,textAlign:"left", textBaseline:"middle"},labelsContainer);
        this._createDynamicText("biggestGameWinValue","",font,col_2,{x:this.biggestGameWinLabel.x+58,y:yPos,textAlign:"left", textBaseline:"middle"},labelsContainer);

        //labelsContainer.visible = false;

        var loadAnimation = new alteastream.SSAnimator("animatedLogo",250,244,19);
        var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        this.addChild(spinner);
        spinner.x = 987;
        spinner.y = 372;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;

        var videoStream = this.videoStream = new createjs.DOMElement(videoElement);
        alteastream.Lobby.getInstance().addChild(videoStream);

        // stari wait lounge
        this.videoStream.visible = false;
        this.videoStream.scaleX = 0.75;
        this.videoStream.scaleY = 0.75;
        this.setDynamicPosition(0.2,0.107);

        // novi wait lounge
        //this.setDynamicPosition(0.01,-0.01);

        window.addEventListener("resize",function(){
            _instance._resize();
        });
    };

    StreamPreview.getInstance = function(){
        return _instance;
    };

    p.start = function(){
        this._active = true;
        alteastream.LocationMachines.getInstance().resetIntervalUpdateLounge();
    };

    p.exitPreview = function(){
        //if(!this._active)
            //return;
        this._active = false;
        clearTimeout(this._changeStreamTimeout);
        this._stopMonitor();
        this._clearPreview();
    };

    p._clearPreview = function(){
        this.currentStream = "";
        this.show(false);
        this.hidePlayer(true);
        this.destroyPlayer();
        this.spinner.runPreload(false);
        this.frame.visible = true;
    };

    p.updateInfo = function(machineInfo){
        this.machineInfo = machineInfo;
        alteastream.AbstractScene.GAME_CODE = machineInfo.gameCode;
        alteastream.AbstractScene.DEFAULT_GAME_ID = machineInfo.defaultGameId;
    };

    p.updateProps = function(machineInfo){
        this.tokensFiredValue.text = machineInfo.tokensFired;
        this.lastWinValue.text = machineInfo.lastWin;
        this.totalWinValue.text = machineInfo.totalWin;
        this.biggestGameWinValue.text = machineInfo.biggestGameWin;

        //symbols
        //var image;
        //var machine = alteastream.MachinesComponent.getInstance().getMachineById(that.currentStream);
       /* if(machineInfo.statusCode !== 1){
            image = "lampRed";
        }else{*/
            //image = machineInfo.isFreeToPlay ? "lampGreenLight" : "lampYellowDark";
       // }
    };

    p.activate = function(){
        if(!this.webRtcPlayer){
            this.webRtcPlayer = new WEBRTCGamePlayer(this.videoElement);
            this._local_activate();// local ver
        }
    };

    p.tryToSwitchStream = function(machine) {
        if(this.currentStream !== machine.machineInfo.machineIndex){// if !machine.selected?
            alteastream.MachinesComponent.getInstance().clearActiveMachines();
            //alteastream.SocketCommunicator.getInstance().lobbySwitchSubscriptions(this.currentStream,machine.machineInfo.machineIndex);
            alteastream.SocketCommunicatorLobby.getInstance().lobbySwitchSubscriptions(this.currentStream,machine.machineInfo.machineIndex);
            this.switchStream(machine.machineInfo);

            alteastream.QueuePanel.getInstance().setCurrentMachine(machine.machineInfo);
            this.changeFrameImage(machine.thumb);
            machine.selectMachine(true);
            // filtering
            //alteastream.MachinesComponent.getInstance().setCurrentlySelectedMachineId(this.currentStream );// todo proveriti da li moze currentStream umesto machineIndex
        }
    };

    p.switchStream = function(machineInfo){
        /*if(is.edge()){
            this.recreateVideoElement();
        }*/
        this.clearVideoElement();
        this.spinner.runPreload(false);
        this.changeStream(machineInfo);
        this.updateInfo(machineInfo);
        this.start();
    };

    p.recreateVideoElement = function(){
        var canvas = document.getElementById("screen");
        canvas.parentNode.removeChild(this.videoElement);
        var videoElement = this.videoElement = document.createElement("video");
        videoElement.id = "videoOutput";
        videoElement.autoplay = true;
        canvas.parentNode.insertBefore(videoElement,canvas);
        var style = videoElement.style;
        style.top = "0px";
        style.left ="0px" ;
        style.width = 100 + "%";
        style.height =  "0 auto";
        style.position = "absolute";
        style.pointerEvents = "none";

        this.webRtcPlayer.video = videoElement;
    };

    p.changeStream = function(machineInfo){
        this.currentStream = machineInfo.machineIndex;
        this.currentMachineName = machineInfo.machineName;
        this.show(true);
        this.hidePlayer(true);
        this._stopMonitor();

        clearTimeout(this._changeStreamTimeout);

        this._changeStreamTimeout = setTimeout(function () {
            _instance.webRtcPlayer.setPreview(machineInfo.previewCamera,alteastream.AbstractScene.SHOP_MACHINE,machineInfo.machineName);// soket, id kuce, naziv masine
            _instance.webRtcPlayer.start();
            _instance.webRtcPlayer.onStreamEvent = function(evt){
                if(evt.type === 'state' && evt.new === 'CONNECTED'){
                    console.log("JUST CONNECTED ::::::::::::::::::: "+evt);
                }
                if(evt.type === 'state' && evt.old === 'DISCONNECTED' && evt.new === 'CONNECTED'){
                    console.log("CONNECTED OK::::::::::::::::::: "+evt);

                    _instance._streamConnected();
                    _instance._onLoaded();
                }
            };
        },1000);

        this._onLoading();
        this._startMonitor(machineInfo);
    };

    p._onLoading = function () {
        //this.parent.onStreamLoading();
        this.frame.visible = true;
        this.spinner.setLabel("Connecting to machine "+_instance.currentMachineName);
        this.spinner.runPreload(true);
    };

    p._onLoaded = function () {
        this.hidePlayer(false);
        this.spinner.runPreload(false);
        this.frame.visible = false;
        console.log("StreamLoaded::::");
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p.hidePlayer = function(bool){
        //this.videoStream.visible = !bool;
        this.videoStream.visible = true;
    };

    p.destroyPlayer = function(){
        this.webRtcPlayer.stop();
        this.clearVideoElement();
    };

    p.dispose = function(){
        clearTimeout(this._changeStreamTimeout);
        this._stopMonitor();
        //this.intervalUpdateLounge(false);
        this.destroyPlayer();
        console.log("DISPOSING STREAM PREVIEW");
    };

    p.clearVideoElement = function(){
        this.videoElement.removeAttribute('src');
        this.videoElement.load();
    };

    p._resize = function(){
        var w = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        var h = (window.innerHeight > 0) ? window.innerHeight : screen.width;

        var spacingX = w/100;
        var spacingY = h/100;

        var dynX = this._getDynamicPosition().x;
        var dynY = this._getDynamicPosition().y;

        this.videoStream.x = w*dynX-spacingX;
        this.videoStream.y = h*dynY+spacingY;
    };

    p.setDynamicPosition = function(xPerc,yPerc){
        this._dynamicPositionXY = {x:xPerc,y:yPerc};
        this._resize();
    };

    p._getDynamicPosition = function(){
        return this._dynamicPositionXY;
    };

    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.labelsContainer.addChild(textInstance);
        textInstance.mouseEnabled = false;
    };

    p.changeFrameImage = function(thumb) {
        if(this.frame.visible){
            gamecore.Utils._removeAllFilters([this.frame]);
            this.frame.image = thumb.hasThumbImage === true ? thumb.image : alteastream.Assets.getImage(alteastream.Assets.images.thumb).image;
            gamecore.Utils.DISPLAY.FILTERS.setBlur([this.frame], 2);
        }
    };

    p._startMonitor = function(machineInfo){
        var that = this;
        this.streamEventMonitor = setInterval(function(){
            if(!that._streamEstablished){
                if(that._attempts<3){
                    if(that._count<6){
                        that._count++;
                        console.log("monitoring.. "+that._count);
                    }else{
                        that._attempts++;
                        that._resetMonitor();
                        that.switchStream(machineInfo);
                        that.spinner.setLabel("Fetching "+_instance.currentMachineName+" preview","#ffcc00");
                    }
                }else{
                    that._stopMonitor();
                    that._attempts = 0;
                    that.spinner.runPreload(false);
                    that.spinner.preloadInfoText.visible = true;
                    that.spinner.setLabel("Preview for "+_instance.currentMachineName+" failed","#ff0000");
                }
            }
        },1000);
    };

    p._resetMonitor = function(){
        clearInterval(this.streamEventMonitor);
        this._count = 0;
    };
    
    p._stopMonitor = function(){
        this._resetMonitor();
        this._streamEstablished = false;
    };

    p._streamConnected = function(){
        this._streamEstablished = true;
        this._attempts = 0;
        this._resetMonitor();
    };

    p._local_activate = function(){
        this.videoElement.src = "videotest.mp4";
        this.videoElement.play();
        this.videoElement.loop = true;
        this.videoElement.muted = false;

        this.currentStream = 333333333;
        this.start();

        this._onLoading();
        setTimeout(function(){
            _instance._onLoaded();
        },1000);
    };

    p.adjustMobile = function(){

        var footer = new createjs.Shape();
        footer.graphics.beginFill("#000000").drawRoundRect(0, 0, 616, 40, 3);//15d510
        footer.cache(0, 0, 616, 40);
        footer.alpha = 0.3;
        footer.x = 319;
        footer.y = 429;

        this.addChildAt(footer, 0);

        this.frame.x = 315;
        this.frame.y = 8;
        this.frame.scaleX = 1.3;
        this.frame.scaleY = 1.3;

        this.labelsContainer.x = 5;

        var txtsYPos = 449;
        var font = "13px HurmeGeometricSans3";

        var valueXCorrection = 4;

        this.tokensInfoLabel.font = font;
        this.tokensInfoLabel.x = 540;
        this.tokensInfoLabel.y = txtsYPos;

        this.tokensFiredLabel.font = font;
        this.tokensFiredLabel.x = 320;//315
        this.tokensFiredLabel.y = txtsYPos;

        this.tokensFiredValue.font = font;
        this.tokensFiredValue.x = this.tokensFiredLabel.x + this.tokensFiredLabel.getBounds().width - valueXCorrection;
        this.tokensFiredValue.y = txtsYPos;

        this.lastWinLabel.font = font;
        this.lastWinLabel.x = 690;
        this.lastWinLabel.y = txtsYPos;

        this.lastWinValue.font = font;
        this.lastWinValue.x = this.lastWinLabel.x + this.lastWinLabel.getBounds().width - valueXCorrection;
        this.lastWinValue.y = txtsYPos;

        this.totalWinLabel.font = font;
        this.totalWinLabel.x = 775;
        this.totalWinLabel.y = txtsYPos;

        this.totalWinValue.font = font;
        this.totalWinValue.x = this.totalWinLabel.x + this.totalWinLabel.getBounds().width - valueXCorrection;
        this.totalWinValue.y = txtsYPos;

        this.biggestGameWinLabel.font = font;
        this.biggestGameWinLabel.x = 850;
        this.biggestGameWinLabel.y = txtsYPos;

        this.biggestGameWinValue.font = font;
        this.biggestGameWinValue.x = this.biggestGameWinLabel.x + this.biggestGameWinLabel.getBounds().width - valueXCorrection;
        this.biggestGameWinValue.y = txtsYPos;

        if(this.spinner)
            this.removeChild(this.spinner);

        var loadAnimation = new alteastream.SSAnimator("animatedLogo",125,122,19);
        var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        this.addChild(spinner);
        spinner.x = 645;
        spinner.y = 160;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;

        // stari wait lounge
        this.videoStream.scaleX = 0.65;
        this.videoStream.scaleY = 0.65;
        this.setDynamicPosition(0.349,0.157);

        // novi wait lounge
        //this.setDynamicPosition(0.01,-0.01);

        this.removeChild(this.frame);//todo proveriti
    }

    alteastream.StreamPreview = createjs.promote(StreamPreview,"Container");
})();