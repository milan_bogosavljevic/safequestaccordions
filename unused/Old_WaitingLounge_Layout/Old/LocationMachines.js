
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LocationMachines = function(lobby) {
        this.Container_constructor();
        this.initialize_LocationMachines(lobby);
    };
    var p = createjs.extend(LocationMachines, createjs.Container);

// static properties:
// events:

// public properties:
    p.lobby = null;
    p.gridLayoutIsActive = true;
    p.isFocused = false;
    p._machinesComponent = null;
    p._machinesPerPage = 0;
    p._canUpdateThumbs = true;
    p._backToLocationsButton = null;
    p._backToLobbyButton = null;
    p._locationLabel = null;
    p._locationName = null;
    p._scroller = null;
    p._pagination = null;

    var that = null;

// private properties:

// constructor:
    p.initialize_LocationMachines = function(lobby) {
        console.log("LocationMachines initialized::: ");
        that = this;
        this.setComponents(lobby);
    };

    LocationMachines.getInstance = function(){
        return that;
    };

    p.setComponents = function(lobby) {
        this.lobby = lobby;

        var rowsPerPage = 3;
        var columnsPerPage = 7;
        this._machinesPerPage = rowsPerPage * columnsPerPage;

        var machinesComponent = this._machinesComponent = new alteastream.MachinesComponent(rowsPerPage, columnsPerPage);
        machinesComponent.visible = false;

        var maskWidth = machinesComponent.getWidth();
        var maskHeight = machinesComponent.getMaskHeight();
        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(machinesComponent._startPositionX, machinesComponent._startPositionY, maskWidth, maskHeight);
        machinesComponent.mask = mask;

        var topSectionBackground = this._topSectionBackground = new createjs.Shape();
        topSectionBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 1679, 50, 4);
        topSectionBackground.alpha = 0.75;
        topSectionBackground.cache(0, 0, 1679, 50);
        topSectionBackground.visible = false;
        topSectionBackground.x = 21;
        topSectionBackground.y = -56;

        var buttonBackTxt = new createjs.Text("To Locations","15px HurmeGeometricSans3","#ffffff").set({textAlign:"left",textBaseline:"middle"});
        var backToLocationsButton = this._backToLocationsButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBack),2,buttonBackTxt);
        backToLocationsButton.centerText(15, 2);
        backToLocationsButton.visible = false;
        backToLocationsButton.x = 21 + (backToLocationsButton._singleImgWidth*0.5);
        backToLocationsButton.y = -110 + (backToLocationsButton._height*0.5);
        backToLocationsButton.setClickHandler(function () {
            that.backToLocationsButtonHandler();
        });

        var buttonBackToLobbyTxt = new createjs.Text("To Lobby","15px HurmeGeometricSans3","#ffffff").set({textAlign:"left",textBaseline:"middle"});
        var backToLobbyButton = this._backToLobbyButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBack),2,buttonBackToLobbyTxt);
        backToLobbyButton.centerText(15, 2);
        backToLobbyButton.visible = false;
        backToLobbyButton.x = -55 + (backToLobbyButton._singleImgWidth*0.5);
        backToLobbyButton.y = -115 + (backToLobbyButton._height*0.5);
        backToLobbyButton.setClickHandler(function () {
            that.backToLobbyButtonHandler();
            //new socket
            /*console.log("leaveQueue:: backToLobbyButtonHandler");
            that.queuePanel.leaveQueue(function () {
                console.log("no socket? getMachines here ?? ");
                //that.intervalUpdateLobby(true);// if back from entranceCounter?
            })*/
        });

        var bigTent = this._bigTent = alteastream.Assets.getImage(alteastream.Assets.images.house);
        bigTent.mouseEnabled = false;
        bigTent.regX = bigTent.image.width/2;
        bigTent.regY = bigTent.image.height/2;
        bigTent.x = 861;
        bigTent.y = 390;

        var streamPreview = this.streamPreview = new alteastream.StreamPreview();
        streamPreview.mouseEnabled = false;
        streamPreview.mouseChildren = false;
        streamPreview.show(false);

        var _locationLabelShape = this._locationLabelShape = new createjs.Shape();
        _locationLabelShape.graphics.beginFill("#000000").drawRoundRect(0, 0, 234, 50, 4);
        _locationLabelShape.alpha = 0.7;
        _locationLabelShape.cache(0, 0, 234, 50);
        _locationLabelShape.x = 21;
        _locationLabelShape.y = -56;
        _locationLabelShape.visible = false;

        var _locationLabel = this._locationLabel = new createjs.Text("Mock text","bold 16px HurmeGeometricSans3","#a0a0a0").set({textAlign:"left", textBaseline:"middle", mouseEnabled:false, x:36, y:-29, visible:false});

        //var _numOfMachinesLabel = this._numOfMachinesLabel = new createjs.Text("0","bold 16px HurmeGeometricSans3","#a0a0a0").set({textAlign:"left", mouseEnabled:false, x:125, y:-37, visible:false});//y:820

        var divider = this.divider = new createjs.Text("|","22px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", x:1548, y:-30});

        var queuePanel = this.queuePanel = new alteastream.QueuePanel();

        var area = new createjs.Shape();
        area.graphics.beginFill("#fff").drawRect(-130, -26, 130, 50);

        var showAllMachinesButton = this.showAllMachinesButton = new createjs.Text("All Machines","22px HurmeGeometricSans3","#2aff00").set({textAlign:"right", textBaseline:"middle", x:divider.x - 10, y:divider.y});
        showAllMachinesButton.hitArea = area;
        showAllMachinesButton.on("click", function () {
            if(showAllMachinesButton.isActive === false){
                that._showAvailableOrAllButtonsHandler(false);
            }
        });

        var area2 = new createjs.Shape();
        area2.graphics.beginFill("#71c8e0").drawRect(0, -26, 130, 50);

        var showAvailableMachinesButton = this.showAvailableMachinesButton = new createjs.Text("Free To Play","22px HurmeGeometricSans3","#ffffff").set({textAlign:"left", textBaseline:"middle", x:divider.x + 10, y:divider.y});
        showAvailableMachinesButton.hitArea = area2;
        showAvailableMachinesButton.on("click", function () {
            if(showAvailableMachinesButton.isActive === false){
                that._showAvailableOrAllButtonsHandler(true);
            }
        });

        showAllMachinesButton.visible = showAvailableMachinesButton.visible = divider.visible = false;
        showAllMachinesButton.isActive = showAvailableMachinesButton.isActive = false;

        var scroller = this._scroller = new alteastream.Scroller(machinesComponent);
        scroller.setScrollType("scrollByRow");
        scroller.setScrollDirection("y");

        scroller.setPreAnimateScrollMethod(function () {
            scroller.manageButtonsState();
            machinesComponent.mouseChildren = false;
            that._canUpdateThumbs = false;
        });

        scroller.setPostAnimateScrollMethod(function () {
            scroller.updateCurrentDotPosition();
            that._canUpdateThumbs = true;
            machinesComponent.mouseChildren = true;
        });

        var numOfPages = machinesComponent.getMaxNumberOfPages();

        var paginationOptions = {
            numberOfPages:numOfPages,
            scroller:scroller,
            customDot:null,
            customCurrentDot:null,
            dotsSpacing:5,
            dotsWidth:40,
            dotFont:null,
            currentDotFont:null
        };

        var pagination = this._pagination = new alteastream.Pagination(paginationOptions);
        pagination.rotation = 90;
        pagination.x = -25;
        pagination.y = 20;

        pagination.visible = false;
        scroller.plugInPagination(pagination);

        this.addChild(
            topSectionBackground,
            machinesComponent,
            backToLocationsButton,
            backToLobbyButton,
            bigTent,
            streamPreview,
            _locationLabelShape,
            _locationLabel,
            //_numOfMachinesLabel,
            queuePanel,
            divider,
            showAllMachinesButton,
            showAvailableMachinesButton,
            pagination
        );

        this.visible = false;
    };

    p._showAvailableOrAllButtonsHandler = function(onlyAvailable) {
        this._machinesComponent.showOnlyAvailableOrAllMachines(onlyAvailable);
        this.updateAvailableOrAllMachinesButtonsSkin(onlyAvailable);
        this._resetScrollComponent(false);
    };

    p.updateAvailableOrAllMachinesButtonsSkin = function(onlyAvailable) {
        this.showAllMachinesButton.color = onlyAvailable === true ? "#ffffff" : "#2aff00";
        this.showAvailableMachinesButton.color = onlyAvailable === true ? "#2aff00" : "#ffffff";
        this.showAllMachinesButton.isActive = !onlyAvailable;
        this.showAvailableMachinesButton.isActive = onlyAvailable;
    }

    p.backToLocationsButtonHandler = function() {
        //alteastream.StreamPreview.getInstance().exitPreview();
        if(this.lobby.mouseWheel)
            this.lobby.mouseWheel.enable(false);
        this.lobby.backToLocationsFromLobby();
        this._backToLocationsButton.visible = false;

        // blok premesten iz onStreamPreviewDone
        this._machinesComponent.clearActiveMachines();
        this._machinesComponent.disableActiveMachines(true);
        this.lobby.currentMap.clickEnabled(true);
        this.lobby.setBackgroundImagesForStreamPreview(false);
    };

    p.backToLobbyButtonHandler = function() {
        this.streamPreview.exitPreview();
        this._backToLobbyButton.visible = false;
        this.intervalUpdateLounge(false);
        this.intervalUpdateLobby(true);

        this.setListLayout(false);
        this._machinesComponent.clearActiveMachines();
        this.setComponentWhenMachineisFreeToPlay(false);
        this.updateAvailableOrAllMachinesButtonsSkin(false);

        this.queuePanel.exitQueue();

        //new socket remove
        console.log("leaveQueue:: backToLobbyButtonHandler");
        this.queuePanel.leaveQueue(function () {
            console.log("no socket? getMachines here ?? ");
            //that.intervalUpdateLobby(true);// if back from entranceCounter?
        })

        this._soundNotify();
    };

    p.tryLobby = function(){
        if(this.gridLayoutIsActive === false){
            this.backToLobbyButtonHandler();
        }
    };

    // filtering
    p.setListLayout = function(setList) {
        this.gridLayoutIsActive = !setList;
        this.lobby.setBackgroundImagesForStreamPreview(setList);

        if(this._pagination){
            if(this._pagination._numOfPages > 1){
                this._pagination.showPagination(!setList);
            }
        }

        this._machinesComponent.setGridView(!setList);
        this._machinesComponent.setShowOnlyAvailable(setList);

        this._backToLocationsButton.visible = !setList;
        this._backToLobbyButton.visible = setList;
        this._backToLobbyButton.x = setList ? 21 : -55;
        this._backToLobbyButton.y = setList ? -110 : -115;

        this._backToLobbyButton.x += this._backToLobbyButton._singleImgWidth*0.5;
        this._backToLobbyButton.y += this._backToLobbyButton._height*0.5;

        this._locationLabelShape.visible = setList;
        //this._locationLabel.visible = !setList;
        //this._locationLabel.x = setList ? 250 : 36;
        //this._locationLabel.y = setList ? -37 : -37;

        //this._topSectionBackground.visible = !setList;

        this._resetScrollComponent(setList);
        //this._updateMachinesLabel(setList);
        this._updateMachinesLabel();

        this.showAllMachinesButton.visible = !setList;
        this.showAvailableMachinesButton.visible = !setList;
        this.divider.visible = !setList;
    };

    p.onMachineClick = function(machine){
        this.lobby.checkIfHiddenMenuIsShown();
        this.setComponentWhenMachineisFreeToPlay(machine.isFreeToPlay);
        this.streamPreview.tryToSwitchStream(machine);
        if(this.gridLayoutIsActive === true){
            this.setListLayout(true);
            this.queuePanel.enterQueue();
        }else{
            this.queuePanel.exitQueue();
            console.log("leaveQueue:: onMachineClick");
            this.queuePanel.leaveQueue(function () {
                that.queuePanel.enterQueue();
            });
        }

        this._machinesComponent.clearActiveMachines();
        this.intervalUpdateLobby(false);
    };

    p.setComponentWhenMachineisFreeToPlay = function(isFreeToPlay){
        this._machinesComponent.visible = !isFreeToPlay;
        //this._numOfMachinesLabel.visible = !isFreeToPlay;
        this._locationLabel.visible = !isFreeToPlay;
        this.queuePanel.multiPlayerControls.enable(!isFreeToPlay);
        this.queuePanel.updateAvailabilityComponents(isFreeToPlay);
        if(this._pagination){
            if(this._pagination._numOfPages > 1) {
                this._pagination.showPagination(!isFreeToPlay);
            }
        }
    };

    p.populateMachines = function(response){
        this._machinesComponent.populateAllMachines(response);
    };

    p.intervalUpdateLobby = function(bool){
        if(bool === true){
            var requester = alteastream.Requester.getInstance();
            this._machinesComponent.updateThumbnails(this._machinesPerPage, this._scroller.getCurrentScrollLevel());
            var that = this;
            requester.getMachines(alteastream.AbstractScene.SHOP_MACHINE,function(response){
                that._machinesComponent.setMachinesStatus(response);
            });
            this.thumbnailTimer = setInterval(function(){
                console.log(" intervalUpdateLobby>>>>>>>>>>>>>>>>>>>");
                if(that._canUpdateThumbs === true){
                    if(that.gridLayoutIsActive === true){
                        that._machinesComponent.updateThumbnails(that._machinesPerPage, that._scroller.getCurrentScrollLevel());
                    }
                }
                requester.getMachines(alteastream.AbstractScene.SHOP_MACHINE,function(response){
                    that._machinesComponent.setMachinesStatus(response);
                });
            },5000);
        }
        else
            clearInterval(this.thumbnailTimer);
    };
    //new socket
    /*p.intervalUpdateLobby = function(bool){
        if(bool === true){
            this._machinesComponent.updateThumbnails(this._machinesPerPage, this._scroller.getCurrentScrollLevel());
            var that = this;
            this.thumbnailTimer = setInterval(function(){
                console.log(" intervalUpdateLobby>>>>>>>>>>>>>>>>>>>");
                if(that._canUpdateThumbs === true){
                    if(that.gridLayoutIsActive === true){
                        that._machinesComponent.updateThumbnails(that._machinesPerPage, that._scroller.getCurrentScrollLevel());
                    }
                }
            },5000);
        }
        else
            clearInterval(this.thumbnailTimer);
    };*/

    p.intervalUpdateLounge = function(bool){
        var that = this;
        if(bool === true)
            //thumbnailPropsTimer
            this.propsTimer = setInterval(function(){
                that.updateLoungeComponents();
            },2000);
        else
            clearInterval(this.propsTimer);
    };

    /*p.intervalUpdateLounge = function(bool){ // todo mozda kasnije sa setTimeout
        var that = this;
        if(bool === true){
            this.updateLoungeComponents();
            this.propsTimer = setTimeout(function(){
                that.intervalUpdateLounge(true);
            },2000);
        }

        else
            clearTimeout(this.propsTimer);
    };*/

    p.updateLoungeComponents = function(){
        if(this.streamPreview._active){
            alteastream.Requester.getInstance().getCurrentMachine(this.streamPreview.currentStream,function(response){
                that.streamPreview.updateProps(response);
                that.queuePanel._updateAvailabilityComponents(response);
                // filtering
                //if(alteastream.QueuePanel.getInstance().queueExists === true)
                //return;
                //mozda uopste ne ovo sad kad je filtering:
                that._machinesComponent.updateFromCurrentStream(response,that.streamPreview.currentStream);
            });
        }
    };
    //new socket
    /*
    * biggestGameWin: 28
    isFreeToPlay: false
    is_available: true
    lastWin: 6
    name: "4008"
    state: 1
    status: 0
    status_code: 1
    tokensFired: 1176
    totalWin: 38
    */
    /*p.updateLoungeComponents = function(){
        if(this.streamPreview._active){
            alteastream.Requester.getInstance().getCurrentMachine(this.streamPreview.currentStream,function(response){
                that.streamPreview.updateProps(response);
            });
        }
    };*/

    p.resetIntervalUpdateLounge = function(){
        this.intervalUpdateLounge(false);
        this.intervalUpdateLounge(true);
        this.updateLoungeComponents();
    };

// static methods:
// public methods:
    p.focusIn = function(){
        this.visible = true;
        this._bigTent.alpha = 1;

        var method = "focusInAnimated";

        if(alteastream.AbstractScene.BACK_TO_HOUSE !== -1){
            alteastream.AbstractScene.BACK_TO_HOUSE = -1;
            var backgroundMusicIsMuted = window.localStorage.getItem('backgroundMusicIsMuted');
            createjs.Sound.muted = backgroundMusicIsMuted === "false";
            this.lobby.btnSoundHandler();
            method = "focusInRegular";
        }

        this[method]();
    };

    p.focusInRegular = function() {
        this._bigTent.scaleX = this._bigTent.scaleY = 5;
        this._bigTent.alpha = 0;
        this._onFocusDone();
        this.restoreScrollLevelAndSelectedMachine();
    };

    p.focusInAnimated = function() {
        TweenLite.to(this._bigTent,0.5,{scaleX:5,scaleY:5,alpha:0,onComplete:function(that){
                that._onFocusDone();
         },onCompleteParams:[this]});
    };

    p._onFocusDone = function() {
        this.isFocused = true;
        this.setMachinesAndScroll();
        this.showMachinesHideShape(true);
        this.intervalUpdateLobby(true);
        this._backToLocationsButton.visible = true;
        this.lobby.setFocused(this);
        this.lobby.setSwipeTarget(this,"y");
    };

    p.showMachinesHideShape = function(bool) {
        this._bigTent.visible = !bool;
        this._machinesComponent.visible = bool;
        this._topSectionBackground.visible = bool;
        this._locationLabel.visible = bool;
        //this._numOfMachinesLabel.visible = bool;
        this.showAvailableMachinesButton.visible = bool;
        this.showAllMachinesButton.visible = bool;
        this.divider.visible = bool;

        if(this._pagination){
            if(this._pagination._numOfPages > 1) {
                this._pagination.showPagination(bool);
            }
            this._scroller.manageButtonsState();
        }
    };

    p.focusOut = function(objectIn){
        this.intervalUpdateLobby(false);
        //this.streamPreview.exitPreview();
        this.showMachinesHideShape(false);
        this._bigTent.alpha = 0;

        TweenLite.to(this._bigTent,0.5,{scaleX:1,scaleY:1,alpha:1,onComplete:function(that){
                that.isFocused = false;
                that._clearAndReset();
                objectIn.focusIn();
            },onCompleteParams:[this]});
    };

    // filtering
    p.setMachinesAndScroll = function() {
        this._storeLocationName();
        //this._updateLocationLabel();
        //this._updateMachinesLabel(false);
        this._updateMachinesLabel();
        this._resetScrollComponent(false);
        this._machinesComponent.disableActiveMachines(false);
    };

    p._storeLocationName = function() {
        this._locationName = this.lobby.selectedLocation.casinoName.charAt(0).toUpperCase() + this.lobby.selectedLocation.casinoName.substring(1);
    };

    p.onMouseWheel = function(direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    // filtering
    p._resetScrollComponent = function(listView){
        var numOfPages = this._machinesComponent.getNumberOfPages();
        var machineHeight = this._machinesComponent.getMachineHeight();

        this._scroller.scrollByPageCurrentLevel = 0;
        this._scroller.maxScrollLevelByPage = numOfPages - 1;

        var scrollLevelsByRow = [];
        var numberOfRows = Math.ceil(this._machinesComponent.getNumberOfActiveMachines() / this._machinesComponent.getColumnsPerPage());
        var numberOfScrollLevelsByRow = numberOfRows - this._machinesComponent.getRowsPerPage() + 1;
        for(var j = 0; j < numberOfScrollLevelsByRow; j++){
            scrollLevelsByRow.push(j * machineHeight * -1);
        }

        this._scroller.setScrollLevels(scrollLevelsByRow);
        this._scroller.setRowsPerPage(this._machinesComponent.getRowsPerPage());

        if(this._pagination){
            this._pagination.resetComponent(numOfPages);
            if(this._machinesComponent.visible === true){// todo temp solution
                this._pagination.showPagination(numOfPages > 1);
            }
            //this._pagination.setComponentForListView(listView);
            this._scroller.updateCurrentDotPosition();
            this._scroller.setNumberOfPages(numOfPages);
            if(this._pagination.hasButtons === true){
                this._pagination._setButtonsStateOnStart(numOfPages);
            }
            //this._pagination._dotsContainer.rotation = listView ? 90 : 0;
            //this._pagination.x = listView ? -25 : alteastream.AbstractScene.GAME_WIDTH - this._pagination.getPaginationWidth() - this.x - 200;
            //this._pagination.y = listView ? 15 : 872;
            //this._updateButtonsPosition();
            this._scroller.manageButtonsState();
        }

        this._machinesComponent.y = 0;
    };

    p._updateButtonsPosition = function(){
        //this._pagination._upButton.x = listView ? 138 : - 60;
        //this._pagination._downButton.x = listView ? 138 : this._pagination.getPaginationWidth() + 20;
        this._pagination._downButton.x = this._pagination.getPaginationWidth() + 20;
        //this._pagination._upButton.y = listView ? 730 : 3;
        //this._pagination._downButton.y = listView ? 778 : 0;
    };

/*    p._updateLocationLabel = function() {
        this._locationLabel.text = this.lobby.selectedLocation.casinoName.charAt(0).toUpperCase() + this.lobby.selectedLocation.casinoName.substring(1);
    };*/

    p._updateMachinesLabel = function(setList){
        var numOfMachines = " " + this._machinesComponent.getNumberOfActiveMachines();
        var moreThanOne = numOfMachines > 1 ? " machines" : " machine";
        //this._numOfMachinesLabel.text = numOfMachines + moreThanOne;
        //this._numOfMachinesLabel.x = setList ? 95 : this._locationLabel.x + this._locationLabel.getMeasuredWidth() + 10;

        this._locationLabel.text = this.gridLayoutIsActive ? this._locationName + numOfMachines + moreThanOne : numOfMachines + moreThanOne + " free to play";
        //this._locationLabel.x = this.gridLayoutIsActive ? 36 : 80;
    };

    p.restoreScrollLevelAndSelectedMachine = function() {
        var scrollLevel = Number(window.localStorage.getItem('scrollLevel'));
        //var selectedMachineId = Number(window.localStorage.getItem('selectedMachineId'));//staging new
        if(scrollLevel > 0){
            this._scroller.scrollByPageCurrentLevel = scrollLevel;
            //this._manageButtonsState();
            this._scroller.scrollByPage();
        }
        // mozda nema potrebe za ovom proverom posto se metoda poziva samo ako back to house nije -1
        // tj. samo kada se desio povratak iz masine u lobi
        //if(selectedMachineId){//staging new
        //this._machinesComponent.getMachineById(selectedMachineId)._onMouseClick();
        //}
    };

    //StreamPreview callbacks
    //QueuePanel callbacks
    p.onQueueEntered = function(){
        //this._machinesComponent.disableActiveMachines(true);
        this.lobby.currentMap.clickEnabled(false);
        this._soundNotify();
    };

    p.onStartGame = function(){// invalid token fix new
        this.queuePanel.btnStart.setDisabled(true);
        var that = this;
        alteastream.Requester.getInstance().startGameStream(function(response){
            var resp = JSON.stringify(response);

            if(that.lobby.mouseWheel)
                that.lobby.mouseWheel.enable(false);

            that.lobby.onStartGameStream();
            that.intervalUpdateLobby(false);
            that.lobby.socketCommunicator.disposeCommunication();

            that.queuePanel.removeAllChildren();

            that.streamPreview.dispose();
            that.intervalUpdateLounge(false);

            var vid = document.getElementById("videoOutput");
            vid.parentNode.removeChild(vid);

            alteastream.Assets.stopAllSounds();

            setTimeout(function(){that.goToMachine(resp);},1000);
        })
    };

    p.goToMachine = function(response){
        //var qStr = "?usr="+alteastream.AbstractScene.GAME_TOKEN+"&serverUrl="+alteastream.AbstractScene.SERVER_URL+"&machine_name="+alteastream.AbstractScene.GAME_ID+"&id="+Lobby.SHOP_MACHINE;
        window.localStorage.setItem('backToHouse', String(alteastream.AbstractScene.SHOP_MACHINE));
        window.localStorage.setItem('machineParameters', response);// invalid token fix new
        window.localStorage.setItem('scrollLevel', String(this._scroller.scrollByPageCurrentLevel));
        window.localStorage.setItem('backgroundMusicIsMuted', String(createjs.Sound.muted));

        //window.localStorage.setItem('selectedMachineId', String(this._machinesComponent.getActiveMachine().id));//staging new
        //window.top.switchSource("bgMusic"); // hajkova zelja
        //window.top.manageMachineNoise("play");

        //var qStr = "?usr="+alteastream.AbstractScene.GAME_TOKEN+"&serverUrl="+alteastream.AbstractScene.SERVER_URL+"&machine_name="+alteastream.AbstractScene.GAME_ID+"&game="+"rmgame"+"&gameCode="+alteastream.AbstractScene.GAME_CODE+"&backToHouse="+alteastream.AbstractScene.SHOP_MACHINE;
        var qStr = "?usr="+alteastream.AbstractScene.GAME_TOKEN+"&serverUrl="+alteastream.AbstractScene.SERVER_URL+"&machine_name="+alteastream.AbstractScene.GAME_ID+"&game="+"rmgame"+"&gameCode="+alteastream.AbstractScene.GAME_CODE+"&gameName="+this.streamPreview.currentMachineName;
        //window.top.manageBgMusic("pause");
        window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_PROTOCOL+alteastream.AbstractScene.SERVER_IP+"/gameplay/"+qStr);
    };
    
    p._soundNotify = function(){
        alteastream.Assets.playSound("confirm",0.3);
    };

// private methods:
    p._clearAndReset = function(){
        this.visible = false;
        this._bigTent.scaleX = this._bigTent.scaleY = 1;
    };

    alteastream.LocationMachines = createjs.promote(LocationMachines,"Container");
})();