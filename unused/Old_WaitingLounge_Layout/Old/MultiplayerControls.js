
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MultiplayerControls = function(){
        this.Container_constructor();
        this.initMultiplayerControls();
    };

    var p = createjs.extend(MultiplayerControls,createjs.Container);
    var _this;
    p._stakes = [];
    p._selectedStake = 0;
    p._betCounter = null;
    p._countdownTime = 0;
    p._win = 0;
    p._balance = 0;

    p.initMultiplayerControls = function () {
        console.log("MultiplayerControls initialized:::");
        _this = this;
        this.setLayout();
    };

    MultiplayerControls.getInstance = function() {
        return _this;
    };

    p.setLayout = function(){
        var controlsBgShapeLeft = this.controlsBgShapeLeft = new createjs.Shape();
        controlsBgShapeLeft.graphics.beginFill("#101010").drawRoundRect(0, 0, 233, 90, 5);
        controlsBgShapeLeft.cache(0, 0, 233, 90);
        controlsBgShapeLeft.size = {width:233, height:90, roundness:5};
        controlsBgShapeLeft.x = 21;
        controlsBgShapeLeft.y = 813;
        controlsBgShapeLeft.mouseEnabled = false;
        this.addChild(controlsBgShapeLeft);
        controlsBgShapeLeft.alpha = 0.7;

        var controlsBgShapeRight = this.controlsBgShapeRight = new createjs.Shape();
        controlsBgShapeRight.graphics.beginFill("#101010").drawRoundRect(0, 0, 1436, 90, 5);
        controlsBgShapeRight.cache(0, 0, 1436, 90);
        controlsBgShapeRight.size = {width:1436, height:90, roundness:5};
        controlsBgShapeRight.x = 264;
        controlsBgShapeRight.y = 813;
        controlsBgShapeRight.mouseEnabled = false;
        this.addChild(controlsBgShapeRight);
        controlsBgShapeLeft.alpha = 0.7;

        this.separatorContainer = new createjs.Container();
        var separator = alteastream.Assets.getImage(alteastream.Assets.images.separator);
        this.separatorContainer.addChild(separator);
        separator.mouseEnabled = false;
        separator.x = 900;
        separator.y = 838;

        var separator2 = separator.clone();
        this.separatorContainer.addChild(separator2);
        separator2.mouseEnabled = false;
        separator2.x = 1031;
        separator2.y = 838;

        var separator3 = separator.clone();
        this.separatorContainer.addChild(separator3);
        separator3.mouseEnabled = false;
        separator3.x = 1480;
        separator3.y = 838;
        this.addChild(this.separatorContainer);

        this._textColorDisabled = "#383838";
        this._textColorEnabled = "#ffffff";

        var currency = "EUR";//alteastream.Lobby.getInstance().currency;
        var winLabel = this.winLabel = new createjs.Text("WIN ( "+currency+" ) ","15px HurmeGeometricSans3",this._textColorDisabled).set({textAlign:"center", textBaseline:"middle"});
        winLabel.x = 360;
        winLabel.y = 849;
        winLabel.mouseEnabled = false;
        this.addChild(winLabel);

        var winAmount = this.winAmount = new createjs.Text("0.00","bold 24px HurmeGeometricSans3",this._textColorDisabled).set({textAlign:"center", textBaseline:"middle"});
        winAmount.x = winLabel.x;
        winAmount.y = winLabel.y + 25;
        winAmount.mouseEnabled = false;
        this.addChild(winAmount);

        var stakeLabel = this.stakeLabel = new createjs.Text("STAKE ( "+currency+" ) ","15px HurmeGeometricSans3",this._textColorEnabled).set({textAlign:"center", textBaseline:"middle"});
        stakeLabel.x = 968;
        stakeLabel.y = winLabel.y;
        stakeLabel.mouseEnabled = false;
        this.addChild(stakeLabel);

        var stakeAmount = this.stakeAmount = new createjs.Text("0.00","bold 24px HurmeGeometricSans3",this._textColorEnabled).set({textAlign:"center", textBaseline:"middle"});
        stakeAmount.x = stakeLabel.x;
        stakeAmount.y = stakeLabel.y + 25;
        stakeAmount.mouseEnabled = false;
        this.addChild(stakeAmount);

        var btnStakeDec = this.btnStakeDec = this._addButton("btnStakeDec",820,830);
        btnStakeDec.action = -1;
        this.addChild(btnStakeDec);
        btnStakeDec.setClickHandler(function(e){
            _this._onStakeChange(e);
        })

        var btnStakeInc = this.btnStakeInc = this._addButton("btnStakeInc",1054,830);
        btnStakeInc.action = 1;
        this.addChild(btnStakeInc);
        btnStakeInc.setClickHandler(function(e){
            _this._onStakeChange(e);
        })

        var txtStartBet = new createjs.Text("START BETTING","16px HurmeGeometricSans3","#ffffff");
        var txtEndBet = new createjs.Text("STOP BETTING","16px HurmeGeometricSans3","#ffffff");
        var btnParticipate = this.btnParticipate = this._addButton("btnParticipate",1520,860,txtStartBet,1);
        this.addChild(btnParticipate);

        btnParticipate.centerText(85,0);
        btnParticipate.addTextPreset("start", txtStartBet);
        btnParticipate.addTextPreset("stop", txtEndBet);

        var area = new createjs.Shape();
        area.graphics.beginFill("#fff").drawRect(0, 0, btnParticipate.width+130, btnParticipate.height);
        btnParticipate.hitArea = area;

        var betCounter = this._betCounter = new alteastream.BetEntranceCounter();
        betCounter.x = btnParticipate.x;
        betCounter.y = btnParticipate.y;
        this.addChild(betCounter);
        betCounter.mouseEnabled = false;

        var focusShape = this.focusShape = new createjs.Shape();
        focusShape.graphics.beginStroke("#ffcc00");
        focusShape.graphics.setStrokeStyle(3);
        focusShape.graphics.drawRoundRect(0, 0, 1435, 796, 6);
        focusShape.cache(0, 0, 1435, 796);
        this.addChild(focusShape);
        focusShape.x = 264;
        focusShape.y = 4;
        focusShape.mouseEnabled = false;

        var focusShape2 = this.focusShape2 = new createjs.Shape();
        focusShape2.graphics.beginStroke("#ffcc00");
        focusShape2.graphics.setStrokeStyle(3);
        focusShape2.graphics.drawRoundRect(0, 0, 1435, 90, 6);
        focusShape2.cache(0, 0, 1435, 90);
        this.addChild(focusShape2);
        focusShape2.x = 264;
        focusShape2.y = 813;
        focusShape2.mouseEnabled = false;

        this._reset();
    };

    p.setBettingCounterTime = function(time){
        time = time/1000;
        this._countdownTime = time || 60;
    };

    p.setBetToStart = function(bool) {//mp
        if(bool) {
            this.btnParticipate.selectTextPreset("start");
            this.btnParticipate.setClickHandler(function(){
                console.log("comm send:: doBetting ");
                _this._showStakeButtons(false);
                alteastream.SocketCommunicatorLobby.getInstance().doBetting(true);
                //var response = {vl:10};// local ver
                //_this.startBetting(response);// local ver
            })
        } else {
            this.btnParticipate.selectTextPreset("stop");
            this.btnParticipate.setClickHandler(function(){
                console.log("comm send:: stopBetting ");
                alteastream.SocketCommunicatorLobby.getInstance().doBetting(false);
                //_this.stopBetting();// local ver
            })
        }
        var btnImage = bool === true?"btnParticipate":"btnStopParticipate";
        this.btnParticipate.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));
        this.btnParticipate.centerText(85,0);
    };

    p._reset = function(){
        this.setBetToStart(true);
        this.btnStakeDec.setDisabled(true);
        this.btnStakeInc.setDisabled(true);

        this._showUI(false);
        this._enableUI(false);
        this._betCounter.quitCounter();
    };

    p.setBettingUI = function(bool){
        this.setBetToStart(!bool);
        this._betCounter.quitCounter();
        this._enableUI(bool);
    };

    p.show = function(bool){//entire component hide/show
        this.visible = bool;
    };

    p._showUI = function(bool){
        this.separatorContainer.visible =
        this.btnParticipate.visible =
        this.stakeLabel.visible =
        this.stakeAmount.visible =
        this.winAmount.visible =
        this.winLabel.visible = bool;

        //temp, no stake change now
        //this.btnStakeDec.visible =
            //this.btnStakeInc.visible = true;
        this._showStakeButtons(bool);
    };

    p.enable = function(bool){
        if(bool===false){
            this._reset();
        }else{
            this._showUI(true);
            this.showCounter(this._countdownTime);
        }
    };

    p._enableUI = function(bool){
        this.focusShape.visible = this.focusShape2.visible = bool;
        this.winAmount.color =
        this.winLabel.color =
        /*this.stakeAmount.color =
        this.stakeLabel.color =*/ bool === true? this._textColorEnabled: this._textColorDisabled;
    };

    p.showCounter = function(time){
        //from socket
        console.log("SHOW COUNTER " +time);
        this._betCounter.show(true);
        this._betCounter.update(time);

        this.setInitialStakes([0.15,0.25,1.5,1.8,2.0]);// temp, from response
    };

    p._showStakeButtons = function(bool){
        this.btnStakeDec.visible =
            this.btnStakeInc.visible = bool;
    };

    p.setInitialStakes = function(stakes){
        this._stakes = stakes;
        this.setStake(0); //if stakes array
    };

    p._setConfirmedStake = function(stake){
        stake = stake/100;
        this.setStake(this._stakes.indexOf(stake)); //if stakes array
        //stake = stake/1000;// novo s vladom
        //this.setStake(stake);// novo s vladom
    };

    p.startBetting = function(msg){
        console.log("MP STAKE:: "+msg.vl);
        this.setBettingUI(true);
        this._setConfirmedStake(msg.vl);
        //this._showStakeButtons(false);
        alteastream.Assets.playSound("confirm",0.3);
    };

    p.stopBetting = function(){//mp // on button & expired counter
        this.quitBetting();
        //alteastream.LocationMachines.getInstance().tryLobby();
        alteastream.LocationMachines.getInstance().backToLobbyButtonHandler();
        alteastream.QueuePanel.getInstance().leaveQueue(function () {
            console.log("no socket? getMachines here ?? ");
            //that.intervalUpdateLobby(true);// if back from entranceCounter?
        });
    };

    p.betEvent = function(msg){
        //vl:{"am":12345,"bal":12345,"iswin":false}
        //this._win = msg.vl/100;
        //this._win += msg.vl/100;
        //msg = JSON.parse(msg);

        var values = JSON.parse(msg.vl);

        this._balance = values.bal;
        alteastream.Lobby.getInstance().updateLobbyBalance(this._balance);

        if(values.iswin === true){
            this._win += values.am/100;
            var winText = (Math.round(this._win * 1e12) / 1e12).toFixed(2);
            winText += alteastream.Lobby.getInstance()._addZeros(winText,1);
            this.winAmount.text = winText;
            this._resetWinCountTimeout(msg);
        }
        alteastream.Assets.playSound("confirm",0.3);// change sound
    };

    p._resetWinCountTimeout = function(msg) {
        clearTimeout(this._winCountTimer);
        this._winCountTimer = setTimeout(function(){
            _this._win = 0;
            _this.winAmount.text = "0.00";
            //todo u ovom response objektu nema balansa
            //_this._balance = msg;
            //alteastream.Lobby.getInstance().updateLobbyBalance(this._balance);
        },2000);//adjustable
    };

    p.quitBetting = function(){// MACHINE_READY_FOR_PLAYER
        this._selectedStake = 0;
        this.btnStakeDec.setDisabled(true);
        this.btnStakeInc.setDisabled(true);
        this.setBettingUI(false);
    };

    //if stakes array
    p.setStake = function(position){
        this._selectedStake += position;
        var stake = this._stakes[this._selectedStake].toFixed(2);
        this.stakeAmount.text = stake;
        this.btnStakeInc.setDisabled(this._selectedStake === this._stakes.length-1);
        this.btnStakeDec.setDisabled(this._selectedStake === 0);
        alteastream.SocketCommunicatorLobby.getInstance().setBetStake(stake);
        //this.setBetLabelToMax(this._selectedStake === (this._stakes.length - 1));
    };

    /*p.setStake = function(stake){
        //this.stakeAmount.text = stake.toFixed(2);
        this.stakeAmount.text = stake;// novo s vladom
    };*/

    p._onStakeChange = function(e){
        this.setStake(e.currentTarget.action);
        console.log("comm send:: stake: "+this._stakes[this._selectedStake]);//Date.now()
        alteastream.Assets.playSound("confirm",0.3);// todo promeniti sound
    };
    
    p._addButton = function(assetName,x,y,text,states){
        var textLabel = text||null;
        states = states || 3;
        var btn = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images[assetName]),states,textLabel);
        btn.x = x;
        btn.y = y;
        return btn;
    };

    p.adjustMobile = function(){
        var controlsBgShapeLeft = this.controlsBgShapeLeft;
        controlsBgShapeLeft.uncache();
        controlsBgShapeLeft.graphics.clear();

        var controlsBgShapeRight = this.controlsBgShapeRight;
        controlsBgShapeRight.uncache();
        controlsBgShapeRight.graphics.clear();
        controlsBgShapeRight.graphics.beginFill("#000000").drawRoundRect(0, 0, 620, 50, 5);
        controlsBgShapeRight.cache(0, 0, 620, 50);
        controlsBgShapeRight.size = {width:620, height:50, roundness:5};
        controlsBgShapeRight.x = 317;
        controlsBgShapeRight.y = 405;
        controlsBgShapeLeft.alpha = 0.7;

        var focusShape = this.focusShape;
        focusShape.uncache();
        focusShape.graphics.clear();
        focusShape.graphics.beginStroke("#ffcc00");
        focusShape.graphics.setStrokeStyle(2);
        focusShape.graphics.drawRoundRect(0, 0, 620, 390, 6);
        focusShape.cache(0, 0, 620, 390);
        focusShape.x = 317;
        focusShape.y = 10;

        var focusShape2 = this.focusShape2;
        focusShape2.uncache();
        focusShape2.graphics.clear();
        focusShape2.graphics.beginStroke("#ffcc00");
        focusShape2.graphics.setStrokeStyle(2);
        focusShape2.graphics.drawRoundRect(0, 0, 620, 50, 6);
        focusShape2.cache(0, 0, 620, 50);
        focusShape2.x = 317;
        focusShape2.y = 405;

        //controlsBgShapeLeft.visible = false;

        var winLabel = this.winLabel;
        winLabel.x = 385;
        winLabel.y = 420;

        var winAmount = this.winAmount;
        winAmount.x = winLabel.x;
        winAmount.y = winLabel.y + 20;

        var xCorrector = 10;

        var btnStakeDec = this.btnStakeDec;
        btnStakeDec.x = 519 - xCorrector;
        btnStakeDec.y = 410;

        var btnStakeInc = this.btnStakeInc;
        btnStakeInc.x = 679 - xCorrector;
        btnStakeInc.y = 410;

        var stakeLabel = this.stakeLabel;
        stakeLabel.x = 612;
        stakeLabel.y = winLabel.y;

        var stakeAmount = this.stakeAmount;
        stakeAmount.x = stakeLabel.x;
        stakeAmount.y = stakeLabel.y + 20;

        var btnParticipate = this.btnParticipate;
        btnParticipate.x = 802;
        btnParticipate.y = 430;

        //btnParticipate.centerText(95,0);

        btnStakeInc.scale = btnStakeDec.scale = btnParticipate.scale = 0.80;

        var betCounter = this._betCounter;
        betCounter.x = btnParticipate.x;
        betCounter.y = btnParticipate.y;
        betCounter.adjustMobile();

        this.separatorContainer.removeAllChildren();

        this.y += 68;
    }


    alteastream.MultiplayerControls = createjs.promote(MultiplayerControls,"Container");
})();