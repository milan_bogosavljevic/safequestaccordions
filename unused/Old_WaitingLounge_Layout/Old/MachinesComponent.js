
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MachinesComponent = function(rows, columns){
        this.Container_constructor();
        this.initMachinesContainer(rows, columns);
    };

    var p = createjs.extend(MachinesComponent,createjs.Container);
    var _this;

    p.MAX_NUMBER_OF_MACHINES = 70;
    p._totalRows = 0;
    p._columnsPerPage = 0;
    p._rowsPerPage = 0;
    p._ySpacing = 0;
    p._xSpacing = 0;
    p._startPositionX = 0;
    p._startPositionY = 0;
    p._numOfActiveMachines = 0;

    // filtering
    p.showOnlyAvailable = false;
    p._currentlySelectedMachineId = 0;

    p.initMachinesContainer = function (rows, columns) {
        console.log("MachinesComponent initialized:::");
        _this = this;
        this._columnsPerPage = columns;
        this._rowsPerPage = rows;
        this.setMachines();
    };

    MachinesComponent.getInstance = function() {
        return _this;
    };

    p.setMachines = function(){
        this._totalRows = Math.ceil(this.MAX_NUMBER_OF_MACHINES/this._columnsPerPage);
        this._startPositionX = 21;
        this._startPositionY = 2;
        this._ySpacing = 25;
        this._xSpacing = 7;
        this.addMachines();
    };

    p.setGridView = function (grid) {
        this._columnsPerPage = grid === true ? 7 : 1;
        this._rowsPerPage = grid === true ? 3 : 17;//15 with pageButtons
        this._totalRows = Math.ceil(this.MAX_NUMBER_OF_MACHINES/this._columnsPerPage);
        this._startPositionX = 21;
        this._startPositionY = grid === true ? 2 : 3;
        this._xSpacing = grid === true ? 7 : 0;
        this._ySpacing = grid === true ? 25 : 4;//3 with pageButtons
        this.rearrangeMachinesAsGrid(grid);
        this.updateMask();
    };

    p.rearrangeMachinesAsGrid = function(setGrid){
        var xCount = 0;
        var yCount = 0;
        for(var i = 0, j = this.MAX_NUMBER_OF_MACHINES; i < j; i++){
            var machine = this.machines[i];
            machine.setGridMachine(setGrid);
            machine.x = this._startPositionX + xCount * (machine.getWidth() + this._xSpacing);
            machine.y = this._startPositionY + yCount * (machine.getHeight() + this._ySpacing);
            xCount++;
            if(xCount === this._columnsPerPage){
                xCount = 0;
                yCount++;
            }
        }
    };

    p.updateMask = function(){
        var maskWidth = this.getWidth();
        var maskHeight = this.getMaskHeight();
        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(this._startPositionX, this._startPositionY, maskWidth, maskHeight);
        this.mask = mask;
    };

    p.addMachines = function () {
        this.machines = [];
        var xCount = 0;
        var yCount = 0;
        for(var i = 0, j = this.MAX_NUMBER_OF_MACHINES; i < j; i++){
            var machine = new alteastream.Machine();
            machine.x = this._startPositionX + xCount * (machine.getWidth() + this._xSpacing);
            machine.y = this._startPositionY + yCount * (machine.getHeight() + this._ySpacing);
            this.machines.push(machine);
            this.addChild(machine);

            xCount++;
            if(xCount === this._columnsPerPage){
                xCount = 0;
                yCount++;
            }
        }
    };

    p.populateAllMachines = function(machines){
        this.machinesFromServer = machines;
        this.resetComponent(machines);
    };

    p.getMachines = function(){
        return this.machines;
    };

    p.getHeight = function () {
        var machineHeight = this.machines[0].getHeight();
        return this._rowsPerPage * (machineHeight + this._ySpacing);
    };

    p.getWidth = function () {
        var machineWidth = this.machines[0].getWidth();
        return this._columnsPerPage * (machineWidth + this._xSpacing);
    };

    p.getMaskHeight = function() {
        var containerHeight = this.getHeight();
        return containerHeight - this._ySpacing;
    };

    p.getNumberOfPages = function () {
        return Math.ceil(this._totalRows/this._rowsPerPage);
    };

    p.getMaxNumberOfPages = function () {
        var machinesPerPage = this._rowsPerPage*this._columnsPerPage
        return Math.ceil(this.MAX_NUMBER_OF_MACHINES/machinesPerPage);
    };

    p.getMachineHeight = function() {
        var machineHeight = this.machines[0].getHeight();
        return (machineHeight + this._ySpacing);
    };
    
    p.disableActiveMachines = function(bool){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            this.machines[i].disableMouseListeners(bool);
        }  
    };

    p.clearActiveMachines = function(){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            this.machines[i].resetDisplay();
        }
    };

    p.updateThumbnails = function(machinesPerPage, currentScrollLevel){
        var start = this._columnsPerPage * currentScrollLevel;
        var end = start + machinesPerPage;

        if(end > this.getNumberOfActiveMachines()){
            var correction = end - this.getNumberOfActiveMachines();
            end -= correction;
        }

        for(var i = start;i<end;i++){
            this.machines[i].updateThumbnail();
        }
    };

    p.updateFromCurrentStream = function(response,currentStream){
        var machine = this.getMachineById(currentStream);
        if(machine) // nema ni jedna slobodna masina?
        if(machine.getState() === machine.STATE_NORMAL){ //symbols
            if(machine.isFreeToPlay !== response.isFreeToPlay){
                machine.isFreeToPlay = response.isFreeToPlay;// temp patch to prevent double calls
                alteastream.Requester.getInstance().getMachines(alteastream.AbstractScene.SHOP_MACHINE,function(response){
                    _this.setMachinesStatus(response);
                });
            }else{
                machine.setStatus(response);
            }
        }
    };

    p.cacheMachines = function(bool){
        for(var i = 0,j=this._numOfActiveMachines;i<j;i++){
            this.machines[i].doCache(bool);
        }
    };

/*    p.setMachinesStatus = function(machine){
        var machineToUpdate = this.getMachineById(machine.machineIndex);
        if(machineToUpdate){
            // temp block, waiting for all keys
            //////////////////////////////
            machine.isOnline = machineToUpdate.isOnline;
            machine.queueSize = Number(machineToUpdate.queueNumber.text);
            machine.statusCode = 0;
            machine.isInAutoplay = false;
            //////////////////////////////
            machineToUpdate.setStatus(machine);
        }

        for(var p in this.machinesFromServer){
            var machineFromPreviousResponse = this.machinesFromServer[p];
            if(machineFromPreviousResponse.machineIndex === machine.machineIndex){
                //this.machinesFromServer[p] = machine;
                //temp fix with 2 keys only
                this.machinesFromServer[p].isFreeToPlay = machine.isFreeToPlay;
            }
        }
        if(this.showOnlyAvailable === true){
            this.resetComponent(this.machinesFromServer);
        }
    };*/

    //new socket on notify
    p.setMachinesStatus = function(machine){
        var machineToUpdate = this.getMachineById(machine.machineIndex);
        if(machineToUpdate){
            var newMachine = {};

            newMachine.isOnline = machine.hasOwnProperty("isOnline") ? machine.isOnline : machineToUpdate.isOnline;
            newMachine.queueSize = machine.hasOwnProperty("size") ? machine.size : machineToUpdate.queueNumber.text;
            newMachine.statusCode = machine.hasOwnProperty("statusCode") ? machine.statusCode : machineToUpdate.statusCode;
            newMachine.isInAutoplay = machine.hasOwnProperty("isInAutoplay") ? machine.isInAutoplay : machineToUpdate.isInAutoplay;
            newMachine.isFreeToPlay = machine.hasOwnProperty("isFreeToPlay") ? machine.isFreeToPlay : machineToUpdate.isFreeToPlay;

            machineToUpdate.setStatus(newMachine);
        }
        for(var p in this.machinesFromServer){
            var machineFromPreviousResponse = this.machinesFromServer[p];
            if(machineFromPreviousResponse.machineIndex === machine.machineIndex){
                machineFromPreviousResponse.isFreeToPlay = machine.hasOwnProperty("isFreeToPlay") ? machine.isFreeToPlay : machineFromPreviousResponse.isFreeToPlay;
                machineFromPreviousResponse.isInAutoplay = machine.hasOwnProperty("isInAutoplay") ? machine.isInAutoplay : machineFromPreviousResponse.isInAutoplay;
                machineFromPreviousResponse.isOnline = machine.hasOwnProperty("isOnline") ? machine.isOnline : machineFromPreviousResponse.isOnline;
                machineFromPreviousResponse.queueSize = machine.hasOwnProperty("size") ? machine.size : machineFromPreviousResponse.queueSize;
                machineFromPreviousResponse.statusCode = machine.hasOwnProperty("statusCode") ? machine.statusCode : machineFromPreviousResponse.statusCode;
            }
        }
        if(this.showOnlyAvailable === true){
            this.resetComponent(this.machinesFromServer);
        }
    };

    p.getActiveMachine = function(){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            if(this.machines[i].selected === true)
                return this.machines[i];
        }
    };

    p.getMachineById = function(id){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            if(this.machines[i].id === id){
                return this.machines[i];
            }
        }
    };

    // filtering
    p.resetComponent = function(machines) {
        console.log("RESET COMPONENT");
        this._numOfActiveMachines = Object.keys(machines).length;
        this.numOfAvailableMachines = 0;
        var machinesToShow = [];
        var key;
        var machine;
        var activeMachine = this.getActiveMachine();
        if(this.showOnlyAvailable === true){
            for(var s = 0, t = this._numOfActiveMachines; s < t; s++){
                key = Object.keys(machines)[s];
                machine = machines[key];
                if(machine.isFreeToPlay === true && machine.isOnline === true){
                    if(!((parseInt("00001000",2) & machine.statusCode) > 0 || machine.isInAutoplay === true)){// exclude MAINTENANCE state also from showing
                        machinesToShow.push(machine);
                        this.numOfAvailableMachines++;
                    }
                }
            }
        }else{
            for(var u = 0, x = this._numOfActiveMachines; u < x; u++){
                key = Object.keys(machines)[u];
                machine = machines[key];
                machinesToShow.push(machine);
                this.numOfAvailableMachines++;
            }
        }
        var numberOfMachinesToShow = machinesToShow.length;
        for(var i = 0, j = this.MAX_NUMBER_OF_MACHINES; i < j; i++){
            if(i < numberOfMachinesToShow){
                this.machines[i].setMachineInfo(machinesToShow[i]);
                this.machines[i].showMachine(true);
/*                if(this.showOnlyAvailable === false){
                    this.machines[i].updateThumbnail();
                }*/
            }else{
                this.machines[i].showMachine(false);
                this.machines[i].resetMachineProperties();
            }
        }
        if(activeMachine){activeMachine.selectMachine(true);}
        var numberOfMachinesToDivide = this.showOnlyAvailable === true ? this.numOfAvailableMachines : this._numOfActiveMachines;
        this._totalRows = Math.ceil(numberOfMachinesToDivide/this._columnsPerPage);
        this.parent._updateMachinesLabel();
    };

    // filtering
    p.setShowOnlyAvailable = function(onlyAvailable){
/*        this.showOnlyAvailable = show;
        this.resetComponent(this.machinesFromServer);*/
        this.showOnlyAvailableOrAllMachines(onlyAvailable);
        var streamPreview = alteastream.StreamPreview.getInstance();
        for(var i = 0; i < this._numOfActiveMachines; i++){
            if(this.machines[i].id === streamPreview.currentStream){
                this.machines[i].selectMachine(true);
            }
        }
    };

    p.showOnlyAvailableOrAllMachines = function(onlyAvailable) {
        this.showOnlyAvailable = onlyAvailable;
        this.resetComponent(this.machinesFromServer);
    };

    // filtering
    p.setCurrentlySelectedMachineId = function(ind) {// todo proveriti da li moze currentStream umesto _currentlySelectedMachineId
        this._currentlySelectedMachineId = ind;
    };

    p.getColumnsPerPage = function() {
        return this._columnsPerPage;
    };

    p.getRowsPerPage = function() {
        return this._rowsPerPage;
    };

    // filtering
    p.getNumberOfActiveMachines = function() {
        return this.showOnlyAvailable === true ?  this.numOfAvailableMachines : this._numOfActiveMachines;
    };

    p.adjustMobile = function(){
        var numOfMachines = this.MAX_NUMBER_OF_MACHINES;
        var font = "13px HurmeGeometricSans3";
        var upperTxtYPos = 10;
        var yCount = 0;
        this._startPositionX = 6;
        this._startPositionY = 80;
        for(var i = 0; i < numOfMachines; i++) {
            var machine = this.getChildAt(i);

            machine.x = this._startPositionX;
            machine.y = this._startPositionY + yCount * (machine.getHeight() + this._ySpacing);
            yCount++;

            machine.machineIdLabel.font = font;
            machine.machineIdLabel.x = 30;
            machine.machineIdLabel.y = upperTxtYPos;

            machine._idText.font = font;
            machine._idText.x = machine.machineIdLabel.x + machine.machineIdLabel.getBounds().width + 5;
            machine._idText.y = upperTxtYPos;

            machine.machineNameLabel.font = font;
            machine.machineNameLabel.x = 200;
            machine.machineNameLabel.y = upperTxtYPos;

            machine._nameText.font = font;
            machine._nameText.x = machine.machineNameLabel.x + machine.machineNameLabel.getBounds().width + 5;
            machine._nameText.y = upperTxtYPos;

            machine.availabilityLabel.font = font;
            machine.availabilityLabel.x = 258;
            machine.availabilityLabel.y = 174;

            machine.queueNumber.font = font;
            machine.queueNumber.x = 258;
            machine.queueNumber.y = machine.availabilityLabel.y;

            machine._availabilityImage.scale = 0.8;
            machine._availabilityImage.x = 32;
            machine._availabilityImage.y = 161;

            machine.spinner.x = 155;
            machine.spinner.y = 15;

            machine.frame.y = 0;

            machine.thumbScaleX = 0.61;//63
            machine.thumbScaleY = 0.59;//64
            machine.thumb.y = 13;//7
        }

        this.setGridView = function (grid) {
            this._columnsPerPage = grid === true ? 3 : 1;
            this._rowsPerPage = grid === true ? 2 : 8;
            this._totalRows = Math.ceil(this.MAX_NUMBER_OF_MACHINES/this._columnsPerPage);
            this._xSpacing = grid === true ? 10 : 0;
            this._ySpacing = grid === true ? 15 : 6.5;
            this.rearrangeMachinesAsGrid(grid);
            this.updateMask();
        };

        this.setGridView(true);
    }

    alteastream.MachinesComponent = createjs.promote(MachinesComponent,"Container");
})();