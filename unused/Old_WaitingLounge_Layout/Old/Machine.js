
this.alteastream = this.alteastream || {};

(function(){
    "use strict";

    var Machine = function(){
        this.Container_constructor();
        this.initMachine();
    };

    var p = createjs.extend(Machine,createjs.Container);
    p.WIDTH = 0;
    p.HEIGHT = 0;

    p.BITMASK_ERROR = parseInt("00000010",2);//0b00000010
    p.BITMASK_MAINTENANCE = parseInt("00001000",2);//0b00001000

    p.STATE_NORMAL = 1;
    p.STATE_ERROR = 3;
    p.STATE_MAINTENANCE = 9;
    p.STATE_OFFLINE = -1;

    p.isFreeToPlay = false;
    p.isOnline = false;
    p._currentState = false;
    p.thumbScaleX = 0;
    p.thumbScaleY = 0;
    p._dynamicX = 0;
    p._dynamicY = 0;
    p._idText = null;
    p.id = null;
    p._nameText = null;
    p._availabilityText = null;
    p._availabilityImage = null;
    p.selected = false;
    p._mouseContainer = null;

/*    p.listMachineBackground = null;
    p.gridBackground = null;*/
    p.background = null;
    p.overlay = null;

    p.defaultGameId = null;

    p.initMachine = function () {
        this._local_activate();// local ver

        this.thumbScaleX = 0.48;
        this.thumbScaleY = 0.61;

        //var background = this.gridBackground = alteastream.Assets.getImage(alteastream.Assets.images.machineBg);
        var background = this.background = alteastream.Assets.getImage(alteastream.Assets.images.machineBg);
        var overlay = this.overlay = alteastream.Assets.getImage(alteastream.Assets.images.machineOverlay);

        var thumb = this.thumb = alteastream.Assets.getImage(alteastream.Assets.images.thumb);
        thumb.mouseEnabled = false;
        thumb.visible = false;
        thumb.y = 43;

        var noThumb = this.noThumb = alteastream.Assets.getImage(alteastream.Assets.images.noThumb);
        noThumb.mouseEnabled = false;
        noThumb.visible = false;

        var frame = this.frame = alteastream.Assets.getImage(alteastream.Assets.images.window);
        frame.mouseEnabled = false;
        frame.visible = false;

        this.WIDTH = frame.image.width;
        this.HEIGHT = frame.image.height;

/*        var listBackgroundWidth = window.localStorage.getItem('isMobile') === "yes" ? 309 : 240;

        var listMachineBackground = this.listMachineBackground = new createjs.Shape();
        listMachineBackground.graphics.beginFill("#000000").drawRect(0, 0, listBackgroundWidth, 43);
        listMachineBackground.cache(0, 0, listBackgroundWidth, 43);
        listMachineBackground.visible = false;*/

/*        var listMachineBackground = this.listMachineBackground = alteastream.Assets.getImage(alteastream.Assets.images.listMachineBackground);
        listMachineBackground.visible = false;*/

        var _mouseContainer = this._mouseContainer = new createjs.Container();
        _mouseContainer.addChild(background,thumb,noThumb,overlay,/*listMachineBackground,*/ frame);
        this.addChild(_mouseContainer);

        var font = "15px HurmeGeometricSans3";
        var fontBigger = "14px HurmeGeometricSans3";
        var white = "#ffffff";
        var gray = "#828282";
        var yellow = "#ffde1e";
        var firstY = 24;
        var secondY = 230;
        var corrX = 5;
        
        this._createLabelText("machineIdLabel","ID:",font,gray,{x:15,y:firstY,textAlign:"left",textBaseline:"middle"});
        this._createText("_idText","",font,white,{x:42,y:firstY,textAlign:"left",textBaseline:"middle"});

        this._createLabelText("machineNameLabel","NAME: ",font,gray,{x:130,y:firstY,textAlign:"left",textBaseline:"middle"});
        this._createText("_nameText","",font,white,{x:187,y:firstY,textAlign:"left",textBaseline:"middle"});

        this._createLabelText("availabilityLabel","WAITING LOUNGE: ",fontBigger,white,{x:190+corrX,y:secondY,textAlign:"right",textBaseline:"middle"});
        this._createLabelText("queueNumber","0",fontBigger,yellow,{x:195+corrX,y:secondY,textAlign:"left",textBaseline:"middle"});

        this._availabilityImage = alteastream.Assets.getImage(alteastream.Assets.images.lampGreen);
        this._availabilityImage.x = 10;//120
        this._availabilityImage.y = 217;
        this.addChild(this._availabilityImage);

        var spinner = this.spinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = 120;
        spinner.y = 45;
        spinner.runPreload(true);
    };

    p.setMachineInfo = function(machine) {
        this._idText.text = machine.machineIndex;
        this._nameText.text = machine.machineName;
        this.setStatus(machine);
        this.id = machine.machineIndex;
        this.machineName = machine.machineName;
        this.gameCode = machine.gameCode;
        this.defaultGameId = machine.defaultGameId;
        this.machineInfo = machine;
    };

    p.resetMachineProperties = function() {
        this.isFreeToPlay = false;
        this.isOnline = false;
        this.id = null;
        this.machineName = null;
        this.machineInfo = null;
        this.gameCode = null;
        this.defaultGameId = null;
    };

    p.setGridMachine = function(setGrid) {
        this.frame.image = setGrid ? alteastream.Assets.getImageURI(alteastream.Assets.images.window) : alteastream.Assets.getImageURI(alteastream.Assets.images.windowList);
        this.background.image = setGrid ? alteastream.Assets.getImageURI(alteastream.Assets.images.machineBg) : alteastream.Assets.getImageURI(alteastream.Assets.images.listMachineBackground);
/*        this.WIDTH = setGrid ? this.frame.image.width :this.listMachineBackground.bitmapCache.width-6;
        this.HEIGHT = setGrid ? this.frame.image.height :this.listMachineBackground.bitmapCache.height;*/
/*        this.WIDTH = setGrid ? this.frame.image.width :this.listMachineBackground.image.width-6;
        this.HEIGHT = setGrid ? this.frame.image.height :this.listMachineBackground.image.height;*/
        this.WIDTH = this.background.image.width;
        this.HEIGHT = this.background.image.height;
        //this.listMachineBackground.visible = !setGrid;
        //this.gridBackground.visible = setGrid;
        this.overlay.visible = setGrid;
        this.thumb.visible = setGrid;
        if(this.thumb.hasThumbImage === false){
            this.noThumb.visible = setGrid;
        }
        this.spinner.visible = setGrid;
        this._availabilityImage.visible = setGrid;
        this.availabilityLabel.visible = setGrid;
        this.queueNumber.visible = setGrid;
        this.machineIdLabel.y = setGrid ? 24 : 22;
        this._idText.y = setGrid ? 24 : 22;
        this.machineNameLabel.y = setGrid ? 24 : 22;
        this._nameText.y = setGrid ? 24 : 22;
        if(window.localStorage.getItem('isMobile') === "yes"){
        //if(true){
            this.machineIdLabel.x = setGrid ? 14 : 15;
            this.machineIdLabel.y = setGrid ? 18 : 22;
            this._idText.x = setGrid ? 34 : 35;
            this._idText.y = this.machineIdLabel.y
            this.machineNameLabel.x = setGrid ? 190 : 215;
            this.machineNameLabel.y = this.machineIdLabel.y
            this._nameText.x = setGrid ? 235 : 265;
            this._nameText.y = this.machineIdLabel.y;
        }
    };

    p.disableMouseListeners = function(bool){
        if(bool){
            this._mouseContainer.removeAllEventListeners("rollover");
            this._mouseContainer.removeAllEventListeners("rollout");
            this._mouseContainer.removeAllEventListeners("click");
        }else{
            var that = this;
            this._mouseContainer.addEventListener("rollover",function(){
                that._onMouseOver(true);
            });
            this._mouseContainer.addEventListener("rollout",function(){
                that._onMouseOver(false);
            });
            this._mouseContainer.addEventListener("click",function(){
                that._onMouseClick();
            });
        }
    };

    p.showMachine = function(bool) {
        if(this.visible !== bool)
            this.visible = bool;
    };

    p.updateThumbnail = function(){
        var imgName = alteastream.AbstractScene.SHOP_MACHINE+"_"+this.machineName;
        alteastream.Assets.getThumbnail(imgName,this.thumb,this.WIDTH,this.HEIGHT,function(){
            this.noThumb.visible = !this.thumb.hasThumbImage;
            if(this.parent.showOnlyAvailable === true){
                if(this.isFreeToPlay === true && this.isOnline === true){
                    this.showMachine(true);
                }
            }else{
                this.showMachine(true);
            }
            //this.showMachine(true);
                if(this.spinner.active){
                    this.spinner.runPreload(false);
                    this.thumb.scaleX = this.thumbScaleX;
                    this.thumb.scaleY = this.thumbScaleY;
                    this.thumb.visible = true;
                }
                if(this.selected === true){
                    alteastream.StreamPreview.getInstance().changeFrameImage(this.thumb);
                }
        }.bind(this));
    };

    //symbols
    p.setStatus = function(machine){
        //machine.isFreeToPlay = false;//temp

        // filtering
        this.isFreeToPlay = machine.isFreeToPlay;
        this.isOnline = machine.isOnline;
        //machine.queNumber = machine.queueSize || 0;

        //this.queueNumber.text = machine.queNumber;
        this.queueNumber.text = machine.queueSize || 0;
        this.queueNumber.visible = false;

        var image;
        var text = "";
        var textColor = "#ffffff";

        var state = this.STATE_NORMAL;
        if((this.BITMASK_ERROR & machine.statusCode) > 0){
            state = this.STATE_ERROR;
            text = "ERROR";
            textColor = "#ff4646";
            image = "lampRed";
        }else if((this.BITMASK_MAINTENANCE & machine.statusCode) > 0 || machine.isInAutoplay === true){
            state = this.STATE_MAINTENANCE;
            text = "MAINTENANCE";
            textColor = "#ffa02b";
            image = "lampRed";
        }else if(machine.isOnline === false){
            state = this.STATE_OFFLINE;
            text = "OFFLINE";
            textColor = "#ff4646";
            image = "lampRed";
        }else{
            if(machine.isFreeToPlay === true){
                text = "PLAY NOW";
                image = "lampGreen";
            }else{
                text = "WAITING LOUNGE: ";
                image = "lampYellow";
                this.queueNumber.visible = true;
            }
        }
        /*if((this.BITMASK_MAINTENANCE & machine.statusCode) > 0 || machine.isInAutoplay === true){//machine.isInAutoplay temp fix Andrija
            state = this.STATE_MAINTENANCE;
        }
        if((this.BITMASK_ERROR & machine.statusCode) > 0){
            state = this.STATE_ERROR;
        }else {
            if(machine.isOnline === false){
                state = this.STATE_OFFLINE;
            }
        }*/

        /*if(state === this.STATE_NORMAL){
            if(machine.isFreeToPlay === true){
                text = "PLAY NOW";
                image = "lampGreen";
            }else{
                text = "WAITING LOUNGE: ";
                image = "lampYellow";
                this.queueNumber.visible = true;
            }
        }else{
            if(state === this.STATE_OFFLINE){
                text = this._getStateInfo(state);
                textColor = "#ff4646";
                image = "lampRed";
            }else{
                text = this._getStateInfo(state);
                textColor = text === "ERROR"?"#ff4646":"#ffa02b";
                image = "lampRed";
            }
        }*/

        this.setState(state);
        this._availabilityImage.image = alteastream.Assets.getImage(alteastream.Assets.images[image]).image;
        this.availabilityLabel.text = text;
        this.availabilityLabel.color = textColor;
        this.mouseEnabled = state === this.STATE_NORMAL;
    };

    p.setState = function(state){
        this._currentState = state;
    };

    p.getState = function(){
        return this._currentState;
    };
    
    p._getStateInfo = function(statusCode){
        var infoText ="";
        switch(statusCode){
            case this.STATE_ERROR:
                infoText = "ERROR";
                break;
            case this.STATE_MAINTENANCE:
                infoText = "MAINTENANCE";
                break;
            case this.STATE_OFFLINE:
                infoText = "OFFLINE";
                break;
        }
        return infoText;
    };

    p.getHeight = function () {
        return this.HEIGHT;
    };

    p.getWidth = function () {
        return this.WIDTH;
    };

    p.setSelected = function (bool) {
        this.selected = bool;
    };

    p.resetDisplay = function () {
        this.selectMachine(false);
    };

    p.markSelected = function(bool){
        this.frame.visible = bool;
    };

    p.doCache = function(bool){
       /* if(bool === true)
            this.cache(0,0,this.WIDTH,this.HEIGHT);
        else
            this.uncache();*/
    };

    p._onMouseOver = function (bool) {
/*        if(this.selected)
            return;*/
        this.markSelected(bool);
    };

    p.selectMachine = function(bool) {
        //this.markSelected(bool);
        this.setSelected(bool);
    };

    p._onMouseClick = function () {
        alteastream.LocationMachines.getInstance().onMachineClick(this);
    };

    p._createLabelText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign,textBaseline:props.textBaseline});
        this.addChild(textInstance);
    };
    
    p._createText = function(instance,text,font,color,props){

        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign,textBaseline:props.textBaseline});
        this.addChild(textInstance);
    };

    p._local_activate = function() {
        var _this = this;
        this._onMouseClick = function () {
            var freetoplayTest = _this.machineInfo.isFreeToPlay;
            var indxToSend = _this.machineInfo.machineName;
            console.log("_onMouseClick this.name "+_this.machineInfo.machineName);
            alteastream.LocationMachines.getInstance().setComponentWhenMachineisFreeToPlay(freetoplayTest);//QueuePanel.manageQueueReadyState
            var streamPreview = alteastream.StreamPreview.getInstance();

            //if(this.currentStream !== _this.machineInfo.machineIndex){
            if(streamPreview.currentStream !== _this.id){
                alteastream.MachinesComponent.getInstance().clearActiveMachines();
                //alteastream.SocketCommunicator.getInstance().lobbySwitchSubscriptions(this.currentStream,machineInfo.machineIndex);
                console.log("lobbySwitchSubscriptions currentStream::"+streamPreview.currentStream);
                console.log("lobbySwitchSubscriptions machineInfo.machineIndex::"+_this.machineInfo.machineIndex);
                // novi wait lounge comment this line
                streamPreview.switchStream(_this.machineInfo);
                alteastream.QueuePanel.getInstance().setCurrentMachine(_this.machineInfo);
                streamPreview.changeFrameImage(_this.thumb);
                _this.selectMachine(true);
                alteastream.Lobby.getInstance().checkIfHiddenMenuIsShown();
                // filtering
                //alteastream.MachinesComponent.getInstance().setCurrentlySelectedMachineId(_this.machineInfo.machineIndex);// todo proveriti da li moze currentStream umesto machineIndex
                if(alteastream.LocationMachines.getInstance().gridLayoutIsActive){
                    alteastream.LocationMachines.getInstance().setListLayout(true);
                    console.log("machine_index onClick to queue::: "+indxToSend);
                    if(freetoplayTest===true){
                        alteastream.QueuePanel.getInstance().enterQueue();//new
                        alteastream.QueuePanel.getInstance().manageQueueReadyState(10);
                    }else{
                        console.log("onMouseClick enterQueue");
                        alteastream.QueuePanel.getInstance().enterQueue();//new
                    }
                }
                else{
                    alteastream.QueuePanel.getInstance().exitQueue();
                    alteastream.QueuePanel.getInstance().leaveQueue(
                        function () {alteastream.QueuePanel.getInstance().enterQueue();
                            alteastream.QueuePanel.getInstance().manageQueueReadyState(10);
                        }
                    );
                }
                /*console.log("MachineName "+_this.machineName);
                console.log("machineInfo.machineName "+_this.machineInfo.machineName);
                console.log("IS FREE TO PLAY "+_this.machineInfo.isFreeToPlay);*/
                //console.log("this.machineInfo.isFreeToPlay::::::::::::::: "+_this.machineInfo.isFreeToPlay); //ovde u lokalu nije isti ID
                //alteastream.LocationMachines.getInstance().setComponentWhenMachineisFreeToPlay(freetoplayTest);//QueuePanel.manageQueueReadyState
                //alteastream.QueuePanel.getInstance().enterMachineQueue();

                alteastream.MachinesComponent.getInstance().clearActiveMachines();
            }
        };
    };

    alteastream.Machine = createjs.promote(Machine,"Container");
})();