
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var BetEntranceCounter = function(){
        this.AbstractCounter_constructor();
        this.initBetEntranceCounter();
    };

    var p = createjs.extend(BetEntranceCounter,alteastream.AbstractCounter);

    var _this;

    p.initBetEntranceCounter = function () {
        console.log("BetEntranceCounter initialized:::");
        _this = this;
        this.strokeTickness = 4;
        this.counterRadius = 22;
        this.colors = ["#15d310", "#ff0000"];
    };

    BetEntranceCounter.getInstance = function() {
        return _this;
    };

    p.setLayout = function(){
        var counterCircleBackground = this.counterCircleBackground = new createjs.Shape();
        counterCircleBackground.graphics.clear().setStrokeStyle(4).beginStroke("#555153").arc(0, 0, 22, 0, (Math.PI/180) * 360).endStroke();
        counterCircleBackground.mouseEnabled = false;
        this.addChild(counterCircleBackground);

        var counterCircle = this.counterCircle = new createjs.Shape();
        counterCircle.rotation = -90;
        counterCircle.mouseEnabled = false;
        this.addChild(counterCircle);

        this._createText("confirmEnterCounter","0","20px HurmeGeometricSans3", "#ffffff",{x:0,y:2,textAlign:"center",textBaseline:"middle"});
    };

    p.quitCounter = function() {
        this.show(false);
        this.clearCounterInterval();
    };

    p.adjustMobile = function(){
        this.counterRadius = 17;
        this.strokeTickness = 3;
        var counterCircleBackground = this.counterCircleBackground;
        counterCircleBackground.graphics.clear();
        counterCircleBackground.graphics.setStrokeStyle(3).beginStroke("#555153").arc(0, 0, this.counterRadius, 0, (Math.PI/180) * 360).endStroke();

        this.confirmEnterCounter.font = "14px HurmeGeometricSans3";
        this.confirmEnterCounter.y = 0;
    }


    alteastream.BetEntranceCounter = createjs.promote(BetEntranceCounter,"AbstractCounter");
})();