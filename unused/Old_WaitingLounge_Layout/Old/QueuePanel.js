(function(){
    "use strict";

    var QueuePanel = function(){
        this.Container_constructor();
        this.initQueuePanel();
    };

    var p = createjs.extend(QueuePanel,createjs.Container);
    p._machineButton = null;
    p.currentMachineInfo = null;
    p._availabilityImage = null;
    p._availabilityText = null;
    p._firstInQueue = false;
    p._queueLeaveCallback = null;
    p.yourPositionInQueue = 0;
    p.queueIsFree = true;
    p.dirtyMachines = [];
    p.previousMachineActive = false;

    p.queueExists = false;

    var _instance = null;

    p.initQueuePanel = function () {
        console.log("QueuePanel initialized:::");
        _instance = this;

        var fontSmall = "16px HurmeGeometricSans3";

        var bannerContainer = this.bannerContainer = new createjs.Container();
/*        var bannerBgShape = this._bannerBgShape = new createjs.Shape();
        bannerBgShape.graphics.beginFill("rgba(52,190,194,0.93)").drawRoundRect(0, 0, 1436, 50, 5);//15d510
        bannerBgShape.cache(0, 0, 1436, 50);
        bannerBgShape.size = {width:1436, height:50, roundness:5};
        bannerBgShape.x = 264;
        bannerBgShape.y = -45;*/

        var banner = this._availabilityBanner = new createjs.Shape();
        banner.graphics.beginFill("#ffcc00").drawRoundRect(0, 0, 1423, 36, 5);//15d510
        banner.cache(0, 0, 1423, 36);
        banner.size = {width:1423, height:36, roundness:5};
        banner.x = 270;
        banner.y = -38;
        var icon = this._availabilityImage = alteastream.Assets.getImage(alteastream.Assets.images.lampGreenLight);
        icon.x = 280;
        icon.y = -30;

        var availabilityText = this._availabilityText = new createjs.Text("","16px HurmeGeometricSans3","#000000").set({textAlign:"left", textBaseline:"middle"});
        availabilityText.x = 310;
        availabilityText.y = -18;

        var machinesQueueNumber = this.machinesQueueNumber = new createjs.Text("0","bold 20px HurmeGeometricSans3","#000000").set({textAlign:"left", textBaseline:"middle"});
        machinesQueueNumber.x = 462;
        machinesQueueNumber.y = -18;

        bannerContainer.addChild(/*bannerBgShape,*/banner, icon, availabilityText, machinesQueueNumber);
        this.addChild(bannerContainer);
        bannerContainer.visible = false;
        bannerContainer.mouseEnabled = false;
        bannerContainer.mouseChildren = false;
        bannerContainer.y-=11;

        this._createText("positionInQueueText","YOUR POSITION:","16px HurmeGeometricSans3", "#000000",{x:1500,y:-29,textAlign:"left", textBaseline:"middle",visible:false});
        this._createText("positionInQueueNumber","N/A","bold 20px HurmeGeometricSans3", "#000000",{x:1640,y:-29,textAlign:"left", textBaseline:"middle",visible:false});

        var multiPlayerControls = this.multiPlayerControls = new alteastream.MultiplayerControls();
        this.addChild(multiPlayerControls);
        this.multiPlayerControls.show(false);

        var entranceCounter = this.entranceCounter = new alteastream.EntranceCounter();
        this.addChild(entranceCounter);
        entranceCounter.mouseEnabled = false;
        entranceCounter.mouseChildren = false;
        this.entranceCounter.show(false);

        var btnStart = this.btnStart = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnStart),3);
        btnStart.x = 137 - (btnStart._singleImgWidth * 0.5);
        btnStart.y = 753 - (btnStart._height * 0.5);
        this.addChild(btnStart);
        btnStart.setClickHandler(this.startGame);
        btnStart.visible = false;

        var spinner = this.spinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = 239;
        spinner.y = 720;
        spinner.customScaleGfx(0.5);
        spinner.customGfxPosition(-90, 45);
        spinner.customFont(fontSmall, "right");
        spinner.mouseEnabled = false;
        spinner.setLabel("MAKING ROOM FOR YOU");
        spinner.runPreload(false);
    };

    QueuePanel.getInstance = function(){
        return _instance;
    };

    p.enterQueue = function(){
        console.log("machine_id on enterQueue "+alteastream.AbstractScene.GAME_ID);
        this.enableStart(false);



        alteastream.Requester.getInstance().enterMachine(function(response){
            if(response.tp === 3){
                // filtering
                _instance.queueExists = true;
                if(response.vl>0){//na soket mozda nece moci ALREADY in queue?
                    console.log(" ALREADY in queue::::::::::::::");
                    _instance._firstInQueue = false;
                    //_instance.entranceCounter.show(false);
                    _instance.spinner.runPreload(true);
                }else{
                    console.log(" ADDED to queue::::::::::::::");
                    _instance._firstInQueue = true;
                    _instance.entranceCounter.show(false);
                    _instance.btnStart.visible = false;

                    if(_instance.spinner.active){
                        _instance.spinner.runPreload(false);
                    }
                    _instance.visible = true;
                    _instance.positionInQueueText.visible = true;
                    _instance.positionInQueueNumber.visible = true;
                    _instance.bannerContainer.visible = true;
                    _instance.multiPlayerControls.show(true);
                    _instance.parent.onQueueEntered();
                }
            }else{
                alteastream.Lobby.getInstance().throwAlert(alteastream.Alert.ERROR, "adding to queue failed");
            }
            // filtering
            //alteastream.Lobby.getInstance().currentLocationMachines.setListLayout(true);
        });

        //this.manageQueueReadyState();// local ver
    };

    //new socket
    /*p.enterQueue = function(){
        console.log("machine_id on enterQueue "+alteastream.AbstractScene.GAME_ID);
        this.enableStart(false);
        alteastream.SocketCommunicatorLobby.getInstance().enterQueue();
    };*/

    //new socket
/*    p.onQueueEnter = function(){
        console.log(" ADDED to queue::::::::::::::");
        _instance.queueExists = true;
        _instance._firstInQueue = true;

        //_instance.entranceCounter.show(false);
        //_instance.btnStart.visible = false;
        _instance.enableStart(false);

        if(_instance.spinner.active){
            _instance.spinner.runPreload(false);
        }
        _instance.visible = true;
        _instance.positionInQueueText.visible = true;
        _instance.positionInQueueNumber.visible = true;
        _instance.bannerContainer.visible = true;
        _instance.multiPlayerControls.show(true);
        _instance.parent.onQueueEntered();

        //_instance.updateAvailabilityComponents(_instance.queueIsFree);
    };*/

    // new socket
    p.onQueueDeferred = function(code){
        _instance.resetDisplay();
        //new socket
        _instance.updateAvailabilityComponents(_instance.queueIsFree);
       // if(code === 7){
           // _instance.machineCleared = false;
        //}
        _instance.dirtyMachines.push(_instance.parent.streamPreview.currentStream);
    };

    p.resetDisplay = function(){
        _instance.queueIsFree = false;
        _instance._firstInQueue = false;
        _instance.visible = true;
        _instance.bannerContainer.visible = true;
        _instance.entranceCounter.show(false);
        _instance.btnStart.visible = false;
        _instance.spinner.runPreload(true);

        _instance.parent._machinesComponent.visible = false;
        _instance.parent._locationLabel.visible = false;
    };

    p.machineIsDirty = function(machineIndex){
        if(_instance.dirtyMachines.indexOf(machineIndex)>-1){
            _instance.dirtyMachines.splice(_instance.dirtyMachines.indexOf(machineIndex),1);
            return _instance.parent.streamPreview.currentStream === machineIndex;
        }
    };

    //new socket
    /*p.onQueueError = function(code){
        _instance._firstInQueue = false;
        alteastream.Lobby.getInstance().throwAlert(alteastream.Alert.ERROR, "queuing failed: "+code,function(){
            _instance.parent.backToLobbyButtonHandler();
        });
    };*/

    //new socket
    p.exitQueue = function(){ //+leaveQueue mora za njim, ne na expire
        this.entranceCounter.clearCounterInterval();
        this.multiPlayerControls.show(false);
        this.multiPlayerControls.enable(false);

        this.resetEntrance();
        this.visible = false;
        this.positionInQueueText.visible = false;
        this.positionInQueueNumber.visible = false;
        this.bannerContainer.visible = false;
    };

    p.leaveQueue = function(callback){
        alteastream.Requester.getInstance().leaveMachine(function(response){
            if(response.tp === 4){
                console.log(" REMOVED from queue::::::::::::::");
                // filtering (two lines)
                //alteastream.Lobby.getInstance().currentLocationMachines.setListLayout(false);
                _instance.queueExists = false;
                if(callback){
                    callback();
                }
            }else{
                alteastream.Lobby.getInstance().throwAlert(alteastream.Alert.ERROR, "removing from queue failed");
            }
        });
    };

    //new socket
    /*p.leaveQueue = function(callback){
        if(this.yourPositionInQueue > 0){
            this.setQueueLeaveCallback(callback);
            alteastream.SocketCommunicatorLobby.getInstance().leaveQueue();
        }
    };*/

    //new socket
    p.onQueueLeave = function(){
        this._firstInQueue = false;
        if(this._queueLeaveCallback){
            this._queueLeaveCallback();
            this.setQueueLeaveCallback(null);
        }
    };
   //new socket
    p.setQueueLeaveCallback = function(callback){
        this._queueLeaveCallback = callback;
    };

    p.manageQueueReadyState = function(waitTime){
        this.updateAvailabilityComponents(true);
        this.enableStart(true);
        this.entranceCounter.update(waitTime);
    };

    p.startGame = function(){
        //_instance.removeAllChildren();// invalid token fix old
        _instance.parent.onStartGame();
    };

    p.enableStart = function(bool){
        if(this._firstInQueue){
            this.btnStart.setDisabled(bool);
        } else{
            this.btnStart.setDisabled(!bool);
            this.entranceCounter.show(false);
            this.btnStart.visible = false;
        }
    };

    //new socket
    /*p.enableStart = function(bool){
        this.btnStart.setDisabled(!bool);
        this.btnStart.visible = bool;
        this.entranceCounter.show(bool);
    };*/

    p.updateQueueInfo = function(response){
        this.machinesQueueNumber.text = response;

        if(!this._firstInQueue){
            this.entranceCounter.show(false);
            this.btnStart.visible = false;
            if(this.spinner.active){
                this.spinner.runPreload(false);
            }
            if(!response > 0 || response === "you_left_the_game"){
                console.log("updateQueueInfo:: you_left_the_game");
                //this.buttonEnter.visible = true;
                //this.buttonEnter.setDisabled(false);
                // BACK TO GAME VERSION
                //this.btnBackToGame.visible = false;
            }
        }
    };

    //new socket
    /*p.updateQueueInfo = function(msgVl){
        //if(msgVl.size){
        var parsedMsg = JSON.parse(msgVl);
        console.log("parced msg");
        console.log(parsedMsg);
        if(typeof parsedMsg == "object"){
            this.yourPositionInQueue = parsedMsg.you;
            this.machinesQueueNumber.text = parsedMsg.size;
            this.positionInQueueNumber.text = this.yourPositionInQueue;
        }else{
            this.yourPositionInQueue = 0;
            this.machinesQueueNumber.text = msgVl;
            this.positionInQueueNumber.text = this.yourPositionInQueue;
        }
    };*/

    p._updateAvailabilityComponents = function(machineInfo){// todo mozda da ne radi ovo ako nema availability promena, queue pozicija se nece menjati ovde
        var image = machineInfo.isFreeToPlay ? "lampGreenLight" : "lampYellowDark";
        var bannerColor = machineInfo.isFreeToPlay ? "#15d510" : "#ffcc00";
        var textColor = machineInfo.isFreeToPlay ? "#ffffff" : "#000000";
        var text = machineInfo.isFreeToPlay ? "FREE TO PLAY" : "WAITING LOUNGE:";

/*        var bannerBg = this._bannerBgShape;
        bannerBg.uncache();
        bannerBg.graphics.clear();
        bannerBg.graphics.beginFill("#000000").drawRoundRect(0, 0, bannerBg.size.width, bannerBg.size.height, bannerBg.size.roundness);
        bannerBg.cache(0, 0, bannerBg.size.width, bannerBg.size.height);*/

        var banner = this._availabilityBanner;
        banner.uncache();
        banner.graphics.clear();
        banner.graphics.beginFill(bannerColor).drawRoundRect(0, 0, banner.size.width, banner.size.height, banner.size.roundness);
        banner.cache(0, 0, banner.size.width, banner.size.height);//_bannerBgShape

        this._availabilityImage.image = alteastream.Assets.getImage(alteastream.Assets.images[image]).image;
        this._availabilityText.color = textColor;
        this._availabilityText.text = text;

        this.positionInQueueText.visible = this.machinesQueueNumber.visible =
            this.positionInQueueNumber.visible = !machineInfo.isFreeToPlay;
    };

    //new socket
    p.updateAvailabilityComponents = function(isFreeToPlay){
        console.log("updateAvailabilityComponents "+isFreeToPlay);
        var image = isFreeToPlay === true ? "lampGreenLight" : "lampYellowDark";
        var bannerColor = isFreeToPlay === true ? "#15d510" : "#ffcc00";
        var textColor = isFreeToPlay === true  ? "#ffffff" : "#000000";
        var text = isFreeToPlay === true  ? "FREE TO PLAY" : "WAITING LOUNGE:";

/*        var bannerBg = this._bannerBgShape;
        bannerBg.uncache();
        bannerBg.graphics.clear();
        bannerBg.graphics.beginFill("#000000").drawRoundRect(0, 0, bannerBg.size.width, bannerBg.size.height, bannerBg.size.roundness);
        bannerBg.cache(0, 0, bannerBg.size.width, bannerBg.size.height);*/

        var banner = this._availabilityBanner;
        banner.uncache();
        banner.graphics.clear();
        banner.graphics.beginFill(bannerColor).drawRoundRect(0, 0, banner.size.width, banner.size.height, banner.size.roundness);
        banner.cache(0, 0, banner.size.width, banner.size.height);//_bannerBgShape

        this._availabilityImage.image = alteastream.Assets.getImage(alteastream.Assets.images[image]).image;
        this._availabilityText.color = textColor;
        this._availabilityText.text = text;

        this.positionInQueueText.visible = this.machinesQueueNumber.visible =
            this.positionInQueueNumber.visible = !isFreeToPlay;
    };

    p.resetEntrance = function(){
        //console.log("QUEUE resetEntrance:::::::::: "+this._firstInQueue);
        //if(this._firstInQueue)
        this._firstInQueue = false;
        //this.entranceCounter.show(false);
        //this.btnStart.visible = false;
        this.enableStart(false);
        this.spinner.runPreload(false);
    };

    p.setCurrentMachine = function(machineInfo) {
        alteastream.AbstractScene.GAME_ID = machineInfo.machineIndex;
        alteastream.Lobby.getInstance().currentMap.clickEnabled(false);
        if(this.spinner.active === true){
            this.spinner.runPreload(false);
        }
    };

    p._createText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline, visible:props.visible});
        this.addChild(textInstance);
    };

    p.adjustMobile = function(){
        var font = "bold 14px HurmeGeometricSans3";

        this._availabilityBanner.size = {width:617, height:50, roundness:3};
        this._availabilityBanner.x = 317;
        this._availabilityBanner.y = 21;

        this._availabilityImage.x = 323;
        this._availabilityImage.y = 36;

        this._availabilityText.font = font;
        this._availabilityText.x = 350;
        this._availabilityText.y = this._availabilityImage.y + 12;

        this.positionInQueueText.font = font;
        this.positionInQueueText.x = 720;
        this.positionInQueueText.y = 37;

        this.positionInQueueNumber.font = font;
        this.positionInQueueNumber.x = 838;
        this.positionInQueueNumber.y = this.positionInQueueText.y;

        this.machinesQueueNumber.font = font;
        this.machinesQueueNumber.x = this._availabilityText.x + 130;
        this.machinesQueueNumber.y = this._availabilityText.y;

        this.btnStart.x = 158 - (this.btnStart._singleImgWidth * 0.5);
        this.btnStart.y = 410 - (this.btnStart._height * 0.5);

        this.spinner.x = 230;
        this.spinner.y = 378;
        this.spinner.customFont("12px HurmeGeometricSans3", "right");
        this.spinner.customGfxPosition(-70, 45);

        this.entranceCounter.adjustMobile();
        this.multiPlayerControls.adjustMobile();
    };

    alteastream.QueuePanel = createjs.promote(QueuePanel,"Container");
})();