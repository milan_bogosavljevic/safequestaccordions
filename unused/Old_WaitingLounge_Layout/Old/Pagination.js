
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var Pagination = function(options){
        this.Container_constructor();
        this.initPagination(options);
    };

    var p = createjs.extend(Pagination,createjs.Container);
    var _this;

    p._dotsContainer = null;
    p._currentDot = null;
    p._componentWidth = 0;
    p._numOfPages = 0;
    p._dotWidth = 0;
    p._dotSpacing = 0;
    p._upButton = null;
    p._downButton = null;
    p.hasButtons = false;

    p.initPagination = function (options) {
        console.log("Pagination initialized:::");
        _this = this;
        this._numOfPages = options.numberOfPages;
        this._dotSpacing = options.dotsSpacing;
        this._dotWidth = options.dotsWidth;
        this._dotsContainer = new createjs.Container();
        this.addChild(this._dotsContainer);
        this.addDots(options);
        this._dotsContainer.on("click", function (e) {
            options.scroller.dotClicked(e.target.parent.name);
        });
    };

    p.setUpButton = function(button, x, y, scroller) {
        this._upButton = button;
        this._upButton.x = x;
        this._upButton.y = y;
        this.addChild(this._upButton);
        this._upButton.setClickHandler(function () {
            scroller.moveUp();
        });
        this.hasButtons = true;
    };

    p.setDownButton = function(button, x, y, scroller) {
        this._downButton = button;
        this._downButton.x = x;
        this._downButton.y = y;
        this.addChild(this._downButton);
        this._downButton.setClickHandler(function () {
            scroller.moveDown();
        });
    };

    p.disableButtons = function(disable) {
        this._upButton.setDisabled(disable);
        this._downButton.setDisabled(disable);
    };

    p._setButtonsStateOnStart = function(numOfPages) {
        if(numOfPages > 1){
            this._upButton.setDisabled(true);
            this._downButton.setDisabled(false);
        }
    };

    p.addDots = function(options) {
        var y = 0;
        var counter = 0;
        var spacing = this._dotSpacing;
        var R = this._dotWidth;
        var r = R/4;
        var maxDotPerLine = Math.floor(1360 / spacing);

        for(var i = 0; i < this._numOfPages; i++){
            var cont = new createjs.Container();
            var dot;
            if(options.customDot){
                dot = alteastream.Assets.getImage(alteastream.Assets.images[options.customDot]);
                dot.regX = dot.image.width/2;
                dot.regY = dot.image.height/2;
            }else{
                dot = new createjs.Shape();
                dot.graphics.beginFill("#6e6e6e").drawCircle(0, 0, r);
                dot.cache(-r,-r,R,R);
            }

            cont.x = (i * R) + (spacing * i);
            cont.y = y;
            cont.name = i;

            cont.addChild(dot);

            if(options.dotFont){
                var num = new createjs.Text(i+1,"16px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle"});
                num.y = 2;
                cont.addChild(num);
            }

            this._dotsContainer.addChild(cont);
            counter++;
            if(counter === maxDotPerLine){
                counter = 0;
                y = 22;
            }
        }

        var cd = this._currentDot = new createjs.Container();
        var currentDot;
        if(options.customCurrentDot){
            currentDot = alteastream.Assets.getImage(alteastream.Assets.images[options.customCurrentDot]);
            currentDot.regX = currentDot.image.width/2;
            currentDot.regY = currentDot.image.height/2;
        }else{
            currentDot = new createjs.Shape();
            currentDot.graphics.beginFill("#ffffff").drawCircle(0, 0, r);
            currentDot.cache(-r,-r,R,R);
            currentDot.x = 0;
            currentDot.y = 0;
            currentDot.mouseEnabled = false;
        }

        cd.addChild(currentDot);

        if(options.currentDotFont){
            var txt = new createjs.Text(1,"16px HurmeGeometricSans3","#000000").set({textAlign:"center", textBaseline:"middle"});
            txt.y = 2;
            cd.addChild(txt);
        }

        this._dotsContainer.addChild(cd);

        this._setComponentWidth();
    };

    p.adjustDotsTextFields = function(x, y, texts) {
        for(var i = 0; i < this._dotsContainer.numChildren-1; i++){
            var dot = this._dotsContainer.getChildAt(i);
            var text = dot.getChildAt(1);
            text.x = x;
            text.y = y;
            if(texts){
                text.text = texts[i];
            }
        }
    };

    p.removeTextFromCurrentDot = function() {
        var currentDotContainer = this._dotsContainer.getChildAt(this._dotsContainer.numChildren-1);
        currentDotContainer.removeChildAt(1);
    }

    p._setComponentWidth = function() {
        this._componentWidth = (this._dotWidth + this._dotSpacing) * (this._numOfPages - 1) + this._dotWidth;
        if(this._componentWidth < this._dotWidth){
            this._componentWidth = this._dotWidth;
        }
    };

    p.getPaginationWidth = function(){
        return this._componentWidth;
    };

    p.moveCurrentDot = function(index) {
        var current = this._dotsContainer.getChildAt(index);
        this._currentDot.x = current.x;
        this._currentDot.y = current.y;
        if(this._currentDot.getChildAt(1)){
            this._currentDot.getChildAt(1).text = current.getChildAt(1).text;
        }
    };

    p.doMovingDotScaleAnimation = function(selected) {
        for(var i = 0; i < this._dotsContainer.numChildren; i++){
            var dot = this._dotsContainer.getChildAt(i);
            if(dot.scale > 1){
                createjs.Tween.get(dot).to({scale:1},500,createjs.Ease.quadOut);
            }
        }
        this._currentDot.scale = 1;
        var selectedDot = this._dotsContainer.getChildAt(selected);
        createjs.Tween.get(selectedDot).to({scale:1.2},500,createjs.Ease.quadOut);
        createjs.Tween.get(this._currentDot).to({scale:1.2},500,createjs.Ease.quadOut);
    };

    p.setComponentForListView = function(setForList){
        var w = this._dotWidth;
        var cdShape = this._currentDot.getChildAt(0);
        cdShape.uncache();
        cdShape.graphics.clear();
        var r = w/4;
        if(setForList){
            cdShape.graphics.beginFill("#ffffff").drawCircle(0, 0, r);
            cdShape.cache(-r,-r,w,w);
        }else{
            cdShape.graphics.beginFill("#ffffff").drawRect(-20, -20, 40, 40);
            cdShape.cache(-20, -20, 40, 40);
        }
        this._currentDot.getChildAt(1).visible = !setForList;
        for(var i = 0; i < this._dotsContainer.numChildren-1; i++){
            this._dotsContainer.getChildAt(i).getChildAt(0).alpha = setForList ? 1 : 0.01;
            this._dotsContainer.getChildAt(i).getChildAt(1).visible = !setForList;
        }
    };

    p.resetComponent = function(numOfPages) {
        this._numOfPages = numOfPages;
        for(var i = 0; i < this._dotsContainer.numChildren-1; i++){
            this._dotsContainer.getChildAt(i).visible = i < numOfPages;
        }
        this._setComponentWidth();
    };

    p.showPagination = function(show) {
        this.visible = show;
    };

    p.hideUI = function(bool){
        this._upButton.visible = !bool;
        this._downButton.visible = !bool;
    };

    p.enablePaginationClick = function(enable) {
        this.mouseEnabled = enable;
    };

    alteastream.Pagination = createjs.promote(Pagination,"Container");
})();


