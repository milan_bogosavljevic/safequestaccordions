
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var EntranceCounter = function(){
        this.AbstractCounter_constructor();
        this.initEntranceCounter();
    };

    var p = createjs.extend(EntranceCounter,alteastream.AbstractCounter);

    var _this;

    p.initEntranceCounter = function () {
        console.log("EntranceCounter initialized:::");
        _this = this;
        this.strokeTickness = 12;
        this.counterRadius = 100;
        this.colors = ["#15d510", "#ff0000"];

        this._local_activate();// local ver
    };

    EntranceCounter.getInstance = function() {
        return _this;
    };

    p.setLayout = function(){
        var counterShape = this.counterShape = new createjs.Shape();
        counterShape.graphics.beginFill("#000000").drawRoundRect(0, 0, 233, 796, 5);
        counterShape.cache(0, 0, 233, 796);
        counterShape.size = {width:233, height:796, roundness:5};
        counterShape.x = 21;
        counterShape.y = 3;
        counterShape.mouseEnabled = false;
        this.addChild(counterShape);
        counterShape.alpha = 0.6;
        
        var counterCircleBackground = this.counterCircleBackground = new createjs.Shape();
        counterCircleBackground.graphics.clear().setStrokeStyle(12).beginStroke("#555153").arc(0, 0, 100, 0, (Math.PI/180) * 360).endStroke();
        counterCircleBackground.x = 140;
        counterCircleBackground.y = 140;
        counterCircleBackground.mouseEnabled = false;
        this.addChild(counterCircleBackground);

        var counterCircle = this.counterCircle = new createjs.Shape();
        counterCircle.rotation = -90;
        counterCircle.x = counterCircleBackground.x;
        counterCircle.y = counterCircleBackground.y;
        counterCircle.mouseEnabled = false;
        this.addChild(counterCircle);

        var white = "#ffffff";
        this._createText("confirmEnterText","CONFIRM ENTER IN:","20px HurmeGeometricSans3", white,{x:140,y:-29,textAlign:"center", textBaseline:"middle"});//1172
        this._createText("confirmEnterCounter","0","78px HurmeGeometricSans3", "#ffffff",{x:140,y:115,textAlign:"center", textBaseline:"middle"});//1332
        this._createText("secondsText","SECONDS","24px HurmeGeometricSans3", "#ffffff",{x:140,y:175,textAlign:"center", textBaseline:"middle"});//1332
        this._createText("infoText","Ready for you!\nConfirm play!\nGood Luck!","bold 24px HurmeGeometricSans3", "#ffffff",{x:140,y:432,textAlign:"center", textBaseline:"middle"});//1332
        this.infoText.lineHeight = 50;
    };

    p.adjustMobile = function(){
        var counterShape = this.counterShape;
        counterShape.uncache();
        counterShape.graphics.clear();
        counterShape.graphics.beginFill("#000000").drawRoundRect(0, 0, 303, 385, 5);
        counterShape.cache(0, 0, 303, 385);
        counterShape.size = {width:303, height:385, roundness:5};
        counterShape.x = 6;//44
        counterShape.y = 80;//5
        counterShape.mouseEnabled = false;

        this.confirmEnterText.x = 158;
        this.confirmEnterText.y = 105;

        this.confirmEnterCounter.x = this.confirmEnterText.x;
        this.confirmEnterCounter.y = 215;

        this.secondsText.x = this.confirmEnterCounter.x;
        this.secondsText.y = 282;

        this.counterCircleBackground.x = 158;
        this.counterCircleBackground.y = 245;

        this.counterCircle.x = this.counterCircleBackground.x;
        this.counterCircle.y = this.counterCircleBackground.y;

        this.removeChild(this.infoText);
    };

    p._local_activate = function() {
        this._updateCounter = function(count){
            var queuePanel = alteastream.QueuePanel.getInstance();
            if(queuePanel._firstInQueue){
                this.show(true);
                queuePanel.btnStart.visible = true;
                if(count>-1)
                    this.confirmEnterCounter.text = count;
                queuePanel.btnStart.setDisabled(false);
            }else{
                queuePanel.btnStart.setDisabled(true);
                queuePanel.btnStart.visible = false;
                this.show(false);
            }
        }.bind(this);
    }
    

    alteastream.EntranceCounter = createjs.promote(EntranceCounter,"AbstractCounter");
})();