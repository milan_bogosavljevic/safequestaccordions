
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreviewBkp = function(width,height){
        this.Container_constructor();
        this.initStreamPreviewBkp(width,height);
    };

    var p = createjs.extend(StreamPreviewBkp,createjs.Container);

    p.startX = 0;
    p.sWidth = 0;
    p.sHeight = 0;
    p.src = "";
    p.id = "";

    var _instance = null;

    p.initStreamPreviewBkp = function (width,height) {
        _instance = this;
        this.sWidth = width;
        this.sHeight = height;

        /*var iFrame = this.iFrame = document.createElement('iframe');
        iFrame.id = 'previewHolder';
        iFrame.src = "";
        iFrame.style.top = 0;
        iFrame.style.left = 0;
        iFrame.style.width = this.sWidth + "px";
        iFrame.style.height = this.sHeight + "px";
        iFrame.style.position = "absolute";
        iFrame.style.backgroundColor = 'none';//#43535e
        //iFrame.style.backgroundColor = '#000000';//#43535e
        //iFrame.style.backgroundImage = "url(../lobby/assets/images/nosignal.png)";
        //iFrame.style.border = "1px #43535e solid";
        iFrame.style.border = "none";
        iFrame.style.pointerEvents = "none";
        iFrame.allow = "autoplay";
        document.getElementById("wrapper").appendChild(iFrame);

        var videoStream = this.videoStream = new createjs.DOMElement(iFrame);
        this.addChild(videoStream);
        */ //make for Iframe as separate, and call this by CanvasStream

        var protocol = 'ws://';
        var port = ':9999';
        this.source = "139.59.147.230";//or some default src index 1 for ex.
        this.videoElement = document.createElement('canvas');
        this.videoElement.id = "canvasPreview";

        document.getElementById("wrapper").appendChild(this.videoElement);
        var style = this.videoElement.style;
        style.position = "absolute";
        style.border = 0;
        style.left = 0;
        style.top = 0;
        style.width = this.sWidth + "px";
        style.height = this.sHeight+"px";
        style.outline = "none";
        style.pointerEvents = "none";

        var client = new WebSocket(protocol+this.source+port);
        var canvas = document.getElementById("canvasPreview");
        var player = new jsmpeg(client, {
            canvas: canvas
        });

        this.videoStream = new createjs.DOMElement(this.videoElement);
        this.addChild(this.videoStream);this.videoStream.mouseEnabled = false;

        this.videoStream.visible = false;

        //this.setDynamicPosition(0.01,0.55);//0.01,0.55 side component
        this.setDynamicPosition(0.240,0.115);//inside machine 0.24,0.11

        //var fontSmall = "24px Hurme";
        //var white = "#fff";
        //this._createDynamicText("machineInfoLabel","in queue for machine ID:",fontSmall,white,{x:150,y:210,textAlign:"center",textBaseline:"alphabetic"});
        //this._createDynamicText("machineInfoText","--",fontSmall,white,{x:295,y:210,textAlign:"left",textBaseline:"alphabetic"});

        window.addEventListener("resize",function(){
            _instance._resize();
        });
    };

    StreamPreviewBkp.getInstance = function(){
        return _instance;
    };

    p.updateInfo = function(){
        //this.machineInfoText.text = this.id;
    };

    p.setSource = function(src){
        //src = src.split("?")[0];
        this.src = src;//+"?autoplay=1&cc_load_policy=0&controls=0&rel=0&showinfo=0&disablekb=1&iv_load_policy=3&modestbranding=1&fs=0";
    };

    p.setID = function(id){
        this.id = id;
    };

    p.activate = function(){
        _instance.videoStream.visible = true;
        //_instance.iFrame.src = _instance.src;// za Iframe
    };

    p.reset = function(){
        _instance.videoStream.visible = false;
        //_instance.iFrame.src = "";// za Iframe
        _instance.id = "";
        //this.machineInfoText.text = "--";
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p._resize = function(){
        var w = window.innerWidth;
        var h = window.innerHeight;

        var gameWidth = alteastream.AbstractScene.GAME_WIDTH;
        var gameHeight = alteastream.AbstractScene.GAME_HEIGHT;

        var scaleX = w / (gameWidth+this.sWidth);
        var scaleY = h / (gameHeight+this.sHeight);

        this.scaleX = (gameWidth*scaleX)/1000;
        this.scaleY = (gameWidth*scaleY)/1000;
        this.x = w*this._getDynamicPosition().x;
        this.y = h*this._getDynamicPosition().y;
    };

    p.setDynamicPosition = function(xPerc,yPerc){
        this._dynamicPositionXY = {x:xPerc,y:yPerc};
        this._resize();
    };

    p._getDynamicPosition = function(){
        return this._dynamicPositionXY;
    };

    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new alteastream.DynamicText(text,font,color).set(props);
        this.addChild(textInstance);
    };

    alteastream.StreamPreviewBkp = createjs.promote(StreamPreviewBkp,"Container");
})();


