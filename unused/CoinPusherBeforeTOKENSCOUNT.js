/*http://139.59.147.230:82/games/rules*/
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var CoinPusher = function() {
        this.AbstractScene_constructor();
        this.initialize_CoinPusher();
    };
    var p = createjs.extend(CoinPusher, alteastream.AbstractScene);

// static properties:
// events:
// public properties:
// private properties:

    var _instance = null;
    p.active = false;
    p._info = null;
    p.bonusWon = 0;
    p.bonusActive = false;
    p.hasBonus = false;
    p.minStake = 0;
    p.maxStake = 0;
    p.stakeInc = 0;
    p.credit = 0;
    p._stakeIncrements = [];
    p._selectedStake = 0;
    p.betManager = null;
    p.prizeQueue = [];
    p.queueCount = 0;
    p.queuePlaying = false;
    p.okToShowBalance = true;

    p.autoCounter = 0;

// constructor:
    p.initialize_CoinPusher = function() {
        stage.enableMouseOver(0);
        _instance = this;

        this._local_activate();// local ver
        this._callInitMethods();

        console.log("initialize_CoinPusher..");
    };

    CoinPusher.getInstance = function(){
        return _instance;
    };

    p._callInitMethods = function() {
        this._setControlBoard();
        this.setHttpCommunication();
        this.setSocketCommunication();
        this._addInfo();
        this.startCommunication();
        this.monitorNetworkStatus();
    };

    p.showNetworkStatus = function(){
        var status = navigator.onLine? "online" : "offline";
        _instance.throwAlert(alteastream.Alert.INFO,"You are "+status,_instance.socketCommunicator.tryReconnect(status));
    };

    p._addInfo = function() {
        this._info = new alteastream.CoinPusherInfo();
        this._info.visible = false;
        this.addChild(this._info);
    };

    p._setInfo = function() {
        this._info.setInfo(this.payoutManager, this.hasBonus, this.betManager.getMultiplier());
        if(this.hasBonus === true){
            this._info.setBonusInfo(this.powerUp.neededForBonus)
        }
    };

    p._switchInfoAndWheelChildIndexes = function() {
        this.swapChildren(this._info, this.bonusContainer);
    };

    p._setStreamVideo = function(address){
        this.streamVideo = new alteastream.VideoStream(this,"videoOutput",address);
        //this.streamVideo.activate(function(){_instance.showGame(true);});
    };

    p._setControlBoard = function(){
        var stakeIncrements = [0, 0, 0, 0, 0, 0, 0];
        this.setStakeIncrements(stakeIncrements);
        this.setBetTotal();

        this.controlBoard = new alteastream.CoinPusherControlBoard();
        this.controlBoard.plugin(this);
        this.controlBoard.initialSetup();

        var controller = new alteastream.Controller(this);
        controller.activate();
        this.controlBoard.twinComponent = controller;

        /*        var isMobile = this._isMobile(); // verzija ako hocemo da se ne instancira keyboard listener za mobilni
                if(isMobile === "not"){
                    var controller = new alteastream.Controller(this);
                    controller.activate();
                    this.controlBoard.twinComponent = controller;
                }*/
    };

    p._setBonusApplet = function(options){
        this.hasBonus = true;
        this.bonusContainer = new createjs.Container();

        var wheel = this.bonusWheel = new alteastream.PusherBonusWheel(this,options.bonus);
        this.bonusContainer.addChild(wheel);

        var highlightImg = "wheel_" + wheel.bonusesReceived.length * 2 + "_bonus_fields_highlight";
        var bonusHighlightField = this.bonusHighlightField = alteastream.Assets.getImage(alteastream.Assets.images[highlightImg]);
        bonusHighlightField.regX = bonusHighlightField.image.width * 0.5;
        bonusHighlightField.regY = bonusHighlightField.image.height;
        bonusHighlightField.visible = false;
        this.bonusContainer.addChild(bonusHighlightField);

        var ring = alteastream.Assets.getImage(alteastream.Assets.images.bounsWonBgImageRing);
        this.bonusContainer.addChild(ring);
        ring.regX = ring.image.width*0.5;
        ring.regY = ring.image.width*0.5;
        ring.mouseEnabled = false;

        var powerUp = this.powerUp = new alteastream.PowerUpWheel(this);
        this.bonusContainer.addChild(powerUp);
        var radius = 366;
        powerUp.build("lampOff","lampOn",radius,options.bonus_limit);

        var bounsWonBgImage = alteastream.Assets.getImage(alteastream.Assets.images.bounsWonBgImage);
        this.bonusContainer.addChild(bounsWonBgImage);
        bounsWonBgImage.regX = bounsWonBgImage.image.width*0.5;
        bounsWonBgImage.regY = bounsWonBgImage.image.height*0.5;

        var bonusWonText = this.bonusWonText = new createjs.Text("", '96px Technology', "#ed2224").set({textBaseline:"middle",textAlign:"right"});
        this.bonusContainer.addChild(bonusWonText);
        bonusWonText.x = 66;
        bonusWonText.y = 9;

        var marker = this.marker = alteastream.Assets.getImage(alteastream.Assets.images.arrow);
        this.bonusContainer.addChild(marker);
        marker.regX = marker.image.width*0.5;
        marker.x = 0;
        marker.y = -420;
        marker.mouseEnabled = false;

        this.addChild(this.bonusContainer);
        this.bonusContainer.x = this.bonusContainer.startX = 250;
        this.bonusContainer.y = this.bonusContainer.startY = 250;
        this.bonusContainer.xPosForAnimation = 400;
        this.bonusContainer.yPosForAnimation = 400;
        this.bonusContainer.xPosForInfo = 960;
        this.bonusContainer.yPosForInfo = 230;
        this.bonusContainer.mouseEnabled = false;
        this.bonusContainer.scaleX = this.bonusContainer.scaleY = 0.5;

        this.bonusWheel.setSkin(function () {
            _instance.streamVideo.activate(function(){_instance.showGame(true);});
        });
    };

    /*    p._isMobile = function() { // verzija ako hocemo da se ne instancira keyboard listener za mobilni
            return localStorage.getItem("isMobile");
        };*/

    p.bonusFocusIn = function(bool,callback){
        if(bool === true){
            TweenLite.to(this.bonusContainer,1,{ease: Expo.easeInOut,x:this.bonusContainer.xPosForAnimation,y:this.bonusContainer.yPosForAnimation,scaleX:0.7,scaleY:0.7,onComplete:function(){
                    callback();
                }});
        }else{
            TweenLite.to(this.bonusContainer,1,{ease: Expo.easeInOut, x:this.bonusContainer.startX,y:this.bonusContainer.startY,scaleX:0.5,scaleY:0.5,onComplete:function(){
                    callback();
                }});
        }
    };

    p.startCommunication = function(){
        this.addChild(this.controlBoard);
        this.controlBoard.visible = false;
    };

    /*p.onSocketConnect = function(frame){// invalid token fix old
        _instance.socketCommunicator.onGameConnect(frame);
        _instance.requester.startGameStream(function(response){
            _instance.socketCommunicator.startGame(response);
            _instance._setStreamVideo(response);
            _instance.setGameParameters(response);
            _instance.setPayout(response.payout);
            _instance.setRegularStakes();
            _instance.setCurrency();
            if(Object.keys(response.bonus).length === 0){
                _instance.streamVideo.activate(function(){_instance.showGame(true);});
            }else{
                _instance._setBonusApplet(response);
            }
            _instance._setInfo();
            _instance.showGame(false);
            _instance.setRenderer();

            var canvas = document.getElementById("screen");
            canvas.focus();
            if(document.activeElement !== canvas)
                canvas.focus();
        });
    };*/

    p.onSocketConnect = function(frame){ // invalid token fix new
        _instance.socketCommunicator.onGameConnect(frame);
        var response = window.localStorage.getItem('machineParameters');
        var machineResponse = JSON.parse(response);
        _instance.socketCommunicator.startGame(machineResponse);
        _instance._setStreamVideo(machineResponse);
        _instance.setGameParameters(machineResponse);
        _instance.setPayout(machineResponse.payout);
        _instance.setRegularStakes();
        _instance.setCurrency();
        if(Object.keys(machineResponse.bonus).length === 0){
            _instance.streamVideo.activate(function(){_instance.showGame(true);});
        }else{
            _instance._setBonusApplet(machineResponse);
        }
        _instance._setInfo();
        _instance.showGame(false);
        _instance.setRenderer();
        window.localStorage.removeItem('machineParameters');

        var canvas = document.getElementById("screen");
        canvas.focus();
        if(document.activeElement !== canvas)
            canvas.focus();
    };

    p.showGame = function(bool){
        if(this.hasBonus === true){
            this.bonusContainer.visible = bool;
        }

        if(bool === true){
            //temp reminder::
            if(!this.betManager.canBet(this.credit,this.getSelectedStake())){
                this.throwAlert(alteastream.Alert.EXCEPTION,"Insufficient balance");
            }

            alteastream.Assets.playSound("shootCoinsSpoken", 0.2);
        }

        this.controlBoard.visible = bool;
    };

    p.setPayout = function(response){ // todo temp creating cleanPayouts, response should give us cleaned array
        var tags = Object.keys(response);
        var cleanPayouts = {};
        for(var i = 0; i < tags.length; i++){
            var tag = tags[i];
            var prize = response[tag];

            if(prize !== 0 && tag !== "101"){
                cleanPayouts[tag] = prize;
            }
        }
        var refund = response["101"] || 0.25;
        refund = refund*this.betManager.getMultiplier();
        this.payoutManager = new alteastream.PayoutManager(cleanPayouts,refund);
    };

    p.onInfo = function(){
        this._info.visible = !this._info.visible;
        if(this.hasBonus === true){
            this._setBonuswheelPositionAndForInfo(this._info.visible);
        }

        alteastream.Assets.playSound("btnDownDouble");
    };

    p._setBonuswheelPositionAndForInfo = function(infoIsActive) {
        if(infoIsActive === true){
            this.bonusContainer.x = this.bonusContainer.xPosForInfo;
            this.bonusContainer.y = this.bonusContainer.yPosForInfo;
        }else{
            this.bonusContainer.x = this.bonusContainer.startX;
            this.bonusContainer.y = this.bonusContainer.startY;
        }
    };

    p.checkIfinfoIsActive = function() {
        if(this._info.visible === true){
            this.onInfo();
        }
    };

    p.onPrizeDetection = function(msgValue){
        var amount = this.payoutManager.getPayoutByPrize(msgValue.prize);
        this.okToShowBalance = false;
        this.setCrd(msgValue.balance);
        this.showWin(amount,msgValue);
    };

    p.onQuit = function(){
        this.streamVideo.stop();
        this.killGame();
        this.closeGame();
    };

    p.closeGame = function(){
        this.requester.backToLobby(function (res) {
            window.top.manageBgMusic("pause");
            window.top.stopHelperSound("machineNoise");
            //window.localStorage.setItem('backgroundMusicIsMuted', String(_instance.controlBoard.soundToggled));
            window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
            //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN+"&backToHouse="+alteastream.AbstractScene.BACK_TO_HOUSE);
            //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_URL+"/lobby/index.html?usr="+alteastream.AbstractScene.GAME_TOKEN);
        });
    };

    p.fillToBonus = function(count){
        this.powerUp.fill(count);
    };

    p.onBonusWon = function(msg){
        var bal = msg.vl.balance - msg.vl.money;
        this.setCrd(bal);
        this.okToShowBalance = false;
        this.onBonusAllowed(msg.vl.amount);
    };

    p.onBonusAllowed = function(amount){
        console.log("onBonusAllowed");
        this.checkIfinfoIsActive();
        this.controlBoard.disablePlayButtons(true);
        this.bonusWon = amount;
        this.bonusActive = true;

        this.powerUp.fillAll();
        this.spinForBonus();
    };

    p.spinForBonus = function(){
        this.powerUp.lightsAnimation(false);
        alteastream.Assets.playSound("bonusWinnerSpoken_1",0.3);
        this.bonusFocusIn(true,function(){
            _instance.powerUp.rotationAnimation(true);
            _instance.bonusWheel.spin(_instance.bonusWon);
            alteastream.Assets.playSoundChannelWithOptions("bonusSpinLoop", {loop:-1,vol:0.2});
        });
    };

    p.onBonusPlayed = function(){
        console.log("onBonusPlayed");
        this.bonusHighlightField.visible = true;
        this.bonusActive = false;
        this.bonusWonText.visible = true;
        this.bonusWonText.text = this.bonusWon;
        this.powerUp.rotationAnimation(false);
        this.powerUp.lightsAnimation(true);
        alteastream.Assets.playSound("win1",0.3);
        var bonusWonSpoken = "prize_"+this.bonusWon;
        var appendixWord = "bonusWord";// temp
        this.playSoundChain("youHaveWonSpoken",0.3,function(){
            _instance.playSoundChain(bonusWonSpoken,0.3,function(){
                _instance.playSoundChain(appendixWord,0.3,function(){
                    _instance.powerUp.lightsAnimation(false);
                    _instance._bonusCoinSpawn(_instance.bonusWon);
                });
            });
        });
    };

    p.endBonusFeature = function(){
        this.bonusFocusIn(false,function(){
            _instance.okToShowBalance = true;
            _instance.bonusWonText.text = "";
            _instance.bonusWonText.visible = false;
            _instance.bonusHighlightField.visible = false;
            _instance.bonusWheel.highlightWin(false);
            _instance.powerUp.clear();
            _instance.controlBoard.disablePlayButtons(false);
            _instance.controlBoard.setCrd(_instance.credit);
            _instance.bonusWon = 0;
            alteastream.Assets.stopSoundChannel("bonusSpinLoop");
            alteastream.Assets.playSound("win1",0.3);
            console.log("BONUS END");
        });
    };

    p._bonusCoinSpawn = function(n){
        var utils = gamecore.Utils.NUMBER;
        var coinOr = alteastream.Assets.getImage(alteastream.Assets.images.chip_1);
        var cnt = 0;
        for(var i = 0; i < n; ++i) {
            setTimeout(function(){
                var coin = coinOr.clone();
                _instance.addChild(coin);

                coin.pivot = coin.image.width*0.5;
                coin.regX = coin.pivot;
                coin.regY = coin.pivot;
                coin.x = _instance.bonusContainer.xPosForAnimation;
                coin.y = _instance.bonusContainer.yPosForAnimation;

                coin.turn = 0.2;
                coin.vr = utils.randomRange(-4,4);
                coin.vx = utils.randomRange(-4,4);
                coin.vy = utils.randomRange(-15,-30);
                alteastream.Assets.playSound("bonusInc",0.3);

                coin.on("tick", function spawn(){
                    this.vy += 1.1;
                    this.x += this.vx;
                    this.y += this.vy;
                    this.rotation += this.vr;
                    this.scaleY += this.turn;

                    if(this.scaleY > 0.9)
                        this.turn = -0.2;

                    else if(this.scaleY < 0.1)
                        this.turn = 0.2;

                    if(this.y - this.pivot > 1080) {
                        this.off("tick",spawn);
                        this.parent.removeChild(this);
                        cnt++;
                        if(cnt === n){
                            _instance.endBonusFeature();
                        }
                    }
                });
            },i*100);
        }
    };

    // VERZIJA 3 //
    p.refundCoinSpawn = function(response) {
        // todo vrednost refund coina u evrima 0.025e a 0.25 pravog coina(onaj koji se ispaljuje)
        var refundTokenValue = this.payoutManager.getRefundValue();
        _instance.setCrd(response.vl.balance);
        var balanceText = this.controlBoard._textCrdAmount;
        balanceText.color = "#f7ff28";
        var container = new createjs.Container();
        var back = alteastream.Assets.getImage(alteastream.Assets.images.refundComponentBackground);
        var coinsChargedLabel = new createjs.Text('Coins Charged!', '18px HurmeGeometricSans3', "#ffffff").set({textBaseline:"middle",textAlign:"center"});
        coinsChargedLabel.x = 103;
        coinsChargedLabel.y = 124;
        var coinsLabel = new createjs.Text('Coins', '36px HurmeGeometricSans3', "#ffffff").set({textBaseline:"middle",textAlign:"right"});
        coinsLabel.x = 105;
        coinsLabel.y = 200;

        //var coinsValue = (Math.floor(refundTokenValue * response.vl.tokens * 100))/10;//tokens version
        var coinsValue = ((Math.floor(refundTokenValue * response.vl.tokens * 100))*this.betManager.getMultiplier()).toFixed(2);
        //console.log(`${refundTokenValue} * ${response.vl.tokens} * 100 * ${this.betManager.getMultiplier()}`);
        var coinsNumber = new createjs.Text(coinsValue, '36px HurmeGeometricSans3', "#35c971").set({textBaseline:"middle",textAlign:"left"});
        coinsNumber.x = 123;//128
        coinsNumber.y = 200;
        var eurLabel = new createjs.Text('EUR', '25px HurmeGeometricSans3', "#bbc2cc").set({textBaseline:"middle",textAlign:"right"});
        eurLabel.x = 95;
        eurLabel.y = 255;
        //var refundTokenValue = this.payoutManager.getRefundValue();
        //var refundTokenValue = (Math.floor(refund * response.vl.money))/100;//money version
        var refundValue = (Math.floor(refundTokenValue * response.vl.tokens * 100))/100;//tokens version
        var eurNumber = new createjs.Text(refundValue, '25px HurmeGeometricSans3', "#ed802e").set({textBaseline:"middle",textAlign:"left"});
        eurNumber.x = 115;
        eurNumber.y = 255;
        container.addChild(back, coinsChargedLabel, coinsLabel, coinsNumber, eurLabel, eurNumber);
        container.regX = back.image.width * 0.5;
        container.regY = back.image.height * 0.5;
        container.x = alteastream.AbstractScene.GAME_WIDTH * 0.5;
        container.y = alteastream.AbstractScene.GAME_HEIGHT * 0.25;
        container.scale = 0;
        this.addChild(container);

        container.cache(0, 0, back.image.width, back.image.height);

        var containerXPositionForAnimation = this.controlBoard._textCrdAmount.x;
        var containerYPositionForAnimation = this.controlBoard._textCrdAmount.y;

        this.playSound("refund",0.3);
        createjs.Tween.get(container).to({scale:1},200).wait(2000).to({scale:0, x:containerXPositionForAnimation, y:containerYPositionForAnimation},300).call(function () {
            balanceText.color = "#00ff00";
            container.removeAllChildren();
            _instance.removeChild(container);
        });
    };
    // VERZIJA 3 //

    // VERZIJA 1 //
    /*    p.refundCoinSpawn = function(n){
            var utils = gamecore.Utils.NUMBER;
            var coinOr = alteastream.Assets.getImage(alteastream.Assets.images.chip_0);
            var cnt = 0;
            var cbHeight = _instance.controlBoard.background.image.height;
            //var refundTotalPopup =
            for(var i = 0; i < n; ++i) {
                setTimeout(function(){
                    var coin = coinOr.clone();
                    _instance.addChildAt(coin,0);

                    coin.pivot = coin.image.width*0.5;
                    coin.regX = coin.pivot;
                    coin.regY = coin.pivot;
                    coin.x = alteastream.AbstractScene.GAME_WIDTH*0.5;
                    coin.y = alteastream.AbstractScene.GAME_HEIGHT-cbHeight-coin.pivot;

                    coin.turn = 0.2;
                    coin.vr = utils.randomRange(-4,4);
                    coin.vx = utils.randomRange(-4,4);
                    coin.vy = utils.randomRange(-15,-30);
                    alteastream.Assets.playSound("bonusInc",0.3);

                    coin.on("tick", function spawn(){
                        this.vy += 1.1;
                        this.x += this.vx;
                        this.y += this.vy;
                        this.rotation += this.vr;
                        this.scaleY += this.turn;

                        if(this.scaleY > 0.9)
                            this.turn = -0.2;

                        else if(this.scaleY < 0.1)
                            this.turn = 0.2;

                        if(this.y - this.pivot > alteastream.AbstractScene.GAME_HEIGHT) {
                            this.off("tick",spawn);
                            this.parent.removeChild(this);
                            cnt++;
                            if(cnt === n){
                                setTimeout(function(){
                                    console.log("some popup info here>>>>>>>>>>");
                                    //_instance.removeChild(refundTotal);
                                },2000);
                            }
                        }
                    });
                },i*100);
            }
        };*/
    // VERZIJA 1 //

    p.setGameParameters = function(response){
        this.setCrd(response.balance);

        this.minStake = 1;
        this.maxStake = response.maxTokens || 20;
        this.stakeInc = 1;
        this.setStakeIncrementsMinMax(this.minStake, this.maxStake, this.stakeInc);

        this.currency = response.currency || "N/A";

        this.betManager.setMultiplier(response.tokenValue);
    };

    p.setBetTotal = function(){
        this.betManager = new alteastream.BetTotal(this);
    };

    p.setCurrency = function(){
        this.controlBoard.setCurrency(this.currency);
    };

    p.updateInfoPrizes = function(toValue) {
        this._info.updatePrizes(toValue);
    };

    p.updateWheelPrizes = function(toValue) {
        var multiplier = this.betManager.getMultiplier();
        this.bonusWheel.updatePrizes(toValue, multiplier);
    };

    p.setRegularStakes = function(){
        this.setStake(0);
    };

    p.setStake = function(position){
        this._selectedStake = position;
        this.betManager.setStake();
    };

    p.getSelectedStake = function(){
        return this._stakeIncrements[this._selectedStake];
    };

    p.setStakeIncrements = function(increments){
        this._stakeIncrements = increments;
    };

    p.setStakeIncrementsMinMax = function(min,max,inc){
        var arr = [];
        for(var i = min; i <= max; i = Math.round((i+inc)*100)/100){
            arr.push(i);
        }
        this._stakeIncrements = arr;
    };

    p.setCrd = function(amount){
        this.credit = amount;
        if(this.okToShowBalance){
            this.controlBoard.setCrd(amount);
        }
    };

    p.setWin = function(amount){
        this.controlBoard.setWin(amount);
    };

    //PARAMS TO SEND
    p.getSendBetParams = function(){
        return  {bet: this.betManager.betValue()};
    };

    p.getShooterDirectionParams = function(dir){
        return  {direction: dir};
    };

    //BUTTON HANDLERS
    p.moveShooterBtnHandler = function(e) {
        this.socketCommunicator.moveShooter(e);
        this.playSound("btnDown");

        this.playSoundChannelWithOptions("shooter",{});
    };

    p.stopShooterBtnHandler = function(e) {
        this.socketCommunicator.stopMovingShooter(e);
        this.playSound("btnUp");

        this.stopSoundChannel("shooter");
    };

    p.allowedShooterBtn = function(msg) {
        this.controlBoard.tryShooter();
        this.setCrd(msg.vl);
    };

    /*p.sendBtnHandler = function(e) {
        if(this.controlBoard.btnSend._disabled)
            return;
        this.controlBoard.blockShooter(true);
        this.socketCommunicator.sendBet(this.getSendBetParams());
        //this.playSound("btnDownDouble");
        this.playSound("insertCoin",0.6);
        this.checkIfinfoIsActive();
    };*/

    p.sendBtnHandler = function(e) {
        if(this.betManager.canBet(this.credit,this.getSelectedStake())){
            if(this.controlBoard.btnSend._disabled)
                return;
            this.controlBoard.blockShooter(true);
            this.socketCommunicator.sendBet(this.getSendBetParams());
            //this.playSound("btnDownDouble");
            this.playSound("insertCoin",0.6);
            this.checkIfinfoIsActive();
        }else{
            this.controlBoard.blockShooter(true);
            var that = this;
            this.throwAlert(alteastream.Alert.EXCEPTION,"Insufficient balance",function(){
                that.controlBoard.tryShooter();
            })
        }
    };

    p.collectBtnHandler = function() {
        this.socketCommunicator.collect(this.onCollectResponse);
    };

    p.incStakeBtnHandler = function(){
        this.setStake(this._selectedStake + 1);
        this.playSound("btnDownDouble");
    };

    p.decStakeBtnHandler = function(){
        this.setStake(this._selectedStake - 1);
        this.playSound("btnDownDouble");
    };

    p.infoBtnHandler = function() {
        this.onInfo();
    };

    p.quitBtnHandler = function(){
        this.socketCommunicator.disposeCommunication();
        this.controlBoard.disablePlayButtons(true);
        this.blockControls(true);
        this.playSoundChain("seeYouSpoken",0.3,this.onQuit,this);
    };


    p.quitLimitReached = function(){
        this.controlBoard.disablePlayButtons(true);
        this.blockControls(true);
        this.playSound("byeSpoken",0.2);
        this.throwAlert(alteastream.Alert.INFO,"Non playing time reached...game is closed",function(){_instance.onQuit();});
    };

    p.inactivityWarning = function(n){
        if(n!==3){
            var type = n===1?"First":"Second";
            this.throwAlert(alteastream.Alert.WARNING, type+" warning...return to game");
            this.playSound("continuePlayingSpoken",0.2);
        }else{
            this.controlBoard.disablePlayButtons(true);
            this.blockControls(true);
            this.throwAlert(alteastream.Alert.INFO,"No activity...game is closed",function(){_instance.onQuit();});
        }
    };

    p.blockControls = function(bool){
        this.controlBoard.mouseEnabled = !bool;
    };

    p.showWin = function(amount,msgValue){
        var prize = msgValue.prize;
        //var win = msgValue.win;
        this.prizeQueue.unshift([amount,prize]);
        if(!this.queuePlaying)
            this.playQueue();
    };

    p.playQueue = function(){
        //console.log("START QUEUE");
        this.queuePlaying = true;
        var multiplier = this.betManager.getMultiplier();

        var iterateQueue = function(){
            if(_instance.prizeQueue.length>0){
                var lastElement = _instance.prizeQueue.pop();
                //var win = lastElement[2];
                _instance.queuePlaying = true;
                var chip = new alteastream.Chip(lastElement[0],lastElement[1],multiplier);
                var _width = alteastream.AbstractScene.GAME_WIDTH || document.getElementById("screen").width;
                var _height = alteastream.AbstractScene.GAME_HEIGHT || document.getElementById("screen").height;
                chip.x = _width-250;
                chip.y = _height*0.5+50;
                _instance.addChild(chip);
                var yAnimation = _height * 0.2;

                var isEur = _instance.controlBoard._toValueView;

                //console.log("IS BONUS?::  "+_instance.isBonus);
                TweenLite.to(chip,2,{y:yAnimation,onUpdate:function () {
                        var doAnimation = _instance.controlBoard._toValueView !== isEur;
                        isEur = _instance.controlBoard._toValueView;
                        chip.updateChip(_instance.controlBoard._toValueView, doAnimation);
                    },onComplete:function(){
                        _instance.removeChild(chip);
                        chip = null;
                        //_instance.setWin(win);
                        if(!_instance.bonusActive){
                            _instance.okToShowBalance = true;
                            _instance.setCrd(_instance.credit);
                        }
                    },ease: Elastic.easeOut.config(0.5, 0.1)});

                var prizeSpoken = "prize_"+lastElement[0];
                _instance.playSoundChain(prizeSpoken,0.3,function(){// append bonus on next soundChain?
                    _instance.queueCount++;
                    iterateQueue();
                },_instance);
            }else{
                if(_instance.queueCount > 2){
                    var phrase = "";
                    if(_instance.queueCount < 6 ) { phrase = "reallyGoodSpoken";}
                    if(_instance.queueCount > 5 && _instance.queueCount < 9 ) { phrase = "feelDizzySpoken";}
                    if(_instance.queueCount > 8 ) { phrase = "keepGoingSpoken";}
                    _instance.playSoundChain(phrase,0.3,function(){ _instance.queuePlaying = false; iterateQueue();});
                }else{
                    _instance.queuePlaying = false;

                    if(!_instance.bonusActive){
                        _instance.okToShowBalance = true;
                        _instance.setCrd(_instance.credit);
                        _instance.controlBoard.blockShooter(false);
                    }

                    /* if(_instance.bonusWon>0){
                         console.log("onBonusAllowed YES QUEUE");
                         _instance.spinForBonus(_instance.bonusWon);
                     }*/
                }
                _instance.queueCount = 0;
            }
        };
        this.playSound("win1",0.3);
        this.playSoundChain("youHaveWonSpoken",0.3,iterateQueue);
    };

    p.onMachineError = function(err){
        this.throwAlert(alteastream.Alert.WARNING,"Machine error...");
        this.playSound("errorSpoken",0.2);
    };

    p._local_activate = function() {
        /*        this._isMobile = function() { // verzija ako hocemo da se ne instancira keyboard listener za mobilni
                    //return "yes";
                    return "not";
                };*/
        this.test_BonusFill = function(){
            setTimeout(function(){_instance.powerUp.fill(1);},1000);
            setTimeout(function(){_instance.powerUp.fill(5);},1500);
            setTimeout(function(){_instance.powerUp.fill(6);},2000);
            setTimeout(function(){_instance.powerUp.fill(8);},2500);
            setTimeout(function(){_instance.powerUp.fill(20);},3000);
        };
        this.test_BonusSpin = function(){
            _instance.onBonusAllowed(5);
        };
        this.test_Queue = function(){
            setTimeout(function(){
                var vl = {"prize":"1","amount":50,"money":500,"balance":661000,"win":8};
                _instance.onPrizeDetection(vl)
            },500);
            setTimeout(function(){
                var vl = {"prize":"2","amount":10,"money":1000,"balance":661700,"win":8};
                _instance.onPrizeDetection(vl)
            },1500);
            setTimeout(function(){
                console.log("START SECOND");
                var vl1 = {"prize":"3","amount":15,"money":1500,"balance":661700,"win":8};
                _instance.onPrizeDetection(vl1);
            },2500);
            setTimeout(function(){
                console.log("START THIRD");
                var vl2 = {"prize":"4","amount":45,"money":4500,"balance":661700,"win":8};
                for(var i = 0;i<2;i++){
                    _instance.onPrizeDetection(vl2);
                }
            },3500);
        };

        this.test_BonusSpin = function(n){
            _instance.onBonusAllowed(n);
        };
        this._callInitMethods = function () {};
        ///////////////////BONUS TEST ADD//////////////////////
        this._setControlBoard();
        //response sa bonusom
        //var response = {"balance":10000,"tokens":0,"maxTokens":20,"currency":"EUR","payout":{1: 5, 2: 15, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 40, 12: 100},"bonus":{"1":5,"2":10,"3":15,"4":45,"5":90},"bonus_limit":20,"gameCode":"ticketcircus"};
        //response bez bonusa
        //var response = {"balance":2055700,"tokens":0,"maxTokens":20,"currency":"EUR","payout":{1: 5, 2: 15, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 40, 12: 100},"bonus":{},"bonus_limit":0,"gameCode":"ticketcircus","tokenValue":100.0};
        //coins "payout":{"1":5,"2":10,"3":15,"4":45,"5":45}
        //marble "payout":{"1":5,"2":10,"3":15,"4":45,"5":45}
        var response = {"state":0,"key":"79a2fb56-7c2c-46cf-97d5-35db5661425e","controlIdleTime":60000,"streamendpoint":"ws://87.130.112.6:8889","webrtcurl":"ws://87.130.112.6:8083/ws","balance":999800,"tokenValue":100.0,"tokens":0,"maxTokens":20,"currency":"EUR","payout":{"1":5,"2":10,"3":15,"4":45,"101":0.25},"bonus":{},"bonus_limit":0,"gameCode":"ticketcircus"};
        this.setGameParameters(response);
        if(Object.keys(response.bonus).length === 0){/*this.streamVideo.activate(function(){this.showGame(true);});*/}
        else{this._setBonusApplet(response);}
        this.bonusWon = 10;//with bonus
        this.setPayout(response.payout);
        this.setRegularStakes();
        this.setCurrency();
        this._addInfo();
        this._setInfo();
        this._switchInfoAndWheelChildIndexes();

        this.addChild(this.controlBoard);
        //_instance.test_Queue();

        // no need for now, there is no bonus wheel
        /*        var button = new createjs.Shape();
                button.graphics.beginFill('#29abe2').drawRoundRect(0,0,170,56,6);
                button.regX = 85;
                button.regY = 28;
                button.x = alteastream.AbstractScene.GAME_WIDTH*0.5;
                button.y = alteastream.AbstractScene.GAME_HEIGHT*0.5;
                this.addChild(button);
                button.cache(0,0,200,100);
                button.on("click", function () {
                    _instance.removeChild(button);
                    ///////////////////BONUS TEST START//////////////////////
                    _instance.test_BonusSpin(_instance.bonusWon);
                    ///////////////////BONUS TEST END//////////////////////
                    ///////////////////QUEUE TEST START//////////////////////
                    //_instance.test_Queue();// includes spin if bonusWon >0
                    ///////////////////QUEUE TEST END//////////////////////
                });*/

        //5 = blue 0.50, tag id 1
        //10 = green 1, tag id 2
        //15 = pink 2, tag id 3
        //45 = orange 5, tag id 4
        //90 = yellow 10, tag id 5

        //NEW PAYOUTS::::::::::::::::COINS::::::::::::::::::::::::
        //one coin = 0.10
        //0.25 = metal 0.025,    tag id 0

        //5 = blue 0.50,         tag id 1
        //10 = green/black 1.00, tag id 2
        //15 = red 15.00,        tag id 3
        //45 = yellow 4.50,      tag id 4


        // "TOKENS DETECTION" msg  - Only available in Cash Circus and Cash Festival machines (COINS)

        //NEW PAYOUTS::::::::::::::::MARBLE::::::::::::::::::::::::
        //one pearl = 0.10
        //5 = blue 0.50,             tag id 1
        //15 = green/black 1.00,     tag id 2
        //40 = red 4.00,             tag id 3
        //100 = yellow 10.00,        tag id 4

        //this.test_Queue();
        /*        setInterval(function () {
                    _instance.refundCoinSpawn({"ts":1578476179098,"vl":{"tokens":11,"amount":100,"money":1100,"balance":1028000},"tp":6,"cat":50});
                },4000);*/

    };

// static methods:
// public methods:

    alteastream.CoinPusher = createjs.promote(CoinPusher,"AbstractScene");
})();