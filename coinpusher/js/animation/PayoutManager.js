
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var PayoutManager = function(payout,refund) {
        this.initialize_PayoutManager(payout,refund);
    };

    var p = PayoutManager.prototype;
    var _payouts = null;
    // private properties:
    p._refund = null;

    // static properties:
    // events:
    // public properties:
    // private methods:
    // constructor:
    p.initialize_PayoutManager = function(payout,refund) {
        _payouts = payout;
        this._refund = refund;
    };
    // static methods:
    // public methods:
    //{"state":0,"key":"468356ef-484f-4b7e-b774-f871416fa2b4","controlIdleTime":60000,"streamendpoint":"ws://139.59.137.63:8889","webrtcurl":"ws://84.199.144.157:8083/ws","balance":1000000,"tokens":0,"maxTokens":20,"currency":"EUR","payout":{"1":1,"2":5,"3":10,"4":25,"5":50,"6":1,"7":1,"8":1,"9":1,"10":1,"11":1,"12":1},"bonus":{"1":5,"2":10,"3":15,"4":45,"5":90},"bonus_limit":20,"gameCode":"ticketcircus"}
    //{""payout":{"1":5,"2":10,"3":15,"4":45},"bonus":{"1":5,"2":10,"3":15,"4":45,"5":90},"bonus_limit":20,"gameCode":"ticketcircus"}
    //multiplier = 0.1
    p.getPayoutByPrize = function(prize){
         return _payouts[prize];
         //(Math.round(Math.floor(5 * 10)) / 100).toFixed(2);
    };

    p.getPayouts = function() {
        return _payouts;
    };

    p.getRefundValue = function() {
        return this._refund;
    };

    alteastream.PayoutManager = PayoutManager;
})();