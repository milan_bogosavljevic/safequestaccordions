"use strict";
// namespace:
this.alteastream = this.alteastream || {};

(function(){

    var CoinBonusField = function(n,i,length) {
        this.ABSField_constructor(i,length);
        this.initialize_CoinBonusField(n,length);
    };
    var p = createjs.extend(CoinBonusField, alteastream.ABSField);

    // public methods
    p.outerRadiusShrink = 68;
    p.innerRadiusShrink = 145;
    // constructor
    p.initialize_CoinBonusField = function(n,length){
        this.defaultTextColor = "#fff";
        this.defaultSliceColor = "#00fff5";
        this.highlightTextColor = "#000000";

        this.startX = 0;
        this.startY = 60;
        this.sWidth = this.WIDTH_SUM/length;
        this.sHeight = 380;

        //var slice = this.slice = this._createSlice(this.defaultSliceColor); // dynamic draw field ver
        //this.addChild(slice); // dynamic draw field ver
        //slice.alpha = 0.3;

        this.regX = this.sWidth * 0.5;
        this.regY = this.sHeight;

        var font = '50px Impact';

        var spacingY = this.count % 2===0?this.outerRadiusShrink:this.innerRadiusShrink;
        var itemValue = this.itemValue = new createjs.Text(String(n), font, this.defaultTextColor).set({textBaseline:"top",textAlign:"center",mouseEnabled:false});

        this.addChild(itemValue);
        itemValue.x = this.regX;
        itemValue.y = spacingY;
    };

    alteastream.CoinBonusField = createjs.promote(CoinBonusField,"ABSField");
})();
