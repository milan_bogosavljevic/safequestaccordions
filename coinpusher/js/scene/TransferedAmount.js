this.alteastream = this.alteastream || {};

(function() {
    "use strict";

    var TransferedAmount = function(amount, font, color, callback) {
        this.Container_constructor();
        this.initialize_TransferedAmount(amount, font, color, callback);
    };

    var p = createjs.extend(TransferedAmount, createjs.Container);

    p.initialize_TransferedAmount = function (amount, font, color, callback){
        var amountText = new createjs.Text("+ " + amount, font, color);
        amountText.textAlign = "center";
        amountText.textBaseline = "middle";

        var w = amountText.getMeasuredWidth() + 30;
        var h = amountText.getMeasuredHeight() + 30;

        var shape = new createjs.Shape();
        shape.graphics.beginFill("#000000").drawRoundRect(0, 0, w, h, 5);
        shape.cache(0, 0, w, h);
        shape.regX = w * 0.5;
        shape.regY = h * 0.5;

        var contToAnimate = new createjs.Container();
        contToAnimate.alpha = 0;
        contToAnimate.scale = 0.5;
        contToAnimate.addChild(shape, amountText);
        this.addChild(contToAnimate);
        this.mouseChildren = false;
        this.mouseEnabled = false;
        var _this = this;

        createjs.Tween.get(contToAnimate).to({alpha:1, y:-20}, 150, createjs.Ease.quartIn).call(function (){
            createjs.Tween.get(contToAnimate).to({y:-150, scale:1},500, createjs.Ease.quartOut).wait(650).call(function (){
                createjs.Tween.get(contToAnimate).to({alpha:0,scale:0.5},300, createjs.Ease.quartOut).call(function(){
                    contToAnimate.removeAllChildren();
                    _this.removeChild(contToAnimate);
                    _this.parent.removeChild(_this);
                    if(callback){
                        callback();
                    }
                })
            })
        });
    }

    alteastream.TransferedAmount = createjs.promote(TransferedAmount,"Container");
})();