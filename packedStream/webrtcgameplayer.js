function WEBRTCGamePlayer(element) {
    var me = this;

    this.video              = element;
    this.STOPPED			= 0;
    this.CONNECTING_WS		= 1;
    this.WS_CONNECTED		= 2;
    this.NEGOTIATE_STREAM	= 3;
    this.STREAMING			= 4;

    this.status				= this.STOPPED;
    this.ws					= null;
    this.secure				= false;
    this.url				= "";
    this.address        	= "";  //res.webrtcurl + "?token=" + user_token + "&id=" + machine_name;
    this.camera         	= "";  //res.webrtccam;

    this.sendMessage 		= function(message) {
        console.debug(message);
        var jsonMessage = JSON.stringify(message);
        console.log('Senging message: ' + jsonMessage);
        this.ws.send(jsonMessage);
    };

    this.onError 				= function(msg){

    };

    this.onStreamEvent			= function(msg){
        console.debug(msg);
    };

    this.connectHandler		= function(){
        console.debug("socket connected")
    };

    this.startResponse = function(message) {
        console.log('SDP answer received from server. Processing ...');
        this.webRtcPeer.processAnswer(message.sdpAnswer, function(error) {
            if (error) {
                me.onError(error);
                return false;
            }
        });
    };
    this.messageHandler		= function(message){
        var parsedMessage = JSON.parse(message.data);
        console.info('Received message: ' + message.data);
        switch (parsedMessage.id) {
            case 'ready':
                me.startStreaming();
                break;
            case 'startResponse':
                me.startResponse(parsedMessage);
                break;
            case 'error':
                var errMsg = parsedMessage.message || parsedMessage.txt;
                me.onError('Error message from server: ' + errMsg);
                break;
            case 'streamEvent':
                me.onStreamEvent(parsedMessage);
                break;
            case 'iceCandidate':
                me.webRtcPeer.addIceCandidate(parsedMessage.candidate, function(error) {
                    if (error){
                        me.onError('Error adding candidate: ' + JSON.stringify(error));
                        return false;
                    }
                });
                break;
            //default:
            //me.onError('Unrecognized message', JSON.stringify(parsedMessage));
        }
    };

    this.startStreaming			= function(){
        console.log("startSTREAMING:::::::::::::::::::::");
        var options = {
            remoteVideo : me.video,
            mediaConstraints : {
                //audio : false,
                audio : true,
                video : {
                    width: 1920,
                    //framerate: 25
                    framerate: 60
                }
            },
            onicecandidate : me.onIceCandidate
        };
        //console.info('User media constraints' + userMediaConstraints);

        this.webRtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options,
            function(error) {
                if (error){
                    me.onError();
                    return false;
                }
                me.webRtcPeer.generateOffer(me.onOffer);
            });
        //this.webRtcPeer.addTranscoder('video');
        //this.webRtcPeer.addTranscoder('audio');
        this.webRtcPeer.logger = null;
    }

    this.setPreview2 = function(url,house_id,camera_stream){//setPreview2
        console.log("SET PREVIEW2::::::::::::::::::: ");
        this.url			= url;
        this.address 		= url +"?house_id=" + house_id + "&machine_name=" + camera_stream + "&c2=1";
    }

    this.setPreview = function(url,house_id,camera_stream){
        console.log("SET PREVIEW::::::::::::::::::: ");
        this.url			= url;
        this.address 		= url +"?house_id=" + house_id + "&machine_name=" + camera_stream;
    };

    this.set = function(url,token,machine_index){
        console.log("SET GAMEPLAY::::::::::::::::::: ");
        this.url			= url;
        this.token 			= token;
        this.machine_index 	= machine_index;
        this.address 		= url +"?token=" + token + "&id=" + machine_index;
    };

    this.start = function(){
        console.log("player start");
        this.stop();
        this.status			= this.CONNECTING_WS;
        this.ws = new WebSocket(this.address);
        this.ws.onmessage 	= this.messageHandler;
        this.ws.onopen		= this.connectHandler;
    };

    this.startGameplayStream = function(){//test dacha
        console.log("player startGameplayStream");
        this.status			= this.CONNECTING_WS;
        this.ws = new WebSocket(this.address);
        this.ws.onmessage 	= this.messageHandler;
        this.ws.onopen		= this.connectHandler;
    };

    this.stop = function(){
        console.log("player stop ");
        if (me.webRtcPeer) {
            console.log('dispose webRtcPeer');
            me.webRtcPeer.dispose();
            me.webRtcPeer = null;
        }
        if(this.ws != null){
            console.log('dispose ws');
            this.ws.close();
            this.ws.onmessage 	= null;
            this.ws.onopen 		= null;
            this.ws.onclose		= null;
            this.ws				= null;
        }
        console.debug("player stop");
    }

    this.onIceCandidate = function(candidate){
        //console.log('Local candidate' + JSON.stringify(candidate));
        var message = {
            id : 'onIceCandidate',
            candidate : candidate
        };
        me.sendMessage(message);
    };

    this.onOffer = function(error, offerSdp) {

        if (error) {
            me.onError('Error generating the offer');
            return false;
        }
        console.info('Invoking SDP offer callback function ');
        var message = {
            id : 'start',
            //token : this.token,//test dacha
            token : alteastream.AbstractScene.GAME_TOKEN,
            sdpOffer : offerSdp
        };
        //console.debug(message)
        me.sendMessage(message);
    }
}
