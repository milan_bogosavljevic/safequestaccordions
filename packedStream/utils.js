
this.gamecore = this.gamecore || {};

(function() {
        "use strict";

var Utils = function() {};

// static private properties:
// static public properties:
// events:
// public properties:

    //NUMBER
    Utils.NUMBER = {};

/*    Utils.NUMBER.roundDecimal = function(value){
        return Math.round(value * 100) / 100;
    };*/

    Utils.NUMBER.roundDecimal = function(value){
        return (Math.round(value * 1e12) / 1e12);
    };

    Utils.NUMBER.floorDecimal = function(value){
        return (Math.floor(value * 1e12) / 1e12);
    };

    Utils.NUMBER.randomRange = function(minNumber, maxNumber) {
        return Math.round(Math.random()*(maxNumber - minNumber)) + minNumber;
    };

    Utils.NUMBER.addZeros = function(number,max){
        var fromPoint = String(number);
        var decimals = fromPoint.split('.');
        var zeros = "";
        if(decimals[1] === undefined){
            zeros=".00";
            if(max>1){
                zeros+="0";
            }
        }else{
            if(decimals[1].length===1){
                zeros="00";
            }
            if(decimals[1].length===max){
                zeros="0";
            }
        }
        return zeros;
    };



    //DISPLAY
    Utils.DISPLAY = {};

    Utils.DISPLAY.cacheObject = function(target){
        var rec = target.getBounds();
        target.cache(rec.x, rec.y, rec.width, rec.height);
    };

    Utils.DISPLAY.cacheText = function(target){
        var rec = target.getBounds();
        var newRec = {};
        switch(target.textAlign){
            case "left":
                newRec.x = 0;
                newRec.width = rec.width;
                break;
            case "center":
                newRec.x = rec.x - rec.width*0.5;
                newRec.width = rec.width*2;
                break;
            case "right":
                newRec.x = - rec.width;
                newRec.width = rec.width;
                break;
        }
        target.cache(newRec.x, rec.y, newRec.width, rec.height);
        //target.cache(newRec.x, rec.y, newRec.width, rec.height+target.yCacheAdd);// version for bitmapText
    };

    Utils.DISPLAY.updateCache = function(target){
        target.cache(0, 0, target.image.width, target.image.height);
        target.updateCache();
    };

    Utils.DISPLAY.setProps = function(tgt,props){
        for(var p in props)
            tgt[p] = props[p];
    };



    //FILTERS
    Utils.DISPLAY.FILTERS = {};

    Utils.DISPLAY.FILTERS.setHue = function(target,value){
        var hue = new createjs.ColorMatrixFilter(new createjs.ColorMatrix().adjustHue(value));
        Utils._applyFilter(target,hue);
    };

    Utils.DISPLAY.FILTERS.setBrightness = function(target,value){
        var brightness = new createjs.ColorMatrixFilter(new createjs.ColorMatrix().adjustBrightness(value));
        Utils._applyFilter(target,brightness);
    };

    Utils.DISPLAY.FILTERS.setSaturation = function(target,value){
        var saturation = new createjs.ColorMatrixFilter(new createjs.ColorMatrix().adjustSaturation(value));
        Utils._applyFilter(target,saturation);
    };

    Utils.DISPLAY.FILTERS.setColor = function(target,HSB){
        var color = new createjs.ColorMatrixFilter(new createjs.ColorMatrix().adjustHue(HSB.h).adjustSaturation(HSB.s).adjustBrightness(HSB.b));
        Utils._applyFilter(target,color);
    };

    Utils.DISPLAY.FILTERS.resetSaturation = function(target){// napraviti za konkretni filter
        Utils._removeAllFilters(target);
    };

    Utils.DISPLAY.FILTERS.resetBrightness = function(target){// napraviti za konkretni filter
        Utils._removeAllFilters(target);
    };

    Utils.DISPLAY.FILTERS.setBlur = function(target,yBlur){
        var blurFilter = new createjs.BlurFilter(yBlur, yBlur, 2);
        Utils._applyFilter(target,blurFilter);
    };




// private properties:
    Utils._applyFilter = function(target,filter){
        var width = target[0].image.width;
        var height = target[0].image.height;
        var length = target.length;
        var i = 0;
        for(i;i<length;i++){
            target[i].filters = [filter];
            target[i].cache(0, 0, width, height);
        }
    };

    Utils._removeAllFilters = function(target){
        var length = target.length;
        var i = 0;
        for(i;i<length;i++){
            target[i].uncache();
            target[i].filters = [];
        }
    };
// constructor:
// static methods:
    //createjs.Sound.activePlugin.toString();




    gamecore.Utils = Utils;
})();
