//Coinpusher v 1.1.6

// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var Bulbs = function(scene) {
    this.initialize_Bulbs(scene);
};
var p = Bulbs.prototype;

// static properties:
// public properties:
    p.scene = null;
    p.animate = true;
// private properties:
    p._bulbs = null;
// constructor:

    p.initialize_Bulbs = function(scene) {
        this.scene = scene;
        this._bulbs = [];
    };

// private methods:
    p._loop = function (scope) {
        if(scope.animate === true) {
            var rDel = gamecore.Utils.NUMBER.randomRange(2000, 7000);
            setTimeout(function () {
                scope.update();
            }, rDel);
        }else{
            scope.removeAllChildren();
            scope._bulbs = [];
        }
    };
// public methods:
    p.createBulb = function(xPos,yPos,colorRGB){
        var bulb = alteastream.Assets.getImage(alteastream.Assets.images.flare);
        this.scene.addChild(bulb);
        bulb.x = xPos;
        bulb.y = yPos;
        bulb.regX = bulb.image.height*0.5;
        bulb.regY = bulb.image.height*0.5;
        //bulb.compositeOperation = "screen";

        bulb.mouseEnabled = false;
        bulb.filters = [
            new createjs.ColorFilter(0, 0, 0, colorRGB.a, colorRGB.r, colorRGB.g, colorRGB.b)
        ];
        bulb.cache(0,0,bulb.image.width,bulb.image.height);
        this._bulbs.push(bulb);
    };

    p.update = function(){
        for(var i = 0,j = this._bulbs.length;i<j;i++){
            var bulb = this._bulbs[i];
            var rRot = bulb.rotation +250;
            var rScale = gamecore.Utils.NUMBER.randomRange(0.3,1);
            var rDel = gamecore.Utils.NUMBER.randomRange(0.1,0.3);
            TweenMax.to(bulb,0.2,{delay:rDel,rotation:rRot,scaleX:rScale,scaleY:rScale,alpha:1});
            TweenMax.to(bulb,0.2,{delay:0.2+rDel,onComplete:this._loop,onCompleteParams:[this],rotation:rRot+45,scaleX:0.1,scaleY:0.1,alpha:0});
        }
    };

    p.dispose = function(){
        this.animate = false;
    };

    alteastream.Bulbs = Bulbs;
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var LightBeams = function(scene) {
    this.initialize_LightBeams(scene);
};
var p = LightBeams.prototype;

// static properties:
// public properties:
    p.beamImage = null;
    p.scene = null;
// private properties:
    p._beamsArray = null;
// constructor:

    p.initialize_LightBeams = function(scene) {
        this.scene = scene;
        this._beamsArray = [];
    };

// private methods:
    p._loop = function (scope) {
        scope.update();
    };
// public methods:
    p.createBeam = function(xPos,yPos,fromSide,colorRGB){
        var beam = alteastream.Assets.getImage(alteastream.Assets.images.beam);
        this.scene.addChild(beam);
        beam.x = xPos;
        beam.y = yPos;
        beam.regX = 0;
        beam.regY = beam.image.height*0.5;
       // beam.compositeOperation = "screen";
        if(fromSide === 1){
            beam.scaleX*=-1;
            beam.rotation = -60;
        }else beam.rotation = 60;

        beam.side = fromSide;
        beam.mouseEnabled = false;
        beam.filters = [
            new createjs.ColorFilter(0, 0, 0, colorRGB.a, colorRGB.r, colorRGB.g, colorRGB.b)
        ];
        beam.cache(0,0,beam.image.width,beam.image.height);
        this._beamsArray.push(beam);
    };

    p.update = function(){
        for(var i = 0,j = this._beamsArray.length;i<j;i++){
            var beam = this._beamsArray[i];
            var leftOrRight = beam.side === 0?80:-80;
            var rRot = gamecore.Utils.NUMBER.randomRange(0,leftOrRight);
            TweenMax.to(beam,2,{ease:Sine.easeInOut,onComplete:this._loop,onCompleteParams:[this],rotation:rRot});
        }
    };

    p.showBeams = function(show) {
        for(var i = 0; i < this._beamsArray.length; i++) {
            this._beamsArray[i].visible = show;
        }
    };

    alteastream.LightBeams = LightBeams;
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LobbyAssets = function() {};
    var Assets = alteastream.Assets;

    LobbyAssets.count = 0;

    LobbyAssets.loadManifest = function(lang){
        Assets.setImageLang(lang);
        var noCache = "?ver="+Assets.VERSION;
        //var language = Assets.getImageLang();
        //var localePath = "assets/images/locale/" + language + "/";
        return [
            //images
            {id:Assets.images.beam, src:"assets/images/beam.png"+noCache},
            {id:Assets.images.flare, src:"assets/images/sparkle.png"+noCache},
            {id:Assets.images.window, src:"assets/images/window.png"+noCache},
            {id:Assets.images.windowList, src:"assets/images/windowList.png"+noCache},
            {id:Assets.images.window_big, src:"assets/images/window_big.png"+noCache},
            {id:Assets.images.machineBg, src:"assets/images/machineBg.png"+noCache},
            {id:Assets.images.machineOverlay, src:"assets/images/machineOverlay.png"+noCache},
            {id:Assets.images.thumb, src:"assets/images/thumb.png"+noCache},
            {id:Assets.images.noThumb, src:"assets/images/noThumb.png"+noCache},
            {id:Assets.images.spinner, src:"assets/images/spinner.png"+noCache},
            {id:Assets.images.mainBackground,src:"assets/images/mainBackground.jpg"+noCache},
            {id:Assets.images.mainBackgroundOverlay,src:"assets/images/mainBackgroundOverlay.png"+noCache},
            {id:Assets.images.mainBackgroundOverlay2,src:"assets/images/mainBackgroundOverlay2.png"+noCache},
            {id:Assets.images.infoBackground,src:"assets/images/infoBackground.jpg"+noCache},
            {id:Assets.images.infoPage1Image,src:"assets/images/infoPage1Image.png"+noCache},
            {id:Assets.images.infoPage2Image,src:"assets/images/infoPage2Image.png"+noCache},
            {id:Assets.images.infoPage3Image,src:"assets/images/infoPage3Image.png"+noCache},
            {id:Assets.images.mainLogo,src:"assets/images/logo.png"+noCache},
            {id:Assets.images.animatedLogo,src:"assets/images/gausel.png"+noCache},
            {id:Assets.images.floor,src:"assets/images/locations/floor.png"+noCache},
            {id:Assets.images.houseClosed,src:"assets/images/locations/houseClosed.png"+noCache},
            {id:Assets.images.house,src:"assets/images/locations/house.png"+noCache},
            {id:Assets.images.houseHighlight,src:"assets/images/locations/houseHighlight.png"+noCache},
            {id:Assets.images.smallHouse,src:"assets/images/locations/smallHouse.png"+noCache},
            {id:Assets.images.smallHouseCurrent,src:"assets/images/locations/smallHouseCurrent.png"+noCache},
            {id:Assets.images.lampGreen,src:"assets/images/lampGreen.png"+noCache},
            {id:Assets.images.lampGreenLight,src:"assets/images/lampGreenLight.png"+noCache},
            {id:Assets.images.lampYellow,src:"assets/images/lampYellow.png"+noCache},
            {id:Assets.images.lampYellowDark,src:"assets/images/lampYellowDark.png"+noCache},
            {id:Assets.images.lampRed,src:"assets/images/lampRed.png"+noCache},
            {id:Assets.images.listMachineBackground,src:"assets/images/listMachineBackground.png"+noCache},
            {id:Assets.images.noAvailableMachinesImg,src:"assets/images/noAvailableMachines.png"+noCache},

            // co-play prize win animation
            {id:Assets.images.chip_1_back, src:"assets/images/chip_1.png"+noCache},
            {id:Assets.images.chip_2_back, src:"assets/images/chip_2.png"+noCache},
            {id:Assets.images.chip_3_back, src:"assets/images/chip_3.png"+noCache},
            {id:Assets.images.chip_4_back, src:"assets/images/chip_4.png"+noCache},
            {id:Assets.images.chip_5_back, src:"assets/images/chip_5.png"+noCache},
            {id:Assets.images.chip_6_back, src:"assets/images/chip_6.png"+noCache},
            {id:Assets.images.coinWin, src:"assets/images/coinWin.png"+noCache},

            //gameTypesNew
            {id:Assets.images.ticketCircusType, src:"assets/images/locations/ticketCircusType.png"+noCache},
            {id:Assets.images.cashFestivalType, src:"assets/images/locations/cashFestivalType.png"+noCache},
            {id:Assets.images.diamondsAndPearlsType, src:"assets/images/locations/diamondsAndPearlsType.png"+noCache},
            //btns
            {id:Assets.images.btnArrowLeftSS,src:"assets/images/buttons/arrow_btn.png"+noCache},
            {id:Assets.images.btnStart,src:"assets/images/buttons/start_btn.png"+noCache},
            {id:Assets.images.btnBack,src:"assets/images/buttons/back_btn.png"+noCache},
            {id:Assets.images.btnBack2,src:"assets/images/buttons/back_btn2.png"+noCache},
            {id:Assets.images.btnBackForEntranceCounter,src:"assets/images/buttons/back_btn_for_counter.png"+noCache},
            {id:Assets.images.btnExitForQuitCounter,src:"assets/images/buttons/exit_btn_for_counter.png"+noCache},
            {id:Assets.images.btnParticipate,src:"assets/images/buttons/btnParticipate.png"+noCache},
            {id:Assets.images.btnStopParticipate,src:"assets/images/buttons/btnStopParticipate.png"+noCache},
            {id:Assets.images.btnStakeDec,src:"assets/images/buttons/stakeDec.png"+noCache},
            {id:Assets.images.btnStakeInc,src:"assets/images/buttons/stakeInc.png"+noCache},
            {id:Assets.images.btnInfoSS,src:"assets/images/buttons/infoSS.png"+noCache},
            {id:Assets.images.infoButtonLeft,src:"assets/images/buttons/btnInfoPagesLeft.png"+noCache},
            {id:Assets.images.infoButtonRight,src:"assets/images/buttons/btnInfoPagesRight.png"+noCache},
            {id:Assets.images.soundOn,src:"assets/images/buttons/sound_icon_default.png"+noCache},
            {id:Assets.images.soundOff,src:"assets/images/buttons/sound_icon_disabled.png"+noCache},
            {id:Assets.images.btnQuitSS,src:"assets/images/buttons/quitSS.png"+noCache},
            {id:Assets.images.btnFullScreen_On,src:"assets/images/buttons/btnFullScreen_On.png"+noCache},
            {id:Assets.images.btnFullScreen_Off,src:"assets/images/buttons/btnFullScreen_Off.png"+noCache},
            {id:Assets.images.btnPlayerActivity,src:"assets/images/buttons/btnPlayerActivity.png"+noCache},
            //sounds
            {id:Assets.sounds.confirm,src:"assets/sounds/confirm.ogg"+noCache},
            {id:Assets.sounds.welcomeMessageSpoken,src:"assets/sounds/welcome_message_01.ogg"+noCache},
            {id:Assets.sounds.error,src:"assets/sounds/error.ogg"+noCache},
            {id:Assets.sounds.errorSpoken,src:"assets/sounds/error_spoken_01.ogg"+noCache},
            {id:Assets.sounds.info,src:"assets/sounds/info.ogg"+noCache},
            {id:Assets.sounds.warning,src:"assets/sounds/warning.ogg"+noCache},
            {id:Assets.sounds.exception,src:"assets/sounds/exception.ogg"+noCache},
            {id:Assets.sounds.refund,src:"assets/sounds/refund.ogg"+noCache}
        ];
    };

    LobbyAssets.loadFontManifest = function(){
        return [
            "css/HurmeGeometricSans3.otf",
            "css/HurmeGeometricSans3 Light.otf",
            "css/HurmeGeometricSans3 Bold.otf",
            "css/NIAGSOL.TTF"
        ];
    };

    /*LobbyAssets.atlasImage = {};
    var aI = LobbyAssets.atlasImage = {};
    aI[Assets.atlasImage.ss] = "ss";*/

    LobbyAssets.texts = {};
    var t = LobbyAssets.texts["en"] = {};
    t[Assets.texts.win] = "WIN";
    t[Assets.texts.sessionDoesNotExist] = "Session does not exist";
    t[Assets.texts.casinoServiceUnreachable] = "CasinoServiceUnreachable";
    t[Assets.texts.infoText1] = "Select a machine to play directly, or participate in a game.";
    t[Assets.texts.infoText2] = "Currently under maintenance/error or offline.";
    t[Assets.texts.infoText3] = "Available for instant play.";
    t[Assets.texts.infoText4] = "Enter CO-PLAY and participate in active game while waiting your turn.";
    t[Assets.texts.infoText5] = "Actively participate on other players game by watching the live stream and betting on the outcome.\n\n" +
        "You are notified of number of players in CO-PLAY, and of your current position in the queue.\n\n" +
        "The list of all currently unoccupied machines (if any) is also available for you to start your instant play.\n\n" +
        "If you select 'BACK TO LOBBY', you will be returned to the lobby section and loose your queue position for selected machine.";
    t[Assets.texts.infoText6] = "Confirmation applet will prompt you to confirm your game play within 10 seconds.\n\n" +
        "If you don't comply timely, or you select 'BACK TO LOBBY', you will be returned to the lobby section and your pending game session will be cancelled";
    t[Assets.texts.infoText7] = "QUEUE";
    t[Assets.texts.infoText8] = "Here is the info for the last 24 hours, for more details please contact";

    //t = LobbyAssets.texts["sr"] = {};

    alteastream.LobbyAssets = LobbyAssets;
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LobbyMobileAssets = function() {};
    var Assets = alteastream.Assets;

    LobbyMobileAssets.count = 0;

    LobbyMobileAssets.loadManifest = function(lang){
        Assets.setImageLang(lang);
        //var language = Assets.getImageLang();
        //var localePath = "assets/mobile/images/locale/" + language + "/";
        var noCache = "?ver="+Assets.VERSION;

        return [
            //locale
            /*{id:Assets.imgTexts.drawTxtImg,src:localePath + "/drawTxt.png"+noCache},
             */
            //images
            {id:Assets.images.beam, src:"assets/mobile/images/beam.png"+noCache},
            {id:Assets.images.flare, src:"assets/mobile/images/sparkle.png"+noCache},
            {id:Assets.images.window, src:"assets/mobile/images/window.png"+noCache},
            {id:Assets.images.window_big, src:"assets/mobile/images/window_big.png"+noCache},
            {id:Assets.images.machineBg, src:"assets/mobile/images/machineBg.png"+noCache},
            {id:Assets.images.machineOverlay, src:"assets/mobile/images/machineOverlay.png"+noCache},
            {id:Assets.images.thumb, src:"assets/mobile/images/thumb.png"+noCache},
            {id:Assets.images.noThumb, src:"assets/mobile/images/noThumb.png"+noCache},
            {id:Assets.images.spinner, src:"assets/mobile/images/spinner.png"+noCache},
            {id:Assets.images.mainBackground,src:"assets/mobile/images/mainBackground.jpg"+noCache},
            {id:Assets.images.mainBackgroundOverlay,src:"assets/mobile/images/mainBackgroundOverlay.png"+noCache},
            {id:Assets.images.mainBackgroundOverlay2,src:"assets/mobile/images/mainBackgroundOverlay2.png"+noCache},
            {id:Assets.images.infoBackground,src:"assets/mobile/images/infoBackground.jpg"+noCache},
            {id:Assets.images.infoPage1Image,src:"assets/mobile/images/infoPage1Image.png"+noCache},
            {id:Assets.images.infoPage2Image,src:"assets/mobile/images/infoPage2Image.png"+noCache},
            {id:Assets.images.infoPage3Image,src:"assets/mobile/images/infoPage3Image.png"+noCache},
            {id:Assets.images.mainLogo,src:"assets/mobile/images/logo.png"+noCache},
            {id:Assets.images.animatedLogo,src:"assets/mobile/images/gausel.png"+noCache},
            {id:Assets.images.floor,src:"assets/mobile/images/locations/floor.png"+noCache},
            {id:Assets.images.houseClosed,src:"assets/mobile/images/locations/houseClosed.png"+noCache},
            {id:Assets.images.house,src:"assets/mobile/images/locations/house.png"+noCache},
            {id:Assets.images.houseHighlight,src:"assets/mobile/images/locations/houseHighlight.png"+noCache},
            {id:Assets.images.smallHouse,src:"assets/mobile/images/locations/smallHouse.png"+noCache},
            {id:Assets.images.smallHouseCurrent,src:"assets/mobile/images/locations/smallHouseCurrent.png"+noCache},
            {id:Assets.images.lampGreen,src:"assets/mobile/images/lampGreen.png"+noCache},
            {id:Assets.images.lampGreenLight,src:"assets/mobile/images/lampGreenLight.png"+noCache},
            {id:Assets.images.lampYellow,src:"assets/mobile/images/lampYellow.png"+noCache},
            {id:Assets.images.lampYellowDark,src:"assets/mobile/images/lampYellowDark.png"+noCache},
            {id:Assets.images.lampRed,src:"assets/mobile/images/lampRed.png"+noCache},
            {id:Assets.images.listMachineBackground,src:"assets/mobile/images/listMachineBackground.png"+noCache},
            {id:Assets.images.chip_1_back, src:"assets/mobile/images/chip_1.png"+noCache},
            {id:Assets.images.chip_2_back, src:"assets/mobile/images/chip_2.png"+noCache},
            {id:Assets.images.chip_3_back, src:"assets/mobile/images/chip_3.png"+noCache},
            {id:Assets.images.chip_4_back, src:"assets/mobile/images/chip_4.png"+noCache},
            {id:Assets.images.chip_5_back, src:"assets/mobile/images/chip_5.png"+noCache},
            {id:Assets.images.chip_6_back, src:"assets/mobile/images/chip_6.png"+noCache},
            {id:Assets.images.coinWin, src:"assets/mobile/images/coinWin.png"+noCache},
            {id:Assets.images.noAvailableMachinesImg,src:"assets/mobile/images/noAvailableMachines.png"+noCache},
            //gameTypesNew
            {id:Assets.images.ticketCircusType, src:"assets/mobile/images/locations/ticketCircusType.png"+noCache},
            {id:Assets.images.cashFestivalType, src:"assets/mobile/images/locations/cashFestivalType.png"+noCache},
            {id:Assets.images.diamondsAndPearlsType, src:"assets/mobile/images/locations/diamondsAndPearlsType.png"+noCache},
            //btns
            {id:Assets.images.btnArrowLeftSS,src:"assets/mobile/images/buttons/arrow_btn.png"+noCache},
            {id:Assets.images.btnStart,src:"assets/mobile/images/buttons/start_btn.png"+noCache},
            {id:Assets.images.btnBack,src:"assets/mobile/images/buttons/back_btn.png"+noCache},
            {id:Assets.images.btnBack2,src:"assets/mobile/images/buttons/back_btn2.png"+noCache},
            {id:Assets.images.btnBackForEntranceCounter,src:"assets/mobile/images/buttons/back_btn_for_counter.png"+noCache},
            {id:Assets.images.btnExitForQuitCounter,src:"assets/images/buttons/exit_btn_for_counter.png"+noCache},
            {id:Assets.images.btnParticipate,src:"assets/mobile/images/buttons/btnParticipate.png"+noCache},
            {id:Assets.images.btnStopParticipate,src:"assets/mobile/images/buttons/btnStopParticipate.png"+noCache},
            {id:Assets.images.btnStakeDec,src:"assets/mobile/images/buttons/stakeDec.png"+noCache},
            {id:Assets.images.btnStakeInc,src:"assets/mobile/images/buttons/stakeInc.png"+noCache},
            {id:Assets.images.infoButtonLeft,src:"assets/images/buttons/btnInfoPagesLeft.png"+noCache},
            {id:Assets.images.infoButtonRight,src:"assets/images/buttons/btnInfoPagesRight.png"+noCache},
            {id:Assets.images.btnInfoSS,src:"assets/mobile/images/buttons/infoSS.png"+noCache},
            {id:Assets.images.soundOn,src:"assets/mobile/images/buttons/sound_icon_default.png"+noCache},
            {id:Assets.images.soundOff,src:"assets/mobile/images/buttons/sound_icon_disabled.png"+noCache},
            {id:Assets.images.btnQuitSS,src:"assets/mobile/images/buttons/quitSS.png"+noCache},
            {id:Assets.images.btnPlayerActivity,src:"assets/mobile/images/buttons/btnPlayerActivity.png"+noCache},
            {id:Assets.images.btnHiddenMenu,src:"assets/mobile/images/buttons/hiddenmenu_btn.png"+noCache},
            {id:Assets.images.btnHiddenMenuTransparent,src:"assets/mobile/images/buttons/hiddenmenu_btn_transparent.png"+noCache},
            {id:Assets.images.btnHiddenMenuTransparentBlack,src:"assets/mobile/images/buttons/hiddenmenu_btn_transparent_black.png"+noCache},
            //sounds
            {id:Assets.sounds.confirm,src:"assets/sounds/confirm.ogg"+noCache},
            {id:Assets.sounds.welcomeMessageSpoken,src:"assets/sounds/welcome_message_01.ogg"+noCache},
            {id:Assets.sounds.error,src:"assets/sounds/error.ogg"+noCache},
            {id:Assets.sounds.errorSpoken,src:"assets/sounds/error_spoken_01.ogg"+noCache},
            {id:Assets.sounds.info,src:"assets/sounds/info.ogg"+noCache},
            {id:Assets.sounds.warning,src:"assets/sounds/warning.ogg"+noCache},
            {id:Assets.sounds.exception,src:"assets/sounds/exception.ogg"+noCache},
            {id:Assets.sounds.refund,src:"assets/sounds/refund.ogg"+noCache}
        ];
    };

    LobbyMobileAssets.loadFontManifest = function(){
        return [
            "css/HurmeGeometricSans3.otf",
            "css/HurmeGeometricSans3 Light.otf",
            "css/HurmeGeometricSans3 Bold.otf",
            "css/NIAGSOL.TTF"
        ];
    };

    /*LobbyMobileAssets.atlasImage = {};
    var aI = LobbyMobileAssets.atlasImage = {};
    aI[Assets.atlasImage.ss] = "ss";*/

    LobbyMobileAssets.texts = {};
    var t = LobbyMobileAssets.texts["en"] = {};
    t[Assets.texts.win] = "WIN";
    t[Assets.texts.sessionDoesNotExist] = "Session does not exist";
    t[Assets.texts.casinoServiceUnreachable] = "CasinoServiceUnreachable";
    t[Assets.texts.infoText1] = "Select a machine to play directly, or participate in a game.";
    t[Assets.texts.infoText2] = "Currently under maintenance/error or offline.";
    t[Assets.texts.infoText3] = "Available for instant play.";
    t[Assets.texts.infoText4] = "Enter CO-PLAY and participate in active game while waiting your turn.";
    t[Assets.texts.infoText5] = "Actively participate on other players game by watching the live stream and betting on the outcome.\n\n" +
        "You are notified of number of players in CO-PLAY, and of your current position in the queue.\n\n" +
        "The list of all currently unoccupied machines (if any) is also available for you to start your instant play.\n\n" +
        "If you select 'BACK TO LOBBY', you will be returned to the lobby section and loose your queue position for selected machine.";
    t[Assets.texts.infoText6] = "Confirmation applet will prompt you to confirm your game play within 10 seconds.\n\n" +
        "If you don't comply timely, or you select 'BACK TO LOBBY', you will be returned to the lobby section and your pending game session will be cancelled";
    t[Assets.texts.infoText7] = "QUEUE";
    t[Assets.texts.infoText8] = "Here is the info for the last 24 hours, for more details please contact";

    //t = LobbyMobileAssets.texts["sr"] = {};

    alteastream.LobbyMobileAssets = LobbyMobileAssets;
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var CarouselUI = function(maxIndex , lobby){
        this.Container_constructor();
        this.initCarouselUI(maxIndex,lobby);
    };

    var p = createjs.extend(CarouselUI,createjs.Container);

    // private properties
    p._currentIndex = null;
    p._maxIndex = null;
    p._currentIndexText = null;
    // constructor
    p.initCarouselUI = function (maxIndex , lobby) {
        var that = this;

        this._currentIndex = 0;
        this._maxIndex = maxIndex;

        var leftButton = alteastream.Assets.getImage(alteastream.Assets.images.leftButton);
        leftButton.x = 0;
        leftButton.y = 0;
        this.addChild(leftButton);

        var rightButton = alteastream.Assets.getImage(alteastream.Assets.images.rightButton);
        rightButton.x = 250;
        rightButton.y = 0;
        this.addChild(rightButton);

        leftButton.addEventListener("click" , function () {
            if(that._currentIndex > 0){
                that._currentIndex--;
                that._updateCurrentIndexText();
                lobby.animateSwitch(that._currentIndex , "left");
            }
        });

        rightButton.addEventListener("click" , function () {
            if(that._currentIndex < that._maxIndex){
                that._currentIndex++;
                that._updateCurrentIndexText();
                lobby.animateSwitch(that._currentIndex , "right");
            }
        });

        var currentHouseIndexText = this._currentIndexText = new alteastream.BitmapText("" , "36px HurmeGeometricSans3", "#fff",{textAlign:"right",x:140,y:20});
        var numberOfHousesText = new alteastream.BitmapText("/ "+(this._maxIndex+1) , "36px HurmeGeometricSans3", "#fff",{textAlign:"left",x:155,y:20});

        this.addChild(currentHouseIndexText,numberOfHousesText);

        this._updateCurrentIndexText(); // pravi problem ako se bilo sta setuje od align-a sem left , zato stoji ovaj poziv
    };
    // private methods
    p._updateCurrentIndexText = function(){
        this._currentIndexText.setText(this._currentIndex+1);
    };
    // public methods
    p.enableClick = function(bool){
        this.mouseEnabled = bool;
    };
    
    alteastream.CarouselUI = createjs.promote(CarouselUI,"Container");
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var HouseSelection = function (houses) {
        this.Container_constructor();
        this.initialize_HouseSelection(houses);
    };

    var p = createjs.extend(HouseSelection,createjs.Container);

    // static properties:
    // events:
    // private vars:
    // public vars:
    var _this = null;
    // public properties
    p.spacing = 0;
    // private properties
    p._bigHousesCont = null;
    p._scroller = null;
    p._pagination = null;

    // constructor:
    p.initialize_HouseSelection = function (houses) {
        console.log("HouseSelection Initialized");
        _this = this;
        this.spacing = 750;
        this.name = "CAROUSEL";
        this.visible = false;

        var housesContainer = this._bigHousesCont = new createjs.Container();
        this._setup(houses);
        var scroller = this._scroller = new alteastream.Scroller(this._bigHousesCont);
        var scrollLevels = [];
        for(var i = 0; i < houses.length; i++){
            scrollLevels.push(i * this.spacing * -1);
        }

        scroller.setScrollType("scrollByPage");
        scroller.setScrollDirection("x");
        scroller.setRowsPerPage(1);
        scroller.setNumberOfPages(houses.length);
        scroller.setScrollLevels(scrollLevels);

        var dotImage = alteastream.Assets.getImage(alteastream.Assets.images.smallHouse);
        var paginationOptions = {
            numberOfPages:houses.length,
            scroller:scroller,
            customDot:"smallHouse",
            customCurrentDot:"smallHouseCurrent",
            dotFont:"16px HurmeGeometricSans3",
            currentDotFont:null,
            dotsSpacing:25,
            dotsWidth:dotImage.image.width
        };

        dotImage = null;

        var pagination = this._pagination = new alteastream.Pagination(paginationOptions);

        var leftButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnArrowLeftSS),3);
        leftButton.setDisabled(true);

        var rightButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnArrowLeftSS),3);
        rightButton.rotation = 180;

        var lX = -175;
        var rX = pagination.getPaginationWidth() + 100;

        pagination.setUpButton(leftButton, lX, -(leftButton._height*0.5), scroller);
        pagination.setDownButton(rightButton, rX, (rightButton._height*0.5), scroller);
        pagination.removeTextFromCurrentDot();

        var paginationTexts = [];
        for(var j = 0; j < this._bigHousesCont.numChildren; j++){
            paginationTexts.push(this._bigHousesCont.getChildAt(j).name);
        }

        pagination.adjustDotsTextFields(0,-50,paginationTexts);
        pagination.textDownPosition = -50;
        pagination.textUpPosition = -60;
        pagination.x = - (pagination.getPaginationWidth()/2) + (pagination._dotWidth/2);
        pagination.y = (alteastream.AbstractScene.GAME_HEIGHT/2) - 50;
        pagination._dotsContainer.getChildAt(0).scale = 1.2;
        pagination._currentDot.scale = 1.2;
        pagination.showPagination(houses.length > 1);
        pagination.setShorAndLongTexts();

        scroller.plugInPagination(pagination);
        scroller.setPreAnimateScrollMethod(function () {
            _this.clicksEnabled(false);
            scroller.manageButtonsState();
            scroller.updateCurrentDotPosition();
            pagination.doMovingDotScaleAnimation(scroller.scrollByPageCurrentLevel)
        });
        scroller.setPostAnimateScrollMethod(function () {
            _this.enableSelectedHouse();
            _this.clicksEnabled(true);
        });

        this.addChild(housesContainer/*, pagination*/);
    };
    // private methods:
    p._setup =  function(houses) {
        for (var i=0; i < houses.length; i++){
            var location = new alteastream.Location(houses[i]);
            //location.x = i * this.spacing;
            location.y = i * 50;
            this._bigHousesCont.addChild(location);
        }



        /*        var houseToEnable = this._bigHousesCont.getChildAt(0);
                if(houseToEnable.enabled){
                    houseToEnable.allow = true;
                }*/

        this.visible = true;
    };

    // public methods:
    p.onSwipe = function (direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    p.showChosenHouse = function(bool) {//map.selectedLocation
        //var ind = this._scroller.scrollByPageCurrentLevel;
        //var house = this._bigHousesCont.getChildAt(ind);
        //house.visible = bool;

        var selectedLocation = this.parent.lobby.selectedLocation;
        var ind = this._bigHousesCont.getChildIndex(selectedLocation);

        if(ind !== this._bigHousesCont.numChildren - 1){
            for(var i = ind+1; i < this._bigHousesCont.numChildren; i++){
                var house = this._bigHousesCont.getChildAt(i);
                house.y += 280;
            }
        }
    };

    p.removeHighlight = function() {
        for(var i = 0; i < this._bigHousesCont.numChildren; i++){
            var location = this._bigHousesCont.getChildAt(i);
            if(location.enabled === true){
                location.highlight(false);
            }
        }
    };

    p.enableSelectedHouse = function() {
        /*        for(var i = 0; i < this._bigHousesCont.numChildren; i++){
                    var house = this._bigHousesCont.getChildAt(i);
                    house.allow = false;
                }*/
        /*        var ind = this._scroller.scrollByPageCurrentLevel;
                var houseToEnable = this._bigHousesCont.getChildAt(ind)
                if(houseToEnable.enabled){
                    houseToEnable.allow = true;
                }*/
    };

    p.clicksEnabled = function(bool) {
        this.mouseEnabled = bool;
    };

    p.getLocation = function(id){
        for(var i = 0; i < this._bigHousesCont.numChildren; i++){
            var locationCont = this._bigHousesCont.getChildAt(i);
            if(locationCont.id === String(id)){
                return locationCont;
            }
        }
    };

    p.storeLocationPosition  = function() {
        window.localStorage.setItem('locationPosition', String(this._scroller.scrollByPageCurrentLevel));
    };

    p.restoreLocationPosition = function() {
        var storedLocationPosition = Number(window.localStorage.getItem('locationPosition'));
        if(storedLocationPosition === this._scroller.scrollByPageCurrentLevel){//todo mozda da se proveri samo dali je 0
            return;
        }
        this._scroller.scrollByPageCurrentLevel = storedLocationPosition;
        this._scroller.scrollByPage(false);
    };

    p.adjustMobile = function(){
        this.x = 448;
        this.y = -45;
        this.spacing = 375;
        var scrollLevels = [];
        var R = this._pagination._dotWidth = this._pagination._dotsContainer.getChildAt(0).getChildAt(0).image.width;
        var spacing = this._pagination._dotSpacing = 30;
        this._pagination._setComponentWidth();
        this._pagination.textDownPosition = -28;
        this._pagination.textUpPosition = -38;
        for (var i=0; i < this._bigHousesCont.numChildren; i++){
            var houseCont = this._bigHousesCont.getChildAt(i);
            houseCont.x = i * this.spacing;

            var locationText = houseCont.getChildAt(1);
            //locationText.font = "35px NIAGSOL";
            locationText.font = locationText.text.length<14?"35px NIAGSOL":"25px NIAGSOL";
            locationText.y = -52;

            var smallLocationCont = this._pagination._dotsContainer.getChildAt(i);
            smallLocationCont.x = (i * R) + (spacing * i);

            var smallLocationText = smallLocationCont.getChildAt(1);
            if(smallLocationText){
                smallLocationText.font = "13px HurmeGeometricSans3";
                smallLocationText.y = i > 0 ? this._pagination.textDownPosition : this._pagination.textUpPosition;
            }

            scrollLevels.push(i * this.spacing * -1);
        }
        this._pagination.x = - (this._pagination.getPaginationWidth()/2) + (R/2);
        this._pagination.y = (alteastream.AbstractScene.GAME_HEIGHT/2) - 15;
        this._pagination.hideUI(true);
        this._scroller.setScrollLevels(scrollLevels);
    };

    alteastream.HouseSelection = createjs.promote(HouseSelection,"Container");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var Info = function(){
        this.Container_constructor();
        this._initInfo();
    };

    var p = createjs.extend(Info,createjs.Container);
    // private properties
    p._currentPage = 0;
    p._pages = null;
    // constructor
    p._initInfo = function () {
        var _this = this;
        this._pages = [];

        var background = alteastream.Assets.getImage(alteastream.Assets.images.infoBackground);

        var left = this._leftButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.infoButtonLeft),3);
        left.x = 1665;
        left.y = 165;
        left.setClickHandler(function () {
            if(_this._currentPage > 0){
                _this._currentPage--;
                _this._buttonsHandler();
            }
        });
        left.setDisabled(true);

        var right = this._rightButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.infoButtonRight),3);
        right.x = 1750;
        right.y = 165;
        right.setClickHandler(function () {
            if(_this._currentPage < _this._pages.length - 1){
                _this._currentPage++;
                _this._buttonsHandler();
            }
        });

        var fontHeader = "20px HurmeGeometricSans3";
        var fontHeader2 = "18px HurmeGeometricSans3";
        var fontText = "16px HurmeGeometricSans3";

        var page1 = new createjs.Container();

        var welcomeText = new createjs.Text("Welcome" , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:111, y:100, mouseEnabled:false});

        var headerShape = new createjs.Shape();
        headerShape.graphics.beginFill("#d60168").drawRect(111, 164, 182, 45);
        headerShape.cache(111, 164, 182, 45);

        var textSectionBackground = new createjs.Shape();
        textSectionBackground.graphics.beginFill("#000000").drawRect(1071, 270, 733, 540);
        textSectionBackground.alpha = 0.6;
        textSectionBackground.cache(1071, 270, 733, 540);
        
        this.addChild(background,welcomeText,headerShape,textSectionBackground,left,right);

        var page1Image = alteastream.Assets.getImage(alteastream.Assets.images.infoPage1Image);
        page1Image.x = 111;
        page1Image.y = 270;

        var page1Header = new createjs.Text("Lobby Section" , fontHeader, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:135, y:188, mouseEnabled:false});
        var page1Header2 = new createjs.Text("Lobby Section" , fontHeader2, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1166, y:330, mouseEnabled:false});

        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText1);
        var lobbySectionDescription = new createjs.Text(textSrc , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1166, y:375, mouseEnabled:false, lineWidth:630});

        var lampRed = alteastream.Assets.getImage(alteastream.Assets.images.lampRed);
        lampRed.x = 1166;
        lampRed.y = 436;
        lampRed.scale = 1.2;

        var lampRedLabel = new createjs.Text("Unavailable machine" , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1208, y:lampRed.y+8, mouseEnabled:false});

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText2);
        var lampRedDescription = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left", textBaseline:"middle", x:1166, y:lampRed.y+54, mouseEnabled:false, lineWidth:630});

        var lampGreen = alteastream.Assets.getImage(alteastream.Assets.images.lampGreen);
        lampGreen.x = 1166;
        lampGreen.y = 548;
        lampGreen.scale = 1.2;

        var lampGreenLabel = new createjs.Text("Free machine" , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1208, y:lampGreen.y+8, mouseEnabled:false});

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText3);
        var lampGreenDescription = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left", textBaseline:"middle", x:1166, y:lampGreen.y+54, mouseEnabled:false, lineWidth:630});

        var lampYellow = alteastream.Assets.getImage(alteastream.Assets.images.lampYellow);
        lampYellow.x = 1166;
        lampYellow.y = 653;
        lampYellow.scale = 1.2;

        var lampYellowLabel = new createjs.Text("Occupied machine" , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1208, y:lampYellow.y+8, mouseEnabled:false});

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText4);
        var lampYellowDescription = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left", textBaseline:"middle", x:1166, y:lampYellow.y+54, mouseEnabled:false, lineWidth:630,lineHeight:28});

        page1.addChild(page1Image,page1Header,page1Header2,lobbySectionDescription,lampRed,lampRedLabel,lampRedDescription,lampGreen,lampGreenLabel,lampGreenDescription,lampYellow,lampYellowLabel,lampYellowDescription);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var page2 = new createjs.Container();

        var page2Image = alteastream.Assets.getImage(alteastream.Assets.images.infoPage2Image);
        page2Image.x = 111;
        page2Image.y = 270;

        var page2Header = new createjs.Text("Co-Play" , fontHeader, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:165, y:188, mouseEnabled:false});
        var page2Header2 = new createjs.Text("Co-Play" , fontHeader2, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1166, y:365, mouseEnabled:false});//y:360 original position

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText5);
        var page2Text = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left", textBaseline:"middle", x:1166, y:410, mouseEnabled:false, lineWidth:630,lineHeight:28});//y:405 original position

        page2.addChild(page2Image,page2Header,page2Header2,page2Text);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var page3 = new createjs.Container();

        var page3Image = alteastream.Assets.getImage(alteastream.Assets.images.infoPage3Image);
        page3Image.x = 111;
        page3Image.y = 270;

        var page3Header = new createjs.Text("Game Entrance" , fontHeader, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:132, y:188, mouseEnabled:false});
        var page3Header2 = new createjs.Text("Game Entrance" , fontHeader2, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1166, y:450, mouseEnabled:false});

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText6);
        var page3Text = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left",textBaseline:"middle", x:1166, y:510, mouseEnabled:false, lineWidth:630,lineHeight:28});

        page3.addChild(page3Image,page3Header,page3Header2,page3Text);

        this._pages = [page1,page2,page3];
        this.addChild(page1,page2,page3);

        page2.visible = false;
        page3.visible = false;

        this.regX = background.image.width/2;
        this.regY = background.image.height/2;

        this.preventClickThrough(true);
    };
    // private methods
    p._buttonsHandler = function(){
        this._leftButton.setDisabled(this._currentPage < 1);
        this._rightButton.setDisabled(this._currentPage === this._pages.length - 1);
        this._managePagesVisibility();
    };

    p._managePagesVisibility = function(){
        for(var i = 0; i < this._pages.length; i++){
            this._pages[i].visible = i === this._currentPage;
        }
    };
    // public methods
    p.resetCurrentPage = function(){
        this._currentPage = 0;
        this._buttonsHandler();
    };

    p.preventClickThrough = function(bool){
        var method = bool === true? "on":"off";
        this[method]("click",function noClick(e){});
    };


    p.adjustMobile = function(){
        var fontHeader = "16px Roboto";
        var fontText = "16px Roboto";

        this._leftButton.x = 780;
        this._leftButton.y = 415;

        this._rightButton.x = 855;
        this._rightButton.y = 415;

        var welcomeTxt = this.getChildAt(1);
        welcomeTxt.font = fontHeader;
        welcomeTxt.x = 55;
        welcomeTxt.y = 50;

        var headerShape = this.getChildAt(2);
        headerShape.uncache();
        headerShape.graphics.clear();
        headerShape.graphics.beginFill("#d60168").drawRect(55, 82, 122, 26);
        headerShape.cache(55, 82, 122, 26);

        var textSectionBackground = this.getChildAt(3);
        textSectionBackground.uncache();
        textSectionBackground.graphics.clear();
        textSectionBackground.graphics.beginFill("#000000").drawRect(535, 135, 366, 270);
        textSectionBackground.cache(535, 135, 366, 270);

        var page1 = this._pages[0];

        var page1Image = page1.getChildAt(0);
        page1Image.x = 55;
        page1Image.y = 135;

        var page1Header = page1.getChildAt(1);
        page1Header.font = fontHeader;
        page1Header.x = 65;
        page1Header.y = 96;

        var xCorrection = 15;
        var lobbySectionDescription = page1.getChildAt(3);
        lobbySectionDescription.font = fontText;
        lobbySectionDescription.x = 583 - xCorrection;
        lobbySectionDescription.y = 157;
        lobbySectionDescription.lineWidth = 315;

        var lampRed = page1.getChildAt(4);
        lampRed.scale = 0.6;
        lampRed.x = 583 - xCorrection;
        lampRed.y = 208;

        var lampRedLabel = page1.getChildAt(5);
        lampRedLabel.x = 604 - xCorrection;
        lampRedLabel.y = 217;
        lampRedLabel.font = fontText;

        var lampRedDescription = page1.getChildAt(6);
        lampRedDescription.x = 583 - xCorrection;
        lampRedDescription.y = 236;
        lampRedDescription.font = fontText;
        lampRedDescription.color = "#afafaf";

        var lampGreen = page1.getChildAt(7);
        lampGreen.scale = 0.6;
        lampGreen.x = 583 - xCorrection;
        lampGreen.y = 264;

        var lampGreenLabel = page1.getChildAt(8);
        lampGreenLabel.x = 604 - xCorrection;
        lampGreenLabel.y = 272;
        lampGreenLabel.font = fontText;

        var lampGreenDescription = page1.getChildAt(9);
        lampGreenDescription.x = 583 - xCorrection;
        lampGreenDescription.y = 291;
        lampGreenDescription.font = fontText;
        lampGreenDescription.color = "#afafaf";
        lampGreenDescription.lineHeight = 16;

        var lampYellow = page1.getChildAt(10);
        lampYellow.scale = 0.6;
        lampYellow.x = 583 - xCorrection;
        lampYellow.y = 316;

        var lampYellowLabel = page1.getChildAt(11);
        lampYellowLabel.x = 604 - xCorrection;
        lampYellowLabel.y = 325;
        lampYellowLabel.font = fontText;
        lampYellowLabel.lineHeight = 16;

        var lampYellowDescription = page1.getChildAt(12);
        lampYellowDescription.x = 583 - xCorrection;
        lampYellowDescription.y = 343;
        lampYellowDescription.font = fontText;
        lampYellowDescription.color = "#afafaf";
        lampYellowDescription.lineWidth = 315;
        lampYellowDescription.lineHeight = 16;

        page1.removeChildAt(2);
//////////////////////////////////////////////////////////////////////////////////
        var page2 = this._pages[1];

        var page2Image = page2.getChildAt(0);
        page2Image.x = 55;
        page2Image.y = 135;

        var page2Header = page2.getChildAt(1);
        page2Header.font = fontHeader;
        page2Header.x = 87;
        page2Header.y = 96;

        var page2Text = page2.getChildAt(3);
        page2Text.x = 568;
        page2Text.y = 153;
        page2Text.lineWidth = 315;
        page2Text.lineHeight = 16;
        page2Text.font = fontText;
        page2Text.color = "#afafaf";
        page2.removeChildAt(2);
//////////////////////////////////////////////////////////////////////////////////
        var page3 = this._pages[2];

        var page3Image = page3.getChildAt(0);
        page3Image.x = 55;
        page3Image.y = 135;

        var page3Header = page3.getChildAt(1);
        page3Header.font = fontHeader;
        page3Header.x = 61;
        page3Header.y = 96;

        var page3Text = page3.getChildAt(3);
        page3Text.x = 568;
        page3Text.y = 220;
        page3Text.font = fontText;
        page3Text.lineWidth = 315;
        page3Text.lineHeight = 16;

        page3Text.color = "#afafaf";
        page3.removeChildAt(2);
    }

    alteastream.Info = createjs.promote(Info,"Container");
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Lobby = function() {
        this.AbstractScene_constructor();
        this.initialize_Lobby();
    };
    var p = createjs.extend(Lobby, alteastream.AbstractScene);

    // static properties:
    // events:
    // public properties:
    p.selectedLocation = null;
    p.currentMap = null;
    p.currentLocationMachines = null;
    p.beams = null;
    p.btnFs = null;
    p.btnSound = null;
    p.btnInfo = null;
    p.btnQuit = null;
    p.currency = null;
    p.mouseWheel = null;
    p.balanceBackground = null;
    // private properties:
    p._currentFocused = null;
    p._housesArr = null;
    p._info = null;
    p._logo = null;
    p._playerActivityPanel = null;
    var _instance = null;

    // constructor:
    p.initialize_Lobby = function() {
        console.log("LOBBY INIT");
        _instance = this;


        this.monitorNetworkStatus();
        this.setCommunicator();

    };
    // static methods:
    Lobby.getInstance = function(){
        return _instance;
    };

    // private methods
    p._setBalance = function() {
        var bgShape = this.balanceBackground = new createjs.Shape();
        bgShape.graphics.beginFill("#000000").drawRoundRect(0, 0, 233, 90, 5);
        bgShape.cache(0, 0, 233, 90);
        bgShape.x = 120;
        bgShape.y = 976;
        bgShape.mouseEnabled = false;
        bgShape.alpha = 0.7;
        bgShape.visible = false;

        var _startCurrency = "--";
        var lobbyBalanceLabel = this.lobbyBalanceLabel = new createjs.Text("BALANCE ( "+_startCurrency+" ) ","15px HurmeGeometricSans3","#a39f9e").set({textAlign:"center", textBaseline:"middle", mouseEnabled:false, x:240, y:1010, visible:true});
        var lobbyBalanceAmount = this.lobbyBalanceAmount = new createjs.Text("0.00","bold 24px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", mouseEnabled:false, x:lobbyBalanceLabel.x, y:lobbyBalanceLabel.y+23, visible:true});//y:820
        this.addChild(bgShape, lobbyBalanceLabel, lobbyBalanceAmount);
    };

    p._setComponents = function(response){
        var mainBackground = this.mainBackground = alteastream.Assets.getImage(alteastream.Assets.images.mainBackground);

        var backgroundOverlay = this.mainBackgroundOverlay = alteastream.Assets.getImage(alteastream.Assets.images.mainBackgroundOverlay);
        backgroundOverlay.mouseEnabled = false;

        var backgroundOverlay2 = this.mainBackgroundOverlay2 = alteastream.Assets.getImage(alteastream.Assets.images.mainBackgroundOverlay2);
        backgroundOverlay2.mouseEnabled = false;
        backgroundOverlay2.y = alteastream.AbstractScene.GAME_HEIGHT - backgroundOverlay2.image.height;

        var logo = this._logo = alteastream.Assets.getImage(alteastream.Assets.images.mainLogo);
        logo.mouseEnabled = false;
        logo.x = (alteastream.AbstractScene.GAME_WIDTH/2) - (logo.image.width/2);
        logo.y = -8;

        var currentMap = this.currentMap = new alteastream.ABSMap(this);//TEMP ALL

        currentMap.x =64;
        currentMap.y = 600;
        currentMap.name = "circus";
        currentMap.visible = alteastream.AbstractScene.BACK_TO_HOUSE === -1;

        this.setFocused(currentMap);

        if (Object.keys(response).length === 0 && response.constructor === Object) {//TEMP CHECK
            this.addChild(mainBackground, currentMap, logo, this.currentLocationMachines);
            this.throwAlert(alteastream.Alert.ERROR,"No Shops detected",function(){
                _instance.onQuit();
            });
            return;
        } else {
            this.setMap(response);
            this.setCurrentLocationMachines();
            this.addChild(mainBackground, currentMap, this.currentLocationMachines);
        }

        currentMap.addObserver(new alteastream.MouseObserver(this,"clickObserver","click"));

        this._setCarousel();
        this._setInfo();
        this._setPlayerActivity();
        this._setTopButtons();
        this._setEffects();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5+460,36);
        this._setBalance();

        this.mouseWheel = new alteastream.MouseWheel();
        this.mouseWheel.setTarget(this.currentLocationMachines);
    };

    /*p._setPlayerActivity = function() {
    _instance._playerActivityPanel = new alteastream.PlayerActivityPanel();
    _instance.addChild(_instance._playerActivityPanel);
    _instance._playerActivityPanel.y-=30;
    _instance._playerActivityPanel.visible = false;
    _instance._playerActivityPanel.preventClickThrough(true);
};

p._getPlayerActivity = function(){
    // temp update!!!!
    _instance.requester.getPlayerActivity(function (response) {
        _instance._playerActivityPanel.setPanel(response);
    });
    var activityInterval = setInterval(function(){
        _instance.requester.getPlayerActivity(function (response) {
            _instance._playerActivityPanel.setPanel(response);
        });
    },5000);

    setTimeout(function(){
        clearInterval(activityInterval);
    },30000);
};*/

    p._setPlayerActivity = function() {
        _instance._playerActivityPanel = new alteastream.PlayerActivityPanel();
        _instance.addChild(_instance._playerActivityPanel);
        _instance._playerActivityPanel.y-=30;
        _instance._playerActivityPanel.visible = false;
        _instance._playerActivityPanel.preventClickThrough(true);
    };

    p._getPlayerActivity = function(){
        _instance.requester.getPlayerActivity(function (response) {
            if(response.length){
                _instance._playerActivityPanel.setPanel(response);
            }else{
                _instance._startPlayerActivityInterval();
            }
        });
    };

    p._startPlayerActivityInterval = function() {
        console.log("nema response, startuje interval");
        var attempt = 0;
        var activityInterval = setInterval(function(){
            _instance.requester.getPlayerActivity(function (response) {
                //console.log("pokusaj " + attempt);
                if(response.length){
                    //console.log("sad ima response");
                    _instance._playerActivityPanel.setPanel(response);
                    clearInterval(activityInterval);
                }else{
                    //console.log("nema response");
                    attempt++;
                    if(attempt === 6){
                        clearInterval(activityInterval);
                        //_instance.throwAlert(alteastream.Alert.ERROR,"Activity log not available");
                    }
                }
            });
        },5000);
    };

    p._setEffects = function(){
        var beams = this.beams = new alteastream.LightBeams(this);
        beams.createBeam(0,-20,0,{r:255,g:0,b:0,a:1});
        beams.createBeam(30,-20,0,{r:255,g:255,b:0,a:1});

        beams.createBeam(1920,-20,1,{r:0,g:255,b:0,a:1});
        beams.createBeam(1950,-20,1,{r:255,g:0,b:255,a:1});
        beams.update();

        this.bulbsContainer = new createjs.Container();
        this.addChild(this.bulbsContainer);
        this.bulbsContainer.mouseEnabled = false;
        this.bulbsContainer.mouseChildren = false;
        var util = gamecore.Utils.NUMBER;
        for(var i=0,j=30;i<j;i++){
            var xp = util.randomRange(20,1900);
            var yp = util.randomRange(20,250);
            var bulbs = new alteastream.Bulbs(this.bulbsContainer);
            bulbs.createBulb(xp,yp,{r:255,g:255,b:255,a:1});
            bulbs.update();
        }
    };

    p._setCarousel = function() {
        this.currentLocationMachines._machinesComponent.addAcordions(this._housesArr);
    };

    p._setInfo = function() {
        var info = this._info = new alteastream.Info();
        info.visible = false;
        info.x = alteastream.AbstractScene.GAME_WIDTH/2;
        info.y = alteastream.AbstractScene.GAME_HEIGHT/2;
        this.addChild(info);
    };

    p._showInfoOnStart = function(bool){
        this._info.scaleX = this._info.scaleY = bool===true? 1:0;

        this.btnInfo.x = bool===true? 1764: this.btnInfo.originalXPosition;
        this.btnFs.visible = this.btnSound.visible = this.btnQuit.visible = this.btnPlayerActivity.visible = !bool;
    };

    p._setTopButtons = function() {
        var assets = alteastream.Assets;
        var yPos = 10;

        var btnQuit = this.btnQuit = new alteastream.ImageButton(assets.getImageURI(assets.images.btnQuitSS),3);
        var width = btnQuit.width;
        var spacing = 2;

        btnQuit.x = alteastream.AbstractScene.GAME_WIDTH - 164;
        btnQuit.y = yPos;
        btnQuit.setClickHandler(_instance.btnQuitHandler);
        this.addChild(btnQuit);

        var btnInfo = this.btnInfo =  new alteastream.ImageButton(assets.getImageURI(assets.images.btnInfoSS),3);
        btnInfo.x = btnQuit.x - width - spacing;
        btnInfo.y = yPos;
        btnInfo.originalXPosition = btnInfo.x;
        btnInfo.setClickHandler(_instance.btnInfoHandler);
        this.addChild(btnInfo);

        var btnPlayerActivity = this.btnPlayerActivity = alteastream.Assets.getImage(assets.images.btnPlayerActivity);
        btnPlayerActivity.x = btnInfo.x - width - spacing;
        btnPlayerActivity.y = yPos;
        btnPlayerActivity.originalXPosition = btnPlayerActivity.x;
        btnPlayerActivity.addEventListener("click", _instance.btnPlayerActivityHandler);
        this.addChild(btnPlayerActivity);

        var btnSound = this.btnSound = alteastream.Assets.getImage(assets.images.soundOn);
        btnSound.x = btnPlayerActivity.x - width - spacing;
        btnSound.y = yPos;
        btnSound.addEventListener("click", _instance.btnSoundHandler);
        this.addChild(btnSound);

        var btnFs = this.btnFs = alteastream.Assets.getImage(assets.images.btnFullScreen_On);
        btnFs.x = btnSound.x - width - spacing;
        btnFs.y = yPos;

        var method;
        if(is.safari() === true || is.edge() === true){
            method = "otherBrowsersFullScreenToggle";
        }else{
            method = "normalBrowsersFullScreenToggle";
        }
        btnFs.addEventListener("click", this[method]);
        this.addChild(btnFs);
    };

    p._local_activate = function() {
        this.startCommunication = function(communicator){
            _instance.globalMouseEnabled(false);
            //communicator.getHouses(function(response) {// todo getGameTypes??
            //communicator.getGameTypes(function(response) {// todo getGameTypes??
            communicator.getGameCategories(function(response) {// todo getGameTypes??
                _instance.globalMouseEnabled(true);
                console.log("HOUSES:: " + response);
                _instance._setComponents(response);
                _instance.defineInfoOnStart();
                _instance.socketCommunicator = new alteastream.SocketCommunicatorLobby();
                setTimeout(function () {_instance.onSocketConnect();}, 500);
            });
        };

        this.onSocketConnect = function(frame){
            _instance.setRenderer();
            _instance.getBalanceAndActivity();
            if(alteastream.AbstractScene.BACK_TO_HOUSE !== -1){
                var location = _instance.currentMap.carousel.getLocation(alteastream.AbstractScene.BACK_TO_HOUSE);
                _instance.enterShop(location);
            }
        };
    };

    // public methods:
    p.resetBackgroundMusic = function() {
        window.parent.startHelperSound("lobbyBgMusic",{loop:-1,volume:0.2});
    };

    p.setCommunicator = function(){
        this.requester = new alteastream.Requester(this);
        this.startCommunication(this.requester);
    };

    p.startCommunication = function(communicator){
        this.globalMouseEnabled(false);
        //communicator.getHouses(function(response) {
        communicator.getGameCategories(function(response) {
                _instance.globalMouseEnabled(true);
                console.log("HOUSES:: " + response);
                _instance._setComponents(response);
                _instance.defineInfoOnStart();
                //new socket
                _instance.setSocketCommunication("Lobby");
                _instance.socketCommunicator.activate();
        });
    };

    p.onSocketConnect = function(frame){
            _instance.setRenderer();
            _instance.socketCommunicator.onLobbyConnect(frame);
            _instance.getBalanceAndActivity();
            if(alteastream.AbstractScene.BACK_TO_HOUSE !== -1){
                var backgroundMusicIsMuted = window.localStorage.getItem('backgroundMusicIsMuted');
                createjs.Sound.muted = backgroundMusicIsMuted === "false";
                _instance.btnSoundHandler();
                var location  = _instance.currentLocationMachines._machinesComponent.getAcordionByName(alteastream.AbstractScene.BACK_TO_HOUSE);
                _instance.currentLocationMachines._machinesComponent.acordionClickHandler(location);
            }
    };

    p.getName = function(suf){
        return Lobby["LOCATION_"+ suf];
    };

    p.setFocused = function(object){
        this._currentFocused = object;
    };

    p.getFocused = function(){
        return this._currentFocused;
    };

    p.switchFocusIn = function(objectIn){
        this.getFocused().focusOut(objectIn);
    };

    p.clickObserver = function(location){
        if(location.allow === true){
            this.enterShop(location);
        }
    };

    p.enterShop = function(location){
        this.checkIfHiddenMenuIsShown();
        location.mouseEnabled = false;
        this.globalMouseEnabled(false);
        //this.requester.getMachines(location.id,function(response){
        //this.requester.getMachinesGt(location.id,function(response){
        var method = "getMachinesBy" + location.category;
        //this.requester.getMachinesByCategory(location.name,function(response){
        this.requester[method](location.name,function(response){
            if(Object.keys(response).length === 0 && response.constructor === Object){//temp, on Exception
                _instance.playSound("errorSpoken");
                _instance.throwAlert(alteastream.Alert.ERROR,"No machines available",function(){location.mouseEnabled = true; _instance.globalMouseEnabled(true);});
            }else{
                //window.parent.switchSource("locationSoundBg");
                var musicAction = createjs.Sound.muted === true ? "pause" : "play";
                window.parent.manageBgMusic(musicAction);
                location.mouseEnabled = true;
                _instance.globalMouseEnabled(true);
                _instance.selectedLocation = location;// tent container // todo mozda ovo iskoristiti kao selected acordion
                _instance.currentLocationMachines._machinesComponent.selectedAcordion = location;// tent container
                alteastream.AbstractScene.BACK_TO_HOUSE = location.name;
                _instance._logo.visible = false;
                _instance.currentLocationMachines.streamPreview.activate();
                _instance.currentLocationMachines.populateMachines(response);
                _instance.switchFocusIn(_instance.currentLocationMachines);
                _instance.mouseWheel.enable(true);
                _instance.currentLocationMachines._resetScrollComponent(false);
            }
        });
    };

    p.backToLocationsFromLobby = function() {
        this.switchFocusIn(this.currentMap);
        this.currentMap.visible = true;

        this._logo.visible = true;
        this.beams.showBeams(true);
        //window.parent.switchSource("bgMusic");
        //var musicAction = createjs.Sound.muted === true ? "pause" : "play";
        //window.parent.manageBgMusic(musicAction);
    };

    p.hideBackgroundsForFullScreenVideo = function(hide) {
        this.mainBackground.visible =
            this.mainBackgroundOverlay.visible =
                this.mainBackgroundOverlay2.visible = !hide;
    };

    p.defineInfoOnStart = function(){
        this._showInfoOnStart(false);
    };

    p.getBalanceAndActivity = function(){
        alteastream.Requester.getInstance().getBalance(function (resp) {//9471.55 treba  a sad je 9471.55
            console.log(resp);

            var allowSpectate = true;
            if(resp.hasOwnProperty("allow_spectate")){
                allowSpectate = resp.allow_spectate;
            }
            alteastream.AbstractScene.HAS_COPLAY = allowSpectate;
            //decimal
            //var code = 8364;//resp.currency
            //var currency = _instance.currency = String.fromCharCode( unicode);

            //hex, css
            //var code = '20AC';//20A4 resp.currency
            //var currency = _instance.currency = String.fromCharCode( parseInt(code, 16) );

            //_instance.lobbyBalanceLabel.text = "BALANCE ( "+currency+" ) ";

            //novi poziv::
            //{"balance":870910,"currency":"EUR","ingame":false}
            _instance.currentLocationMachines.queuePanel.previousMachineActive = resp.ingame;
            _instance.currency = resp.currency;
            _instance.lobbyBalanceLabel.text = "BALANCE ( "+_instance.currency+" ) ";
            _instance.updateLobbyBalance(resp.balance);

            _instance.currentLocationMachines._machinesComponent.setCurrency(_instance.currency);

            //set hasCoPlay here from some param
            if(alteastream.AbstractScene.HAS_COPLAY){
                _instance.currentLocationMachines.queuePanel.addMultiplayerControls(resp.participateCounter);
                //_instance.currentLocationMachines.queuePanel.multiPlayerControls.setBettingCounterTime(resp.participateCounter);
            }

            _instance._getPlayerActivity();
        });
    };

    p.updateLobbyBalance = function(val){
        var crdAmountCents = Math.min(val/1000);//1718900
        crdAmountCents = crdAmountCents.toFixed(2);
        this.lobbyBalanceAmount.text = crdAmountCents;
    };

    p.setOverlay = function() {
        var isMob = window.localStorage.getItem('isMobile') === "yes";
        var background = new createjs.Bitmap(queue.getResult("back"));
        var logo = new createjs.Bitmap(queue.getResult("logo"));
        var buttonProps = isMob ? {w:86,h:28,round:3,f:"12px Arial"} : {w:170,h:56,round:6,f:"24px Arial"};

        var button = new createjs.Shape();
        button.graphics.setStrokeStyle(2);
        button.graphics.beginStroke("#ffffff").beginFill('#000000').drawRoundRect(0,0,buttonProps.w,buttonProps.h,buttonProps.round);
        button.regX = buttonProps.w/2;
        button.regY = buttonProps.h/2;
        button.x = alteastream.AbstractScene.GAME_WIDTH * 0.5;
        button.y = (alteastream.AbstractScene.GAME_HEIGHT* 0.5) + (buttonProps.h * 2);

        var txt = new createjs.Text("PLAY NOW", buttonProps.f, "#ffffff");
        txt.textAlign = 'center';
        txt.textBaseline = 'middle';
        txt.x = button.x;
        txt.y = button.y;

        var overlayClickCalback = function () {
            _instance.resetBackgroundMusic();
            if(isMob){
                if(is.safari()){
                    var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
                    if(is.not.ipad()) {
                        if(!isiPadOS){
                            alteastream.ABSStarter.getInstance().safariResize();
                        }
                    }
                }
            }
        };

        var overlayOptions = {
            background:background,
            graphics:[logo],
            texts:[txt],
            button:button,
            clickCallback:overlayClickCalback
        }

        this.setStartOverlay(overlayOptions);
    };

    p.setBackgroundImagesForStreamPreview = function(set) {
        var img = set === true ? "mainBackgroundWithHole" : "mainBackground";
        this.mainBackground.image = alteastream.Assets.getImage(alteastream.Assets.images[img]).image;
        this.mainBackgroundOverlay.visible = !set;
        this.mainBackgroundOverlay2.visible = !set;
    };

    p.setMap = function(locations){
        this._housesArr = [];

        for(var i = 0; i < locations.stake_categories.length; i++){
            if(locations.stake_categories[j] !== null){
                if(locations.stake_categories[i].length > 2){
                    var stake = {
                        name:locations.stake_categories[i],
                        category:"Stake"
                    };
                    this._housesArr.push(stake);
                }
            }
        }

        for(var j = 0; j < locations.payout_categories.length; j++){
            if(locations.payout_categories[j] !== null){
                if(locations.payout_categories[j].length > 2){
                    var payout = {
                        name:locations.payout_categories[j],
                        category:"Payout"
                    };
                    this._housesArr.push(payout);
                }
            }
        }
    };

    p.setCurrentLocationMachines = function() {
        var currentLocationMachines = this.currentLocationMachines = new alteastream.LocationMachines(this);
        currentLocationMachines.x = 100;
        currentLocationMachines.y = 121;
    };

    p.btnQuitHandler = function() {
        var handler = _instance._info.visible === true ? "btnInfoHandler" : "_setQuitPanel";
        _instance[handler]();
        //_instance._setQuitPanel();
    };

    p.btnPlayerActivityHandler = function() {
        if(_instance._info.visible === true){
            _instance.hideInfo();
        }
        _instance._playerActivityPanel.visible = !_instance._playerActivityPanel.visible;
        if(_instance._playerActivityPanel.visible === false){
            _instance._playerActivityPanel.setToFirstPage(false);
            _instance.mouseWheel.setTarget(_instance.currentLocationMachines);
            if(_instance._currentFocused.name === "circus"){
                _instance.mouseWheel.enable(false);
            }
        }else{
            _instance.mouseWheel.setTarget(_instance._playerActivityPanel);
            if(_instance.mouseWheel._enabled === false){
                _instance.mouseWheel.enable(true);
            }
        }
        _instance._playerActivityPanel.activateSupportLinkListeners(_instance._playerActivityPanel.visible);
    };

    p.btnInfoHandler = function(){
        if(_instance._playerActivityPanel.visible === true){
            _instance.hidePlayerActivity();
        }
        _instance._info.visible = true;
        _instance.btnInfo.mouseEnabled = false;
        var scale = _instance._info.scaleX === 1 ? 0 : 1;
        if(scale === 1){_instance._info.resetCurrentPage();}
        _instance._info.preventClickThrough(scale === 1);
        createjs.Tween.get(_instance._info).to({scaleX:scale,scaleY:scale},300,createjs.Ease.quadInOut).call(function () {
            _instance.btnInfo.mouseEnabled = true;
            if(scale === 0){_instance._info.visible = false;}
        });
    };

    p.hidePlayerActivity = function() {
        this._playerActivityPanel.visible = false;
        this._playerActivityPanel.setToFirstPage(false);
        this.mouseWheel.setTarget(this.currentLocationMachines);
        if(_instance._currentFocused.name === "circus"){
            _instance.mouseWheel.enable(false);
        }
    };

    p.hideInfo = function() {
        this._info.visible = false;
        this._info.scale = 0;
        this._info.resetCurrentPage();
        this.btnInfo.mouseEnabled = true;
    };

    p.btnSoundHandler = function() {
        var assets = alteastream.Assets;
        createjs.Sound.muted = !createjs.Sound.muted;
        _instance.btnSound.image = createjs.Sound.muted === true ? assets.getImage(assets.images.soundOff).image : assets.getImage(assets.images.soundOn).image;
        var action = createjs.Sound.muted === true ? "pause":"play";
        window.parent.manageBgMusic(action);
    };

    p.quitConfirmed = function(){
        _instance.onQuit();
    };

    p.onQuit = function(){
        if(_instance.socketCommunicator)
            _instance.socketCommunicator.disposeCommunication();
        _instance.killGame();
        var quitUrl = alteastream.AbstractScene.GAME_QUIT === "" ? alteastream.AbstractScene.GAME_PROTOCOL+alteastream.AbstractScene.SERVER_IP + "/demo" : alteastream.AbstractScene.GAME_QUIT;
        quitUrl += "/";
        window.parent.location.href = decodeURIComponent(quitUrl);
    };

    p.onStartGameStream = function(){
        //_instance.currentMap.carousel.storeLocationPosition();
        //new socket comment heartbeatGame:
        //_instance.socketCommunicator.heartbeatGame();

        _instance.socketCommunicator.heartbeatUser();
    };

    p.enableSwiper = function(enable){};
    p.setSwipeTarget = function(target,axis){};
    p.checkIfHiddenMenuIsShown = function(){};

    alteastream.Lobby = createjs.promote(Lobby,"AbstractScene");
})();

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LobbyMobile = function() {
        console.log("Lobby mobile init.....");
        this.Lobby_constructor();
        this.initialize_LobbyMobile();
    };
    var p = createjs.extend(LobbyMobile, alteastream.Lobby);

    // private properties
    p._hiddenMenu = null;
    p._hiddenMenuIsShown = false;
    // public properties:
    p.swiper = null;

    // static properties:
    // events:

    var _instance = null;

    // constructor:
    p.initialize_LobbyMobile = function() {
        _instance = this;
    };
    // static methods:
    LobbyMobile.getInstance = function(){
        return _instance;
    };

    // private methods
    p.super_setComponents = p._setComponents;
    p._setComponents = function(response){
        this.super_setComponents(response);

        this.currentMap.x = 32;
        this.currentMap.y = 300;

        //this.currentMap.carousel.adjustMobile();
        this._adjustBeams();
        this._adjustBulbs();
        this._info.adjustMobile();
        this._adjustBalance();
        this._setSwiper();
        //this._adjustClock();// uncomment if clock need
        stage.enableMouseOver(0);
    };

    p._adjustClock = function() {
        this.clock.x = 480;
        this.clock.y = 5;
        this.showFSClock(true);
    };

    p._adjustBalance = function() {
        var balanceBack = this.balanceBackground;
        balanceBack.uncache();
        balanceBack.graphics.clear();
        balanceBack.graphics.beginFill("#000000").drawRect(0, 0, 194, 45);
        balanceBack.cache(0, 0, 194, 45);
        balanceBack.x = 16;
        balanceBack.y = 480;

        this.lobbyBalanceLabel.font = "14px HurmeGeometricSans3";
        this.lobbyBalanceAmount.font = "bold 16px HurmeGeometricSans3";
        this.lobbyBalanceLabel.x = balanceBack.x + 99;
        this.lobbyBalanceLabel.y = balanceBack.y + 13;
        this.lobbyBalanceAmount.x = this.lobbyBalanceLabel.x;
        this.lobbyBalanceAmount.y = this.lobbyBalanceLabel.y + 20;
    };

    p._adjustBeams = function() {
        var beams = this.beams._beamsArray;
        beams[0].x = 0;
        beams[0].y = -10;
        beams[1].x = 15;
        beams[1].y = -10;
        beams[2].x = 960;
        beams[2].y = -10;
        beams[3].x = 975;
        beams[3].y = -10;
    };

    p._adjustBulbs = function() {
        var bulbsNum = this.bulbsContainer.numChildren;
        var util = gamecore.Utils.NUMBER;
        for(var i = 0; i < bulbsNum; i++){
            var xp = util.randomRange(10,950);
            var yp = util.randomRange(10,125);
            var bulb = this.bulbsContainer.getChildAt(i);
            bulb.x = xp;
            bulb.y = yp;
        }
    };

    p._setPlayerActivity = function() {
        _instance._playerActivityPanel = new alteastream.PlayerActivityPanel();
        _instance.addChild(_instance._playerActivityPanel);
        _instance._playerActivityPanel.visible = false;
        _instance._playerActivityPanel.preventClickThrough(true);
    };

    /*p._getPlayerActivity = function(){
        this.requester.getPlayerActivity(function (response) {
            _instance._playerActivityPanel.setPanel(response);
            _instance._adjustActivityLog();
        });
    };*/

    p._getPlayerActivity = function(){
        _instance.requester.getPlayerActivity(function (response) {
            if(response.length){
                _instance._playerActivityPanel.setPanel(response);
                _instance._playerActivityPanel.adjustMobile();
            }else{
                _instance._startPlayerActivityInterval();
            }
        });
    };

    p._startPlayerActivityInterval = function() {
        var attempt = 0;
        var activityInterval = setInterval(function(){
            _instance.requester.getPlayerActivity(function (response) {
                if(response.length){
                    _instance._playerActivityPanel.setPanel(response);
                    _instance._playerActivityPanel.adjustMobile();
                    clearInterval(activityInterval);
                }else{
                    attempt++;
                    if(attempt === 6){
                        clearInterval(activityInterval);
                    }
                }
            });
        },5000);
    };

    p._showInfoOnStart = function(bool){
        this._info.scaleX = this._info.scaleY = bool===true? 1:0;

        this.btnSound.visible = this.btnQuit.visible = this.btnPlayerActivity.visible = !bool;
        this._hiddenMenuButton.visible = !bool;
        this.btnInfo.y = bool === true ? -55:this.btnQuit.y + (this.btnQuit.height*2);
        this.btnInfo.x = bool === true ? -100 :0;
    }

    p._setTopButtons = function() {
        var assets = alteastream.Assets;
        var hiddenMenu = this._hiddenMenu = new createjs.Container();

        var menuButton = this._hiddenMenuButton = new alteastream.ImageButton(assets.getImageURI(assets.images.btnHiddenMenu),3);
        menuButton.x = 878;
        menuButton.y = 12;
        menuButton.visible = false;
        this.addChild(menuButton);

        var menuButtonHandler = function () {
            _instance.showHiddenMenu(_instance._hiddenMenu.x === alteastream.AbstractScene.GAME_WIDTH);
        };

        menuButton.setClickHandler(menuButtonHandler);

        hiddenMenu.x = alteastream.AbstractScene.GAME_WIDTH;
        hiddenMenu.y = menuButton.y + 80;

        var btnQuit = this.btnQuit = new alteastream.ImageButton(assets.getImageURI(assets.images.btnQuitSS),3);
        var btnHeight = btnQuit.height;
        btnQuit.visible = false;

        var btnQuitHandler = function () {
            _instance.checkIfHiddenMenuIsShown();
            _instance.btnQuitHandler();
        };
        btnQuit.setClickHandler(btnQuitHandler);

        hiddenMenu.addChild(btnQuit);

        var btnSound = this.btnSound = alteastream.Assets.getImage(assets.images.soundOn);
        btnSound.y = btnQuit.y + btnHeight;
        btnSound.visible = false;
        btnSound.addEventListener("click", function () {
            createjs.Sound.muted = !createjs.Sound.muted;
            btnSound.image = createjs.Sound.muted === true ? assets.getImage(assets.images.soundOff).image : assets.getImage(assets.images.soundOn).image;
            var action = createjs.Sound.muted === true ? "pause":"play";
            window.parent.manageBgMusic(action);
            _instance.checkIfHiddenMenuIsShown();
        });
        hiddenMenu.addChild(btnSound);

        var btnInfo = this.btnInfo =  new alteastream.ImageButton(assets.getImageURI(assets.images.btnInfoSS),3);

        btnInfo.setClickHandler(_instance.btnInfoHandler);

        hiddenMenu.addChild(btnInfo);

        var btnPlayerActivity = this.btnPlayerActivity =  alteastream.Assets.getImage(assets.images.btnPlayerActivity);
        btnPlayerActivity.y = btnQuit.y + (btnHeight * 3);
        btnPlayerActivity.originalYPosition = btnPlayerActivity.y;
        btnPlayerActivity.addEventListener("click" , function () {
            _instance.btnPlayerActivityHandler();
        });
        hiddenMenu.addChild(btnPlayerActivity);

        this.addChild(hiddenMenu);
    };

    p._setSwiper = function(){
        var swiper = this.swiper = new alteastream.Swiper();
        swiper.activate(true);
        this.setSwipeTarget(this.currentMap.carousel,"x");
    };

    // public methods
    p.btnPlayerActivityHandler = function() {
        _instance._playerActivityPanel.visible = !_instance._playerActivityPanel.visible;
        _instance.btnPlayerActivity.x = _instance._playerActivityPanel.visible === true ? -30 : 0;
        _instance.btnPlayerActivity.y = _instance._playerActivityPanel.visible === true ? -55 : _instance.btnPlayerActivity.originalYPosition;
        _instance._hiddenMenuButton.visible = _instance.btnSound.visible = _instance.btnQuit.visible = _instance.btnInfo.visible = !_instance._playerActivityPanel.visible;
        if(_instance._playerActivityPanel.visible === false){
            _instance.showHiddenMenu(false);
            _instance._playerActivityPanel.setToFirstPage(false);
            if(_instance._currentFocused.name === "circus"){
                _instance.setSwipeTarget(_instance.currentMap.carousel,"x");
            }else{
                _instance.setSwipeTarget(_instance.currentLocationMachines,"y");
            }
        }else{
            _instance.setSwipeTarget(_instance._playerActivityPanel, "y");
        }
        _instance._playerActivityPanel.activateSupportLinkListeners(_instance._playerActivityPanel.visible);
    };

    p.btnInfoHandler = function(){
        _instance._info.visible = true;
        _instance.btnInfo.mouseEnabled = false;
        var scale = _instance._info.scaleX === 1 ? 0 : 1;
        _instance._hiddenMenuButton.visible = _instance.btnSound.visible = _instance.btnQuit.visible = _instance.btnPlayerActivity.visible = scale === 0;
        _instance.btnInfo.y = scale === 0 ? _instance.btnQuit.y + (_instance.btnQuit.height*2) : -55;
        _instance.btnInfo.x = scale === 0 ? 0 : -30;
        if(scale === 0){_instance.showHiddenMenu(false);}
        if(scale === 1){_instance._info.resetCurrentPage();}
        _instance.btnQuit.visible = scale === 0;
        createjs.Tween.get(_instance._info).to({scaleX:scale,scaleY:scale},300,createjs.Ease.quadInOut).call(function () {
            _instance.btnInfo.mouseEnabled = true;
            if(scale === 0){_instance._info.visible = false;}
        });
    };

    p.showHiddenMenu = function(show) {
        this._hiddenMenuButton.setDisabled(true);
        this._hiddenMenuIsShown = show;
        var xP = show === true ? this._hiddenMenuButton.x : alteastream.AbstractScene.GAME_WIDTH;
        createjs.Tween.get(this._hiddenMenu).to({x:xP},500,createjs.Ease.quadOut).call(function () {
            _instance._hiddenMenuButton.setDisabled(false);
        })
    };

    p.checkIfHiddenMenuIsShown = function() {
        if(this._hiddenMenuIsShown === true) {
            this.showHiddenMenu(false);
        }
    };

    p.setCurrentLocationMachines = function() {
        var currentLocationMachines = this.currentLocationMachines = new alteastream.LocationMachinesMobile(this);
        currentLocationMachines.x = 10;
        currentLocationMachines.y = 10;
    };

    p.setSwipeTarget = function (target,axis) {
        this.swiper.setTargetAndDirection(target,axis);
    };

    p.enableSwiper = function(enable) {
        this.swiper.activate(enable);
    };

    alteastream.LobbyMobile = createjs.promote(LobbyMobile,"Lobby");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var PlayerActivityPanel = function(){
        this.Container_constructor();
        this._initPlayerActivityPanel();
    };

    var p = createjs.extend(PlayerActivityPanel,createjs.Container);

    // private properties
    p._txtFields = null;
    p._response = null;
    p._numberOfPages = 0;
    p._rowsPerPage = 0;
    p._tableContent = null;
    p._tableContentStartYPos = null;
    p._numberOfRows = null;
    p._backgroundShapeW = null;
    p._backgroundShapeH = null;
    p._scroller = null;
    // public properties
    p.headerTexts = null;
    p.backgroundShapes = null;
    p.labels = null;

    var _this;

    // constructor
    p._initPlayerActivityPanel = function () {
        console.log("Activity Initialized");
        _this = this;
        this._tableContent = new createjs.Container();
        this._tableContent.x = 111;
        this._tableContent.y = this._tableContentStartYPos = 329;
        var background = alteastream.Assets.getImage(alteastream.Assets.images.infoBackground);
        this.addChild(background, this._tableContent);

        var mainLabel = new createjs.Text("Activity Log","bold 62px Roboto","#ffffff").set({textAlign:"left", textBaseline:"middle", x:111, y:150, mouseEnabled:false});
        this.addChild(mainLabel);

        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText8);
        var infoText = new createjs.Text(textSrc,"18px Roboto","#ffffff").set({textAlign:"left", textBaseline:"middle", x:111, y:225, mouseEnabled:false});
        this.addChild(infoText);

        var supportLink = this.supportLink = new createjs.Text("support.","18px Roboto","#d60168").set({textAlign:"center", textBaseline:"middle", x:695, y:225});
        supportLink.cursor = "pointer";

        var area = new createjs.Shape();
        area.graphics.beginFill("#fff").drawRect(-50, -25, 100, 50);
        supportLink.hitArea = area;

        this.addChild(supportLink);

        this.headerTexts = [mainLabel, infoText, supportLink];

        this._scroller = new alteastream.Scroller(this._tableContent);
        this._scroller.setScrollType("scrollByRow");
        this._scroller.setScrollDirection("y");
    };

    // private methods
    p._setMask = function() {
        var tableMask = new createjs.Shape();
        tableMask.graphics.beginFill("#ffffff").drawRect(this._tableContent.x, this._tableContent.y, this._backgroundShapeW, (this._backgroundShapeH * this._rowsPerPage));
        this._tableContent.mask = tableMask;
    };

    p._setTableBackground = function(numberOfRows) {
        this.backgroundShapes = [];
        var shapeW = this._backgroundShapeW = 1692;
        var shapeH = this._backgroundShapeH = 57;
        var numberOfTableRows = numberOfRows < (this._rowsPerPage) ? numberOfRows : numberOfRows;//+1 for labels
        for(var i = 0; i < numberOfTableRows; i++){
            var color = i%2 === 0 ? "#141210" : "#191815";
            var shape = new createjs.Shape();
            shape.graphics.beginFill(color).drawRect(0, 0, shapeW, shapeH);
            shape.cache(0, 0, shapeW, shapeH);
            shape.mouseEnabled = false;
            shape.y = (i * shapeH);
            this._tableContent.addChild(shape);
            this.backgroundShapes.push(shape);
        }
    };

    p._setScrollLevels = function() {
        var scrollLevels = [];
        var numOfLevels = this._numberOfRows - this._rowsPerPage + 1;
        for(var i = 0; i < numOfLevels; i++){
            scrollLevels.push((i * this._backgroundShapeH * -1) + this._tableContentStartYPos);
        }
        this._scroller.setScrollLevels(scrollLevels);
    };

    p._setLabels = function() {
        var labelsY = 300;
        var spacing = 242;
        var font = "18px Roboto";
        var color = "#a7a7a6";
        var currency = this.parent.currency;
        var machineName = new createjs.Text("MACHINE NAME",font,color).set({textAlign:"center", textBaseline:"middle", x:232, y:labelsY, mouseEnabled:false});
        var startTime = new createjs.Text("START TIME",font,color).set({textAlign:"center", textBaseline:"middle", x:machineName.x + spacing, y:labelsY, mouseEnabled:false});
        var endTime = new createjs.Text("END TIME",font,color).set({textAlign:"center", textBaseline:"middle", x:startTime.x + spacing, y:labelsY, mouseEnabled:false});
        var totalBet = new createjs.Text("TOTAL BET ("+currency+")",font,color).set({textAlign:"center", textBaseline:"middle", x:endTime.x + spacing, y:labelsY, mouseEnabled:false});
        var totalWin = new createjs.Text("TOTAL WIN ("+currency+")",font,color).set({textAlign:"center", textBaseline:"middle", x:totalBet.x + spacing, y:labelsY, mouseEnabled:false});
        var payout = new createjs.Text("PAYOUT (%)",font,color).set({textAlign:"center", textBaseline:"middle", x:totalWin.x + spacing, y:labelsY, mouseEnabled:false});
        var revenue = new createjs.Text("REVENUE ("+currency+")",font,color).set({textAlign:"center", textBaseline:"middle", x:payout.x + spacing, y:labelsY, mouseEnabled:false});

        this.addChild(machineName, startTime, endTime, totalBet, totalWin, payout, revenue);
        this.labels = [machineName, startTime, endTime, totalBet, totalWin, payout, revenue];
    };

    p._populateHistory = function(response) {
        response = response.reverse();
        var labelsY = -29;
        var spacing = 242;
        var font = "18px Roboto";
        var grey = "#a7a7a6";
        var white = "#ffffff";
        var yellow = "#dfdf2d";
        var red = "#d43a6a";
        var green = "#2dfcac";
        var machineName = new createjs.Text("",font,white).set({textAlign:"center", textBaseline:"middle", lineHeight:57, x:121, y:labelsY, mouseEnabled:false});
        var startTime = new createjs.Text("",font,grey).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:machineName.x + spacing, y:labelsY, mouseEnabled:false});
        var endTime = new createjs.Text("",font,grey).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:startTime.x + spacing, y:labelsY, mouseEnabled:false});
        var totalBet = new createjs.Text("",font,yellow).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:endTime.x + spacing, y:labelsY, mouseEnabled:false});
        var totalWin = new createjs.Text("",font,red).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:totalBet.x + spacing, y:labelsY, mouseEnabled:false});
        var payout = new createjs.Text("",font,white).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:totalWin.x + spacing, y:labelsY, mouseEnabled:false});
        var revenue = new createjs.Text("",font,green).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:payout.x + spacing, y:labelsY, mouseEnabled:false});

        this._txtFields = [machineName, startTime, endTime, totalBet, totalWin, payout, revenue];
        this._tableContent.addChild(machineName, startTime, endTime, totalBet, totalWin, payout, revenue);

        var rowsToPopulate = response.length;
        this._manageTableContent(0, rowsToPopulate);
    };

    p._manageTableContent = function(start,end){
        for(var i = start; i < end; i++){
            var row = this._response[i];
            var keyCounter = 0;
            for(var key in row){
                var text = row[key];
                if(key === "startTime" || key === "endTime"){
                    var endOfText = text.indexOf(".");
                    text = text.substring(0, endOfText)
                }
                if(key === "totalBet" || key === "totalWin" || key === "revenue"){
                    var value = Number(row[key]);
                    text = Math.min(value/1000).toFixed(2);
                }
                if(key === "payout"){
                    var value = Number(row[key]);
                    text = value.toFixed(2);
                }
                this._txtFields[keyCounter].text += "\n" + text;
                keyCounter++;
            }
        }
    };

    p._linkClickHandler = function() {
        // todo uncomment when support page is done
        //window.open("http://"+alteastream.AbstractScene.SERVER_IP + "/support.html");
    };

    p._linkOverHandler = function() {
        _this.supportLink.color = "#dfdf2d"
    };

    p._linkOutHandler = function() {
        _this.supportLink.color = "#d60168"
    };

    // public methods
    p.activateSupportLinkListeners = function(activate) {
        var setListener = activate === true ? "addEventListener" : "removeEventListener";
        this.supportLink[setListener]("click", this._linkClickHandler);
        var isMobile = window.localStorage.getItem('isMobile');
        if(isMobile === "not"){
            this.supportLink[setListener]("mouseover", this._linkOverHandler);
            this.supportLink[setListener]("mouseout", this._linkOutHandler);
        }
    };

    p.setPanel = function(response) {
        this._response = response;
        this._rowsPerPage = 11;
        this._numberOfPages = Math.ceil(response.length/this._rowsPerPage);
        this._numberOfRows = response.length;

        this._setTableBackground(this._numberOfRows);
        this._setLabels();
        this._populateHistory(response);
        this._setScrollLevels();
        this._setMask();
    };

    p.setToFirstPage = function() {
        if(this._numberOfPages > 0){
            this._scroller.setToFirstPage(false);
        }
    };

    p.preventClickThrough = function(bool){
        var method = bool === true? "on":"off";
        this[method]("click",function noClick(e){});
    };

    p.onMouseWheel = function(direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    p.onSwipe = function (direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    p.adjustMobile = function(){
        var mainLabel = this.headerTexts[0];
        mainLabel.font = "bold 31px Roboto";
        mainLabel.x = 55;
        mainLabel.y = 75;

        var infoText = this.headerTexts[1];
        infoText.font = "14px Roboto";
        infoText.x = 55;
        infoText.y = 112;

        var supportLink = this.supportLink;
        supportLink.font = "14px Roboto";
        supportLink.x = 512;
        supportLink.y = 112;

        var shapeW = this._backgroundShapeW = 846;
        var shapeH = this._backgroundShapeH = 28;

        for(var i = 0; i < this.backgroundShapes.length; i++){
            var color = i%2 === 0 ? "#141210" : "#191815";
            var shape = this.backgroundShapes[i];
            shape.uncache();
            shape.graphics.clear();
            shape.graphics.beginFill(color).drawRect(0, 0, shapeW, shapeH);
            shape.cache(0, 0, shapeW, shapeH);
            shape.y = i * shapeH;
        }

        var labelsY = 150;
        var spacing = 121;
        var labelsStartX = 116;
        for(var j = 0; j < this.labels.length; j++){
            var label = this.labels[j];
            label.font = "12px Roboto";
            label.x = labelsStartX + (j * spacing);
            label.y = labelsY;
        }

        for(var k = 0; k < this._txtFields.length; k++){
            var field = this._txtFields[k];
            field.font = "12px Roboto";
            field.lineHeight = shapeH;
            // todo ne kapiram zasto ovo ne radi
            /*            field.x = labelsStartX + (k * spacing);
                        field.y = labelsY;*/
            field.x = 60 + (k * spacing);
            field.y = -12;
        }

        this._tableContent.x = 55;
        this._tableContent.y = this._tableContentStartYPos = 164;

        this._setMask();
        this._setScrollLevels();
    };

    alteastream.PlayerActivityPanel = createjs.promote(PlayerActivityPanel,"Container");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var BetEntranceCounter = function(){
        this.AbstractCounter_constructor();
        this.initBetEntranceCounter();
    };

    var p = createjs.extend(BetEntranceCounter,alteastream.AbstractCounter);

    var _this;

    p.initBetEntranceCounter = function () {
        console.log("BetEntranceCounter initialized:::");
        _this = this;
        this.strokeTickness = 12;
        this.counterRadius = 100;
        this.degree = 360;
        this.colors = ["#ffcc00", "#ff0000"];
    };

    BetEntranceCounter.getInstance = function() {
        return _this;
    };

    // private methods
    p._updateCounter = function(count){
        if(count>-1)
            this.confirmEnterCounter.text = count;
        else{
            this.clearCounterInterval();
            alteastream.SocketCommunicatorLobby.getInstance().doBetting(false);
        }
    };

    // public methods
    p.setLayout = function(){
        var shapeW = 233;
        var shapeH = 844;

        var labelShape = this._labelShape = new createjs.Shape();
        labelShape.graphics.beginFill("#000000").drawRect(0, 0, 234, 50, 4);
        labelShape.alpha = 0.7;
        labelShape.cache(0, 0, shapeW, 50);
        labelShape.x = 21;
        labelShape.y = -56;
        this.addChild(labelShape);

        var counterShape = this.counterShape = new createjs.Shape();
        counterShape.graphics.beginFill("#000000").drawRect(0, 0, shapeW, shapeH, 5);
        counterShape.cache(0, 0, shapeW, shapeH);
        counterShape.size = {width:shapeW, height:shapeH, roundness:5};
        counterShape.x = 21;
        counterShape.y = 3;
        counterShape.mouseEnabled = false;
        this.addChild(counterShape);
        counterShape.alpha = 0.7;

        var circleX = 137;

        var counterCircleBackground = this.counterCircleBackground = new createjs.Shape();
        counterCircleBackground.graphics.setStrokeStyle(12).beginStroke("#555153").arc(0, 0, 100, 0, (Math.PI/180) * 360).endStroke();
        counterCircleBackground.x = circleX;
        counterCircleBackground.y = 140;
        counterCircleBackground.mouseEnabled = false;
        this.addChild(counterCircleBackground);

        var counterCircle = this.counterCircle = new createjs.Shape();
        counterCircle.rotation = -90;
        counterCircle.x = circleX;
        counterCircle.y = counterCircleBackground.y;
        counterCircle.mouseEnabled = false;
        this.addChild(counterCircle);

        this._createText("confirmEnterText","CONFIRM IN:","bold 16px HurmeGeometricSans3", "#a0a0a0",{x:circleX,y:-29,textAlign:"center",textBaseline:"middle"});//1172
        this._createText("confirmEnterCounter","0","78px HurmeGeometricSans3", "#ffffff",{x:circleX,y:115,textAlign:"center",textBaseline:"middle"});//1332
        this._createText("secondsText","SECONDS","24px HurmeGeometricSans3", "#ffffff",{x:circleX,y:180,textAlign:"center",textBaseline:"middle"});//1332
        this._createText("stakeLabel","CO-PLAY STAKE\n(EUR)","bold 22px HurmeGeometricSans3", "#ffffff",{x:circleX,y:484,textAlign:"center",textBaseline:"middle"});
        this._createText("infoText","START\nPARTICIPATING!","bold 22px HurmeGeometricSans3", "#ffffff",{x:circleX,y:785,textAlign:"center",textBaseline:"middle"});//1332
        this.infoText.lineHeight = 25;
    };

    p.quitCounter = function() {
        this.show(false);
        this.clearCounterInterval();
    };

    p.adjustMobile = function(){
        var yCorrector = 12;

        var labelShape = this._labelShape;
        labelShape.uncache();
        labelShape.graphics.beginFill("#000000").drawRect(0, 0, 140, 45);
        labelShape.alpha = 0.7;
        labelShape.cache(0, 0, 140, 45);
        labelShape.x = 6;
        labelShape.y = 20 + yCorrector;

        var counterShape = this.counterShape;
        counterShape.uncache();
        counterShape.graphics.clear();
        counterShape.graphics.beginFill("#000000").drawRect(0, 0, 140, 331);
        counterShape.cache(0, 0, 140, 331);
        counterShape.size = {width:140, height:331};
        counterShape.x = 6;
        counterShape.y = 77 + yCorrector;
        counterShape.mouseEnabled = false;

        this.counterRadius = 36;
        this.strokeTickness = 5;
        var counterCircleBackground = this.counterCircleBackground;
        counterCircleBackground.graphics.clear();
        counterCircleBackground.graphics.setStrokeStyle(this.strokeTickness).beginStroke("#555153").arc(0, 0, this.counterRadius, 0, (Math.PI/180) * 360).endStroke();
        counterCircleBackground.x = counterShape.x + 70;
        counterCircleBackground.y = counterShape.y + 45;

        this.counterCircle.x = counterCircleBackground.x;
        this.counterCircle.y = counterCircleBackground.y;

        this.confirmEnterText.font = "14px HurmeGeometricSans3";
        this.confirmEnterText.x = labelShape.x + 70;
        this.confirmEnterText.y = labelShape.y + 22;

        this.confirmEnterCounter.font = "26px HurmeGeometricSans3";
        this.confirmEnterCounter.x = this.confirmEnterText.x;
        this.confirmEnterCounter.y = counterCircleBackground.y - 3;

        this.secondsText.font = "9px HurmeGeometricSans3";
        this.secondsText.x = this.confirmEnterCounter.x;
        this.secondsText.y = counterCircleBackground.y + 13;

        this.stakeLabel.font = "12px HurmeGeometricSans3";
        this.stakeLabel.x = this.confirmEnterText.x;
        this.stakeLabel.y = 266;

        this.infoText.font = "12px HurmeGeometricSans3";
        this.infoText.x = this.confirmEnterText.x;
        this.infoText.y = this.confirmEnterText.y + 340;
        this.infoText.lineHeight = 15;
    }


    alteastream.BetEntranceCounter = createjs.promote(BetEntranceCounter,"AbstractCounter");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var EntranceCounter = function(){
        this.AbstractCounter_constructor();
        this.initEntranceCounter();
    };

    var p = createjs.extend(EntranceCounter,alteastream.AbstractCounter);

    var _this;

    p.thinCircle = null;

    p.initEntranceCounter = function () {
        console.log("EntranceCounter initialized:::");
        _this = this;
        this.strokeTickness = 19;
        this.counterRadius = 250;
        this.degree = 288;
        this.colors = ["#f9c221", "#ff0000"];

    };

    EntranceCounter.getInstance = function() {
        return _this;
    };

    p.setLayout = function(){
        var counterCircleBackground = this.counterCircleBackground = new createjs.Shape();
        counterCircleBackground.graphics.clear().setStrokeStyle(19).beginStroke("#555153").arc(0, 0, 250, 0, (Math.PI/180) * 288).endStroke();
        counterCircleBackground.rotation = -234;
        counterCircleBackground.x = 860;
        counterCircleBackground.y = 390;
        counterCircleBackground.mouseEnabled = false;
        this.addChild(counterCircleBackground);

        var counterCircle = this.counterCircle = new createjs.Shape();
        counterCircle.rotation = -234;
        counterCircle.x = counterCircleBackground.x;
        counterCircle.y = counterCircleBackground.y;
        counterCircle.mouseEnabled = false;
        this.addChild(counterCircle);

        var thinCircle = this.thinCircle = new createjs.Shape();
        thinCircle.graphics.clear().setStrokeStyle(3).beginStroke("#744006").arc(0, 0, 320, 0, (Math.PI/180) * 328).endStroke();
        thinCircle.rotation = -254;
        thinCircle.x = 860;
        thinCircle.y = 390;
        thinCircle.mouseEnabled = false;
        this.addChild(thinCircle);

        this._createText("confirmEnterCounter","0","bold 130px HurmeGeometricSans3", "#ffffff",{x:860,y:280,textAlign:"center",textBaseline:"middle"});//1332
        this._createText("secondsText","SECONDS","18px HurmeGeometricSans3", "#ffffff",{x:this.confirmEnterCounter.x,y:this.confirmEnterCounter.y + 100,textAlign:"center",textBaseline:"middle"});//1332
        this._createText("infoText","READY FOR YOU!\nCONFIRM PLAY!\nGOOD LUCK!","bold 24px HurmeGeometricSans3", "#ffffff",{x:this.confirmEnterCounter.x,y:this.confirmEnterCounter.y + 180,textAlign:"center",textBaseline:"middle"});//1332
        this.infoText.lineHeight = 30;
    };

    p.adjustMobile = function(){
        this.strokeTickness = 10;
        this.counterRadius = 145;
        this.degree = 268;

        this.confirmEnterCounter.font = "bold 68px HurmeGeometricSans3"
        this.confirmEnterCounter.x = 470;
        this.confirmEnterCounter.y = 160;

        this.secondsText.font = "12px HurmeGeometricSans3"
        this.secondsText.x = this.confirmEnterCounter.x;
        this.secondsText.y = this.confirmEnterCounter.y + 50;

        this.infoText.font = "bold 14px HurmeGeometricSans3";
        this.infoText.x = this.confirmEnterCounter.x;
        this.infoText.y = this.confirmEnterCounter.y + 90;
        this.infoText.lineHeight = 15;

        var counterCircleBackground = this.counterCircleBackground
        counterCircleBackground.graphics.clear().setStrokeStyle(10).beginStroke("#555153").arc(0, 0, 145, 0, (Math.PI/180) * this.degree).endStroke();
        counterCircleBackground.rotation = -224;
        counterCircleBackground.x = alteastream.AbstractScene.GAME_WIDTH/2-10;
        counterCircleBackground.y = alteastream.AbstractScene.GAME_HEIGHT/2-38;

        var counterCircle = this.counterCircle;
        counterCircle.graphics.clear().setStrokeStyle(10).beginStroke("#555153").arc(0, 0, 145, 0, (Math.PI/180) * this.degree).endStroke();
        counterCircle.rotation = -224;
        counterCircle.x = counterCircleBackground.x;
        counterCircle.y = counterCircleBackground.y;

        var thinCircle = this.thinCircle;
        thinCircle.graphics.clear().setStrokeStyle(1).beginStroke("#744006").arc(0, 0, 200, 0, (Math.PI/180) * 308).endStroke();
        thinCircle.rotation = -244;
        thinCircle.x = counterCircleBackground.x;
        thinCircle.y = counterCircleBackground.y;
    };

    p._local_activate = function() {
        this._updateCounter = function(count){
            var queuePanel = alteastream.QueuePanel.getInstance();
            if(queuePanel._firstInQueue){
                this.show(true);
                queuePanel.btnStart.visible = true;
                if(count>-1)
                    this.confirmEnterCounter.text = count;
                queuePanel.btnStart.setDisabled(false);
            }else{
                queuePanel.btnStart.setDisabled(true);
                queuePanel.btnStart.visible = false;
                this.show(false);
            }
        }.bind(this);
    }

    alteastream.EntranceCounter = createjs.promote(EntranceCounter,"AbstractCounter");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LocationMachines = function(lobby) {
        this.Container_constructor();
        this.initialize_LocationMachines(lobby);
    };
    var p = createjs.extend(LocationMachines, createjs.Container);

    var _this = null;
// static properties:
// events:

    // public properties:
    p.lobby = null;
    p.gridLayoutIsActive = true;
    p.isFocused = false;
    p.machineCoinsAndPrizesInfo = false;
    // private properties:
    p._machinesComponent = null;
    p._machinesPerPage = 0;
    p._canUpdateThumbs = true;
    p._backToLobbyButton = null;
    p._locationLabel = null;
    p._locationName = null;
    p._scroller = null;
    p._pagination = null;
    p._showOnlyAvailable = false;
    p._gridLayoutScrollLevel = 0;

// constructor:
    p.initialize_LocationMachines = function(lobby) {
        console.log("LocationMachines initialized::: ");
        _this = this;
        this.setComponents(lobby);
    };

    LocationMachines.getInstance = function(){
        return _this;
    };

    // private methods:
    p._soundNotify = function(){
        alteastream.Assets.playSound("confirm",0.3);
    };

    p._onFocusDone = function() {
        this.isFocused = true;
        this.setMachinesAndScroll();
        this.showMachinesHideShape(true);
        this.intervalUpdateLobby(true);
        this.lobby.setFocused(this);
        this.lobby.setSwipeTarget(this,"y");
    };

    p._showAvailableOrAllButtonsHandler = function() {
        this._machinesComponent.showOnlyAvailableOrAllMachines(this._showOnlyAvailable);
        this.updateAvailableOrAllMachinesButtonsSkin(this._showOnlyAvailable);
        this._resetScrollComponent(false);
    };

    p._storeLocationName = function() {
        this._locationName = this.lobby.selectedLocation.casinoName.charAt(0).toUpperCase() + this.lobby.selectedLocation.casinoName.substring(1);
    };

    p._storeGridScrollLevel = function () {
        this._gridLayoutScrollLevel = this._scroller.scrollByRowCurrentLevel;
    };

    // filtering
    p._resetScrollComponent = function(listView){
        console.log("RESET SCROLL COMP");
        if(listView === true) {
            this._storeGridScrollLevel();
        }
        var numOfPages = this._machinesComponent.getNumberOfPages();
        var machineHeight = this._machinesComponent.getMachineHeight();
        this._scroller.maxScrollLevelByPage = numOfPages - 1;

        var scrollLevelsByRow = [];
        var numberOfRows = Math.ceil(this._machinesComponent.getNumberOfActiveMachines() / this._machinesComponent.getColumnsPerPage());

        var numberOfScrollLevelsByRow = numberOfRows - this._machinesComponent.getRowsPerPage() + 1;
        for(var j = 0; j < numberOfScrollLevelsByRow; j++){
            scrollLevelsByRow.push((j * machineHeight * -1));
        }

        this._scroller.setScrollLevels(scrollLevelsByRow);
        this._scroller.setRowsPerPage(this._machinesComponent.getRowsPerPage());
        this._scroller.setNumberOfPages(numOfPages);
        this._machinesComponent.setAcordionsLayout(listView);
        if(this._machinesComponent.selectedAcordion !== null){
            if(numberOfScrollLevelsByRow > 0){
                this._scroller.setToFirstPage(true, 200);
            }
        }
    };

    p._updateMachinesLabel = function(){
        var numOfMachines = " " + this._machinesComponent.getNumberOfActiveMachines();
        var moreThanOne = numOfMachines > 1 ? " machines" : " machine";
        this._locationLabel.text = this.gridLayoutIsActive ? this._locationName + numOfMachines + moreThanOne : numOfMachines + moreThanOne + " available";
    };

    // public methods
    p.setComponents = function(lobby) {
        this.lobby = lobby;

        var rowsPerPage = 3;
        var columnsPerPage = 4;
        this._machinesPerPage = rowsPerPage * columnsPerPage;
        var machinesComponent = this._machinesComponent = new alteastream.MachinesComponent(rowsPerPage, columnsPerPage);

        var maskWidth = machinesComponent.getWidth();
        var maskHeight = machinesComponent.getMaskHeight();
        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(machinesComponent._startPositionX, machinesComponent._startPositionY, maskWidth, maskHeight);
        mask.setBounds(machinesComponent._startPositionX, machinesComponent._startPositionY, maskWidth, maskHeight);
        machinesComponent.mask = mask;

        var topSectionBackground = this._topSectionBackground = new createjs.Shape();
        topSectionBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 1679, 50, 4);
        topSectionBackground.alpha = 0.75;
        topSectionBackground.cache(0, 0, 1679, 50);
        topSectionBackground.x = 22;
        topSectionBackground.y = -56;

        var streamOverlay = this.streamOverlay = new createjs.Shape();
        streamOverlay.graphics.beginFill("#000000").drawRect(0, 0, 1920, 1080);
        streamOverlay.alpha = 0.4;
        streamOverlay.cache(0, 0, 1920, 1080);
        streamOverlay.visible = false;
        streamOverlay.x = -100;
        streamOverlay.y = -121;

        var buttonBackToLobbyTxt = new createjs.Text("BACK TO LOBBY","13px HurmeGeometricSans3","#ffffff").set({textAlign:"left"});
        var backToLobbyButton = this._backToLobbyButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBack),2,buttonBackToLobbyTxt);
        backToLobbyButton.centerText(4, 2);
        backToLobbyButton.visible = false;
        backToLobbyButton.x = -55 + (backToLobbyButton._singleImgWidth*0.5);
        backToLobbyButton.y = -115 + (backToLobbyButton._height*0.5);
        backToLobbyButton.setClickHandler(function () {
            _this.backToLobbyButtonHandler();
            //new socket
            console.log("leaveQueue:: backToLobbyButtonHandler");
            _this.queuePanel.leaveQueue(function () {
                console.log("no socket? getMachines here ?? ");
                //_this.intervalUpdateLobby(true);// if back from entranceCounter?
            })
        });

        var streamPreview = this.streamPreview = new alteastream.StreamPreview();
        streamPreview.mouseEnabled = false;
        streamPreview.mouseChildren = false;
        streamPreview.show(false);

        var _locationLabelShape = this._locationLabelShape = new createjs.Shape();
        _locationLabelShape.graphics.beginFill("#000000").drawRect(0, 0, 232, 50, 4);
        _locationLabelShape.alpha = 0.7;
        _locationLabelShape.cache(0, 0, 232, 50);
        _locationLabelShape.x = 21;
        _locationLabelShape.y = -56;

        var _locationLabel = this._locationLabel = new createjs.Text("","bold 16px HurmeGeometricSans3","#a0a0a0").set({textAlign:"left", textBaseline:"middle", mouseEnabled:false, x:36, y:-29, visible:false});
        var divider = this.divider = new createjs.Text("|","22px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", x:1586, y:-30});
        var queuePanel = this.queuePanel = new alteastream.QueuePanel();
        var area = new createjs.Shape();
        area.graphics.beginFill("#fff").drawRect(-130, -26, 130, 50);

        var showAllMachinesButton = this.showAllMachinesButton = new createjs.Text("All Machines","22px HurmeGeometricSans3","#2aff00").set({textAlign:"right", textBaseline:"middle", x:divider.x - 10, y:divider.y});
        showAllMachinesButton.hitArea = area;
        showAllMachinesButton.on("click", function () {
            if(showAllMachinesButton.isActive === false){
                _this._showOnlyAvailable = false;
                _this._showAvailableOrAllButtonsHandler();
            }
        });

        var area2 = new createjs.Shape();
        area2.graphics.beginFill("#71c8e0").drawRect(0, -26, 130, 50);

        var showAvailableMachinesButton = this.showAvailableMachinesButton = new createjs.Text("Available","22px HurmeGeometricSans3","#ffffff").set({textAlign:"left", textBaseline:"middle", x:divider.x + 10, y:divider.y});
        showAvailableMachinesButton.hitArea = area2;
        showAvailableMachinesButton.on("click", function () {
            if(showAvailableMachinesButton.isActive === false){
                _this._showOnlyAvailable = true;
                _this._showAvailableOrAllButtonsHandler();
            }
        });

        showAllMachinesButton.isActive = showAvailableMachinesButton.isActive = false;

        var scroller = this._scroller = new alteastream.Scroller(machinesComponent);
        scroller.setScrollType("scrollByRow");
        scroller.setScrollDirection("y");

        scroller.setPreAnimateScrollMethod(function () {
            scroller.manageButtonsState();
            machinesComponent.mouseChildren = false;
            _this._canUpdateThumbs = false;
        });

        scroller.setPostAnimateScrollMethod(function () {
            _this._canUpdateThumbs = true;
            machinesComponent.mouseChildren = true;
        });

        var machineCoinsAndPrizesInfo = this.machineCoinsAndPrizesInfo = new alteastream.MachineCoinsAndPrizesInfo();
        machineCoinsAndPrizesInfo.visible = false;

        this.addChild(
            topSectionBackground,
            streamOverlay,
            streamPreview,
            machinesComponent,
            _locationLabelShape,
            _locationLabel,
            machineCoinsAndPrizesInfo,
            queuePanel,
            backToLobbyButton,
            //machineCoinsAndPrizesInfo,
            divider,
            showAllMachinesButton,
            showAvailableMachinesButton
        );
    };

    p.showStreamOverlay = function(show) {
        this.streamOverlay.visible = show;
    };

    p.updateAvailableOrAllMachinesButtonsSkin = function(onlyAvailable) {
        this.showAllMachinesButton.color = onlyAvailable === true ? "#ffffff" : "#2aff00";
        this.showAvailableMachinesButton.color = onlyAvailable === true ? "#2aff00" : "#ffffff";
        this.showAllMachinesButton.isActive = !onlyAvailable;
        this.showAvailableMachinesButton.isActive = onlyAvailable;
    }

    p.backToLocationsButtonHandler = function() {
        if(this.lobby.mouseWheel)
            this.lobby.mouseWheel.enable(false);
        this.lobby.backToLocationsFromLobby();

        // blok premesten iz onStreamPreviewDone
        this._machinesComponent.clearActiveMachines();
        this._machinesComponent.disableActiveMachines(true);
        this.lobby.currentMap.clickEnabled(true);
        this.lobby.setBackgroundImagesForStreamPreview(false);
    };

    p.backToLobbyButtonHandler = function() {
        this.streamPreview.exitPreview();
        this._backToLobbyButton.visible = false;
        this.setBackButtonForEntranceCounter(false);
        this.intervalUpdateLounge(false);
        this.intervalUpdateLobby(true);

        this._machinesComponent.clearActiveMachines();
        this.setComponentWhenMachineisFreeToPlay(false);
        this.updateAvailableOrAllMachinesButtonsSkin(false);
        this.queuePanel.exitQueue();
        this.setListLayout(false);

        this.machineCoinsAndPrizesInfo.showPrizes(true);
        this.machineCoinsAndPrizesInfo.visible = false;

        if(this._showOnlyAvailable === true){
            this._showAvailableOrAllButtonsHandler();
        }

        //new socket remove

        this._setScrollPosition();
        this._soundNotify();
    };

    p.tryLobby = function(){
        if(this.gridLayoutIsActive === false){
            this.backToLobbyButtonHandler();
        }
    };

    // filtering
    p.setListLayout = function(setList) {
        this.gridLayoutIsActive = !setList;
        this.lobby.setBackgroundImagesForStreamPreview(setList);
        this.lobby.balanceBackground.visible = setList;

        if(this._pagination){
            if(this._pagination._numOfPages > 1){
                this._pagination.showPagination(!setList);
            }
        }

        this._machinesComponent.setGridView(!setList);
        this._machinesComponent.setShowOnlyAvailable(setList);

        this._backToLobbyButton.visible = setList;
        this._backToLobbyButton.x = setList ? 21 : -55;
        this._backToLobbyButton.y = setList ? -110 : -115;

        this._backToLobbyButton.x += this._backToLobbyButton._singleImgWidth*0.5;
        this._backToLobbyButton.y += this._backToLobbyButton._height*0.5;

        var skin = setList ? "btnBack2" : "btnBack";
        this._backToLobbyButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[skin]));

        this._locationLabelShape.visible = setList;
        this._topSectionBackground.visible = !setList;

        this._resetScrollComponent(setList);
        this._updateMachinesLabel();

        this.showAllMachinesButton.visible = !setList;
        this.showAvailableMachinesButton.visible = !setList;
        this.divider.visible = !setList;

        this.lobby.hideBackgroundsForFullScreenVideo(setList);

        this._locationLabel.x = setList ? 31 : 36;

        this.showStreamOverlay(setList);
    };

    p.onMachineClick = function(machine){// ovde mozda zapamtiti stanje masine tj isFreeToPlay ili setovati neki property u queuePanel
        alteastream.AbstractScene.SHOP_MACHINE = machine.machineInfo.house;
        this.lobby.checkIfHiddenMenuIsShown();
        this.setComponentWhenMachineisFreeToPlay(machine.isFreeToPlay);
        this.streamPreview.tryToSwitchStream(machine);
        if(alteastream.AbstractScene.HAS_COPLAY === true){
            this.queuePanel.multiPlayerControls.showCoinsWinComponent(machine.gameCode === "ticketcircus");
            this.queuePanel.multiPlayerControls.showMultiPlayerControls(!machine.isFreeToPlay);
        }
        if(this.gridLayoutIsActive === true){
            this.visible = false;
            this.setListLayout(true);
            this.queuePanel.enterQueue();
        }else{
            this.queuePanel.exitQueue();
            console.log("leaveQueue:: onMachineClick");
            this.queuePanel.leaveQueue(function () {
                _this.queuePanel.enterQueue();
            });
        }

        this._machinesComponent.clearActiveMachines();
        this.intervalUpdateLobby(false);
    };

    p.setComponentWhenMachineisFreeToPlay = function(isFreeToPlay){
        this._machinesComponent.visible = !isFreeToPlay;
        this._locationLabel.visible = !isFreeToPlay;

        if(alteastream.AbstractScene.HAS_COPLAY)
            this.queuePanel.multiPlayerControls.enable(!isFreeToPlay);// ovo mozda da se ne desava na click nego na onQueueEnter

        this.queuePanel.updateAvailabilityComponents(isFreeToPlay);
        this.queuePanel.bannerContainer.visible = !isFreeToPlay;
        this.machineCoinsAndPrizesInfo.visible = !isFreeToPlay;
        if(this._pagination){
            if(this._pagination._numOfPages > 1) {
                this._pagination.showPagination(!isFreeToPlay);
            }
        }
    };

    p.setBackButtonForEntranceCounter = function(setForCounter){
        var imageName = setForCounter === true ? "btnBackForEntranceCounter" : "btnBack";
        var button = this._backToLobbyButton;
        button.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[imageName]));
        button.x = setForCounter === true ? 900 : 111;
        button.y = setForCounter === true ? 701 : -87;
        var buttonTextX = setForCounter === true ? -40 : 4;
        var buttonTextY = setForCounter === true ? 4 : 2;
        button.centerText(buttonTextX, buttonTextY);
        this.lobby.balanceBackground.visible = false;
    };

    p.populateMachines = function(response){
        this._machinesComponent.populateAllMachines(response);
        var filterSelection = window.localStorage.getItem("filterSelection");
        if(filterSelection !== null) {
            if(filterSelection === "onlyAvailable"){
                this._showOnlyAvailable = true;
                this._showAvailableOrAllButtonsHandler();
            }
        }
    };

    //new socket
    p.intervalUpdateLobby = function(bool){
        if(bool === true){
            this._machinesComponent.updateThumbnails();
            //var that = this;
            this.thumbnailTimer = setInterval(function(){
                console.log(" intervalUpdateLobby>>>>>>>>>>>>>>>>>>>");
                if(_this._canUpdateThumbs === true){
                    if(_this.gridLayoutIsActive === true){
                        _this._machinesComponent.updateThumbnails();
                    }
                }
            },5000);
        }
        else
            clearInterval(this.thumbnailTimer);
    };

    p.intervalUpdateLounge = function(bool){
        //var that = this;
        if(bool === true)
            //thumbnailPropsTimer
            this.propsTimer = setInterval(function(){
                _this.updateLoungeComponents();
            },2000);
        else
            clearInterval(this.propsTimer);
    };

    /*p.intervalUpdateLounge = function(bool){ // todo mozda kasnije sa setTimeout
        //var that = this;
        if(bool === true){
            this.updateLoungeComponents();
            this.propsTimer = setTimeout(function(){
                _this.intervalUpdateLounge(true);
            },2000);
        }

        else
            clearTimeout(this.propsTimer);
    };*/


    //new socket
    /*
    * biggestGameWin: 28
    isFreeToPlay: false
    is_available: true
    lastWin: 6
    name: "4008"
    state: 1
    status: 0
    status_code: 1
    tokensFired: 1176
    totalWin: 38
    */
    p.updateLoungeComponents = function(){
        if(this.streamPreview._active){
            alteastream.Requester.getInstance().getCurrentMachine(this.streamPreview.currentStream,function(response){
                _this.machineCoinsAndPrizesInfo.updateProps(response);
                //_this.streamPreview.updateProps(response);
            });
        }
    };

    p.resetIntervalUpdateLounge = function(){
        this.intervalUpdateLounge(false);
        this.intervalUpdateLounge(true);
        this.updateLoungeComponents();
    };

// static methods:
// public methods:
    p.focusIn = function(){
        this.visible = true;

/*        if(alteastream.AbstractScene.BACK_TO_HOUSE !== -1){
            var backgroundMusicIsMuted = window.localStorage.getItem('backgroundMusicIsMuted');
            createjs.Sound.muted = backgroundMusicIsMuted === "false";
            this.lobby.btnSoundHandler();
        }*/

        this.focusInRegular();
    };

    p.focusInRegular = function() {
        this._onFocusDone();
        this._restoreScrollPosition();
    };

    p.focusInAnimated = function() {
        this._onFocusDone();
    };

    p.showMachinesHideShape = function(bool) {
        if(this._pagination){
            if(this._pagination._numOfPages > 1) {
                this._pagination.showPagination(bool);
            }
            this._scroller.manageButtonsState();
        }
    };

    p.focusOut = function(objectIn){
        this.intervalUpdateLobby(false);
        this.showMachinesHideShape(false);
        objectIn.focusIn();
    };

    // filtering
    p.setMachinesAndScroll = function() {
        this._storeLocationName();
        this._updateMachinesLabel();
        //this._resetScrollComponent(false);
        this._machinesComponent.disableActiveMachines(false);
    };

    p.onMouseWheel = function(direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

/*    p.restoreScrollLevelAndSelectedMachine = function() {
        var scrollLevel = Number(window.localStorage.getItem('scrollLevel'));
        if(scrollLevel > 0){
            this._scroller.scrollByPageCurrentLevel = scrollLevel;
            this._scroller.scrollByPage();
        }
    };*/

    p._restoreScrollPosition = function() {// from gameplay return
        if(this._pagination){
            if(this._pagination._numOfPages > 1) {
                this._scroller.scrollByRowCurrentLevel = this._gridLayoutScrollLevel = Number(window.localStorage.getItem('scrollLevel'));
                this._setScrollPosition();
            }
        }
    };

    p._setScrollPosition = function () {// lobby always on back to lobby
        if(this._pagination) {
            if (this._pagination._numOfPages > 1) {
                this._scroller.scrollByRowCurrentLevel = this._gridLayoutScrollLevel;
                this._scroller.scrollByRow(false);
            }
        }
    };

    //StreamPreview callbacks
    //QueuePanel callbacks
    p.onQueueEntered = function(){
        this.lobby.currentMap.clickEnabled(false);
        this._soundNotify();
    };

    p.onStartGame = function(){// invalid token fix new
        this._backToLobbyButton.visible = false;
        this.queuePanel.btnStart.setDisabled(true);
        //var that = this;
        alteastream.Requester.getInstance().startGameStream(function(response){

            // temp roulette test
            //if(_this.lobby.selectedLocation.casinoName === "Roulette"){
            if(response.gameCode === "roulette"){//response.gamecode
                response.minStake = 0.20;
                response.maxStake = 5.00;
            }

            var resp = JSON.stringify(response);

            if(_this.lobby.mouseWheel)
                _this.lobby.mouseWheel.enable(false);

            _this.lobby.onStartGameStream();
            _this.intervalUpdateLobby(false);
            _this.lobby.socketCommunicator.disposeCommunication();

            _this.queuePanel.removeAllChildren();

            _this.streamPreview.dispose();
            _this.intervalUpdateLounge(false);

            var vid = document.getElementById("videoOutput");
            vid.parentNode.removeChild(vid);

            alteastream.Assets.stopAllSounds();

            setTimeout(function(){_this.goToMachine(resp);},1000);
        }, function () {
            _this._backToLobbyButton.visible = false;
            _this.queuePanel.btnStart.setDisabled(true);
            _this.backToLobbyButtonHandler();
            //new socket
            console.log("leaveQueue:: backToLobbyButtonHandler");
            _this.queuePanel.leaveQueue(function () {
                console.log("no socket? getMachines here ?? ");
                //_this.intervalUpdateLobby(true);// if back from entranceCounter?
            })
        })
    };

    p.goToMachine = function(response){
        //var gameDir = this.lobby.selectedLocation.casinoName === "Roulette" ? "/gameplayRoulette/" : "/gameplay/";
        //var gameDir = this._machinesComponent.getActiveMachine().gameCode === "roulette" ? "/gameplayRoulette/" : "/gameplay/";
        var gameDir = response.gameCode === "roulette" ? "/gameplayRoulette/" : "/gameplay/";
        var filterSelection = this._showOnlyAvailable === true ? "onlyAvailable" : "all";
        window.localStorage.setItem('backToHouse', String(alteastream.AbstractScene.BACK_TO_HOUSE));
        window.localStorage.setItem('machineParameters', response);// invalid token fix new
        window.localStorage.setItem('scrollLevel', String(this._gridLayoutScrollLevel));
        window.localStorage.setItem('backgroundMusicIsMuted', String(createjs.Sound.muted));
        window.localStorage.setItem('filterSelection', filterSelection);
        window.localStorage.setItem('hasCoplay', String(alteastream.AbstractScene.HAS_COPLAY));

        var qStr = "?usr="+alteastream.AbstractScene.GAME_TOKEN+"&serverUrl="+alteastream.AbstractScene.SERVER_URL+"&machine_name="+alteastream.AbstractScene.GAME_ID+"&game="+"rmgame"+"&gameCode="+alteastream.AbstractScene.GAME_CODE+"&gameName="+this.streamPreview.currentMachineName;
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_PROTOCOL+alteastream.AbstractScene.SERVER_IP+"/gameplay/"+qStr);
        window.parent.changeIFrameSrc(alteastream.AbstractScene.GAME_PROTOCOL+alteastream.AbstractScene.SERVER_IP+gameDir+qStr);
    };

    alteastream.LocationMachines = createjs.promote(LocationMachines,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LocationMachinesMobile = function(lobby) {
        this.LocationMachines_constructor(lobby);
        this.initialize_LocationMachinesMobile();
    };

    var p = createjs.extend(LocationMachinesMobile, alteastream.LocationMachines);

// static properties:
// events:
// private properties:
// constructor:
    p.initialize_LocationMachinesMobile = function() {};
    // public properties:
    p.swipeCatcher = null;

    // private methods:
    p._superShowMachinesHideShape = p.showMachinesHideShape;
    p.showMachinesHideShape = function(bool){
        this._superShowMachinesHideShape(bool);
        var btnImage = bool === false ? "btnHiddenMenu" : "btnHiddenMenuTransparent";
        this.lobby._hiddenMenuButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));
    };

    p._superUpdateMachinesLabel = p._updateMachinesLabel;
    p._updateMachinesLabel = function(setList){
        this._superUpdateMachinesLabel(setList);
        this._locationLabel.x = this.gridLayoutIsActive ? 473 : 101;
        this._locationLabel.y = this.gridLayoutIsActive ? 35 : 102;
        this._locationLabel.font = this.gridLayoutIsActive ? "16px HurmeGeometricSans3" : "14px HurmeGeometricSans3";
    };

    p._manageButtonsState = function() {};
    p._setButtonsStateOnStart = function(numOfPages) {};
    p._showButtons = function(show) {};
    p._updateButtonsPosition = function(listView){};

    // static methods:
    // public methods:
    p.setComponents = function(lobby) {
        var _this = this;
        this.lobby = lobby;

        var rowsPerPage = 2;
        var columnsPerPage = 3;

        this._machinesComponent = new alteastream.MachinesComponent(rowsPerPage, columnsPerPage);
        this._machinesPerPage = rowsPerPage * columnsPerPage;

        var maskWidth = this._machinesComponent.getWidth();
        var maskHeight = this._machinesComponent.getMaskHeight();

        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(this._machinesComponent._startPositionX, this._machinesComponent._startPositionY, maskWidth, maskHeight);
        this._machinesComponent.mask = mask;
        this._machinesComponent.mask.setBounds(this._machinesComponent._startPositionX, this._machinesComponent._startPositionY, maskWidth, maskHeight);
        //this._machinesComponent.visible = false;
        this._machinesComponent.adjustMobile();

        var topSectionBackground = this._topSectionBackground = new createjs.Shape();
        topSectionBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 933, 60, 2);
        topSectionBackground.alpha = 0.75;
        topSectionBackground.cache(0, 0, 933, 60);
        topSectionBackground.x = 5;
        topSectionBackground.y = 5;
        this.addChild(topSectionBackground);

        var streamOverlay = this.streamOverlay = new createjs.Shape();
        streamOverlay.graphics.beginFill("#000000").drawRect(0, 0, 960, 540);
        streamOverlay.alpha = 0.4;
        streamOverlay.cache(0, 0, 960, 540);
        streamOverlay.visible = false;
        streamOverlay.x = -10;
        streamOverlay.y = -10;
        this.addChild(streamOverlay);

        var streamPreview = this.streamPreview = new alteastream.StreamPreview();
        this.addChild(streamPreview);
        streamPreview.mouseEnabled = false;
        streamPreview.mouseChildren = false;
        streamPreview.show(false);
        streamPreview.adjustMobile();

        var swipeCatcher = this.swipeCatcher = new createjs.Shape();
        swipeCatcher.graphics.beginFill("#000000").drawRect(-10, -10, 960, 540);
        swipeCatcher.cache(-10, -10, 960, 540);
        swipeCatcher.visible = false;
        swipeCatcher.alpha = 0.01;
        this.addChild(swipeCatcher);

        this.addChild(this._machinesComponent);

        /*        var streamPreview = this.streamPreview = new alteastream.StreamPreview();
                this.addChild(streamPreview);
                streamPreview.mouseEnabled = false;
                streamPreview.mouseChildren = false;
                streamPreview.show(false);
                streamPreview.adjustMobile();*/

        var _locationLabelShape = this._locationLabelShape = new createjs.Shape();
        _locationLabelShape.graphics.beginFill("#000000").drawRect(0, 0, 192, 45);
        _locationLabelShape.alpha = 0.7;
        _locationLabelShape.cache(0, 0, 192, 45);
        _locationLabelShape.x = 8;
        _locationLabelShape.y = 80;
        _locationLabelShape.visible = false;

        var _locationLabel = this._locationLabel = new createjs.Text("Mock text","16px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", mouseEnabled:false, x:45, y:35, visible:false});
        this.addChild(_locationLabelShape, _locationLabel);

        var entranceCounterBackground = this._entranceCounterBackground = alteastream.Assets.getImage(alteastream.Assets.images.infoBackground);
        entranceCounterBackground.visible = false;
        entranceCounterBackground.x = -10;//this.x = 10
        entranceCounterBackground.y = -10;//this.y = 10
        this.addChild(entranceCounterBackground);

        var machineCoinsAndPrizesInfo = this.machineCoinsAndPrizesInfo = new alteastream.MachineCoinsAndPrizesInfo();
        machineCoinsAndPrizesInfo.adjustMobile();
        machineCoinsAndPrizesInfo.visible = false;
        this.addChild(machineCoinsAndPrizesInfo);

        var queuePanel = this.queuePanel = new alteastream.QueuePanel();
        this.addChild(queuePanel);
        queuePanel.adjustMobile();

        var buttonBackToLobbyTxt = new createjs.Text("BACK TO LOBBY","13px HurmeGeometricSans3","#ffffff").set({textAlign:"left"});
        var backToLobbyButton = this._backToLobbyButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBack),2,buttonBackToLobbyTxt);
        backToLobbyButton.visible = false;
        backToLobbyButton.text.visible = false;
        backToLobbyButton.x = 79;
        backToLobbyButton.y = 25;
        backToLobbyButton.setClickHandler(function () {
            _this.backToLobbyButtonHandler();
            _this.lobby.checkIfHiddenMenuIsShown();
        });
        this.addChild(backToLobbyButton);

/*        var machineCoinsAndPrizesInfo = this.machineCoinsAndPrizesInfo = new alteastream.MachineCoinsAndPrizesInfo();
        machineCoinsAndPrizesInfo.adjustMobile();
        machineCoinsAndPrizesInfo.visible = false;
        this.addChild(machineCoinsAndPrizesInfo);*/

        var divider = this.divider = new createjs.Text("|","14px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", x:780, y:36});

        var area = new createjs.Shape();
        area.graphics.beginFill("#fff").drawRect(-82, -28, 82, 56);

        var showAllMachinesButton = this.showAllMachinesButton = new createjs.Text("All Machines","14px HurmeGeometricSans3","#2aff00").set({textAlign:"right", textBaseline:"middle", x:divider.x - 10, y:divider.y});
        showAllMachinesButton.hitArea = area;
        showAllMachinesButton.on("click", function () {
            if(showAllMachinesButton.isActive === false){
                _this._showOnlyAvailable = false;
                _this._showAvailableOrAllButtonsHandler();
                _this.lobby.checkIfHiddenMenuIsShown();
            }
        });

        var area2 = new createjs.Shape();
        area2.graphics.beginFill("#71c8e0").drawRect(0, -28, 80, 56);

        var showAvailableMachinesButton = this.showAvailableMachinesButton = new createjs.Text("Available","14px HurmeGeometricSans3","#ffffff").set({textAlign:"left", textBaseline:"middle", x:divider.x + 10, y:divider.y});
        showAvailableMachinesButton.hitArea = area2;
        showAvailableMachinesButton.on("click", function () {
            if(showAvailableMachinesButton.isActive === false){
                _this._showOnlyAvailable = true;
                _this._showAvailableOrAllButtonsHandler();
                _this.lobby.checkIfHiddenMenuIsShown();
            }
        });

        //showAllMachinesButton.visible = showAvailableMachinesButton.visible = divider.visible = false;
        showAllMachinesButton.isActive = showAvailableMachinesButton.isActive = false;

        var scroller = this._scroller = new alteastream.Scroller(this._machinesComponent);
        scroller.setScrollType("scrollByRow");
        scroller.setPreAnimateScrollMethod(function () {
            _this._machinesComponent.mouseEnabled = false;
            _this.lobby.checkIfHiddenMenuIsShown();
            if(_this.gridLayoutIsActive === true){
                _this._canUpdateThumbs = false;
            }
        });
        scroller.setPostAnimateScrollMethod(function () {
            _this._machinesComponent.mouseEnabled = true;
            if(_this.gridLayoutIsActive === true){
                _this._canUpdateThumbs = true;
            }
        });

        scroller.setScrollDirection("y");

        // filtering
        this.addChild(divider, showAllMachinesButton,showAvailableMachinesButton);
    };

    p.setListLayout = function(setList) {
        this.gridLayoutIsActive = !setList;
        this.lobby.setBackgroundImagesForStreamPreview(setList);
        this.lobby.balanceBackground.visible = setList;
        this._machinesComponent.setGridView(!setList);
        this._machinesComponent.setShowOnlyAvailable(setList);
        this._backToLobbyButton.visible = setList;
        this._topSectionBackground.visible = !setList;
        this._locationLabelShape.visible = setList;
        this._resetScrollComponent(setList);
        this._updateMachinesLabel();
        this._manageButtonsState();

        var btnImage = setList === true ? "btnHiddenMenu" : "btnHiddenMenuTransparent";
        this.lobby._hiddenMenuButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));

        var skin = setList ? "btnBack2" : "btnBack";
        this._backToLobbyButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[skin]));

        this.showAllMachinesButton.visible = !setList;
        this.showAvailableMachinesButton.visible = !setList;
        this.divider.visible = !setList;

        //var scrollType = setList === true ? "scrollByRow" : "scrollByPage";
        //this._scroller.setScrollType(scrollType);

        this.lobby.hideBackgroundsForFullScreenVideo(setList);
        this.showStreamOverlay(setList);

        this.swipeCatcher.visible = setList;
    };

    p.onSwipe = function (direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    p._superSetBackButtonForEntranceCounter = p.setBackButtonForEntranceCounter;
    p.setBackButtonForEntranceCounter = function(setForCounter){
        this._superSetBackButtonForEntranceCounter(setForCounter);
        this._backToLobbyButton.x = setForCounter === true ? 467 : 79;
        this._backToLobbyButton.y = setForCounter === true ? 415 : 25;
        this._backToLobbyButton.text.visible = setForCounter;
        this._backToLobbyButton.centerText(4,2);
        this.lobby._hiddenMenuButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images.btnHiddenMenu));
        this.lobby._hiddenMenuButton.x = 878;
        this.lobby._hiddenMenuButton.y = 12;
    };

    alteastream.LocationMachinesMobile = createjs.promote(LocationMachinesMobile,"LocationMachines");
})();
this.alteastream = this.alteastream || {};

(function(){
    "use strict";

    var Machine = function(){
        this.Container_constructor();
        this.initMachine();
    };

    var p = createjs.extend(Machine,createjs.Container);

    p.WIDTH = 0;
    p.HEIGHT = 0;
    p.BITMASK_ERROR = parseInt("00000010",2);//0b00000010
    p.BITMASK_MAINTENANCE = parseInt("00001000",2);//0b00001000
    p.STATE_NORMAL = 1;
    p.STATE_ERROR = 3;
    p.STATE_MAINTENANCE = 9;
    p.STATE_OFFLINE = -1;

    p._currentState = false;
    p._dynamicX = 0;
    p._dynamicY = 0;
    p._mouseContainer = null;
    //p._idText = null;
    p._nameText = null;
    p._availabilityText = null;
    p._availabilityImage = null;

    p.isFreeToPlay = false;
    p.isOnline = false;
    p.thumbScaleX = 0;
    p.thumbScaleY = 0;
    p.id = null;
    p.selected = false;
    p.background = null;
    p.overlay = null;
    p.defaultGameId = null;
    p.currency = null;

    p.initMachine = function () {

        this.thumbScaleX = 0.835;
        this.thumbScaleY = 0.85;

        var background = this.background = alteastream.Assets.getImage(alteastream.Assets.images.machineBg);
        var overlay = this.overlay = alteastream.Assets.getImage(alteastream.Assets.images.machineOverlay);

        var thumb = this.thumb = alteastream.Assets.getImage(alteastream.Assets.images.thumb);
        thumb.mouseEnabled = false;
        thumb.visible = false;

        var noThumb = this.noThumb = alteastream.Assets.getImage(alteastream.Assets.images.noThumb);
        noThumb.mouseEnabled = false;
        noThumb.visible = false;

        var frame = this.frame = alteastream.Assets.getImage(alteastream.Assets.images.window);
        frame.mouseEnabled = false;
        frame.visible = false;

        this.WIDTH = frame.image.width;
        this.HEIGHT = frame.image.height;

        var _mouseContainer = this._mouseContainer = new createjs.Container();
        _mouseContainer.addChild(background,thumb,noThumb,overlay,frame);
        this.addChild(_mouseContainer);

        var font = "15px HurmeGeometricSans3";
        var fontBigger = "14px HurmeGeometricSans3";
        var fontBig = "17px HurmeGeometricSans3";
        var white = "#ffffff";
        var black = "#000000";
        var gray = "#828282";
        var yellow = "#ffde1e";

        this._availabilityImage = alteastream.Assets.getImage(alteastream.Assets.images.lampGreen);
        this._availabilityImage.x = background.image.width - this._availabilityImage.image.width - 11;
        this._availabilityImage.y = background.image.height - this._availabilityImage.image.height - 9;
        this.addChild(this._availabilityImage);

        var bottomTextsYPos = 252;

        this._createLabelText("machineNameLabel","ID: ",font,gray,{x:8,y:bottomTextsYPos,textAlign:"left",textBaseline:"middle"});
        this.machineNameLabel.gridYPos = bottomTextsYPos;
        this.machineNameLabel.listYPos = 22;
        this._createText("_nameText","",font,white,{x:30,y:bottomTextsYPos,textAlign:"left",textBaseline:"middle"});
        this._nameText.gridYPos = bottomTextsYPos;
        this._nameText.listYPos = 22;

        this._createLabelText("availabilityLabel","CO-PLAY: ",fontBigger,white,{x:236,y:bottomTextsYPos,textAlign:"right",textBaseline:"middle"});
        this.availabilityLabel.originalXPosition = this.availabilityLabel.x;
        this._createLabelText("queueNumber","0",fontBigger,yellow,{x:this.availabilityLabel.x,y:bottomTextsYPos,textAlign:"right",textBaseline:"middle"});

        var upperTextsYPosition = 32;
        this._createLabelText("stakePolicy","",fontBig,black,{x:8,y:upperTextsYPosition,textAlign:"left",textBaseline:"middle"});
        this.stakePolicy.gridXPos = 8;
        this.stakePolicy.gridYPos = upperTextsYPosition;
        this.stakePolicy.listXPos = 87;
        this.stakePolicy.listYPos = 22;
        this.stakePolicy.gridFontColor = black;
        this.stakePolicy.listFontColor = "#ffff00";

        this._createLabelText("payoutPolicy","",fontBig,white,{x:392,y:upperTextsYPosition,textAlign:"right",textBaseline:"middle"});
        this.payoutPolicy.gridXPos = 392;
        this.payoutPolicy.gridYPos = upperTextsYPosition;
        this.payoutPolicy.listXPos = 225;
        this.payoutPolicy.listYPos = 22;
        this.payoutPolicy.gridFontColor = white;
        this.payoutPolicy.listFontColor = "#ffa500";

        var spinner = this.spinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = 200;
        spinner.y = 45;
        spinner.runPreload(true);
    };

    p.setMachineInfo = function(machine) {
        this._nameText.text = machine.machineName;
        this.setStatus(machine);
        this.id = machine.machineIndex;
        this.machineName = machine.machineName;
        this.gameCode = machine.gameCode;
        this.defaultGameId = machine.defaultGameId;
        this.machineInfo = machine;
    };

    p.resetMachineProperties = function() {
        this.isFreeToPlay = false;
        this.isOnline = false;
        this.id = null;
        this.machineName = null;
        this.machineInfo = null;
        this.gameCode = null;
        this.defaultGameId = null;
    };

    p.setGridMachine = function(setGrid) {
        this.frame.image = setGrid ? alteastream.Assets.getImageURI(alteastream.Assets.images.window) : alteastream.Assets.getImageURI(alteastream.Assets.images.windowList);
        this.background.image = setGrid ? alteastream.Assets.getImageURI(alteastream.Assets.images.machineBg) : alteastream.Assets.getImageURI(alteastream.Assets.images.listMachineBackground);
        this.WIDTH = this.background.image.width;
        this.HEIGHT = this.background.image.height;
        this.overlay.visible = setGrid;
        this.thumb.visible = setGrid;
        if(this.thumb.hasThumbImage === false){
            this.noThumb.visible = setGrid;
        }
        this.spinner.visible = setGrid;
        this._availabilityImage.visible = setGrid;
        this.availabilityLabel.visible = setGrid;
        this.queueNumber.visible = setGrid;

        this.machineNameLabel.y = setGrid ? this.machineNameLabel.gridYPos : this.machineNameLabel.listYPos;
        this._nameText.y = setGrid ? this._nameText.gridYPos : this._nameText.listYPos;

        this.payoutPolicy.font = "14px HurmeGeometricSans3";
        this.payoutPolicy.x = setGrid ? this.payoutPolicy.gridXPos : this.payoutPolicy.listXPos;
        this.payoutPolicy.y = setGrid ? this.payoutPolicy.gridYPos : this.payoutPolicy.listYPos;
        this.payoutPolicy.color = setGrid ? this.payoutPolicy.gridFontColor : this.payoutPolicy.listFontColor;

        this.stakePolicy.x = setGrid ? this.stakePolicy.gridXPos : this.stakePolicy.listXPos;
        this.stakePolicy.y = setGrid ? this.stakePolicy.gridYPos : this.stakePolicy.listYPos;
        this.stakePolicy.color = setGrid ? this.stakePolicy.gridFontColor : this.stakePolicy.listFontColor;

        if(window.localStorage.getItem('isMobile') === "yes"){
            this.machineNameLabel.x = setGrid ? 5 : 5;
            this.machineNameLabel.y = setGrid ? 174 : 22;
            this._nameText.x = setGrid ? 25 : 25;
            this._nameText.y = this.machineNameLabel.y;

            this.stakePolicy.x = setGrid ? 4 : 65;
            this.stakePolicy.y = 22;

            this.payoutPolicy.x = setGrid ? 300 : 188;
            this.payoutPolicy.y = 22;
        }
    };

    p.disableMouseListeners = function(bool){
        if(bool){
            this._mouseContainer.removeAllEventListeners("rollover");
            this._mouseContainer.removeAllEventListeners("rollout");
            this._mouseContainer.removeAllEventListeners("click");
        }else{
            var that = this;
            this._mouseContainer.addEventListener("rollover",function(){
                that._onMouseOver(true);
            });
            this._mouseContainer.addEventListener("rollout",function(){
                that._onMouseOver(false);
            });
            this._mouseContainer.addEventListener("click",function(){
                that._onMouseClick();
            });
        }
    };

    p.showMachine = function(bool) {
        if(this.visible !== bool)
            this.visible = bool;
    };

    p.updateThumbnail = function(){
        var imgName = this.machineInfo.house+"_"+this.machineName;
        alteastream.Assets.getThumbnail(imgName,this.thumb,this.WIDTH,this.HEIGHT,function(){
            this.noThumb.visible = !this.thumb.hasThumbImage;
            if(this.parent.showOnlyAvailable === true){
                if(this.isFreeToPlay === true && this.isOnline === true){
                    this.showMachine(true);
                }
            }else{
                this.showMachine(true);
            }
            if(this.spinner.active){
                this.spinner.runPreload(false);
                this.thumb.scaleX = this.thumbScaleX;
                this.thumb.scaleY = this.thumbScaleY;
                this.thumb.visible = true;
            }
            if(this.selected === true){
                alteastream.StreamPreview.getInstance().changeFrameImage(this.thumb);
            }
        }.bind(this));
    };

    //symbols
    p.setStatus = function(machine){
        // filtering
        this.isFreeToPlay = machine.isFreeToPlay;
        this.isOnline = machine.isOnline;
        this.queueNumber.text = machine.queueSize || 0;
        this.queueNumber.visible = false;

        var image;
        var text = "";
        var textColor = "#ffffff";

        var state = this.STATE_NORMAL;
        if((this.BITMASK_ERROR & machine.statusCode) > 0){
            state = this.STATE_ERROR;
            text = "ERROR";
            textColor = "#ff4646";
            image = "lampRed";
        }else if((this.BITMASK_MAINTENANCE & machine.statusCode) > 0 || machine.isInAutoplay === true){
            state = this.STATE_MAINTENANCE;
            text = "MAINTENANCE";
            textColor = "#ffa02b";
            image = "lampRed";
        }else if(machine.isOnline === false){
            state = this.STATE_OFFLINE;
            text = "OFFLINE";
            textColor = "#ff4646";
            image = "lampRed";
        }else{
            if(machine.isFreeToPlay === true){
                text = "PLAY NOW";
                image = "lampGreen";
            }else{
                if(alteastream.AbstractScene.HAS_COPLAY){
                        text = "CO-PLAY: ";
                    }else{
                        text = "WAITING: ";
                    }
                image = "lampYellow";
                this.queueNumber.visible = true;
            }
        }

        this.setState(state);
        this._availabilityImage.image = alteastream.Assets.getImage(alteastream.Assets.images[image]).image;
        this.availabilityLabel.text = text;
        this.availabilityLabel.color = textColor;
        this.mouseEnabled = state === this.STATE_NORMAL;

        if(this.queueNumber.visible === true){
            this.availabilityLabel.x = this.queueNumber.x - this.queueNumber.getMeasuredWidth();
        }else{
            this.availabilityLabel.x = this.availabilityLabel.originalXPosition;
        }

        //var stakePolicy = machine.policy.stake;
        //var payoutPolicy = machine.policy.payout;
        this.stakePolicy.text = machine.stakename;
/*        var firstPart = payoutPolicy.slice(0, payoutPolicy.length - 3);
        firstPart = firstPart.charAt(0).toUpperCase() + firstPart.slice(1, firstPart.length);
        var secondPart = payoutPolicy.slice(payoutPolicy.length - 3, payoutPolicy.length);
        secondPart = secondPart.charAt(0).toLowerCase() + secondPart.slice(1, secondPart.length);*/
        this.payoutPolicy.text = machine.payoutname;
    };

    p.setCurrency = function (currency) {
        this.currency = currency;
    };

    p.setState = function(state){
        this._currentState = state;
    };

    p.getState = function(){
        return this._currentState;
    };

    p._getStateInfo = function(statusCode){
        var infoText ="";
        switch(statusCode){
            case this.STATE_ERROR:
                infoText = "ERROR";
                break;
            case this.STATE_MAINTENANCE:
                infoText = "MAINTENANCE";
                break;
            case this.STATE_OFFLINE:
                infoText = "OFFLINE";
                break;
        }
        return infoText;
    };

    p.getHeight = function () {
        return this.HEIGHT;
    };

    p.getWidth = function () {
        return this.WIDTH;
    };

    p.setSelected = function (bool) {
        this.selected = bool;
    };

    p.resetDisplay = function () {
        this.selectMachine(false);
    };

    p.markSelected = function(bool){
        this.frame.visible = bool;
    };

    p.doCache = function(bool){
        /* if(bool === true)
             this.cache(0,0,this.WIDTH,this.HEIGHT);
         else
             this.uncache();*/
    };

    p._onMouseOver = function (bool) {
        this.markSelected(bool);
    };

    p.selectMachine = function(bool) {
        this.setSelected(bool);
    };

    p._onMouseClick = function () {
        alteastream.LocationMachines.getInstance().onMachineClick(this);
    };

    p._createLabelText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign,textBaseline:props.textBaseline});
        this._mouseContainer.addChild(textInstance);
    };

    p._createText = function(instance,text,font,color,props){

        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign,textBaseline:props.textBaseline});
        this._mouseContainer.addChild(textInstance);
    };

    p._local_activate = function() {
        var _this = this;
        this._onMouseClick = function () {
            var freetoplayTest = _this.machineInfo.isFreeToPlay;
            var indxToSend = _this.machineInfo.machineName;
            console.log("_onMouseClick this.name "+_this.machineInfo.machineName);
            alteastream.LocationMachines.getInstance().setComponentWhenMachineisFreeToPlay(freetoplayTest);//QueuePanel.manageQueueReadyState
            var streamPreview = alteastream.StreamPreview.getInstance();

            //if(this.currentStream !== _this.machineInfo.machineIndex){
            if(streamPreview.currentStream !== _this.id){
                alteastream.MachinesComponent.getInstance().clearActiveMachines();
                //alteastream.SocketCommunicator.getInstance().lobbySwitchSubscriptions(this.currentStream,machineInfo.machineIndex);
                console.log("lobbySwitchSubscriptions currentStream::"+streamPreview.currentStream);
                console.log("lobbySwitchSubscriptions machineInfo.machineIndex::"+_this.machineInfo.machineIndex);
                // novi wait lounge comment this line
                //streamPreview.switchStream(_this.machineInfo);
                alteastream.QueuePanel.getInstance().setCurrentMachine(_this.machineInfo);
                streamPreview.changeFrameImage(_this.thumb);
                _this.selectMachine(true);
                alteastream.Lobby.getInstance().checkIfHiddenMenuIsShown();
                // filtering
                //alteastream.MachinesComponent.getInstance().setCurrentlySelectedMachineId(_this.machineInfo.machineIndex);// todo proveriti da li moze currentStream umesto machineIndex
                if(alteastream.LocationMachines.getInstance().gridLayoutIsActive){
                    alteastream.LocationMachines.getInstance().setListLayout(true);
                    console.log("machine_index onClick to queue::: "+indxToSend);
                    if(freetoplayTest===true){
                        alteastream.QueuePanel.getInstance().enterQueue();//new
                        alteastream.QueuePanel.getInstance().manageQueueReadyState(10);
                    }else{
                        console.log("onMouseClick enterQueue");
                        alteastream.QueuePanel.getInstance().enterQueue();//new
                    }
                }
                else{
                    alteastream.QueuePanel.getInstance().exitQueue();
                    alteastream.QueuePanel.getInstance().leaveQueue(
                        function () {alteastream.QueuePanel.getInstance().enterQueue();
                            alteastream.QueuePanel.getInstance().manageQueueReadyState(10);
                        }
                    );
                }
                /*console.log("MachineName "+_this.machineName);
                console.log("machineInfo.machineName "+_this.machineInfo.machineName);
                console.log("IS FREE TO PLAY "+_this.machineInfo.isFreeToPlay);*/
                //console.log("this.machineInfo.isFreeToPlay::::::::::::::: "+_this.machineInfo.isFreeToPlay); //ovde u lokalu nije isti ID
                //alteastream.LocationMachines.getInstance().setComponentWhenMachineisFreeToPlay(freetoplayTest);//QueuePanel.manageQueueReadyState
                //alteastream.QueuePanel.getInstance().enterMachineQueue();

                alteastream.MachinesComponent.getInstance().clearActiveMachines();
            }
        };
    };

    alteastream.Machine = createjs.promote(Machine,"Container");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MachineCoinsAndPrizesInfo = function(){
        this.Container_constructor();
        this.initMachineCoinsAndPrizesInfo();
    };

    var p = createjs.extend(MachineCoinsAndPrizesInfo,createjs.Container);

    var _this;

    p.initMachineCoinsAndPrizesInfo = function () {
        console.log("MachineCoinsAndPrizesInfo initialized:::");
        _this = this;
        this.setLayout();
    };

    MachineCoinsAndPrizesInfo.getInstance = function() {
        return _this;
    };

    // private methods
    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.addChild(textInstance);
        textInstance.mouseEnabled = false;
    };

    // public methods
    p.setLayout = function() {
        var labelsFont = "15px HurmeGeometricSans3";
        var valuesFont = "bold 24px HurmeGeometricSans3";
        var col_1 = "#a39f9e";
        var col_2 = "#ffcc00";
        var col_3 = "#3ee019";
        var labelsY = 889;
        var valuesY = 912;

        var shapeW = 1195;
        var shapeH = 90;
        //hasCoPlay change
        //var controlsBgShapeRight = this.controlsBgShapeRight = new createjs.Shape();
        var footerShape = this.footerShape = new createjs.Shape();
        footerShape.graphics.beginFill("#000000").drawRect(0, 0, shapeW, shapeH);
        footerShape.cache(0, 0, shapeW, shapeH);
        footerShape.size = {width:shapeW, height:shapeH, roundness:5};
        footerShape.x = 263;
        footerShape.y = 855;
        footerShape.mouseEnabled = false;
        this.addChild(footerShape);
        footerShape.alpha = 0.7;

        this._createDynamicText("tokensFiredLabel","COINS FIRED ",labelsFont,col_1,{x:855,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("tokensFiredValue","",valuesFont,col_3,{x:this.tokensFiredLabel.x,y:valuesY,textAlign:"center", textBaseline:"middle"});

        this._createDynamicText("tokensInfoLabel","PRIZES COLLECTED",labelsFont,col_1,{x:1154,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("decorator","--",valuesFont,col_2,{x:1160,y:valuesY,textAlign:"center", textBaseline:"middle"});

        this._createDynamicText("totalWinLabel","TOTAL",labelsFont,col_1,{x:1273,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("totalWinValue","",valuesFont,col_2,{x:this.totalWinLabel.x,y:valuesY,textAlign:"center", textBaseline:"middle"});

        this._createDynamicText("lastWinLabel","LAST",labelsFont,col_1,{x:1339,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("lastWinValue","",valuesFont,col_2,{x:this.lastWinLabel.x,y:valuesY,textAlign:"center", textBaseline:"middle"});

        this._createDynamicText("biggestGameWinLabel","BIGGEST",labelsFont,col_1,{x:1408,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("biggestGameWinValue","",valuesFont,col_2,{x:this.biggestGameWinLabel.x,y:valuesY,textAlign:"center", textBaseline:"middle"});
    };

    p.updateProps = function(machineInfo){
        console.log("updateProps::::::::::::::::::::");
        this.tokensFiredValue.text = machineInfo.tokensFired;
        this.lastWinValue.text = machineInfo.lastWin;
        this.totalWinValue.text = machineInfo.totalWin;
        this.biggestGameWinValue.text = machineInfo.biggestGameWin;
    };

    p.showPrizes = function(show) {
        this.tokensInfoLabel.visible =
        this.decorator.visible =
        this.totalWinLabel.visible =
        this.totalWinValue.visible =
        this.lastWinLabel.visible =
        this.lastWinValue.visible =
        this.biggestGameWinLabel.visible =
        this.biggestGameWinValue.visible = show;
    };

    p.adjustMobile = function() {
        var font = "11px HurmeGeometricSans3"
        var fontBold = "bold 14px HurmeGeometricSans3";

        //hasCoPlay change
        var footerShape = this.footerShape;
        footerShape.uncache();
        footerShape.graphics.clear();
        footerShape.graphics.beginFill("#000000").drawRect(0, 0, 567, 45);
        footerShape.cache(0, 0, 567, 45);
        footerShape.size = {width:567, height:45};
        footerShape.x = 212;
        footerShape.y = 470;
        footerShape.alpha = 0.7;

        this.tokensFiredLabel.font = font;
        this.tokensFiredLabel.x = 470;
        this.tokensFiredLabel.y = 482;

        this.tokensFiredValue.font = fontBold;
        this.tokensFiredValue.x = this.tokensFiredLabel.x;
        this.tokensFiredValue.y = this.tokensFiredLabel.y + 20;

        this.tokensInfoLabel.font = font;
        this.tokensInfoLabel.x = 570;
        this.tokensInfoLabel.y = this.tokensFiredLabel.y;

        this.decorator.font = fontBold;
        this.decorator.x = this.tokensInfoLabel.x;
        this.decorator.y = this.tokensInfoLabel.y + 20;

        this.lastWinLabel.font = font;
        this.lastWinLabel.x = 646;
        this.lastWinLabel.y = this.tokensFiredLabel.y;

        this.lastWinValue.font = fontBold;
        this.lastWinValue.x = this.lastWinLabel.x;
        this.lastWinValue.y = this.lastWinLabel.y + 20;

        this.totalWinLabel.font = font;
        this.totalWinLabel.x = 696;
        this.totalWinLabel.y = this.tokensFiredLabel.y;

        this.totalWinValue.font = fontBold;
        this.totalWinValue.x = this.totalWinLabel.x;
        this.totalWinValue.y = this.totalWinLabel.y + 20;

        this.biggestGameWinLabel.font = font;
        this.biggestGameWinLabel.x = 746;
        this.biggestGameWinLabel.y = this.tokensFiredLabel.y;

        this.biggestGameWinValue.font = fontBold;
        this.biggestGameWinValue.x = this.biggestGameWinLabel.x;
        this.biggestGameWinValue.y = this.biggestGameWinLabel.y + 20;
    };

    alteastream.MachineCoinsAndPrizesInfo = createjs.promote(MachineCoinsAndPrizesInfo,"Container");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MachinesComponent = function(rows, columns){
        this.Container_constructor();
        this.initMachinesContainer(rows, columns);
    };

    var p = createjs.extend(MachinesComponent,createjs.Container);
    var _this;

    p.MAX_NUMBER_OF_MACHINES = 70;
    p.MAX_NUMBER_OF_ACCORDEONS_PER_PAGE = 16;
    p._totalRows = 0;
    p._columnsPerPage = 0;
    p._rowsPerPage = 0;
    p._ySpacing = 0;
    p._xSpacing = 0;
    p._startPositionX = 0;
    p._startPositionY = 0;
    p._numOfActiveMachines = 0;
    p.showOnlyAvailable = false;
    p._startPositionYGrid = 4;
    p._startPositionYList = 3;
    p.acordions = null;
    p.selectedAcordion = null;
    p.newScrollLevels = [];
    p._noAvailableMachinesImg = null;

    p.initMachinesContainer = function (rows, columns) {
        console.log("MachinesComponent initialized:::");
        _this = this;
        this._columnsPerPage = columns;
        this._rowsPerPage = rows;
        this.setMachines();
        var noAvailableMachines =  this._noAvailableMachinesImg = alteastream.Assets.getImage(alteastream.Assets.images.noAvailableMachinesImg);
        noAvailableMachines.regX = noAvailableMachines.image.width + 0.5;
        //noAvailableMachines.regY = noAvailableMachines.image.height + 0.5;
        noAvailableMachines.mouseEnabled = false;
        noAvailableMachines.visible = false;
        this.addChild(noAvailableMachines);
    };

    MachinesComponent.getInstance = function() {
        return _this;
    };

    p.getStartPositionY = function (listView) {
        return listView === true ? this._startPositionYList : this._startPositionYGrid;
    };

    p.setMachines = function(){
        this._totalRows = Math.ceil(this.MAX_NUMBER_OF_MACHINES/this._columnsPerPage);
        this._startPositionX = 23;
        this._startPositionY = 4;
        this._xSpacing = 25;
        this._ySpacing = 25;
        this.addMachines();
    };

    p.setGridView = function (grid) {
        this._columnsPerPage = grid === true ? 4 : 1;
        this._rowsPerPage = grid === true ? 3 : 17;
        this._totalRows = Math.ceil(this.MAX_NUMBER_OF_MACHINES/this._columnsPerPage);
        this._startPositionX = grid === true ? 23 : 21
        this._startPositionY = this.getStartPositionY(!grid);
        this._xSpacing = grid === true ? 25 : 0;
        this._ySpacing = grid === true ? 25 : 7;
        this.rearrangeMachinesAsGrid(grid);
        this.updateMask();
    };

    p.rearrangeMachinesAsGrid = function(setGrid){
        var xCount = 0;
        var yCount = 0;
        for(var i = 0, j = this.MAX_NUMBER_OF_MACHINES; i < j; i++){
            var machine = this.machines[i];
            machine.setGridMachine(setGrid);
            machine.x = this._startPositionX + xCount * (machine.getWidth() + this._xSpacing);
            machine.y = this._startPositionY + yCount * (machine.getHeight() + this._ySpacing);
            xCount++;
            if(xCount === this._columnsPerPage){
                xCount = 0;
                yCount++;
            }
        }
    };

    p.updateMask = function(){
        var maskWidth = this.getWidth();
        var maskHeight = this.getMaskHeight();
        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(this._startPositionX, this._startPositionY, maskWidth, maskHeight);
        this.mask = mask;
        this.mask.setBounds(this._startPositionX, this._startPositionY, maskWidth, maskHeight);
        //this.mask = mask;
    };

    p.animateMask = function (clickedAccordionIndex, expand) {
        var acordionH = this.selectedAcordion.tentImage.image.height;
        var topAccordionsH = expand === true ? acordionH + (acordionH * clickedAccordionIndex) : this.acordions.length * acordionH;
        var maskH = this.mask.getBounds().height;
        var onePercent = maskH/100;

        var downScale = (topAccordionsH/onePercent)/100;
        var scaleToAnimate = expand === true ? 1 : downScale;

        this.mask.scaleY = expand === true ? downScale : 1;
        if(expand === true){
            this.showMachines(true);
        }
        createjs.Tween.get(this.mask).to({scaleY:scaleToAnimate},1000).call(function (){
            if(expand === false){
                _this.showMachines(false);
            }
        });
    };

    p.addMachines = function () {
        this.machines = [];
        var xCount = 0;
        var yCount = 0;
        for(var i = 0, j = this.MAX_NUMBER_OF_MACHINES; i < j; i++){
            var machine = new alteastream.Machine();
            machine.x = this._startPositionX + xCount * (machine.getWidth() + this._xSpacing);
            machine.y = this._startPositionY + yCount * (machine.getHeight() + this._ySpacing);
            this.machines.push(machine);
            this.addChild(machine);
            machine.visible = false;

            xCount++;
            if(xCount === this._columnsPerPage){
                xCount = 0;
                yCount++;
            }
        }
    };

    p.addAcordions = function (acordions) {
        this.acordions = [];
        var startY = 29;
        for(var i = 0; i < acordions.length; i++){
            var acordion = new alteastream.Location(acordions[i]);
            var acordionH = acordion.tentImage.image.height;
            acordion.x = 862;
            acordion.y = i * acordionH + startY + (i * 2);
            acordion.on("click", function (){
                _this.acordionClickHandler(this);
            });
            acordion.on("mouseover", function (){
                this.highlight(true);
            });
            acordion.on("mouseout", function (){
                this.highlight(false);
            });
            this.addChild(acordion);
            this.acordions.push(acordion);
        }

        // todo naci resenje da se scroll aktivira odmah kada se pojave accordioni
        //this.setScrollLevelsForAcordions();
    };

    p.getAcordionByName = function (acordionName) {
        for(var i = 0; i < this.acordions.length; i++){
            if(this.acordions[i].name === acordionName){
                return this.acordions[i];
            }
        }
    };

    p.acordionClickHandler = function (clickedAcordio) {
        if(this.selectedAcordion !== clickedAcordio){
            alteastream.Lobby.getInstance().enterShop(clickedAcordio);
            //this.selectedAcordion = clickedAcordio;
            //this.animateMask(index, true);
            alteastream.LocationMachines.getInstance()._locationLabel.visible = true;
        }else{
            alteastream.LocationMachines.getInstance()._locationLabel.visible = false;
            this._closeAcordion();
        }
    };

    p.setScrollLevelsForAcordions = function () {
        if(this.acordions.length < this.MAX_NUMBER_OF_ACCORDEONS_PER_PAGE){
            alteastream.LocationMachines.getInstance()._scroller.setToFirstPage(false);
            alteastream.LocationMachines.getInstance()._scroller.setScrollLevels([]);
        }else{
            var scrollLevels = [];
            var positionMultiplier = 0;
            for(var i = this.acordions.length - this.MAX_NUMBER_OF_ACCORDEONS_PER_PAGE; i >= 0; i--){
                scrollLevels.push(this.y - (positionMultiplier * this.acordions[i].tentImage.image.height));
                positionMultiplier++;
            }
            alteastream.LocationMachines.getInstance()._scroller.setScrollLevels(scrollLevels);
            alteastream.LocationMachines.getInstance()._scroller.setToFirstPage(false);
        }
    };

    p.showAcordions = function (show) {
        if(this.acordions[0].visible !== show){
            for(var i = 0; i < this.acordions.length; i++){
                this.acordions[i].visible = show;
            }
        }
    };

    p._closeAcordion = function () {
        this._noAvailableMachinesImg.visible = false;
        var acordionH = this.selectedAcordion.tentImage.image.height + 2;
        var counter = 1;
        var index = this.acordions.indexOf(this.selectedAcordion);
        for(var i = index+1; i < this.acordions.length; i++){
            this.acordions[i].y = this.acordions[index].y + (acordionH * counter);
            counter++;
        }
        this.selectedAcordion = null;
        this.showMachines(false);
    };

    p.setAcordionsLayout = function (listView) {
        if(!this.selectedAcordion){return;}
        this._noAvailableMachinesImg.visible = false;

        console.log("SET ACORDIONS");
        if(listView === true){
            this.showAcordions(false);
            return;
        }else {
            this.showAcordions(true);
        }

        var acordionH = this.selectedAcordion.tentImage.image.height;
        var acordionHalfH = acordionH * 0.5;
        var clickedAcIndex = _this.acordions.indexOf(this.selectedAcordion);
        var firstMachine = _this.machines[0];
        var lastMachine = _this.machines[_this.getNumberOfActiveMachines()-1] || _this.machines[0];// || fix for situation when there is no active machines
        var bottomYPos = lastMachine.y + lastMachine.getHeight() + acordionHalfH;
        var scrollLevels = _this.parent._scroller._scrollLevels;

        var hasScrollLevels = scrollLevels.length > 0;

        for(var j = 0; j < _this.newScrollLevels.length; j++){
            var level = _this.newScrollLevels[j];
            if(scrollLevels.indexOf(level) > -1){
                scrollLevels.splice(scrollLevels.indexOf(level), 1);
            }
        }

        scrollLevels[0] = 0;

        _this.newScrollLevels = [];
        var bottomAcordionsPositionCorrector = 2;
        var topAcordionsPositionCorrector = _this.acordions.indexOf(this.selectedAcordion);
        for(var i = 0; i < _this.acordions.length; i++){
            if(i <= clickedAcIndex){
                _this.acordions[i].y = firstMachine.y - 4 - (topAcordionsPositionCorrector * 4) - acordionHalfH - (acordionH * topAcordionsPositionCorrector);
                topAcordionsPositionCorrector--;
                if(hasScrollLevels){
                    var levelToAdd = scrollLevels[0] + (acordionH+4);
                    scrollLevels.unshift(levelToAdd);
                    _this.newScrollLevels.push(levelToAdd);
                }
            }else{
                _this.acordions[i].y = bottomYPos + bottomAcordionsPositionCorrector;
                bottomYPos += acordionH;
                if(hasScrollLevels){
                    levelToAdd = scrollLevels[scrollLevels.length-1] - acordionH - bottomAcordionsPositionCorrector;
                    scrollLevels.push(levelToAdd);
                    _this.newScrollLevels.push(levelToAdd);
                }
                bottomAcordionsPositionCorrector += 2;
            }

            if(i === clickedAcIndex){
                if(this.getNumberOfActiveMachines() === 0){
                    this._noAvailableMachinesImg.x = this.acordions[i].x + this._noAvailableMachinesImg.image.width * 0.5;
                    this._noAvailableMachinesImg.y = this.acordions[i].y + acordionHalfH + 2;
                    this._noAvailableMachinesImg.visible = false;// todo temp je na false, da se ne bi nikad pojavljivala slika, treba da bude na true
                }
            }
        }

/*        if(!hasScrollLevels){
            this.y = (this.getStartPositionY() + acordionH) * (clickedAcIndex + 1);
        }*/

        if(!hasScrollLevels){
            if(window.localStorage.getItem('isMobile') === "yes"){
                var accScrollLevels = [(acordionH+4)*(1 + clickedAcIndex)];
                this.newScrollLevels = [(acordionH+4)*(1 + clickedAcIndex)];
                for(var n = 3; n < this.acordions.length; n++){
                    var slevel = accScrollLevels[accScrollLevels.length-1];
                    accScrollLevels.push(slevel - (acordionH+4));
                    this.newScrollLevels.push(slevel - (acordionH+4));
                }
                this.parent._scroller._scrollLevels = accScrollLevels;
                this.parent._scroller.setToFirstPage(true, 200);
            }else{
                this.y = (this.getStartPositionY() + acordionH) * (clickedAcIndex + 1);
            }
        }

        console.log(scrollLevels);
    };

    p.showMachines = function (show) {
        for(var i = 0; i < this.getNumberOfActiveMachines(); i++){
            this.machines[i].showMachine(show);
        }
    };

    p.populateAllMachines = function(machines){
        this.machinesFromServer = machines;
        this.resetComponent(machines);
    };

    p.getMachines = function(){
        return this.machines;
    };

    p.getHeight = function () {
        var machineHeight = this.machines[0].getHeight();
        return this._rowsPerPage * (machineHeight + this._ySpacing);
    };

    p.getWidth = function () {
        var machineWidth = this.machines[0].getWidth();
        return this._columnsPerPage * (machineWidth + this._xSpacing);
    };

    p.getMaskHeight = function() {
        var containerHeight = this.getHeight();
        return containerHeight - this._ySpacing;
    };

    p.getNumberOfPages = function () {
        return Math.ceil(this._totalRows/this._rowsPerPage);
    };

    p.getMaxNumberOfPages = function () {
        var machinesPerPage = this._rowsPerPage*this._columnsPerPage
        return Math.ceil(this.MAX_NUMBER_OF_MACHINES/machinesPerPage);
    };

    p.getMachineHeight = function() {
        var machineHeight = this.machines[0].getHeight();
        return (machineHeight + this._ySpacing);
    };

    p.disableActiveMachines = function(bool){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            this.machines[i].disableMouseListeners(bool);
        }
    };

    p.clearActiveMachines = function(){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            this.machines[i].resetDisplay();
        }
    };

    p.updateThumbnails = function(){
        var areaFrom = this.mask.getBounds().y;
        var areaTo = this.mask.getBounds().height;
        var parentX = this.parent.x;
        var parentY = this.parent.y;
        var numOfMachines = this.getNumberOfActiveMachines();
        for (var i = 0; i < numOfMachines; i++){
            var topY = this.machines[i].localToGlobal(parentX, parentY).y;
            var bottomY = topY + this.machines[i].getHeight();
            if(this.machines[i].visible === true){
                if(topY > areaFrom && topY < areaTo){
                    this.machines[i].updateThumbnail();
                }else if(bottomY.y > areaFrom && bottomY.y < areaTo){
                    this.machines[i].updateThumbnail();
                }
            }
        }
    };

    p.updateFromCurrentStream = function(response,currentStream){
        var machine = this.getMachineById(currentStream);
        if(machine) // nema ni jedna slobodna masina?
            if(machine.getState() === machine.STATE_NORMAL){ //symbols
                if(machine.isFreeToPlay !== response.isFreeToPlay){
                    machine.isFreeToPlay = response.isFreeToPlay;// temp patch to prevent double calls
                    //alteastream.Requester.getInstance().getMachines(alteastream.AbstractScene.SHOP_MACHINE,function(response){
                    alteastream.Requester.getInstance().getMachinesGt(alteastream.AbstractScene.SHOP_MACHINE,function(response){
                        _this.setMachinesStatus(response);
                    });
                }else{
                    machine.setStatus(response);
                }
            }
    };

    p.cacheMachines = function(bool){
        for(var i = 0,j=this._numOfActiveMachines;i<j;i++){
            this.machines[i].doCache(bool);
        }
    };

    //new socket on notify
    p.setMachinesStatus = function(machine){
        var machineToUpdate = this.getMachineById(machine.machineIndex);
        if(machineToUpdate){
            var newMachine = {};

            newMachine.isOnline = machine.hasOwnProperty("isOnline") ? machine.isOnline : machineToUpdate.isOnline;
            newMachine.queueSize = machine.hasOwnProperty("size") ? machine.size : machineToUpdate.queueNumber.text;
            newMachine.statusCode = machine.hasOwnProperty("statusCode") ? machine.statusCode : machineToUpdate.statusCode;
            newMachine.isInAutoplay = machine.hasOwnProperty("isInAutoplay") ? machine.isInAutoplay : machineToUpdate.isInAutoplay;
            newMachine.isFreeToPlay = machine.hasOwnProperty("isFreeToPlay") ? machine.isFreeToPlay : machineToUpdate.isFreeToPlay;
            newMachine.stakename = machine.hasOwnProperty("stakename") ? machine.stakename : machineToUpdate.stakePolicy.text;
            newMachine.payoutname = machine.hasOwnProperty("payoutname") ? machine.payoutname : machineToUpdate.payoutPolicy.text;

            machineToUpdate.setStatus(newMachine);
        }
        for(var p in this.machinesFromServer){
            var machineFromPreviousResponse = this.machinesFromServer[p];
            if(machineFromPreviousResponse.machineIndex === machine.machineIndex){
                machineFromPreviousResponse.isFreeToPlay = machine.hasOwnProperty("isFreeToPlay") ? machine.isFreeToPlay : machineFromPreviousResponse.isFreeToPlay;
                machineFromPreviousResponse.isInAutoplay = machine.hasOwnProperty("isInAutoplay") ? machine.isInAutoplay : machineFromPreviousResponse.isInAutoplay;
                machineFromPreviousResponse.isOnline = machine.hasOwnProperty("isOnline") ? machine.isOnline : machineFromPreviousResponse.isOnline;
                machineFromPreviousResponse.queueSize = machine.hasOwnProperty("size") ? machine.size : machineFromPreviousResponse.queueSize;
                machineFromPreviousResponse.statusCode = machine.hasOwnProperty("statusCode") ? machine.statusCode : machineFromPreviousResponse.statusCode;
                machineFromPreviousResponse.stakename = machine.hasOwnProperty("stakename") ? machine.stakename : machineFromPreviousResponse.stakename;
                machineFromPreviousResponse.payoutname = machine.hasOwnProperty("payoutname") ? machine.payoutname : machineFromPreviousResponse.payoutname;
            }
        }
        if(this.showOnlyAvailable === true){
            this.resetComponent(this.machinesFromServer);
        }
    };

    p.getActiveMachine = function(){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            if(this.machines[i].selected === true)
                return this.machines[i];
        }
    };

    p.getMachineById = function(id){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            if(this.machines[i].id === id){
                return this.machines[i];
            }
        }
    };

    // filtering
    p.resetComponent = function(machines) {
        //console.log("RESET COMPONENT");
        this._numOfActiveMachines = Object.keys(machines).length;
        this.numOfAvailableMachines = 0;
        var machinesToShow = [];
        var key;
        var machine;
        var activeMachine = this.getActiveMachine();
        if(this.showOnlyAvailable === true){
            for(var s = 0, t = this._numOfActiveMachines; s < t; s++){
                key = Object.keys(machines)[s];
                machine = machines[key];
                if(machine.isFreeToPlay === true && machine.isOnline === true){
                    if(!((parseInt("00001000",2) & machine.statusCode) > 0)){
                        if(!((parseInt("00000010",2) & machine.statusCode) > 0)){
                            if(machine.isInAutoplay === false){
                                machinesToShow.push(machine);
                                this.numOfAvailableMachines++;
                            }
                        }
                    }
                }
            }
        }else{
            for(var u = 0, x = this._numOfActiveMachines; u < x; u++){
                key = Object.keys(machines)[u];
                machine = machines[key];
                machinesToShow.push(machine);
                this.numOfAvailableMachines++;
            }
        }
        var numberOfMachinesToShow = machinesToShow.length;
        for(var i = 0, j = this.MAX_NUMBER_OF_MACHINES; i < j; i++){
            if(i < numberOfMachinesToShow){
                this.machines[i].setMachineInfo(machinesToShow[i]);
            }else{
                this.machines[i].showMachine(false);
                this.machines[i].resetMachineProperties();
            }
        }
        if(activeMachine){activeMachine.selectMachine(true);}
        var numberOfMachinesToDivide = this.showOnlyAvailable === true ? this.numOfAvailableMachines : this._numOfActiveMachines;
        this._totalRows = Math.ceil(numberOfMachinesToDivide/this._columnsPerPage);
        this.parent._updateMachinesLabel();

        if(this.selectedAcordion){
            this.showMachines(true)
        }
    };

    // filtering
    p.setShowOnlyAvailable = function(onlyAvailable){
        this.showOnlyAvailableOrAllMachines(onlyAvailable);
        var streamPreview = alteastream.StreamPreview.getInstance();
        for(var i = 0; i < this._numOfActiveMachines; i++){
            if(this.machines[i].id === streamPreview.currentStream){
                this.machines[i].selectMachine(true);
            }
        }
    };

    p.showOnlyAvailableOrAllMachines = function(onlyAvailable) {
        this.showOnlyAvailable = onlyAvailable;
        if(this.machinesFromServer){
            this.resetComponent(this.machinesFromServer);
        }
    };

    p.getColumnsPerPage = function() {
        return this._columnsPerPage;
    };

    p.getRowsPerPage = function() {
        return this._rowsPerPage;
    };

    // filtering
    p.getNumberOfActiveMachines = function() {
        return this.showOnlyAvailable === true ?  this.numOfAvailableMachines : this._numOfActiveMachines;
    };

    p.setCurrency = function (currency) {
        for(var i = 0; i < this.machines.length; i++){
            this.machines[i].setCurrency(currency);
        }
    };

    p.adjustMobile = function(){
        var numOfMachines = this.MAX_NUMBER_OF_MACHINES;
        var font = "13px HurmeGeometricSans3";
        var upperTxtYPos = 10;
        var yCount = 0;
        this._startPositionX = 6;
        this._startPositionY = 80;
        this._startPositionYList = 137;
        this._startPositionYGrid = 80;
        for(var i = 0; i < numOfMachines; i++) {
            var machine = this.getChildAt(i);

            machine.x = this._startPositionX;
            machine.y = this._startPositionY + yCount * (machine.getHeight() + this._ySpacing);
            yCount++;

            machine.machineNameLabel.font = font;
            machine.machineNameLabel.x = 30;
            machine.machineNameLabel.y = 174;

            machine._nameText.font = font;
            machine._nameText.x = machine.machineNameLabel.x + machine.machineNameLabel.getBounds().width + 5;
            machine._nameText.y = 174;

            machine.availabilityLabel.font = font;
            machine.availabilityLabel.x = 187;
            machine.availabilityLabel.y = 174;
            machine.availabilityLabel.originalXPosition = machine.availabilityLabel.x;

            machine.queueNumber.font = font;
            machine.queueNumber.x = machine.availabilityLabel.x;
            machine.queueNumber.y = machine.availabilityLabel.y;

            machine._availabilityImage.scale = 0.8;
            machine._availabilityImage.x = 277;
            machine._availabilityImage.y = 162;

            machine.spinner.x = 155;
            machine.spinner.y = 15;

            machine.frame.y = 0;

            machine.thumbScaleX = 0.633;
            machine.thumbScaleY = 0.58;
            //machine.thumb.y = 13;//7

            this._startPositionYList = 0;
            this._startPositionYGrid = 0;
        }

        this.setGridView = function (grid) {
            this._columnsPerPage = grid === true ? 3 : 1;
            this._rowsPerPage = grid === true ? 2 : 7;
            this._totalRows = Math.ceil(this.MAX_NUMBER_OF_MACHINES/this._columnsPerPage);
            this._xSpacing = grid === true ? 10 : 0;
            this._ySpacing = grid === true ? 10 : 5;
            this._startPositionY = grid === true ? 80 : 137;
            this.rearrangeMachinesAsGrid(grid);
            this.updateMask();
        };

        this.addAcordions = function (acordions) {
            console.log("ADD ACCORDIONS");
            this.acordions = [];
            var startY = 110;
            for(var i = 0; i < acordions.length; i++){
                var acordion = new alteastream.Location(acordions[i]);
                var acordionH = acordion.tentImage.image.height;
                acordion.x = 472;
                acordion.y = i * acordionH + startY + (i * 2);
                acordion.on("click", function (){
                    _this.acordionClickHandler(this);
                });
                this.addChild(acordion);
                this.acordions.push(acordion);
            }

            // todo naci resenje da se scroll aktivira odmah kada se pojave accordioni
            //this.setScrollLevelsForAcordions();
        };

        this.setGridView(true);
    }

    alteastream.MachinesComponent = createjs.promote(MachinesComponent,"Container");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MultiplayerControls = function(){
        this.Container_constructor();
        this.initMultiplayerControls();
    };

    var p = createjs.extend(MultiplayerControls,createjs.Container);
    var _this;

    // private properties
    //p._stakes = [];//if stakes array
    p._selectedStake = 0;
    p._betCounter = null;
    p._winCountTimer = null;
    p._countdownTime = 0;
    p._prizeWin = 0;
    p._prizeWinAmount = null;
    p._coinsWin = 0;
    p._coinsWinAmount = null;
    p._balance = 0;
    p._refundCoinsCounter = 0;
    p._waitingInterval = null;
    p._waitingSpinner = null;
    p._refundAnimationInterval = null;

    // co-play prize win animation
    p.prizeQueue = [];
    p.queueCount = 0;
    p._prizeQueuePlaying = false;

    p.initMultiplayerControls = function () {
        console.log("MultiplayerControls initialized:::");
        _this = this;
        this.setLayout();
    };

    MultiplayerControls.getInstance = function() {
        return _this;
    };

    // private methods
    p._addButton = function(assetName,x,y,text,states){
        var textLabel = text||null;
        states = states || 3;
        var btn = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images[assetName]),states,textLabel);
        btn.x = x;
        btn.y = y;
        return btn;
    };

    p._reset = function(){
        this.setBetToStart(true);
        this._showUI(false);
        this.showTextFields(false);
        this._betCounter.quitCounter();
    };

    p._showUI = function(bool){
        this.btnParticipate.visible = bool;
    };

    p._resetPrizeWinCountTimeout = function(msg) {
        clearTimeout(this._winCountTimer);
        this._winCountTimer = setTimeout(function(){
            _this._prizeWin = 0;
            _this._prizeWinAmount.text = "0.00";
            _this._highlightWin("prize", false);

            // co-play prize win animation
            alteastream.Lobby.getInstance().updateLobbyBalance(_this._balance);

        },2000);//adjustable
    };

    p._resetCoinsWinCountTimeout = function(msg) {
        clearTimeout(this._coinsWinCountTimer);
        this._coinsWinCountTimer = setTimeout(function(){
            _this._coinsWin = 0;
            _this._coinsWinAmount.text = "0.00";
            _this._highlightWin("coins", false);

            // co-play prize win animation
            alteastream.Lobby.getInstance().updateLobbyBalance(_this._balance);

        },2000);//adjustable
    };

/*    p._highlightWin = function(bool){
        this._prizeWinAmount.color = bool === true?"#00ff4d":"#ffcc00";
        if(this._coinsWinAmount.visible === true){
            this._coinsWinAmount.color = bool === true?"#00ff4d":"#ffcc00";
        }
        //some animations
    };*/

    p._highlightWin = function(winType, bool){
        var winField = "_" + winType + "WinAmount";
        this[winField].color = bool === true?"#00ff4d":"#ffcc00";
    };

    // public methods
    p.setLayout = function(){
        /*var shapeW = 1195;
        var shapeH = 90;

        var controlsBgShapeRight = this.controlsBgShapeRight = new createjs.Shape();//moved to MachineCoinsAndPrizesInfo
        controlsBgShapeRight.graphics.beginFill("#000000").drawRect(0, 0, shapeW, shapeH);
        controlsBgShapeRight.cache(0, 0, shapeW, shapeH);
        controlsBgShapeRight.size = {width:shapeW, height:shapeH, roundness:5};
        controlsBgShapeRight.x = 263;
        controlsBgShapeRight.y = 855;
        controlsBgShapeRight.mouseEnabled = false;
        this.addChild(controlsBgShapeRight);
        controlsBgShapeRight.alpha = 0.7;*/

        var betCounter = this._betCounter = new alteastream.BetEntranceCounter();
        betCounter.x = 1447;
        betCounter.y = 0;
        this.addChild(betCounter);
        betCounter.mouseEnabled = false;

        this._textColorDisabled = "#383838";
        this._textColorEnabled = "#ffffff";

        var currency = "EUR";//alteastream.Lobby.getInstance().currency;
        var labelsFont = "14px HurmeGeometricSans3";
        var valuesFont = "bold 24px HurmeGeometricSans3";
        var labelsY = 889;
        var valuesY = 911;

        var stakeLabel = this.stakeLabel = new createjs.Text("STAKE ( "+currency+" ) ",labelsFont,"#a39f9e");
        stakeLabel.textAlign = "center";
        stakeLabel.textBaseline = "middle";
        stakeLabel.x = 331;
        stakeLabel.y = labelsY;
        stakeLabel.mouseEnabled = false;
        this.addChild(stakeLabel);

        var stakeAmount = this.stakeAmount = new createjs.Text("0.00",valuesFont,"#ffcc00");
        stakeAmount.textAlign = "center";
        stakeAmount.textBaseline = "middle";
        stakeAmount.x = stakeLabel.x;
        stakeAmount.y = valuesY;
        stakeAmount.mouseEnabled = false;
        this.addChild(stakeAmount);

        var stakeAmountMock = this.stakeAmountMock = new createjs.Text("0.00",valuesFont,"#ffcc00");
        stakeAmountMock.textAlign = "center";
        stakeAmountMock.textBaseline = "middle";
        stakeAmountMock.x = 1584;
        stakeAmountMock.y = 540;
        stakeAmountMock.mouseEnabled = false;
        this.addChild(stakeAmountMock);

        var prizeWinLabel = this.prizeWinLabel = new createjs.Text("PRIZE ( "+currency+" ) ",labelsFont,"#a39f9e");
        prizeWinLabel.textAlign = "center";
        prizeWinLabel.textBaseline = "middle";
        prizeWinLabel.x = 437;
        prizeWinLabel.y = labelsY;
        prizeWinLabel.mouseEnabled = false;
        this.addChild(prizeWinLabel);

        var _prizeWinAmount = this._prizeWinAmount = new createjs.Text("0.00",valuesFont,"#ffcc00");
        _prizeWinAmount.textAlign = "center";
        _prizeWinAmount.textBaseline = "middle";
        _prizeWinAmount.x = prizeWinLabel.x;
        _prizeWinAmount.y = valuesY;
        _prizeWinAmount.mouseEnabled = false;
        this.addChild(_prizeWinAmount);

        var txtStartBet = new createjs.Text("START CO-PLAY","bold 24px HurmeGeometricSans3","#000000");
        var txtEndBet = new createjs.Text("STOP CO-PLAY","bold 24px HurmeGeometricSans3","#ffffff");
        var btnParticipate = this.btnParticipate = this._addButton("btnParticipate",1585,900,txtStartBet,2);

        btnParticipate.addTextPreset("start", txtStartBet);
        btnParticipate.addTextPreset("stop", txtEndBet);
        btnParticipate.centerText(0,2);
        this.addChild(btnParticipate);

        var spinner = this._waitingSpinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = btnParticipate.x;
        spinner.y = btnParticipate.y + 30;
        spinner.customScaleGfx(0.5);
        spinner.customGfxPosition(0, -45);
        spinner.mouseEnabled = false;
        spinner.setLabel("Starting CO-PLAY...");
        spinner.runPreload(false);

        //////////////////////////// ADDING STAKE BUTTONS ////////////////////////////
/*        var btnStakeDec = this.btnStakeDec = this._addButton("btnStakeDec",820,830);
        btnStakeDec.action = -1;
        this.addChild(btnStakeDec);
        btnStakeDec.setClickHandler(function(e){
            _this._onStakeChange(e);
        });

        var btnStakeInc = this.btnStakeInc = this._addButton("btnStakeInc",1054,830);
        btnStakeInc.action = 1;
        this.addChild(btnStakeInc);
        btnStakeInc.setClickHandler(function(e){
            _this._onStakeChange(e);
        });

        p._onStakeChange = function(e){
            this.setStake(e.currentTarget.action);
            console.log("comm send:: stake: "+this._stakes[this._selectedStake]);//Date.now()
            alteastream.Assets.playSound("confirm",0.3);// todo promeniti sound
        };

        p.setInitialStakes = function(stakes){
            this._stakes = stakes;
            this.setStake(0); //if stakes array
        };

        p._setConfirmedStake = function(stake){
            stake = stake/100;
            this.setStake(this._stakes.indexOf(stake)); //if stakes array
            //stake = stake/1000;// novo s vladom
            //this.setStake(stake);// novo s vladom
        };

        //if stakes array
        p.setStake = function(position){
            this._selectedStake += position;
            var stake = this._stakes[this._selectedStake].toFixed(2);
            this.stakeAmount.text = stake;
            this.btnStakeInc.setDisabled(this._selectedStake === this._stakes.length-1);
            this.btnStakeDec.setDisabled(this._selectedStake === 0);
            alteastream.SocketCommunicatorLobby.getInstance().setBetStake(stake);
            //this.setBetLabelToMax(this._selectedStake === (this._stakes.length - 1));
        };*/
        //////////////////////////// ADDING STAKE BUTTONS ////////////////////////////

        //if(alteastream.AbstractScene.GAME_CODE !== "pearlpusher"){
            var coinsLabel = this.coinsLabel = new createjs.Text("COINS ( "+currency+" ) ",labelsFont,"#a39f9e");
            coinsLabel.textAlign = "center";
            coinsLabel.textBaseline = "middle";
            coinsLabel.x = 437 + 106;
            coinsLabel.y = labelsY;
            coinsLabel.mouseEnabled = false;
            this.addChild(coinsLabel);

            var coinsWinAmount = this._coinsWinAmount = new createjs.Text("0.00",valuesFont,"#ffcc00");
            coinsWinAmount.textAlign = "center";
            coinsWinAmount.textBaseline = "middle";
            coinsWinAmount.x = coinsLabel.x;
            coinsWinAmount.y = valuesY;
            coinsWinAmount.mouseEnabled = false;
            this.addChild(coinsWinAmount);
        //}

        this._reset();
    };

    p.showCoinsWinComponent = function(show) {
        this.coinsLabel.visible = this._coinsWinAmount.visible = show;
    };

    p.showTextFields = function(show) {
        this.stakeLabel.color = this.prizeWinLabel.color = show === true ? "#a39f9e" : "#3e3d3d";
        this.stakeAmount.color = this._prizeWinAmount.color = show === true ? "#ffcc00" : "#3e3d3d";
        if(this._coinsWinAmount.visible === true){
            this.coinsLabel.color = show === true ? "#a39f9e" : "#3e3d3d";
            this._coinsWinAmount.color = show === true ? "#ffcc00" : "#3e3d3d";
        }
        this.stakeAmountMock.visible = !show;
    };

    p.setBettingCounterTime = function(time){
        time = time/1000;
        this._countdownTime = time || 60;
    };

    p.startWaitingInterval = function (start){
        this._waitingSpinner.runPreload(start);
        this.btnParticipate.visible = !start;
        if(start === true){
            alteastream.SocketCommunicatorLobby.getInstance().doBetting(true);
            this._waitingInterval = setInterval(function (){
                alteastream.SocketCommunicatorLobby.getInstance().doBetting(true);
            },1000);
        }else{
            if(this._waitingInterval){
                clearInterval(this._waitingInterval);
            }
        }
    };

    p.setBetToStart = function(bool) {//mp
        if(bool) {
            this.btnParticipate.selectTextPreset("start");
            this.btnParticipate.setClickHandler(function(){
                alteastream.Assets.playSound("confirm",0.3);
                console.log("comm send:: doBetting ");
                //alteastream.SocketCommunicatorLobby.getInstance().doBetting(true);//live
                _this.startWaitingInterval(true);//live
            })
        } else {
            this.btnParticipate.selectTextPreset("stop");
            this.btnParticipate.setClickHandler(function(){
                console.log("comm send:: stopBetting ");
                alteastream.SocketCommunicatorLobby.getInstance().doBetting(false);//live
            })
        }
        var btnImage = bool === true?"btnParticipate":"btnStopParticipate";
        this.btnParticipate.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));
        this.btnParticipate.centerText(0,2);

        alteastream.QueuePanel.getInstance().setTextsPositionForMultiplayer(!bool);
    };

    p.setBettingUI = function(bool){
        this.setBetToStart(!bool);
        this._betCounter.quitCounter();
        this.showTextFields(bool);
    };

    p.showMultiPlayerControls = function(bool){//entire component hide/show
        this.visible = bool;
    };

    p.enable = function(bool){
        if(bool===false){
            this._reset();
        }else{
            this._showUI(true);
            this.showCounter(this._countdownTime);
        }
    };

    p.showCounter = function(time){
        //from socket
        console.log("SHOW COUNTER " +time);
        this._betCounter.show(true);
        this._betCounter.update(time);

        //this.setInitialStakes([0.15,0.25,1.5,1.8,2.0]);// temp, from response
    };

    p.setConfirmedStake = function(stake){
        this._spectatorStake = stake/1000;
        //stake = stake/100;
        //this.setStake(this._stakes.indexOf(stake)); //if stakes array
        //stake = stake/1000;// novo s vladom
        this.setStake(1);// novo s vladom
    };

    p.startBetting = function(){
        //on response {currentBet:0.2} npr
        this.setBettingUI(true);
        this.startWaitingInterval(false);
        //this.setConfirmedStake(msg.vl);
        alteastream.LocationMachines.getInstance().machineCoinsAndPrizesInfo.showPrizes(false);
        alteastream.LocationMachines.getInstance().showStreamOverlay(false);
        //alteastream.Assets.playSound("confirm",0.3);

        // co-play prize win animation
        //this.testPrizeWinAnimation();
    };

    p.stopBetting = function(){// on button & expired counter
        //this.quitBetting();
        var locationMachines = alteastream.LocationMachines.getInstance();
        locationMachines.machineCoinsAndPrizesInfo.showPrizes(true);
        locationMachines.backToLobbyButtonHandler();
        alteastream.QueuePanel.getInstance().leaveQueue(function () {
            console.log("no socket? getMachines here ?? ");
            //that.intervalUpdateLobby(true);// if back from entranceCounter?
        });
    };

/*    p.betEvent = function(msg){
        //vl:{"am":12345,"bal":12345,"iswin":false}
        //this._prizeWin = msg.vl/100;
        //this._prizeWin += msg.vl/100;
        //msg = JSON.parse(msg);

        var values = JSON.parse(msg.vl);

        this._balance = values.bal;
        alteastream.Lobby.getInstance().updateLobbyBalance(this._balance);

        if(values.iswin === true){
            if(values.isrefund === true){
                this._coinsWin += values.am/1000;
                
                var coinsWinText = gamecore.Utils.NUMBER.roundDecimal(this._coinsWin).toFixed(2);
                coinsWinText += gamecore.Utils.NUMBER.addZeros(coinsWinText, 1);
                
                this._coinsWinAmount.text = coinsWinText;
                this._highlightWin("coins", true);
                this._resetCoinsWinCountTimeout(msg);
            }else{
                this._prizeWin += values.am/1000;
                //var winText = (Math.round(this._prizeWin * 1e12) / 1e12).toFixed(2);
                //winText += alteastream.Lobby.getInstance()._addZeros(winText,1);

                var winText = gamecore.Utils.NUMBER.roundDecimal(this._prizeWin).toFixed(2);
                winText += gamecore.Utils.NUMBER.addZeros(winText, 1);

                this._prizeWinAmount.text = winText;
                this._highlightWin("prize", true);
                this._resetPrizeWinCountTimeout(msg);
            }
        }else{
            this.setStake(values.am);
        }
        alteastream.Assets.playSound("confirm",0.3);// change sound
    };*/

    // co-play prize win animation START
    p.betEvent = function(msg){
        var values = JSON.parse(msg.vl);//live
        //var values = msg.vl; //test
        this._balance = values.bal;

        if(values.iswin === true){
            var win = values.am/1000;
            if(values.isrefund === true){
                //this.showWin(win, null, "coins");
                this._showCoinsWin(win);
            }else{
                var tag = Number(values.tagid);
                //this.showWin(win, tag, "prize");
                this._showPrizeWin(win, tag);
            }
        }else{
            this.setStake(values.am);
            if(!this._prizeQueuePlaying && this._refundAnimationInterval === null){
                alteastream.Lobby.getInstance().updateLobbyBalance(this._balance);
            }
            //alteastream.Assets.playSound("confirm",0.3);// change sound
        }
    };

/*    p.testPrizeWinAnimation = function () {
        setTimeout(function (){
            var res = {};
            res.vl = {iswin:true, isrefund:false, am:1000, bal:5000, tagid:1};
            _this.betEvent(res);
        },1000);

        setTimeout(function (){
            var res = {};
            res.vl = {iswin:true,isrefund:false, am:2000, bal:7000, tagid:2};
            _this.betEvent(res);
        },1300);

        setTimeout(function (){
            var res = {};
            res.vl = {iswin:true,isrefund:false, am:3000, bal:10000, tagid:3};
            _this.betEvent(res);
        },1800);
        var res = {};
        res.vl = {iswin:true,isrefund:true, am:3000, bal:10000, tagid:1};
        _this.betEvent(res);
        _this.betEvent(res);
        _this.betEvent(res);
        _this.betEvent(res);
        _this.betEvent(res);
    };*/

/*    p.showWin = function(win, tag, winType){
        if(winType === "prize"){
            this.prizeQueue.unshift([win,tag,winType]);
            if(!this._prizeQueuePlaying){
                this._playPrizeQueue();
            }
        }else{
            this.refundCoinSpawn(win);
        }
    };*/

    p._showPrizeWin = function (win, tag) {
        this.prizeQueue.unshift([win,tag]);
        if(!this._prizeQueuePlaying){
            this._playPrizeQueue();
        }
    };

    p._showCoinsWin = function (win) {
        this.refundCoinSpawn(win);
    };

    p.refundCoinSpawn = function (win) {
        this._refundCoinsCounter++;
        if(this._refundAnimationInterval === null) {
            this._refundAnimationInterval = setInterval(function () {
                console.log("cs");
                _this._refundCoinsCounter--;
                _this._coinSpawnAnimation();
                //_this.playSound("refund", 0.8);

                _this._showWin(win, "coins");

                if (_this._refundCoinsCounter === 0) {
                    clearInterval(_this._refundAnimationInterval);
                    _this._refundAnimationInterval = null;
                }
            }, 100);
        }
    };

    p._coinSpawnAnimation = function(){
        var coin = new alteastream.SpringAnim('coinWin');
        coin.x = this._coinsWinAmount.x;
        coin.y = alteastream.AbstractScene.GAME_HEIGHT+coin.pivot;
        this.addChild(coin);
        this._coinSpawnAnimationParams(coin);
        alteastream.Assets.playSound("refund",0.8);// change sound
    };

    p._coinSpawnAnimationParams = function(coin){
        coin.setGravity(1.5);
        coin.setVR(-10,10);
        coin.setVX(-1.5,1.5);
        coin.setVY(-28,-32);
        coin.animate();
    };

    p._playPrizeQueue = function(){
        this._prizeQueuePlaying = true;
        //var lobby = alteastream.Lobby.getInstance();// ovo treba zbog play sound
        var iterateQueue = function(){
            if(_this.prizeQueue.length>0){
                var lastElement = _this.prizeQueue.pop();
                _this._prizeQueuePlaying = true;
                var winForChip = lastElement[0] + gamecore.Utils.NUMBER.addZeros(lastElement[0], 1);
                var chip = new alteastream.Chip(winForChip, lastElement[1], 1);
                var _height = alteastream.AbstractScene.GAME_HEIGHT || document.getElementById("screen").height;
                chip.x = _this.prizeWinLabel.x;
                chip.y = _height+chip.getSize();
                _this.addChild(chip);
                var yAnimation = (_this.prizeWinLabel.y-chip.getSize()*0.5)-10;
                alteastream.Assets.playSound("refund",0.8);// change sound
                TweenLite.to(chip,2,{y:yAnimation,onComplete:function(){
                        _this.removeChild(chip);
                        chip = null;
                    },ease: Elastic.easeOut.config(0.5, 0.1)});
                _this._showWin(lastElement[0], "prize");
                // nemamo sound fajlove za sad pa zato stoji timeout
/*                var prizeSpoken = "prize_"+lastElement[0];
                lobby.playSoundChain(prizeSpoken,0.3,function(){// append bonus on next soundChain?
                    _this.queueCount++;
                    iterateQueue();
                },_this);*/

                setTimeout(function (){
                    _this.queueCount++;
                    iterateQueue();
                },1000);
            }else{
                if(_this.queueCount > 2){
                    var phrase = "";
                    if(_this.queueCount < 6 ) { phrase = "reallyGoodSpoken";}
                    if(_this.queueCount > 5 && _this.queueCount < 9 ) { phrase = "feelDizzySpoken";}
                    if(_this.queueCount > 8 ) { phrase = "keepGoingSpoken";}

                    // nemamo sound fajlove za sad pa zato stoji timeout
                    //lobby.playSoundChain(phrase,0.3,function(){ _this._prizeQueuePlaying = false; iterateQueue();});

                    setTimeout(function (){
                        _this._prizeQueuePlaying = false;
                        iterateQueue();
                    },1000);
                }else{
                    _this._prizeQueuePlaying = false;
                }
                _this.queueCount = 0;
            }
        };
        // nemamo sound fajlove za sad pa zato stoji timeout
        //lobby.playSound("win1",0.3);
        //lobby.playSoundChain("youHaveWonSpoken",0.3,iterateQueue);

        setTimeout(function (){
            iterateQueue();
        },1000);
    };

    p._showWin = function(winValue, winType) {
        this._highlightWin(winType, true);

        //this._prizeWin += win;
        var win = "_" + winType + "Win";
        var winAmount = win + "Amount";
        var resetMethod = "_reset" + winType.charAt(0).toUpperCase() + winType.slice(1) + "WinCountTimeout";

        this[win] += winValue;
        //var winText = gamecore.Utils.NUMBER.roundDecimal(this._prizeWin).toFixed(2);
        var winText = gamecore.Utils.NUMBER.roundDecimal(this[win]).toFixed(2);
        winText += gamecore.Utils.NUMBER.addZeros(winText, 1);
        //this._prizeWinAmount.text = winText;
        this[winAmount].text = winText;


        //this._resetPrizeWinCountTimeout();
        this[resetMethod]();
    };
    // co-play prize win animation END

    p.quitBetting = function(){// MACHINE_READY_FOR_PLAYER
        this._selectedStake = 0;
        this.setBettingUI(false);
        this.startWaitingInterval(false);
    };

    p.setStake = function(stake){
        stake *= this._spectatorStake;
        this._selectedStake = stake;
        //var zeros = alteastream.Lobby.getInstance()._addZeros(String(stake),1);
        var stakeTxt = gamecore.Utils.NUMBER.roundDecimal(stake);
        stakeTxt += gamecore.Utils.NUMBER.addZeros(stakeTxt, 1);
/*        this.stakeAmount.text = stake + zeros;// novo s vladom
        this.stakeAmountMock.text = stake + zeros; */
        this.stakeAmount.text = stakeTxt;// novo s vladom
        this.stakeAmountMock.text = stakeTxt;
    };

    p.adjustMobile = function(){
        //hasCoPlay change
        /*var controlsBgShapeRight = this.controlsBgShapeRight;//moved to MachineCoinsAndPrizesInfo
        controlsBgShapeRight.uncache();
        controlsBgShapeRight.graphics.clear();
        controlsBgShapeRight.graphics.beginFill("#000000").drawRect(0, 0, 567, 45);
        controlsBgShapeRight.cache(0, 0, 567, 45);
        controlsBgShapeRight.size = {width:567, height:45};
        controlsBgShapeRight.x = 212;
        controlsBgShapeRight.y = 470;
        controlsBgShapeRight.alpha = 0.7;*/

        var font = "11px HurmeGeometricSans3";
        var fontBold = "bold 14px HurmeGeometricSans3";

        var prizeWinLabel = this.prizeWinLabel;
        prizeWinLabel.text = "PRIZE";
        prizeWinLabel.font = font;
        prizeWinLabel.x = 290;
        prizeWinLabel.y = 482;

        var _prizeWinAmount = this._prizeWinAmount;
        _prizeWinAmount.font = fontBold;
        _prizeWinAmount.x = prizeWinLabel.x;
        _prizeWinAmount.y = prizeWinLabel.y + 20;

        var coinsWinLabel = this.coinsLabel;
        coinsWinLabel.text = "COINS";
        coinsWinLabel.font = font;
        coinsWinLabel.x = 342;
        coinsWinLabel.y = prizeWinLabel.y;

        var coinsWinAmount = this._coinsWinAmount;
        coinsWinAmount.font = fontBold;
        coinsWinAmount.x = coinsWinLabel.x;
        coinsWinAmount.y = coinsWinLabel.y + 20;

        var stakeLabel = this.stakeLabel;
        stakeLabel.text = "STAKE";
        stakeLabel.font = font;
        stakeLabel.x = 238;
        stakeLabel.y = prizeWinLabel.y;

        var stakeAmount = this.stakeAmount;
        stakeAmount.font = fontBold;
        stakeAmount.x = stakeLabel.x;
        stakeAmount.y = _prizeWinAmount.y;//345

        var stakeAmountMock = this.stakeAmountMock;
        stakeAmountMock.font = "bold 16px HurmeGeometricSans3";
        stakeAmountMock.x = 862;
        stakeAmountMock.y = 349;//345

        var betCounter = this._betCounter;
        betCounter.x = 786;
        betCounter.y = 48;
        betCounter.adjustMobile();

        var btnParticipate = this.btnParticipate;
        btnParticipate.x = betCounter.x + 75;
        btnParticipate.y = betCounter.y + 444;

        for(var p in btnParticipate._presetFonts){
            btnParticipate._presetFonts[p].text.font = "bold 14px HurmeGeometricSans3";
        }

        btnParticipate.selectTextPreset("start");

        var spinner = this._waitingSpinner;
        spinner.customGfxPosition(0, -22);
        spinner.customFont("bold 12px HurmeGeometricSans3", "center");
        spinner.customScaleGfx(0.2);
        spinner.x = btnParticipate.x;
        spinner.y = btnParticipate.y + 11;

        this._coinSpawnAnimationParams = function(coin){
            coin.setGravity(0.75);
            coin.setVR(-5,5);
            coin.setVX(-0.75,0.75);
            coin.setVY(-14,-16);
            coin.animate();
        };
    }

    alteastream.MultiplayerControls = createjs.promote(MultiplayerControls,"Container");
})();
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var Pagination = function(options){
        this.Container_constructor();
        this.initPagination(options);
    };

    var p = createjs.extend(Pagination,createjs.Container);
    var _this;

    // private properties
    p._dotsContainer = null;
    p._currentDot = null;
    p._componentWidth = 0;
    p._numOfPages = 0;
    p._dotWidth = 0;
    p._dotSpacing = 0;
    p._upButton = null;
    p._downButton = null;
    p._dotsBackground = null;
    // public properties
    p.hasButtons = false;
    p.textUpPosition = null;
    p.textDownPosition = null;

    p.initPagination = function (options) {
        console.log("Pagination initialized:::");
        _this = this;
        if(options.dotsBackground){
            var dotsBackground = this._dotsBackground = new createjs.Shape();
            dotsBackground.graphics.beginFill("#000000")[options.dotsBackground.shape](options.dotsBackground.x, options.dotsBackground.y, options.dotsBackground.w, options.dotsBackground.h, options.dotsBackground.roundness);
            dotsBackground.alpha = options.dotsBackground.alpha;
            dotsBackground.cache(options.dotsBackground.x, options.dotsBackground.y, options.dotsBackground.w, options.dotsBackground.h);
            this.addChild(dotsBackground);
        }
        this._numOfPages = options.numberOfPages;
        this._dotSpacing = options.dotsSpacing;
        this._dotWidth = options.dotsWidth;
        this._dotsContainer = new createjs.Container();
        this.addChild(this._dotsContainer);
        this.addDots(options);
        this._dotsContainer.on("click", function (e) {
            options.scroller.dotClicked(e.target.parent.name);
        });
    };

    // private methods
    p._setButtonsStateOnStart = function(numOfPages) {
        if(numOfPages > 1){
            this._upButton.setDisabled(true);
            this._downButton.setDisabled(false);
        }
    };

    p._setComponentWidth = function() {
        this._componentWidth = (this._dotWidth + this._dotSpacing) * (this._numOfPages - 1) + this._dotWidth;
        if(this._componentWidth < this._dotWidth){
            this._componentWidth = this._dotWidth;
        }
    };

    p._resetDotsBackground = function() {
        var oldValues = {
            x:this._dotsBackground.bitmapCache.x,
            y:this._dotsBackground.bitmapCache.y,
            h:this._dotsBackground.bitmapCache.height,
            roundness:this._dotsBackground.graphics.command.radiusBL,
            alpha:this._dotsBackground.alpha
        }
        var newWidth = ((this._numOfPages) * (this._dotWidth + this._dotSpacing)) - (this._dotSpacing);
        var dotsBack = this._dotsBackground;
        dotsBack.uncache();
        dotsBack.graphics.clear();
        dotsBack.graphics.beginFill("#000000").drawRoundRect(oldValues.x, oldValues.y, newWidth, oldValues.h, oldValues.roundness);
        dotsBack.alpha = oldValues.alpha;
        dotsBack.cache(oldValues.x, oldValues.y, newWidth, oldValues.h);
    };

    // public methods
    p.setUpButton = function(button, x, y, scroller) {
        this._upButton = button;
        this._upButton.x = x;
        this._upButton.y = y;
        this.addChild(this._upButton);
        this._upButton.setClickHandler(function () {
            scroller.moveUp();
        });
        this.hasButtons = true;
    };

    p.setDownButton = function(button, x, y, scroller) {
        this._downButton = button;
        this._downButton.x = x;
        this._downButton.y = y;
        this.addChild(this._downButton);
        this._downButton.setClickHandler(function () {
            scroller.moveDown();
        });
    };

    p.disableButtons = function(disable) {
        this._upButton.setDisabled(disable);
        this._downButton.setDisabled(disable);
    };

    p.addDots = function(options) {
        var y = 0;
        var counter = 0;
        var spacing = this._dotSpacing;
        var R = this._dotWidth;
        var r = R/4;
        var maxDotPerLine = Math.floor(1360 / spacing);

        for(var i = 0; i < this._numOfPages; i++){
            var cont = new createjs.Container();
            var dot;
            if(options.customDot){
                dot = alteastream.Assets.getImage(alteastream.Assets.images[options.customDot]);
                dot.regX = dot.image.width/2;
                dot.regY = dot.image.height/2;
            }else{
                dot = new createjs.Shape();
                dot.graphics.beginFill("#6e6e6e").drawCircle(0, 0, r);
                dot.cache(-r,-r,R,R);
            }

            cont.x = (i * R) + (spacing * i);
            cont.y = y;
            cont.name = i;

            cont.addChild(dot);

            if(options.dotFont){
                var num = new createjs.Text(i+1,"16px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle"});
                num.y = 2;
                cont.addChild(num);
            }

            this._dotsContainer.addChild(cont);
            counter++;
            if(counter === maxDotPerLine){
                counter = 0;
                y = 22;
            }
        }

        var cd = this._currentDot = new createjs.Container();
        cd.mouseEnabled = false;
        var currentDot;
        if(options.customCurrentDot){
            currentDot = alteastream.Assets.getImage(alteastream.Assets.images[options.customCurrentDot]);
            currentDot.regX = currentDot.image.width/2;
            currentDot.regY = currentDot.image.height/2;
        }else{
            currentDot = new createjs.Shape();
            currentDot.graphics.beginFill("#ffffff").drawCircle(0, 0, r);
            currentDot.cache(-r,-r,R,R);
            currentDot.x = 0;
            currentDot.y = 0;
            currentDot.mouseEnabled = false;
        }

        cd.addChild(currentDot);

        if(options.currentDotFont){
            var txt = new createjs.Text(1,"16px HurmeGeometricSans3","#000000").set({textAlign:"center", textBaseline:"middle"});
            txt.y = 2;
            cd.addChild(txt);
        }

        this._dotsContainer.addChild(cd);

        this._setComponentWidth();
    };

    p.adjustDotsTextFields = function(x, y, texts) {
        for(var i = 0; i < this._dotsContainer.numChildren-1; i++){
            var dot = this._dotsContainer.getChildAt(i);
            var text = dot.getChildAt(1);
            text.x = x;
            text.y = y;
            if(texts){
                text.text = texts[i];
            }
        }
    };

    p.setShorAndLongTexts = function () {
        for(var i = 0; i < this._dotsContainer.numChildren-1; i++){
            var dot = this._dotsContainer.getChildAt(i);
            var text = dot.getChildAt(1);
            var words = text.text.split(" ");
            var shortText = words.length > 1 ? words[0] + "..." : text.text;
            if(shortText.length > 10){
                shortText = shortText.slice(0,7) + "...";
            }

            //text.short = words.length > 1 ? words[0] + "..." : text.text;
            text.short = shortText;
            text.long = text.text;
            if(i > 0){
                text.text = text.short;
            }else{
                text.y = this.textUpPosition;
            }
        }
    };

    p.removeTextFromCurrentDot = function() {
        var currentDotContainer = this._dotsContainer.getChildAt(this._dotsContainer.numChildren-1);
        currentDotContainer.removeChildAt(1);
    };

    p.getPaginationWidth = function(){
        return this._componentWidth;
    };

    p.moveCurrentDot = function(index) {
        var current = this._dotsContainer.getChildAt(index);
        this._currentDot.x = current.x;
        this._currentDot.y = current.y;
        if(this._currentDot.getChildAt(1)){
            this._currentDot.getChildAt(1).text = current.getChildAt(1).text;
        }
    };

    p.doMovingDotScaleAnimation = function(selected) {// this method is only used by house selection
        for(var i = 0; i < this._dotsContainer.numChildren; i++){
            var dot = this._dotsContainer.getChildAt(i);
            if(dot.scale > 1){
                createjs.Tween.get(dot).to({scale:1},500,createjs.Ease.quadOut);
                var dotText = dot.getChildAt(1);
                if(dotText){// we need to check if there is text because currentDot container doesn't contain it
                    createjs.Tween.get(dotText).to({y:this.textDownPosition},500,createjs.Ease.quadOut);
                    dotText.text = dotText.short;
                }
            }
        }

        this._currentDot.scale = 1;
        var selectedDot = this._dotsContainer.getChildAt(selected);
        var selectedDotText = selectedDot.getChildAt(1);
        createjs.Tween.get(selectedDot).to({scale:1.2},500,createjs.Ease.quadOut);
        createjs.Tween.get(this._currentDot).to({scale:1.2},500,createjs.Ease.quadOut);
        createjs.Tween.get(selectedDotText).to({y:this.textUpPosition},500,createjs.Ease.quadOut).call(function (){
            selectedDotText.text = selectedDotText.long;
        });
    };

    p.setComponentForListView = function(setForList){
        var w = this._dotWidth;
        var cdShape = this._currentDot.getChildAt(0);
        cdShape.uncache();
        cdShape.graphics.clear();
        var r = w/4;
        if(setForList){
            cdShape.graphics.beginFill("#ffffff").drawCircle(0, 0, r);
            cdShape.cache(-r,-r,w,w);
        }else{
            cdShape.graphics.beginFill("#ffffff").drawRect(-20, -20, 40, 40);
            cdShape.cache(-20, -20, 40, 40);
        }
        this._currentDot.getChildAt(1).visible = !setForList;
        for(var i = 0; i < this._dotsContainer.numChildren-1; i++){
            this._dotsContainer.getChildAt(i).getChildAt(0).alpha = setForList ? 1 : 0.01;
            this._dotsContainer.getChildAt(i).getChildAt(1).visible = !setForList;
        }
    };

    p.resetComponent = function(numOfPages) {
        this._numOfPages = numOfPages;
        for(var i = 0; i < this._dotsContainer.numChildren-1; i++){
            this._dotsContainer.getChildAt(i).visible = i < numOfPages;
        }
        if(this._dotsBackground){
            this._resetDotsBackground();
        }
        this._setComponentWidth();
        this.regX = this._componentWidth * 0.5;
    };

    p.showPagination = function(show) {
        this.visible = show;
    };

    p.hideUI = function(bool){
        this._upButton.visible = !bool;
        this._downButton.visible = !bool;
    };

    p.enablePaginationClick = function(enable) {
        this.mouseEnabled = enable;
    };

    alteastream.Pagination = createjs.promote(Pagination,"Container");
})();


(function(){
    "use strict";

    var QueuePanel = function(){
        this.Container_constructor();
        this.initQueuePanel();
    };

    var p = createjs.extend(QueuePanel,createjs.Container);

    // private properties
    p._machineButton = null;
    p._availabilityImage = null;
    p._availabilityText = null;
    p._firstInQueue = false;
    p._queueLeaveCallback = null;
    // public methods
    p.currentMachineInfo = null;
    p.yourPositionInQueue = 0;
    p.queueIsFree = true;
    p.dirtyMachines = [];
    p.previousMachineActive = false;
    p.queueExists = false;

    var _instance = null;

    p.initQueuePanel = function () {
        console.log("QueuePanel initialized:::");
        _instance = this;

        var bannerContainer = this.bannerContainer = new createjs.Container();

        var labelsFont = "15px HurmeGeometricSans3";
        var valuesFont = "bold 24px HurmeGeometricSans3";

        var availabilityText = this._availabilityText = new createjs.Text("",labelsFont,"#a39f9e").set({textAlign:"center", textBaseline:"middle"});
        var machinesQueueNumber = this.machinesQueueNumber = new createjs.Text("0",valuesFont,"#ffcc00").set({textAlign:"center",textBaseline:"middle"});
        var positionInQueueText = this.positionInQueueText = new createjs.Text("YOUR POSITION",labelsFont,"#a39f9e").set({textAlign:"center",textBaseline:"middle"});
        var positionInQueueNumber = this.positionInQueueNumber = new createjs.Text("--",valuesFont,"#ffcc00").set({textAlign:"center",textBaseline:"middle"});

/*        if(alteastream.AbstractScene.HAS_COPLAY){
            var multiPlayerControls = this.multiPlayerControls = new alteastream.MultiplayerControls();
            this.addChild(multiPlayerControls);
            this.multiPlayerControls.show(false);
        }else{
            positionInQueueText.x = positionInQueueNumber.x = 350;
            positionInQueueText.y = 900;
            positionInQueueNumber.y = 900 + 23;
        }*/

        positionInQueueText.x = positionInQueueNumber.x = 350;
        positionInQueueText.y = 900;
        positionInQueueNumber.y = 900 + 23;

        bannerContainer.addChild(availabilityText, machinesQueueNumber, positionInQueueText, positionInQueueNumber);
        this.addChild(bannerContainer);
        bannerContainer.visible = false;
        bannerContainer.mouseEnabled = false;
        bannerContainer.mouseChildren = false;
        bannerContainer.y-=11;

        var entranceCounterBackground = this._entranceCounterBackground = alteastream.Assets.getImage(alteastream.Assets.images.infoBackground);
        entranceCounterBackground.visible = false;
        entranceCounterBackground.x = -100;//this.x
        entranceCounterBackground.y = -121;//this.y
        this.addChild(entranceCounterBackground);

        var entranceCounter = this.entranceCounter = new alteastream.EntranceCounter();
        this.addChild(entranceCounter);
        entranceCounter.mouseEnabled = false;
        entranceCounter.mouseChildren = false;
        this.entranceCounter.show(false);

        var btnStartText = new createjs.Text("PLAY NOW","bold 32px Roboto", "#ffffff").set({x:0,y:0,textAlign:"center",textBaseline:"middle"});
        var btnStart = this.btnStart = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnStart),3,btnStartText);
        btnStart.x = 860;//743
        btnStart.y = 615;//575
        this.addChild(btnStart);
        btnStart.setClickHandler(this.startGame);
        btnStart.visible = false;

        this.setSpinnerBackground();
        var fontSpinner = "26px HurmeGeometricSans3";
        var spinner = this.spinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = this.spinnerBackground.x + 960;
        spinner.y = this.spinnerBackground.y + 470;
        spinner.customScaleGfx(1.4);
        spinner.customGfxPosition(0, 240);
        spinner.customFont(fontSpinner, "center");
        spinner.mouseEnabled = false;
        spinner.setLabel("MAKING ROOM FOR YOU\n\nPLEASE WAIT...");
        spinner.runPreload(false);
    };

    QueuePanel.getInstance = function(){
        return _instance;
    };

    p.addMultiplayerControls = function (participateCounter) {
        var multiPlayerControls = this.multiPlayerControls = new alteastream.MultiplayerControls();
        this.addChild(multiPlayerControls);
        this.multiPlayerControls.showMultiPlayerControls(false);
        this.multiPlayerControls.setBettingCounterTime(participateCounter);
        if(window.localStorage.getItem('isMobile') === "yes"){
            this.multiPlayerControls.adjustMobile();
        }
    };

    p._createText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline, visible:props.visible});
        this.addChild(textInstance);
    };

    p._updateAvailabilityComponents = function(machineInfo){// metoda se zove samo u lokalnoj verziji
        this._availabilityText.text = machineInfo.isFreeToPlay ? "AVAILABLE" : "CO-PLAY";
        //this.positionInQueueText.visible =
            //this.positionInQueueNumber.visible =
                this.machinesQueueNumber.visible = !machineInfo.isFreeToPlay;

        if(!alteastream.AbstractScene.HAS_COPLAY){
            this.machinesQueueNumber.visible = false;
            this._availabilityText.visible = false;
        }
    };

    //new socket
    p.updateAvailabilityComponents = function(isFreeToPlay){
        this._availabilityText.text = isFreeToPlay === true  ? "AVAILABLE" : "CO-PLAY";
        //this.positionInQueueText.visible =
            //this.positionInQueueNumber.visible =
                this.machinesQueueNumber.visible = !isFreeToPlay;

        if(!alteastream.AbstractScene.HAS_COPLAY){
            this.machinesQueueNumber.visible = false;
            this._availabilityText.visible = false;
        }

        this._entranceCounterBackground.visible = isFreeToPlay;
    };

    p.activateWaitingSpinner = function (activate) {
        if(!alteastream.AbstractScene.HAS_COPLAY)
            return;
        // possible fix for double spinner
        // also remove if check from _handleQueueFailed
        //if(this.spinner.active !== activate){
        this.multiPlayerControls.enable(false);
        this.multiPlayerControls.showMultiPlayerControls(false);
        this.spinnerBackground.visible = activate;
        this._entranceCounterBackground.visible = true;
        var lm = alteastream.LocationMachines.getInstance();
        lm.machineCoinsAndPrizesInfo.visible = false;
        //lm._pagination.showPagination(false);
        lm.setBackButtonForEntranceCounter(activate);
        this.spinner.runPreload(activate);
        //}
    };

    p.setSpinnerBackground = function () {
        var spinnerBackground = this.spinnerBackground = new createjs.Container();
        spinnerBackground.mouseEnabled = false;

        var fatCircle = new createjs.Shape();
        fatCircle.graphics.setStrokeStyle(19).beginStroke("#f9c221").arc(0, 0, 250, 0, (Math.PI/180) * 310).endStroke();
        fatCircle.rotation = -245;
        fatCircle.x = 960;
        fatCircle.y = 511;

        var thinCircle = new createjs.Shape();
        thinCircle.graphics.clear().setStrokeStyle(3).beginStroke("#744006").arc(0, 0, 320, 0, (Math.PI/180) * 328).endStroke();
        thinCircle.rotation = -254;
        thinCircle.x = 960;
        thinCircle.y = 511;

        spinnerBackground.addChild(fatCircle, thinCircle);
        spinnerBackground.x = -100;// location machines x = 100
        spinnerBackground.y = -121;// location machines y = -121
        spinnerBackground.visible = false;
        this.addChild(spinnerBackground);
    };


                // filtering


        // testing making room for you
        /*        setTimeout(function (){
                    _instance.activateWaitingSpinner(true);
                },1000);*/


    //new socket
    p.enterQueue = function(){
        console.log("machine_id on enterQueue "+alteastream.AbstractScene.GAME_ID);
        this.enableStart(false);
        alteastream.SocketCommunicatorLobby.getInstance().enterQueue();
    };

    //new socket
    p.onQueueEnter = function(parsedMsg){
        console.log(" ADDED to queue::::::::::::::");
        _instance.queueExists = true;
        _instance._firstInQueue = true;

        //_instance.entranceCounter.show(false);
        //_instance.btnStart.visible = false;
        _instance.enableStart(false);

            if(_instance.spinner.active){
                //_instance.spinner.runPreload(false);
                //_instance.activateWaitingSpinner(false);
            }
            _instance.visible = true;
            _instance.positionInQueueText.visible = true;
            _instance.positionInQueueNumber.visible = true;
            //_instance.multiPlayerControls.setConfirmedStake(parsedMsg.stake);//proveriti sa Andrijom

            if(alteastream.AbstractScene.HAS_COPLAY){
                var stake = parsedMsg.stake || 13;
                _instance.multiPlayerControls.setConfirmedStake(stake);
            }

            _instance.parent.onQueueEntered();
            alteastream.LocationMachines.getInstance().visible = true;

        //_instance.updateAvailabilityComponents(_instance.queueIsFree);
    };

    // new socket
    p.onQueueDeferred = function(code){
        _instance.resetDisplay();
        //new socket
        //_instance.updateAvailabilityComponents(_instance.queueIsFree);
        // if(code === 7){
        // _instance.machineCleared = false;
        //}
        _instance.dirtyMachines.push(_instance.parent.streamPreview.currentStream);
        alteastream.LocationMachines.getInstance().visible = true;
    };

    p.resetDisplay = function(){
        _instance.queueIsFree = false;
        _instance._firstInQueue = false;
        _instance.visible = true;
        _instance.entranceCounter.show(false);
        _instance.btnStart.visible = false;
        _instance.activateWaitingSpinner(true);
        _instance.parent._machinesComponent.visible = false;
        _instance.parent._locationLabel.visible = false;
    };

    p.machineIsDirty = function(machineIndex){
        if(_instance.dirtyMachines.indexOf(machineIndex)>-1){
            _instance.dirtyMachines.splice(_instance.dirtyMachines.indexOf(machineIndex),1);
            return _instance.parent.streamPreview.currentStream === machineIndex;
        }
    };

    //new socket
    p.onQueueError = function(code){
        _instance._firstInQueue = false;
        alteastream.Lobby.getInstance().throwAlert(alteastream.Alert.ERROR, "queuing failed: "+code,function(){
            _instance.parent.backToLobbyButtonHandler();
        });
    };

    //new socket
    p.exitQueue = function(){ //+leaveQueue mora za njim, ne na expire
        this.entranceCounter.clearCounterInterval();
        if(alteastream.AbstractScene.HAS_COPLAY){
            this.multiPlayerControls.showMultiPlayerControls(false);
            this.multiPlayerControls.enable(false);
            this.multiPlayerControls.quitBetting();
        }

        this.resetEntrance();
        this.visible = false;
        this.positionInQueueText.visible = false;
        this.positionInQueueNumber.visible = false;
        this.bannerContainer.visible = false;
    };


    //new socket
    p.leaveQueue = function(callback){
        if(this.yourPositionInQueue > 0){
            this.setQueueLeaveCallback(callback);
            alteastream.SocketCommunicatorLobby.getInstance().leaveQueue();
        }
    };

    //new socket
    p.onQueueLeave = function(){
        this._firstInQueue = false;
        if(this._queueLeaveCallback){
            this._queueLeaveCallback();
            this.setQueueLeaveCallback(null);
        }
    };
    //new socket
    p.setQueueLeaveCallback = function(callback){
        this._queueLeaveCallback = callback;
    };

    p.manageQueueReadyState = function(waitTime){
        this.updateAvailabilityComponents(true);
        this.enableStart(true);
        this.entranceCounter.update(waitTime);

        if(this.spinner.active){
            this.activateWaitingSpinner(false);
        }

        this.parent.setBackButtonForEntranceCounter(true);
    };

    p.startGame = function(){
        _instance.parent.onStartGame();
    };



    //new socket
    p.enableStart = function(bool){
        this.btnStart.setDisabled(!bool);
        this.btnStart.visible = bool;
        this.entranceCounter.show(bool);
        if(bool === true){
            this.multiPlayerControls.showMultiPlayerControls(false);
        }
    };



    //new socket
    p.updateQueueInfo = function(msgVl){
        //if(msgVl.size){
        var parsedMsg = JSON.parse(msgVl);
        console.log("parced msg");
        console.log(parsedMsg);
        if(typeof parsedMsg == "object"){
            this.yourPositionInQueue = parsedMsg.you;
            this.machinesQueueNumber.text = parsedMsg.size;
            this.positionInQueueNumber.text = this.yourPositionInQueue;
        }else{
            this.yourPositionInQueue = 0;
            this.machinesQueueNumber.text = msgVl;
            this.positionInQueueNumber.text = this.yourPositionInQueue;
        }
    };

    p.resetEntrance = function(wait){
        this._firstInQueue = false;
        this.enableStart(false);
        if(!wait){
            this.activateWaitingSpinner(false);
        }
    };

    p.setCurrentMachine = function(machineInfo) {
        alteastream.AbstractScene.GAME_ID = machineInfo.machineIndex;
        alteastream.Lobby.getInstance().currentMap.clickEnabled(false);
        if(this.spinner.active === true){
            this.activateWaitingSpinner(false);
        }
    };

    p.setTextsPositionForMultiplayer = function(setForMp) {
        this._availabilityText.font = this.positionInQueueText.font = setForMp === true ? "15px HurmeGeometricSans3" : "bold 22px HurmeGeometricSans3";
        this._availabilityText.color = this.positionInQueueText.color = setForMp === true ? "#a39f9e" : "#ffffff";

        this._availabilityText.x = this.machinesQueueNumber.x = setForMp === true ? 1235 : 1585;//347
        this.positionInQueueText.x = this.positionInQueueNumber.x = setForMp === true ? 1381 : 1585;//497

        var ySpacing = setForMp === true ? 23 : 28;

        this._availabilityText.y = setForMp === true ? 900 : 358;
        this.machinesQueueNumber.y = this._availabilityText.y + ySpacing;

        this.positionInQueueText.y = setForMp === true ? 900 : 657;
        this.positionInQueueNumber.y = this.positionInQueueText.y + ySpacing;
    };

    p.adjustMobile = function(){
        this._availabilityText.font = "12px HurmeGeometricSans3";
        this._availabilityText.x = 862;
        this._availabilityText.y = 267;

        this.machinesQueueNumber.font = "bold 16px HurmeGeometricSans3";
        this.machinesQueueNumber.x = this._availabilityText.x;
        this.machinesQueueNumber.y = this._availabilityText.y + 20;

        this.positionInQueueText.font = "11px HurmeGeometricSans3";
        this.positionInQueueText.x = 265;
        this.positionInQueueText.y = 493;

        this.positionInQueueNumber.font = "bold 16px HurmeGeometricSans3";
        this.positionInQueueNumber.x = this.positionInQueueText.x;
        this.positionInQueueNumber.y = this.positionInQueueText.y + 20;

        this.btnStart.text.font = "bold 24px Roboto";
        this.btnStart.x = 470;
        this.btnStart.y = 346;

        this.spinner.x = 470;
        this.spinner.y = 220;
        this.spinner.customScaleGfx(0.8);
        this.spinner.customFont("15px HurmeGeometricSans3", "center");
        this.spinner.customGfxPosition(0, 130);

        this.spinnerBackground.x = -10;// location machines x = 10
        this.spinnerBackground.y = -10;// location machines y = 10

        this._entranceCounterBackground.x = -10;
        this._entranceCounterBackground.y = -10;

        var fatCircle = this.spinnerBackground.getChildAt(0);
        var thinCircle = this.spinnerBackground.getChildAt(1);

        fatCircle.graphics.clear().setStrokeStyle(10).beginStroke("#f9c221").arc(0, 0, 145, 0, (Math.PI/180) * 310).endStroke();
        fatCircle.rotation = -245;
        fatCircle.x = 480;
        fatCircle.y = 243;

        thinCircle.graphics.clear().setStrokeStyle(1).beginStroke("#744006").arc(0, 0, 200, 0, (Math.PI/180) * 308).endStroke();
        thinCircle.rotation = -244;
        thinCircle.x = fatCircle.x;
        thinCircle.y = fatCircle.y;

        this.entranceCounter.adjustMobile();
        //if(alteastream.AbstractScene.HAS_COPLAY)
            //this.multiPlayerControls.adjustMobile();

        this.setTextsPositionForMultiplayer = function(setForMp) {
            _instance._availabilityText.color = _instance.positionInQueueText.color = setForMp === true ? "#a39f9e" : "#ffffff";
            _instance._availabilityText.x = _instance.machinesQueueNumber.x = setForMp === true ? 600 : 862;
            _instance.positionInQueueText.x = _instance.positionInQueueNumber.x = setForMp === true ? _instance.machinesQueueNumber.x + 126 : _instance.machinesQueueNumber.x;

            _instance._availabilityText.y = setForMp === true ? 493 : 267;
            _instance.machinesQueueNumber.y = _instance._availabilityText.y + 23;

            _instance.positionInQueueText.y = setForMp === true ? 493 : 398;
            _instance.positionInQueueNumber.y = _instance.positionInQueueText.y + 23;

            _instance._availabilityText.font = _instance.positionInQueueText.font = setForMp === true ? "11px HurmeGeometricSans3" : "12px HurmeGeometricSans3";
            _instance.machinesQueueNumber.font = _instance.positionInQueueNumber.font = setForMp === true ? "bold 14px HurmeGeometricSans3" : "bold 16px HurmeGeometricSans3";
        };
    };

    alteastream.QueuePanel = createjs.promote(QueuePanel,"Container");
})();this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var SpectatorStream = function (scene,options,address){
        this.ABSVideoStream_constructor(scene,options,address);
        this.initialize_SpectatorStream();
    };

    var p = createjs.extend(SpectatorStream, alteastream.ABSVideoStream);
// static properties:
// events:

// public vars:
// private vars:
    p.streamMonitor = null;
    p.spinner = null;
    p.spinnerBackground = null;
// constructor:
    p.initialize_SpectatorStream = function (){
    };

    p.setPlayer = function(address){
        if(address!==null){
            this.machineInfo = address;
            var streamPreviewParams = JSON.parse(localStorage.getItem("streamPreviewParams"));
            this.webrtcPlayer.setPreview(address.previewCamera, alteastream.AbstractScene.SHOP_MACHINE, address.machineName);//streamPreviewParams.machineName
        }
    };

    p.handleError = function(error){
        var that = this;
        this.scene.throwAlert(alteastream.Alert.ERROR,error,function(){
            that.webrtcPlayer.stop();
            console.error("ERROR");
        });
    }

    p.concreteActivate = function(){
        this.scene.frame.visible = false;
        this.spinner.runPreload(false);
        this.scene.removeChild(this.spinner);
        this.scene.removeChild(this.spinnerBackground);
        this.spinner = null;
    };

    p.addSpinner = function(){
        //var loadAnimation = new alteastream.SSAnimator("animatedLogo",250,244,19);
        //var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        var spinner = this.spinner = new alteastream.MockLoader();
        this.scene.addChild(spinner);
        spinner.x = 864;
        spinner.y = 372;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;

        var isMobile = window.localStorage.getItem('isMobile');
        if(isMobile === "yes"){
            this.adjustMobile();
        }
    }

    p.setMedia = function(){

    }

    p.setPreloadPoster = function(bool){
        /*var poster = alteastream.Assets.getImage(alteastream.Assets.images.poster);
        this.scene.addChild(poster);
        poster.mouseEnabled = false;*/
    }

    p.addMonitor = function(){
        this.streamMonitor = new alteastream.StreamMonitor(this);
    };

    p.removeMonitor = function(){
        this.streamMonitor._stopMonitor();
    };

    p.super_stop = p.stop;
    p.stop = function(){
        this.super_stop();
        if(this.streamMonitor) {// todo temp fix
            this.removeMonitor();
        }
        if(this.spinner){// todo temp fix
            this.spinner.runPreload(false);
        }
    };

    p.streamConnected = function(){
        this.streamMonitor.streamConnected();
        this.spinner.runPreload(false);
        this.scene.frame.visible = false;
    };

    p.startStreaming = function(){
        this.webrtcPlayer.start();//preview
    };

    p.onStreamFailed = function(){
        this.spinner.runPreload(false);
        this.spinner.preloadInfoText.visible = true;
        this.spinner.setLabel("Preview for "+this.scene.currentMachineName+" failed","#ff0000");
    };

    p.adjustMobile = function () {
        //var loadAnimation = new alteastream.SSAnimator("animatedLogo",125,122,19);
        //var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        var spinner = this.spinner = new alteastream.MockLoader();
        this.scene.addChild(spinner);
        spinner.x = 470;
        spinner.y = 160;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;
    };

    p._local_activate = function(){//click on screen while loading to start video
        this.videoElement.setSource("../lobby/videotest.mp4");
        this.videoElement.play();
        this.videoElement.setLoop(true);
        this.videoElement.setMuted(false);

        //this.scene.controlBoard.restoreMachineSoundState();
        //this.scene.controlBoard.restoreMusicSoundState();
    };

    alteastream.SpectatorStream = createjs.promote(SpectatorStream,"ABSVideoStream");
})();this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreview = function(){
        this.Container_constructor();
        this.initStreamPreview();
    };

    var p = createjs.extend(StreamPreview,createjs.Container);
    var _this = null;
// private properties
    p._active = false;
// public properties
    p.currentStream = "";
    p.currentMachineName = "";

    p.initStreamPreview = function () {
        console.log("StreamPreview initialized:::");
        _this = this;

        var options = {id:"videoOutput",width:1920,height:1080,style:{width:"100%",height:"100%"}};
        this.streamVideo = new alteastream.SpectatorStream(this,options,null);

        var frame = this.frame = alteastream.Assets.getImage(alteastream.Assets.images.window_big);
        frame.x = -100;// location machines x = 100
        frame.y = -121;// location machines y = 121
        frame.scaleX = 4.8;
        frame.scaleY = 4;
        this.addChild(frame);
        frame.mouseEnabled = false;
    };

    StreamPreview.getInstance = function(){
        return _this;
    };

// private methods
// public methods
    p.activate = function(){

    };

    p.start = function(){
        this._active = true;
        alteastream.LocationMachines.getInstance().resetIntervalUpdateLounge();
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p.exitPreview = function(){
        //backToLobbyButtonHandler
        this.dispose();
        console.log("EXITED STREAM PREVIEW");
    };

    p.dispose = function(){
        //onStartGame
        this._active = false;
        this.streamVideo.stop();
        this.currentStream = "";
        this.frame.visible = true;
        this.show(false);
        console.log("DISPOSED STREAM PREVIEW");
    };

    p.tryToSwitchStream = function(machine) {
        if(this.currentStream !== machine.machineInfo.machineIndex){// if !machine.selected?
            alteastream.MachinesComponent.getInstance().clearActiveMachines();
            alteastream.SocketCommunicatorLobby.getInstance().lobbySwitchSubscriptions(this.currentStream,machine.machineInfo.machineIndex);
            alteastream.QueuePanel.getInstance().setCurrentMachine(machine.machineInfo);

            machine.selectMachine(true);
            // stream preview roulette
            //if(alteastream.Lobby.getInstance().selectedLocation.casinoName === "Roulette"){
            if(machine.machineInfo.gameCode === "roulette"){
                this.switchStream(machine.machineInfo);
                var obj = {
                    previewCamera:machine.machineInfo.previewCamera,
                    shopMachine:alteastream.AbstractScene.SHOP_MACHINE,
                    machineName:machine.machineInfo.machineName,
                    casinoName:alteastream.Lobby.getInstance().selectedLocation.casinoName
                }

                var objToStore = JSON.stringify(obj);
                window.localStorage.setItem("streamPreviewParams", objToStore);
            }// stream preview roulette
            else{
                if(!machine.isFreeToPlay){
                    this.switchStream(machine.machineInfo);
                    this.changeStream(machine.machineInfo);
                    this.changeFrameImage(machine.thumb);
                }else{
                    this.updateInfo(machine.machineInfo);
                }
            }
        }
    };

    p.switchStream = function(machineInfo){
        this.currentStream = machineInfo.machineIndex;
        this.currentMachineName = machineInfo.machineName;
        this.updateInfo(machineInfo);
        this.start();
    };

    p.changeStream = function(machineInfo){
        this.show(true);
        this.frame.visible = true;
        this.streamVideo.setPlayer(machineInfo);
        this.streamVideo.activate();
        this.streamVideo.spinner.setLabel("Connecting to machine "+this.currentMachineName);
        this.streamVideo.spinner.runPreload(true);
    };

    p.changeFrameImage = function(thumb) {
        if(this.frame.visible){
            gamecore.Utils._removeAllFilters([this.frame]);
            this.frame.image = thumb.hasThumbImage === true ? thumb.image : alteastream.Assets.getImage(alteastream.Assets.images.thumb).image;
            gamecore.Utils.DISPLAY.FILTERS.setBlur([this.frame], 2);
        }
    };

    p.updateInfo = function(machineInfo){
        this.machineInfo = machineInfo;
        alteastream.AbstractScene.GAME_CODE = machineInfo.gameCode;
        alteastream.AbstractScene.DEFAULT_GAME_ID = machineInfo.defaultGameId;
    };

    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.labelsContainer.addChild(textInstance);
        textInstance.mouseEnabled = false;
    };

    p.adjustMobile = function(){
/*        var footer = new createjs.Shape();
        footer.graphics.beginFill("#000000").drawRoundRect(0, 0, 616, 40, 3);
        footer.cache(0, 0, 616, 40);
        footer.alpha = 0.3;
        footer.x = 319;
        footer.y = 429;

        this.addChildAt(footer, 0);*/

        this.frame.x = -10;// location machines x = 10
        this.frame.y = -10;// location machines y = 10
        this.frame.scaleX = 2;
        this.frame.scaleY = 2;

/*        if(this.spinner)
            this.removeChild(this.spinner);

        var loadAnimation = new alteastream.SSAnimator("animatedLogo",125,122,19);
        var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        this.addChild(spinner);
        spinner.x = 470;
        spinner.y = 160;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;*/

        //this.removeChild(this.frame);//todo proveriti
    }

    p._local_activate = function(){
        this.currentStream = 333333333;
        this.start();

        this.frame.visible = true;
        setTimeout(function(){
            _this.frame.visible = false;
            console.log("StreamLoaded::::");
        },1000);
    };

    alteastream.StreamPreview = createjs.promote(StreamPreview,"Container");
})();this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreviewTest = function(){
        this.Container_constructor();
        this.initStreamPreviewTest();
    };

    var p = createjs.extend(StreamPreviewTest,createjs.Container);
    var _this = null;
// private properties
    p._active = false;
// public properties
    p.currentStream = "";
    p.currentMachineName = "";

    p.initStreamPreviewTest = function () {
        console.log("StreamPreviewTest initialized:::");
        _this = this;

        var options = {id:"videoOutput",width:1920,height:1080,style:{width:"100%",height:"100%"}};
        this.streamVideo = new alteastream.SpectatorStream(this,options,null);

        var frame = this.frame = alteastream.Assets.getImage(alteastream.Assets.images.window_big);
        frame.x = -100;// location machines x = 100
        frame.y = -121;// location machines y = 121
        frame.scaleX = 4;
        frame.scaleY = 4;
        this.addChild(frame);
        frame.mouseEnabled = false;
    };

    StreamPreviewTest.getInstance = function(){
        return _this;
    };

// private methods
// public methods
    p.activate = function(){

    };

    p.start = function(){
        this._active = true;
        alteastream.LocationMachines.getInstance().resetIntervalUpdateLounge();
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p.exitPreview = function(){
        //backToLobbyButtonHandler
        this.dispose();
        console.log("EXITED STREAM PREVIEW");
    };

    p.dispose = function(){
        //onStartGame
        this._active = false;
        this.streamVideo.stop();
        this.currentStream = "";
        this.frame.visible = true;
        this.show(false);
        console.log("DISPOSED STREAM PREVIEW");
    };

    p.tryToSwitchStream = function(machine) {
        if(this.currentStream !== machine.machineInfo.machineIndex){// if !machine.selected?
            alteastream.MachinesComponent.getInstance().clearActiveMachines();
            alteastream.SocketCommunicatorLobby.getInstance().lobbySwitchSubscriptions(this.currentStream,machine.machineInfo.machineIndex);
            alteastream.QueuePanel.getInstance().setCurrentMachine(machine.machineInfo);

            machine.selectMachine(true);
            // stream preview roulette
            if(alteastream.Lobby.getInstance().selectedLocation.casinoName === "Roulette"){
                this.switchStream(machine.machineInfo);
                var obj = {
                    previewCamera:machine.machineInfo.previewCamera,
                    shopMachine:alteastream.AbstractScene.SHOP_MACHINE,
                    machineName:machine.machineInfo.machineName,
                    casinoName:alteastream.Lobby.getInstance().selectedLocation.casinoName
                }

                var objToStore = JSON.stringify(obj);
                window.localStorage.setItem("streamPreviewParams", objToStore);
            }// stream preview roulette
            else{
                if(!machine.isFreeToPlay){
                    this.switchStream(machine.machineInfo);
                    this.changeStream(machine.machineInfo);
                    this.changeFrameImage(machine.thumb);
                }else{
                    this.updateInfo(machine.machineInfo);
                }
            }
        }
    };

    p.switchStream = function(machineInfo){
        this.currentStream = machineInfo.machineIndex;
        this.currentMachineName = machineInfo.machineName;
        this.updateInfo(machineInfo);
        this.start();
    };
    
    p.changeStream = function(machineInfo){
        this.show(true);
        this.frame.visible = true;
        this.streamVideo.spinner.setLabel("Connecting to machine "+scene.currentMachineName);
        this.streamVideo.spinner.runPreload(true);

        this.streamVideo.setPlayer(machineInfo);
        this.streamVideo.activate();
    };

    p.changeFrameImage = function(thumb) {
        if(this.frame.visible){
            gamecore.Utils._removeAllFilters([this.frame]);
            this.frame.image = thumb.hasThumbImage === true ? thumb.image : alteastream.Assets.getImage(alteastream.Assets.images.thumb).image;
            gamecore.Utils.DISPLAY.FILTERS.setBlur([this.frame], 2);
        }
    };

    p.updateInfo = function(machineInfo){
        this.machineInfo = machineInfo;
        alteastream.AbstractScene.GAME_CODE = machineInfo.gameCode;
        alteastream.AbstractScene.DEFAULT_GAME_ID = machineInfo.defaultGameId;
    };

    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.labelsContainer.addChild(textInstance);
        textInstance.mouseEnabled = false;
    };

    p.adjustMobile = function(){
        var footer = new createjs.Shape();
        footer.graphics.beginFill("#000000").drawRoundRect(0, 0, 616, 40, 3);
        footer.cache(0, 0, 616, 40);
        footer.alpha = 0.3;
        footer.x = 319;
        footer.y = 429;

        this.addChildAt(footer, 0);

        this.frame.x = -10;// location machines x = 10
        this.frame.y = -10;// location machines y = 10
        this.frame.scaleX = 2;
        this.frame.scaleY = 2;

        if(this.spinner)
            this.removeChild(this.spinner);

        var loadAnimation = new alteastream.SSAnimator("animatedLogo",125,122,19);
        var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        this.addChild(spinner);
        spinner.x = 470;
        spinner.y = 160;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;

        //this.removeChild(this.frame);//todo proveriti
    }

    p._local_activate = function(){
        this.currentStream = 333333333;
        this.start();

        this.frame.visible = true;
        setTimeout(function(){
            _this.frame.visible = false;
            console.log("StreamLoaded::::");
        },1000);
    };

    alteastream.StreamPreviewTest = createjs.promote(StreamPreviewTest,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSMap = function(lobby) {
        this.Container_constructor();
        this.initialize_ABSMap(lobby);
    };
    var p = createjs.extend(ABSMap, createjs.Container);
    var _this = null;
    // static properties:
    // events:
    // public properties:
    p.name = "";
    p.carousel = null;
    // private properties:
    // constructor:
    p.initialize_ABSMap = function(lobby) {
        _this = this;
        this.lobby = lobby;
        this.addAllListeners();
        this.mouseSubject = new alteastream.MouseSubject();

        this.selectedLocation = null;
    };
    // private methods
    p._onMouseAction = function(e){
        var location = e.target.parent;
        _this.mouseSubject.notifyObservers(e.type,location);

        switch(e.type){
            case "mouseover":
                if(location.allow === true)
                    location.highlight(true);
                break;
            case "mouseout":
                if(location.allow === true)
                    location.highlight(false);
                break;
            case "click":
                //if(location.allow === true)
                //location.highlight(true);
                break;
        }
    };


// static methods:
    // public methods:
    p.addObserver = function(observer){
        this.mouseSubject.subscribe(observer);
    };

    p.removeObserver = function(observer){
        this.mouseSubject.unsubscribe(observer);
    };

    p.addAllListeners = function(){
        this.addEventListener("mouseover",this._onMouseAction );
        this.addEventListener("mouseout",this._onMouseAction );
        this.addEventListener("click",this._onMouseAction );
    };

    p.removeAllListeners = function(){
        this.removeEventListener("mouseover",this._onMouseAction );
        this.removeEventListener("mouseout",this._onMouseAction );
        this.removeEventListener("click",this._onMouseAction );
    };

    p.focusOut = function(objectIn){ // NEW NO ANIMATION
        this.mouseEnabled = false;
        this.removeAllListeners();
        //this.highlightCountries();
        //this.carousel.hideUI(true);
        //this.carousel.showChosenHouse(false);
        //this.visible = false;
        objectIn.focusIn();
    };

    p.focusIn = function(){
        this.mouseEnabled = false; // todo mozda ne mora
        this.addAllListeners();
        this.mouseEnabled = true; // todo mozda ne mora
        //this.carousel.showChosenHouse(true);
        //this.highlightCountries();
        this.lobby.setSwipeTarget(this.lobby.currentMap.carousel,"x");
        this.lobby.setFocused(this);
    };

    p.clickEnabled = function(bool){
        /*        var b = this.getChildAt(0);
                b.mouseEnabled = bool;*/
        this.mouseEnabled = bool;
    };

    p.highlightCountries = function(bool){//not very good, change
        this.carousel.removeHighlight();
    };

    p.setCarousel = function(houses) {
        var carousel = this.carousel = new alteastream.HouseSelection(houses);
        carousel.x = 897;// ovde
        carousel.y = -450;
        this.addChild(carousel);
    };

    alteastream.ABSMap = createjs.promote(ABSMap,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var isMobile = window.localStorage.getItem('isMobile');

    //gameTypesNew
    var GameTypes = function() {};
    var assets = alteastream.Assets;

/*    GameTypes.Type_1 = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.ticketCircusType),
            "x": 0,
            "y": yPos
        };
    };*/

    GameTypes.Type_circus = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.ticketCircusType),
            "x": 0,
            "y": yPos
        };
    };

/*    GameTypes.Type_2 = function () {
        var yPos = isMobile === "yes"? 178:355;
        return {
            "decal": assets.getImage(assets.images.diamondsAndPearlsType),
            "x": 5,
            "y": yPos
        };
    };*/

    GameTypes.Type_pearlspush = function () {
        var yPos = isMobile === "yes"? 178:355;
        return {
            "decal": assets.getImage(assets.images.diamondsAndPearlsType),
            "x": 5,
            "y": yPos
        };
    };

/*    GameTypes.Type_3 = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.cashFestivalType),
            "x": 0,
            "y": yPos
        };
    };*/

/*    GameTypes.Type_12 = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.cashFestivalType),
            "x": 5,
            "y": yPos
        };
    };*/

    GameTypes.Type_roulette = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.cashFestivalType),
            "x": 5,
            "y": yPos
        };
    };

    alteastream.GameTypes = GameTypes;
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Location = function(location) {
        this.Container_constructor();
        this.initialize_Location(location);
    };
    var p = createjs.extend(Location, createjs.Container);
    var _this = null;
    // static properties:
    // events:
    // public properties:
    p.name = "";
    p.casinoName = "";
    p.gameClass = "";
    p.id = null;
    p.machines = null;
    p.allow = true;
    p.enabled = false;
    p.tentImage = null;
    p.category = null;
    // private properties:
    // constructor:
    p.initialize_Location = function(location) {
        _this = this;
        this.id = location.id;
        this.category = location.category;
        this.name = location.name;
        this.casinoName = location.name;
        this.gameClass = location.gameClass;
        //this.enabled = location.enabled;
        this.enabled = true;
        this._setView();
    };
    // private methods:
    p._setView = function(){
        var tent = this.tentImage = alteastream.Assets.getImage(alteastream.Assets.images.house);
        tent.regX = tent.image.width*0.5;
        tent.regY = tent.image.height*0.5;
        var font = "18px Roboto";
        var locationText = new createjs.Text(this.casinoName,font,"#ffffff").set({x:20 - tent.image.width*0.5, y:0, textAlign:"left", textBaseline:"middle", mouseEnabled:false});

        //gameTypesNew
/*        var gameTypeSkin = alteastream.GameTypes["Type_"+this.gameClass]();
        gameTypeSkin.decal.regX = gameTypeSkin.decal.image.width*0.5;
        gameTypeSkin.decal.regY = gameTypeSkin.decal.image.height;
        gameTypeSkin.decal.x = gameTypeSkin.x;
        gameTypeSkin.decal.y = gameTypeSkin.y;*/
        this.addChild(tent,locationText/*,gameTypeSkin.decal*/);

        //gameTypesNew remove line
        //this.addChild(tent,locationText);

        this.clickEnabled(this.enabled);
    };
    // static methods:
    // public methods:
    p.clickEnabled = function(bool){
        this.mouseEnabled = bool;
    };

    p.highlight = function(bool){
        var imgId = bool === true ? "houseHighlight" : "house";
        this.tentImage.image = alteastream.Assets.getImageURI(alteastream.Assets.images[imgId]);
        //this.gameTypeSkin.decal.image = bool === true? this.gameTypeSkin.decalHighlight.image:this.gameTypeSkin.decal.image;
    };

    alteastream.Location = createjs.promote(Location,"Container");
})();
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LocationFactory = function() {};
    var assets = alteastream.Assets;

    LocationFactory.Germany = function () {
        return {
            "skin": assets.getImage(assets.images.germany),
            "flag": assets.getImage(assets.images.germanyFlag),
            "xPos": 900,
            "yPos": -100
        };
    };

    LocationFactory.Austria = function () {
        return {
            "skin": assets.getImage(assets.images.austria),
            "flag": assets.getImage(assets.images.austriaFlag),
            "xPos": 1200,
            "yPos": -100
        };
    };

    LocationFactory.Italy = function () {
        return {
            "skin": assets.getImage(assets.images.italy),
            "flag": assets.getImage(assets.images.italyFlag),
            "xPos": 286,
            "yPos": 514
        };
    };

    LocationFactory.Poland = function () {
        return {
            "skin": assets.getImage(assets.images.poland),
            "flag": assets.getImage(assets.images.polandFlag),
            "xPos": 363,
            "yPos": 360
        };
    };

    LocationFactory.Serbia = function () {
        return {
            "skin": assets.getImage(assets.images.serbia),
            "flag": assets.getImage(assets.images.serbiaFlag),
            "xPos": 321,
            "yPos": -50
        };
    };

    LocationFactory.Belgium = function () {
        return {
            "skin": assets.getImage(assets.images.belgium),
            "flag": assets.getImage(assets.images.belgiumFlag),
            "xPos": 1470,
            "yPos": -50
        };
    };

    alteastream.LocationFactory = LocationFactory;
})();
// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var Swiper = function (){
        this.initialize_Swiper();
    };

    var p = Swiper.prototype;
    var _this = null;
    // static properties:
    // public vars:
    // private vars:
    // private properties
    p._target = null;
    p._direction = null;
    // constructor:
    p.initialize_Swiper = function (){
        _this = this;
        createjs.Touch.enable(stage);
    };
    // static methods:
    // public methods:
    p.activate = function(bool){
        var addRemoveListener = bool ===true?"addEventListener":"removeEventListener";
        stage[addRemoveListener]('mousedown', this._onDown);
        stage[addRemoveListener]('stagemouseup', this._onUp);
    };

    p.setTargetAndDirection = function(obj,direction){
        this._target = obj;
        this._direction = direction;
    };

    // private methods:
    p._onDown = function(event){
        _this.prevInteraction = new Date().getTime();
        _this.prevStageX = event.stageX;
        _this.prevStageY = event.stageY;
        stage.addEventListener('pressmove', _this._onMove);
    };

    p._onMove = function(event){
        var dx = _this._direction === "x"?event.stageX - _this.prevStageX:0;
        var dy = _this._direction === "y"?event.stageY - _this.prevStageY:0;
        var distance = Math.sqrt(dx*dx+dy*dy);
        _this.isDown = true;
        if(distance >= 10){
            var deltaTime = new Date().getTime() - _this.prevInteraction;
            var speed = distance / deltaTime;
            if (speed>0.3){
                stage.removeEventListener('pressmove', _this._onMove);
                var dist = _this._direction === "x"?dx:dy;
                _this._swipe(dist>0?1:-1);
            }
        }
    };

    p._onUp = function(event){
        if(_this.isDown){
            stage.removeEventListener('pressmove', _this._onMove);
            _this.isDown=false;
        }
    };

    p._swipe = function(direction){
        this._target.onSwipe(direction);
    };

    alteastream.Swiper = Swiper;
})();

