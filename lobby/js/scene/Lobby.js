

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Lobby = function() {
        this.AbstractScene_constructor();
        this.initialize_Lobby();
    };
    var p = createjs.extend(Lobby, alteastream.AbstractScene);

    // static properties:
    // events:
    // public properties:
    p.selectedLocation = null;
    p.currentMap = null;
    p.currentLocationMachines = null;
    p.beams = null;
    p.btnFs = null;
    p.btnSound = null;
    p.btnInfo = null;
    p.btnQuit = null;
    p.currency = null;
    p.mouseWheel = null;
    p.balanceBackground = null;
    // private properties:
    p._currentFocused = null;
    p._housesArr = null;
    p._info = null;
    p._logo = null;
    p._playerActivityPanel = null;
    var _instance = null;

    // constructor:
    p.initialize_Lobby = function() {
        console.log("LOBBY INIT");
        _instance = this;

        this._local_activate();// local ver

        this.monitorNetworkStatus();
        this.setCommunicator();

    };
    // static methods:
    Lobby.getInstance = function(){
        return _instance;
    };

    // private methods
    p._setBalance = function() {
        var bgShape = this.balanceBackground = new createjs.Shape();
        bgShape.graphics.beginFill("#000000").drawRoundRect(0, 0, 233, 90, 5);
        bgShape.cache(0, 0, 233, 90);
        bgShape.x = 120;
        bgShape.y = 976;
        bgShape.mouseEnabled = false;
        bgShape.alpha = 0.7;
        bgShape.visible = false;

        var _startCurrency = "--";
        var lobbyBalanceLabel = this.lobbyBalanceLabel = new createjs.Text("BALANCE ( "+_startCurrency+" ) ","15px HurmeGeometricSans3","#a39f9e").set({textAlign:"center", textBaseline:"middle", mouseEnabled:false, x:240, y:1010, visible:true});
        var lobbyBalanceAmount = this.lobbyBalanceAmount = new createjs.Text("0.00","bold 24px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", mouseEnabled:false, x:lobbyBalanceLabel.x, y:lobbyBalanceLabel.y+23, visible:true});//y:820
        this.addChild(bgShape, lobbyBalanceLabel, lobbyBalanceAmount);
    };

    p._setComponents = function(response){
        var mainBackground = this.mainBackground = alteastream.Assets.getImage(alteastream.Assets.images.mainBackground);

        var backgroundOverlay = this.mainBackgroundOverlay = alteastream.Assets.getImage(alteastream.Assets.images.mainBackgroundOverlay);
        backgroundOverlay.mouseEnabled = false;

        var backgroundOverlay2 = this.mainBackgroundOverlay2 = alteastream.Assets.getImage(alteastream.Assets.images.mainBackgroundOverlay2);
        backgroundOverlay2.mouseEnabled = false;
        backgroundOverlay2.y = alteastream.AbstractScene.GAME_HEIGHT - backgroundOverlay2.image.height;

        var logo = this._logo = alteastream.Assets.getImage(alteastream.Assets.images.mainLogo);
        logo.mouseEnabled = false;
        logo.x = (alteastream.AbstractScene.GAME_WIDTH/2) - (logo.image.width/2);
        logo.y = -8;

        var currentMap = this.currentMap = new alteastream.ABSMap(this);//TEMP ALL

        currentMap.x =64;
        currentMap.y = 600;
        currentMap.name = "circus";
        currentMap.visible = alteastream.AbstractScene.BACK_TO_HOUSE === -1;

        this.setFocused(currentMap);

        if (Object.keys(response).length === 0 && response.constructor === Object) {//TEMP CHECK
            this.addChild(mainBackground, currentMap, logo, this.currentLocationMachines);
            this.throwAlert(alteastream.Alert.ERROR,"No Shops detected",function(){
                _instance.onQuit();
            });
            return;
        } else {
            this.setMap(response);
            this.setCurrentLocationMachines();
            this.addChild(mainBackground, currentMap, this.currentLocationMachines);
        }

        currentMap.addObserver(new alteastream.MouseObserver(this,"clickObserver","click"));

        this._setCarousel();
        this._setInfo();
        this._setPlayerActivity();
        this._setTopButtons();
        this._setEffects();
        this.setFSClock(alteastream.AbstractScene.GAME_WIDTH*0.5+460,36);
        this._setBalance();

        this.mouseWheel = new alteastream.MouseWheel();
        this.mouseWheel.setTarget(this.currentLocationMachines);
    };

    /*p._setPlayerActivity = function() {
    _instance._playerActivityPanel = new alteastream.PlayerActivityPanel();
    _instance.addChild(_instance._playerActivityPanel);
    _instance._playerActivityPanel.y-=30;
    _instance._playerActivityPanel.visible = false;
    _instance._playerActivityPanel.preventClickThrough(true);
};

p._getPlayerActivity = function(){
    // temp update!!!!
    _instance.requester.getPlayerActivity(function (response) {
        _instance._playerActivityPanel.setPanel(response);
    });
    var activityInterval = setInterval(function(){
        _instance.requester.getPlayerActivity(function (response) {
            _instance._playerActivityPanel.setPanel(response);
        });
    },5000);

    setTimeout(function(){
        clearInterval(activityInterval);
    },30000);
};*/

    p._setPlayerActivity = function() {
        _instance._playerActivityPanel = new alteastream.PlayerActivityPanel();
        _instance.addChild(_instance._playerActivityPanel);
        _instance._playerActivityPanel.y-=30;
        _instance._playerActivityPanel.visible = false;
        _instance._playerActivityPanel.preventClickThrough(true);
    };

    p._getPlayerActivity = function(){
        _instance.requester.getPlayerActivity(function (response) {
            if(response.length){
                _instance._playerActivityPanel.setPanel(response);
            }else{
                _instance._startPlayerActivityInterval();
            }
        });
    };

    p._startPlayerActivityInterval = function() {
        console.log("nema response, startuje interval");
        var attempt = 0;
        var activityInterval = setInterval(function(){
            _instance.requester.getPlayerActivity(function (response) {
                //console.log("pokusaj " + attempt);
                if(response.length){
                    //console.log("sad ima response");
                    _instance._playerActivityPanel.setPanel(response);
                    clearInterval(activityInterval);
                }else{
                    //console.log("nema response");
                    attempt++;
                    if(attempt === 6){
                        clearInterval(activityInterval);
                        //_instance.throwAlert(alteastream.Alert.ERROR,"Activity log not available");
                    }
                }
            });
        },5000);
    };

    p._setEffects = function(){
        var beams = this.beams = new alteastream.LightBeams(this);
        beams.createBeam(0,-20,0,{r:255,g:0,b:0,a:1});
        beams.createBeam(30,-20,0,{r:255,g:255,b:0,a:1});

        beams.createBeam(1920,-20,1,{r:0,g:255,b:0,a:1});
        beams.createBeam(1950,-20,1,{r:255,g:0,b:255,a:1});
        beams.update();

        this.bulbsContainer = new createjs.Container();
        this.addChild(this.bulbsContainer);
        this.bulbsContainer.mouseEnabled = false;
        this.bulbsContainer.mouseChildren = false;
        var util = gamecore.Utils.NUMBER;
        for(var i=0,j=30;i<j;i++){
            var xp = util.randomRange(20,1900);
            var yp = util.randomRange(20,250);
            var bulbs = new alteastream.Bulbs(this.bulbsContainer);
            bulbs.createBulb(xp,yp,{r:255,g:255,b:255,a:1});
            bulbs.update();
        }
    };

    p._setCarousel = function() {
        this.currentLocationMachines._machinesComponent.addAcordions(this._housesArr);
    };

    p._setInfo = function() {
        var info = this._info = new alteastream.Info();
        info.visible = false;
        info.x = alteastream.AbstractScene.GAME_WIDTH/2;
        info.y = alteastream.AbstractScene.GAME_HEIGHT/2;
        this.addChild(info);
    };

    p._showInfoOnStart = function(bool){
        this._info.scaleX = this._info.scaleY = bool===true? 1:0;

        this.btnInfo.x = bool===true? 1764: this.btnInfo.originalXPosition;
        this.btnFs.visible = this.btnSound.visible = this.btnQuit.visible = this.btnPlayerActivity.visible = !bool;
    };

    p._setTopButtons = function() {
        var assets = alteastream.Assets;
        var yPos = 10;

        var btnQuit = this.btnQuit = new alteastream.ImageButton(assets.getImageURI(assets.images.btnQuitSS),3);
        var width = btnQuit.width;
        var spacing = 2;

        btnQuit.x = alteastream.AbstractScene.GAME_WIDTH - 164;
        btnQuit.y = yPos;
        btnQuit.setClickHandler(_instance.btnQuitHandler);
        this.addChild(btnQuit);

        var btnInfo = this.btnInfo =  new alteastream.ImageButton(assets.getImageURI(assets.images.btnInfoSS),3);
        btnInfo.x = btnQuit.x - width - spacing;
        btnInfo.y = yPos;
        btnInfo.originalXPosition = btnInfo.x;
        btnInfo.setClickHandler(_instance.btnInfoHandler);
        this.addChild(btnInfo);

        var btnPlayerActivity = this.btnPlayerActivity = alteastream.Assets.getImage(assets.images.btnPlayerActivity);
        btnPlayerActivity.x = btnInfo.x - width - spacing;
        btnPlayerActivity.y = yPos;
        btnPlayerActivity.originalXPosition = btnPlayerActivity.x;
        btnPlayerActivity.addEventListener("click", _instance.btnPlayerActivityHandler);
        this.addChild(btnPlayerActivity);

        var btnSound = this.btnSound = alteastream.Assets.getImage(assets.images.soundOn);
        btnSound.x = btnPlayerActivity.x - width - spacing;
        btnSound.y = yPos;
        btnSound.addEventListener("click", _instance.btnSoundHandler);
        this.addChild(btnSound);

        var btnFs = this.btnFs = alteastream.Assets.getImage(assets.images.btnFullScreen_On);
        btnFs.x = btnSound.x - width - spacing;
        btnFs.y = yPos;

        var method;
        if(is.safari() === true || is.edge() === true){
            method = "otherBrowsersFullScreenToggle";
        }else{
            method = "normalBrowsersFullScreenToggle";
        }
        btnFs.addEventListener("click", this[method]);
        this.addChild(btnFs);
    };

    p._local_activate = function() {
        this.startCommunication = function(communicator){
            _instance.globalMouseEnabled(false);
            //communicator.getHouses(function(response) {// todo getGameTypes??
            //communicator.getGameTypes(function(response) {// todo getGameTypes??
            communicator.getGameCategories(function(response) {// todo getGameTypes??
                _instance.globalMouseEnabled(true);
                console.log("HOUSES:: " + response);
                _instance._setComponents(response);
                _instance.defineInfoOnStart();
                _instance.socketCommunicator = new alteastream.SocketCommunicatorLobby();
                setTimeout(function () {_instance.onSocketConnect();}, 500);
            });
        };

        this.onSocketConnect = function(frame){
            _instance.setRenderer();
            _instance.getBalanceAndActivity();
            if(alteastream.AbstractScene.BACK_TO_HOUSE !== -1){
                var location = _instance.currentMap.carousel.getLocation(alteastream.AbstractScene.BACK_TO_HOUSE);
                _instance.enterShop(location);
            }
        };
    };

    // public methods:
    p.resetBackgroundMusic = function() {
        window.parent.startHelperSound("lobbyBgMusic",{loop:-1,volume:0.2});
    };

    p.setCommunicator = function(){
        this.requester = new alteastream.Requester(this);
        this.startCommunication(this.requester);
    };

    p.startCommunication = function(communicator){
        this.globalMouseEnabled(false);
        //communicator.getHouses(function(response) {
        communicator.getGameCategories(function(response) {
                _instance.globalMouseEnabled(true);
                console.log("HOUSES:: " + response);
                _instance._setComponents(response);
                _instance.defineInfoOnStart();
                _instance.setSocketCommunication();// local ver
                //new socket
                 /* // local ver
                _instance.setSocketCommunication("Lobby");
                _instance.socketCommunicator.activate();
                */ // local ver
        });
    };

    p.onSocketConnect = function(frame){
            _instance.setRenderer();
            _instance.socketCommunicator.onLobbyConnect(frame);
            _instance.getBalanceAndActivity();
            if(alteastream.AbstractScene.BACK_TO_HOUSE !== -1){
                var backgroundMusicIsMuted = window.localStorage.getItem('backgroundMusicIsMuted');
                createjs.Sound.muted = backgroundMusicIsMuted === "false";
                _instance.btnSoundHandler();
                var location  = _instance.currentLocationMachines._machinesComponent.getAcordionByName(alteastream.AbstractScene.BACK_TO_HOUSE);
                _instance.currentLocationMachines._machinesComponent.acordionClickHandler(location);
            }
    };

    p.getName = function(suf){
        return Lobby["LOCATION_"+ suf];
    };

    p.setFocused = function(object){
        this._currentFocused = object;
    };

    p.getFocused = function(){
        return this._currentFocused;
    };

    p.switchFocusIn = function(objectIn){
        this.getFocused().focusOut(objectIn);
    };

    p.clickObserver = function(location){
        if(location.allow === true){
            this.enterShop(location);
        }
    };

    p.enterShop = function(location){
        this.checkIfHiddenMenuIsShown();
        location.mouseEnabled = false;
        this.globalMouseEnabled(false);
        //this.requester.getMachines(location.id,function(response){
        //this.requester.getMachinesGt(location.id,function(response){
        var method = "getMachinesBy" + location.category;
        //this.requester.getMachinesByCategory(location.name,function(response){
        this.requester[method](location.name,function(response){
            if(Object.keys(response).length === 0 && response.constructor === Object){//temp, on Exception
                _instance.playSound("errorSpoken");
                _instance.throwAlert(alteastream.Alert.ERROR,"No machines available",function(){location.mouseEnabled = true; _instance.globalMouseEnabled(true);});
            }else{
                //window.parent.switchSource("locationSoundBg");
                var musicAction = createjs.Sound.muted === true ? "pause" : "play";
                window.parent.manageBgMusic(musicAction);
                location.mouseEnabled = true;
                _instance.globalMouseEnabled(true);
                _instance.selectedLocation = location;// tent container // todo mozda ovo iskoristiti kao selected acordion
                _instance.currentLocationMachines._machinesComponent.selectedAcordion = location;// tent container
                alteastream.AbstractScene.BACK_TO_HOUSE = location.name;
                _instance._logo.visible = false;
                _instance.currentLocationMachines.streamPreview.activate();
                _instance.currentLocationMachines.populateMachines(response);
                _instance.switchFocusIn(_instance.currentLocationMachines);
                _instance.mouseWheel.enable(true);
                _instance.currentLocationMachines._resetScrollComponent(false);
            }
        });
    };

    p.backToLocationsFromLobby = function() {
        this.switchFocusIn(this.currentMap);
        this.currentMap.visible = true;

        this._logo.visible = true;
        this.beams.showBeams(true);
        //window.parent.switchSource("bgMusic");
        //var musicAction = createjs.Sound.muted === true ? "pause" : "play";
        //window.parent.manageBgMusic(musicAction);
    };

    p.hideBackgroundsForFullScreenVideo = function(hide) {
        this.mainBackground.visible =
            this.mainBackgroundOverlay.visible =
                this.mainBackgroundOverlay2.visible = !hide;
    };

    p.defineInfoOnStart = function(){
        this._showInfoOnStart(false);
    };

    p.getBalanceAndActivity = function(){
        alteastream.Requester.getInstance().getBalance(function (resp) {//9471.55 treba  a sad je 9471.55
            console.log(resp);

            var allowSpectate = true;
            if(resp.hasOwnProperty("allow_spectate")){
                allowSpectate = resp.allow_spectate;
            }
            alteastream.AbstractScene.HAS_COPLAY = allowSpectate;
            //decimal
            //var code = 8364;//resp.currency
            //var currency = _instance.currency = String.fromCharCode( unicode);

            //hex, css
            //var code = '20AC';//20A4 resp.currency
            //var currency = _instance.currency = String.fromCharCode( parseInt(code, 16) );

            //_instance.lobbyBalanceLabel.text = "BALANCE ( "+currency+" ) ";

            //novi poziv::
            //{"balance":870910,"currency":"EUR","ingame":false}
            _instance.currentLocationMachines.queuePanel.previousMachineActive = resp.ingame;
            _instance.currency = resp.currency;
            _instance.lobbyBalanceLabel.text = "BALANCE ( "+_instance.currency+" ) ";
            _instance.updateLobbyBalance(resp.balance);

            _instance.currentLocationMachines._machinesComponent.setCurrency(_instance.currency);

            //set hasCoPlay here from some param
            if(alteastream.AbstractScene.HAS_COPLAY){
                _instance.currentLocationMachines.queuePanel.addMultiplayerControls(resp.participateCounter);
                //_instance.currentLocationMachines.queuePanel.multiPlayerControls.setBettingCounterTime(resp.participateCounter);
            }

            _instance._getPlayerActivity();
        });
    };

    p.updateLobbyBalance = function(val){
        var crdAmountCents = Math.min(val/1000);//1718900
        crdAmountCents = crdAmountCents.toFixed(2);
        this.lobbyBalanceAmount.text = crdAmountCents;
    };

    p.setOverlay = function() {
        var isMob = window.localStorage.getItem('isMobile') === "yes";
        var background = new createjs.Bitmap(queue.getResult("back"));
        var logo = new createjs.Bitmap(queue.getResult("logo"));
        var buttonProps = isMob ? {w:86,h:28,round:3,f:"12px Arial"} : {w:170,h:56,round:6,f:"24px Arial"};

        var button = new createjs.Shape();
        button.graphics.setStrokeStyle(2);
        button.graphics.beginStroke("#ffffff").beginFill('#000000').drawRoundRect(0,0,buttonProps.w,buttonProps.h,buttonProps.round);
        button.regX = buttonProps.w/2;
        button.regY = buttonProps.h/2;
        button.x = alteastream.AbstractScene.GAME_WIDTH * 0.5;
        button.y = (alteastream.AbstractScene.GAME_HEIGHT* 0.5) + (buttonProps.h * 2);

        var txt = new createjs.Text("PLAY NOW", buttonProps.f, "#ffffff");
        txt.textAlign = 'center';
        txt.textBaseline = 'middle';
        txt.x = button.x;
        txt.y = button.y;

        var overlayClickCalback = function () {
            _instance.resetBackgroundMusic();
            if(isMob){
                if(is.safari()){
                    var isiPadOS = navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1;
                    if(is.not.ipad()) {
                        if(!isiPadOS){
                            alteastream.ABSStarter.getInstance().safariResize();
                        }
                    }
                }
            }
        };

        var overlayOptions = {
            background:background,
            graphics:[logo],
            texts:[txt],
            button:button,
            clickCallback:overlayClickCalback
        }

        this.setStartOverlay(overlayOptions);
    };

    p.setBackgroundImagesForStreamPreview = function(set) {
        var img = set === true ? "mainBackgroundWithHole" : "mainBackground";
        this.mainBackground.image = alteastream.Assets.getImage(alteastream.Assets.images[img]).image;
        this.mainBackgroundOverlay.visible = !set;
        this.mainBackgroundOverlay2.visible = !set;
    };

    p.setMap = function(locations){
        this._housesArr = [];

        for(var i = 0; i < locations.stake_categories.length; i++){
            if(locations.stake_categories[j] !== null){
                if(locations.stake_categories[i].length > 2){
                    var stake = {
                        name:locations.stake_categories[i],
                        category:"Stake"
                    };
                    this._housesArr.push(stake);
                }
            }
        }

        for(var j = 0; j < locations.payout_categories.length; j++){
            if(locations.payout_categories[j] !== null){
                if(locations.payout_categories[j].length > 2){
                    var payout = {
                        name:locations.payout_categories[j],
                        category:"Payout"
                    };
                    this._housesArr.push(payout);
                }
            }
        }
    };

    p.setCurrentLocationMachines = function() {
        var currentLocationMachines = this.currentLocationMachines = new alteastream.LocationMachines(this);
        currentLocationMachines.x = 100;
        currentLocationMachines.y = 121;
    };

    p.btnQuitHandler = function() {
        var handler = _instance._info.visible === true ? "btnInfoHandler" : "_setQuitPanel";
        _instance[handler]();
        //_instance._setQuitPanel();
    };

    p.btnPlayerActivityHandler = function() {
        if(_instance._info.visible === true){
            _instance.hideInfo();
        }
        _instance._playerActivityPanel.visible = !_instance._playerActivityPanel.visible;
        if(_instance._playerActivityPanel.visible === false){
            _instance._playerActivityPanel.setToFirstPage(false);
            _instance.mouseWheel.setTarget(_instance.currentLocationMachines);
            if(_instance._currentFocused.name === "circus"){
                _instance.mouseWheel.enable(false);
            }
        }else{
            _instance.mouseWheel.setTarget(_instance._playerActivityPanel);
            if(_instance.mouseWheel._enabled === false){
                _instance.mouseWheel.enable(true);
            }
        }
        _instance._playerActivityPanel.activateSupportLinkListeners(_instance._playerActivityPanel.visible);
    };

    p.btnInfoHandler = function(){
        if(_instance._playerActivityPanel.visible === true){
            _instance.hidePlayerActivity();
        }
        _instance._info.visible = true;
        _instance.btnInfo.mouseEnabled = false;
        var scale = _instance._info.scaleX === 1 ? 0 : 1;
        if(scale === 1){_instance._info.resetCurrentPage();}
        _instance._info.preventClickThrough(scale === 1);
        createjs.Tween.get(_instance._info).to({scaleX:scale,scaleY:scale},300,createjs.Ease.quadInOut).call(function () {
            _instance.btnInfo.mouseEnabled = true;
            if(scale === 0){_instance._info.visible = false;}
        });
    };

    p.hidePlayerActivity = function() {
        this._playerActivityPanel.visible = false;
        this._playerActivityPanel.setToFirstPage(false);
        this.mouseWheel.setTarget(this.currentLocationMachines);
        if(_instance._currentFocused.name === "circus"){
            _instance.mouseWheel.enable(false);
        }
    };

    p.hideInfo = function() {
        this._info.visible = false;
        this._info.scale = 0;
        this._info.resetCurrentPage();
        this.btnInfo.mouseEnabled = true;
    };

    p.btnSoundHandler = function() {
        var assets = alteastream.Assets;
        createjs.Sound.muted = !createjs.Sound.muted;
        _instance.btnSound.image = createjs.Sound.muted === true ? assets.getImage(assets.images.soundOff).image : assets.getImage(assets.images.soundOn).image;
        var action = createjs.Sound.muted === true ? "pause":"play";
        window.parent.manageBgMusic(action);
    };

    p.quitConfirmed = function(){
        //_instance.currentLocationMachines.queuePanel.leaveQueue(_instance.onQuit);
        //_instance.onQuit();
        if(_instance.currentLocationMachines.queuePanel.yourPositionInQueue > 0){
            _instance.currentLocationMachines.queuePanel.setQueueLeaveCallback(_instance.onQuit);
            alteastream.SocketCommunicatorLobby.getInstance().leaveQueue();
        }else{
            _instance.onQuit();
        }
    };

    p.onQuit = function(){
        if(_instance.socketCommunicator)
            _instance.socketCommunicator.disposeCommunication();
        _instance.killGame();
        var quitUrl = alteastream.AbstractScene.GAME_QUIT === "" ? alteastream.AbstractScene.GAME_PROTOCOL+alteastream.AbstractScene.SERVER_IP + "/demo" : alteastream.AbstractScene.GAME_QUIT;
        quitUrl += "/";
        window.parent.location.href = decodeURIComponent(quitUrl);
    };

    p.onStartGameStream = function(){
        //_instance.currentMap.carousel.storeLocationPosition();
        //new socket comment heartbeatGame:
        //_instance.socketCommunicator.heartbeatGame();

        _instance.socketCommunicator.heartbeatUser();
    };

    p.enableSwiper = function(enable){};
    p.setSwipeTarget = function(target,axis){};
    p.checkIfHiddenMenuIsShown = function(){};

    alteastream.Lobby = createjs.promote(Lobby,"AbstractScene");
})();