
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var Info = function(){
        this.Container_constructor();
        this._initInfo();
    };

    var p = createjs.extend(Info,createjs.Container);
    // private properties
    p._currentPage = 0;
    p._pages = null;
    // constructor
    p._initInfo = function () {
        var _this = this;
        this._pages = [];

        var background = alteastream.Assets.getImage(alteastream.Assets.images.infoBackground);

        var left = this._leftButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.infoButtonLeft),3);
        left.x = 1665;
        left.y = 165;
        left.setClickHandler(function () {
            if(_this._currentPage > 0){
                _this._currentPage--;
                _this._buttonsHandler();
            }
        });
        left.setDisabled(true);

        var right = this._rightButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.infoButtonRight),3);
        right.x = 1750;
        right.y = 165;
        right.setClickHandler(function () {
            if(_this._currentPage < _this._pages.length - 1){
                _this._currentPage++;
                _this._buttonsHandler();
            }
        });

        var fontHeader = "20px HurmeGeometricSans3";
        var fontHeader2 = "18px HurmeGeometricSans3";
        var fontText = "16px HurmeGeometricSans3";

        var page1 = new createjs.Container();

        var welcomeText = new createjs.Text("Welcome" , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:111, y:100, mouseEnabled:false});

        var headerShape = new createjs.Shape();
        headerShape.graphics.beginFill("#d60168").drawRect(111, 164, 182, 45);
        headerShape.cache(111, 164, 182, 45);

        var textSectionBackground = new createjs.Shape();
        textSectionBackground.graphics.beginFill("#000000").drawRect(1071, 270, 733, 540);
        textSectionBackground.alpha = 0.6;
        textSectionBackground.cache(1071, 270, 733, 540);
        
        this.addChild(background,welcomeText,headerShape,textSectionBackground,left,right);

        var page1Image = alteastream.Assets.getImage(alteastream.Assets.images.infoPage1Image);
        page1Image.x = 111;
        page1Image.y = 270;

        var page1Header = new createjs.Text("Lobby Section" , fontHeader, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:135, y:188, mouseEnabled:false});
        var page1Header2 = new createjs.Text("Lobby Section" , fontHeader2, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1166, y:330, mouseEnabled:false});

        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText1);
        var lobbySectionDescription = new createjs.Text(textSrc , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1166, y:375, mouseEnabled:false, lineWidth:630});

        var lampRed = alteastream.Assets.getImage(alteastream.Assets.images.lampRed);
        lampRed.x = 1166;
        lampRed.y = 436;
        lampRed.scale = 1.2;

        var lampRedLabel = new createjs.Text("Unavailable machine" , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1208, y:lampRed.y+8, mouseEnabled:false});

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText2);
        var lampRedDescription = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left", textBaseline:"middle", x:1166, y:lampRed.y+54, mouseEnabled:false, lineWidth:630});

        var lampGreen = alteastream.Assets.getImage(alteastream.Assets.images.lampGreen);
        lampGreen.x = 1166;
        lampGreen.y = 548;
        lampGreen.scale = 1.2;

        var lampGreenLabel = new createjs.Text("Free machine" , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1208, y:lampGreen.y+8, mouseEnabled:false});

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText3);
        var lampGreenDescription = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left", textBaseline:"middle", x:1166, y:lampGreen.y+54, mouseEnabled:false, lineWidth:630});

        var lampYellow = alteastream.Assets.getImage(alteastream.Assets.images.lampYellow);
        lampYellow.x = 1166;
        lampYellow.y = 653;
        lampYellow.scale = 1.2;

        var lampYellowLabel = new createjs.Text("Occupied machine" , fontText, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1208, y:lampYellow.y+8, mouseEnabled:false});

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText4);
        var lampYellowDescription = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left", textBaseline:"middle", x:1166, y:lampYellow.y+54, mouseEnabled:false, lineWidth:630,lineHeight:28});

        page1.addChild(page1Image,page1Header,page1Header2,lobbySectionDescription,lampRed,lampRedLabel,lampRedDescription,lampGreen,lampGreenLabel,lampGreenDescription,lampYellow,lampYellowLabel,lampYellowDescription);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var page2 = new createjs.Container();

        var page2Image = alteastream.Assets.getImage(alteastream.Assets.images.infoPage2Image);
        page2Image.x = 111;
        page2Image.y = 270;

        var page2Header = new createjs.Text("Co-Play" , fontHeader, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:165, y:188, mouseEnabled:false});
        var page2Header2 = new createjs.Text("Co-Play" , fontHeader2, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1166, y:365, mouseEnabled:false});//y:360 original position

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText5);
        var page2Text = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left", textBaseline:"middle", x:1166, y:410, mouseEnabled:false, lineWidth:630,lineHeight:28});//y:405 original position

        page2.addChild(page2Image,page2Header,page2Header2,page2Text);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var page3 = new createjs.Container();

        var page3Image = alteastream.Assets.getImage(alteastream.Assets.images.infoPage3Image);
        page3Image.x = 111;
        page3Image.y = 270;

        var page3Header = new createjs.Text("Game Entrance" , fontHeader, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:132, y:188, mouseEnabled:false});
        var page3Header2 = new createjs.Text("Game Entrance" , fontHeader2, "#ffffff").set({textAlign:"left", textBaseline:"middle", x:1166, y:450, mouseEnabled:false});

        textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText6);
        var page3Text = new createjs.Text(textSrc , fontText, "#afafaf").set({textAlign:"left",textBaseline:"middle", x:1166, y:510, mouseEnabled:false, lineWidth:630,lineHeight:28});

        page3.addChild(page3Image,page3Header,page3Header2,page3Text);

        this._pages = [page1,page2,page3];
        this.addChild(page1,page2,page3);

        page2.visible = false;
        page3.visible = false;

        this.regX = background.image.width/2;
        this.regY = background.image.height/2;

        this.preventClickThrough(true);
    };
    // private methods
    p._buttonsHandler = function(){
        this._leftButton.setDisabled(this._currentPage < 1);
        this._rightButton.setDisabled(this._currentPage === this._pages.length - 1);
        this._managePagesVisibility();
    };

    p._managePagesVisibility = function(){
        for(var i = 0; i < this._pages.length; i++){
            this._pages[i].visible = i === this._currentPage;
        }
    };
    // public methods
    p.resetCurrentPage = function(){
        this._currentPage = 0;
        this._buttonsHandler();
    };

    p.preventClickThrough = function(bool){
        var method = bool === true? "on":"off";
        this[method]("click",function noClick(e){});
    };


    p.adjustMobile = function(){
        var fontHeader = "16px Roboto";
        var fontText = "16px Roboto";

        this._leftButton.x = 780;
        this._leftButton.y = 415;

        this._rightButton.x = 855;
        this._rightButton.y = 415;

        var welcomeTxt = this.getChildAt(1);
        welcomeTxt.font = fontHeader;
        welcomeTxt.x = 55;
        welcomeTxt.y = 50;

        var headerShape = this.getChildAt(2);
        headerShape.uncache();
        headerShape.graphics.clear();
        headerShape.graphics.beginFill("#d60168").drawRect(55, 82, 122, 26);
        headerShape.cache(55, 82, 122, 26);

        var textSectionBackground = this.getChildAt(3);
        textSectionBackground.uncache();
        textSectionBackground.graphics.clear();
        textSectionBackground.graphics.beginFill("#000000").drawRect(535, 135, 366, 270);
        textSectionBackground.cache(535, 135, 366, 270);

        var page1 = this._pages[0];

        var page1Image = page1.getChildAt(0);
        page1Image.x = 55;
        page1Image.y = 135;

        var page1Header = page1.getChildAt(1);
        page1Header.font = fontHeader;
        page1Header.x = 65;
        page1Header.y = 96;

        var xCorrection = 15;
        var lobbySectionDescription = page1.getChildAt(3);
        lobbySectionDescription.font = fontText;
        lobbySectionDescription.x = 583 - xCorrection;
        lobbySectionDescription.y = 157;
        lobbySectionDescription.lineWidth = 315;

        var lampRed = page1.getChildAt(4);
        lampRed.scale = 0.6;
        lampRed.x = 583 - xCorrection;
        lampRed.y = 208;

        var lampRedLabel = page1.getChildAt(5);
        lampRedLabel.x = 604 - xCorrection;
        lampRedLabel.y = 217;
        lampRedLabel.font = fontText;

        var lampRedDescription = page1.getChildAt(6);
        lampRedDescription.x = 583 - xCorrection;
        lampRedDescription.y = 236;
        lampRedDescription.font = fontText;
        lampRedDescription.color = "#afafaf";

        var lampGreen = page1.getChildAt(7);
        lampGreen.scale = 0.6;
        lampGreen.x = 583 - xCorrection;
        lampGreen.y = 264;

        var lampGreenLabel = page1.getChildAt(8);
        lampGreenLabel.x = 604 - xCorrection;
        lampGreenLabel.y = 272;
        lampGreenLabel.font = fontText;

        var lampGreenDescription = page1.getChildAt(9);
        lampGreenDescription.x = 583 - xCorrection;
        lampGreenDescription.y = 291;
        lampGreenDescription.font = fontText;
        lampGreenDescription.color = "#afafaf";
        lampGreenDescription.lineHeight = 16;

        var lampYellow = page1.getChildAt(10);
        lampYellow.scale = 0.6;
        lampYellow.x = 583 - xCorrection;
        lampYellow.y = 316;

        var lampYellowLabel = page1.getChildAt(11);
        lampYellowLabel.x = 604 - xCorrection;
        lampYellowLabel.y = 325;
        lampYellowLabel.font = fontText;
        lampYellowLabel.lineHeight = 16;

        var lampYellowDescription = page1.getChildAt(12);
        lampYellowDescription.x = 583 - xCorrection;
        lampYellowDescription.y = 343;
        lampYellowDescription.font = fontText;
        lampYellowDescription.color = "#afafaf";
        lampYellowDescription.lineWidth = 315;
        lampYellowDescription.lineHeight = 16;

        page1.removeChildAt(2);
//////////////////////////////////////////////////////////////////////////////////
        var page2 = this._pages[1];

        var page2Image = page2.getChildAt(0);
        page2Image.x = 55;
        page2Image.y = 135;

        var page2Header = page2.getChildAt(1);
        page2Header.font = fontHeader;
        page2Header.x = 87;
        page2Header.y = 96;

        var page2Text = page2.getChildAt(3);
        page2Text.x = 568;
        page2Text.y = 153;
        page2Text.lineWidth = 315;
        page2Text.lineHeight = 16;
        page2Text.font = fontText;
        page2Text.color = "#afafaf";
        page2.removeChildAt(2);
//////////////////////////////////////////////////////////////////////////////////
        var page3 = this._pages[2];

        var page3Image = page3.getChildAt(0);
        page3Image.x = 55;
        page3Image.y = 135;

        var page3Header = page3.getChildAt(1);
        page3Header.font = fontHeader;
        page3Header.x = 61;
        page3Header.y = 96;

        var page3Text = page3.getChildAt(3);
        page3Text.x = 568;
        page3Text.y = 220;
        page3Text.font = fontText;
        page3Text.lineWidth = 315;
        page3Text.lineHeight = 16;

        page3Text.color = "#afafaf";
        page3.removeChildAt(2);
    }

    alteastream.Info = createjs.promote(Info,"Container");
})();