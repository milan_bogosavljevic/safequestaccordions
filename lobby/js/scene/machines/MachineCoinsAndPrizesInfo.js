
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MachineCoinsAndPrizesInfo = function(){
        this.Container_constructor();
        this.initMachineCoinsAndPrizesInfo();
    };

    var p = createjs.extend(MachineCoinsAndPrizesInfo,createjs.Container);

    var _this;

    p.initMachineCoinsAndPrizesInfo = function () {
        console.log("MachineCoinsAndPrizesInfo initialized:::");
        _this = this;
        this.setLayout();
    };

    MachineCoinsAndPrizesInfo.getInstance = function() {
        return _this;
    };

    // private methods
    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.addChild(textInstance);
        textInstance.mouseEnabled = false;
    };

    // public methods
    p.setLayout = function() {
        var labelsFont = "15px HurmeGeometricSans3";
        var valuesFont = "bold 24px HurmeGeometricSans3";
        var col_1 = "#a39f9e";
        var col_2 = "#ffcc00";
        var col_3 = "#3ee019";
        var labelsY = 889;
        var valuesY = 912;

        var shapeW = 1195;
        var shapeH = 90;
        //hasCoPlay change
        //var controlsBgShapeRight = this.controlsBgShapeRight = new createjs.Shape();
        var footerShape = this.footerShape = new createjs.Shape();
        footerShape.graphics.beginFill("#000000").drawRect(0, 0, shapeW, shapeH);
        footerShape.cache(0, 0, shapeW, shapeH);
        footerShape.size = {width:shapeW, height:shapeH, roundness:5};
        footerShape.x = 263;
        footerShape.y = 855;
        footerShape.mouseEnabled = false;
        this.addChild(footerShape);
        footerShape.alpha = 0.7;

        this._createDynamicText("tokensFiredLabel","COINS FIRED ",labelsFont,col_1,{x:855,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("tokensFiredValue","",valuesFont,col_3,{x:this.tokensFiredLabel.x,y:valuesY,textAlign:"center", textBaseline:"middle"});

        this._createDynamicText("tokensInfoLabel","PRIZES COLLECTED",labelsFont,col_1,{x:1154,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("decorator","--",valuesFont,col_2,{x:1160,y:valuesY,textAlign:"center", textBaseline:"middle"});

        this._createDynamicText("totalWinLabel","TOTAL",labelsFont,col_1,{x:1273,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("totalWinValue","",valuesFont,col_2,{x:this.totalWinLabel.x,y:valuesY,textAlign:"center", textBaseline:"middle"});

        this._createDynamicText("lastWinLabel","LAST",labelsFont,col_1,{x:1339,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("lastWinValue","",valuesFont,col_2,{x:this.lastWinLabel.x,y:valuesY,textAlign:"center", textBaseline:"middle"});

        this._createDynamicText("biggestGameWinLabel","BIGGEST",labelsFont,col_1,{x:1408,y:labelsY,textAlign:"center", textBaseline:"middle"});
        this._createDynamicText("biggestGameWinValue","",valuesFont,col_2,{x:this.biggestGameWinLabel.x,y:valuesY,textAlign:"center", textBaseline:"middle"});
    };

    p.updateProps = function(machineInfo){
        console.log("updateProps::::::::::::::::::::");
        this.tokensFiredValue.text = machineInfo.tokensFired;
        this.lastWinValue.text = machineInfo.lastWin;
        this.totalWinValue.text = machineInfo.totalWin;
        this.biggestGameWinValue.text = machineInfo.biggestGameWin;
    };

    p.showPrizes = function(show) {
        this.tokensInfoLabel.visible =
        this.decorator.visible =
        this.totalWinLabel.visible =
        this.totalWinValue.visible =
        this.lastWinLabel.visible =
        this.lastWinValue.visible =
        this.biggestGameWinLabel.visible =
        this.biggestGameWinValue.visible = show;
    };

    p.adjustMobile = function() {
        var font = "11px HurmeGeometricSans3"
        var fontBold = "bold 14px HurmeGeometricSans3";

        //hasCoPlay change
        var footerShape = this.footerShape;
        footerShape.uncache();
        footerShape.graphics.clear();
        footerShape.graphics.beginFill("#000000").drawRect(0, 0, 567, 45);
        footerShape.cache(0, 0, 567, 45);
        footerShape.size = {width:567, height:45};
        footerShape.x = 212;
        footerShape.y = 470;
        footerShape.alpha = 0.7;

        this.tokensFiredLabel.font = font;
        this.tokensFiredLabel.x = 470;
        this.tokensFiredLabel.y = 482;

        this.tokensFiredValue.font = fontBold;
        this.tokensFiredValue.x = this.tokensFiredLabel.x;
        this.tokensFiredValue.y = this.tokensFiredLabel.y + 20;

        this.tokensInfoLabel.font = font;
        this.tokensInfoLabel.x = 570;
        this.tokensInfoLabel.y = this.tokensFiredLabel.y;

        this.decorator.font = fontBold;
        this.decorator.x = this.tokensInfoLabel.x;
        this.decorator.y = this.tokensInfoLabel.y + 20;

        this.lastWinLabel.font = font;
        this.lastWinLabel.x = 646;
        this.lastWinLabel.y = this.tokensFiredLabel.y;

        this.lastWinValue.font = fontBold;
        this.lastWinValue.x = this.lastWinLabel.x;
        this.lastWinValue.y = this.lastWinLabel.y + 20;

        this.totalWinLabel.font = font;
        this.totalWinLabel.x = 696;
        this.totalWinLabel.y = this.tokensFiredLabel.y;

        this.totalWinValue.font = fontBold;
        this.totalWinValue.x = this.totalWinLabel.x;
        this.totalWinValue.y = this.totalWinLabel.y + 20;

        this.biggestGameWinLabel.font = font;
        this.biggestGameWinLabel.x = 746;
        this.biggestGameWinLabel.y = this.tokensFiredLabel.y;

        this.biggestGameWinValue.font = fontBold;
        this.biggestGameWinValue.x = this.biggestGameWinLabel.x;
        this.biggestGameWinValue.y = this.biggestGameWinLabel.y + 20;
    };

    alteastream.MachineCoinsAndPrizesInfo = createjs.promote(MachineCoinsAndPrizesInfo,"Container");
})();