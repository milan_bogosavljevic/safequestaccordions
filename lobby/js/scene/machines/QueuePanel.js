(function(){
    "use strict";

    var QueuePanel = function(){
        this.Container_constructor();
        this.initQueuePanel();
    };

    var p = createjs.extend(QueuePanel,createjs.Container);

    // private properties
    p._machineButton = null;
    p._availabilityImage = null;
    p._availabilityText = null;
    p._firstInQueue = false;
    p._queueLeaveCallback = null;
    // public methods
    p.currentMachineInfo = null;
    p.yourPositionInQueue = 0;
    p.queueIsFree = true;
    p.dirtyMachines = [];
    p.previousMachineActive = false;
    p.queueExists = false;

    var _instance = null;

    p.initQueuePanel = function () {
        console.log("QueuePanel initialized:::");
        _instance = this;

        var bannerContainer = this.bannerContainer = new createjs.Container();

        var labelsFont = "15px HurmeGeometricSans3";
        var valuesFont = "bold 24px HurmeGeometricSans3";

        var availabilityText = this._availabilityText = new createjs.Text("",labelsFont,"#a39f9e").set({textAlign:"center", textBaseline:"middle"});
        var machinesQueueNumber = this.machinesQueueNumber = new createjs.Text("0",valuesFont,"#ffcc00").set({textAlign:"center",textBaseline:"middle"});
        var positionInQueueText = this.positionInQueueText = new createjs.Text("YOUR POSITION",labelsFont,"#a39f9e").set({textAlign:"center",textBaseline:"middle"});
        var positionInQueueNumber = this.positionInQueueNumber = new createjs.Text("--",valuesFont,"#ffcc00").set({textAlign:"center",textBaseline:"middle"});

/*        if(alteastream.AbstractScene.HAS_COPLAY){
            var multiPlayerControls = this.multiPlayerControls = new alteastream.MultiplayerControls();
            this.addChild(multiPlayerControls);
            this.multiPlayerControls.show(false);
        }else{
            positionInQueueText.x = positionInQueueNumber.x = 350;
            positionInQueueText.y = 900;
            positionInQueueNumber.y = 900 + 23;
        }*/

        positionInQueueText.x = positionInQueueNumber.x = 350;
        positionInQueueText.y = 900;
        positionInQueueNumber.y = 900 + 23;

        bannerContainer.addChild(availabilityText, machinesQueueNumber, positionInQueueText, positionInQueueNumber);
        this.addChild(bannerContainer);
        bannerContainer.visible = false;
        bannerContainer.mouseEnabled = false;
        bannerContainer.mouseChildren = false;
        bannerContainer.y-=11;

        var entranceCounterBackground = this._entranceCounterBackground = alteastream.Assets.getImage(alteastream.Assets.images.infoBackground);
        entranceCounterBackground.visible = false;
        entranceCounterBackground.x = -100;//this.x
        entranceCounterBackground.y = -121;//this.y
        this.addChild(entranceCounterBackground);

        var entranceCounter = this.entranceCounter = new alteastream.EntranceCounter();
        this.addChild(entranceCounter);
        entranceCounter.mouseEnabled = false;
        entranceCounter.mouseChildren = false;
        this.entranceCounter.show(false);

        var btnStartText = new createjs.Text("PLAY NOW","bold 32px Roboto", "#ffffff").set({x:0,y:0,textAlign:"center",textBaseline:"middle"});
        var btnStart = this.btnStart = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnStart),3,btnStartText);
        btnStart.x = 860;//743
        btnStart.y = 615;//575
        this.addChild(btnStart);
        btnStart.setClickHandler(this.startGame);
        btnStart.visible = false;

        this.setSpinnerBackground();
        var fontSpinner = "26px HurmeGeometricSans3";
        var spinner = this.spinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = this.spinnerBackground.x + 960;
        spinner.y = this.spinnerBackground.y + 470;
        spinner.customScaleGfx(1.4);
        spinner.customGfxPosition(0, 240);
        spinner.customFont(fontSpinner, "center");
        spinner.mouseEnabled = false;
        spinner.setLabel("MAKING ROOM FOR YOU\n\nPLEASE WAIT...");
        spinner.runPreload(false);
    };

    QueuePanel.getInstance = function(){
        return _instance;
    };

    p.addMultiplayerControls = function (participateCounter) {
        var multiPlayerControls = this.multiPlayerControls = new alteastream.MultiplayerControls();
        this.addChild(multiPlayerControls);
        this.multiPlayerControls.showMultiPlayerControls(false);
        this.multiPlayerControls.setBettingCounterTime(participateCounter);
        if(window.localStorage.getItem('isMobile') === "yes"){
            this.multiPlayerControls.adjustMobile();
        }
    };

    p._createText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline, visible:props.visible});
        this.addChild(textInstance);
    };

    p._updateAvailabilityComponents = function(machineInfo){// metoda se zove samo u lokalnoj verziji
        this._availabilityText.text = machineInfo.isFreeToPlay ? "AVAILABLE" : "CO-PLAY";
        //this.positionInQueueText.visible =
            //this.positionInQueueNumber.visible =
                this.machinesQueueNumber.visible = !machineInfo.isFreeToPlay;

        if(!alteastream.AbstractScene.HAS_COPLAY){
            this.machinesQueueNumber.visible = false;
            this._availabilityText.visible = false;
        }
    };

    //new socket
    p.updateAvailabilityComponents = function(isFreeToPlay){
        this._availabilityText.text = isFreeToPlay === true  ? "AVAILABLE" : "CO-PLAY";
        //this.positionInQueueText.visible =
            //this.positionInQueueNumber.visible =
                this.machinesQueueNumber.visible = !isFreeToPlay;

        if(!alteastream.AbstractScene.HAS_COPLAY){
            this.machinesQueueNumber.visible = false;
            this._availabilityText.visible = false;
        }

        this._entranceCounterBackground.visible = isFreeToPlay;
    };

    p.activateWaitingSpinner = function (activate) {
        if(!alteastream.AbstractScene.HAS_COPLAY)
            return;
        // possible fix for double spinner
        // also remove if check from _handleQueueFailed
        //if(this.spinner.active !== activate){
        this.multiPlayerControls.enable(false);
        this.multiPlayerControls.showMultiPlayerControls(false);
        this.spinnerBackground.visible = activate;
        this._entranceCounterBackground.visible = true;
        var lm = alteastream.LocationMachines.getInstance();
        lm.machineCoinsAndPrizesInfo.visible = false;
        //lm._pagination.showPagination(false);
        lm.setBackButtonForEntranceCounter(activate);
        this.spinner.runPreload(activate);
        //}
    };

    p.setSpinnerBackground = function () {
        var spinnerBackground = this.spinnerBackground = new createjs.Container();
        spinnerBackground.mouseEnabled = false;

        var fatCircle = new createjs.Shape();
        fatCircle.graphics.setStrokeStyle(19).beginStroke("#f9c221").arc(0, 0, 250, 0, (Math.PI/180) * 310).endStroke();
        fatCircle.rotation = -245;
        fatCircle.x = 960;
        fatCircle.y = 511;

        var thinCircle = new createjs.Shape();
        thinCircle.graphics.clear().setStrokeStyle(3).beginStroke("#744006").arc(0, 0, 320, 0, (Math.PI/180) * 328).endStroke();
        thinCircle.rotation = -254;
        thinCircle.x = 960;
        thinCircle.y = 511;

        spinnerBackground.addChild(fatCircle, thinCircle);
        spinnerBackground.x = -100;// location machines x = 100
        spinnerBackground.y = -121;// location machines y = -121
        spinnerBackground.visible = false;
        this.addChild(spinnerBackground);
    };

    p.enterQueue = function(){// local ver
        console.log("machine_id on enterQueue "+alteastream.AbstractScene.GAME_ID);// local ver
        this.enableStart(false);// local ver

        alteastream.Requester.getInstance().enterMachine(function(response){// local ver
            if(response.tp === 3){// local ver
                // filtering
                _instance.queueExists = true;// local ver
                if(response.vl>0){//na soket mozda nece moci ALREADY in queue? // local ver
                    console.log(" ALREADY in queue::::::::::::::");// local ver
                    _instance._firstInQueue = false;// local ver
                    _instance.activateWaitingSpinner(true);// local ver
                }else{// local ver
                    console.log(" ADDED to queue::::::::::::::");// local ver
                    _instance._firstInQueue = true;// local ver
                    _instance.entranceCounter.show(false);// local ver
                    _instance.btnStart.visible = false;// local ver

                    if(_instance.spinner.active){// local ver
                        _instance.activateWaitingSpinner(false);// local ver
                    }// local ver
                    _instance.visible = true;// local ver
                    _instance.positionInQueueText.visible = true;// local ver
                    _instance.positionInQueueNumber.visible = true;// local ver
                    if(alteastream.AbstractScene.HAS_COPLAY)// local ver
                        _instance.multiPlayerControls.setConfirmedStake(10);// local ver
                    _instance.parent.onQueueEntered();// local ver
                }// local ver
            }else{// local ver
                alteastream.Lobby.getInstance().throwAlert(alteastream.Alert.ERROR, "adding to queue failed");// local ver
            }// local ver
            // filtering // local ver
            //alteastream.Lobby.getInstance().currentLocationMachines.setListLayout(true);// local ver
        });// local ver

        // testing making room for you
        /*        setTimeout(function (){
                    _instance.activateWaitingSpinner(true);
                },1000);*/

        //this.manageQueueReadyState();// local ver
    };// local ver

    //new socket
    /* // local ver
    p.enterQueue = function(){
        console.log("machine_id on enterQueue "+alteastream.AbstractScene.GAME_ID);
        this.enableStart(false);
        alteastream.SocketCommunicatorLobby.getInstance().enterQueue();
    };

    //new socket
    p.onQueueEnter = function(parsedMsg){
        console.log(" ADDED to queue::::::::::::::");
        _instance.queueExists = true;
        _instance._firstInQueue = true;

        //_instance.entranceCounter.show(false);
        //_instance.btnStart.visible = false;
        _instance.enableStart(false);

            if(_instance.spinner.active){
                //_instance.spinner.runPreload(false);
                //_instance.activateWaitingSpinner(false);
            }
            _instance.visible = true;
            _instance.positionInQueueText.visible = true;
            _instance.positionInQueueNumber.visible = true;
            //_instance.multiPlayerControls.setConfirmedStake(parsedMsg.stake);//proveriti sa Andrijom

            if(alteastream.AbstractScene.HAS_COPLAY){
                var stake = parsedMsg.stake || 13;
                _instance.multiPlayerControls.setConfirmedStake(stake);
            }

            _instance.parent.onQueueEntered();
            alteastream.LocationMachines.getInstance().visible = true;

        //_instance.updateAvailabilityComponents(_instance.queueIsFree);
    };
    */ // local ver

    // new socket
    p.onQueueDeferred = function(code){
        _instance.resetDisplay();
        //new socket
        //_instance.updateAvailabilityComponents(_instance.queueIsFree);
        // if(code === 7){
        // _instance.machineCleared = false;
        //}
        _instance.dirtyMachines.push(_instance.parent.streamPreview.currentStream);
        alteastream.LocationMachines.getInstance().visible = true;
    };

    p.resetDisplay = function(){
        _instance.queueIsFree = false;
        _instance._firstInQueue = false;
        _instance.visible = true;
        _instance.entranceCounter.show(false);
        _instance.btnStart.visible = false;
        _instance.activateWaitingSpinner(true);
        _instance.parent._machinesComponent.visible = false;
        _instance.parent._locationLabel.visible = false;
    };

    p.machineIsDirty = function(machineIndex){
        if(_instance.dirtyMachines.indexOf(machineIndex)>-1){
            _instance.dirtyMachines.splice(_instance.dirtyMachines.indexOf(machineIndex),1);
            return _instance.parent.streamPreview.currentStream === machineIndex;
        }
    };

    //new socket
    /* // local ver
    p.onQueueError = function(code){
        _instance._firstInQueue = false;
        alteastream.Lobby.getInstance().throwAlert(alteastream.Alert.ERROR, "queuing failed: "+code,function(){
            _instance.parent.backToLobbyButtonHandler();
        });
    };
    */ // local ver

    //new socket
    p.exitQueue = function(){ //+leaveQueue mora za njim, ne na expire
        this.entranceCounter.clearCounterInterval();
        if(alteastream.AbstractScene.HAS_COPLAY){
            this.multiPlayerControls.showMultiPlayerControls(false);
            this.multiPlayerControls.enable(false);
            this.multiPlayerControls.quitBetting();
        }

        this.resetEntrance();
        this.visible = false;
        this.positionInQueueText.visible = false;
        this.positionInQueueNumber.visible = false;
        this.bannerContainer.visible = false;
    };

    p.leaveQueue = function(callback){// local ver
        alteastream.Requester.getInstance().leaveMachine(function(response){// local ver
            if(response.tp === 4){// local ver
                console.log(" REMOVED from queue::::::::::::::");// local ver
                _instance.queueExists = false;// local ver
                if(callback){// local ver
                    callback();// local ver
                }// local ver
            }else{// local ver
                alteastream.Lobby.getInstance().throwAlert(alteastream.Alert.ERROR, "removing from queue failed");// local ver
            }// local ver
        });// local ver
    };// local ver

    //new socket
    /* // local ver
    p.leaveQueue = function(callback){
        if(this.yourPositionInQueue > 0){
            this.setQueueLeaveCallback(callback);
            alteastream.SocketCommunicatorLobby.getInstance().leaveQueue();
        }
    };
    */ // local ver

    //new socket
    p.onQueueLeave = function(){
        this._firstInQueue = false;
        if(this._queueLeaveCallback){
            this._queueLeaveCallback();
            this.setQueueLeaveCallback(null);
        }
    };
    //new socket
    p.setQueueLeaveCallback = function(callback){
        this._queueLeaveCallback = callback;
    };

    p.manageQueueReadyState = function(waitTime){
        this.updateAvailabilityComponents(true);
        this.enableStart(true);
        this.entranceCounter.update(waitTime);

        if(this.spinner.active){
            this.activateWaitingSpinner(false);
        }

        this.parent.setBackButtonForEntranceCounter(true);
    };

    p.startGame = function(){
        _instance.parent.onStartGame();
    };

    p.enableStart = function(bool){// local ver
        if(this._firstInQueue){// local ver
            this.btnStart.setDisabled(bool);// local ver
        } else{// local ver
            this.btnStart.setDisabled(!bool);// local ver
            this.entranceCounter.show(false);// local ver
            this.btnStart.visible = false;// local ver
        }// local ver

        if(alteastream.AbstractScene.HAS_COPLAY)// local ver
            this.multiPlayerControls.showMultiPlayerControls(!bool);// local ver
    };// local ver

    //new socket
    /* // local ver
    p.enableStart = function(bool){
        this.btnStart.setDisabled(!bool);
        this.btnStart.visible = bool;
        this.entranceCounter.show(bool);
        if(alteastream.AbstractScene.HAS_COPLAY === true){
            if(bool === true){
                this.multiPlayerControls.showMultiPlayerControls(false);
            }
        }
    };
    */ // local ver

    p.updateQueueInfo = function(response){// local ver
        this.machinesQueueNumber.text = response;// local ver

        if(!this._firstInQueue){// local ver
            this.entranceCounter.show(false);// local ver
            this.btnStart.visible = false;// local ver
            if(this.spinner.active){// local ver
                this.activateWaitingSpinner(false);// local ver
            }// local ver
            if(!response > 0 || response === "you_left_the_game"){// local ver
                console.log("updateQueueInfo:: you_left_the_game");// local ver
            }// local ver
        }// local ver
    };// local ver

    //new socket
    /* // local ver
    p.updateQueueInfo = function(msgVl){
        //if(msgVl.size){
        var parsedMsg = JSON.parse(msgVl);
        console.log("parced msg");
        console.log(parsedMsg);
        if(typeof parsedMsg == "object"){
            this.yourPositionInQueue = parsedMsg.you;
            this.machinesQueueNumber.text = parsedMsg.size;
            this.positionInQueueNumber.text = this.yourPositionInQueue;
        }else{
            this.yourPositionInQueue = 0;
            this.machinesQueueNumber.text = msgVl;
            this.positionInQueueNumber.text = this.yourPositionInQueue;
        }
    };
    */ // local ver

    p.resetEntrance = function(wait){
        this._firstInQueue = false;
        this.enableStart(false);
        if(!wait){
            this.activateWaitingSpinner(false);
        }
    };

    p.setCurrentMachine = function(machineInfo) {
        alteastream.AbstractScene.GAME_ID = machineInfo.machineIndex;
        alteastream.Lobby.getInstance().currentMap.clickEnabled(false);
        if(this.spinner.active === true){
            this.activateWaitingSpinner(false);
        }
    };

    p.setTextsPositionForMultiplayer = function(setForMp) {
        this._availabilityText.font = this.positionInQueueText.font = setForMp === true ? "15px HurmeGeometricSans3" : "bold 22px HurmeGeometricSans3";
        this._availabilityText.color = this.positionInQueueText.color = setForMp === true ? "#a39f9e" : "#ffffff";

        this._availabilityText.x = this.machinesQueueNumber.x = setForMp === true ? 1235 : 1585;//347
        this.positionInQueueText.x = this.positionInQueueNumber.x = setForMp === true ? 1381 : 1585;//497

        var ySpacing = setForMp === true ? 23 : 28;

        this._availabilityText.y = setForMp === true ? 900 : 358;
        this.machinesQueueNumber.y = this._availabilityText.y + ySpacing;

        this.positionInQueueText.y = setForMp === true ? 900 : 657;
        this.positionInQueueNumber.y = this.positionInQueueText.y + ySpacing;
    };

    p.adjustMobile = function(){
        this._availabilityText.font = "12px HurmeGeometricSans3";
        this._availabilityText.x = 862;
        this._availabilityText.y = 267;

        this.machinesQueueNumber.font = "bold 16px HurmeGeometricSans3";
        this.machinesQueueNumber.x = this._availabilityText.x;
        this.machinesQueueNumber.y = this._availabilityText.y + 20;

        this.positionInQueueText.font = "11px HurmeGeometricSans3";
        this.positionInQueueText.x = 265;
        this.positionInQueueText.y = 493;

        this.positionInQueueNumber.font = "bold 16px HurmeGeometricSans3";
        this.positionInQueueNumber.x = this.positionInQueueText.x;
        this.positionInQueueNumber.y = this.positionInQueueText.y + 20;

        this.btnStart.text.font = "bold 24px Roboto";
        this.btnStart.x = 470;
        this.btnStart.y = 346;

        this.spinner.x = 470;
        this.spinner.y = 220;
        this.spinner.customScaleGfx(0.8);
        this.spinner.customFont("15px HurmeGeometricSans3", "center");
        this.spinner.customGfxPosition(0, 130);

        this.spinnerBackground.x = -10;// location machines x = 10
        this.spinnerBackground.y = -10;// location machines y = 10

        this._entranceCounterBackground.x = -10;
        this._entranceCounterBackground.y = -10;

        var fatCircle = this.spinnerBackground.getChildAt(0);
        var thinCircle = this.spinnerBackground.getChildAt(1);

        fatCircle.graphics.clear().setStrokeStyle(10).beginStroke("#f9c221").arc(0, 0, 145, 0, (Math.PI/180) * 310).endStroke();
        fatCircle.rotation = -245;
        fatCircle.x = 480;
        fatCircle.y = 243;

        thinCircle.graphics.clear().setStrokeStyle(1).beginStroke("#744006").arc(0, 0, 200, 0, (Math.PI/180) * 308).endStroke();
        thinCircle.rotation = -244;
        thinCircle.x = fatCircle.x;
        thinCircle.y = fatCircle.y;

        this.entranceCounter.adjustMobile();
        //if(alteastream.AbstractScene.HAS_COPLAY)
            //this.multiPlayerControls.adjustMobile();

        this.setTextsPositionForMultiplayer = function(setForMp) {
            _instance._availabilityText.color = _instance.positionInQueueText.color = setForMp === true ? "#a39f9e" : "#ffffff";
            _instance._availabilityText.x = _instance.machinesQueueNumber.x = setForMp === true ? 600 : 862;
            _instance.positionInQueueText.x = _instance.positionInQueueNumber.x = setForMp === true ? _instance.machinesQueueNumber.x + 126 : _instance.machinesQueueNumber.x;

            _instance._availabilityText.y = setForMp === true ? 493 : 267;
            _instance.machinesQueueNumber.y = _instance._availabilityText.y + 23;

            _instance.positionInQueueText.y = setForMp === true ? 493 : 398;
            _instance.positionInQueueNumber.y = _instance.positionInQueueText.y + 23;

            _instance._availabilityText.font = _instance.positionInQueueText.font = setForMp === true ? "11px HurmeGeometricSans3" : "12px HurmeGeometricSans3";
            _instance.machinesQueueNumber.font = _instance.positionInQueueNumber.font = setForMp === true ? "bold 14px HurmeGeometricSans3" : "bold 16px HurmeGeometricSans3";
        };
    };

    alteastream.QueuePanel = createjs.promote(QueuePanel,"Container");
})();