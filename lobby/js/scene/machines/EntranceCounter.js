
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var EntranceCounter = function(){
        this.AbstractCounter_constructor();
        this.initEntranceCounter();
    };

    var p = createjs.extend(EntranceCounter,alteastream.AbstractCounter);

    var _this;

    p.thinCircle = null;

    p.initEntranceCounter = function () {
        console.log("EntranceCounter initialized:::");
        _this = this;
        this.strokeTickness = 19;
        this.counterRadius = 250;
        this.degree = 288;
        this.colors = ["#f9c221", "#ff0000"];

        this._local_activate();// local ver
    };

    EntranceCounter.getInstance = function() {
        return _this;
    };

    p.setLayout = function(){
        var counterCircleBackground = this.counterCircleBackground = new createjs.Shape();
        counterCircleBackground.graphics.clear().setStrokeStyle(19).beginStroke("#555153").arc(0, 0, 250, 0, (Math.PI/180) * 288).endStroke();
        counterCircleBackground.rotation = -234;
        counterCircleBackground.x = 860;
        counterCircleBackground.y = 390;
        counterCircleBackground.mouseEnabled = false;
        this.addChild(counterCircleBackground);

        var counterCircle = this.counterCircle = new createjs.Shape();
        counterCircle.rotation = -234;
        counterCircle.x = counterCircleBackground.x;
        counterCircle.y = counterCircleBackground.y;
        counterCircle.mouseEnabled = false;
        this.addChild(counterCircle);

        var thinCircle = this.thinCircle = new createjs.Shape();
        thinCircle.graphics.clear().setStrokeStyle(3).beginStroke("#744006").arc(0, 0, 320, 0, (Math.PI/180) * 328).endStroke();
        thinCircle.rotation = -254;
        thinCircle.x = 860;
        thinCircle.y = 390;
        thinCircle.mouseEnabled = false;
        this.addChild(thinCircle);

        this._createText("confirmEnterCounter","0","bold 130px HurmeGeometricSans3", "#ffffff",{x:860,y:280,textAlign:"center",textBaseline:"middle"});//1332
        this._createText("secondsText","SECONDS","18px HurmeGeometricSans3", "#ffffff",{x:this.confirmEnterCounter.x,y:this.confirmEnterCounter.y + 100,textAlign:"center",textBaseline:"middle"});//1332
        this._createText("infoText","READY FOR YOU!\nCONFIRM PLAY!\nGOOD LUCK!","bold 24px HurmeGeometricSans3", "#ffffff",{x:this.confirmEnterCounter.x,y:this.confirmEnterCounter.y + 180,textAlign:"center",textBaseline:"middle"});//1332
        this.infoText.lineHeight = 30;
    };

    p.adjustMobile = function(){
        this.strokeTickness = 10;
        this.counterRadius = 145;
        this.degree = 268;

        this.confirmEnterCounter.font = "bold 68px HurmeGeometricSans3"
        this.confirmEnterCounter.x = 470;
        this.confirmEnterCounter.y = 160;

        this.secondsText.font = "12px HurmeGeometricSans3"
        this.secondsText.x = this.confirmEnterCounter.x;
        this.secondsText.y = this.confirmEnterCounter.y + 50;

        this.infoText.font = "bold 14px HurmeGeometricSans3";
        this.infoText.x = this.confirmEnterCounter.x;
        this.infoText.y = this.confirmEnterCounter.y + 90;
        this.infoText.lineHeight = 15;

        var counterCircleBackground = this.counterCircleBackground
        counterCircleBackground.graphics.clear().setStrokeStyle(10).beginStroke("#555153").arc(0, 0, 145, 0, (Math.PI/180) * this.degree).endStroke();
        counterCircleBackground.rotation = -224;
        counterCircleBackground.x = alteastream.AbstractScene.GAME_WIDTH/2-10;
        counterCircleBackground.y = alteastream.AbstractScene.GAME_HEIGHT/2-38;

        var counterCircle = this.counterCircle;
        counterCircle.graphics.clear().setStrokeStyle(10).beginStroke("#555153").arc(0, 0, 145, 0, (Math.PI/180) * this.degree).endStroke();
        counterCircle.rotation = -224;
        counterCircle.x = counterCircleBackground.x;
        counterCircle.y = counterCircleBackground.y;

        var thinCircle = this.thinCircle;
        thinCircle.graphics.clear().setStrokeStyle(1).beginStroke("#744006").arc(0, 0, 200, 0, (Math.PI/180) * 308).endStroke();
        thinCircle.rotation = -244;
        thinCircle.x = counterCircleBackground.x;
        thinCircle.y = counterCircleBackground.y;
    };

    p._local_activate = function() {
        this._updateCounter = function(count){
            var queuePanel = alteastream.QueuePanel.getInstance();
            if(queuePanel._firstInQueue){
                this.show(true);
                queuePanel.btnStart.visible = true;
                if(count>-1)
                    this.confirmEnterCounter.text = count;
                queuePanel.btnStart.setDisabled(false);
            }else{
                queuePanel.btnStart.setDisabled(true);
                queuePanel.btnStart.visible = false;
                this.show(false);
            }
        }.bind(this);
    }

    alteastream.EntranceCounter = createjs.promote(EntranceCounter,"AbstractCounter");
})();