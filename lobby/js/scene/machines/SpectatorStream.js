this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var SpectatorStream = function (scene,options,address){
        this.ABSVideoStream_constructor(scene,options,address);
        this.initialize_SpectatorStream();
    };

    var p = createjs.extend(SpectatorStream, alteastream.ABSVideoStream);
// static properties:
// events:

// public vars:
// private vars:
    p.streamMonitor = null;
    p.spinner = null;
    p.spinnerBackground = null;
// constructor:
    p.initialize_SpectatorStream = function (){
    };

    p.setPlayer = function(address){
        if(address!==null){
            this.machineInfo = address;
            /* // local ver
            var streamPreviewParams = JSON.parse(localStorage.getItem("streamPreviewParams"));
            this.webrtcPlayer.setPreview(address.previewCamera, alteastream.AbstractScene.SHOP_MACHINE, address.machineName);//streamPreviewParams.machineName
            */ // local ver
            this.webrtcPlayer.set(address.webrtcurl,alteastream.AbstractScene.GAME_TOKEN,alteastream.AbstractScene.GAME_ID);// local ver
        }
        this._local_activate();// local ver
    };

    p.handleError = function(error){
        var that = this;
        this.scene.throwAlert(alteastream.Alert.ERROR,error,function(){
            that.webrtcPlayer.stop();
            console.error("ERROR");
        });
    }

    p.concreteActivate = function(){
        this.scene.frame.visible = false;
        this.spinner.runPreload(false);
        this.scene.removeChild(this.spinner);
        this.scene.removeChild(this.spinnerBackground);
        this.spinner = null;
    };

    p.addSpinner = function(){
        //var loadAnimation = new alteastream.SSAnimator("animatedLogo",250,244,19);
        //var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        var spinner = this.spinner = new alteastream.MockLoader();
        this.scene.addChild(spinner);
        spinner.x = 864;
        spinner.y = 372;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;

        var isMobile = window.localStorage.getItem('isMobile');
        if(isMobile === "yes"){
            this.adjustMobile();
        }
    }

    p.setMedia = function(){

    }

    p.setPreloadPoster = function(bool){
        /*var poster = alteastream.Assets.getImage(alteastream.Assets.images.poster);
        this.scene.addChild(poster);
        poster.mouseEnabled = false;*/
    }

    p.addMonitor = function(){
        this.streamMonitor = new alteastream.StreamMonitor(this);
    };

    p.removeMonitor = function(){
        this.streamMonitor._stopMonitor();
    };

    p.super_stop = p.stop;
    p.stop = function(){
        this.super_stop();
        if(this.streamMonitor) {// todo temp fix
            this.removeMonitor();
        }
        if(this.spinner){// todo temp fix
            this.spinner.runPreload(false);
        }
    };

    p.streamConnected = function(){
        this.streamMonitor.streamConnected();
        this.spinner.runPreload(false);
        this.scene.frame.visible = false;
    };

    p.startStreaming = function(){
        this.webrtcPlayer.start();//preview
    };

    p.onStreamFailed = function(){
        this.spinner.runPreload(false);
        this.spinner.preloadInfoText.visible = true;
        this.spinner.setLabel("Preview for "+this.scene.currentMachineName+" failed","#ff0000");
    };

    p.adjustMobile = function () {
        //var loadAnimation = new alteastream.SSAnimator("animatedLogo",125,122,19);
        //var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        var spinner = this.spinner = new alteastream.MockLoader();
        this.scene.addChild(spinner);
        spinner.x = 470;
        spinner.y = 160;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;
    };

    p._local_activate = function(){//click on screen while loading to start video
        this.videoElement.setSource("../lobby/videotest.mp4");
        this.videoElement.play();
        this.videoElement.setLoop(true);
        this.videoElement.setMuted(false);

        //this.scene.controlBoard.restoreMachineSoundState();
        //this.scene.controlBoard.restoreMusicSoundState();
    };

    alteastream.SpectatorStream = createjs.promote(SpectatorStream,"ABSVideoStream");
})();