
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MultiplayerControls = function(){
        this.Container_constructor();
        this.initMultiplayerControls();
    };

    var p = createjs.extend(MultiplayerControls,createjs.Container);
    var _this;

    // private properties
    //p._stakes = [];//if stakes array
    p._selectedStake = 0;
    p._betCounter = null;
    p._winCountTimer = null;
    p._countdownTime = 0;
    p._prizeWin = 0;
    p._prizeWinAmount = null;
    p._coinsWin = 0;
    p._coinsWinAmount = null;
    p._balance = 0;
    p._refundCoinsCounter = 0;
    p._waitingInterval = null;
    p._waitingSpinner = null;
    p._refundAnimationInterval = null;

    // co-play prize win animation
    p.prizeQueue = [];
    p.queueCount = 0;
    p._prizeQueuePlaying = false;

    p.initMultiplayerControls = function () {
        console.log("MultiplayerControls initialized:::");
        _this = this;
        this.setLayout();
    };

    MultiplayerControls.getInstance = function() {
        return _this;
    };

    // private methods
    p._addButton = function(assetName,x,y,text,states){
        var textLabel = text||null;
        states = states || 3;
        var btn = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images[assetName]),states,textLabel);
        btn.x = x;
        btn.y = y;
        return btn;
    };

    p._reset = function(){
        this.setBetToStart(true);
        this._showUI(false);
        this.showTextFields(false);
        this._betCounter.quitCounter();
    };

    p._showUI = function(bool){
        this.btnParticipate.visible = bool;
    };

    p._resetPrizeWinCountTimeout = function(msg) {
        clearTimeout(this._winCountTimer);
        this._winCountTimer = setTimeout(function(){
            _this._prizeWin = 0;
            _this._prizeWinAmount.text = "0.00";
            _this._highlightWin("prize", false);

            // co-play prize win animation
            alteastream.Lobby.getInstance().updateLobbyBalance(_this._balance);

        },2000);//adjustable
    };

    p._resetCoinsWinCountTimeout = function(msg) {
        clearTimeout(this._coinsWinCountTimer);
        this._coinsWinCountTimer = setTimeout(function(){
            _this._coinsWin = 0;
            _this._coinsWinAmount.text = "0.00";
            _this._highlightWin("coins", false);

            // co-play prize win animation
            alteastream.Lobby.getInstance().updateLobbyBalance(_this._balance);

        },2000);//adjustable
    };

/*    p._highlightWin = function(bool){
        this._prizeWinAmount.color = bool === true?"#00ff4d":"#ffcc00";
        if(this._coinsWinAmount.visible === true){
            this._coinsWinAmount.color = bool === true?"#00ff4d":"#ffcc00";
        }
        //some animations
    };*/

    p._highlightWin = function(winType, bool){
        var winField = "_" + winType + "WinAmount";
        this[winField].color = bool === true?"#00ff4d":"#ffcc00";
    };

    // public methods
    p.setLayout = function(){
        /*var shapeW = 1195;
        var shapeH = 90;

        var controlsBgShapeRight = this.controlsBgShapeRight = new createjs.Shape();//moved to MachineCoinsAndPrizesInfo
        controlsBgShapeRight.graphics.beginFill("#000000").drawRect(0, 0, shapeW, shapeH);
        controlsBgShapeRight.cache(0, 0, shapeW, shapeH);
        controlsBgShapeRight.size = {width:shapeW, height:shapeH, roundness:5};
        controlsBgShapeRight.x = 263;
        controlsBgShapeRight.y = 855;
        controlsBgShapeRight.mouseEnabled = false;
        this.addChild(controlsBgShapeRight);
        controlsBgShapeRight.alpha = 0.7;*/

        var betCounter = this._betCounter = new alteastream.BetEntranceCounter();
        betCounter.x = 1447;
        betCounter.y = 0;
        this.addChild(betCounter);
        betCounter.mouseEnabled = false;

        this._textColorDisabled = "#383838";
        this._textColorEnabled = "#ffffff";

        var currency = "EUR";//alteastream.Lobby.getInstance().currency;
        var labelsFont = "14px HurmeGeometricSans3";
        var valuesFont = "bold 24px HurmeGeometricSans3";
        var labelsY = 889;
        var valuesY = 911;

        var stakeLabel = this.stakeLabel = new createjs.Text("STAKE ( "+currency+" ) ",labelsFont,"#a39f9e");
        stakeLabel.textAlign = "center";
        stakeLabel.textBaseline = "middle";
        stakeLabel.x = 331;
        stakeLabel.y = labelsY;
        stakeLabel.mouseEnabled = false;
        this.addChild(stakeLabel);

        var stakeAmount = this.stakeAmount = new createjs.Text("0.00",valuesFont,"#ffcc00");
        stakeAmount.textAlign = "center";
        stakeAmount.textBaseline = "middle";
        stakeAmount.x = stakeLabel.x;
        stakeAmount.y = valuesY;
        stakeAmount.mouseEnabled = false;
        this.addChild(stakeAmount);

        var stakeAmountMock = this.stakeAmountMock = new createjs.Text("0.00",valuesFont,"#ffcc00");
        stakeAmountMock.textAlign = "center";
        stakeAmountMock.textBaseline = "middle";
        stakeAmountMock.x = 1584;
        stakeAmountMock.y = 540;
        stakeAmountMock.mouseEnabled = false;
        this.addChild(stakeAmountMock);

        var prizeWinLabel = this.prizeWinLabel = new createjs.Text("PRIZE ( "+currency+" ) ",labelsFont,"#a39f9e");
        prizeWinLabel.textAlign = "center";
        prizeWinLabel.textBaseline = "middle";
        prizeWinLabel.x = 437;
        prizeWinLabel.y = labelsY;
        prizeWinLabel.mouseEnabled = false;
        this.addChild(prizeWinLabel);

        var _prizeWinAmount = this._prizeWinAmount = new createjs.Text("0.00",valuesFont,"#ffcc00");
        _prizeWinAmount.textAlign = "center";
        _prizeWinAmount.textBaseline = "middle";
        _prizeWinAmount.x = prizeWinLabel.x;
        _prizeWinAmount.y = valuesY;
        _prizeWinAmount.mouseEnabled = false;
        this.addChild(_prizeWinAmount);

        var txtStartBet = new createjs.Text("START CO-PLAY","bold 24px HurmeGeometricSans3","#000000");
        var txtEndBet = new createjs.Text("STOP CO-PLAY","bold 24px HurmeGeometricSans3","#ffffff");
        var btnParticipate = this.btnParticipate = this._addButton("btnParticipate",1585,900,txtStartBet,2);

        btnParticipate.addTextPreset("start", txtStartBet);
        btnParticipate.addTextPreset("stop", txtEndBet);
        btnParticipate.centerText(0,2);
        this.addChild(btnParticipate);

        var spinner = this._waitingSpinner = new alteastream.MockLoader();
        this.addChild(spinner);
        spinner.x = btnParticipate.x;
        spinner.y = btnParticipate.y + 30;
        spinner.customScaleGfx(0.5);
        spinner.customGfxPosition(0, -45);
        spinner.mouseEnabled = false;
        spinner.setLabel("Starting CO-PLAY...");
        spinner.runPreload(false);

        //////////////////////////// ADDING STAKE BUTTONS ////////////////////////////
/*        var btnStakeDec = this.btnStakeDec = this._addButton("btnStakeDec",820,830);
        btnStakeDec.action = -1;
        this.addChild(btnStakeDec);
        btnStakeDec.setClickHandler(function(e){
            _this._onStakeChange(e);
        });

        var btnStakeInc = this.btnStakeInc = this._addButton("btnStakeInc",1054,830);
        btnStakeInc.action = 1;
        this.addChild(btnStakeInc);
        btnStakeInc.setClickHandler(function(e){
            _this._onStakeChange(e);
        });

        p._onStakeChange = function(e){
            this.setStake(e.currentTarget.action);
            console.log("comm send:: stake: "+this._stakes[this._selectedStake]);//Date.now()
            alteastream.Assets.playSound("confirm",0.3);// todo promeniti sound
        };

        p.setInitialStakes = function(stakes){
            this._stakes = stakes;
            this.setStake(0); //if stakes array
        };

        p._setConfirmedStake = function(stake){
            stake = stake/100;
            this.setStake(this._stakes.indexOf(stake)); //if stakes array
            //stake = stake/1000;// novo s vladom
            //this.setStake(stake);// novo s vladom
        };

        //if stakes array
        p.setStake = function(position){
            this._selectedStake += position;
            var stake = this._stakes[this._selectedStake].toFixed(2);
            this.stakeAmount.text = stake;
            this.btnStakeInc.setDisabled(this._selectedStake === this._stakes.length-1);
            this.btnStakeDec.setDisabled(this._selectedStake === 0);
            alteastream.SocketCommunicatorLobby.getInstance().setBetStake(stake);
            //this.setBetLabelToMax(this._selectedStake === (this._stakes.length - 1));
        };*/
        //////////////////////////// ADDING STAKE BUTTONS ////////////////////////////

        //if(alteastream.AbstractScene.GAME_CODE !== "pearlpusher"){
            var coinsLabel = this.coinsLabel = new createjs.Text("COINS ( "+currency+" ) ",labelsFont,"#a39f9e");
            coinsLabel.textAlign = "center";
            coinsLabel.textBaseline = "middle";
            coinsLabel.x = 437 + 106;
            coinsLabel.y = labelsY;
            coinsLabel.mouseEnabled = false;
            this.addChild(coinsLabel);

            var coinsWinAmount = this._coinsWinAmount = new createjs.Text("0.00",valuesFont,"#ffcc00");
            coinsWinAmount.textAlign = "center";
            coinsWinAmount.textBaseline = "middle";
            coinsWinAmount.x = coinsLabel.x;
            coinsWinAmount.y = valuesY;
            coinsWinAmount.mouseEnabled = false;
            this.addChild(coinsWinAmount);
        //}

        this._reset();
    };

    p.showCoinsWinComponent = function(show) {
        this.coinsLabel.visible = this._coinsWinAmount.visible = show;
    };

    p.showTextFields = function(show) {
        this.stakeLabel.color = this.prizeWinLabel.color = show === true ? "#a39f9e" : "#3e3d3d";
        this.stakeAmount.color = this._prizeWinAmount.color = show === true ? "#ffcc00" : "#3e3d3d";
        if(this._coinsWinAmount.visible === true){
            this.coinsLabel.color = show === true ? "#a39f9e" : "#3e3d3d";
            this._coinsWinAmount.color = show === true ? "#ffcc00" : "#3e3d3d";
        }
        this.stakeAmountMock.visible = !show;
    };

    p.setBettingCounterTime = function(time){
        time = time/1000;
        this._countdownTime = time || 60;
    };

    p.startWaitingInterval = function (start){
        this._waitingSpinner.runPreload(start);
        this.btnParticipate.visible = !start;
        if(start === true){
            alteastream.SocketCommunicatorLobby.getInstance().doBetting(true);
            this._waitingInterval = setInterval(function (){
                alteastream.SocketCommunicatorLobby.getInstance().doBetting(true);
            },1000);
        }else{
            if(this._waitingInterval){
                clearInterval(this._waitingInterval);
            }
        }
    };

    p.setBetToStart = function(bool) {//mp
        if(bool) {
            this.btnParticipate.selectTextPreset("start");
            this.btnParticipate.setClickHandler(function(){
                alteastream.Assets.playSound("confirm",0.3);
                console.log("comm send:: doBetting ");
                /* // local ver
                //alteastream.SocketCommunicatorLobby.getInstance().doBetting(true);//live
                _this.startWaitingInterval(true);//live
                */ // local ver
                _this.startBetting();// local ver
            })
        } else {
            this.btnParticipate.selectTextPreset("stop");
            this.btnParticipate.setClickHandler(function(){
                console.log("comm send:: stopBetting ");
                /* // local ver
                alteastream.SocketCommunicatorLobby.getInstance().doBetting(false);//live
                */ // local ver
                _this.stopBetting();// local ver
            })
        }
        var btnImage = bool === true?"btnParticipate":"btnStopParticipate";
        this.btnParticipate.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));
        this.btnParticipate.centerText(0,2);

        alteastream.QueuePanel.getInstance().setTextsPositionForMultiplayer(!bool);
    };

    p.setBettingUI = function(bool){
        this.setBetToStart(!bool);
        this._betCounter.quitCounter();
        this.showTextFields(bool);
    };

    p.showMultiPlayerControls = function(bool){//entire component hide/show
        this.visible = bool;
    };

    p.enable = function(bool){
        if(bool===false){
            this._reset();
        }else{
            this._showUI(true);
            this.showCounter(this._countdownTime);
            this.showCounter(10)// local ver
        }
    };

    p.showCounter = function(time){
        //from socket
        console.log("SHOW COUNTER " +time);
        this._betCounter.show(true);
        this._betCounter.update(time);

        //this.setInitialStakes([0.15,0.25,1.5,1.8,2.0]);// temp, from response
    };

    p.setConfirmedStake = function(stake){
        this._spectatorStake = stake/1000;
        //stake = stake/100;
        //this.setStake(this._stakes.indexOf(stake)); //if stakes array
        //stake = stake/1000;// novo s vladom
        this.setStake(1);// novo s vladom
    };

    p.startBetting = function(){
        //on response {currentBet:0.2} npr
        this.setBettingUI(true);
        this.startWaitingInterval(false);
        //this.setConfirmedStake(msg.vl);
        alteastream.LocationMachines.getInstance().machineCoinsAndPrizesInfo.showPrizes(false);
        alteastream.LocationMachines.getInstance().showStreamOverlay(false);
        //alteastream.Assets.playSound("confirm",0.3);

        // co-play prize win animation
        //this.testPrizeWinAnimation();
    };

    p.stopBetting = function(){// on button & expired counter
        //this.quitBetting();
        var locationMachines = alteastream.LocationMachines.getInstance();
        locationMachines.machineCoinsAndPrizesInfo.showPrizes(true);
        locationMachines.backToLobbyButtonHandler();
        alteastream.QueuePanel.getInstance().leaveQueue(function () {
            console.log("no socket? getMachines here ?? ");
            //that.intervalUpdateLobby(true);// if back from entranceCounter?
        });
    };

/*    p.betEvent = function(msg){
        //vl:{"am":12345,"bal":12345,"iswin":false}
        //this._prizeWin = msg.vl/100;
        //this._prizeWin += msg.vl/100;
        //msg = JSON.parse(msg);

        var values = JSON.parse(msg.vl);

        this._balance = values.bal;
        alteastream.Lobby.getInstance().updateLobbyBalance(this._balance);

        if(values.iswin === true){
            if(values.isrefund === true){
                this._coinsWin += values.am/1000;
                
                var coinsWinText = gamecore.Utils.NUMBER.roundDecimal(this._coinsWin).toFixed(2);
                coinsWinText += gamecore.Utils.NUMBER.addZeros(coinsWinText, 1);
                
                this._coinsWinAmount.text = coinsWinText;
                this._highlightWin("coins", true);
                this._resetCoinsWinCountTimeout(msg);
            }else{
                this._prizeWin += values.am/1000;
                //var winText = (Math.round(this._prizeWin * 1e12) / 1e12).toFixed(2);
                //winText += alteastream.Lobby.getInstance()._addZeros(winText,1);

                var winText = gamecore.Utils.NUMBER.roundDecimal(this._prizeWin).toFixed(2);
                winText += gamecore.Utils.NUMBER.addZeros(winText, 1);

                this._prizeWinAmount.text = winText;
                this._highlightWin("prize", true);
                this._resetPrizeWinCountTimeout(msg);
            }
        }else{
            this.setStake(values.am);
        }
        alteastream.Assets.playSound("confirm",0.3);// change sound
    };*/

    // co-play prize win animation START
    p.betEvent = function(msg){
        var values = JSON.parse(msg.vl);//live
        //var values = msg.vl; //test
        this._balance = values.bal;

        if(values.iswin === true){
            var win = values.am/1000;
            if(values.isrefund === true){
                //this.showWin(win, null, "coins");
                this._showCoinsWin(win);
            }else{
                var tag = Number(values.tagid);
                //this.showWin(win, tag, "prize");
                this._showPrizeWin(win, tag);
            }
        }else{
            this.setStake(values.am);
            if(!this._prizeQueuePlaying && this._refundAnimationInterval === null){
                alteastream.Lobby.getInstance().updateLobbyBalance(this._balance);
            }
            //alteastream.Assets.playSound("confirm",0.3);// change sound
        }
    };

/*    p.testPrizeWinAnimation = function () {
        setTimeout(function (){
            var res = {};
            res.vl = {iswin:true, isrefund:false, am:1000, bal:5000, tagid:1};
            _this.betEvent(res);
        },1000);

        setTimeout(function (){
            var res = {};
            res.vl = {iswin:true,isrefund:false, am:2000, bal:7000, tagid:2};
            _this.betEvent(res);
        },1300);

        setTimeout(function (){
            var res = {};
            res.vl = {iswin:true,isrefund:false, am:3000, bal:10000, tagid:3};
            _this.betEvent(res);
        },1800);
        var res = {};
        res.vl = {iswin:true,isrefund:true, am:3000, bal:10000, tagid:1};
        _this.betEvent(res);
        _this.betEvent(res);
        _this.betEvent(res);
        _this.betEvent(res);
        _this.betEvent(res);
    };*/

/*    p.showWin = function(win, tag, winType){
        if(winType === "prize"){
            this.prizeQueue.unshift([win,tag,winType]);
            if(!this._prizeQueuePlaying){
                this._playPrizeQueue();
            }
        }else{
            this.refundCoinSpawn(win);
        }
    };*/

    p._showPrizeWin = function (win, tag) {
        this.prizeQueue.unshift([win,tag]);
        if(!this._prizeQueuePlaying){
            this._playPrizeQueue();
        }
    };

    p._showCoinsWin = function (win) {
        this.refundCoinSpawn(win);
    };

    p.refundCoinSpawn = function (win) {
        this._refundCoinsCounter++;
        if(this._refundAnimationInterval === null) {
            this._refundAnimationInterval = setInterval(function () {
                console.log("cs");
                _this._refundCoinsCounter--;
                _this._coinSpawnAnimation();
                //_this.playSound("refund", 0.8);

                _this._showWin(win, "coins");

                if (_this._refundCoinsCounter === 0) {
                    clearInterval(_this._refundAnimationInterval);
                    _this._refundAnimationInterval = null;
                }
            }, 100);
        }
    };

    p._coinSpawnAnimation = function(){
        var coin = new alteastream.SpringAnim('coinWin');
        coin.x = this._coinsWinAmount.x;
        coin.y = alteastream.AbstractScene.GAME_HEIGHT+coin.pivot;
        this.addChild(coin);
        this._coinSpawnAnimationParams(coin);
        alteastream.Assets.playSound("refund",0.8);// change sound
    };

    p._coinSpawnAnimationParams = function(coin){
        coin.setGravity(1.5);
        coin.setVR(-10,10);
        coin.setVX(-1.5,1.5);
        coin.setVY(-28,-32);
        coin.animate();
    };

    p._playPrizeQueue = function(){
        this._prizeQueuePlaying = true;
        //var lobby = alteastream.Lobby.getInstance();// ovo treba zbog play sound
        var iterateQueue = function(){
            if(_this.prizeQueue.length>0){
                var lastElement = _this.prizeQueue.pop();
                _this._prizeQueuePlaying = true;
                var winForChip = lastElement[0] + gamecore.Utils.NUMBER.addZeros(lastElement[0], 1);
                var chip = new alteastream.Chip(winForChip, lastElement[1], 1);
                var _height = alteastream.AbstractScene.GAME_HEIGHT || document.getElementById("screen").height;
                chip.x = _this.prizeWinLabel.x;
                chip.y = _height+chip.getSize();
                _this.addChild(chip);
                var yAnimation = (_this.prizeWinLabel.y-chip.getSize()*0.5)-10;
                alteastream.Assets.playSound("refund",0.8);// change sound
                TweenLite.to(chip,2,{y:yAnimation,onComplete:function(){
                        _this.removeChild(chip);
                        chip = null;
                    },ease: Elastic.easeOut.config(0.5, 0.1)});
                _this._showWin(lastElement[0], "prize");
                // nemamo sound fajlove za sad pa zato stoji timeout
/*                var prizeSpoken = "prize_"+lastElement[0];
                lobby.playSoundChain(prizeSpoken,0.3,function(){// append bonus on next soundChain?
                    _this.queueCount++;
                    iterateQueue();
                },_this);*/

                setTimeout(function (){
                    _this.queueCount++;
                    iterateQueue();
                },1000);
            }else{
                if(_this.queueCount > 2){
                    var phrase = "";
                    if(_this.queueCount < 6 ) { phrase = "reallyGoodSpoken";}
                    if(_this.queueCount > 5 && _this.queueCount < 9 ) { phrase = "feelDizzySpoken";}
                    if(_this.queueCount > 8 ) { phrase = "keepGoingSpoken";}

                    // nemamo sound fajlove za sad pa zato stoji timeout
                    //lobby.playSoundChain(phrase,0.3,function(){ _this._prizeQueuePlaying = false; iterateQueue();});

                    setTimeout(function (){
                        _this._prizeQueuePlaying = false;
                        iterateQueue();
                    },1000);
                }else{
                    _this._prizeQueuePlaying = false;
                }
                _this.queueCount = 0;
            }
        };
        // nemamo sound fajlove za sad pa zato stoji timeout
        //lobby.playSound("win1",0.3);
        //lobby.playSoundChain("youHaveWonSpoken",0.3,iterateQueue);

        setTimeout(function (){
            iterateQueue();
        },1000);
    };

    p._showWin = function(winValue, winType) {
        this._highlightWin(winType, true);

        //this._prizeWin += win;
        var win = "_" + winType + "Win";
        var winAmount = win + "Amount";
        var resetMethod = "_reset" + winType.charAt(0).toUpperCase() + winType.slice(1) + "WinCountTimeout";

        this[win] += winValue;
        //var winText = gamecore.Utils.NUMBER.roundDecimal(this._prizeWin).toFixed(2);
        var winText = gamecore.Utils.NUMBER.roundDecimal(this[win]).toFixed(2);
        winText += gamecore.Utils.NUMBER.addZeros(winText, 1);
        //this._prizeWinAmount.text = winText;
        this[winAmount].text = winText;


        //this._resetPrizeWinCountTimeout();
        this[resetMethod]();
    };
    // co-play prize win animation END

    p.quitBetting = function(){// MACHINE_READY_FOR_PLAYER
        this._selectedStake = 0;
        this.setBettingUI(false);
        this.startWaitingInterval(false);
    };

    p.setStake = function(stake){
        stake *= this._spectatorStake;
        this._selectedStake = stake;
        //var zeros = alteastream.Lobby.getInstance()._addZeros(String(stake),1);
        var stakeTxt = gamecore.Utils.NUMBER.roundDecimal(stake);
        stakeTxt += gamecore.Utils.NUMBER.addZeros(stakeTxt, 1);
/*        this.stakeAmount.text = stake + zeros;// novo s vladom
        this.stakeAmountMock.text = stake + zeros; */
        this.stakeAmount.text = stakeTxt;// novo s vladom
        this.stakeAmountMock.text = stakeTxt;
    };

    p.adjustMobile = function(){
        //hasCoPlay change
        /*var controlsBgShapeRight = this.controlsBgShapeRight;//moved to MachineCoinsAndPrizesInfo
        controlsBgShapeRight.uncache();
        controlsBgShapeRight.graphics.clear();
        controlsBgShapeRight.graphics.beginFill("#000000").drawRect(0, 0, 567, 45);
        controlsBgShapeRight.cache(0, 0, 567, 45);
        controlsBgShapeRight.size = {width:567, height:45};
        controlsBgShapeRight.x = 212;
        controlsBgShapeRight.y = 470;
        controlsBgShapeRight.alpha = 0.7;*/

        var font = "11px HurmeGeometricSans3";
        var fontBold = "bold 14px HurmeGeometricSans3";

        var prizeWinLabel = this.prizeWinLabel;
        prizeWinLabel.text = "PRIZE";
        prizeWinLabel.font = font;
        prizeWinLabel.x = 290;
        prizeWinLabel.y = 482;

        var _prizeWinAmount = this._prizeWinAmount;
        _prizeWinAmount.font = fontBold;
        _prizeWinAmount.x = prizeWinLabel.x;
        _prizeWinAmount.y = prizeWinLabel.y + 20;

        var coinsWinLabel = this.coinsLabel;
        coinsWinLabel.text = "COINS";
        coinsWinLabel.font = font;
        coinsWinLabel.x = 342;
        coinsWinLabel.y = prizeWinLabel.y;

        var coinsWinAmount = this._coinsWinAmount;
        coinsWinAmount.font = fontBold;
        coinsWinAmount.x = coinsWinLabel.x;
        coinsWinAmount.y = coinsWinLabel.y + 20;

        var stakeLabel = this.stakeLabel;
        stakeLabel.text = "STAKE";
        stakeLabel.font = font;
        stakeLabel.x = 238;
        stakeLabel.y = prizeWinLabel.y;

        var stakeAmount = this.stakeAmount;
        stakeAmount.font = fontBold;
        stakeAmount.x = stakeLabel.x;
        stakeAmount.y = _prizeWinAmount.y;//345

        var stakeAmountMock = this.stakeAmountMock;
        stakeAmountMock.font = "bold 16px HurmeGeometricSans3";
        stakeAmountMock.x = 862;
        stakeAmountMock.y = 349;//345

        var betCounter = this._betCounter;
        betCounter.x = 786;
        betCounter.y = 48;
        betCounter.adjustMobile();

        var btnParticipate = this.btnParticipate;
        btnParticipate.x = betCounter.x + 75;
        btnParticipate.y = betCounter.y + 444;

        for(var p in btnParticipate._presetFonts){
            btnParticipate._presetFonts[p].text.font = "bold 14px HurmeGeometricSans3";
        }

        btnParticipate.selectTextPreset("start");

        var spinner = this._waitingSpinner;
        spinner.customGfxPosition(0, -22);
        spinner.customFont("bold 12px HurmeGeometricSans3", "center");
        spinner.customScaleGfx(0.2);
        spinner.x = btnParticipate.x;
        spinner.y = btnParticipate.y + 11;

        this._coinSpawnAnimationParams = function(coin){
            coin.setGravity(0.75);
            coin.setVR(-5,5);
            coin.setVX(-0.75,0.75);
            coin.setVY(-14,-16);
            coin.animate();
        };
    }

    alteastream.MultiplayerControls = createjs.promote(MultiplayerControls,"Container");
})();