this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var StreamPreviewTest = function(){
        this.Container_constructor();
        this.initStreamPreviewTest();
    };

    var p = createjs.extend(StreamPreviewTest,createjs.Container);
    var _this = null;
// private properties
    p._active = false;
// public properties
    p.currentStream = "";
    p.currentMachineName = "";

    p.initStreamPreviewTest = function () {
        console.log("StreamPreviewTest initialized:::");
        _this = this;

        var options = {id:"videoOutput",width:1920,height:1080,style:{width:"100%",height:"100%"}};
        this.streamVideo = new alteastream.SpectatorStream(this,options,null);

        var frame = this.frame = alteastream.Assets.getImage(alteastream.Assets.images.window_big);
        frame.x = -100;// location machines x = 100
        frame.y = -121;// location machines y = 121
        frame.scaleX = 4;
        frame.scaleY = 4;
        this.addChild(frame);
        frame.mouseEnabled = false;
    };

    StreamPreviewTest.getInstance = function(){
        return _this;
    };

// private methods
// public methods
    p.activate = function(){

    };

    p.start = function(){
        this._active = true;
        alteastream.LocationMachines.getInstance().resetIntervalUpdateLounge();
    };

    p.show = function(bool) {
        this.visible = bool;
    };

    p.exitPreview = function(){
        //backToLobbyButtonHandler
        this.dispose();
        console.log("EXITED STREAM PREVIEW");
    };

    p.dispose = function(){
        //onStartGame
        this._active = false;
        this.streamVideo.stop();
        this.currentStream = "";
        this.frame.visible = true;
        this.show(false);
        console.log("DISPOSED STREAM PREVIEW");
    };

    p.tryToSwitchStream = function(machine) {
        if(this.currentStream !== machine.machineInfo.machineIndex){// if !machine.selected?
            alteastream.MachinesComponent.getInstance().clearActiveMachines();
            alteastream.SocketCommunicatorLobby.getInstance().lobbySwitchSubscriptions(this.currentStream,machine.machineInfo.machineIndex);
            alteastream.QueuePanel.getInstance().setCurrentMachine(machine.machineInfo);

            machine.selectMachine(true);
            // stream preview roulette
            if(alteastream.Lobby.getInstance().selectedLocation.casinoName === "Roulette"){
                this.switchStream(machine.machineInfo);
                var obj = {
                    previewCamera:machine.machineInfo.previewCamera,
                    shopMachine:alteastream.AbstractScene.SHOP_MACHINE,
                    machineName:machine.machineInfo.machineName,
                    casinoName:alteastream.Lobby.getInstance().selectedLocation.casinoName
                }

                var objToStore = JSON.stringify(obj);
                window.localStorage.setItem("streamPreviewParams", objToStore);
            }// stream preview roulette
            else{
                if(!machine.isFreeToPlay){
                    this.switchStream(machine.machineInfo);
                    this.changeStream(machine.machineInfo);
                    this.changeFrameImage(machine.thumb);
                }else{
                    this.updateInfo(machine.machineInfo);
                }
            }
        }
    };

    p.switchStream = function(machineInfo){
        this.currentStream = machineInfo.machineIndex;
        this.currentMachineName = machineInfo.machineName;
        this.updateInfo(machineInfo);
        this.start();
    };
    
    p.changeStream = function(machineInfo){
        this.show(true);
        this.frame.visible = true;
        this.streamVideo.spinner.setLabel("Connecting to machine "+scene.currentMachineName);
        this.streamVideo.spinner.runPreload(true);

        this.streamVideo.setPlayer(machineInfo);
        this.streamVideo.activate();
    };

    p.changeFrameImage = function(thumb) {
        if(this.frame.visible){
            gamecore.Utils._removeAllFilters([this.frame]);
            this.frame.image = thumb.hasThumbImage === true ? thumb.image : alteastream.Assets.getImage(alteastream.Assets.images.thumb).image;
            gamecore.Utils.DISPLAY.FILTERS.setBlur([this.frame], 2);
        }
    };

    p.updateInfo = function(machineInfo){
        this.machineInfo = machineInfo;
        alteastream.AbstractScene.GAME_CODE = machineInfo.gameCode;
        alteastream.AbstractScene.DEFAULT_GAME_ID = machineInfo.defaultGameId;
    };

    p._createDynamicText = function(instance,text,font,color,props){
        var textInstance = this[instance] = new createjs.Text(text, font, color).set({x:props.x, y:props.y, textAlign:props.textAlign, textBaseline:props.textBaseline});
        this.labelsContainer.addChild(textInstance);
        textInstance.mouseEnabled = false;
    };

    p.adjustMobile = function(){
        var footer = new createjs.Shape();
        footer.graphics.beginFill("#000000").drawRoundRect(0, 0, 616, 40, 3);
        footer.cache(0, 0, 616, 40);
        footer.alpha = 0.3;
        footer.x = 319;
        footer.y = 429;

        this.addChildAt(footer, 0);

        this.frame.x = -10;// location machines x = 10
        this.frame.y = -10;// location machines y = 10
        this.frame.scaleX = 2;
        this.frame.scaleY = 2;

        if(this.spinner)
            this.removeChild(this.spinner);

        var loadAnimation = new alteastream.SSAnimator("animatedLogo",125,122,19);
        var spinner = this.spinner = new alteastream.MockLoader(loadAnimation);
        this.addChild(spinner);
        spinner.x = 470;
        spinner.y = 160;
        spinner.mouseEnabled = false;
        spinner.mouseChildren = false;

        //this.removeChild(this.frame);//todo proveriti
    }

    p._local_activate = function(){
        this.currentStream = 333333333;
        this.start();

        this.frame.visible = true;
        setTimeout(function(){
            _this.frame.visible = false;
            console.log("StreamLoaded::::");
        },1000);
    };

    alteastream.StreamPreviewTest = createjs.promote(StreamPreviewTest,"Container");
})();