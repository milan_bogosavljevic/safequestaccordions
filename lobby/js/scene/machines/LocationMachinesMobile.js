
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LocationMachinesMobile = function(lobby) {
        this.LocationMachines_constructor(lobby);
        this.initialize_LocationMachinesMobile();
    };

    var p = createjs.extend(LocationMachinesMobile, alteastream.LocationMachines);

// static properties:
// events:
// private properties:
// constructor:
    p.initialize_LocationMachinesMobile = function() {};
    // public properties:
    p.swipeCatcher = null;

    // private methods:
    p._superShowMachinesHideShape = p.showMachinesHideShape;
    p.showMachinesHideShape = function(bool){
        this._superShowMachinesHideShape(bool);
        var btnImage = bool === false ? "btnHiddenMenu" : "btnHiddenMenuTransparent";
        this.lobby._hiddenMenuButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));
    };

    p._superUpdateMachinesLabel = p._updateMachinesLabel;
    p._updateMachinesLabel = function(setList){
        this._superUpdateMachinesLabel(setList);
        this._locationLabel.x = this.gridLayoutIsActive ? 473 : 101;
        this._locationLabel.y = this.gridLayoutIsActive ? 35 : 102;
        this._locationLabel.font = this.gridLayoutIsActive ? "16px HurmeGeometricSans3" : "14px HurmeGeometricSans3";
    };

    p._manageButtonsState = function() {};
    p._setButtonsStateOnStart = function(numOfPages) {};
    p._showButtons = function(show) {};
    p._updateButtonsPosition = function(listView){};

    // static methods:
    // public methods:
    p.setComponents = function(lobby) {
        var _this = this;
        this.lobby = lobby;

        var rowsPerPage = 2;
        var columnsPerPage = 3;

        this._machinesComponent = new alteastream.MachinesComponent(rowsPerPage, columnsPerPage);
        this._machinesPerPage = rowsPerPage * columnsPerPage;

        var maskWidth = this._machinesComponent.getWidth();
        var maskHeight = this._machinesComponent.getMaskHeight();

        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(this._machinesComponent._startPositionX, this._machinesComponent._startPositionY, maskWidth, maskHeight);
        this._machinesComponent.mask = mask;
        this._machinesComponent.mask.setBounds(this._machinesComponent._startPositionX, this._machinesComponent._startPositionY, maskWidth, maskHeight);
        //this._machinesComponent.visible = false;
        this._machinesComponent.adjustMobile();

        var topSectionBackground = this._topSectionBackground = new createjs.Shape();
        topSectionBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 933, 60, 2);
        topSectionBackground.alpha = 0.75;
        topSectionBackground.cache(0, 0, 933, 60);
        topSectionBackground.x = 5;
        topSectionBackground.y = 5;
        this.addChild(topSectionBackground);

        var streamOverlay = this.streamOverlay = new createjs.Shape();
        streamOverlay.graphics.beginFill("#000000").drawRect(0, 0, 960, 540);
        streamOverlay.alpha = 0.4;
        streamOverlay.cache(0, 0, 960, 540);
        streamOverlay.visible = false;
        streamOverlay.x = -10;
        streamOverlay.y = -10;
        this.addChild(streamOverlay);

        var streamPreview = this.streamPreview = new alteastream.StreamPreview();
        this.addChild(streamPreview);
        streamPreview.mouseEnabled = false;
        streamPreview.mouseChildren = false;
        streamPreview.show(false);
        streamPreview.adjustMobile();

        var swipeCatcher = this.swipeCatcher = new createjs.Shape();
        swipeCatcher.graphics.beginFill("#000000").drawRect(-10, -10, 960, 540);
        swipeCatcher.cache(-10, -10, 960, 540);
        swipeCatcher.visible = false;
        swipeCatcher.alpha = 0.01;
        this.addChild(swipeCatcher);

        this.addChild(this._machinesComponent);

        /*        var streamPreview = this.streamPreview = new alteastream.StreamPreview();
                this.addChild(streamPreview);
                streamPreview.mouseEnabled = false;
                streamPreview.mouseChildren = false;
                streamPreview.show(false);
                streamPreview.adjustMobile();*/

        var _locationLabelShape = this._locationLabelShape = new createjs.Shape();
        _locationLabelShape.graphics.beginFill("#000000").drawRect(0, 0, 192, 45);
        _locationLabelShape.alpha = 0.7;
        _locationLabelShape.cache(0, 0, 192, 45);
        _locationLabelShape.x = 8;
        _locationLabelShape.y = 80;
        _locationLabelShape.visible = false;

        var _locationLabel = this._locationLabel = new createjs.Text("Mock text","16px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", mouseEnabled:false, x:45, y:35, visible:false});
        this.addChild(_locationLabelShape, _locationLabel);

        var entranceCounterBackground = this._entranceCounterBackground = alteastream.Assets.getImage(alteastream.Assets.images.infoBackground);
        entranceCounterBackground.visible = false;
        entranceCounterBackground.x = -10;//this.x = 10
        entranceCounterBackground.y = -10;//this.y = 10
        this.addChild(entranceCounterBackground);

        var machineCoinsAndPrizesInfo = this.machineCoinsAndPrizesInfo = new alteastream.MachineCoinsAndPrizesInfo();
        machineCoinsAndPrizesInfo.adjustMobile();
        machineCoinsAndPrizesInfo.visible = false;
        this.addChild(machineCoinsAndPrizesInfo);

        var queuePanel = this.queuePanel = new alteastream.QueuePanel();
        this.addChild(queuePanel);
        queuePanel.adjustMobile();

        var buttonBackToLobbyTxt = new createjs.Text("BACK TO LOBBY","13px HurmeGeometricSans3","#ffffff").set({textAlign:"left"});
        var backToLobbyButton = this._backToLobbyButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBack),2,buttonBackToLobbyTxt);
        backToLobbyButton.visible = false;
        backToLobbyButton.text.visible = false;
        backToLobbyButton.x = 79;
        backToLobbyButton.y = 25;
        backToLobbyButton.setClickHandler(function () {
            _this.backToLobbyButtonHandler();
            _this.lobby.checkIfHiddenMenuIsShown();
        });
        this.addChild(backToLobbyButton);

/*        var machineCoinsAndPrizesInfo = this.machineCoinsAndPrizesInfo = new alteastream.MachineCoinsAndPrizesInfo();
        machineCoinsAndPrizesInfo.adjustMobile();
        machineCoinsAndPrizesInfo.visible = false;
        this.addChild(machineCoinsAndPrizesInfo);*/

        var divider = this.divider = new createjs.Text("|","14px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", x:780, y:36});

        var area = new createjs.Shape();
        area.graphics.beginFill("#fff").drawRect(-82, -28, 82, 56);

        var showAllMachinesButton = this.showAllMachinesButton = new createjs.Text("All Machines","14px HurmeGeometricSans3","#2aff00").set({textAlign:"right", textBaseline:"middle", x:divider.x - 10, y:divider.y});
        showAllMachinesButton.hitArea = area;
        showAllMachinesButton.on("click", function () {
            if(showAllMachinesButton.isActive === false){
                _this._showOnlyAvailable = false;
                _this._showAvailableOrAllButtonsHandler();
                _this.lobby.checkIfHiddenMenuIsShown();
            }
        });

        var area2 = new createjs.Shape();
        area2.graphics.beginFill("#71c8e0").drawRect(0, -28, 80, 56);

        var showAvailableMachinesButton = this.showAvailableMachinesButton = new createjs.Text("Available","14px HurmeGeometricSans3","#ffffff").set({textAlign:"left", textBaseline:"middle", x:divider.x + 10, y:divider.y});
        showAvailableMachinesButton.hitArea = area2;
        showAvailableMachinesButton.on("click", function () {
            if(showAvailableMachinesButton.isActive === false){
                _this._showOnlyAvailable = true;
                _this._showAvailableOrAllButtonsHandler();
                _this.lobby.checkIfHiddenMenuIsShown();
            }
        });

        //showAllMachinesButton.visible = showAvailableMachinesButton.visible = divider.visible = false;
        showAllMachinesButton.isActive = showAvailableMachinesButton.isActive = false;

        var scroller = this._scroller = new alteastream.Scroller(this._machinesComponent);
        scroller.setScrollType("scrollByRow");
        scroller.setPreAnimateScrollMethod(function () {
            _this._machinesComponent.mouseEnabled = false;
            _this.lobby.checkIfHiddenMenuIsShown();
            if(_this.gridLayoutIsActive === true){
                _this._canUpdateThumbs = false;
            }
        });
        scroller.setPostAnimateScrollMethod(function () {
            _this._machinesComponent.mouseEnabled = true;
            if(_this.gridLayoutIsActive === true){
                _this._canUpdateThumbs = true;
            }
        });

        scroller.setScrollDirection("y");

        // filtering
        this.addChild(divider, showAllMachinesButton,showAvailableMachinesButton);
    };

    p.setListLayout = function(setList) {
        this.gridLayoutIsActive = !setList;
        this.lobby.setBackgroundImagesForStreamPreview(setList);
        this.lobby.balanceBackground.visible = setList;
        this._machinesComponent.setGridView(!setList);
        this._machinesComponent.setShowOnlyAvailable(setList);
        this._backToLobbyButton.visible = setList;
        this._topSectionBackground.visible = !setList;
        this._locationLabelShape.visible = setList;
        this._resetScrollComponent(setList);
        this._updateMachinesLabel();
        this._manageButtonsState();

        var btnImage = setList === true ? "btnHiddenMenu" : "btnHiddenMenuTransparent";
        this.lobby._hiddenMenuButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[btnImage]));

        var skin = setList ? "btnBack2" : "btnBack";
        this._backToLobbyButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[skin]));

        this.showAllMachinesButton.visible = !setList;
        this.showAvailableMachinesButton.visible = !setList;
        this.divider.visible = !setList;

        //var scrollType = setList === true ? "scrollByRow" : "scrollByPage";
        //this._scroller.setScrollType(scrollType);

        this.lobby.hideBackgroundsForFullScreenVideo(setList);
        this.showStreamOverlay(setList);

        this.swipeCatcher.visible = setList;
    };

    p.onSwipe = function (direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    p._superSetBackButtonForEntranceCounter = p.setBackButtonForEntranceCounter;
    p.setBackButtonForEntranceCounter = function(setForCounter){
        this._superSetBackButtonForEntranceCounter(setForCounter);
        this._backToLobbyButton.x = setForCounter === true ? 467 : 79;
        this._backToLobbyButton.y = setForCounter === true ? 415 : 25;
        this._backToLobbyButton.text.visible = setForCounter;
        this._backToLobbyButton.centerText(4,2);
        this.lobby._hiddenMenuButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images.btnHiddenMenu));
        this.lobby._hiddenMenuButton.x = 878;
        this.lobby._hiddenMenuButton.y = 12;
    };

    alteastream.LocationMachinesMobile = createjs.promote(LocationMachinesMobile,"LocationMachines");
})();