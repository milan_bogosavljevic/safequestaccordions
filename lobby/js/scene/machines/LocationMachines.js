
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LocationMachines = function(lobby) {
        this.Container_constructor();
        this.initialize_LocationMachines(lobby);
    };
    var p = createjs.extend(LocationMachines, createjs.Container);

    var _this = null;
// static properties:
// events:

    // public properties:
    p.lobby = null;
    p.gridLayoutIsActive = true;
    p.isFocused = false;
    p.machineCoinsAndPrizesInfo = false;
    // private properties:
    p._machinesComponent = null;
    p._machinesPerPage = 0;
    p._canUpdateThumbs = true;
    p._backToLobbyButton = null;
    p._locationLabel = null;
    p._locationName = null;
    p._scroller = null;
    p._pagination = null;
    p._showOnlyAvailable = false;
    p._gridLayoutScrollLevel = 0;

// constructor:
    p.initialize_LocationMachines = function(lobby) {
        console.log("LocationMachines initialized::: ");
        _this = this;
        this.setComponents(lobby);
    };

    LocationMachines.getInstance = function(){
        return _this;
    };

    // private methods:
    p._soundNotify = function(){
        alteastream.Assets.playSound("confirm",0.3);
    };

    p._onFocusDone = function() {
        this.isFocused = true;
        this.setMachinesAndScroll();
        this.showMachinesHideShape(true);
        this.intervalUpdateLobby(true);
        this.lobby.setFocused(this);
        this.lobby.setSwipeTarget(this,"y");
    };

    p._showAvailableOrAllButtonsHandler = function() {
        this._machinesComponent.showOnlyAvailableOrAllMachines(this._showOnlyAvailable);
        this.updateAvailableOrAllMachinesButtonsSkin(this._showOnlyAvailable);
        this._resetScrollComponent(false);
    };

    p._storeLocationName = function() {
        this._locationName = this.lobby.selectedLocation.casinoName.charAt(0).toUpperCase() + this.lobby.selectedLocation.casinoName.substring(1);
    };

    p._storeGridScrollLevel = function () {
        this._gridLayoutScrollLevel = this._scroller.scrollByRowCurrentLevel;
    };

    // filtering
    p._resetScrollComponent = function(listView){
        console.log("RESET SCROLL COMP");
        if(listView === true) {
            this._storeGridScrollLevel();
        }
        var numOfPages = this._machinesComponent.getNumberOfPages();
        var machineHeight = this._machinesComponent.getMachineHeight();
        this._scroller.maxScrollLevelByPage = numOfPages - 1;

        var scrollLevelsByRow = [];
        var numberOfRows = Math.ceil(this._machinesComponent.getNumberOfActiveMachines() / this._machinesComponent.getColumnsPerPage());

        var numberOfScrollLevelsByRow = numberOfRows - this._machinesComponent.getRowsPerPage() + 1;
        for(var j = 0; j < numberOfScrollLevelsByRow; j++){
            scrollLevelsByRow.push((j * machineHeight * -1));
        }

        this._scroller.setScrollLevels(scrollLevelsByRow);
        this._scroller.setRowsPerPage(this._machinesComponent.getRowsPerPage());
        this._scroller.setNumberOfPages(numOfPages);
        this._machinesComponent.setAcordionsLayout(listView);
        if(this._machinesComponent.selectedAcordion !== null){
            if(numberOfScrollLevelsByRow > 0){
                this._scroller.setToFirstPage(true, 200);
            }
        }
    };

    p._updateMachinesLabel = function(){
        var numOfMachines = " " + this._machinesComponent.getNumberOfActiveMachines();
        var moreThanOne = numOfMachines > 1 ? " machines" : " machine";
        this._locationLabel.text = this.gridLayoutIsActive ? this._locationName + numOfMachines + moreThanOne : numOfMachines + moreThanOne + " available";
    };

    // public methods
    p.setComponents = function(lobby) {
        this.lobby = lobby;

        var rowsPerPage = 3;
        var columnsPerPage = 4;
        this._machinesPerPage = rowsPerPage * columnsPerPage;
        var machinesComponent = this._machinesComponent = new alteastream.MachinesComponent(rowsPerPage, columnsPerPage);

        var maskWidth = machinesComponent.getWidth();
        var maskHeight = machinesComponent.getMaskHeight();
        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(machinesComponent._startPositionX, machinesComponent._startPositionY, maskWidth, maskHeight);
        mask.setBounds(machinesComponent._startPositionX, machinesComponent._startPositionY, maskWidth, maskHeight);
        machinesComponent.mask = mask;

        var topSectionBackground = this._topSectionBackground = new createjs.Shape();
        topSectionBackground.graphics.beginFill("#000000").drawRoundRect(0, 0, 1679, 50, 4);
        topSectionBackground.alpha = 0.75;
        topSectionBackground.cache(0, 0, 1679, 50);
        topSectionBackground.x = 22;
        topSectionBackground.y = -56;

        var streamOverlay = this.streamOverlay = new createjs.Shape();
        streamOverlay.graphics.beginFill("#000000").drawRect(0, 0, 1920, 1080);
        streamOverlay.alpha = 0.4;
        streamOverlay.cache(0, 0, 1920, 1080);
        streamOverlay.visible = false;
        streamOverlay.x = -100;
        streamOverlay.y = -121;

        var buttonBackToLobbyTxt = new createjs.Text("BACK TO LOBBY","13px HurmeGeometricSans3","#ffffff").set({textAlign:"left"});
        var backToLobbyButton = this._backToLobbyButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnBack),2,buttonBackToLobbyTxt);
        backToLobbyButton.centerText(4, 2);
        backToLobbyButton.visible = false;
        backToLobbyButton.x = -55 + (backToLobbyButton._singleImgWidth*0.5);
        backToLobbyButton.y = -115 + (backToLobbyButton._height*0.5);
        backToLobbyButton.setClickHandler(function () {
            _this.backToLobbyButtonHandler();
            //new socket
            /* // local ver
            console.log("leaveQueue:: backToLobbyButtonHandler");
            _this.queuePanel.leaveQueue(function () {
                console.log("no socket? getMachines here ?? ");
                //_this.intervalUpdateLobby(true);// if back from entranceCounter?
            })
            */ // local ver
        });

        var streamPreview = this.streamPreview = new alteastream.StreamPreview();
        streamPreview.mouseEnabled = false;
        streamPreview.mouseChildren = false;
        streamPreview.show(false);

        var _locationLabelShape = this._locationLabelShape = new createjs.Shape();
        _locationLabelShape.graphics.beginFill("#000000").drawRect(0, 0, 232, 50, 4);
        _locationLabelShape.alpha = 0.7;
        _locationLabelShape.cache(0, 0, 232, 50);
        _locationLabelShape.x = 21;
        _locationLabelShape.y = -56;

        var _locationLabel = this._locationLabel = new createjs.Text("","bold 16px HurmeGeometricSans3","#a0a0a0").set({textAlign:"left", textBaseline:"middle", mouseEnabled:false, x:36, y:-29, visible:false});
        var divider = this.divider = new createjs.Text("|","22px HurmeGeometricSans3","#ffffff").set({textAlign:"center", textBaseline:"middle", x:1586, y:-30});
        var queuePanel = this.queuePanel = new alteastream.QueuePanel();
        var area = new createjs.Shape();
        area.graphics.beginFill("#fff").drawRect(-130, -26, 130, 50);

        var showAllMachinesButton = this.showAllMachinesButton = new createjs.Text("All Machines","22px HurmeGeometricSans3","#2aff00").set({textAlign:"right", textBaseline:"middle", x:divider.x - 10, y:divider.y});
        showAllMachinesButton.hitArea = area;
        showAllMachinesButton.on("click", function () {
            if(showAllMachinesButton.isActive === false){
                _this._showOnlyAvailable = false;
                _this._showAvailableOrAllButtonsHandler();
            }
        });

        var area2 = new createjs.Shape();
        area2.graphics.beginFill("#71c8e0").drawRect(0, -26, 130, 50);

        var showAvailableMachinesButton = this.showAvailableMachinesButton = new createjs.Text("Available","22px HurmeGeometricSans3","#ffffff").set({textAlign:"left", textBaseline:"middle", x:divider.x + 10, y:divider.y});
        showAvailableMachinesButton.hitArea = area2;
        showAvailableMachinesButton.on("click", function () {
            if(showAvailableMachinesButton.isActive === false){
                _this._showOnlyAvailable = true;
                _this._showAvailableOrAllButtonsHandler();
            }
        });

        showAllMachinesButton.isActive = showAvailableMachinesButton.isActive = false;

        var scroller = this._scroller = new alteastream.Scroller(machinesComponent);
        scroller.setScrollType("scrollByRow");
        scroller.setScrollDirection("y");

        scroller.setPreAnimateScrollMethod(function () {
            scroller.manageButtonsState();
            machinesComponent.mouseChildren = false;
            _this._canUpdateThumbs = false;
        });

        scroller.setPostAnimateScrollMethod(function () {
            _this._canUpdateThumbs = true;
            machinesComponent.mouseChildren = true;
            //odkomentovati ako krene da se radi logika da samo vidiljive masine imaju mouseEnabled true
            //machinesComponent.disableActiveMachines(false);
        });

        var machineCoinsAndPrizesInfo = this.machineCoinsAndPrizesInfo = new alteastream.MachineCoinsAndPrizesInfo();
        machineCoinsAndPrizesInfo.visible = false;

        this.addChild(
            topSectionBackground,
            streamOverlay,
            streamPreview,
            machinesComponent,
            _locationLabelShape,
            _locationLabel,
            machineCoinsAndPrizesInfo,
            queuePanel,
            backToLobbyButton,
            //machineCoinsAndPrizesInfo,
            divider,
            showAllMachinesButton,
            showAvailableMachinesButton
        );
    };

    p.showStreamOverlay = function(show) {
        this.streamOverlay.visible = show;
    };

    p.updateAvailableOrAllMachinesButtonsSkin = function(onlyAvailable) {
        this.showAllMachinesButton.color = onlyAvailable === true ? "#ffffff" : "#2aff00";
        this.showAvailableMachinesButton.color = onlyAvailable === true ? "#2aff00" : "#ffffff";
        this.showAllMachinesButton.isActive = !onlyAvailable;
        this.showAvailableMachinesButton.isActive = onlyAvailable;
    }

    p.backToLocationsButtonHandler = function() {
        if(this.lobby.mouseWheel)
            this.lobby.mouseWheel.enable(false);
        this.lobby.backToLocationsFromLobby();

        // blok premesten iz onStreamPreviewDone
        this._machinesComponent.clearActiveMachines();
        this._machinesComponent.disableActiveMachines(true);
        this.lobby.currentMap.clickEnabled(true);
        this.lobby.setBackgroundImagesForStreamPreview(false);
    };

    p.backToLobbyButtonHandler = function() {
        this.streamPreview.exitPreview();
        this._backToLobbyButton.visible = false;
        this.setBackButtonForEntranceCounter(false);
        this.intervalUpdateLounge(false);
        this.intervalUpdateLobby(true);

        this._machinesComponent.clearActiveMachines();
        this.setComponentWhenMachineisFreeToPlay(false);
        this.updateAvailableOrAllMachinesButtonsSkin(false);
        this.queuePanel.exitQueue();
        this.setListLayout(false);

        this.machineCoinsAndPrizesInfo.showPrizes(true);
        this.machineCoinsAndPrizesInfo.visible = false;

        if(this._showOnlyAvailable === true){
            this._showAvailableOrAllButtonsHandler();
        }

        //new socket remove
        console.log("leaveQueue:: backToLobbyButtonHandler"); // local ver
        this.queuePanel.leaveQueue(function () {// local ver
            console.log("no socket? getMachines here ?? ");// local ver
            //_this.intervalUpdateLobby(true);// if back from entranceCounter? // local ver
        }) // local ver

        this._setScrollPosition();
        this._soundNotify();
    };

    p.tryLobby = function(){
        if(this.gridLayoutIsActive === false){
            this.backToLobbyButtonHandler();
        }
    };

    // filtering
    p.setListLayout = function(setList) {
        this.gridLayoutIsActive = !setList;
        this.lobby.setBackgroundImagesForStreamPreview(setList);
        this.lobby.balanceBackground.visible = setList;

        if(this._pagination){
            if(this._pagination._numOfPages > 1){
                this._pagination.showPagination(!setList);
            }
        }

        this._machinesComponent.setGridView(!setList);
        this._machinesComponent.setShowOnlyAvailable(setList);

        this._backToLobbyButton.visible = setList;
        this._backToLobbyButton.x = setList ? 21 : -55;
        this._backToLobbyButton.y = setList ? -110 : -115;

        this._backToLobbyButton.x += this._backToLobbyButton._singleImgWidth*0.5;
        this._backToLobbyButton.y += this._backToLobbyButton._height*0.5;

        var skin = setList ? "btnBack2" : "btnBack";
        this._backToLobbyButton.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[skin]));

        this._locationLabelShape.visible = setList;
        this._topSectionBackground.visible = !setList;

        this._resetScrollComponent(setList);
        this._updateMachinesLabel();

        this.showAllMachinesButton.visible = !setList;
        this.showAvailableMachinesButton.visible = !setList;
        this.divider.visible = !setList;

        this.lobby.hideBackgroundsForFullScreenVideo(setList);

        this._locationLabel.x = setList ? 31 : 36;

        this.showStreamOverlay(setList);
    };

    p.onMachineClick = function(machine){// ovde mozda zapamtiti stanje masine tj isFreeToPlay ili setovati neki property u queuePanel
        alteastream.AbstractScene.SHOP_MACHINE = machine.machineInfo.house;
        this.lobby.checkIfHiddenMenuIsShown();
        this.setComponentWhenMachineisFreeToPlay(machine.isFreeToPlay);
        this.streamPreview.tryToSwitchStream(machine);
        if(alteastream.AbstractScene.HAS_COPLAY === true){
            this.queuePanel.multiPlayerControls.showCoinsWinComponent(machine.gameCode === "ticketcircus");
            this.queuePanel.multiPlayerControls.showMultiPlayerControls(!machine.isFreeToPlay);
        }
        if(this.gridLayoutIsActive === true){
            this.visible = false;
            this.setListLayout(true);
            this.queuePanel.enterQueue();
        }else{
            this.queuePanel.exitQueue();
            console.log("leaveQueue:: onMachineClick");
            this.queuePanel.leaveQueue(function () {
                _this.queuePanel.enterQueue();
            });
        }

        this._machinesComponent.clearActiveMachines();
        this.intervalUpdateLobby(false);
    };

    p.setComponentWhenMachineisFreeToPlay = function(isFreeToPlay){
        this._machinesComponent.visible = !isFreeToPlay;
        this._locationLabel.visible = !isFreeToPlay;

        if(alteastream.AbstractScene.HAS_COPLAY)
            this.queuePanel.multiPlayerControls.enable(!isFreeToPlay);// ovo mozda da se ne desava na click nego na onQueueEnter

        this.queuePanel.updateAvailabilityComponents(isFreeToPlay);
        this.queuePanel.bannerContainer.visible = !isFreeToPlay;
        this.machineCoinsAndPrizesInfo.visible = !isFreeToPlay;
        if(this._pagination){
            if(this._pagination._numOfPages > 1) {
                this._pagination.showPagination(!isFreeToPlay);
            }
        }
    };

    p.setBackButtonForEntranceCounter = function(setForCounter){
        var imageName = setForCounter === true ? "btnBackForEntranceCounter" : "btnBack";
        var button = this._backToLobbyButton;
        button.changeSkin(alteastream.Assets.getImageURI(alteastream.Assets.images[imageName]));
        button.x = setForCounter === true ? 900 : 111;
        button.y = setForCounter === true ? 701 : -87;
        var buttonTextX = setForCounter === true ? -40 : 4;
        var buttonTextY = setForCounter === true ? 4 : 2;
        button.centerText(buttonTextX, buttonTextY);
        this.lobby.balanceBackground.visible = false;
    };

    p.populateMachines = function(response){
        this._machinesComponent.populateAllMachines(response);
        var filterSelection = window.localStorage.getItem("filterSelection");
        if(filterSelection !== null) {
            if(filterSelection === "onlyAvailable"){
                this._showOnlyAvailable = true;
                this._showAvailableOrAllButtonsHandler();
            }
        }
    };

    p.intervalUpdateLobby = function(bool){// local ver
        if(bool === true){// local ver
            var requester = alteastream.Requester.getInstance();// local ver
            this._machinesComponent.updateThumbnails();// local ver
            //var that = this;// local ver
            //requester.getMachines(alteastream.AbstractScene.SHOP_MACHINE,function(response){// local ver
            requester.getMachinesGt(alteastream.AbstractScene.SHOP_MACHINE,function(response){// local ver
                _this._machinesComponent.setMachinesStatus(response);// local ver
            });// local ver
            this.thumbnailTimer = setInterval(function(){// local ver
                //console.log(" intervalUpdateLobby>>>>>>>>>>>>>>>>>>>");// local ver
                if(_this._canUpdateThumbs === true){// local ver
                    if(_this.gridLayoutIsActive === true){// local ver
                        _this._machinesComponent.updateThumbnails();// local ver
                    }// local ver
                }// local ver
                //requester.getMachines(alteastream.AbstractScene.SHOP_MACHINE,function(response){// local ver
                requester.getMachinesGt(alteastream.AbstractScene.SHOP_MACHINE,function(response){// local ver
                    _this._machinesComponent.setMachinesStatus(response);// local ver
                });// local ver
            },5000);// local ver
        }// local ver
        else// local ver
            clearInterval(this.thumbnailTimer);// local ver
    };// local ver
    //new socket
    /*// local ver
    p.intervalUpdateLobby = function(bool){
        if(bool === true){
            this._machinesComponent.updateThumbnails();
            //var that = this;
            this.thumbnailTimer = setInterval(function(){
                console.log(" intervalUpdateLobby>>>>>>>>>>>>>>>>>>>");
                if(_this._canUpdateThumbs === true){
                    if(_this.gridLayoutIsActive === true){
                        _this._machinesComponent.updateThumbnails();
                    }
                }
            },5000);
        }
        else
            clearInterval(this.thumbnailTimer);
    };
    */ // local ver

    p.intervalUpdateLounge = function(bool){
        //var that = this;
        if(bool === true)
            //thumbnailPropsTimer
            this.propsTimer = setInterval(function(){
                _this.updateLoungeComponents();
            },2000);
        else
            clearInterval(this.propsTimer);
    };

    /*p.intervalUpdateLounge = function(bool){ // todo mozda kasnije sa setTimeout
        //var that = this;
        if(bool === true){
            this.updateLoungeComponents();
            this.propsTimer = setTimeout(function(){
                _this.intervalUpdateLounge(true);
            },2000);
        }

        else
            clearTimeout(this.propsTimer);
    };*/

    p.updateLoungeComponents = function(){// local ver
        if(this.streamPreview._active){// local ver
            alteastream.Requester.getInstance().getCurrentMachine(this.streamPreview.currentStream,function(response){// local ver
                _this.machineCoinsAndPrizesInfo.updateProps(response);// local ver
                _this.queuePanel._updateAvailabilityComponents(response);// local ver
                // filtering // local ver
                //if(alteastream.QueuePanel.getInstance().queueExists === true)// local ver
                //return; // local ver
                //mozda uopste ne ovo sad kad je filtering:// local ver
                _this._machinesComponent.updateFromCurrentStream(response,_this.streamPreview.currentStream);// local ver
            });// local ver
        }// local ver
    };// local ver

    //new socket
    /*
    * biggestGameWin: 28
    isFreeToPlay: false
    is_available: true
    lastWin: 6
    name: "4008"
    state: 1
    status: 0
    status_code: 1
    tokensFired: 1176
    totalWin: 38
    */
    /* // local ver
    p.updateLoungeComponents = function(){
        if(this.streamPreview._active){
            alteastream.Requester.getInstance().getCurrentMachine(this.streamPreview.currentStream,function(response){
                _this.machineCoinsAndPrizesInfo.updateProps(response);
                //_this.streamPreview.updateProps(response);
            });
        }
    };
    */ // local ver

    p.resetIntervalUpdateLounge = function(){
        this.intervalUpdateLounge(false);
        this.intervalUpdateLounge(true);
        this.updateLoungeComponents();
    };

// static methods:
// public methods:
    p.focusIn = function(){
        this.visible = true;

/*        if(alteastream.AbstractScene.BACK_TO_HOUSE !== -1){
            var backgroundMusicIsMuted = window.localStorage.getItem('backgroundMusicIsMuted');
            createjs.Sound.muted = backgroundMusicIsMuted === "false";
            this.lobby.btnSoundHandler();
        }*/

        this.focusInRegular();
    };

    p.focusInRegular = function() {
        this._onFocusDone();
        this._restoreScrollPosition();
    };

    p.focusInAnimated = function() {
        this._onFocusDone();
    };

    p.showMachinesHideShape = function(bool) {
        if(this._pagination){
            if(this._pagination._numOfPages > 1) {
                this._pagination.showPagination(bool);
            }
            this._scroller.manageButtonsState();
        }
    };

    p.focusOut = function(objectIn){
        this.intervalUpdateLobby(false);
        this.showMachinesHideShape(false);
        objectIn.focusIn();
    };

    // filtering
    p.setMachinesAndScroll = function() {
        this._storeLocationName();
        this._updateMachinesLabel();
        //this._resetScrollComponent(false);
        this._machinesComponent.disableActiveMachines(false);
    };

    p.onMouseWheel = function(direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

/*    p.restoreScrollLevelAndSelectedMachine = function() {
        var scrollLevel = Number(window.localStorage.getItem('scrollLevel'));
        if(scrollLevel > 0){
            this._scroller.scrollByPageCurrentLevel = scrollLevel;
            this._scroller.scrollByPage();
        }
    };*/

    p._restoreScrollPosition = function() {// from gameplay return
        if(this._pagination){
            if(this._pagination._numOfPages > 1) {
                this._scroller.scrollByRowCurrentLevel = this._gridLayoutScrollLevel = Number(window.localStorage.getItem('scrollLevel'));
                this._setScrollPosition();
            }
        }
    };

    p._setScrollPosition = function () {// lobby always on back to lobby
        if(this._pagination) {
            if (this._pagination._numOfPages > 1) {
                this._scroller.scrollByRowCurrentLevel = this._gridLayoutScrollLevel;
                this._scroller.scrollByRow(false);
            }
        }
    };

    //StreamPreview callbacks
    //QueuePanel callbacks
    p.onQueueEntered = function(){
        this.lobby.currentMap.clickEnabled(false);
        this._soundNotify();
    };

    p.onStartGame = function(){// invalid token fix new
        this._backToLobbyButton.visible = false;
        this.queuePanel.btnStart.setDisabled(true);
        //var that = this;
        alteastream.Requester.getInstance().startGameStream(function(response){

            // temp roulette test
            //if(_this.lobby.selectedLocation.casinoName === "Roulette"){
            if(response.gameCode === "roulette"){//response.gamecode
                response.minStake = 0.20;
                response.maxStake = 5.00;
            }

            var resp = JSON.stringify(response);

            if(_this.lobby.mouseWheel)
                _this.lobby.mouseWheel.enable(false);

            _this.lobby.onStartGameStream();
            _this.intervalUpdateLobby(false);
            _this.lobby.socketCommunicator.disposeCommunication();

            _this.queuePanel.removeAllChildren();

            _this.streamPreview.dispose();
            _this.intervalUpdateLounge(false);

            var vid = document.getElementById("videoOutput");
            vid.parentNode.removeChild(vid);

            alteastream.Assets.stopAllSounds();

            setTimeout(function(){_this.goToMachine(resp);},1000);
        }, function () {
            _this._backToLobbyButton.visible = false;
            _this.queuePanel.btnStart.setDisabled(true);
            _this.backToLobbyButtonHandler();
            //new socket
            console.log("leaveQueue:: backToLobbyButtonHandler");
            _this.queuePanel.leaveQueue(function () {
                console.log("no socket? getMachines here ?? ");
                //_this.intervalUpdateLobby(true);// if back from entranceCounter?
            })
        })
    };

    p.goToMachine = function(response){
        //var gameDir = this.lobby.selectedLocation.casinoName === "Roulette" ? "/gameplayRoulette/" : "/gameplay/";
        //var gameDir = this._machinesComponent.getActiveMachine().gameCode === "roulette" ? "/gameplayRoulette/" : "/gameplay/";
        var gameDir = response.gameCode === "roulette" ? "/gameplayRoulette/" : "/gameplay/";
        var filterSelection = this._showOnlyAvailable === true ? "onlyAvailable" : "all";
        window.localStorage.setItem('backToHouse', String(alteastream.AbstractScene.BACK_TO_HOUSE));
        window.localStorage.setItem('machineParameters', response);// invalid token fix new
        window.localStorage.setItem('scrollLevel', String(this._gridLayoutScrollLevel));
        window.localStorage.setItem('backgroundMusicIsMuted', String(createjs.Sound.muted));
        window.localStorage.setItem('filterSelection', filterSelection);
        window.localStorage.setItem('hasCoplay', String(alteastream.AbstractScene.HAS_COPLAY));

        var qStr = "?usr="+alteastream.AbstractScene.GAME_TOKEN+"&serverUrl="+alteastream.AbstractScene.SERVER_URL+"&machine_name="+alteastream.AbstractScene.GAME_ID+"&game="+"rmgame"+"&gameCode="+alteastream.AbstractScene.GAME_CODE+"&gameName="+this.streamPreview.currentMachineName;
        //window.top.changeIFrameSrc(alteastream.AbstractScene.GAME_PROTOCOL+alteastream.AbstractScene.SERVER_IP+"/gameplay/"+qStr);
        window.parent.changeIFrameSrc(alteastream.AbstractScene.GAME_PROTOCOL+alteastream.AbstractScene.SERVER_IP+gameDir+qStr);
    };

    alteastream.LocationMachines = createjs.promote(LocationMachines,"Container");
})();