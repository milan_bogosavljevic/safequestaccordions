
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var MachinesComponent = function(rows, columns){
        this.Container_constructor();
        this.initMachinesContainer(rows, columns);
    };

    var p = createjs.extend(MachinesComponent,createjs.Container);
    var _this;

    p.MAX_NUMBER_OF_MACHINES = 70;
    p.MAX_NUMBER_OF_ACCORDEONS_PER_PAGE = 16;
    p._totalRows = 0;
    p._columnsPerPage = 0;
    p._rowsPerPage = 0;
    p._ySpacing = 0;
    p._xSpacing = 0;
    p._startPositionX = 0;
    p._startPositionY = 0;
    p._numOfActiveMachines = 0;
    p.showOnlyAvailable = false;
    p._startPositionYGrid = 4;
    p._startPositionYList = 3;
    p.acordions = null;
    p.selectedAcordion = null;
    p.newScrollLevels = [];
    p._noAvailableMachinesImg = null;

    p.initMachinesContainer = function (rows, columns) {
        console.log("MachinesComponent initialized:::");
        _this = this;
        this._columnsPerPage = columns;
        this._rowsPerPage = rows;
        this.setMachines();
        var noAvailableMachines =  this._noAvailableMachinesImg = alteastream.Assets.getImage(alteastream.Assets.images.noAvailableMachinesImg);
        noAvailableMachines.regX = noAvailableMachines.image.width + 0.5;
        //noAvailableMachines.regY = noAvailableMachines.image.height + 0.5;
        noAvailableMachines.mouseEnabled = false;
        noAvailableMachines.visible = false;
        this.addChild(noAvailableMachines);
    };

    MachinesComponent.getInstance = function() {
        return _this;
    };

    p.getStartPositionY = function (listView) {
        return listView === true ? this._startPositionYList : this._startPositionYGrid;
    };

    p.setMachines = function(){
        this._totalRows = Math.ceil(this.MAX_NUMBER_OF_MACHINES/this._columnsPerPage);
        this._startPositionX = 23;
        this._startPositionY = 4;
        this._xSpacing = 25;
        this._ySpacing = 25;
        this.addMachines();
    };

    p.setGridView = function (grid) {
        this._columnsPerPage = grid === true ? 4 : 1;
        this._rowsPerPage = grid === true ? 3 : 17;
        this._totalRows = Math.ceil(this.MAX_NUMBER_OF_MACHINES/this._columnsPerPage);
        this._startPositionX = grid === true ? 23 : 21
        this._startPositionY = this.getStartPositionY(!grid);
        this._xSpacing = grid === true ? 25 : 0;
        this._ySpacing = grid === true ? 25 : 7;
        this.rearrangeMachinesAsGrid(grid);
        this.updateMask();
    };

    p.rearrangeMachinesAsGrid = function(setGrid){
        var xCount = 0;
        var yCount = 0;
        for(var i = 0, j = this.MAX_NUMBER_OF_MACHINES; i < j; i++){
            var machine = this.machines[i];
            machine.setGridMachine(setGrid);
            machine.x = this._startPositionX + xCount * (machine.getWidth() + this._xSpacing);
            machine.y = this._startPositionY + yCount * (machine.getHeight() + this._ySpacing);
            xCount++;
            if(xCount === this._columnsPerPage){
                xCount = 0;
                yCount++;
            }
        }
    };

    p.updateMask = function(){
        var maskWidth = this.getWidth();
        var maskHeight = this.getMaskHeight();
        var mask = new createjs.Shape();
        mask.graphics.beginFill("#456e86").drawRect(this._startPositionX, this._startPositionY, maskWidth, maskHeight);
        this.mask = mask;
        this.mask.setBounds(this._startPositionX, this._startPositionY, maskWidth, maskHeight);
        //this.mask = mask;
    };

    p.animateMask = function (clickedAccordionIndex, expand) {
        var acordionH = this.selectedAcordion.tentImage.image.height;
        var topAccordionsH = expand === true ? acordionH + (acordionH * clickedAccordionIndex) : this.acordions.length * acordionH;
        var maskH = this.mask.getBounds().height;
        var onePercent = maskH/100;

        var downScale = (topAccordionsH/onePercent)/100;
        var scaleToAnimate = expand === true ? 1 : downScale;

        this.mask.scaleY = expand === true ? downScale : 1;
        if(expand === true){
            this.showMachines(true);
        }
        createjs.Tween.get(this.mask).to({scaleY:scaleToAnimate},1000).call(function (){
            if(expand === false){
                _this.showMachines(false);
            }
        });
    };

    p.addMachines = function () {
        this.machines = [];
        var xCount = 0;
        var yCount = 0;
        for(var i = 0, j = this.MAX_NUMBER_OF_MACHINES; i < j; i++){
            var machine = new alteastream.Machine();
            machine.x = this._startPositionX + xCount * (machine.getWidth() + this._xSpacing);
            machine.y = this._startPositionY + yCount * (machine.getHeight() + this._ySpacing);
            this.machines.push(machine);
            this.addChild(machine);
            machine.visible = false;

            xCount++;
            if(xCount === this._columnsPerPage){
                xCount = 0;
                yCount++;
            }
        }
    };

    p.addAcordions = function (acordions) {
        this.acordions = [];
        var startY = 29;
        for(var i = 0; i < acordions.length; i++){
            var acordion = new alteastream.Location(acordions[i]);
            var acordionH = acordion.tentImage.image.height;
            acordion.x = 862;
            acordion.y = i * acordionH + startY + (i * 2);
            acordion.on("click", function (){
                _this.acordionClickHandler(this);
            });
            acordion.on("mouseover", function (){
                this.highlight(true);
            });
            acordion.on("mouseout", function (){
                this.highlight(false);
            });
            this.addChild(acordion);
            this.acordions.push(acordion);
        }

        // todo naci resenje da se scroll aktivira odmah kada se pojave accordioni
        //this.setScrollLevelsForAcordions();
    };

    p.getAcordionByName = function (acordionName) {
        for(var i = 0; i < this.acordions.length; i++){
            if(this.acordions[i].name === acordionName){
                return this.acordions[i];
            }
        }
    };

    p.acordionClickHandler = function (clickedAcordio) {
        if(this.selectedAcordion !== clickedAcordio){
            alteastream.Lobby.getInstance().enterShop(clickedAcordio);
            //this.selectedAcordion = clickedAcordio;
            //this.animateMask(index, true);
            alteastream.LocationMachines.getInstance()._locationLabel.visible = true;
        }else{
            alteastream.LocationMachines.getInstance()._locationLabel.visible = false;
            this._closeAcordion();
        }
    };

    p.setScrollLevelsForAcordions = function () {
        if(this.acordions.length < this.MAX_NUMBER_OF_ACCORDEONS_PER_PAGE){
            //alteastream.LocationMachines.getInstance()._scroller.setToFirstPage(false);
            alteastream.LocationMachines.getInstance()._scroller.setScrollLevels([]);
        }else{
            var scrollLevels = [];
            var positionMultiplier = 0;
            for(var i = this.acordions.length - this.MAX_NUMBER_OF_ACCORDEONS_PER_PAGE; i >= 0; i--){
                scrollLevels.push(this.y - (positionMultiplier * this.acordions[i].tentImage.image.height));
                positionMultiplier++;
            }
            alteastream.LocationMachines.getInstance()._scroller.setScrollLevels(scrollLevels);
            alteastream.LocationMachines.getInstance()._scroller.setToFirstPage(false);
        }
    };

    p.showAcordions = function (show) {
        if(this.acordions[0].visible !== show){
            for(var i = 0; i < this.acordions.length; i++){
                this.acordions[i].visible = show;
            }
        }
    };

    p._closeAcordion = function () {
        this._noAvailableMachinesImg.visible = false;
        var acordionH = this.selectedAcordion.tentImage.image.height + 2;
        var counter = 1;
        var index = this.acordions.indexOf(this.selectedAcordion);
        for(var i = index+1; i < this.acordions.length; i++){
            this.acordions[i].y = this.acordions[index].y + (acordionH * counter);
            counter++;
        }
        this.selectedAcordion = null;
        this.showMachines(false);
        this.setScrollLevelsForAcordions();
    };

    p.setAcordionsLayout = function (listView) {
        if(!this.selectedAcordion){return;}
        this._noAvailableMachinesImg.visible = false;

        console.log("SET ACORDIONS");
        if(listView === true){
            this.showAcordions(false);
            return;
        }else {
            this.showAcordions(true);
        }

        var acordionH = this.selectedAcordion.tentImage.image.height;
        var acordionHalfH = acordionH * 0.5;
        var clickedAcIndex = _this.acordions.indexOf(this.selectedAcordion);
        var firstMachine = _this.machines[0];
        var lastMachine = _this.machines[_this.getNumberOfActiveMachines()-1] || _this.machines[0];// || fix for situation when there is no active machines
        var bottomYPos = lastMachine.y + lastMachine.getHeight() + acordionHalfH;
        var scrollLevels = _this.parent._scroller._scrollLevels;

        var hasScrollLevels = scrollLevels.length > 0;

        for(var j = 0; j < _this.newScrollLevels.length; j++){
            var level = _this.newScrollLevels[j];
            if(scrollLevels.indexOf(level) > -1){
                scrollLevels.splice(scrollLevels.indexOf(level), 1);
            }
        }

        scrollLevels[0] = 0;

        _this.newScrollLevels = [];
        var bottomAcordionsPositionCorrector = 2;
        var topAcordionsPositionCorrector = _this.acordions.indexOf(this.selectedAcordion);
        var levelToAdd;
        for(var i = 0; i < _this.acordions.length; i++){
            if(i <= clickedAcIndex){
                _this.acordions[i].y = firstMachine.y - 4 - (topAcordionsPositionCorrector * 4) - acordionHalfH - (acordionH * topAcordionsPositionCorrector);
                topAcordionsPositionCorrector--;
                if(hasScrollLevels){
                    levelToAdd = scrollLevels[0] + (acordionH+4);
                    scrollLevels.unshift(levelToAdd);
                    _this.newScrollLevels.push(levelToAdd);
                }
            }else{
                _this.acordions[i].y = bottomYPos + bottomAcordionsPositionCorrector;
                bottomYPos += acordionH;
                if(hasScrollLevels){
                    levelToAdd = scrollLevels[scrollLevels.length-1] - acordionH - bottomAcordionsPositionCorrector;
                    scrollLevels.push(levelToAdd);
                    _this.newScrollLevels.push(levelToAdd);
                }
                bottomAcordionsPositionCorrector += 2;
            }

            if(i === clickedAcIndex){
                if(this.getNumberOfActiveMachines() === 0){
                    this._noAvailableMachinesImg.x = this.acordions[i].x + this._noAvailableMachinesImg.image.width * 0.5;
                    this._noAvailableMachinesImg.y = this.acordions[i].y + acordionHalfH + 2;
                    this._noAvailableMachinesImg.visible = false;// todo temp je na false, da se ne bi nikad pojavljivala slika, treba da bude na true
                }
            }
        }

/*        if(!hasScrollLevels){
            this.y = (this.getStartPositionY() + acordionH) * (clickedAcIndex + 1);
        }*/

        if(!hasScrollLevels){
            if(window.localStorage.getItem('isMobile') === "yes"){
                var accScrollLevels = [(acordionH+4)*(1 + clickedAcIndex)];
                this.newScrollLevels = [(acordionH+4)*(1 + clickedAcIndex)];
                for(var n = 3; n < this.acordions.length; n++){
                    var slevel = accScrollLevels[accScrollLevels.length-1];
                    accScrollLevels.push(slevel - (acordionH+4));
                    this.newScrollLevels.push(slevel - (acordionH+4));
                }
                this.parent._scroller._scrollLevels = accScrollLevels;
                this.parent._scroller.setToFirstPage(true, 200);
            }else{
                this.y = (this.getStartPositionY() + acordionH) * (clickedAcIndex + 1);
            }
        }
    };

    p.showMachines = function (show) {
        for(var i = 0; i < this.getNumberOfActiveMachines(); i++){
            this.machines[i].showMachine(show);
        }
    };

    p.populateAllMachines = function(machines){
        this.machinesFromServer = machines;
        this.resetComponent(machines);
    };

    p.getMachines = function(){
        return this.machines;
    };

    p.getHeight = function () {
        var machineHeight = this.machines[0].getHeight();
        return this._rowsPerPage * (machineHeight + this._ySpacing);
    };

    p.getWidth = function () {
        var machineWidth = this.machines[0].getWidth();
        return this._columnsPerPage * (machineWidth + this._xSpacing);
    };

    p.getMaskHeight = function() {
        var containerHeight = this.getHeight();
        return containerHeight - this._ySpacing;
    };

    p.getNumberOfPages = function () {
        return Math.ceil(this._totalRows/this._rowsPerPage);
    };

    p.getMaxNumberOfPages = function () {
        var machinesPerPage = this._rowsPerPage*this._columnsPerPage
        return Math.ceil(this.MAX_NUMBER_OF_MACHINES/machinesPerPage);
    };

    p.getMachineHeight = function() {
        var machineHeight = this.machines[0].getHeight();
        return (machineHeight + this._ySpacing);
    };

    p.disableActiveMachines = function(bool){
        var numOfMachines = this.getNumberOfActiveMachines();
        for(var i = 0;i<numOfMachines;i++){
            //this.machines[i].disableMouseListeners(bool);
            this.machines[i].mouseEnabled = !bool;
        }
    };

/*    p.disableActiveMachines = function(bool){
        var areaFrom = this.mask.getBounds().y;
        var areaTo = this.mask.getBounds().height;
        var parentX = this.parent.x;
        var parentY = this.parent.y;
        var numOfMachines = this.getNumberOfActiveMachines();
        for (var i = 0; i < numOfMachines; i++){
            var topY = this.machines[i].localToGlobal(parentX, parentY).y;
            var bottomY = topY + this.machines[i].getHeight();
            this.machines[i].mouseEnabled = false;
            //console.log("machine enabled " + i);
            //if(this.machines[i].visible === true){
            console.log("topY " + topY + " from " + areaFrom + " to " + areaTo);
                if(topY > areaFrom && topY < areaTo){
                    this.machines[i].mouseEnabled = !bool;
                    console.log("machine enabled if " + i);
                }else if(bottomY.y > areaFrom && bottomY.y < areaTo){
                    this.machines[i].mouseEnabled = !bool;
                    console.log("machine enabled else machine" + i);
                }
            //}
        }
    };*/

    p.clearActiveMachines = function(){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            this.machines[i].resetDisplay();
        }
    };

    p.updateThumbnails = function(){
        var areaFrom = this.mask.getBounds().y;
        var areaTo = this.mask.getBounds().height;
        var parentX = this.parent.x;
        var parentY = this.parent.y;
        var numOfMachines = this.getNumberOfActiveMachines();
        for (var i = 0; i < numOfMachines; i++){
            var topY = this.machines[i].localToGlobal(parentX, parentY).y;
            var bottomY = topY + this.machines[i].getHeight();
            if(this.machines[i].visible === true){
                if(topY > areaFrom && topY < areaTo){
                    this.machines[i].updateThumbnail();
                }else if(bottomY.y > areaFrom && bottomY.y < areaTo){
                    this.machines[i].updateThumbnail();
                }
            }
        }
    };

    p.updateFromCurrentStream = function(response,currentStream){
        var machine = this.getMachineById(currentStream);
        if(machine) // nema ni jedna slobodna masina?
            if(machine.getState() === machine.STATE_NORMAL){ //symbols
                if(machine.isFreeToPlay !== response.isFreeToPlay){
                    machine.isFreeToPlay = response.isFreeToPlay;// temp patch to prevent double calls
                    //alteastream.Requester.getInstance().getMachines(alteastream.AbstractScene.SHOP_MACHINE,function(response){

                    var method = "getMachinesBy" + alteastream.Lobby.getInstance().selectedLocation.category;
                    alteastream.Requester.getInstance()[method](alteastream.AbstractScene.SHOP_MACHINE,function(response){
                        _this.setMachinesStatus(response);
                    });

                    /*alteastream.Requester.getInstance().getMachinesGt(alteastream.AbstractScene.SHOP_MACHINE,function(response){
                        _this.setMachinesStatus(response);
                    });*/
                }else{
                    machine.setStatus(response);
                }
            }
    };

    p.cacheMachines = function(bool){
        for(var i = 0,j=this._numOfActiveMachines;i<j;i++){
            this.machines[i].doCache(bool);
        }
    };

    //new socket on notify
    p.setMachinesStatus = function(machine){
        var machineToUpdate = this.getMachineById(machine.machineIndex);
        if(machineToUpdate){
            var newMachine = {};

            newMachine.isOnline = machine.hasOwnProperty("isOnline") ? machine.isOnline : machineToUpdate.isOnline;
            newMachine.queueSize = machine.hasOwnProperty("size") ? machine.size : machineToUpdate.queueNumber.text;
            newMachine.statusCode = machine.hasOwnProperty("statusCode") ? machine.statusCode : machineToUpdate.statusCode;
            newMachine.isInAutoplay = machine.hasOwnProperty("isInAutoplay") ? machine.isInAutoplay : machineToUpdate.isInAutoplay;
            newMachine.isFreeToPlay = machine.hasOwnProperty("isFreeToPlay") ? machine.isFreeToPlay : machineToUpdate.isFreeToPlay;
            newMachine.stakename = machine.hasOwnProperty("stakename") ? machine.stakename : machineToUpdate.stakePolicy.text;
            newMachine.payoutname = machine.hasOwnProperty("payoutname") ? machine.payoutname : machineToUpdate.payoutPolicy.text;

            machineToUpdate.setStatus(newMachine);
        }
        for(var p in this.machinesFromServer){
            var machineFromPreviousResponse = this.machinesFromServer[p];
            if(machineFromPreviousResponse.machineIndex === machine.machineIndex){
                machineFromPreviousResponse.isFreeToPlay = machine.hasOwnProperty("isFreeToPlay") ? machine.isFreeToPlay : machineFromPreviousResponse.isFreeToPlay;
                machineFromPreviousResponse.isInAutoplay = machine.hasOwnProperty("isInAutoplay") ? machine.isInAutoplay : machineFromPreviousResponse.isInAutoplay;
                machineFromPreviousResponse.isOnline = machine.hasOwnProperty("isOnline") ? machine.isOnline : machineFromPreviousResponse.isOnline;
                machineFromPreviousResponse.queueSize = machine.hasOwnProperty("size") ? machine.size : machineFromPreviousResponse.queueSize;
                machineFromPreviousResponse.statusCode = machine.hasOwnProperty("statusCode") ? machine.statusCode : machineFromPreviousResponse.statusCode;
                machineFromPreviousResponse.stakename = machine.hasOwnProperty("stakename") ? machine.stakename : machineFromPreviousResponse.stakename;
                machineFromPreviousResponse.payoutname = machine.hasOwnProperty("payoutname") ? machine.payoutname : machineFromPreviousResponse.payoutname;
            }
        }
        if(this.showOnlyAvailable === true){
            this.resetComponent(this.machinesFromServer);
        }
    };

    p.getActiveMachine = function(){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            if(this.machines[i].selected === true)
                return this.machines[i];
        }
    };

    p.getMachineById = function(id){
        for(var i = 0;i<this._numOfActiveMachines;i++){
            if(this.machines[i].id === id){
                return this.machines[i];
            }
        }
    };

    // filtering
    p.resetComponent = function(machines) {
        //console.log("RESET COMPONENT");
        this._numOfActiveMachines = Object.keys(machines).length;
        this.numOfAvailableMachines = 0;
        var machinesToShow = [];
        var key;
        var machine;
        var activeMachine = this.getActiveMachine();
        if(this.showOnlyAvailable === true){
            for(var s = 0, t = this._numOfActiveMachines; s < t; s++){
                key = Object.keys(machines)[s];
                machine = machines[key];
                if(machine.isFreeToPlay === true && machine.isOnline === true){
                    if(!((parseInt("00001000",2) & machine.statusCode) > 0)){
                        if(!((parseInt("00000010",2) & machine.statusCode) > 0)){
                            if(machine.isInAutoplay === false){
                                machinesToShow.push(machine);
                                this.numOfAvailableMachines++;
                            }
                        }
                    }
                }
            }
        }else{
            for(var u = 0, x = this._numOfActiveMachines; u < x; u++){
                key = Object.keys(machines)[u];
                machine = machines[key];
                machinesToShow.push(machine);
                this.numOfAvailableMachines++;
            }
        }
        var numberOfMachinesToShow = machinesToShow.length;
        for(var i = 0, j = this.MAX_NUMBER_OF_MACHINES; i < j; i++){
            if(i < numberOfMachinesToShow){
                this.machines[i].setMachineInfo(machinesToShow[i]);
            }else{
                this.machines[i].showMachine(false);
                this.machines[i].resetMachineProperties();
            }
        }
        if(activeMachine){activeMachine.selectMachine(true);}
        var numberOfMachinesToDivide = this.showOnlyAvailable === true ? this.numOfAvailableMachines : this._numOfActiveMachines;
        this._totalRows = Math.ceil(numberOfMachinesToDivide/this._columnsPerPage);
        this.parent._updateMachinesLabel();

        if(this.selectedAcordion){
            this.showMachines(true)
        }
    };

    // filtering
    p.setShowOnlyAvailable = function(onlyAvailable){
        this.showOnlyAvailableOrAllMachines(onlyAvailable);
        var streamPreview = alteastream.StreamPreview.getInstance();
        for(var i = 0; i < this._numOfActiveMachines; i++){
            if(this.machines[i].id === streamPreview.currentStream){
                this.machines[i].selectMachine(true);
            }
        }
    };

    p.showOnlyAvailableOrAllMachines = function(onlyAvailable) {
        this.showOnlyAvailable = onlyAvailable;
        if(this.machinesFromServer){
            this.resetComponent(this.machinesFromServer);
        }
    };

    p.getColumnsPerPage = function() {
        return this._columnsPerPage;
    };

    p.getRowsPerPage = function() {
        return this._rowsPerPage;
    };

    // filtering
    p.getNumberOfActiveMachines = function() {
        return this.showOnlyAvailable === true ?  this.numOfAvailableMachines : this._numOfActiveMachines;
    };

    p.setCurrency = function (currency) {
        for(var i = 0; i < this.machines.length; i++){
            this.machines[i].setCurrency(currency);
        }
    };

    p.adjustMobile = function(){
        var numOfMachines = this.MAX_NUMBER_OF_MACHINES;
        var font = "13px HurmeGeometricSans3";
        var upperTxtYPos = 10;
        var yCount = 0;
        this.MAX_NUMBER_OF_ACCORDEONS_PER_PAGE = 5;
        this._startPositionX = 6;
        this._startPositionY = 80;
        this._startPositionYList = 137;
        this._startPositionYGrid = 80;
        for(var i = 0; i < numOfMachines; i++) {
            var machine = this.getChildAt(i);

            machine.x = this._startPositionX;
            machine.y = this._startPositionY + yCount * (machine.getHeight() + this._ySpacing);
            yCount++;

            machine.machineNameLabel.font = font;
            machine.machineNameLabel.x = 30;
            machine.machineNameLabel.y = 174;

            machine._nameText.font = font;
            machine._nameText.x = machine.machineNameLabel.x + machine.machineNameLabel.getBounds().width + 5;
            machine._nameText.y = 174;

            machine.availabilityLabel.font = font;
            machine.availabilityLabel.x = 187;
            machine.availabilityLabel.y = 174;
            machine.availabilityLabel.originalXPosition = machine.availabilityLabel.x;

            machine.queueNumber.font = font;
            machine.queueNumber.x = machine.availabilityLabel.x;
            machine.queueNumber.y = machine.availabilityLabel.y;

            machine._availabilityImage.scale = 0.8;
            machine._availabilityImage.x = 277;
            machine._availabilityImage.y = 162;

            machine.spinner.x = 155;
            machine.spinner.y = 15;

            machine.frame.y = 0;

            machine.thumbScaleX = 0.633;
            machine.thumbScaleY = 0.58;
            //machine.thumb.y = 13;//7

            this._startPositionYList = 0;
            this._startPositionYGrid = 0;
        }

        this.setGridView = function (grid) {
            this._columnsPerPage = grid === true ? 3 : 1;
            this._rowsPerPage = grid === true ? 2 : 7;
            this._totalRows = Math.ceil(this.MAX_NUMBER_OF_MACHINES/this._columnsPerPage);
            this._xSpacing = grid === true ? 10 : 0;
            this._ySpacing = grid === true ? 10 : 5;
            this._startPositionY = grid === true ? 80 : 137;
            this.rearrangeMachinesAsGrid(grid);
            this.updateMask();
        };

        this.addAcordions = function (acordions) {
            console.log("ADD ACCORDIONS");
            this.acordions = [];
            var startY = 110;
            for(var i = 0; i < acordions.length; i++){
                var acordion = new alteastream.Location(acordions[i]);
                var acordionH = acordion.tentImage.image.height;
                acordion.x = 472;
                acordion.y = i * acordionH + startY + (i * 2);
                acordion.on("click", function (){
                    _this.acordionClickHandler(this);
                });
                this.addChild(acordion);
                this.acordions.push(acordion);
            }

            // todo naci resenje da se scroll aktivira odmah kada se pojave accordioni
            //this.setScrollLevelsForAcordions();
        };

        this.setGridView(true);
    }

    alteastream.MachinesComponent = createjs.promote(MachinesComponent,"Container");
})();