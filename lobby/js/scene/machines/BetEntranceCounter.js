
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var BetEntranceCounter = function(){
        this.AbstractCounter_constructor();
        this.initBetEntranceCounter();
    };

    var p = createjs.extend(BetEntranceCounter,alteastream.AbstractCounter);

    var _this;

    p.initBetEntranceCounter = function () {
        console.log("BetEntranceCounter initialized:::");
        _this = this;
        this.strokeTickness = 12;
        this.counterRadius = 100;
        this.degree = 360;
        this.colors = ["#ffcc00", "#ff0000"];
    };

    BetEntranceCounter.getInstance = function() {
        return _this;
    };

    // private methods
    p._updateCounter = function(count){
        if(count>-1)
            this.confirmEnterCounter.text = count;
        else{
            this.clearCounterInterval();
            alteastream.SocketCommunicatorLobby.getInstance().doBetting(false);
        }
    };

    // public methods
    p.setLayout = function(){
        var shapeW = 233;
        var shapeH = 844;

        var labelShape = this._labelShape = new createjs.Shape();
        labelShape.graphics.beginFill("#000000").drawRect(0, 0, 234, 50, 4);
        labelShape.alpha = 0.7;
        labelShape.cache(0, 0, shapeW, 50);
        labelShape.x = 21;
        labelShape.y = -56;
        this.addChild(labelShape);

        var counterShape = this.counterShape = new createjs.Shape();
        counterShape.graphics.beginFill("#000000").drawRect(0, 0, shapeW, shapeH, 5);
        counterShape.cache(0, 0, shapeW, shapeH);
        counterShape.size = {width:shapeW, height:shapeH, roundness:5};
        counterShape.x = 21;
        counterShape.y = 3;
        counterShape.mouseEnabled = false;
        this.addChild(counterShape);
        counterShape.alpha = 0.7;

        var circleX = 137;

        var counterCircleBackground = this.counterCircleBackground = new createjs.Shape();
        counterCircleBackground.graphics.setStrokeStyle(12).beginStroke("#555153").arc(0, 0, 100, 0, (Math.PI/180) * 360).endStroke();
        counterCircleBackground.x = circleX;
        counterCircleBackground.y = 140;
        counterCircleBackground.mouseEnabled = false;
        this.addChild(counterCircleBackground);

        var counterCircle = this.counterCircle = new createjs.Shape();
        counterCircle.rotation = -90;
        counterCircle.x = circleX;
        counterCircle.y = counterCircleBackground.y;
        counterCircle.mouseEnabled = false;
        this.addChild(counterCircle);

        this._createText("confirmEnterText","CONFIRM IN:","bold 16px HurmeGeometricSans3", "#a0a0a0",{x:circleX,y:-29,textAlign:"center",textBaseline:"middle"});//1172
        this._createText("confirmEnterCounter","0","78px HurmeGeometricSans3", "#ffffff",{x:circleX,y:115,textAlign:"center",textBaseline:"middle"});//1332
        this._createText("secondsText","SECONDS","24px HurmeGeometricSans3", "#ffffff",{x:circleX,y:180,textAlign:"center",textBaseline:"middle"});//1332
        this._createText("stakeLabel","CO-PLAY STAKE\n(EUR)","bold 22px HurmeGeometricSans3", "#ffffff",{x:circleX,y:484,textAlign:"center",textBaseline:"middle"});
        this._createText("infoText","START\nPARTICIPATING!","bold 22px HurmeGeometricSans3", "#ffffff",{x:circleX,y:785,textAlign:"center",textBaseline:"middle"});//1332
        this.infoText.lineHeight = 25;
    };

    p.quitCounter = function() {
        this.show(false);
        this.clearCounterInterval();
    };

    p.adjustMobile = function(){
        var yCorrector = 12;

        var labelShape = this._labelShape;
        labelShape.uncache();
        labelShape.graphics.beginFill("#000000").drawRect(0, 0, 140, 45);
        labelShape.alpha = 0.7;
        labelShape.cache(0, 0, 140, 45);
        labelShape.x = 6;
        labelShape.y = 20 + yCorrector;

        var counterShape = this.counterShape;
        counterShape.uncache();
        counterShape.graphics.clear();
        counterShape.graphics.beginFill("#000000").drawRect(0, 0, 140, 331);
        counterShape.cache(0, 0, 140, 331);
        counterShape.size = {width:140, height:331};
        counterShape.x = 6;
        counterShape.y = 77 + yCorrector;
        counterShape.mouseEnabled = false;

        this.counterRadius = 36;
        this.strokeTickness = 5;
        var counterCircleBackground = this.counterCircleBackground;
        counterCircleBackground.graphics.clear();
        counterCircleBackground.graphics.setStrokeStyle(this.strokeTickness).beginStroke("#555153").arc(0, 0, this.counterRadius, 0, (Math.PI/180) * 360).endStroke();
        counterCircleBackground.x = counterShape.x + 70;
        counterCircleBackground.y = counterShape.y + 45;

        this.counterCircle.x = counterCircleBackground.x;
        this.counterCircle.y = counterCircleBackground.y;

        this.confirmEnterText.font = "14px HurmeGeometricSans3";
        this.confirmEnterText.x = labelShape.x + 70;
        this.confirmEnterText.y = labelShape.y + 22;

        this.confirmEnterCounter.font = "26px HurmeGeometricSans3";
        this.confirmEnterCounter.x = this.confirmEnterText.x;
        this.confirmEnterCounter.y = counterCircleBackground.y - 3;

        this.secondsText.font = "9px HurmeGeometricSans3";
        this.secondsText.x = this.confirmEnterCounter.x;
        this.secondsText.y = counterCircleBackground.y + 13;

        this.stakeLabel.font = "12px HurmeGeometricSans3";
        this.stakeLabel.x = this.confirmEnterText.x;
        this.stakeLabel.y = 266;

        this.infoText.font = "12px HurmeGeometricSans3";
        this.infoText.x = this.confirmEnterText.x;
        this.infoText.y = this.confirmEnterText.y + 340;
        this.infoText.lineHeight = 15;
    }


    alteastream.BetEntranceCounter = createjs.promote(BetEntranceCounter,"AbstractCounter");
})();