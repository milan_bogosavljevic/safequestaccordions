
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var PlayerActivityPanel = function(){
        this.Container_constructor();
        this._initPlayerActivityPanel();
    };

    var p = createjs.extend(PlayerActivityPanel,createjs.Container);

    // private properties
    p._txtFields = null;
    p._response = null;
    p._numberOfPages = 0;
    p._rowsPerPage = 0;
    p._tableContent = null;
    p._tableContentStartYPos = null;
    p._numberOfRows = null;
    p._backgroundShapeW = null;
    p._backgroundShapeH = null;
    p._scroller = null;
    // public properties
    p.headerTexts = null;
    p.backgroundShapes = null;
    p.labels = null;

    var _this;

    // constructor
    p._initPlayerActivityPanel = function () {
        console.log("Activity Initialized");
        _this = this;
        this._tableContent = new createjs.Container();
        this._tableContent.x = 111;
        this._tableContent.y = this._tableContentStartYPos = 329;
        var background = alteastream.Assets.getImage(alteastream.Assets.images.infoBackground);
        this.addChild(background, this._tableContent);

        var mainLabel = new createjs.Text("Activity Log","bold 62px Roboto","#ffffff").set({textAlign:"left", textBaseline:"middle", x:111, y:150, mouseEnabled:false});
        this.addChild(mainLabel);

        var textSrc = alteastream.Assets.getText(alteastream.Assets.texts.infoText8);
        var infoText = new createjs.Text(textSrc,"18px Roboto","#ffffff").set({textAlign:"left", textBaseline:"middle", x:111, y:225, mouseEnabled:false});
        this.addChild(infoText);

        var supportLink = this.supportLink = new createjs.Text("support.","18px Roboto","#d60168").set({textAlign:"center", textBaseline:"middle", x:695, y:225});
        supportLink.cursor = "pointer";

        var area = new createjs.Shape();
        area.graphics.beginFill("#fff").drawRect(-50, -25, 100, 50);
        supportLink.hitArea = area;

        this.addChild(supportLink);

        this.headerTexts = [mainLabel, infoText, supportLink];

        this._scroller = new alteastream.Scroller(this._tableContent);
        this._scroller.setScrollType("scrollByRow");
        this._scroller.setScrollDirection("y");
    };

    // private methods
    p._setMask = function() {
        var tableMask = new createjs.Shape();
        tableMask.graphics.beginFill("#ffffff").drawRect(this._tableContent.x, this._tableContent.y, this._backgroundShapeW, (this._backgroundShapeH * this._rowsPerPage));
        this._tableContent.mask = tableMask;
    };

    p._setTableBackground = function(numberOfRows) {
        this.backgroundShapes = [];
        var shapeW = this._backgroundShapeW = 1692;
        var shapeH = this._backgroundShapeH = 57;
        var numberOfTableRows = numberOfRows < (this._rowsPerPage) ? numberOfRows : numberOfRows;//+1 for labels
        for(var i = 0; i < numberOfTableRows; i++){
            var color = i%2 === 0 ? "#141210" : "#191815";
            var shape = new createjs.Shape();
            shape.graphics.beginFill(color).drawRect(0, 0, shapeW, shapeH);
            shape.cache(0, 0, shapeW, shapeH);
            shape.mouseEnabled = false;
            shape.y = (i * shapeH);
            this._tableContent.addChild(shape);
            this.backgroundShapes.push(shape);
        }
    };

    p._setScrollLevels = function() {
        var scrollLevels = [];
        var numOfLevels = this._numberOfRows - this._rowsPerPage + 1;
        for(var i = 0; i < numOfLevels; i++){
            scrollLevels.push((i * this._backgroundShapeH * -1) + this._tableContentStartYPos);
        }
        this._scroller.setScrollLevels(scrollLevels);
    };

    p._setLabels = function() {
        var labelsY = 300;
        var spacing = 242;
        var font = "18px Roboto";
        var color = "#a7a7a6";
        var currency = this.parent.currency;
        var machineName = new createjs.Text("MACHINE NAME",font,color).set({textAlign:"center", textBaseline:"middle", x:232, y:labelsY, mouseEnabled:false});
        var startTime = new createjs.Text("START TIME",font,color).set({textAlign:"center", textBaseline:"middle", x:machineName.x + spacing, y:labelsY, mouseEnabled:false});
        var endTime = new createjs.Text("END TIME",font,color).set({textAlign:"center", textBaseline:"middle", x:startTime.x + spacing, y:labelsY, mouseEnabled:false});
        var totalBet = new createjs.Text("TOTAL BET ("+currency+")",font,color).set({textAlign:"center", textBaseline:"middle", x:endTime.x + spacing, y:labelsY, mouseEnabled:false});
        var totalWin = new createjs.Text("TOTAL WIN ("+currency+")",font,color).set({textAlign:"center", textBaseline:"middle", x:totalBet.x + spacing, y:labelsY, mouseEnabled:false});
        var payout = new createjs.Text("PAYOUT (%)",font,color).set({textAlign:"center", textBaseline:"middle", x:totalWin.x + spacing, y:labelsY, mouseEnabled:false});
        var revenue = new createjs.Text("REVENUE ("+currency+")",font,color).set({textAlign:"center", textBaseline:"middle", x:payout.x + spacing, y:labelsY, mouseEnabled:false});

        this.addChild(machineName, startTime, endTime, totalBet, totalWin, payout, revenue);
        this.labels = [machineName, startTime, endTime, totalBet, totalWin, payout, revenue];
    };

    p._populateHistory = function(response) {
        response = response.reverse();
        var labelsY = -29;
        var spacing = 242;
        var font = "18px Roboto";
        var grey = "#a7a7a6";
        var white = "#ffffff";
        var yellow = "#dfdf2d";
        var red = "#d43a6a";
        var green = "#2dfcac";
        var machineName = new createjs.Text("",font,white).set({textAlign:"center", textBaseline:"middle", lineHeight:57, x:121, y:labelsY, mouseEnabled:false});
        var startTime = new createjs.Text("",font,grey).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:machineName.x + spacing, y:labelsY, mouseEnabled:false});
        var endTime = new createjs.Text("",font,grey).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:startTime.x + spacing, y:labelsY, mouseEnabled:false});
        var totalBet = new createjs.Text("",font,yellow).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:endTime.x + spacing, y:labelsY, mouseEnabled:false});
        var totalWin = new createjs.Text("",font,red).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:totalBet.x + spacing, y:labelsY, mouseEnabled:false});
        var payout = new createjs.Text("",font,white).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:totalWin.x + spacing, y:labelsY, mouseEnabled:false});
        var revenue = new createjs.Text("",font,green).set({textAlign:"center", textBaseline:"middle",lineHeight:57, x:payout.x + spacing, y:labelsY, mouseEnabled:false});

        this._txtFields = [machineName, startTime, endTime, totalBet, totalWin, payout, revenue];
        this._tableContent.addChild(machineName, startTime, endTime, totalBet, totalWin, payout, revenue);

        var rowsToPopulate = response.length;
        this._manageTableContent(0, rowsToPopulate);
    };

    p._manageTableContent = function(start,end){
        for(var i = start; i < end; i++){
            var row = this._response[i];
            var keyCounter = 0;
            for(var key in row){
                var text = row[key];
                if(key === "startTime" || key === "endTime"){
                    var endOfText = text.indexOf(".");
                    text = text.substring(0, endOfText)
                }
                if(key === "totalBet" || key === "totalWin" || key === "revenue"){
                    var value = Number(row[key]);
                    text = Math.min(value/1000).toFixed(2);
                }
                if(key === "payout"){
                    var value = Number(row[key]);
                    text = value.toFixed(2);
                }
                this._txtFields[keyCounter].text += "\n" + text;
                keyCounter++;
            }
        }
    };

    p._linkClickHandler = function() {
        // todo uncomment when support page is done
        //window.open("http://"+alteastream.AbstractScene.SERVER_IP + "/support.html");
    };

    p._linkOverHandler = function() {
        _this.supportLink.color = "#dfdf2d"
    };

    p._linkOutHandler = function() {
        _this.supportLink.color = "#d60168"
    };

    // public methods
    p.activateSupportLinkListeners = function(activate) {
        var setListener = activate === true ? "addEventListener" : "removeEventListener";
        this.supportLink[setListener]("click", this._linkClickHandler);
        var isMobile = window.localStorage.getItem('isMobile');
        if(isMobile === "not"){
            this.supportLink[setListener]("mouseover", this._linkOverHandler);
            this.supportLink[setListener]("mouseout", this._linkOutHandler);
        }
    };

    p.setPanel = function(response) {
        this._response = response;
        this._rowsPerPage = 11;
        this._numberOfPages = Math.ceil(response.length/this._rowsPerPage);
        this._numberOfRows = response.length;

        this._setTableBackground(this._numberOfRows);
        this._setLabels();
        this._populateHistory(response);
        this._setScrollLevels();
        this._setMask();
    };

    p.setToFirstPage = function() {
        if(this._numberOfPages > 0){
            this._scroller.setToFirstPage(false);
        }
    };

    p.preventClickThrough = function(bool){
        var method = bool === true? "on":"off";
        this[method]("click",function noClick(e){});
    };

    p.onMouseWheel = function(direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    p.onSwipe = function (direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    p.adjustMobile = function(){
        var mainLabel = this.headerTexts[0];
        mainLabel.font = "bold 31px Roboto";
        mainLabel.x = 55;
        mainLabel.y = 75;

        var infoText = this.headerTexts[1];
        infoText.font = "14px Roboto";
        infoText.x = 55;
        infoText.y = 112;

        var supportLink = this.supportLink;
        supportLink.font = "14px Roboto";
        supportLink.x = 512;
        supportLink.y = 112;

        var shapeW = this._backgroundShapeW = 846;
        var shapeH = this._backgroundShapeH = 28;

        for(var i = 0; i < this.backgroundShapes.length; i++){
            var color = i%2 === 0 ? "#141210" : "#191815";
            var shape = this.backgroundShapes[i];
            shape.uncache();
            shape.graphics.clear();
            shape.graphics.beginFill(color).drawRect(0, 0, shapeW, shapeH);
            shape.cache(0, 0, shapeW, shapeH);
            shape.y = i * shapeH;
        }

        var labelsY = 150;
        var spacing = 121;
        var labelsStartX = 116;
        for(var j = 0; j < this.labels.length; j++){
            var label = this.labels[j];
            label.font = "12px Roboto";
            label.x = labelsStartX + (j * spacing);
            label.y = labelsY;
        }

        for(var k = 0; k < this._txtFields.length; k++){
            var field = this._txtFields[k];
            field.font = "12px Roboto";
            field.lineHeight = shapeH;
            // todo ne kapiram zasto ovo ne radi
            /*            field.x = labelsStartX + (k * spacing);
                        field.y = labelsY;*/
            field.x = 60 + (k * spacing);
            field.y = -12;
        }

        this._tableContent.x = 55;
        this._tableContent.y = this._tableContentStartYPos = 164;

        this._setMask();
        this._setScrollLevels();
    };

    alteastream.PlayerActivityPanel = createjs.promote(PlayerActivityPanel,"Container");
})();