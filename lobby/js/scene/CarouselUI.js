
this.alteastream = this.alteastream || {};
(function(){
    "use strict";

    var CarouselUI = function(maxIndex , lobby){
        this.Container_constructor();
        this.initCarouselUI(maxIndex,lobby);
    };

    var p = createjs.extend(CarouselUI,createjs.Container);

    // private properties
    p._currentIndex = null;
    p._maxIndex = null;
    p._currentIndexText = null;
    // constructor
    p.initCarouselUI = function (maxIndex , lobby) {
        var that = this;

        this._currentIndex = 0;
        this._maxIndex = maxIndex;

        var leftButton = alteastream.Assets.getImage(alteastream.Assets.images.leftButton);
        leftButton.x = 0;
        leftButton.y = 0;
        this.addChild(leftButton);

        var rightButton = alteastream.Assets.getImage(alteastream.Assets.images.rightButton);
        rightButton.x = 250;
        rightButton.y = 0;
        this.addChild(rightButton);

        leftButton.addEventListener("click" , function () {
            if(that._currentIndex > 0){
                that._currentIndex--;
                that._updateCurrentIndexText();
                lobby.animateSwitch(that._currentIndex , "left");
            }
        });

        rightButton.addEventListener("click" , function () {
            if(that._currentIndex < that._maxIndex){
                that._currentIndex++;
                that._updateCurrentIndexText();
                lobby.animateSwitch(that._currentIndex , "right");
            }
        });

        var currentHouseIndexText = this._currentIndexText = new alteastream.BitmapText("" , "36px HurmeGeometricSans3", "#fff",{textAlign:"right",x:140,y:20});
        var numberOfHousesText = new alteastream.BitmapText("/ "+(this._maxIndex+1) , "36px HurmeGeometricSans3", "#fff",{textAlign:"left",x:155,y:20});

        this.addChild(currentHouseIndexText,numberOfHousesText);

        this._updateCurrentIndexText(); // pravi problem ako se bilo sta setuje od align-a sem left , zato stoji ovaj poziv
    };
    // private methods
    p._updateCurrentIndexText = function(){
        this._currentIndexText.setText(this._currentIndex+1);
    };
    // public methods
    p.enableClick = function(bool){
        this.mouseEnabled = bool;
    };
    
    alteastream.CarouselUI = createjs.promote(CarouselUI,"Container");
})();