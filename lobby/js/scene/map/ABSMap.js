
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var ABSMap = function(lobby) {
        this.Container_constructor();
        this.initialize_ABSMap(lobby);
    };
    var p = createjs.extend(ABSMap, createjs.Container);
    var _this = null;
    // static properties:
    // events:
    // public properties:
    p.name = "";
    p.carousel = null;
    // private properties:
    // constructor:
    p.initialize_ABSMap = function(lobby) {
        _this = this;
        this.lobby = lobby;
        this.addAllListeners();
        this.mouseSubject = new alteastream.MouseSubject();

        this.selectedLocation = null;
    };
    // private methods
    p._onMouseAction = function(e){
        var location = e.target.parent;
        _this.mouseSubject.notifyObservers(e.type,location);

        switch(e.type){
            case "mouseover":
                if(location.allow === true)
                    location.highlight(true);
                break;
            case "mouseout":
                if(location.allow === true)
                    location.highlight(false);
                break;
            case "click":
                //if(location.allow === true)
                //location.highlight(true);
                break;
        }
    };


// static methods:
    // public methods:
    p.addObserver = function(observer){
        this.mouseSubject.subscribe(observer);
    };

    p.removeObserver = function(observer){
        this.mouseSubject.unsubscribe(observer);
    };

    p.addAllListeners = function(){
        this.addEventListener("mouseover",this._onMouseAction );
        this.addEventListener("mouseout",this._onMouseAction );
        this.addEventListener("click",this._onMouseAction );
    };

    p.removeAllListeners = function(){
        this.removeEventListener("mouseover",this._onMouseAction );
        this.removeEventListener("mouseout",this._onMouseAction );
        this.removeEventListener("click",this._onMouseAction );
    };

    p.focusOut = function(objectIn){ // NEW NO ANIMATION
        this.mouseEnabled = false;
        this.removeAllListeners();
        //this.highlightCountries();
        //this.carousel.hideUI(true);
        //this.carousel.showChosenHouse(false);
        //this.visible = false;
        objectIn.focusIn();
    };

    p.focusIn = function(){
        this.mouseEnabled = false; // todo mozda ne mora
        this.addAllListeners();
        this.mouseEnabled = true; // todo mozda ne mora
        //this.carousel.showChosenHouse(true);
        //this.highlightCountries();
        this.lobby.setSwipeTarget(this.lobby.currentMap.carousel,"x");
        this.lobby.setFocused(this);
    };

    p.clickEnabled = function(bool){
        /*        var b = this.getChildAt(0);
                b.mouseEnabled = bool;*/
        this.mouseEnabled = bool;
    };

    p.highlightCountries = function(bool){//not very good, change
        this.carousel.removeHighlight();
    };

    p.setCarousel = function(houses) {
        var carousel = this.carousel = new alteastream.HouseSelection(houses);
        carousel.x = 897;// ovde
        carousel.y = -450;
        this.addChild(carousel);
    };

    alteastream.ABSMap = createjs.promote(ABSMap,"Container");
})();