
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LocationFactory = function() {};
    var assets = alteastream.Assets;

    LocationFactory.Germany = function () {
        return {
            "skin": assets.getImage(assets.images.germany),
            "flag": assets.getImage(assets.images.germanyFlag),
            "xPos": 900,
            "yPos": -100
        };
    };

    LocationFactory.Austria = function () {
        return {
            "skin": assets.getImage(assets.images.austria),
            "flag": assets.getImage(assets.images.austriaFlag),
            "xPos": 1200,
            "yPos": -100
        };
    };

    LocationFactory.Italy = function () {
        return {
            "skin": assets.getImage(assets.images.italy),
            "flag": assets.getImage(assets.images.italyFlag),
            "xPos": 286,
            "yPos": 514
        };
    };

    LocationFactory.Poland = function () {
        return {
            "skin": assets.getImage(assets.images.poland),
            "flag": assets.getImage(assets.images.polandFlag),
            "xPos": 363,
            "yPos": 360
        };
    };

    LocationFactory.Serbia = function () {
        return {
            "skin": assets.getImage(assets.images.serbia),
            "flag": assets.getImage(assets.images.serbiaFlag),
            "xPos": 321,
            "yPos": -50
        };
    };

    LocationFactory.Belgium = function () {
        return {
            "skin": assets.getImage(assets.images.belgium),
            "flag": assets.getImage(assets.images.belgiumFlag),
            "xPos": 1470,
            "yPos": -50
        };
    };

    alteastream.LocationFactory = LocationFactory;
})();