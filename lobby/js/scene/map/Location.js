
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var Location = function(location) {
        this.Container_constructor();
        this.initialize_Location(location);
    };
    var p = createjs.extend(Location, createjs.Container);
    var _this = null;
    // static properties:
    // events:
    // public properties:
    p.name = "";
    p.casinoName = "";
    p.gameClass = "";
    p.id = null;
    p.machines = null;
    p.allow = true;
    p.enabled = false;
    p.tentImage = null;
    p.category = null;
    // private properties:
    // constructor:
    p.initialize_Location = function(location) {
        _this = this;
        this.id = location.id;
        this.category = location.category;
        this.name = location.name;
        this.casinoName = location.name;
        this.gameClass = location.gameClass;
        //this.enabled = location.enabled;
        this.enabled = true;
        this._setView();
    };
    // private methods:
    p._setView = function(){
        var tent = this.tentImage = alteastream.Assets.getImage(alteastream.Assets.images.house);
        tent.regX = tent.image.width*0.5;
        tent.regY = tent.image.height*0.5;
        var font = "18px Roboto";
        var locationText = new createjs.Text(this.casinoName,font,"#ffffff").set({x:20 - tent.image.width*0.5, y:0, textAlign:"left", textBaseline:"middle", mouseEnabled:false});

        //gameTypesNew
/*        var gameTypeSkin = alteastream.GameTypes["Type_"+this.gameClass]();
        gameTypeSkin.decal.regX = gameTypeSkin.decal.image.width*0.5;
        gameTypeSkin.decal.regY = gameTypeSkin.decal.image.height;
        gameTypeSkin.decal.x = gameTypeSkin.x;
        gameTypeSkin.decal.y = gameTypeSkin.y;*/
        this.addChild(tent,locationText/*,gameTypeSkin.decal*/);

        //gameTypesNew remove line
        //this.addChild(tent,locationText);

        this.clickEnabled(this.enabled);
    };
    // static methods:
    // public methods:
    p.clickEnabled = function(bool){
        this.mouseEnabled = bool;
    };

    p.highlight = function(bool){
        var imgId = bool === true ? "houseHighlight" : "house";
        this.tentImage.image = alteastream.Assets.getImageURI(alteastream.Assets.images[imgId]);
        //this.gameTypeSkin.decal.image = bool === true? this.gameTypeSkin.decalHighlight.image:this.gameTypeSkin.decal.image;
    };

    alteastream.Location = createjs.promote(Location,"Container");
})();