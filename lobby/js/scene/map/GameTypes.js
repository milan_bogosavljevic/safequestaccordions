
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var isMobile = window.localStorage.getItem('isMobile');
    //yes/not for lokal // local ver
    //isMobile = "not";// local ver

    //gameTypesNew
    var GameTypes = function() {};
    var assets = alteastream.Assets;

/*    GameTypes.Type_1 = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.ticketCircusType),
            "x": 0,
            "y": yPos
        };
    };*/

    GameTypes.Type_circus = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.ticketCircusType),
            "x": 0,
            "y": yPos
        };
    };

/*    GameTypes.Type_2 = function () {
        var yPos = isMobile === "yes"? 178:355;
        return {
            "decal": assets.getImage(assets.images.diamondsAndPearlsType),
            "x": 5,
            "y": yPos
        };
    };*/

    GameTypes.Type_pearlspush = function () {
        var yPos = isMobile === "yes"? 178:355;
        return {
            "decal": assets.getImage(assets.images.diamondsAndPearlsType),
            "x": 5,
            "y": yPos
        };
    };

/*    GameTypes.Type_3 = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.cashFestivalType),
            "x": 0,
            "y": yPos
        };
    };*/

/*    GameTypes.Type_12 = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.cashFestivalType),
            "x": 5,
            "y": yPos
        };
    };*/

    GameTypes.Type_roulette = function () {
        var yPos = isMobile === "yes"? 160:325;
        return {
            "decal": assets.getImage(assets.images.cashFestivalType),
            "x": 5,
            "y": yPos
        };
    };

    alteastream.GameTypes = GameTypes;
})();