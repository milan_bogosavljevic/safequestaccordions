
// namespace:
this.alteastream = this.alteastream || {};

(function (){
    "use strict";
    var Swiper = function (){
        this.initialize_Swiper();
    };

    var p = Swiper.prototype;
    var _this = null;
    // static properties:
    // public vars:
    // private vars:
    // private properties
    p._target = null;
    p._direction = null;
    // constructor:
    p.initialize_Swiper = function (){
        _this = this;
        createjs.Touch.enable(stage);
    };
    // static methods:
    // public methods:
    p.activate = function(bool){
        var addRemoveListener = bool ===true?"addEventListener":"removeEventListener";
        stage[addRemoveListener]('mousedown', this._onDown);
        stage[addRemoveListener]('stagemouseup', this._onUp);
    };

    p.setTargetAndDirection = function(obj,direction){
        this._target = obj;
        this._direction = direction;
    };

    // private methods:
    p._onDown = function(event){
        _this.prevInteraction = new Date().getTime();
        _this.prevStageX = event.stageX;
        _this.prevStageY = event.stageY;
        stage.addEventListener('pressmove', _this._onMove);
    };

    p._onMove = function(event){
        var dx = _this._direction === "x"?event.stageX - _this.prevStageX:0;
        var dy = _this._direction === "y"?event.stageY - _this.prevStageY:0;
        var distance = Math.sqrt(dx*dx+dy*dy);
        _this.isDown = true;
        if(distance >= 10){
            var deltaTime = new Date().getTime() - _this.prevInteraction;
            var speed = distance / deltaTime;
            if (speed>0.3){
                stage.removeEventListener('pressmove', _this._onMove);
                var dist = _this._direction === "x"?dx:dy;
                _this._swipe(dist>0?1:-1);
            }
        }
    };

    p._onUp = function(event){
        if(_this.isDown){
            stage.removeEventListener('pressmove', _this._onMove);
            _this.isDown=false;
        }
    };

    p._swipe = function(direction){
        this._target.onSwipe(direction);
    };

    alteastream.Swiper = Swiper;
})();