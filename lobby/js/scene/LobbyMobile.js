

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LobbyMobile = function() {
        console.log("Lobby mobile init.....");
        this.Lobby_constructor();
        this.initialize_LobbyMobile();
    };
    var p = createjs.extend(LobbyMobile, alteastream.Lobby);

    // private properties
    p._hiddenMenu = null;
    p._hiddenMenuIsShown = false;
    // public properties:
    p.swiper = null;

    // static properties:
    // events:

    var _instance = null;

    // constructor:
    p.initialize_LobbyMobile = function() {
        _instance = this;
    };
    // static methods:
    LobbyMobile.getInstance = function(){
        return _instance;
    };

    // private methods
    p.super_setComponents = p._setComponents;
    p._setComponents = function(response){
        this.super_setComponents(response);

        this.currentMap.x = 32;
        this.currentMap.y = 300;

        //this.currentMap.carousel.adjustMobile();
        this._adjustBeams();
        this._adjustBulbs();
        this._info.adjustMobile();
        this._adjustBalance();
        this._setSwiper();
        //this._adjustClock();// uncomment if clock need
        stage.enableMouseOver(0);
    };

    p._adjustClock = function() {
        this.clock.x = 480;
        this.clock.y = 5;
        this.showFSClock(true);
    };

    p._adjustBalance = function() {
        var balanceBack = this.balanceBackground;
        balanceBack.uncache();
        balanceBack.graphics.clear();
        balanceBack.graphics.beginFill("#000000").drawRect(0, 0, 194, 45);
        balanceBack.cache(0, 0, 194, 45);
        balanceBack.x = 16;
        balanceBack.y = 480;

        this.lobbyBalanceLabel.font = "14px HurmeGeometricSans3";
        this.lobbyBalanceAmount.font = "bold 16px HurmeGeometricSans3";
        this.lobbyBalanceLabel.x = balanceBack.x + 99;
        this.lobbyBalanceLabel.y = balanceBack.y + 13;
        this.lobbyBalanceAmount.x = this.lobbyBalanceLabel.x;
        this.lobbyBalanceAmount.y = this.lobbyBalanceLabel.y + 20;
    };

    p._adjustBeams = function() {
        var beams = this.beams._beamsArray;
        beams[0].x = 0;
        beams[0].y = -10;
        beams[1].x = 15;
        beams[1].y = -10;
        beams[2].x = 960;
        beams[2].y = -10;
        beams[3].x = 975;
        beams[3].y = -10;
    };

    p._adjustBulbs = function() {
        var bulbsNum = this.bulbsContainer.numChildren;
        var util = gamecore.Utils.NUMBER;
        for(var i = 0; i < bulbsNum; i++){
            var xp = util.randomRange(10,950);
            var yp = util.randomRange(10,125);
            var bulb = this.bulbsContainer.getChildAt(i);
            bulb.x = xp;
            bulb.y = yp;
        }
    };

    p._setPlayerActivity = function() {
        _instance._playerActivityPanel = new alteastream.PlayerActivityPanel();
        _instance.addChild(_instance._playerActivityPanel);
        _instance._playerActivityPanel.visible = false;
        _instance._playerActivityPanel.preventClickThrough(true);
    };

    /*p._getPlayerActivity = function(){
        this.requester.getPlayerActivity(function (response) {
            _instance._playerActivityPanel.setPanel(response);
            _instance._adjustActivityLog();
        });
    };*/

    p._getPlayerActivity = function(){
        _instance.requester.getPlayerActivity(function (response) {
            if(response.length){
                _instance._playerActivityPanel.setPanel(response);
                _instance._playerActivityPanel.adjustMobile();
            }else{
                _instance._startPlayerActivityInterval();
            }
        });
    };

    p._startPlayerActivityInterval = function() {
        var attempt = 0;
        var activityInterval = setInterval(function(){
            _instance.requester.getPlayerActivity(function (response) {
                if(response.length){
                    _instance._playerActivityPanel.setPanel(response);
                    _instance._playerActivityPanel.adjustMobile();
                    clearInterval(activityInterval);
                }else{
                    attempt++;
                    if(attempt === 6){
                        clearInterval(activityInterval);
                    }
                }
            });
        },5000);
    };

    p._showInfoOnStart = function(bool){
        this._info.scaleX = this._info.scaleY = bool===true? 1:0;

        this.btnSound.visible = this.btnQuit.visible = this.btnPlayerActivity.visible = !bool;
        this._hiddenMenuButton.visible = !bool;
        this.btnInfo.y = bool === true ? -55:this.btnQuit.y + (this.btnQuit.height*2);
        this.btnInfo.x = bool === true ? -100 :0;
    }

    p._setTopButtons = function() {
        var assets = alteastream.Assets;
        var hiddenMenu = this._hiddenMenu = new createjs.Container();

        var menuButton = this._hiddenMenuButton = new alteastream.ImageButton(assets.getImageURI(assets.images.btnHiddenMenu),3);
        menuButton.x = 878;
        menuButton.y = 12;
        menuButton.visible = false;
        this.addChild(menuButton);

        var menuButtonHandler = function () {
            _instance.showHiddenMenu(_instance._hiddenMenu.x === alteastream.AbstractScene.GAME_WIDTH);
        };

        menuButton.setClickHandler(menuButtonHandler);

        hiddenMenu.x = alteastream.AbstractScene.GAME_WIDTH;
        hiddenMenu.y = menuButton.y + 80;

        var btnQuit = this.btnQuit = new alteastream.ImageButton(assets.getImageURI(assets.images.btnQuitSS),3);
        var btnHeight = btnQuit.height;
        btnQuit.visible = false;

        var btnQuitHandler = function () {
            _instance.checkIfHiddenMenuIsShown();
            _instance.btnQuitHandler();
        };
        btnQuit.setClickHandler(btnQuitHandler);

        hiddenMenu.addChild(btnQuit);

        var btnSound = this.btnSound = alteastream.Assets.getImage(assets.images.soundOn);
        btnSound.y = btnQuit.y + btnHeight;
        btnSound.visible = false;
        btnSound.addEventListener("click", function () {
            createjs.Sound.muted = !createjs.Sound.muted;
            btnSound.image = createjs.Sound.muted === true ? assets.getImage(assets.images.soundOff).image : assets.getImage(assets.images.soundOn).image;
            var action = createjs.Sound.muted === true ? "pause":"play";
            window.parent.manageBgMusic(action);
            _instance.checkIfHiddenMenuIsShown();
        });
        hiddenMenu.addChild(btnSound);

        var btnInfo = this.btnInfo =  new alteastream.ImageButton(assets.getImageURI(assets.images.btnInfoSS),3);

        btnInfo.setClickHandler(_instance.btnInfoHandler);

        hiddenMenu.addChild(btnInfo);

        var btnPlayerActivity = this.btnPlayerActivity =  alteastream.Assets.getImage(assets.images.btnPlayerActivity);
        btnPlayerActivity.y = btnQuit.y + (btnHeight * 3);
        btnPlayerActivity.originalYPosition = btnPlayerActivity.y;
        btnPlayerActivity.addEventListener("click" , function () {
            _instance.btnPlayerActivityHandler();
        });
        hiddenMenu.addChild(btnPlayerActivity);

        this.addChild(hiddenMenu);
    };

    p._setSwiper = function(){
        var swiper = this.swiper = new alteastream.Swiper();
        swiper.activate(true);
        this.setSwipeTarget(this.currentMap.carousel,"x");
    };

    // public methods
    p.btnPlayerActivityHandler = function() {
        _instance._playerActivityPanel.visible = !_instance._playerActivityPanel.visible;
        _instance.btnPlayerActivity.x = _instance._playerActivityPanel.visible === true ? -30 : 0;
        _instance.btnPlayerActivity.y = _instance._playerActivityPanel.visible === true ? -55 : _instance.btnPlayerActivity.originalYPosition;
        _instance._hiddenMenuButton.visible = _instance.btnSound.visible = _instance.btnQuit.visible = _instance.btnInfo.visible = !_instance._playerActivityPanel.visible;
        if(_instance._playerActivityPanel.visible === false){
            _instance.showHiddenMenu(false);
            _instance._playerActivityPanel.setToFirstPage(false);
            if(_instance._currentFocused.name === "circus"){
                _instance.setSwipeTarget(_instance.currentMap.carousel,"x");
            }else{
                _instance.setSwipeTarget(_instance.currentLocationMachines,"y");
            }
        }else{
            _instance.setSwipeTarget(_instance._playerActivityPanel, "y");
        }
        _instance._playerActivityPanel.activateSupportLinkListeners(_instance._playerActivityPanel.visible);
    };

    p.btnInfoHandler = function(){
        _instance._info.visible = true;
        _instance.btnInfo.mouseEnabled = false;
        var scale = _instance._info.scaleX === 1 ? 0 : 1;
        _instance._hiddenMenuButton.visible = _instance.btnSound.visible = _instance.btnQuit.visible = _instance.btnPlayerActivity.visible = scale === 0;
        _instance.btnInfo.y = scale === 0 ? _instance.btnQuit.y + (_instance.btnQuit.height*2) : -55;
        _instance.btnInfo.x = scale === 0 ? 0 : -30;
        if(scale === 0){_instance.showHiddenMenu(false);}
        if(scale === 1){_instance._info.resetCurrentPage();}
        _instance.btnQuit.visible = scale === 0;
        createjs.Tween.get(_instance._info).to({scaleX:scale,scaleY:scale},300,createjs.Ease.quadInOut).call(function () {
            _instance.btnInfo.mouseEnabled = true;
            if(scale === 0){_instance._info.visible = false;}
        });
    };

    p.showHiddenMenu = function(show) {
        this._hiddenMenuButton.setDisabled(true);
        this._hiddenMenuIsShown = show;
        var xP = show === true ? this._hiddenMenuButton.x : alteastream.AbstractScene.GAME_WIDTH;
        createjs.Tween.get(this._hiddenMenu).to({x:xP},500,createjs.Ease.quadOut).call(function () {
            _instance._hiddenMenuButton.setDisabled(false);
        })
    };

    p.checkIfHiddenMenuIsShown = function() {
        if(this._hiddenMenuIsShown === true) {
            this.showHiddenMenu(false);
        }
    };

    p.setCurrentLocationMachines = function() {
        var currentLocationMachines = this.currentLocationMachines = new alteastream.LocationMachinesMobile(this);
        currentLocationMachines.x = 10;
        currentLocationMachines.y = 10;
    };

    p.setSwipeTarget = function (target,axis) {
        this.swiper.setTargetAndDirection(target,axis);
    };

    p.enableSwiper = function(enable) {
        this.swiper.activate(enable);
    };

    alteastream.LobbyMobile = createjs.promote(LobbyMobile,"Lobby");
})();