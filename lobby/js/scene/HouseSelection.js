

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var HouseSelection = function (houses) {
        this.Container_constructor();
        this.initialize_HouseSelection(houses);
    };

    var p = createjs.extend(HouseSelection,createjs.Container);

    // static properties:
    // events:
    // private vars:
    // public vars:
    var _this = null;
    // public properties
    p.spacing = 0;
    // private properties
    p._bigHousesCont = null;
    p._scroller = null;
    p._pagination = null;

    // constructor:
    p.initialize_HouseSelection = function (houses) {
        console.log("HouseSelection Initialized");
        _this = this;
        this.spacing = 750;
        this.name = "CAROUSEL";
        this.visible = false;

        var housesContainer = this._bigHousesCont = new createjs.Container();
        this._setup(houses);
        var scroller = this._scroller = new alteastream.Scroller(this._bigHousesCont);
        var scrollLevels = [];
        for(var i = 0; i < houses.length; i++){
            scrollLevels.push(i * this.spacing * -1);
        }

        scroller.setScrollType("scrollByPage");
        scroller.setScrollDirection("x");
        scroller.setRowsPerPage(1);
        scroller.setNumberOfPages(houses.length);
        scroller.setScrollLevels(scrollLevels);

        var dotImage = alteastream.Assets.getImage(alteastream.Assets.images.smallHouse);
        var paginationOptions = {
            numberOfPages:houses.length,
            scroller:scroller,
            customDot:"smallHouse",
            customCurrentDot:"smallHouseCurrent",
            dotFont:"16px HurmeGeometricSans3",
            currentDotFont:null,
            dotsSpacing:25,
            dotsWidth:dotImage.image.width
        };

        dotImage = null;

        var pagination = this._pagination = new alteastream.Pagination(paginationOptions);

        var leftButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnArrowLeftSS),3);
        leftButton.setDisabled(true);

        var rightButton = new alteastream.ImageButton(alteastream.Assets.getImageURI(alteastream.Assets.images.btnArrowLeftSS),3);
        rightButton.rotation = 180;

        var lX = -175;
        var rX = pagination.getPaginationWidth() + 100;

        pagination.setUpButton(leftButton, lX, -(leftButton._height*0.5), scroller);
        pagination.setDownButton(rightButton, rX, (rightButton._height*0.5), scroller);
        pagination.removeTextFromCurrentDot();

        var paginationTexts = [];
        for(var j = 0; j < this._bigHousesCont.numChildren; j++){
            paginationTexts.push(this._bigHousesCont.getChildAt(j).name);
        }

        pagination.adjustDotsTextFields(0,-50,paginationTexts);
        pagination.textDownPosition = -50;
        pagination.textUpPosition = -60;
        pagination.x = - (pagination.getPaginationWidth()/2) + (pagination._dotWidth/2);
        pagination.y = (alteastream.AbstractScene.GAME_HEIGHT/2) - 50;
        pagination._dotsContainer.getChildAt(0).scale = 1.2;
        pagination._currentDot.scale = 1.2;
        pagination.showPagination(houses.length > 1);
        pagination.setShorAndLongTexts();

        scroller.plugInPagination(pagination);
        scroller.setPreAnimateScrollMethod(function () {
            _this.clicksEnabled(false);
            scroller.manageButtonsState();
            scroller.updateCurrentDotPosition();
            pagination.doMovingDotScaleAnimation(scroller.scrollByPageCurrentLevel)
        });
        scroller.setPostAnimateScrollMethod(function () {
            _this.enableSelectedHouse();
            _this.clicksEnabled(true);
        });

        this.addChild(housesContainer/*, pagination*/);
    };
    // private methods:
    p._setup =  function(houses) {
        for (var i=0; i < houses.length; i++){
            var location = new alteastream.Location(houses[i]);
            //location.x = i * this.spacing;
            location.y = i * 50;
            this._bigHousesCont.addChild(location);
        }



        /*        var houseToEnable = this._bigHousesCont.getChildAt(0);
                if(houseToEnable.enabled){
                    houseToEnable.allow = true;
                }*/

        this.visible = true;
    };

    // public methods:
    p.onSwipe = function (direction) {
        if(direction === 1){
            this._scroller.moveUp();
        }
        else{
            this._scroller.moveDown();
        }
    };

    p.showChosenHouse = function(bool) {//map.selectedLocation
        //var ind = this._scroller.scrollByPageCurrentLevel;
        //var house = this._bigHousesCont.getChildAt(ind);
        //house.visible = bool;

        var selectedLocation = this.parent.lobby.selectedLocation;
        var ind = this._bigHousesCont.getChildIndex(selectedLocation);

        if(ind !== this._bigHousesCont.numChildren - 1){
            for(var i = ind+1; i < this._bigHousesCont.numChildren; i++){
                var house = this._bigHousesCont.getChildAt(i);
                house.y += 280;
            }
        }
    };

    p.removeHighlight = function() {
        for(var i = 0; i < this._bigHousesCont.numChildren; i++){
            var location = this._bigHousesCont.getChildAt(i);
            if(location.enabled === true){
                location.highlight(false);
            }
        }
    };

    p.enableSelectedHouse = function() {
        /*        for(var i = 0; i < this._bigHousesCont.numChildren; i++){
                    var house = this._bigHousesCont.getChildAt(i);
                    house.allow = false;
                }*/
        /*        var ind = this._scroller.scrollByPageCurrentLevel;
                var houseToEnable = this._bigHousesCont.getChildAt(ind)
                if(houseToEnable.enabled){
                    houseToEnable.allow = true;
                }*/
    };

    p.clicksEnabled = function(bool) {
        this.mouseEnabled = bool;
    };

    p.getLocation = function(id){
        for(var i = 0; i < this._bigHousesCont.numChildren; i++){
            var locationCont = this._bigHousesCont.getChildAt(i);
            if(locationCont.id === String(id)){
                return locationCont;
            }
        }
    };

    p.storeLocationPosition  = function() {
        window.localStorage.setItem('locationPosition', String(this._scroller.scrollByPageCurrentLevel));
    };

    p.restoreLocationPosition = function() {
        var storedLocationPosition = Number(window.localStorage.getItem('locationPosition'));
        if(storedLocationPosition === this._scroller.scrollByPageCurrentLevel){//todo mozda da se proveri samo dali je 0
            return;
        }
        this._scroller.scrollByPageCurrentLevel = storedLocationPosition;
        this._scroller.scrollByPage(false);
    };

    p.adjustMobile = function(){
        this.x = 448;
        this.y = -45;
        this.spacing = 375;
        var scrollLevels = [];
        var R = this._pagination._dotWidth = this._pagination._dotsContainer.getChildAt(0).getChildAt(0).image.width;
        var spacing = this._pagination._dotSpacing = 30;
        this._pagination._setComponentWidth();
        this._pagination.textDownPosition = -28;
        this._pagination.textUpPosition = -38;
        for (var i=0; i < this._bigHousesCont.numChildren; i++){
            var houseCont = this._bigHousesCont.getChildAt(i);
            houseCont.x = i * this.spacing;

            var locationText = houseCont.getChildAt(1);
            //locationText.font = "35px NIAGSOL";
            locationText.font = locationText.text.length<14?"35px NIAGSOL":"25px NIAGSOL";
            locationText.y = -52;

            var smallLocationCont = this._pagination._dotsContainer.getChildAt(i);
            smallLocationCont.x = (i * R) + (spacing * i);

            var smallLocationText = smallLocationCont.getChildAt(1);
            if(smallLocationText){
                smallLocationText.font = "13px HurmeGeometricSans3";
                smallLocationText.y = i > 0 ? this._pagination.textDownPosition : this._pagination.textUpPosition;
            }

            scrollLevels.push(i * this.spacing * -1);
        }
        this._pagination.x = - (this._pagination.getPaginationWidth()/2) + (R/2);
        this._pagination.y = (alteastream.AbstractScene.GAME_HEIGHT/2) - 15;
        this._pagination.hideUI(true);
        this._scroller.setScrollLevels(scrollLevels);
    };

    alteastream.HouseSelection = createjs.promote(HouseSelection,"Container");
})();