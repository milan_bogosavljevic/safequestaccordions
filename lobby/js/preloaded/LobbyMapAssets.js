

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LobbyAssets = function() {};
    var Assets = alteastream.Assets;

    LobbyAssets.count = 0;

    LobbyAssets.loadManifest = function(lang){
        Assets.setImageLang(lang);
        var noCache = "?ver="+Assets.VERSION;
        //var language = Assets.getImageLang();
        //var localePath = "assets/images/locale/" + language + "/";
        return [
            //images
            {id:Assets.images.beam, src:"assets/images/beam.png"+noCache},
            {id:Assets.images.flare, src:"assets/images/sparkle.png"+noCache},
            {id:Assets.images.window, src:"assets/images/window.png"+noCache},
            {id:Assets.images.windowList, src:"assets/images/windowList.png"+noCache},
            {id:Assets.images.window_big, src:"assets/images/window_big.png"+noCache},
            {id:Assets.images.machineBg, src:"assets/images/machineBg.png"+noCache},
            {id:Assets.images.machineOverlay, src:"assets/images/machineOverlay.png"+noCache},
            {id:Assets.images.thumb, src:"assets/images/thumb.png"+noCache},
            {id:Assets.images.noThumb, src:"assets/images/noThumb.png"+noCache},
            {id:Assets.images.spinner, src:"assets/images/spinner.png"+noCache},
            {id:Assets.images.mainBackground,src:"assets/images/mainBackground.jpg"+noCache},
            {id:Assets.images.mainBackgroundOverlay,src:"assets/images/mainBackgroundOverlay.png"+noCache},
            {id:Assets.images.mainBackgroundOverlay2,src:"assets/images/mainBackgroundOverlay2.png"+noCache},
            {id:Assets.images.infoBackground,src:"assets/images/infoBackground.jpg"+noCache},
            {id:Assets.images.infoPage1Image,src:"assets/images/infoPage1Image.png"+noCache},
            {id:Assets.images.infoPage2Image,src:"assets/images/infoPage2Image.png"+noCache},
            {id:Assets.images.infoPage3Image,src:"assets/images/infoPage3Image.png"+noCache},
            {id:Assets.images.mainLogo,src:"assets/images/logo.png"+noCache},
            {id:Assets.images.animatedLogo,src:"assets/images/gausel.png"+noCache},
            {id:Assets.images.floor,src:"assets/images/locations/floor.png"+noCache},
            {id:Assets.images.houseClosed,src:"assets/images/locations/houseClosed.png"+noCache},
            {id:Assets.images.house,src:"assets/images/locations/house.png"+noCache},
            {id:Assets.images.houseHighlight,src:"assets/images/locations/houseHighlight.png"+noCache},
            {id:Assets.images.smallHouse,src:"assets/images/locations/smallHouse.png"+noCache},
            {id:Assets.images.smallHouseCurrent,src:"assets/images/locations/smallHouseCurrent.png"+noCache},
            {id:Assets.images.lampGreen,src:"assets/images/lampGreen.png"+noCache},
            {id:Assets.images.lampGreenLight,src:"assets/images/lampGreenLight.png"+noCache},
            {id:Assets.images.lampYellow,src:"assets/images/lampYellow.png"+noCache},
            {id:Assets.images.lampYellowDark,src:"assets/images/lampYellowDark.png"+noCache},
            {id:Assets.images.lampRed,src:"assets/images/lampRed.png"+noCache},
            {id:Assets.images.listMachineBackground,src:"assets/images/listMachineBackground.png"+noCache},
            {id:Assets.images.noAvailableMachinesImg,src:"assets/images/noAvailableMachines.png"+noCache},

            // co-play prize win animation
            {id:Assets.images.chip_1_back, src:"assets/images/chip_1.png"+noCache},
            {id:Assets.images.chip_2_back, src:"assets/images/chip_2.png"+noCache},
            {id:Assets.images.chip_3_back, src:"assets/images/chip_3.png"+noCache},
            {id:Assets.images.chip_4_back, src:"assets/images/chip_4.png"+noCache},
            {id:Assets.images.chip_5_back, src:"assets/images/chip_5.png"+noCache},
            {id:Assets.images.chip_6_back, src:"assets/images/chip_6.png"+noCache},
            {id:Assets.images.coinWin, src:"assets/images/coinWin.png"+noCache},

            //gameTypesNew
            {id:Assets.images.ticketCircusType, src:"assets/images/locations/ticketCircusType.png"+noCache},
            {id:Assets.images.cashFestivalType, src:"assets/images/locations/cashFestivalType.png"+noCache},
            {id:Assets.images.diamondsAndPearlsType, src:"assets/images/locations/diamondsAndPearlsType.png"+noCache},
            //btns
            {id:Assets.images.btnArrowLeftSS,src:"assets/images/buttons/arrow_btn.png"+noCache},
            {id:Assets.images.btnStart,src:"assets/images/buttons/start_btn.png"+noCache},
            {id:Assets.images.btnBack,src:"assets/images/buttons/back_btn.png"+noCache},
            {id:Assets.images.btnBack2,src:"assets/images/buttons/back_btn2.png"+noCache},
            {id:Assets.images.btnBackForEntranceCounter,src:"assets/images/buttons/back_btn_for_counter.png"+noCache},
            {id:Assets.images.btnExitForQuitCounter,src:"assets/images/buttons/exit_btn_for_counter.png"+noCache},
            {id:Assets.images.btnParticipate,src:"assets/images/buttons/btnParticipate.png"+noCache},
            {id:Assets.images.btnStopParticipate,src:"assets/images/buttons/btnStopParticipate.png"+noCache},
            {id:Assets.images.btnStakeDec,src:"assets/images/buttons/stakeDec.png"+noCache},
            {id:Assets.images.btnStakeInc,src:"assets/images/buttons/stakeInc.png"+noCache},
            {id:Assets.images.btnInfoSS,src:"assets/images/buttons/infoSS.png"+noCache},
            {id:Assets.images.infoButtonLeft,src:"assets/images/buttons/btnInfoPagesLeft.png"+noCache},
            {id:Assets.images.infoButtonRight,src:"assets/images/buttons/btnInfoPagesRight.png"+noCache},
            {id:Assets.images.soundOn,src:"assets/images/buttons/sound_icon_default.png"+noCache},
            {id:Assets.images.soundOff,src:"assets/images/buttons/sound_icon_disabled.png"+noCache},
            {id:Assets.images.btnQuitSS,src:"assets/images/buttons/quitSS.png"+noCache},
            {id:Assets.images.btnFullScreen_On,src:"assets/images/buttons/btnFullScreen_On.png"+noCache},
            {id:Assets.images.btnFullScreen_Off,src:"assets/images/buttons/btnFullScreen_Off.png"+noCache},
            {id:Assets.images.btnPlayerActivity,src:"assets/images/buttons/btnPlayerActivity.png"+noCache},
            //sounds
            {id:Assets.sounds.confirm,src:"assets/sounds/confirm.ogg"+noCache},
            {id:Assets.sounds.welcomeMessageSpoken,src:"assets/sounds/welcome_message_01.ogg"+noCache},
            {id:Assets.sounds.error,src:"assets/sounds/error.ogg"+noCache},
            {id:Assets.sounds.errorSpoken,src:"assets/sounds/error_spoken_01.ogg"+noCache},
            {id:Assets.sounds.info,src:"assets/sounds/info.ogg"+noCache},
            {id:Assets.sounds.warning,src:"assets/sounds/warning.ogg"+noCache},
            {id:Assets.sounds.exception,src:"assets/sounds/exception.ogg"+noCache},
            {id:Assets.sounds.refund,src:"assets/sounds/refund.ogg"+noCache}
        ];
    };

    LobbyAssets.loadFontManifest = function(){
        return [
            "css/HurmeGeometricSans3.otf",
            "css/HurmeGeometricSans3 Light.otf",
            "css/HurmeGeometricSans3 Bold.otf",
            "css/NIAGSOL.TTF"
        ];
    };

    /*LobbyAssets.atlasImage = {};
    var aI = LobbyAssets.atlasImage = {};
    aI[Assets.atlasImage.ss] = "ss";*/

    LobbyAssets.texts = {};
    var t = LobbyAssets.texts["en"] = {};
    t[Assets.texts.win] = "WIN";
    t[Assets.texts.sessionDoesNotExist] = "Session does not exist";
    t[Assets.texts.casinoServiceUnreachable] = "CasinoServiceUnreachable";
    t[Assets.texts.infoText1] = "Select a machine to play directly, or participate in a game.";
    t[Assets.texts.infoText2] = "Currently under maintenance/error or offline.";
    t[Assets.texts.infoText3] = "Available for instant play.";
    t[Assets.texts.infoText4] = "Enter CO-PLAY and participate in active game while waiting your turn.";
    t[Assets.texts.infoText5] = "Actively participate on other players game by watching the live stream and betting on the outcome.\n\n" +
        "You are notified of number of players in CO-PLAY, and of your current position in the queue.\n\n" +
        "The list of all currently unoccupied machines (if any) is also available for you to start your instant play.\n\n" +
        "If you select 'BACK TO LOBBY', you will be returned to the lobby section and loose your queue position for selected machine.";
    t[Assets.texts.infoText6] = "Confirmation applet will prompt you to confirm your game play within 10 seconds.\n\n" +
        "If you don't comply timely, or you select 'BACK TO LOBBY', you will be returned to the lobby section and your pending game session will be cancelled";
    t[Assets.texts.infoText7] = "QUEUE";
    t[Assets.texts.infoText8] = "Here is the info for the last 24 hours, for more details please contact";

    //t = LobbyAssets.texts["sr"] = {};

    alteastream.LobbyAssets = LobbyAssets;
})();