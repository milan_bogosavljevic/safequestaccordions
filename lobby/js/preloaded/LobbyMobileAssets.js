

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

    var LobbyMobileAssets = function() {};
    var Assets = alteastream.Assets;

    LobbyMobileAssets.count = 0;

    LobbyMobileAssets.loadManifest = function(lang){
        Assets.setImageLang(lang);
        //var language = Assets.getImageLang();
        //var localePath = "assets/mobile/images/locale/" + language + "/";
        var noCache = "?ver="+Assets.VERSION;

        return [
            //locale
            /*{id:Assets.imgTexts.drawTxtImg,src:localePath + "/drawTxt.png"+noCache},
             */
            //images
            {id:Assets.images.beam, src:"assets/mobile/images/beam.png"+noCache},
            {id:Assets.images.flare, src:"assets/mobile/images/sparkle.png"+noCache},
            {id:Assets.images.window, src:"assets/mobile/images/window.png"+noCache},
            {id:Assets.images.window_big, src:"assets/mobile/images/window_big.png"+noCache},
            {id:Assets.images.machineBg, src:"assets/mobile/images/machineBg.png"+noCache},
            {id:Assets.images.machineOverlay, src:"assets/mobile/images/machineOverlay.png"+noCache},
            {id:Assets.images.thumb, src:"assets/mobile/images/thumb.png"+noCache},
            {id:Assets.images.noThumb, src:"assets/mobile/images/noThumb.png"+noCache},
            {id:Assets.images.spinner, src:"assets/mobile/images/spinner.png"+noCache},
            {id:Assets.images.mainBackground,src:"assets/mobile/images/mainBackground.jpg"+noCache},
            {id:Assets.images.mainBackgroundOverlay,src:"assets/mobile/images/mainBackgroundOverlay.png"+noCache},
            {id:Assets.images.mainBackgroundOverlay2,src:"assets/mobile/images/mainBackgroundOverlay2.png"+noCache},
            {id:Assets.images.infoBackground,src:"assets/mobile/images/infoBackground.jpg"+noCache},
            {id:Assets.images.infoPage1Image,src:"assets/mobile/images/infoPage1Image.png"+noCache},
            {id:Assets.images.infoPage2Image,src:"assets/mobile/images/infoPage2Image.png"+noCache},
            {id:Assets.images.infoPage3Image,src:"assets/mobile/images/infoPage3Image.png"+noCache},
            {id:Assets.images.mainLogo,src:"assets/mobile/images/logo.png"+noCache},
            {id:Assets.images.animatedLogo,src:"assets/mobile/images/gausel.png"+noCache},
            {id:Assets.images.floor,src:"assets/mobile/images/locations/floor.png"+noCache},
            {id:Assets.images.houseClosed,src:"assets/mobile/images/locations/houseClosed.png"+noCache},
            {id:Assets.images.house,src:"assets/mobile/images/locations/house.png"+noCache},
            {id:Assets.images.houseHighlight,src:"assets/mobile/images/locations/houseHighlight.png"+noCache},
            {id:Assets.images.smallHouse,src:"assets/mobile/images/locations/smallHouse.png"+noCache},
            {id:Assets.images.smallHouseCurrent,src:"assets/mobile/images/locations/smallHouseCurrent.png"+noCache},
            {id:Assets.images.lampGreen,src:"assets/mobile/images/lampGreen.png"+noCache},
            {id:Assets.images.lampGreenLight,src:"assets/mobile/images/lampGreenLight.png"+noCache},
            {id:Assets.images.lampYellow,src:"assets/mobile/images/lampYellow.png"+noCache},
            {id:Assets.images.lampYellowDark,src:"assets/mobile/images/lampYellowDark.png"+noCache},
            {id:Assets.images.lampRed,src:"assets/mobile/images/lampRed.png"+noCache},
            {id:Assets.images.listMachineBackground,src:"assets/mobile/images/listMachineBackground.png"+noCache},
            {id:Assets.images.chip_1_back, src:"assets/mobile/images/chip_1.png"+noCache},
            {id:Assets.images.chip_2_back, src:"assets/mobile/images/chip_2.png"+noCache},
            {id:Assets.images.chip_3_back, src:"assets/mobile/images/chip_3.png"+noCache},
            {id:Assets.images.chip_4_back, src:"assets/mobile/images/chip_4.png"+noCache},
            {id:Assets.images.chip_5_back, src:"assets/mobile/images/chip_5.png"+noCache},
            {id:Assets.images.chip_6_back, src:"assets/mobile/images/chip_6.png"+noCache},
            {id:Assets.images.coinWin, src:"assets/mobile/images/coinWin.png"+noCache},
            {id:Assets.images.noAvailableMachinesImg,src:"assets/mobile/images/noAvailableMachines.png"+noCache},
            //gameTypesNew
            {id:Assets.images.ticketCircusType, src:"assets/mobile/images/locations/ticketCircusType.png"+noCache},
            {id:Assets.images.cashFestivalType, src:"assets/mobile/images/locations/cashFestivalType.png"+noCache},
            {id:Assets.images.diamondsAndPearlsType, src:"assets/mobile/images/locations/diamondsAndPearlsType.png"+noCache},
            //btns
            {id:Assets.images.btnArrowLeftSS,src:"assets/mobile/images/buttons/arrow_btn.png"+noCache},
            {id:Assets.images.btnStart,src:"assets/mobile/images/buttons/start_btn.png"+noCache},
            {id:Assets.images.btnBack,src:"assets/mobile/images/buttons/back_btn.png"+noCache},
            {id:Assets.images.btnBack2,src:"assets/mobile/images/buttons/back_btn2.png"+noCache},
            {id:Assets.images.btnBackForEntranceCounter,src:"assets/mobile/images/buttons/back_btn_for_counter.png"+noCache},
            {id:Assets.images.btnExitForQuitCounter,src:"assets/images/buttons/exit_btn_for_counter.png"+noCache},
            {id:Assets.images.btnParticipate,src:"assets/mobile/images/buttons/btnParticipate.png"+noCache},
            {id:Assets.images.btnStopParticipate,src:"assets/mobile/images/buttons/btnStopParticipate.png"+noCache},
            {id:Assets.images.btnStakeDec,src:"assets/mobile/images/buttons/stakeDec.png"+noCache},
            {id:Assets.images.btnStakeInc,src:"assets/mobile/images/buttons/stakeInc.png"+noCache},
            {id:Assets.images.infoButtonLeft,src:"assets/images/buttons/btnInfoPagesLeft.png"+noCache},
            {id:Assets.images.infoButtonRight,src:"assets/images/buttons/btnInfoPagesRight.png"+noCache},
            {id:Assets.images.btnInfoSS,src:"assets/mobile/images/buttons/infoSS.png"+noCache},
            {id:Assets.images.soundOn,src:"assets/mobile/images/buttons/sound_icon_default.png"+noCache},
            {id:Assets.images.soundOff,src:"assets/mobile/images/buttons/sound_icon_disabled.png"+noCache},
            {id:Assets.images.btnQuitSS,src:"assets/mobile/images/buttons/quitSS.png"+noCache},
            {id:Assets.images.btnPlayerActivity,src:"assets/mobile/images/buttons/btnPlayerActivity.png"+noCache},
            {id:Assets.images.btnHiddenMenu,src:"assets/mobile/images/buttons/hiddenmenu_btn.png"+noCache},
            {id:Assets.images.btnHiddenMenuTransparent,src:"assets/mobile/images/buttons/hiddenmenu_btn_transparent.png"+noCache},
            {id:Assets.images.btnHiddenMenuTransparentBlack,src:"assets/mobile/images/buttons/hiddenmenu_btn_transparent_black.png"+noCache},
            //sounds
            {id:Assets.sounds.confirm,src:"assets/sounds/confirm.ogg"+noCache},
            {id:Assets.sounds.welcomeMessageSpoken,src:"assets/sounds/welcome_message_01.ogg"+noCache},
            {id:Assets.sounds.error,src:"assets/sounds/error.ogg"+noCache},
            {id:Assets.sounds.errorSpoken,src:"assets/sounds/error_spoken_01.ogg"+noCache},
            {id:Assets.sounds.info,src:"assets/sounds/info.ogg"+noCache},
            {id:Assets.sounds.warning,src:"assets/sounds/warning.ogg"+noCache},
            {id:Assets.sounds.exception,src:"assets/sounds/exception.ogg"+noCache},
            {id:Assets.sounds.refund,src:"assets/sounds/refund.ogg"+noCache}
        ];
    };

    LobbyMobileAssets.loadFontManifest = function(){
        return [
            "css/HurmeGeometricSans3.otf",
            "css/HurmeGeometricSans3 Light.otf",
            "css/HurmeGeometricSans3 Bold.otf",
            "css/NIAGSOL.TTF"
        ];
    };

    /*LobbyMobileAssets.atlasImage = {};
    var aI = LobbyMobileAssets.atlasImage = {};
    aI[Assets.atlasImage.ss] = "ss";*/

    LobbyMobileAssets.texts = {};
    var t = LobbyMobileAssets.texts["en"] = {};
    t[Assets.texts.win] = "WIN";
    t[Assets.texts.sessionDoesNotExist] = "Session does not exist";
    t[Assets.texts.casinoServiceUnreachable] = "CasinoServiceUnreachable";
    t[Assets.texts.infoText1] = "Select a machine to play directly, or participate in a game.";
    t[Assets.texts.infoText2] = "Currently under maintenance/error or offline.";
    t[Assets.texts.infoText3] = "Available for instant play.";
    t[Assets.texts.infoText4] = "Enter CO-PLAY and participate in active game while waiting your turn.";
    t[Assets.texts.infoText5] = "Actively participate on other players game by watching the live stream and betting on the outcome.\n\n" +
        "You are notified of number of players in CO-PLAY, and of your current position in the queue.\n\n" +
        "The list of all currently unoccupied machines (if any) is also available for you to start your instant play.\n\n" +
        "If you select 'BACK TO LOBBY', you will be returned to the lobby section and loose your queue position for selected machine.";
    t[Assets.texts.infoText6] = "Confirmation applet will prompt you to confirm your game play within 10 seconds.\n\n" +
        "If you don't comply timely, or you select 'BACK TO LOBBY', you will be returned to the lobby section and your pending game session will be cancelled";
    t[Assets.texts.infoText7] = "QUEUE";
    t[Assets.texts.infoText8] = "Here is the info for the last 24 hours, for more details please contact";

    //t = LobbyMobileAssets.texts["sr"] = {};

    alteastream.LobbyMobileAssets = LobbyMobileAssets;
})();