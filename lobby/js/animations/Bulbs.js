
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var Bulbs = function(scene) {
    this.initialize_Bulbs(scene);
};
var p = Bulbs.prototype;

// static properties:
// public properties:
    p.scene = null;
    p.animate = true;
// private properties:
    p._bulbs = null;
// constructor:

    p.initialize_Bulbs = function(scene) {
        this.scene = scene;
        this._bulbs = [];
    };

// private methods:
    p._loop = function (scope) {
        if(scope.animate === true) {
            var rDel = gamecore.Utils.NUMBER.randomRange(2000, 7000);
            setTimeout(function () {
                scope.update();
            }, rDel);
        }else{
            scope.removeAllChildren();
            scope._bulbs = [];
        }
    };
// public methods:
    p.createBulb = function(xPos,yPos,colorRGB){
        var bulb = alteastream.Assets.getImage(alteastream.Assets.images.flare);
        this.scene.addChild(bulb);
        bulb.x = xPos;
        bulb.y = yPos;
        bulb.regX = bulb.image.height*0.5;
        bulb.regY = bulb.image.height*0.5;
        //bulb.compositeOperation = "screen";

        bulb.mouseEnabled = false;
        bulb.filters = [
            new createjs.ColorFilter(0, 0, 0, colorRGB.a, colorRGB.r, colorRGB.g, colorRGB.b)
        ];
        bulb.cache(0,0,bulb.image.width,bulb.image.height);
        this._bulbs.push(bulb);
    };

    p.update = function(){
        for(var i = 0,j = this._bulbs.length;i<j;i++){
            var bulb = this._bulbs[i];
            var rRot = bulb.rotation +250;
            var rScale = gamecore.Utils.NUMBER.randomRange(0.3,1);
            var rDel = gamecore.Utils.NUMBER.randomRange(0.1,0.3);
            TweenMax.to(bulb,0.2,{delay:rDel,rotation:rRot,scaleX:rScale,scaleY:rScale,alpha:1});
            TweenMax.to(bulb,0.2,{delay:0.2+rDel,onComplete:this._loop,onCompleteParams:[this],rotation:rRot+45,scaleX:0.1,scaleY:0.1,alpha:0});
        }
    };

    p.dispose = function(){
        this.animate = false;
    };

    alteastream.Bulbs = Bulbs;
})();