
// namespace:
this.alteastream = this.alteastream || {};

(function () {
        "use strict";

var LightBeams = function(scene) {
    this.initialize_LightBeams(scene);
};
var p = LightBeams.prototype;

// static properties:
// public properties:
    p.beamImage = null;
    p.scene = null;
// private properties:
    p._beamsArray = null;
// constructor:

    p.initialize_LightBeams = function(scene) {
        this.scene = scene;
        this._beamsArray = [];
    };

// private methods:
    p._loop = function (scope) {
        scope.update();
    };
// public methods:
    p.createBeam = function(xPos,yPos,fromSide,colorRGB){
        var beam = alteastream.Assets.getImage(alteastream.Assets.images.beam);
        this.scene.addChild(beam);
        beam.x = xPos;
        beam.y = yPos;
        beam.regX = 0;
        beam.regY = beam.image.height*0.5;
       // beam.compositeOperation = "screen";
        if(fromSide === 1){
            beam.scaleX*=-1;
            beam.rotation = -60;
        }else beam.rotation = 60;

        beam.side = fromSide;
        beam.mouseEnabled = false;
        beam.filters = [
            new createjs.ColorFilter(0, 0, 0, colorRGB.a, colorRGB.r, colorRGB.g, colorRGB.b)
        ];
        beam.cache(0,0,beam.image.width,beam.image.height);
        this._beamsArray.push(beam);
    };

    p.update = function(){
        for(var i = 0,j = this._beamsArray.length;i<j;i++){
            var beam = this._beamsArray[i];
            var leftOrRight = beam.side === 0?80:-80;
            var rRot = gamecore.Utils.NUMBER.randomRange(0,leftOrRight);
            TweenMax.to(beam,2,{ease:Sine.easeInOut,onComplete:this._loop,onCompleteParams:[this],rotation:rRot});
        }
    };

    p.showBeams = function(show) {
        for(var i = 0; i < this._beamsArray.length; i++) {
            this._beamsArray[i].visible = show;
        }
    };

    alteastream.LightBeams = LightBeams;
})();