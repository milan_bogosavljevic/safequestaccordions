

// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";
var PreloaderStage = function(stage) {
    this.Stage_constructor(stage);
    this.initialize();
}
var p = PreloaderStage.prototype = createjs.extend(PreloaderStage,createjs.Stage);

// static properties:
// events:
// public properties:

// private properties:
    p._percent = null;
    p._registrants = null;
// constructor:
    p.initialize = function() {
        this._percent = new alteastream.Percent();
        this._registrants = [];
    };

// static methods:
// public methods:
    p.getProgress = function() {
        return this._percent.getDecimal();
    };

    p.setProgress = function(percent) {
        this._percent.set(percent);
        this.propagate();
    };

    p.register = function(observer) {
        if(!typeof observer.isPreloaderComponent === 'function'
            || !observer.isPreloaderComponent())
            throw "Can not register non PreloaderStageComponet object instance";

        this._registrants.push(observer);
    };

    p.propagate = function() {
        for(var index in this._registrants)
            this._registrants[index].setProgress(this._percent.getDecimal());
    };

    p.render = function(){
        var that = this;
        createjs.Ticker.timingMode = createjs.Ticker.RAF;
        that.enableMouseOver(20);
        createjs.Ticker.addEventListener("tick", function(event) {
            that.update(event);
        });
    };

// private methods:

alteastream.PreloaderStage = createjs.promote(PreloaderStage,"Stage");
})();