
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var PreloaderComponentImage = function(stage, logo, canv) {
    this.initialize(stage, logo, canv);
};

var p = PreloaderComponentImage.prototype = new alteastream.PreloaderComponent();

// static properties:
// events:
// public properties:
// private properties:
// constructor:
    p.PreloaderComponent_initialize = p.initialize;

    p.initialize = function(stage, logo, canv) {
        this.PreloaderComponent_initialize(stage);

        var shape = new createjs.Shape();
        shape.graphics.beginFill("#000000").drawRect(0, 0, canv.width, canv.height);
        shape.cache(0, 0, canv.width, canv.height);
        this.addChild(shape);

        this.addChild(logo);
        logo.mouseEnabled = false;

        /*var that = this;
        this.addOnProgressChangeAction(function() {
            image2.alpha = that.getProgress();
        });*/
    };

// static methods:
// public methods:
// private methods:
    p._getSaturation = function() {
        return (this.getProgress() - 1) * 100;
    };

     alteastream.PreloaderComponentImage = PreloaderComponentImage;
})();