
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var PreloaderComponent = function(stage) {
    this.initialize(stage);
}
var p = PreloaderComponent.prototype = new createjs.Container();

// static properties:
// events:
// public properties:
// private properties:
    p._percent = null;
    p._actions = null;

// constructor:
    p.Container_initialize = p.initialize;

    p.initialize = function(stage) {
        this.Container_initialize();
        this._percent = new alteastream.Percent();
        this._actions = [];
        if(stage){
            stage.register(this);
            stage.addChild(this);
            // debug log for iphone
            //stage.setChildIndex(logText,stage.numChildren-1);
        }
    };

// static methods:
// public methods:
    p.getProgress = function() {
        return this._percent.getDecimal();
    };
    p.getProgressPercent = function() {
        return this._percent.getPercent();
    }
    p.setProgress = function(percent) {
        this._percent.set(percent);
        this._performActions();
    };
    p.isPreloaderComponent = function () {
        return true;
    };
    p.addOnProgressChangeAction = function(action) {
        this._actions.push(action);
    };

// private methods:
    p._performActions = function() {
        for(var index in this._actions)
            this._actions[index]();
    };

alteastream.PreloaderComponent = PreloaderComponent;
})();