
// namespace:
this.alteastream = this.alteastream || {};

(function () {
    "use strict";

var Percent = function() {
    this.initialize();
}
var p = Percent.prototype;

// static properties:
// events:
// public properties:
// private properties:
    p._percent = 0;

// constructor:
    p.initialize = function() {}

// static methods:
// public methods:
    p.getPercent = function() {
        return this._percent * 100;
    }
    p.getDecimal = function() {
        return this._percent;
    }
    p.toString = function() {
        return this.getPercent() + " %";
    }
    p.set = function(percent) {
        if(percent < 0) throw "Percent can not be negative";
        if(percent > 1) percent = 1;

        this._percent = percent;
    }

// private methods:

alteastream.Percent = Percent;
})();